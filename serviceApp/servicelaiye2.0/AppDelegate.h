//
//  AppDelegate.h
//  servicelaiye2.0
//
//  Created by 盛东 on 15/12/9.
//  Copyright © 2015年 shengdong. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "WXApi.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate, RCIMConnectionStatusDelegate, CLLocationManagerDelegate, WXApiDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

