//
//  WYHistoryTableViewCell.h
//  servicelaiye2.0
//
//  Created by 汪洋 on 15/12/28.
//  Copyright © 2015年 shengdong. All rights reserved.
//

#import <UIKit/UIKit.h>

@class WYHistoryModel;

@interface WYHistoryTableViewCell : UITableViewCell

-(void)setContentByHistoryModel:(WYHistoryModel *)historyModel;

-(void)setRecordContentByHistoryModel:(WYHistoryModel *)historyModel;

-(UIImage *)getImage;
@end
