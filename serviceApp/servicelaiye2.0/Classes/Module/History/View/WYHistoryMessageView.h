//
//  WYHistoryMessageView.h
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/1/6.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WYHistoryModel.h"

@protocol WYHistoryMessageViewDelegate <NSObject>

@optional

-(void)hiddenHistoryMsgView;

@end

@interface WYHistoryMessageView : UIView

@property (nonatomic, strong) WYHistoryModel *historyModel;

@property (nonatomic,weak) id <WYHistoryMessageViewDelegate> historyMessageDelegate;

-(instancetype)initWithModel:(WYHistoryModel *)historyModel;

@end
