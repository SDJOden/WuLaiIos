//
//  WYHistoryHeaderView.m
//  servicelaiye2.0
//
//  Created by 汪洋 on 15/12/31.
//  Copyright © 2015年 shengdong. All rights reserved.
//

#import "WYHistoryHeaderView.h"


@interface WYHistoryHeaderView ()

@property (nonatomic, strong) UIButton *refreshBtn;

@property (nonatomic, strong) UIButton *connectUserBtn;

@property (nonatomic, strong) UIView *lineView;

@end

@implementation WYHistoryHeaderView

-(void)layoutSubviews{
    [super layoutSubviews];
    [self.refreshBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self).offset(self.center.x / 2.0);
        make.width.equalTo(60);
        make.height.equalTo(30);
        make.centerY.equalTo(self);
    }];
    [self.connectUserBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self).offset(-self.center.x / 2.0);
        make.width.equalTo(60);
        make.height.equalTo(30);
        make.centerY.equalTo(self);
        
    }];
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self);
        make.right.equalTo(self);
        make.width.equalTo(self);
        make.height.equalTo(1);
    }];
}

-(void)refresh{
    if ([self.historyHeaderViewDelegate respondsToSelector:@selector(refreshDidClicked:)]) {
        [self.historyHeaderViewDelegate
         refreshDidClicked:self];
    }
}

-(void)connect{
    if ([self.historyHeaderViewDelegate respondsToSelector:@selector(connectDidClicked:)]) {
        [self.historyHeaderViewDelegate connectDidClicked:self];
    }
}

- (UIButton *)refreshBtn {
    if(_refreshBtn == nil) {
        _refreshBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_refreshBtn setImage:[UIImage imageNamed:@"refresh_hover"] forState:UIControlStateNormal];
        [_refreshBtn setImage:[UIImage imageNamed:@"refresh"] forState:UIControlStateHighlighted];
        [_refreshBtn setTitle:@"刷新" forState:UIControlStateNormal];
        [_refreshBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        _refreshBtn.titleLabel.font = [UIFont boldSystemFontOfSize:15];
        [_refreshBtn addTarget:self action:@selector(refresh) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_refreshBtn];
    }
    return _refreshBtn;
}

- (UIButton *)connectUserBtn {
    if(_connectUserBtn == nil) {
        _connectUserBtn = [[UIButton alloc] init];
        _connectUserBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_connectUserBtn setImage:[UIImage imageNamed:@"transfer_hover"] forState:UIControlStateNormal];
        [_connectUserBtn setImage:[UIImage imageNamed:@"transfer"] forState:UIControlStateHighlighted];
//        [_connectUserBtn setImage:[UIImage imageNamed:@""] forState:UIControlStateDisabled];
        _connectUserBtn.titleLabel.font = [UIFont boldSystemFontOfSize:15];
        [_connectUserBtn setTitle:@"接入" forState:UIControlStateNormal];
        [_connectUserBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_connectUserBtn addTarget:self action:@selector(connect) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_connectUserBtn];
    }
    return _connectUserBtn;
}

- (UIView *)lineView {
    if(_lineView == nil) {
        _lineView = [[UIView alloc] init];
        UIView *lineView = [[UIView alloc]init];
        lineView.backgroundColor = [UIColor colorWithWhite:0.855 alpha:1.000];
        _lineView = lineView;
        [self addSubview:lineView];
    }
    return _lineView;
}


@end
