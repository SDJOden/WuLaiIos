//
//  WYHistorySearchView.m
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/1/12.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import "WYHistorySearchView.h"
#import "WYSearchDataModel.h"

@interface WYHistorySearchView () <UITextFieldDelegate>

@property (nonatomic, strong) UITextField *geniusNameTextField;
//
//@property (nonatomic, strong) UIButton *dateLbBtn;
//
//@property (nonatomic, strong) UIButton *searchBtn;

@property (nonatomic, strong) UIView *textBackgroundView;

@property (nonatomic, strong) UIView *lineView;

@end

@implementation WYHistorySearchView

-(void)layoutSubviews{
    [super layoutSubviews];
    
    [self.textBackgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(8);
        make.centerY.equalTo(self);
        make.left.equalTo(self).offset(20);
        make.right.equalTo(self).offset(-20);
    }];
    
//    [self.dateLbBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(self).offset(8);
//        make.centerY.equalTo(self);
//        make.left.equalTo(self).offset(10);
//        make.width.equalTo(100);
//    }];
    
    [self.geniusNameTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(8);
        make.centerY.equalTo(self);
        make.left.equalTo(self).offset(25);
        make.right.equalTo(self).offset(-25);
    }];
    
//    [self.searchBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(self).offset(8);
//        make.centerY.equalTo(self);
//        make.right.equalTo(self).offset(-10);
//        make.width.offset(80);
//    }];
    
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.bottom).offset(-1);
        make.height.equalTo(0.5);
        make.left.equalTo(self);
        make.right.equalTo(self);
    }];
    
}

- (UITextField *)geniusNameTextField {
	if(_geniusNameTextField == nil) {
		_geniusNameTextField = [[UITextField alloc] init];
        _geniusNameTextField.placeholder = @"请输入小来姓名或编号";
        _geniusNameTextField.textAlignment = NSTextAlignmentCenter;
        _geniusNameTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
        _geniusNameTextField.backgroundColor = [UIColor clearColor];
        _geniusNameTextField.font = [UIFont systemFontOfSize:12];
        _geniusNameTextField.leftViewMode = UITextFieldViewModeAlways;
        _geniusNameTextField.delegate = self;
        [self addSubview:_geniusNameTextField];
	}
	return _geniusNameTextField;
}

//- (UIButton *)searchBtn {
//	if(_searchBtn == nil) {
//		_searchBtn = [[UIButton alloc] init];
//        _searchBtn.backgroundColor = [UIColor colorWithRed:0.8118 green:0.8118 blue:0.8118 alpha:1.0];
//        _searchBtn.titleLabel.font = [UIFont systemFontOfSize:12];
//        [_searchBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//        [_searchBtn setTitle:@"搜索" forState:UIControlStateNormal];
//        [_searchBtn addTarget:self action:@selector(search) forControlEvents:UIControlEventTouchUpInside];
//        [self addSubview:_searchBtn];
//	}
//	return _searchBtn;
//}
//
//- (UIButton *)dateLbBtn {
//    if(_dateLbBtn == nil) {
//        _dateLbBtn = [[UIButton alloc] init];
//        _dateLbBtn.backgroundColor = [UIColor clearColor];
//        _dateLbBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
//        _dateLbBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0);
//        [_dateLbBtn setTitle:[NSString stringWithFormat:@"%@",[self stringWithDate:[NSDate date]]] forState:UIControlStateNormal];
//        [_dateLbBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//        _dateLbBtn.titleLabel.font = [UIFont systemFontOfSize:12];
//        [_dateLbBtn addTarget:self action:@selector(showDatePicker) forControlEvents:UIControlEventTouchUpInside];
//        [self addSubview:_dateLbBtn];
//    }
//    return _dateLbBtn;
//}

- (UIView *)textBackgroundView {
    if(_textBackgroundView == nil) {
        _textBackgroundView = [[UIView alloc] init];
        _textBackgroundView.backgroundColor = [UIColor colorWithRed:0.7842 green:0.7842 blue:0.7842 alpha:1.0];
        _textBackgroundView.layer.masksToBounds = YES;
        _textBackgroundView.layer.cornerRadius = 3.0;
        [self addSubview:_textBackgroundView];
    }
    return _textBackgroundView;
}

- (UIView *)lineView {
    if(_lineView == nil) {
        _lineView = [[UIView alloc] init];
        _lineView.backgroundColor = [UIColor colorWithWhite:0.855 alpha:1.000];
        [self addSubview:_lineView];
    }
    return _lineView;
}

//-(void)search{
//    if ([self.historySearchViewDelegate respondsToSelector:@selector(requestDataWithSearchModel:)]) {
//        [self.historySearchViewDelegate requestDataWithSearchModel:[WYSearchDataModel searchDataModelWithGeniusName:self.geniusNameTextField.text andWithDate:[self.dateLbBtn.titleLabel.text stringByReplacingOccurrencesOfString:@"-" withString:@""]]];
//    }
//}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
//    textField.textAlignment = NSTextAlignmentLeft;
//    if ([self.historySearchViewDelegate respondsToSelector:@selector(textFieldDidClicked)]) {
//        [self.historySearchViewDelegate textFieldDidClicked];
//    }
    
    [[NSNotificationCenter defaultCenter]postNotificationName:@"searchHis" object:self];
    
}

-(void)showDatePicker{
    if ([self.historySearchViewDelegate respondsToSelector:@selector(dateBtnDidClick)]) {
        [self endEditing:YES];
        [self.historySearchViewDelegate dateBtnDidClick];
    }
}

//-(void)changeDate:(NSDate *)date{
//    [self.dateLbBtn setTitle:[self stringWithDate:date] forState:UIControlStateNormal];
//}

-(NSString *)stringWithDate:(NSDate *)date{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    dateFormatter.dateFormat = @"yyyy-MM-dd";
    return [dateFormatter stringFromDate:date];
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self endEditing:YES];
}

-(NSString *)geniusStr{
    return self.geniusNameTextField.text;
}

//-(NSString *)dateStr{
//    return [self.dateLbBtn.titleLabel.text stringByReplacingOccurrencesOfString:@"-" withString:@""];
//}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

@end
