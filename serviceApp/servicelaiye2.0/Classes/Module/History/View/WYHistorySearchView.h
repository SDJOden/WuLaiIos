//
//  WYHistorySearchView.h
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/1/12.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import <UIKit/UIKit.h>

@class WYHistorySearchView;
@class WYSearchDataModel;

@protocol WYHistorySearchViewDelegate <NSObject>

@optional

-(void)dateBtnDidClick;

-(void)requestDataWithSearchModel:(WYSearchDataModel *)searchDataModel;

-(void)textFieldDidClicked;

@end

@interface WYHistorySearchView : UIView

@property (nonatomic,weak) id <WYHistorySearchViewDelegate> historySearchViewDelegate;

-(void)changeDate:(NSDate *)date;

-(NSString *)geniusStr;

-(NSString *)dateStr;

@end
