//
//  WYHistoryMessageView.m
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/1/6.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import "WYHistoryMessageView.h"
#import "SRRefreshView.h"
#import "SDJCommonSetting.h"
#import "RCMessageCell.h"
#import "SDJTextMessageCell.h"
#import "SDJImageMessageCell.h"
#import "SDJCardTypeOneCell.h"
#import "SDJCardTypeTwoCell.h"
#import "SDJCardTypeThreeCell.h"
#import "SDJCardTypeFourCell.h"
#import "SDJCardTypeFiveCell.h"
#import "RCLocationMessageCell.h"
#import "RCVoiceMessageCell.h"

@interface WYHistoryMessageView () <UICollectionViewDelegate, UICollectionViewDataSource, SRRefreshDelegate, RCMessageCellDelegate>

//@property (nonatomic, strong) UIView *headerView;

//@property (nonatomic, strong) UIButton *backBtn;

@property(nonatomic, strong) UICollectionView *conversationMessageCollectionView; // 展示会话的CollectionView控件，可以修改这个控件的属性比如背景

@property(nonatomic, strong) NSMutableArray *conversationDataRepository; // 会话数据存储数组

@property(nonatomic, strong) UICollectionViewFlowLayout *customFlowLayout;

@property(nonatomic, strong) SRRefreshView *slimeView;

@property (nonatomic, assign) NSInteger refreshFlag;
//计数
@property (nonatomic, assign) NSInteger dataNumber;
/**
 * 请求计数
 */
@property (nonatomic, assign) NSInteger requestCount;

@property (nonatomic, assign) int page;

@end

static NSString *const sdjtextCellIndentifier = @"sdjtextCellIndentifier";
static NSString *const sdjimageCellIndentifier = @"sdjimageCellIndentifier";
static NSString *const sdjCardTypeOneCellIndentifier = @"sdjCardTypeOneCellIndentifier";
static NSString *const sdjCardTypeTwoCellIndentifier = @"sdjCardTypeTwoCellIndentifier";
static NSString *const sdjCardTypeThreeCellIndentifier = @"sdjCardTypeThreeCellIndentifier";
static NSString *const sdjCardTypeFourCellIndentifier = @"sdjCardTypeFourCellIndentifier";
static NSString *const sdjCardTypeFiveCellIndentifier = @"sdjCardTypeFiveCellIndentifier";
static NSString *const sdjlocationCellIndentifier = @"sdjlocationCellIndentifier";
static NSString *const sdjvoiceCellIndentifier = @"sdjvoiceCellIndentifier";

@implementation WYHistoryMessageView

-(instancetype)initWithModel:(WYHistoryModel *)historyModel{
    if (self = [super init]) {
        self.historyModel = historyModel;
        self.dataNumber = 0;
        //初始化
        self.page = 1;
        [self requestHistoryMsg];
    }
    return self;
}

-(void)requestHistoryMsg {
    self.requestCount = 0;
    __weak typeof(self) __weakSelf = self;
    [[SDJNetworkTools sharedTools]userCurrentMessage:[SDJUserAccount loadAccount].token user_id:@(self.historyModel.inner_uid).stringValue size:@"10" pn:@(self.page).stringValue finished:^(NSDictionary<NSString *,id> * _Nullable result, NSError * _Nullable error) {
        if (![result[@"error"] isEqual:[NSNull null]]) {//异端登陆
            NSNumber *code = result[@"error"][@"error_code"];
            if (code.integerValue == 20000) {
                NSString *UserPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).firstObject stringByAppendingPathComponent:@"account.plist"];
                [[NSFileManager defaultManager]removeItemAtPath:UserPath error:nil];
                [[RCIMClient sharedRCIMClient] logout];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"WYRootViewControllerSwitchNotification" object:@"0"];
                    
                });
            }
        }
        else {
            if (__weakSelf.page == 1) {
                [__weakSelf.conversationDataRepository removeAllObjects];
            }
            NSArray *historyMsgArray = result[@"data"][@"latest_msgs"];
            __weakSelf.dataNumber = historyMsgArray.count;
            NSMutableArray *mutableDataArray = [NSMutableArray array];
            for (NSDictionary *historyMsg in historyMsgArray) {
                int msgType = ((NSNumber *)historyMsg[@"extra"][@"msg_type"]).intValue;
                if (msgType == 1 || msgType == 3) {//text消息
                    RCMessageModel * msgModel = [WYTool createMsgModel:historyMsg];
                    [mutableDataArray addObject:msgModel];
                    __weakSelf.requestCount ++;
                    if (_requestCount == __weakSelf.dataNumber) {
                        [__weakSelf addDataToConversationDataRepository:mutableDataArray];
                        [__weakSelf.conversationMessageCollectionView reloadData];
                        [__weakSelf scrollToSpace:mutableDataArray];
                    }
                }
                else if (msgType == 2) {//image消息
                    RCMessageModel * msgModel = [WYTool createImageModel:historyMsg];
                    UIImageView *imageView = [[UIImageView alloc]init];
                    [mutableDataArray addObject:msgModel];
                    [imageView sd_setImageWithURL:[NSURL URLWithString:historyMsg[@"imageUri"]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            ((RCImageMessage *)msgModel.content).thumbnailImage = imageView.image;
                            ((RCImageMessage *)msgModel.content).originalImage = imageView.image;
                            __weakSelf.requestCount ++;
                            if (_requestCount == __weakSelf.dataNumber) {
                                [__weakSelf addDataToConversationDataRepository:mutableDataArray];
                                [__weakSelf.conversationMessageCollectionView reloadData];
                                [__weakSelf scrollToSpace:mutableDataArray];
                            }
                        });
                    }];
                    
                }
                else if(msgType == 5) {
                    RCMessageModel * msgModel = [WYTool createVoiceMsgModel:historyMsg];
                    [mutableDataArray addObject:msgModel];
                    __weakSelf.requestCount ++;
                    if (_requestCount == __weakSelf.dataNumber) {
                        [__weakSelf addDataToConversationDataRepository:mutableDataArray];
                        [__weakSelf.conversationMessageCollectionView reloadData];
                        [__weakSelf scrollToSpace:mutableDataArray];
                    }
                }
                else if (msgType == 6) {
                    RCMessageModel * msgModel = [WYTool createCardMsgModel:historyMsg];
                    [mutableDataArray addObject:msgModel];
                    __weakSelf.requestCount ++;
                    if (_requestCount == __weakSelf.dataNumber) {
                        [__weakSelf addDataToConversationDataRepository:mutableDataArray];
                        [__weakSelf.conversationMessageCollectionView reloadData];
                        [__weakSelf scrollToSpace:mutableDataArray];
                    }
                }
                else if (msgType == 7) {
                    RCMessageModel * msgModel = [WYTool createSingleCardMsgModel:historyMsg];
                    [mutableDataArray addObject:msgModel];
                    __weakSelf.requestCount ++;
                    if (_requestCount == __weakSelf.dataNumber) {
                        [__weakSelf addDataToConversationDataRepository:mutableDataArray];
                        [__weakSelf.conversationMessageCollectionView reloadData];
                        [__weakSelf scrollToSpace:mutableDataArray];
                    }
                }
                else {
                    __weakSelf.requestCount ++;
                    if (_requestCount == __weakSelf.dataNumber) {
                        [__weakSelf addDataToConversationDataRepository:mutableDataArray];
                        [__weakSelf.conversationMessageCollectionView reloadData];
                        [__weakSelf scrollToSpace:mutableDataArray];
                    }
                }
            }
        }
    }];
}

/**
 *  加入到显示的数组
 */
-(void)addDataToConversationDataRepository:(NSArray *)dataArray{
    for (int i = 0; i < dataArray.count; i ++) {
        [self.conversationDataRepository insertObject:dataArray[i] atIndex:i];
    }
}

/**
 *  滚动到恰当的地方
 */
-(void)scrollToSpace:(NSArray *)dataArray{
    if (self.page == 1) {
        [self.conversationMessageCollectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:self.conversationDataRepository.count - 1 inSection:0] atScrollPosition:UICollectionViewScrollPositionTop animated:NO];
    }
    else if (self.page > 1) {
        NSIndexPath *indexPath =[NSIndexPath indexPathForRow:dataArray.count inSection:0];
        [self.conversationMessageCollectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionTop animated:NO];
    }
    _requestCount = 0;
}


///**
// *  导航栏
// */
//- (UIView *)headerView {
//    if(_headerView == nil) {
//        _headerView = [[UIView alloc] init];
//        _headerView.backgroundColor = [UIColor whiteColor];
//        [self addSubview:_headerView];
//    }
//    return _headerView;
//}
//
///**
// *  导航栏按钮
// */
//- (UIButton *)backBtn {
//    if(_backBtn == nil) {
//        _backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//        [_backBtn setTitle:@"返回" forState:UIControlStateNormal];
//        [_backBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//        [_backBtn addTarget:self action:@selector(msgTableViewHiden) forControlEvents:UIControlEventTouchUpInside];
//        [_backBtn.layer setMasksToBounds:YES];
//        [_backBtn.layer setCornerRadius:5.0];//设置矩圆角半径
//        [_backBtn.layer setBorderWidth:1.0];//边框宽度
//        CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
//        CGColorRef colorref = CGColorCreate(colorSpace,(CGFloat[]){ 0.667, 0.667, 0.667, 1 });
//        [_backBtn.layer setBorderColor:colorref];
//        CGColorSpaceRelease(colorSpace);
//        CGColorRelease(colorref);
//        [_headerView addSubview:_backBtn];
//    }
//    return _backBtn;
//}

/**
 *  对话视图
 */
- (UICollectionView *)conversationMessageCollectionView {
    if (!_conversationMessageCollectionView) {
        _conversationMessageCollectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:self.customFlowLayout];
//        _conversationMessageCollectionView.collectionViewLayout = self.customFlowLayout;
        [_conversationMessageCollectionView setBackgroundColor:HEXCOLOR(0xf6f6f6)];// MARK 单聊背景色
        _conversationMessageCollectionView.showsHorizontalScrollIndicator = NO;
        _conversationMessageCollectionView.alwaysBounceVertical = YES;
        
        //注册cell
        [self registerClass:[SDJTextMessageCell class] forCellWithReuseIdentifier:sdjtextCellIndentifier];
        [self registerClass:[SDJImageMessageCell class] forCellWithReuseIdentifier:sdjimageCellIndentifier];
        [self registerClass:[RCLocationMessageCell class] forCellWithReuseIdentifier:sdjlocationCellIndentifier];
        [self registerClass:[RCVoiceMessageCell class] forCellWithReuseIdentifier:sdjvoiceCellIndentifier];
        [self registerClass:[SDJCardTypeOneCell class] forCellWithReuseIdentifier:sdjCardTypeOneCellIndentifier];
        [self registerClass:[SDJCardTypeTwoCell class] forCellWithReuseIdentifier:sdjCardTypeTwoCellIndentifier];
        [self registerClass:[SDJCardTypeThreeCell class] forCellWithReuseIdentifier:sdjCardTypeThreeCellIndentifier];
        [self registerClass:[SDJCardTypeFourCell class] forCellWithReuseIdentifier:sdjCardTypeFourCellIndentifier];
        [self registerClass:[SDJCardTypeFiveCell class] forCellWithReuseIdentifier:sdjCardTypeFiveCellIndentifier];
        
        self.conversationMessageCollectionView.dataSource = self;
        self.conversationMessageCollectionView.delegate = self;
        [self addSubview:_conversationMessageCollectionView];
        
//        // 点击聊天页滚到底部的tap手势
//        UITapGestureRecognizer *resetBottomTapGesture =
//        [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap4ResetDefaultBottomBarStatus:)];
//        [resetBottomTapGesture setDelegate:self];
//        [_conversationMessageCollectionView addGestureRecognizer:resetBottomTapGesture];
    }
    return _conversationMessageCollectionView;
}
- (void)registerClass:(Class)cellClass forCellWithReuseIdentifier:(NSString *)identifier {
    [self.conversationMessageCollectionView registerClass:cellClass forCellWithReuseIdentifier:identifier];
}

/**
 *  下拉刷新控件
 */
- (SRRefreshView *)slimeView {
    if(_slimeView == nil) {
        _slimeView = [[SRRefreshView alloc] init];
        _slimeView.delegate = self;
        _slimeView.upInset = 0;
        _slimeView.slimeMissWhenGoingBack = YES;
        _slimeView.slime.bodyColor = [UIColor blackColor];
        _slimeView.slime.skinColor = [UIColor whiteColor];
        _slimeView.slime.lineWith = 1;
        _slimeView.slime.shadowBlur = 4;
        _slimeView.slime.shadowColor = [UIColor blackColor];
        [_conversationMessageCollectionView addSubview:_slimeView];
    }
    return _slimeView;
}

/**
 *  collectionView的布局
 */
- (UICollectionViewFlowLayout *)customFlowLayout {
    if(_customFlowLayout == nil) {
        _customFlowLayout = [[UICollectionViewFlowLayout alloc] init];
        _customFlowLayout.minimumLineSpacing = 0.0f;
        _customFlowLayout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
        _customFlowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    }
    return _customFlowLayout;
}

-(void)layoutSubviews{
    [super layoutSubviews];
//    [self.headerView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(self);
//        make.left.equalTo(self);
//        make.right.equalTo(self);
//        make.height.equalTo(44);
//    }];
//    
//    [self.backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.centerY.equalTo(self.headerView);
//        make.left.equalTo(self.headerView).offset(10);
//        make.width.equalTo(50);
//        make.height.equalTo(30);
//    }];
    
    [self.conversationMessageCollectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self);
        make.left.equalTo(self);
        make.right.equalTo(self);
        make.bottom.equalTo(self);
    }];
    
    self.slimeView.frame = CGRectMake(0, -40, self.bounds.size.width, 40);
}

-(void)msgTableViewHiden{
    if ([self.historyMessageDelegate respondsToSelector:@selector(hiddenHistoryMsgView)]) {
        [self.historyMessageDelegate hiddenHistoryMsgView];
    }
}

#pragma mark --- UICollectionViewDelegate
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.conversationDataRepository.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    RCMessageModel *model =[self.conversationDataRepository objectAtIndex:indexPath.row];
    
    //    model.isDisplayNickname = NO;
    
    if (indexPath.row == 0) {model.isDisplayMessageTime = NO;}
    
    RCMessageContent *messageContent = model.content;
    
    RCMessageCell *cell = nil;
    
    if ([messageContent isMemberOfClass:[RCTextMessage class]]) {
        
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:sdjtextCellIndentifier forIndexPath:indexPath];
        
        [cell setDataModel:model with:NO];
        [cell setDelegate:self];
        
    } else if ([messageContent isMemberOfClass:[RCImageMessage class]]) {
        
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:sdjimageCellIndentifier
                                                         forIndexPath:indexPath];
        [cell setDataModel:model with:NO];
        [cell setDelegate:self];
        
    } else if ([messageContent isMemberOfClass:[RCLocationMessage class]]) {
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:sdjlocationCellIndentifier forIndexPath:indexPath];
        [cell setDataModel:model with:NO];
        [cell setDelegate:self];
        
    } else if ([messageContent isMemberOfClass:[RCVoiceMessage class]]) {
        cell = [collectionView
                dequeueReusableCellWithReuseIdentifier:sdjvoiceCellIndentifier
                forIndexPath:indexPath];
        [cell setDataModel:model with:NO];
        [cell setDelegate:self];
        
    } else if ([messageContent isMemberOfClass:[RCRichContentMessage class]]) {
        
        RCRichContentMessage *message = (RCRichContentMessage *)messageContent;
        
        int title = [message.title intValue];
        
        switch (title) {
            case 6:
                cell = [collectionView dequeueReusableCellWithReuseIdentifier:sdjCardTypeOneCellIndentifier forIndexPath:indexPath];
                break;
            case 7:
                cell = [collectionView dequeueReusableCellWithReuseIdentifier:sdjCardTypeTwoCellIndentifier forIndexPath:indexPath];
                break;
            case 8:
                cell = [collectionView dequeueReusableCellWithReuseIdentifier:sdjCardTypeFiveCellIndentifier forIndexPath:indexPath];
                break;
            default:
                break;
        }
        [cell setDataModel:model with:NO];
        [cell setDelegate:self];
    }
    return cell;
}

//-(void)setDataArray:(NSMutableArray *)mutableArray{
//    self.conversationDataRepository = [mutableArray mutableCopy];
//}

#pragma mark <UICollectionViewDelegateFlowLayout>

//计算cell高度
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    RCMessageModel *model = [self.conversationDataRepository objectAtIndex:indexPath.row];
    
    if (indexPath.row == 0) {model.isDisplayMessageTime = NO;}
    
    model.isDisplayNickname = NO;
    
    RCMessageContent *messageContent = model.content;
    if ([messageContent isMemberOfClass:[RCTextMessage class]] ||
        [messageContent isMemberOfClass:[RCImageMessage class]] ||
        [messageContent isMemberOfClass:[RCLocationMessage class]] ||
        [messageContent isMemberOfClass:[RCVoiceMessage class]] ||
        [messageContent isMemberOfClass:[RCRichContentMessage class]]) {
        
        return [self sizeForItem:collectionView atIndexPath:indexPath];
    } else {
        return CGSizeZero;
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    return CGSizeZero;
}

- (CGSize)sizeForItem:(UICollectionView *)collectionView //信息尺寸自定义
          atIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat __width = CGRectGetWidth(collectionView.frame);
    
    RCMessageModel *model = [self.conversationDataRepository objectAtIndex:indexPath.row];
    RCMessageContent *messageContent = model.content;
    
    model.isDisplayNickname = NO; // 不显示名字
    
    if (indexPath.row == 0) {model.isDisplayMessageTime = NO;}
    
    CGFloat __height = 0.0f;
    
    if ([messageContent isMemberOfClass:[RCTextMessage class]]) {
        
        RCTextMessage *_textMessage = (RCTextMessage *)messageContent;
        
        NSString *text = _textMessage.content;
        
        NSString *pattern = @"<a href=\'(.*?)\'>(.*?)</a>";
        
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern options:0 error:nil];
        
        NSArray *matches = [regex matchesInString:text options:0 range:NSMakeRange(0, text.length)]; // 网址
        
        NSUInteger lastIdx = 0;
        
        NSMutableString *attributedString = [NSMutableString stringWithCapacity:0]; // 总字符串
        
        NSMutableString *realTextString = [NSMutableString stringWithCapacity:0];
        
        if (matches.count) { // 解析部分bug, 1.文字需要提出来, 2. 位置需要调整 matchs.count
            
            NSMutableArray *urlArrs = [[NSMutableArray alloc] init]; // 存url
            int i = 0;
            
            for (NSTextCheckingResult* match in matches)
            {
                NSRange range = match.range;
                
                if (range.location > lastIdx)
                {
                    NSString *temp = [text substringWithRange:NSMakeRange(lastIdx, range.location - lastIdx)];
                    [attributedString appendString:temp];// 记录头
                    [realTextString appendString:temp];
                }
                NSString *title = [text substringWithRange:[match rangeAtIndex:2]]; // 标题
                NSString *url = [text substringWithRange:[match rangeAtIndex:1]]; // 网址
                
                lastIdx = range.location + range.length;
                [attributedString appendFormat:@"<%d>%@</%d>",i,title,i];
                [realTextString appendString:title];
                [urlArrs addObject:url];
                i++;
            }
            if (lastIdx < text.length)
            {
                NSString  *temp = [text substringFromIndex:lastIdx];
                [attributedString appendString:temp];// 记录尾
                [realTextString appendString:temp];
            }
        } else {
            [realTextString appendString:_textMessage.content];
        }
        CGSize __textSize = CGSizeZero;
        if (IOS_FSystenVersion < 7.0) {
            __textSize = RC_MULTILINE_TEXTSIZE_LIOS7(realTextString, [UIFont systemFontOfSize:Text_Message_Font_Size], CGSizeMake(__width -
                                                                                                                                  (10 + [RCIM sharedRCIM].globalMessagePortraitSize.width + 5) * 2 - 5 -
                                                                                                                                  20 * 2,
                                                                                                                                  8000), NSLineBreakByTruncatingTail);
        }else {
            __textSize = RC_MULTILINE_TEXTSIZE_GEIOS7(realTextString, [UIFont systemFontOfSize:Text_Message_Font_Size], CGSizeMake(__width -
                                                                                                                                   (10 + [RCIM sharedRCIM].globalMessagePortraitSize.width + 5) * 2 - 5 -
                                                                                                                                   20 * 2,
                                                                                                                                   8000));
        }
        __textSize = CGSizeMake(ceilf(__textSize.width), ceilf(__textSize.height));
        CGSize __labelSize = CGSizeMake(__textSize.width, __textSize.height);
        CGFloat __bubbleHeight = __labelSize.height + 12 + 12 < 46 ? 46 : (__labelSize.height + 12 + 12);
        __height = __bubbleHeight;
        
    } else if ([messageContent isMemberOfClass:[RCLocationMessage class]]) {
        
        __height = 140;
    } else if ([messageContent isMemberOfClass:[RCVoiceMessage class]]) {
        
        __height = 46;
        
    } else if ([messageContent isMemberOfClass:[RCRichContentMessage class]]) {
        
        RCRichContentMessage *message = (RCRichContentMessage *)messageContent;
        
        int title = [message.title intValue];
        
        switch (title) {
            case 6:
                __height = CARDONE_HEIGHT;
                break;
            case 7:
                __height = CARDTWO_HEIGHT;
                break;
            case 8:
                __height = CARDFIVE_HEIGHT;
                break;
            default:
                break;
        }
        
    } else if ([messageContent isMemberOfClass:[RCImageMessage class]]) {
        RCImageMessage *_imageMessage = (RCImageMessage *)messageContent;
        
        CGSize imageSize = _imageMessage.thumbnailImage.size;
        
        //兼容240
        CGFloat imageWidth = 120;
        CGFloat imageHeight = 120;
        
        CGFloat imageWidthMin = 60;
        CGFloat imageHeightMin = 60;
        
        CGFloat imageWidthMax = [UIScreen mainScreen].bounds.size.width * 0.5;
        CGFloat imageHeightMax = [UIScreen mainScreen].bounds.size.height * 0.3;
        
//        CGFloat imageWidthMax = self.conversationMessageCollectionView.frame.size.width * 0.8;
//        CGFloat imageHeightMax = self.conversationMessageCollectionView.frame.size.height * 0.6;
        
        CGFloat scale = imageWidthMax * 1.0 / imageHeightMax;
        if (imageSize.width < imageWidthMin && imageSize.height < imageHeightMin) {
            if (imageSize.width >= imageSize.height) {
                if (imageSize.width * 1.0 / imageSize.height >= imageWidthMax * 1.0 / imageHeightMin) {
                    imageWidth = imageWidthMax;
                    imageHeight = imageSize.height *imageWidthMax * 1.0 / imageSize.width;
                } else {
                    imageHeight = imageHeightMin;
                    imageWidth = imageSize.width * imageHeightMin * 1.0 / imageSize.height;
                }
            } else {
                if (imageSize.height * 1.0 / imageSize.width >= imageHeightMax * 1.0 / imageWidthMin) {
                    imageHeight = imageHeightMax;
                    imageWidth = imageSize.width *imageHeightMax * 1.0 / imageSize.height;
                } else {
                    imageWidth = imageWidthMin;
                    imageHeight = imageSize.height * imageWidthMin * 1.0 / imageSize.width;
                }
            }
        } else if (imageSize.width > imageWidthMax || imageSize.height > imageHeightMax) {
            if (imageSize.width >= imageSize.height * scale) {
                imageWidth = imageWidthMax;
                imageHeight = imageSize.height *imageWidthMax * 1.0 / imageSize.width;
            } else {
                imageHeight = imageHeightMax;
                imageWidth = imageSize.width * imageHeightMax * 1.0 / imageSize.height;
            }
        } else {
            imageWidth = imageSize.width;
            imageHeight = imageSize.height;
        }
        
        //图片half
        imageSize = CGSizeMake(imageWidth, imageHeight);
        __height = imageSize.height;
    }
    
    if (__height < [RCIM sharedRCIM].globalMessagePortraitSize.height) {
        __height = [RCIM sharedRCIM].globalMessagePortraitSize.height;
    }
    
    //上边距
    __height = __height + 10;
    
    if (model.isDisplayMessageTime) {
        __height = __height + 20 + 10;
    }
    //下边距
    __height = __height + 30;
    
    return CGSizeMake(__width, __height);
}

#pragma mark - slimeRefresh delegate
- (void)slimeRefreshStartRefresh:(SRRefreshView *)refreshView {
    [_slimeView performSelector:@selector(endRefresh) withObject:nil afterDelay:3 inModes:[NSArray arrayWithObject:NSRunLoopCommonModes]];
}

- (NSMutableArray *)conversationDataRepository {
	if(_conversationDataRepository == nil) {
		_conversationDataRepository = [[NSMutableArray alloc] init];
	}
	return _conversationDataRepository;
}

#pragma mark <UIScrollViewDelegate>
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (_refreshFlag == 0) {
        _refreshFlag ++;
        return;
    }
    if (_refreshFlag >= 1) {
        [_slimeView scrollViewDidScroll];
        if (scrollView.contentOffset.y < -5.0f) {
            [self slimeRefreshStartRefresh:_slimeView];
        } else {
            [_slimeView endRefresh];
        }
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView
                  willDecelerate:(BOOL)decelerate {
    [_slimeView scrollViewDidEndDraging];
    if (scrollView.contentOffset.y < -15) {
        [self performSelector:@selector(loadMoreHistoryMessage) withObject:nil afterDelay:0.4f];
    }
}

-(void)loadMoreHistoryMessage{
    self.page ++;
    [self requestHistoryMsg];
    
}

- (void)scrollToBottomAnimated:(BOOL)animated {
    
    //    if ([self.conversationMessageCollectionView numberOfSections] == 0) {return;}
    
    NSUInteger finalRow = MAX(0, [self.conversationMessageCollectionView numberOfItemsInSection:0] - 1);
    
    if (0 == finalRow) {return;}
    
    NSIndexPath *finalIndexPath = [NSIndexPath indexPathForItem:finalRow inSection:0];
    
    [self.conversationMessageCollectionView scrollToItemAtIndexPath:finalIndexPath atScrollPosition:UICollectionViewScrollPositionBottom animated:animated];
}

//长按消息内容
- (void)didLongTouchMessageCell:(RCMessageModel *)model inView:(UIView *)view {
//    CGRect rect = [self convertRect:view.frame fromView:view.superview];
//    UIMenuController *menu = [UIMenuController sharedMenuController];
//    UIMenuItem *copyItem = [[UIMenuItem alloc]initWithTitle:NSLocalizedString(@"复制", @"") action:@selector(onCopyMessage:)];
//    UIMenuItem *deleteItem = [[UIMenuItem alloc]initWithTitle:NSLocalizedString(@"删除", @"") action:@selector(onDeleteMessage:)];
//    if ([model.content isMemberOfClass:[RCTextMessage class]]) {
//        
//        [menu setMenuItems:[NSArray arrayWithObjects:copyItem, deleteItem, nil]];
//    } else {
//        [menu setMenuItems:@[ deleteItem ]];
//    }
//    [menu setTargetRect:rect inView:self];
//    [menu setMenuVisible:YES animated:YES];
}

- (void)dealloc {
    self.conversationMessageCollectionView.dataSource = nil;
    self.conversationMessageCollectionView.delegate = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


@end
