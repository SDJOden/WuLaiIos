//
//  WYHistoryHeaderView.h
//  servicelaiye2.0
//
//  Created by 汪洋 on 15/12/31.
//  Copyright © 2015年 shengdong. All rights reserved.
//

#import <UIKit/UIKit.h>

@class WYHistoryHeaderView;

@protocol WYHistoryHeaderViewDelegate <NSObject>

@optional

-(void)refreshDidClicked:(WYHistoryHeaderView *)headerView;

-(void)connectDidClicked:(WYHistoryHeaderView *)headerView;

@end

@interface WYHistoryHeaderView : UIView

@property(nonatomic,weak)id <WYHistoryHeaderViewDelegate> historyHeaderViewDelegate;

@end
