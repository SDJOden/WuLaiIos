//
//  WYHistoryTableViewCell.m
//  servicelaiye2.0
//
//  Created by 汪洋 on 15/12/28.
//  Copyright © 2015年 shengdong. All rights reserved.
//

#import "WYHistoryTableViewCell.h"
#import "WYHistoryModel.h"

@interface WYHistoryTableViewCell ()
/**
 *  用户头像
 */
@property (nonatomic, strong) UIImageView *iconImageView;
/**
 *  用户ID
 */
@property (nonatomic, strong) UILabel *userIdLb;
/**
 *  昵称
 */
@property (nonatomic, strong) UILabel *nicknameLb;
/**
 *  助理信息
 */
@property (nonatomic, strong) UILabel *geniusInforLb;
/**
 *  最近一次说话时间
 */
@property (nonatomic, strong) UILabel *recentTimeLb;

@end

@implementation WYHistoryTableViewCell

-(void)setContentByHistoryModel:(WYHistoryModel *)historyModel{
//    historyModel;
    if (![historyModel.headimgurl isKindOfClass:[NSNull class]]) {
        [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:historyModel.headimgurl] placeholderImage:[UIImage imageNamed:@"portrait_default"] options:SDWebImageRetryFailed completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            
        }];
    }else {
        [self.iconImageView setImage:[UIImage imageNamed:@"portrait_default"]];
    }
    
    self.userIdLb.text = @(historyModel.inner_uid).stringValue;
    if (historyModel.nickname.length != 0) {
        self.nicknameLb.text = historyModel.nickname;
    }
    else {
        self.nicknameLb.text = [NSString stringWithFormat:@"#%@", @(historyModel.inner_uid).stringValue];
    }
    
    if (!(historyModel.ts == 0)) {
        self.recentTimeLb.text = [self computeTime:historyModel.ts]/*@(historyModel.ts).stringValue*/;
    } else {
        self.recentTimeLb.text = @"";
    }
    
    if([historyModel.vip isKindOfClass:[NSNull class]]) {
        self.userIdLb.textColor = [UIColor blackColor];
        self.nicknameLb.textColor = [UIColor blackColor];
    }else {
        self.userIdLb.textColor = [UIColor redColor];
        self.nicknameLb.textColor = [UIColor redColor];
    }
    
}

-(void)setRecordContentByHistoryModel:(WYHistoryModel *)historyModel{
    [self setContentByHistoryModel:historyModel];
    if (historyModel.username.length == 0) {
        self.geniusInforLb.text = @"";
        return;
    }
    self.geniusInforLb.text = [NSString stringWithFormat:@"%@ %@", historyModel.username, historyModel.realname];
}

-(NSString *)computeTime:(long)ts{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";//大写HH是24小时制，小写hh是12小时制
    NSDate *tsData = [NSDate dateWithTimeIntervalSince1970:ts];
    return [dateFormatter stringFromDate:tsData];
}

-(void)layoutSubviews{
    [super layoutSubviews];
    
    [self.iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(10);
        make.left.equalTo(self).offset(10);
        make.width.equalTo(50);
        make.height.equalTo(50);
    }];
    
    [self.userIdLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(10);
        make.left.equalTo(self.iconImageView.right).offset(10);
        make.width.equalTo(110);
        make.height.equalTo(20);
    }];
    
    [self.nicknameLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(10);
        make.left.equalTo(self.userIdLb.right).offset(5);
        make.right.equalTo(self).offset(-10);
        make.height.equalTo(20);
    }];
    
    [self.geniusInforLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.userIdLb.bottom).offset(10);
        make.left.equalTo(self.userIdLb.left);
        make.width.equalTo(110);
        make.height.equalTo(20);
    }];
    
    [self.recentTimeLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.nicknameLb.bottom).offset(10);
        make.right.equalTo(self).offset(-10);
        make.left.equalTo(self.nicknameLb.left);
        make.height.equalTo(20);
    }];
}

- (UIImageView *)iconImageView {
    if(_iconImageView == nil) {
        _iconImageView = [[UIImageView alloc] init];
        _iconImageView.layer.masksToBounds = YES;
        _iconImageView.layer.cornerRadius = 5.0;
        
        [self.contentView addSubview:_iconImageView];
    }
    return _iconImageView;
}

- (UILabel *)userIdLb {
	if(_userIdLb == nil) {
		_userIdLb = [[UILabel alloc] init];
        _userIdLb.font = [UIFont systemFontOfSize:12];
//        _userIdLb.backgroundColor = [UIColor orangeColor];
        [self.contentView addSubview:_userIdLb];
	}
	return _userIdLb;
}

- (UILabel *)nicknameLb {
	if(_nicknameLb == nil) {
        _nicknameLb = [[UILabel alloc] init];
        _nicknameLb.font = [UIFont systemFontOfSize:12];
//        _nicknameLb.backgroundColor = [UIColor cyanColor];
        [self.contentView addSubview:_nicknameLb];
	}
	return _nicknameLb;
}

- (UILabel *)geniusInforLb {
	if(_geniusInforLb == nil) {
        _geniusInforLb = [[UILabel alloc] init];
        _geniusInforLb.font = [UIFont systemFontOfSize:12];
//        _geniusInforLb.backgroundColor = [UIColor greenColor];
        _geniusInforLb.text = @"--";
        [self.contentView addSubview:_geniusInforLb];
	}
	return _geniusInforLb;
}

- (UILabel *)recentTimeLb {
	if(_recentTimeLb == nil) {
        _recentTimeLb = [[UILabel alloc] init];
        _recentTimeLb.font = [UIFont systemFontOfSize:12];
//        _recentTimeLb.backgroundColor = [UIColor redColor];
        [self.contentView addSubview:_recentTimeLb];
	}
	return _recentTimeLb;
}

-(UIImage *)getImage{
    return self.iconImageView.image;
}

@end
