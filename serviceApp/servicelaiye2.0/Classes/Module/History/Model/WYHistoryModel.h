//
//  WYHistoryModel.h
//  servicelaiye2.0
//
//  Created by 汪洋 on 15/12/28.
//  Copyright © 2015年 shengdong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WYWaitingUserData.h"


@interface WYHistoryModel : NSObject

@property (nonatomic, strong) NSString *headimgurl;

@property (nonatomic, assign) long inner_uid;

@property (nonatomic, copy) NSString *nickname;

@property (nonatomic, assign) long ts;

@property (nonatomic, assign) int user_id;

@property (nonatomic, copy) NSString *realname;

@property (nonatomic, copy) NSString *username;

@property (nonatomic, copy) NSDictionary *vip;

-(void)modelByWYWaitingUserData:(WYWaitingUserData *)userData;

@end
