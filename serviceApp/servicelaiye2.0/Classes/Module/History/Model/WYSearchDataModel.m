//
//  WYSearchDataModel.m
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/1/28.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import "WYSearchDataModel.h"

@implementation WYSearchDataModel

+(instancetype)searchDataModelWithGeniusName:(NSString *)geniusName andWithDate:(NSString *)date{
    return [[self alloc]initWithGeniusName:geniusName andWithDate:date];
}

-(instancetype)initWithGeniusName:(NSString *)geniusName andWithDate:(NSString *)date{
    if (self = [super init]) {
        self.geniusName = geniusName;
        self.date = date;
    }
    return self;
}

@end
