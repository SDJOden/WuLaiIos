//
//  WYHistoryModel.m
//  servicelaiye2.0
//
//  Created by 汪洋 on 15/12/28.
//  Copyright © 2015年 shengdong. All rights reserved.
//

#import "WYHistoryModel.h"
#import "WYWaitingUserData.h"
#import "WYWaitingUsersWechat.h"

@interface WYHistoryModel ()

@end

@implementation WYHistoryModel

-(void)modelByWYWaitingUserData:(WYWaitingUserData *)userData{
    self.inner_uid = userData.inner_uid.integerValue;
    self.nickname = userData.wechat.nickname;
    self.user_id = userData.user_id.intValue;
}

@end
