//
//  WYSearchDataModel.h
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/1/28.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WYSearchDataModel : NSObject

@property (nonatomic, copy) NSString *geniusName;

@property (nonatomic, copy) NSString *date;

+(instancetype)searchDataModelWithGeniusName:(NSString *)geniusName andWithDate:(NSString *)date;

@end
