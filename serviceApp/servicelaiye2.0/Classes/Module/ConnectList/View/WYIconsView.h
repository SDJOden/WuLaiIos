//
//  WYIconsView.h
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/3/31.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WYConnectListModel.h"
//用户标签的view
@interface WYIconsView : UIView

-(CGFloat)addIconsViewWithArr:(WYConnectListModel *)connectListModel;

@end
