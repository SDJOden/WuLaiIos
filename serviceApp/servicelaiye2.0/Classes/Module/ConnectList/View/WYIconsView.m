//
//  WYIconsView.m
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/3/31.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import "WYIconsView.h"

@interface WYIconsView ()

@property (nonatomic, strong) UILabel *scoreLabel;

@property (nonatomic, strong) UIImageView *societyImageView;

@property (nonatomic, strong) UIImageView *vipImageView;

@end

@implementation WYIconsView

- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize {}

-(CGFloat)addIconsViewWithArr:(WYConnectListModel *)connectListModel{
    
    CGFloat x = 0;
    self.scoreLabel.text = @(connectListModel.up_score).stringValue;
    self.scoreLabel.frame = CGRectMake(x, 0, 20, 20);
    
    //VIP判断
    if([connectListModel.vip isKindOfClass:[NSNull class]]) {
        [self.vipImageView removeFromSuperview];
        self.vipImageView = nil;
    }else {
        x += 22;
        self.vipImageView.frame = CGRectMake(x, 0, 20, 20);
    }
    
    if ([connectListModel.qr_user_id isKindOfClass:[NSNull class]] && connectListModel.qr_user_id.integerValue > 0) {
        x += 22;
        self.societyImageView.frame = CGRectMake(x, 0, 20, 20);
    }else {
        [self.societyImageView removeFromSuperview];
        self.societyImageView = nil;
    }
    
    for (NSString *tag in connectListModel.up_tags) {
        x += 22;
        UILabel *label = [[UILabel alloc]init];
        label.frame = CGRectMake(x, 0, 20, 20);
        label.backgroundColor = [UIColor colorWithRed:0.1069 green:0.6172 blue:0.0209 alpha:1.0];
        label.textColor = [UIColor whiteColor];
        label.font = [UIFont systemFontOfSize:10];
        label.textAlignment = NSTextAlignmentCenter;
        [label.layer setMasksToBounds:YES];
        [label.layer setCornerRadius:2.0]; //设置矩圆角半径
        if ([tag isEqualToString:@"potential"]) {
            label.text = @"潜";
        }
        else if ([tag isEqualToString:@"hq"]) {
            label.text = @"优";
        }
        else if ([tag isEqualToString:@"debt"]) {
            label.text = @"欠";
            label.backgroundColor = [UIColor orangeColor];
        }
        else if ([tag isEqualToString:@"new"]) {
            label.text = @"新";
        }
        [self addSubview:label];
    }
    return x + 20;
}

- (UILabel *)scoreLabel {
    
	if(_scoreLabel == nil) {
		_scoreLabel = [[UILabel alloc] init];
        _scoreLabel.backgroundColor = [UIColor lightGrayColor];
        _scoreLabel.textColor = [UIColor whiteColor];
        _scoreLabel.font = [UIFont systemFontOfSize:10];
        _scoreLabel.textAlignment = NSTextAlignmentCenter;
        [_scoreLabel.layer setMasksToBounds:YES];
        [_scoreLabel.layer setCornerRadius:2.0]; //设置矩圆角半径
        [self addSubview:_scoreLabel];
	}
	return _scoreLabel;
}

- (UIImageView *)societyImageView {
    
	if(_societyImageView == nil) {
		_societyImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"society"]];
        _societyImageView.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:_societyImageView];
	}
	return _societyImageView;
}

- (UIImageView *)vipImageView {
    
	if(_vipImageView == nil) {
		_vipImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"vip"]];
        _vipImageView.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:_vipImageView];
	}
	return _vipImageView;
}

@end
