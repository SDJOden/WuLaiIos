//
//  WYNoticeView.m
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/4/11.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import "WYNoticeView.h"

@interface WYNoticeView ()

@property (nonatomic, strong) UILabel *noticeLabel;

@property (nonatomic, strong) UIButton *closeBtn;

@end

@implementation WYNoticeView

- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initialize];
    }
    return self;
}

// 布局
- (void)initialize {
    
    self.backgroundColor = [UIColor colorWithRed:0.7052 green:0.7052 blue:0.7052 alpha:0.5];
    [self.noticeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(self);
        make.left.equalTo(20);
        make.right.equalTo(-30);
    }];
    
    [self.closeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.right.bottom.equalTo(self);
        make.width.equalTo(self.height);
    }];
}

-(void)changeNoticeViewWithNotice:(NSString *)notice{
    
    self.noticeLabel.text = notice;
    self.noticeLabel.userInteractionEnabled = ![self isSingleLine];
}

-(BOOL)isSingleLine{
    
    CGFloat labelHeight = [self.noticeLabel sizeThatFits:CGSizeMake([UIScreen mainScreen].bounds.size.width - 50, MAXFLOAT)].height;
    if ((int)(labelHeight / self.noticeLabel.font.lineHeight) == 1) {
        return YES;
    }
    return NO;
}

#pragma mark --- 懒加载
- (UILabel *)noticeLabel {
    
    if(_noticeLabel == nil) {
        _noticeLabel = [[UILabel alloc] init];
        _noticeLabel.text = @"通知：";
        _noticeLabel.numberOfLines = 0;
        _noticeLabel.userInteractionEnabled = ![self isSingleLine];
        _noticeLabel.font = [UIFont systemFontOfSize:14];
        _noticeLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        _noticeLabel.textColor = [UIColor blackColor];
        UITapGestureRecognizer *labelTapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(noticeLabelTouchUpInside)];
        [_noticeLabel addGestureRecognizer:labelTapGestureRecognizer];
        [self addSubview:_noticeLabel];
    }
    return _noticeLabel;
}

- (UIButton *)closeBtn {
    
	if(_closeBtn == nil) {
        _closeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_closeBtn setImage:[UIImage imageNamed:@"close_hover"] forState:UIControlStateNormal];
        [_closeBtn setImage:[UIImage imageNamed:@"close"] forState:UIControlStateHighlighted];
        [_closeBtn addTarget:self action:@selector(closeNoticeView) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_closeBtn];
	}
	return _closeBtn;
}

-(void)closeNoticeView{
    
    if ([self.noticeViewDelegate respondsToSelector:@selector(closeNoticeView)]) {
        [self.noticeViewDelegate closeNoticeView];
    }
}

-(void)noticeLabelTouchUpInside{
    
    if ([self.noticeViewDelegate respondsToSelector:@selector(noticeInformationWithTitle:)]) {
        [self.noticeViewDelegate noticeInformationWithTitle:self.noticeLabel.text];
    }
}

@end
