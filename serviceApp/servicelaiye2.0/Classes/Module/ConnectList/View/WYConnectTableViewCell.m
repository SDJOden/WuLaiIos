//
//  WYConnectTableViewCell.m
//  servicelaiye
//
//  Created by 汪洋 on 15/12/15.
//  Copyright © 2015年 汪洋. All rights reserved.
//

#import "WYConnectTableViewCell.h"
#import "WYConnectListModel.h"
#import "BBFlashCtntLabel.h"
#import "WYIconsView.h"

@interface WYConnectTableViewCell ()

@property (nonatomic, strong) UILabel *nameLb;

@property (strong, nonatomic) UIImageView *vipUserImageView;

@property (nonatomic, strong) UILabel *userIdLabel;

@property (nonatomic, strong) BBFlashCtntLabel *paoMaLabel;

@property (nonatomic, strong) UILabel *sugDetailLb;

@property (nonatomic, assign) CGFloat iconWidth;

@property (nonatomic, strong) WYIconsView *iconsView;

@end

@implementation WYConnectTableViewCell

-(void)setContentByConnectListModel:(WYConnectListModel *)connectModel{
    
    [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:connectModel.headimgurl] placeholderImage:[UIImage imageNamed:@"portrait_default"]];
    self.nameLb.text = connectModel.nickname;
    self.timeLb.text = connectModel.ts;
    //VIP判断
    if([connectModel.vip isKindOfClass:[NSNull class]]) {
        self.nameLb.textColor = [UIColor blackColor];
    }else {
        self.nameLb.textColor = [UIColor redColor];
    }
    
    self.smallUpRedView.hidden = !connectModel.isHasSmallRightView;
    self.unReadLb.text = @(connectModel.unReadNum).stringValue;
    if (connectModel.unReadNum == 0) {
        self.unReadLb.hidden = YES;
    }else {
        self.unReadLb.hidden = NO;
    }
    
    [self changeContentText:connectModel.contentText];
    
    [self changeSugDomain:connectModel.sugDomainStr.intValue];
    
    self.userIdLabel.text = [NSString stringWithFormat:@"ID:%@", @(connectModel.inner_uid).stringValue];
    
    if (connectModel.up_tags.count > 0) {
        self.iconWidth = connectModel.up_tags.count * 20 + (connectModel.up_tags.count - 1)* 5;
    }
    
    [self.iconsView removeFromSuperview];
    self.iconsView = nil;
    self.iconWidth = [self.iconsView addIconsViewWithArr:connectModel];
    
    [self layout];
}

-(void)layout{
    
    [self.iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(15);
        make.left.equalTo(self.contentView).offset(10);
        make.width.equalTo(50);
        make.height.equalTo(50);
    }];
    
    [self.smallUpRedView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.iconImageView.top).offset(-2);
        make.right.equalTo(self.iconImageView.right).offset(2);
        make.width.equalTo(10);
        make.height.equalTo(10);
    }];
    
    [self.unReadLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.iconImageView.top).offset(-3);
        make.right.equalTo(self.iconImageView.right).offset(3);
        make.width.equalTo(18);
        make.height.equalTo(18);
    }];
    
    [self.nameLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(10);
        make.left.equalTo(self.iconImageView.right).offset(10);
        make.right.equalTo(self.iconsView.right).offset(-10);
        make.height.equalTo(20);
    }];
    
    [self.iconsView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(10);
        make.right.equalTo(self.contentView).offset(-10);
        make.width.equalTo(self.iconWidth);
        make.height.equalTo(20);
    }];
    
    [self.userIdLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.nameLb.bottom);
        make.left.equalTo(self.iconImageView.right).offset(10);
        make.width.equalTo(80);
        make.height.equalTo(20);
    }];
    
    
    [self.timeLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.userIdLabel.bottom);
        make.left.equalTo(self.iconImageView.right).offset(10);
        make.width.equalTo(70);
        make.height.equalTo(20);
    }];
    
    [self.sugDetailLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.timeLb.top);
        make.left.equalTo(self.timeLb.right).offset(5);
        make.width.equalTo(40);
        make.height.equalTo(20);
    }];
    
    [self.paoMaLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.sugDetailLb.top);
        make.left.equalTo(self.sugDetailLb.right).offset(5);
        make.right.equalTo(self.contentView).offset(-5);
        make.height.equalTo(20);
    }];
}

-(void)hiddenCountNum{
    
    self.unReadLb.text = @"0";
    self.unReadLb.hidden = YES;
}

-(void)addCountNum{
    
    self.unReadLb.hidden = NO;
    if (self.unReadLb.hidden) {
        self.unReadLb.text = @"1";
    }
    else {
        self.unReadLb.text = @(self.unReadLb.text.integerValue + 1).stringValue;
    }
}

-(void)changeContentText:(NSString *)content{
    
    self.paoMaLabel.text = content;
}

-(void)changeSugDomain:(int)sugDomain{
    
    NSString *dataPath = [[NSBundle mainBundle]pathForResource:@"serviceStatu" ofType:@"plist"];
//    //读取数据
    NSDictionary *serviceDic = [[NSDictionary alloc]initWithContentsOfFile:dataPath];
    self.sugDetailLb.text = serviceDic[@(sugDomain).stringValue];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    
    [super setSelected:selected animated:animated];
}

-(UIImageView *)iconImageView{
    
    if (!_iconImageView) {
        _iconImageView = [[UIImageView alloc]init];
        _iconImageView.layer.masksToBounds = YES;
        _iconImageView.layer.cornerRadius = 5.0;
        _iconImageView.layer.borderWidth = 0.5;
        _iconImageView.layer.borderColor = [[UIColor lightGrayColor]CGColor];
        [self.contentView addSubview:_iconImageView];
    }
    return _iconImageView;
}

-(UILabel *)nameLb{
    
    if (!_nameLb) {
        _nameLb = [[UILabel alloc]init];
        _nameLb.font = [UIFont boldSystemFontOfSize:15];
        [self.contentView addSubview:_nameLb];
    }
    return _nameLb;
}

-(UIView *)smallUpRedView{
    
    if (!_smallUpRedView) {
        _smallUpRedView = [[UIView alloc]init];
        _smallUpRedView.backgroundColor = [UIColor redColor];
        _smallUpRedView.layer.cornerRadius = 6.0;
        [self.contentView addSubview:self.smallUpRedView];
    }
    return _smallUpRedView;
}

- (UILabel *)unReadLb {
    
    if(_unReadLb == nil) {
        _unReadLb = [[UILabel alloc] init];
        _unReadLb.backgroundColor = [UIColor redColor];
        _unReadLb.layer.masksToBounds = YES;
        _unReadLb.layer.cornerRadius = 9.0;
        _unReadLb.text = @"1";
        _unReadLb.textColor = [UIColor whiteColor];
        _unReadLb.textAlignment = NSTextAlignmentCenter;
        _unReadLb.font = [UIFont boldSystemFontOfSize:12];
        [self.contentView addSubview:_unReadLb];
    }
    return _unReadLb;
}

-(UILabel *)timeLb{
    
    if (!_timeLb) {
        _timeLb = [[UILabel alloc]init];
        _timeLb.font = [UIFont systemFontOfSize:14];
        _timeLb.textColor = [UIColor lightGrayColor];
        [self.contentView addSubview:_timeLb];
    }
    return _timeLb;
}

-(BBFlashCtntLabel *)paoMaLabel{
    
    if(_paoMaLabel == nil) {
        _paoMaLabel = [[BBFlashCtntLabel alloc]init];
        _paoMaLabel.frame = CGRectMake(0, 0, self.contentView.w - 20 - 50 - 10 - 70 - 10 - 10, 20);
        [self.contentView addSubview:_paoMaLabel];
    }
    return _paoMaLabel;
}

- (UILabel *)sugDetailLb {
    
	if(_sugDetailLb == nil) {
		_sugDetailLb = [[UILabel alloc] init];
        _sugDetailLb.textAlignment = NSTextAlignmentCenter;
        _sugDetailLb.font = [UIFont systemFontOfSize:10];
        [_sugDetailLb.layer setMasksToBounds:YES];
        [_sugDetailLb.layer setCornerRadius:4.0]; //设置矩圆角半径
        [_sugDetailLb.layer setBorderWidth:1.0];   //边框宽度
        CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
        CGColorRef colorref = CGColorCreate(colorSpace,(CGFloat[]){ 0, 0, 0, 1 });
        [_sugDetailLb.layer setBorderColor:colorref];
        CGColorSpaceRelease(colorSpace);
        CGColorRelease(colorref);
        [self.contentView addSubview:_sugDetailLb];

	}
	return _sugDetailLb;
}

- (WYIconsView *)iconsView {
    
	if(_iconsView == nil) {
        _iconsView = [[WYIconsView alloc] init];
        [self.contentView addSubview:_iconsView];
	}
	return _iconsView;
}

- (UILabel *)userIdLabel {
    
	if(_userIdLabel == nil) {
		_userIdLabel = [[UILabel alloc] init];
        _userIdLabel.font = [UIFont systemFontOfSize:14];
        _userIdLabel.textColor = [UIColor lightGrayColor];
        _userIdLabel.textAlignment = NSTextAlignmentLeft;
        [self.contentView addSubview:_userIdLabel];
	}
	return _userIdLabel;
}

@end
