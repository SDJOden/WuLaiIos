//
//  WYNoticeView.h
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/4/11.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import <UIKit/UIKit.h>

//展示在用户对话最上面，待办通知等提醒
@protocol WYNoticeViewDelegate <NSObject>

-(void)closeNoticeView;

-(void)noticeInformationWithTitle:(NSString *)title;

@end

@interface WYNoticeView : UIView

-(void)changeNoticeViewWithNotice:(NSString *)notice;

@property (nonatomic, weak) id <WYNoticeViewDelegate> noticeViewDelegate;

@end
