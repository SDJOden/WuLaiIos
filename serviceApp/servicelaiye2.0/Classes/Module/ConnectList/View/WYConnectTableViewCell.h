//
//  WYConnectTableViewCell.h
//  servicelaiye
//
//  Created by 汪洋 on 15/12/15.
//  Copyright © 2015年 汪洋. All rights reserved.
//

#import <UIKit/UIKit.h>

@class WYConnectListModel;

@interface WYConnectTableViewCell : UITableViewCell
//未读
@property (nonatomic, strong) UILabel *unReadLb;
//小红点，最后一条对话来自用户显示小红点
@property (nonatomic, strong) UIView *smallUpRedView;
//用户头像
@property (nonatomic, strong) UIImageView *iconImageView;
//对话时间
@property (nonatomic, strong) UILabel *timeLb;

-(void)setContentByConnectListModel:(WYConnectListModel *)connectModel;
//隐藏计数
-(void)hiddenCountNum;
//增加计数方法
-(void)addCountNum;
//修改用户最近两句话的内容
-(void)changeContentText:(NSString *)content;
//修改意图
-(void)changeSugDomain:(int)sugDomain;

@end
