//
//  WYConnectViewController.m
//  laiye
//
//  Created by 汪洋 on 15/12/5.
//  Copyright © 2015年 汪洋. All rights reserved.
//
#import "WYConnectViewController.h"
#import "View+MASAdditions.h"
#import "WYConnectTableViewCell.h"
#import "SDJChatRoomController.h"
#import "WYConnectTableViewCell.h"
#import "WYConnectListModel.h"
#import "WYUnReadNumModel.h"
#import "WYNoticeViewController.h"
#import "WYNoticeView.h"
#import "MJRefresh.h"
#define AIUPLOAD_URL (CODESTATU?@"http://101.200.177.89:8851/sofa.pbrpc.test.DLServer.Dialogue":@"http://101.200.177.89:8853/sofa.pbrpc.test.DLServer.Dialogue")

@interface WYConnectViewController () <UITableViewDataSource, UITableViewDelegate, WYNoticeViewDelegate>

@property (nonatomic, strong) WYNoticeView *noticeView;

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, assign) int flag;

@property (nonatomic, strong) NSDictionary *statuDict;

@property (nonatomic, strong) NSDictionary *serviceDict;

@property (nonatomic, strong) NSMutableArray *innerUidArray;

@property (nonatomic, strong) NSMutableArray *messageArray;

@property (nonatomic, strong) NSMutableArray *historyMsgModelArray;

@end

typedef enum : NSInteger {
    WYLoginStatuClose = -1,
    WYLoginStatuCanConnect = 0,
    WYLoginStatuCanUse = 5
} WYLoginStatu;

@implementation WYConnectViewController
/**
 *  初始化
 */
-(instancetype)init{
    
    if (self = [super init]) {
        [self registerNotification];
    }
    return self;
}

/**
 *  注册通知
 */
-(void)registerNotification{
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(addCountNum:) name:@"addCountNum" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(addConnectUser:) name:@"addConnectUser" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(deleteConnectUser:) name:@"deleteConnectUser" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(hiddenCellCount:) name:@"hiddenCellCount" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(addNoticeView:) name:@"addNoticeView" object:nil];
}

-(void)home{
    [super home];
    [MobClick event:@"connect_setStatuLeftNaviBarItem_click"];
}

/**
 *  viewDidLoad
 */
- (void)viewDidLoad {
    
    [super viewDidLoad];
    //布局子控件
    [self setSubViews];
    //刷新列表内容
    [self refreshConnectList];
}

/**
 *  布局子控件
 */
-(void)setSubViews{
    //布局tableView
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.bottom.equalTo(self.view);
    }];
}

-(void)clickRefreshList{
    
    [self refreshConnectList];
    
    self.tabBarItem.badgeValue = nil;
}

/**
 *  刷新列表内容，也是refreshBarButtonItem点击方法
 */
-(void)refreshConnectList{
    
    [self refreshNetworking];
    _flag = 0;
}

/**
 *  网络请求列表数据
 */
-(void)refreshNetworking{
    
    __weak typeof(self)__weakSelf = self;
    [[SDJNetworkTools sharedTools]didAccessUsers:[SDJUserAccount loadAccount].token finished:^(NSDictionary<NSString *,id> * _Nullable result, NSError * _Nullable error) {
        [__weakSelf.tableView.mj_header endRefreshing];
        if (error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [__weakSelf.tableView.mj_header endRefreshing];
            });
        }
        if (![result[@"error"] isEqual:[NSNull null]]) {//报错
            NSNumber *code = result[@"error"][@"error_code"];
            if (code.integerValue == 20000) {
                NSString *UserPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).firstObject stringByAppendingPathComponent:@"account.plist"];
                [[NSFileManager defaultManager]removeItemAtPath:UserPath error:nil];
                //其他平台登录，token改变需要先登出融云
                [[RCIMClient sharedRCIMClient] logout];
                // 发送通知让appdelegate去切换根视图为聊天列表界面
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"WYRootViewControllerSwitchNotification" object:@"0"];
                });
            }
        }
        else {
            NSArray *resultArray = result[@"data"][@"user_list"];
            if (resultArray.count) {
                //清空两个数组，一个存用户信息，一个存最后一条时间
                NSString *userIdStr = @"";
                [__weakSelf.innerUidArray removeAllObjects];
                [__weakSelf.dataArray removeAllObjects];
                for (int i = 0; i < resultArray.count; i ++) {
                    if (i == 0) {
                        userIdStr = resultArray[i];
                    }else {
                        userIdStr = [NSString stringWithFormat:@"%@,%@", userIdStr, resultArray[i]];
                    }
                    WYConnectListModel *connectModel = [[WYConnectListModel alloc]init];
                    [__weakSelf.dataArray addObject:connectModel];
                    [__weakSelf.innerUidArray addObject:@(((NSString *)resultArray[i]).integerValue)];
                }
                //请求用户信息
                [[SDJNetworkTools sharedTools]usersInfromationInterface:[SDJUserAccount loadAccount].token user_ids:userIdStr finished:^(NSDictionary<NSString *,id> * _Nullable result, NSError * _Nullable error) {
                    if (error) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [__weakSelf.tableView.mj_header endRefreshing];
                        });
                    }
                    if (![result[@"error"] isEqual:[NSNull null]]) {//报错
                        NSNumber *code = result[@"error"][@"error_code"];
                        if (code.integerValue == 20000) {
                            NSString *UserPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).firstObject stringByAppendingPathComponent:@"account.plist"];
                            [[NSFileManager defaultManager]removeItemAtPath:UserPath error:nil];
                            [[RCIMClient sharedRCIMClient] logout];
                            dispatch_async(dispatch_get_main_queue(), ^{
                                [[NSNotificationCenter defaultCenter] postNotificationName:@"WYRootViewControllerSwitchNotification" object:@"0"];
                            });
                        }
                    }
                    else {
                        NSArray *dataArray = result[@"data"];
                        for (int i = 0; i < dataArray.count; i ++) {
                            WYConnectListModel *connectModel = __weakSelf.dataArray[i];
                            [connectModel refreshWithDict:dataArray[i]];
                            _flag ++;
                        }
                        
                        if (_flag == resultArray.count) {
                            [__weakSelf refreshTableView:resultArray];
                        }
                    }
                }];
            }
            else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [__weakSelf.tableView reloadData];
                    [__weakSelf.tableView.mj_header endRefreshing];
                });
            }
        }
    }];
}

/**
 *  将数据排序
 */
-(void)refreshTableView:(NSArray *)resultArray{
    //排序
    for (int i = 0; i < resultArray.count - 1; i ++) {
        for (int j = i; j < self.dataArray.count; j ++) {
            if (((WYConnectListModel*)self.dataArray[j]).inner_uid == ((NSNumber *)resultArray[i]).intValue) {
                if (j == i) {
                    break;
                }
                WYConnectListModel *conData = self.dataArray[j];
                self.dataArray[j] = self.dataArray[i];
                self.dataArray[i] = conData;
                break;
            }
        }
    }
    _flag = 0;
    
    __weak typeof (self) __weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        [__weakSelf.tableView reloadData];
        [__weakSelf.tableView.mj_header endRefreshing];
    });
}

/**
 *  删除接入用户的网络请求
 */
-(void)deleteData:(NSInteger)row{
    
    WYConnectListModel *connectModel = self.dataArray[row];
    __weak typeof(self)__weakSelf = self;
    [[SDJNetworkTools sharedTools]leaveUserConversation:[SDJUserAccount loadAccount].token user_id:@(connectModel.inner_uid).stringValue finished:^(NSDictionary<NSString *,id> * _Nullable result, NSError * _Nullable error) {
        if (![result[@"error"] isEqual:[NSNull null]]) {//报错
            NSNumber *code = result[@"error"][@"error_code"];
            if (code.integerValue == 20000) {
                NSString *UserPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).firstObject stringByAppendingPathComponent:@"account.plist"];
                [[NSFileManager defaultManager]removeItemAtPath:UserPath error:nil];
                //其他平台登录，token改变需要先登出融云
                [[RCIMClient sharedRCIMClient] logout];
                // 发送通知让appdelegate去切换根视图为聊天列表界面
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"WYRootViewControllerSwitchNotification" object:@"0"];
                });
            }
        }else {
            [__weakSelf aiUpLoadWithUserId:@(connectModel.inner_uid).stringValue];
        }
    }];
}

#pragma mark - 通知
/**
 *  增加红点通知方法
 */
-(void)addCountNum:(NSNotification *)noti{
    
    [self addConnectUser:noti];
    RCMessageModel *msgModel = noti.userInfo[@"messageModel"];
    for (int i = 0; i < self.dataArray.count; i ++) {
        WYConnectListModel *connectModel = self.dataArray[i];
        if (connectModel.inner_uid == ((NSNumber *)noti.userInfo[@"inner_uid"]).integerValue) {
            if (connectModel.unReadNum == 0) {
                self.tabBarItem.badgeValue = @(self.tabBarItem.badgeValue.intValue + 1).stringValue;
                self.unReadNumModel.inner_uid = connectModel.inner_uid;
                self.unReadNumModel.changeMore = YES;
                self.unReadNumModel.totalUnReadNum = self.tabBarItem.badgeValue.integerValue;
            }
            connectModel.unReadNum ++;
            connectModel.contentText = noti.userInfo[@"content"];
            connectModel.ts = [self computeTime:((NSNumber *)noti.userInfo[@"ts"]).integerValue / 1000];
            //增加
            WYConnectTableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
            [cell addCountNum];
            [cell changeContentText:noti.userInfo[@"content"]];
            if (((NSNumber *)noti.userInfo[@"serviceId"]).intValue > 0 && msgModel.messageDirection == MessageDirection_RECEIVE) {
                connectModel.sugDomainStr = noti.userInfo[@"serviceId"];
                [cell changeSugDomain:((NSNumber *)noti.userInfo[@"serviceId"]).intValue];
            }
            cell.timeLb.text = connectModel.ts;
            break;
        }
    }
}

/**
 *  其他助理转接用户增加列表项的通知方法
 */
-(void)addConnectUser:(NSNotification *)noti{
    
    RCMessageModel *model = noti.userInfo[@"messageModel"];
    for (int i = 0; i < self.innerUidArray.count; i ++) {
        NSNumber *innerUid = self.innerUidArray[i];
        if (innerUid.intValue == ((NSNumber *)noti.userInfo[@"inner_uid"]).intValue) {
            //将通知消息加入到模型的messageArray里面
            WYConnectListModel *connectModel = self.dataArray[i];
            [connectModel.messageArray addObject:model];
            return;
        }
    }
    
    [self.innerUidArray insertObject:noti.userInfo[@"inner_uid"] atIndex:0];
    WYConnectListModel *connectModel = [[WYConnectListModel alloc]init];
    [self.dataArray insertObject:connectModel atIndex:0];
    __weak typeof(connectModel)__weakConnectModel = connectModel;
    
    NSString *user_idsStr = [noti.userInfo[@"inner_uid"] isKindOfClass:[NSString class]] ?noti.userInfo[@"inner_uid"] : ((NSNumber *)noti.userInfo[@"inner_uid"]).stringValue;
    __weak typeof(self)__weakSelf = self;
    [[SDJNetworkTools sharedTools]usersInfromationInterface:[SDJUserAccount loadAccount].token user_ids:user_idsStr finished:^(NSDictionary<NSString *,id> * _Nullable result, NSError * _Nullable error) {
        if (![result[@"error"] isEqual:[NSNull null]]) {//异端登陆
            NSNumber *code = result[@"error"][@"error_code"];
            if (code.integerValue == 20000) {
                NSString *UserPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).firstObject stringByAppendingPathComponent:@"account.plist"];
                [[NSFileManager defaultManager]removeItemAtPath:UserPath error:nil];
                [[RCIMClient sharedRCIMClient] logout];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"WYRootViewControllerSwitchNotification" object:@"0"];
                });
            }
        }
        else {
            NSArray *dataArray = result[@"data"];
            [__weakConnectModel updateWithDict:dataArray.lastObject];
            [__weakConnectModel.messageArray addObject:model];
            dispatch_async(dispatch_get_main_queue(), ^{
                [__weakSelf.tableView reloadData];
            });
            __weakSelf.tabBarItem.badgeValue = @(self.tabBarItem.badgeValue.intValue + 1).stringValue;
            __weakSelf.unReadNumModel.inner_uid = connectModel.inner_uid;
            __weakSelf.unReadNumModel.changeMore = YES;
            __weakSelf.unReadNumModel.totalUnReadNum = __weakSelf.tabBarItem.badgeValue.integerValue;
        }
    }];
}

-(NSString *)computeTime:(long)ts{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    dateFormatter.dateFormat = @"HH:mm:ss";//大写HH是24小时制，小写hh是12小时制
    NSDate *tsData = [NSDate dateWithTimeIntervalSince1970:ts];
    return [dateFormatter stringFromDate:tsData];
}

/**
 *  隐藏红点通知方法
 */
-(void)hiddenCellCount:(NSNotification *)noti{
    
    for (int i = 0; i < self.dataArray.count; i ++) {
        WYConnectListModel *connectModel = self.dataArray[i];
        if (connectModel.inner_uid == ((NSNumber *)noti.userInfo[@"innerUid"]).integerValue) {
            if (connectModel.unReadNum != 0) {
                self.tabBarItem.badgeValue = @(self.tabBarItem.badgeValue.intValue - 1).stringValue;
                if (self.tabBarItem.badgeValue.integerValue == 0) {
                    self.tabBarItem.badgeValue = nil;
                }
                self.unReadNumModel.inner_uid = connectModel.inner_uid;
                self.unReadNumModel.changeMore = NO;
                self.unReadNumModel.totalUnReadNum = self.tabBarItem.badgeValue.integerValue;
            }
            connectModel.unReadNum = 0;
            connectModel.ts = noti.userInfo[@"ts"];
            WYConnectTableViewCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
            cell.unReadLb.text = @"0";
            cell.unReadLb.hidden = YES;
            connectModel.isHasSmallRightView = ((NSNumber *)noti.userInfo[@"messageDirection"]).boolValue;
            cell.smallUpRedView.hidden = !connectModel.isHasSmallRightView;
            cell.timeLb.text = noti.userInfo[@"ts"];
            if (((NSNumber *)noti.userInfo[@"service_id"]).intValue > 0) {
                [cell changeSugDomain:((NSNumber *)noti.userInfo[@"service_id"]).intValue];
            }
            break;
        }
    }
}

/**
 *  其他助理抢走用户的通知方法
 */
-(void)deleteConnectUser:(NSNotification *)noti{
    
    NSInteger deleteNum = 0;
    for (int i = 0; i < self.dataArray.count; i ++) {
        WYConnectListModel *connectModel = self.dataArray[i];
        if (connectModel.inner_uid == ((NSNumber *)noti.userInfo[@"inner_uid"]).intValue) {
            deleteNum = i;
            self.unReadNumModel.inner_uid = connectModel.inner_uid;
            if (connectModel.unReadNum > 0) {//之前点开过这个用户，总数不减1
                self.tabBarItem.badgeValue = @(self.tabBarItem.badgeValue.intValue - 1).stringValue;
                if ([self.tabBarItem.badgeValue isEqualToString:@"0"]) {
                    self.tabBarItem.badgeValue = nil;
                }
                self.unReadNumModel.changeMore = NO;
                self.unReadNumModel.totalUnReadNum = self.tabBarItem.badgeValue.integerValue;
            }
            break;
        }
    }
    [self.innerUidArray removeObjectAtIndex:deleteNum];
    [self.dataArray removeObjectAtIndex:deleteNum];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
    });
}

/**
 *  增加通知
 */
-(void)addNoticeView:(NSNotification *)noti{
    
    [self.noticeView changeNoticeViewWithNotice:noti.userInfo[@"content"]];
}

#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (self.dataArray.count > 0) {
        self.navigationItem.title = [NSString stringWithFormat:@"已接入(%ld)", self.dataArray.count];
    }else{
        self.navigationItem.title = @"已接入";
    }
    return self.dataArray.count;
}

-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return UITableViewCellEditingStyleDelete;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *identifer = @"AccessCell";
    WYConnectTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifer forIndexPath:indexPath];
    WYConnectListModel *connectModel = self.dataArray[indexPath.row];
    [cell setContentByConnectListModel:connectModel];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [MobClick event:@"connect_userTabelViewCell_click"];
    NSString *unRead = self.tabBarItem.badgeValue;
    WYConnectTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (cell.unReadLb.hidden == NO) {
        cell.unReadLb.hidden = YES;
        unRead = @(self.tabBarItem.badgeValue.integerValue - 1).stringValue;
        if (unRead.integerValue == 0) {
            unRead = nil;
        }
    }
    
    WYConnectListModel *connectListModel = self.dataArray[indexPath.row];
    //跳转到对话界面
    UIImageView *imageView = [[UIImageView alloc]init];
    
    [imageView sd_setImageWithURL:[NSURL URLWithString:((WYConnectListModel *)self.dataArray[indexPath.row]).headimgurl] placeholderImage:[UIImage imageNamed:@"portrait_default"] options:SDWebImageRetryFailed completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        SDJChatRoomController *chatVC = [[SDJChatRoomController alloc]initWithConversationType:ConversationType_PRIVATE targetId:@"rcg0" connectListModel:connectListModel];
        chatVC.unReadNumModel = self.unReadNumModel;
        chatVC.headImage = imageView.image;
        chatVC.unReadNumStr = unRead;
        chatVC.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:chatVC animated:YES];
    }];
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        //网络请求删除数据
        if (((WYConnectTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath]).unReadLb.hidden  == NO) {
            [self reduceNumOfTabBar];
        }
        [self deleteData:indexPath.row];
        [self.innerUidArray removeObjectAtIndex:indexPath.row];
        [self.dataArray removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
    }
}
/**
 *  上传给ai用户状态
 */
-(void)aiUpLoadWithUserId:(NSString *)userid{
    
    AFHTTPSessionManager *mgr = [AFHTTPSessionManager manager];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    NSString *paramsStr = [NSString stringWithFormat:@"{\"userid\":\"%@\",\"post\":\"EVENT_NOTICE:CLOSE_SESSION:12\"}", userid];
    params[@"request"] = paramsStr;
    //上传状态
    [mgr GET:AIUPLOAD_URL parameters:params progress:^(NSProgress * _Nonnull downloadProgress) {}
     success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {} failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {}];
}

-(void)reduceNumOfTabBar{
    
    if (self.tabBarItem.badgeValue.intValue > 1) {
        self.tabBarItem.badgeValue = @(self.tabBarItem.badgeValue.intValue - 1).stringValue;
    }
    else {
        self.tabBarItem.badgeValue = nil;
    }
    self.unReadNumModel.totalUnReadNum = self.tabBarItem.badgeValue.integerValue;
}

//修改删除名称
-(NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return @"结束对话";
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 80;
}

//分割线顶头
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]){
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    cell.preservesSuperviewLayoutMargins = NO;
    
    if([indexPath row] == ((NSIndexPath*)[[tableView indexPathsForVisibleRows] lastObject]).row){
        dispatch_async(dispatch_get_main_queue(),^{
            int count = 0;
            for (int i = 0; i < self.dataArray.count; i ++) {
                WYConnectListModel * connectModel = self.dataArray[i];
                if (connectModel.unReadNum > 0) {
                    count ++;
                }
            }
            if (count > 0) {
                self.tabBarItem.badgeValue = @(count).stringValue;
            }
            else {
                self.tabBarItem.badgeValue = nil;
            }
            self.unReadNumModel.totalUnReadNum = self.tabBarItem.badgeValue.integerValue;
        });
    }
}

-(void)showNoticeView{}

- (void)dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark --- WYNoticeViewDelegate
-(void)closeNoticeView{
    
    [MobClick event:@"connect_myTabBarItem_click"];
    [self.noticeView removeFromSuperview];
    self.noticeView = nil;
}

-(void)noticeInformationWithTitle:(NSString *)title{
    [self.noticeView removeFromSuperview];
    self.noticeView = nil;
    WYNoticeViewController *noticeVC = [WYNoticeViewController createNoticeVCWithText:title];
    noticeVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:noticeVC animated:YES];
}

#pragma mark --- 懒加载
- (WYNoticeView *)noticeView {
    if(_noticeView == nil) {
        _noticeView = [[WYNoticeView alloc] init];
        _noticeView.noticeViewDelegate = self;
        [self.view addSubview:_noticeView];
        [_noticeView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.right.equalTo(self.view);
            make.height.equalTo(30);
        }];
    }
    return _noticeView;
}

- (UITableView *)tableView {
    
    if(_tableView == nil) {
        _tableView = [[UITableView alloc]init];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        _tableView.mj_header = [MJRefreshStateHeader headerWithRefreshingTarget:self refreshingAction:@selector(clickRefreshList)];
        [_tableView registerClass:[WYConnectTableViewCell class] forCellReuseIdentifier:@"AccessCell"];
        _tableView.tableFooterView = [UIView new];
        [self.view addSubview:_tableView];
    }
    return _tableView;
}

-(NSMutableArray *)dataArray{
    
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}

- (NSMutableArray *)innerUidArray {
    
	if(_innerUidArray == nil) {
		_innerUidArray = [[NSMutableArray alloc] init];
	}
	return _innerUidArray;
}

- (WYUnReadNumModel *)unReadNumModel {
    
	if(_unReadNumModel == nil) {
		_unReadNumModel = [WYUnReadNumModel shareUnReadNumModel];
        _unReadNumModel.totalUnReadNum = 0;
	}
	return _unReadNumModel;
}

- (NSMutableArray *)messageArray {
    
	if(_messageArray == nil) {
		_messageArray = [[NSMutableArray alloc] init];
	}
	return _messageArray;
}


@end
