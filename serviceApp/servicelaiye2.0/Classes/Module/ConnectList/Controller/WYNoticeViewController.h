//
//  WYNoticeViewController.h
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/4/11.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import <UIKit/UIKit.h>
//助理代办通知等点击最上端的label弹出的控制器
@interface WYNoticeViewController : UIViewController

+(instancetype)createNoticeVCWithText:(NSString *)text;

-(instancetype)initNoticeVCWithText:(NSString *)text;

@end
