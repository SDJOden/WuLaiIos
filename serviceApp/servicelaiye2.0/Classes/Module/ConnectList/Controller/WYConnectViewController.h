//
//  WYConnectViewController.h
//  laiye
//
//  Created by 汪洋 on 15/12/5.
//  Copyright © 2015年 汪洋. All rights reserved.
//

#import <UIKit/UIKit.h>

@class WYUnReadNumModel;

//已接入
@interface WYConnectViewController : WYBaseViewController/*UIViewController*/

@property (nonatomic, strong) NSMutableArray *dataArray;

//未读小红点的数据模型
@property (nonatomic, strong) WYUnReadNumModel *unReadNumModel;

@end
