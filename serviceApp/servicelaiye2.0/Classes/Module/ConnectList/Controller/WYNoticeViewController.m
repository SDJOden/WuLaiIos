//
//  WYNoticeViewController.m
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/4/11.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import "WYNoticeViewController.h"

@interface WYNoticeViewController ()

@property (nonatomic, strong) UIView *statusBarBackgroundView;

@property (nonatomic, strong) UILabel *noticeLabel;

@property (nonatomic, strong) UIButton *certainButton;

@end

@implementation WYNoticeViewController

+(instancetype)createNoticeVCWithText:(NSString *)text{
    
    return [[self alloc]initNoticeVCWithText:text];
}

-(instancetype)initNoticeVCWithText:(NSString *)text{
    
    if (self = [super init]) {
        [self setSubViews];
        self.noticeLabel.text = text;
        self.view.backgroundColor = [UIColor whiteColor];
    }
    return self;
}

-(instancetype)init{
    
    if (self = [super init]) {
        self.view.backgroundColor = [UIColor whiteColor];
    }
    return self;
}

-(void)viewDidLoad{
    
    [super viewDidLoad];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

-(void)setSubViews{
    
    [self.statusBarBackgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self.view);
        make.height.equalTo(20);
    }];
    
    [self.noticeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.statusBarBackgroundView.bottom).offset(60);
        make.left.equalTo(self.view).offset(20);
        make.right.equalTo(self.view).offset(-20);
    }];
    
    [self.certainButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.view).offset(-10);
        make.left.equalTo(10);
        make.right.equalTo(-10);
        make.height.equalTo(44);
    }];
}

- (UIView *)statusBarBackgroundView {
    
	if(_statusBarBackgroundView == nil) {
		_statusBarBackgroundView = [[UIView alloc] init];
        _statusBarBackgroundView.backgroundColor = NAVIGATIONBAR_BACKGROUDCOLOR;
        [self.view addSubview:_statusBarBackgroundView];
	}
	return _statusBarBackgroundView;
}

- (UILabel *)noticeLabel {
    
	if(_noticeLabel == nil) {
		_noticeLabel = [[UILabel alloc] init];
        _noticeLabel.font = [UIFont systemFontOfSize:12];
        _noticeLabel.numberOfLines = 0;
        CGRect rect = [_noticeLabel textRectForBounds:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width - 40, MAXFLOAT) limitedToNumberOfLines:0];
        _noticeLabel.frame = CGRectMake(0, 0, rect.size.width, rect.size.height);
        [self.view addSubview:_noticeLabel];
	}
	return _noticeLabel;
}

- (UIButton *)certainButton {
    
	if(_certainButton == nil) {
		_certainButton = [UIButton buttonWithType:UIButtonTypeSystem];
        _certainButton.backgroundColor = [UIColor lightGrayColor];
        [_certainButton setTitle:@"知道了" forState:UIControlStateNormal];
        [_certainButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _certainButton.titleLabel.font = [UIFont systemFontOfSize:14];
        [_certainButton addTarget:self action:@selector(popToTabBarViewController) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:_certainButton];
	}
	return _certainButton;
}

- (void)popToTabBarViewController{
    [self.navigationController popViewControllerAnimated:YES];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

@end
