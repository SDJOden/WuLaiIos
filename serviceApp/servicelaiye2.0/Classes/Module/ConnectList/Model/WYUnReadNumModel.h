//
//  WYUnReadNumModel.h
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/2/2.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WYUnReadNumModel : NSObject

@property (nonatomic, assign) NSInteger totalUnReadNum;

@property (nonatomic, assign) NSInteger inner_uid;

@property (nonatomic, assign) BOOL changeMore;

+(instancetype)shareUnReadNumModel;

@end
