//
//  WYConnectListModel.m
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/1/19.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import "WYConnectListModel.h"
#import "WYWaitingUserData.h"
#import "WYWaitingUsersRegion.h"
#import "WYWaitingUsersWechat.h"
#import "WYHistoryModel.h"

@implementation WYConnectListModel

-(void)refreshWithDict:(NSDictionary *)dict{
    
    _isHasSmallRightView = NO;
    _headimgurl = dict[@"wechat"][@"headimgurl"];
    if(((NSString *)dict[@"wechat"][@"nickname"]).length != 0){//存在微信名显示微信名
        _nickname = dict[@"wechat"][@"nickname"];
    }
    else{//不存在微信名
        if (![dict[@"nickname"] isKindOfClass:[NSNull class]]) {//显示昵称
            _nickname = dict[@"nickname"];
        }
        else {
            if (![dict[@"user_id"] isKindOfClass:[NSNull class]]) {
                _nickname = [NSString stringWithFormat:@"#%@", dict[@"user_id"]];//不存在昵称，显示id
            }
        }
    }
    
    if (![dict[@"region"] isKindOfClass:[NSNull class]]) {
        _province = dict[@"region"][@"province"];
    }
    _inner_uid = ((NSNumber *)dict[@"inner_uid"]).intValue;
    _db_insert_time = ((NSNumber *)dict[@"db_insert_time"]).integerValue;
    _vip = dict[@"vip"];
    _contentText = @"";
    _rc_group_id = dict[@"rc_group_id"];
    _wechat_openid = dict[@"wechat_openid"];
    _up_score = ((NSNumber *)dict[@"up_score"]).integerValue;
    _up_tags = [NSArray arrayWithArray:dict[@"up_tags"]];
    _sugDomainStr = @"";
    _phone = ((NSNumber *)dict[@"phone"]).stringValue;
    if (![dict[@"wechat"][@"qr_user_id"] isKindOfClass:[NSNull class]]) {
        _qr_user_id = dict[@"wechat"][@"qr_user_id"];
    }
}

-(void)updateWithDict:(NSDictionary *)dict{
    
    [self refreshWithDict:dict];
    _unReadNum = 1;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm:ss"];
    _ts = [dateFormatter stringFromDate:[NSDate date]];
}

-(void)makeModelWithHistoryModel:(WYHistoryModel *)historyModel{
    
    _isHasSmallRightView = NO;
    _headimgurl = historyModel.wechat[@"headimgurl"];
    
    if(![historyModel.wechat isKindOfClass:[NSNull class]] && [historyModel.wechat[@"nickname"] length] != 0){//存在微信名显示微信名
        _nickname = historyModel.wechat[@"nickname"];
    }
    else{//不存在微信名
        if (![historyModel.nickname isKindOfClass:[NSNull class]]) {//显示昵称
            _nickname = historyModel.nickname;
        }
        else {
            _nickname = [NSString stringWithFormat:@"#%d", historyModel.user_id];//不存在昵称，显示id
        }
    }
    
    if (![historyModel.region isKindOfClass:[NSNull class]] && [historyModel.region[@"province"] isKindOfClass:[NSNull class]]) {
        _province = historyModel.region[@"province"];
    }
    _inner_uid = (int)historyModel.inner_uid;
    _db_insert_time = [historyModel.db_insert_time integerValue];
    _vip = historyModel.vip;
    _contentText = @"";
    _rc_group_id = historyModel.rc_group_id;
    _wechat_openid = historyModel.wechat_openid;
    _up_score = historyModel.up_score;
    _up_tags = historyModel.up_tags;
    _sugDomainStr = @"";
    _phone = historyModel.phone;
    if (![historyModel.wechat[@"qr_user_id@"] isKindOfClass:[NSNull class]]) {
        _qr_user_id = historyModel.wechat[@"qr_user_id"];
    }
}
    
-(void)makeModelWithWaitModel:(WYWaitingUserData *)userData{
    
    _isHasSmallRightView = NO;
    _headimgurl = userData.wechat.headimgurl;
    
    if(userData.wechat.nickname.length != 0){//存在微信名显示微信名
        _nickname = userData.wechat.nickname;
    }
    else{//不存在微信名
        if (![userData.nickname isKindOfClass:[NSNull class]]) {//显示昵称
            _nickname = userData.nickname;
        }
        else {
            if (![userData.user_id isKindOfClass:[NSNull class]]) {
                _nickname = [NSString stringWithFormat:@"#%@", userData.user_id];//不存在昵称，显示id
            }
        }
    }
    
    if ([userData.region.province isKindOfClass:[NSNull class]]) {
        _province = userData.region.province;
    }
    _inner_uid = userData.inner_uid.intValue;
    _db_insert_time = [userData.db_insert_time integerValue];
    _vip = userData.vip;
    _contentText = @"";
    _rc_group_id = userData.rc_group_id;
    _wechat_openid = userData.wechat_openid;
    _up_score = userData.up_score;
    _up_tags = userData.up_tags;
    _sugDomainStr = @"";
    _phone = userData.phone.stringValue;
    if (![userData.wechat.qr_user_id isKindOfClass:[NSNull class]]) {
        _qr_user_id = userData.wechat.qr_user_id;
    }
}

-(instancetype)init{
    
    if (self = [super init]) {
        _messageArray = [[NSMutableArray alloc]init];
        _historyArray = [[NSMutableArray alloc]init];
    }
    return self;
}

@end
