//
//  WYConnectInformationModel.m
//  servicelaiye
//
//  Created by 汪洋 on 15/12/16.
//  Copyright © 2015年 汪洋. All rights reserved.
//

#import "WYConnectInformationModel.h"


@implementation WYConnectInformationModel

// 属于逻辑判断的东西不要扔在类里面做.
-(void)modelByRCMessage:(RCMessage *)rcMessage{
    
    if ([rcMessage.content isKindOfClass:[RCTextMessage class]]) {
        RCTextMessage *textMsg = (RCTextMessage *)rcMessage.content;
        NSDictionary *msgJSONDic = [WYTool jsonStringToDictionary:textMsg.extra];
        if (((NSNumber *)msgJSONDic[@"direction"]).integerValue) {
            return;
        }
        if (((NSNumber *)msgJSONDic[@"msg_type"]).integerValue == 1) {
            self.src_uid = msgJSONDic[@"src_uid"];
            self.user_src = ((NSNumber *)msgJSONDic[@"user_src"]).intValue;
        }
    }
    else if ([rcMessage.content isKindOfClass:[RCImageMessage class]]) {
        RCImageMessage *imageMsg = (RCImageMessage *)rcMessage.content;
        NSDictionary *msgJSONDic = [WYTool jsonStringToDictionary:imageMsg.extra];
        if (((NSNumber *)msgJSONDic[@"direction"]).integerValue) {
            return;
        }
        if (((NSNumber *)msgJSONDic[@"msg_type"]).integerValue == 2) {
            self.src_uid = msgJSONDic[@"src_uid"];
            self.user_src = ((NSNumber *)msgJSONDic[@"user_src"]).intValue;
        }
    }
    else if ([rcMessage.content isKindOfClass:[RCVoiceMessage class]]) {
        RCImageMessage *imageMsg = (RCImageMessage *)rcMessage.content;
        NSDictionary *msgJSONDic = [WYTool jsonStringToDictionary:imageMsg.extra];
        if (((NSNumber *)msgJSONDic[@"direction"]).integerValue) {
            return;
        }
        if (((NSNumber *)msgJSONDic[@"msg_type"]).integerValue == 5) {
            self.src_uid = msgJSONDic[@"src_uid"];
            self.user_src = ((NSNumber *)msgJSONDic[@"user_src"]).intValue;
        }
    }
    else {}
}

-(id)copyWithZone:(NSZone *)zone{
    
    WYConnectInformationModel *copyModel = [[WYConnectInformationModel alloc]init];
    copyModel.isHasSmallRightView = self.isHasSmallRightView;
    copyModel.unReadNum = self.unReadNum;
    copyModel.msg_ts = self.msg_ts;
    copyModel.inner_uid = self.inner_uid;
    copyModel.src_uid = self.src_uid;
    copyModel.user_gid = self.user_gid;
    copyModel.user_src = self.user_src;
    copyModel.direction = self.direction;
    copyModel.username = self.username;
    copyModel.realname = self.realname;
    return copyModel;
}

@end
