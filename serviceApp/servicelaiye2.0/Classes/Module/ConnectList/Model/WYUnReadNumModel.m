//
//  WYUnReadNumModel.m
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/2/2.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import "WYUnReadNumModel.h"

@implementation WYUnReadNumModel

//单例方法，每个人之创建一个数据模型来记录小红点数量
+(instancetype)shareUnReadNumModel{
    
    static WYUnReadNumModel *sharedUnReadNumModel = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if (!sharedUnReadNumModel) {
            sharedUnReadNumModel = [[self alloc]init];
        }
    });
    return sharedUnReadNumModel;
}

@end
