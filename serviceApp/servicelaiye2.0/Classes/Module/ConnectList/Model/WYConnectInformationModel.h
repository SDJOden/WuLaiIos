//
//  WYConnectInformationModel.h
//  servicelaiye
//
//  Created by 汪洋 on 15/12/16.
//  Copyright © 2015年 汪洋. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RCMessageModel.h"


@interface WYConnectInformationModel : NSObject <NSCopying>

@property (nonatomic, assign) BOOL isHasSmallRightView;

@property (nonatomic, assign) NSInteger unReadNum;

@property (nonatomic, copy) NSString *msg_ts;

@property (nonatomic, assign) int inner_uid;

@property (nonatomic, copy) NSString *src_uid;

@property (nonatomic, copy) NSString *user_gid;

@property (nonatomic, assign) int user_src;

@property (nonatomic, assign) int direction;

@property (nonatomic, copy) NSString *username;

@property (nonatomic, copy) NSString *realname;

-(void)modelByRCMessage:(RCMessage *)rcMessage;

-(id)copyWithZone:(NSZone *)zone;

@end
