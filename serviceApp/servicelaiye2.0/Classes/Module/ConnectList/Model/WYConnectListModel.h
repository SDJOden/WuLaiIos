//
//  WYConnectListModel.h
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/1/19.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WYConnectInformationModel.h"

@interface WYConnectListModel : NSObject /*<NSCopying>*/
/**
 *  是否显示小红点
 */
@property (nonatomic, assign) BOOL isHasSmallRightView;
/**
 *  未读数
 */
@property (nonatomic, assign) NSInteger unReadNum;
/**
 *  最后一次查看对话的时间
 */
@property (nonatomic, copy) NSString *ts;
/**
 *  用户头像url
 */
@property (nonatomic, copy) NSString *headimgurl;
/**
 *  用户昵称
 */
@property (nonatomic, copy) NSString *nickname;

@property (nonatomic, copy) NSString *province;
/**
 *  用户id
 */
@property (nonatomic, assign) int inner_uid;
/**
 *  关注时间
 */
@property (nonatomic, assign) NSInteger db_insert_time;
/**
 *  vip标签
 */
@property (nonatomic, copy) NSDictionary *vip;
/**
 *  消息内容
 */
@property (nonatomic, copy) NSString *contentText;

@property (nonatomic, copy) NSString *rc_group_id;

@property (nonatomic, copy) NSString *wechat_openid;

@property (nonatomic, assign) NSInteger up_score;

@property (nonatomic, strong) NSArray *up_tags;

@property (nonatomic, copy) NSString *qr_user_id;

@property (nonatomic, copy) NSString *sugDomainStr;

@property (nonatomic, copy) NSString *phone;

@property (nonatomic, strong) NSMutableArray *historyArray;

@property (nonatomic, assign) NSInteger page;

@property (nonatomic, strong) NSMutableArray *messageArray;

@property (nonatomic, strong) WYConnectInformationModel *informationModel;

-(void)refreshWithDict:(NSDictionary *)dict;

-(void)updateWithDict:(NSDictionary *)dict;

-(void)makeModelWithHistoryModel:(WYHistoryModel *)historyModel;

-(void)makeModelWithWaitModel:(WYWaitingUserData *)userData;

@end
