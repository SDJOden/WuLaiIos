//
//  WYTopMessageView.h
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/3/22.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WYHistoryModel.h"


@protocol WYTopMessageViewDelegate <NSObject>

@optional

-(void)hiddenTopMsgView;

@end

@interface WYTopMessageView : UIView

@property (nonatomic, strong) WYHistoryModel *historyModel;

@property (nonatomic, strong) UIButton *dateLbBtn;

@property (nonatomic,weak) id <WYTopMessageViewDelegate> topMessageDelegate;

-(instancetype)initWithModel:(WYHistoryModel *)historyModel;


@end
