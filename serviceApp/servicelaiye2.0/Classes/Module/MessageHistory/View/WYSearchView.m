//
//  WYSearchView.m
//  servicelaiye2.0
//
//  Created by 汪洋 on 15/12/31.
//  Copyright © 2015年 shengdong. All rights reserved.
//

#import "WYSearchView.h"

typedef enum : NSUInteger {
    WY_SEARCH_USER_PHONE = 0,
    WY_SEARCH_USER_ID = 1,
    WY_SEARCH_USER_NICKNAME = 2,
    WY_SEARCH_USER_MESSAGE_CONTENT = 3
} WYSearchStatu;

@interface WYSearchView ()<UITextFieldDelegate>

//@property (nonatomic, strong) UILabel *searchLb;

@property (nonatomic, strong) UIButton *searchStatuBtn;

@property (nonatomic, strong) UITextField * searchInputTextField;

@property (nonatomic, strong) UIView *textBackgroundView;

@property (nonatomic, strong) UIButton *searchBtn;

@property (nonatomic, strong) UIView *lineView;

@end

@implementation WYSearchView

-(void)layoutSubviews{
    [super layoutSubviews];
    [self.textBackgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self);
        make.top.left.equalTo(self).offset(8);
        make.right.equalTo(self.searchInputTextField.right);
    }];
    
    [self.searchStatuBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self);
        make.height.equalTo(25);
        make.left.equalTo(self).offset(10);
        make.width.equalTo(60);
    }];
    
    [self.searchInputTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self);
        make.height.equalTo(28);
        make.left.equalTo(self.searchStatuBtn.right);
        make.right.equalTo(self.searchBtn.left).offset(-5);
    }];
    
    [self.searchBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self);
        make.top.equalTo(self).offset(8);
        make.right.equalTo(self).offset(-10);
        make.width.equalTo(80);
    }];
    
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.bottom).offset(-1);
        make.height.equalTo(0.5);
        make.left.equalTo(self);
        make.right.equalTo(self);
    }];
}

- (UIButton *)searchStatuBtn {
	if(_searchStatuBtn == nil) {
		_searchStatuBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_searchStatuBtn setTitle:@"手机号" forState:UIControlStateNormal];
        [_searchStatuBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        _searchStatuBtn.titleLabel.textAlignment = NSTextAlignmentLeft;
        _searchStatuBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0);
        _searchStatuBtn.titleLabel.font = [UIFont systemFontOfSize:12];
        _searchStatuBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [_searchStatuBtn addTarget:self action:@selector(statuChange) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_searchStatuBtn];
	}
	return _searchStatuBtn;
}

- (UITextField *)searchInputTextField {
	if(_searchInputTextField == nil) {
		_searchInputTextField = [[UITextField alloc] init];
        _searchInputTextField.placeholder = @"请输入搜索内容";
        _searchInputTextField.delegate = self;
//        _searchInputTextField.borderStyle = UITextBorderStyleBezel;
        _searchInputTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
        _searchInputTextField.font = [UIFont systemFontOfSize:12];
        [self addSubview:_searchInputTextField];
	}
	return _searchInputTextField;
}

- (UIButton *)searchBtn {
	if(_searchBtn == nil) {
		_searchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _searchBtn.backgroundColor = [UIColor colorWithRed:0.8186 green:0.8186 blue:0.8186 alpha:1.0];
        [_searchBtn setTitle:@"搜索" forState:UIControlStateNormal];
        [_searchBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        _searchBtn.titleLabel.font = [UIFont systemFontOfSize:12];
        [_searchBtn addTarget:self action:@selector(search) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_searchBtn];
	}
	return _searchBtn;
}

- (UIView *)lineView {
    if(_lineView == nil) {
        _lineView = [[UIView alloc] init];
        _lineView.backgroundColor = [UIColor colorWithWhite:0.855 alpha:1.000];
        [self addSubview:_lineView];
    }
    return _lineView;
}

-(void)changeBtnText:(NSInteger)number{
    switch (number) {
        case 0:
            [self.searchStatuBtn setTitle:@"手机号" forState:UIControlStateNormal];
            break;
        case 1:
            [self.searchStatuBtn setTitle:@"用户 ID" forState:UIControlStateNormal];
            break;
        case 2:
            [self.searchStatuBtn setTitle:@"昵称" forState:UIControlStateNormal];
            break;
        case 3:
            [self.searchStatuBtn setTitle:@"消息内容" forState:UIControlStateNormal];
            break;
        default:
            break;
    }
}

-(NSString *)getText{
    return self.searchInputTextField.text;
}

-(void)reset{
    [self.searchStatuBtn setTitle:@"手机号" forState:UIControlStateNormal];
    self.searchInputTextField.text = @"";
}

-(void)search{//搜索
    if ([self.searchViewDelegate respondsToSelector:@selector(searchUserInformation)]) {
        [self.searchViewDelegate searchUserInformation];
    }
}

-(void)statuChange{
    if ([self.searchViewDelegate respondsToSelector:@selector(statuDidClicked)]) {
        [self.searchViewDelegate statuDidClicked];
    }
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self endEditing:YES];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    if ([self.searchViewDelegate respondsToSelector:@selector(textFieldDidClicked)]) {
        [self.searchViewDelegate textFieldDidClicked];
    }
}

-(void)resetTextField{
    self.searchInputTextField.text = @"";
}

- (UIView *)textBackgroundView {
	if(_textBackgroundView == nil) {
		_textBackgroundView = [[UIView alloc] init];
        _textBackgroundView.backgroundColor = [UIColor colorWithRed:0.7939 green:0.7939 blue:0.7939 alpha:1.0];
        [self addSubview:_textBackgroundView];
	}
	return _textBackgroundView;
}

@end
