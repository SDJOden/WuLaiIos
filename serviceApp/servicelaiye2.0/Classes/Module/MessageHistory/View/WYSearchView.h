//
//  WYSearchView.h
//  servicelaiye2.0
//
//  Created by 汪洋 on 15/12/31.
//  Copyright © 2015年 shengdong. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol WYSearchViewDelegate <NSObject>

@optional

-(void)statuDidClicked;

-(void)searchUserInformation;

-(void)textFieldDidClicked;

@end

@interface WYSearchView : UIView

@property (nonatomic,weak) id <WYSearchViewDelegate> searchViewDelegate;

-(void)changeBtnText:(NSInteger)number;

-(NSString *)getText;

-(void)reset;

-(void)resetTextField;

@end
