//
//  WYWaitingUsersWechat.m
//  laiye
//
//  Created by 汪洋 on 15/12/3.
//  Copyright © 2015年 汪洋. All rights reserved.
//

#import "WYWaitingUsersWechat.h"

@implementation WYWaitingUsersWechat

- (instancetype)initWithDict:(NSDictionary *)dict{
    
    if (self = [super init]) {
        _headimgurl = dict[@"headimgurl"];
        _nickname = dict[@"nickname"];
        _user_id = dict[@"user_id"];
        _qr_user_id = dict[@"qr_user_id"];
    }
    return self;
}

+ (instancetype)modelWithDict:(NSDictionary *)dict{
    
    return [[self alloc]initWithDict:dict];
}

@end
