//
//  WYWaitingUsersRegion.m
//  laiye
//
//  Created by 汪洋 on 15/12/3.
//  Copyright © 2015年 汪洋. All rights reserved.
//

#import "WYWaitingUsersRegion.h"

@implementation WYWaitingUsersRegion

- (instancetype)initWithDict:(NSDictionary *)dict{
    
    if (self = [super init]) {
        if (![dict isKindOfClass:[NSNull class]]) {
            _city = dict[@"city"];
            _province = dict[@"province"];
            _source = dict[@"source"];
            _ts = dict[@"ts"];
        }
    }
    return self;
}

+ (instancetype)modelWithDict:(NSDictionary *)dict{
    
    return [[self alloc]initWithDict:dict];
}

@end
