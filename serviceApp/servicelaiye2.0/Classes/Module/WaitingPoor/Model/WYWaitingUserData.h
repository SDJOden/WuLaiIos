//
//  WYWaitingUserData.h
//  laiye
//
//  Created by 汪洋 on 15/12/3.
//  Copyright © 2015年 汪洋. All rights reserved.
//

#import <Foundation/Foundation.h>

@class WYWaitingUsersRegion;
@class WYWaitingUsersWechat;

@interface WYWaitingUserData : NSObject

@property (nonatomic, copy) NSString *avatar_url;
@property (nonatomic, copy) NSString *wechat_openid;
@property (nonatomic, strong) NSNumber *user_id;
@property (nonatomic, strong) NSNumber *level;
@property (nonatomic, strong) NSDictionary *msg_domain;
@property (nonatomic, copy) NSString *nickname;
@property (nonatomic, strong) NSNumber *db_insert_time;
@property (nonatomic, strong) WYWaitingUsersRegion *region;
@property (nonatomic, strong) NSNumber *order_count;
@property (nonatomic, strong) NSNumber *inner_uid;
@property (nonatomic, strong) NSNumber *phone;
@property (nonatomic, strong) WYWaitingUsersWechat *wechat;
@property (nonatomic, copy) NSArray *user_msg;
@property (nonatomic, strong) NSString *rc_group_id;
@property (nonatomic, strong) NSNumber *user_ts;
@property (nonatomic, copy) NSString *rcu_id;
@property (nonatomic, strong) NSDictionary *vip;
@property (nonatomic, assign) NSInteger up_score;
@property (nonatomic, strong) NSArray *up_tags;

- (instancetype)initWithDict:(NSDictionary *)dict;

+ (instancetype)modelWithDict:(NSDictionary *)dict;

@end
