//
//  WYWaitingUsersRegion.h
//  laiye
//
//  Created by 汪洋 on 15/12/3.
//  Copyright © 2015年 汪洋. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WYWaitingUsersRegion : NSObject

@property (nonatomic, copy) NSString *province;
@property (nonatomic, copy) NSString *city;
@property (nonatomic, copy) NSString *ts;
@property (nonatomic, copy) NSString *source;

- (instancetype)initWithDict:(NSDictionary *)dict;

+ (instancetype)modelWithDict:(NSDictionary *)dict;

@end
