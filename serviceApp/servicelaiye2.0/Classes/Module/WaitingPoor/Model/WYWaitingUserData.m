//
//  WYWaitingUserData.m
//  laiye
//
//  Created by 汪洋 on 15/12/3.
//  Copyright © 2015年 汪洋. All rights reserved.
//

#import "WYWaitingUserData.h"
#import "WYWaitingUsersRegion.h"
#import "WYWaitingUsersWechat.h"

@implementation WYWaitingUserData

- (instancetype)initWithDict:(NSDictionary *)dict{
    
    if (self = [super init]) {
        _avatar_url = dict[@"avatar_url"];
        _db_insert_time = dict[@"db_insert_time"];
        _inner_uid = dict[@"inner_uid"];
        _level = dict[@"level"];
        _msg_domain = dict[@"msg_domain"];
        _nickname = dict[@"nickname"];
        _order_count = dict[@"order_count"];
        _phone = dict[@"phone"];
        _rc_group_id = dict[@"rc_group_id"];
        _rcu_id = dict[@"rcu_id"];
        _region = [WYWaitingUsersRegion modelWithDict:dict[@"region"]];
        _user_id = dict[@"user_id"];
        _user_msg = dict[@"user_msg"];
        _user_ts = dict[@"user_ts"];
        _wechat = [WYWaitingUsersWechat modelWithDict:dict[@"wechat"]];
        _wechat_openid = dict[@"wechat_openid"];
        _vip = dict[@"vip"];
        _up_score = ((NSNumber *)dict[@"up_score"]).integerValue;
        _up_tags = dict[@"up_tags"];
    }
    return self;
}

+ (instancetype)modelWithDict:(NSDictionary *)dict{
    
    return [[self alloc]initWithDict:dict];
}


@end
