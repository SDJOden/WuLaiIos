//
//  WYWaitUserHisMsgViewController.m
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/7/20.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import "WYWaitUserHisMsgViewController.h"
#import "GradientView.h"
#import "WYWaitUserHisMsgTableViewCell.h"
#import "WYWaitMsgTextCell.h"
#import "WYWaitMsgImageCell.h"
#import "WYWaitMsgVoiceCell.h"
#import "WYWaitMsgCardCell.h"
#import "RCMessageModel.h"
#import "VoiceConverter.h"
#import "WYConnectListModel.h"
#import "WYWaitingUserData.h"
#import "SDJChatRoomController.h"
#import "WYConnectViewController.h"
#import "WYChatRoomLoveViewController.h"
#import "UIBarButtonItem+Extension.h"
#import "MJRefresh.h"
#import "MBProgressHUD.h"

@interface WYWaitUserHisMsgViewController () <UITableViewDelegate, UITableViewDataSource, WYWaitUserHisMsgTableViewCellDelegate>

@property (nonatomic, assign) int page;
@property (nonatomic, assign) int dataNumber;
@property (nonatomic, assign) BOOL isRequestEnable;
@property (nonatomic, strong) UITableView *waitMsgTableView;
@property (nonatomic, strong) GradientView *separateView;
@property (nonatomic, strong) UIButton *accessButton;
@property (nonatomic, strong) NSMutableArray *msgListArray;
@property (nonatomic, assign) BOOL isFirstTime;

@end

#define WAITCELLIDENTIFIER @"wyWaitUserHisMsgTableViewCell"
#define WAIT_TEXTCELL_IDENTIFIER @"wyWaitMsgTextCell"
#define WAIT_IMAGECELL_IDENTIFIER @"wyWaitMsgImageCell"
#define WAIT_VOICECELL_IDENTIFIER @"wyWaitMsgVoiceCell"
#define WAIT_CARDCELL_IDENTIFIER @"wyWaitMsgCardCell"

@implementation WYWaitUserHisMsgViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self rcinit];
    [self addLeftItem];
    [self layout];
}

-(void)rcinit{
    
    self.title = _nickName;
    self.view.backgroundColor = [UIColor whiteColor];
    _isFirstTime = YES;
    _page = 1;
    _isRequestEnable = YES;
}

-(void)addLeftItem{
    
    UIBarButtonItem *leftItem = [UIBarButtonItem itemWithTitle:@"待接入" target:self action:@selector(popToVC) left:YES];
    self.navigationItem.leftBarButtonItem = leftItem;
}

-(void)layout{
    
    self.waitMsgTableView.frame = CGRectMake(0, 0, SCREENWIDTH, SCREENHEIGHT - 64 - 44);
    self.separateView.frame = CGRectMake(0, self.view.h - 72 - 64, SCREENWIDTH, 72);
    self.accessButton.frame = CGRectMake((self.view.w - 150)*0.5, 5, 150, 44);
}

-(void)setUserData:(WYWaitingUserData *)userData{
    
    _userData = userData;
    [self.connectListModel makeModelWithWaitModel:userData];
    _userId = [userData.user_id.stringValue copy];
    [self rcinit];
    [self loadNewMsgData];
}

-(void)loadNewMsgData{
    
    [self.msgListArray removeAllObjects];
    [self requestTopMsg];
}

-(void)moreMsgData{
    
    if (!_isRequestEnable) {
        return ;
    }
    _page ++;
    _isFirstTime = NO;
    _isRequestEnable = NO;
    [self requestTopMsg];
}

-(void)requestTopMsg{
    
    __weak __typeof__(self) weakSelf = self;

    [[SDJNetworkTools sharedTools]userCurrentMessage:[SDJUserAccount loadAccount].token user_id:_userId size:@"10" pn:@(_page).stringValue finished:^(NSDictionary<NSString *,id> * _Nullable result, NSError * _Nullable error) {
        if (error) {
            [weakSelf.waitMsgTableView.mj_header endRefreshing];
            _isRequestEnable = YES;
            return ;
        }
        if (![result[@"error"] isEqual:[NSNull null]]) {//异端登陆
            NSNumber *code = result[@"error"][@"error_code"];
            if (code.integerValue == 20000) {
                NSString *UserPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).firstObject stringByAppendingPathComponent:@"account.plist"];
                [[NSFileManager defaultManager]removeItemAtPath:UserPath error:nil];
                [[RCIMClient sharedRCIMClient] logout];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"WYRootViewControllerSwitchNotification" object:@"0"];
                });
            }
        }
        else {
            NSArray *historyMsgArray = result[@"data"][@"latest_msgs"];
            NSMutableArray *messageArray = [[NSMutableArray alloc]init];
            __block int downloadFlag = 0;
            _dataNumber = (int)historyMsgArray.count;
            for (NSDictionary *historyMsg in historyMsgArray) {
                int msgType = ((NSNumber *)historyMsg[@"extra"][@"msg_type"]).intValue;
                if (msgType == 1) {//text消息
                    RCMessageModel * msgModel = [WYTool createMsgModel:historyMsg];
                    [messageArray addObject:msgModel];
                    downloadFlag ++;
                    if (downloadFlag == _dataNumber) {
                        [weakSelf messageDeal:[[messageArray reverseObjectEnumerator] allObjects]];
                        downloadFlag = 0;
                    }
                }
                else if (msgType == 2) {//image消息
                    RCMessageModel * msgModel = [WYTool createImageModel:historyMsg];
                    UIImageView *imageView = [[UIImageView alloc]init];
                    [imageView sd_setImageWithURL:[NSURL URLWithString:historyMsg[@"imageUri"]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            ((RCImageMessage *)msgModel.content).thumbnailImage = imageView.image;
                            ((RCImageMessage *)msgModel.content).originalImage = imageView.image;
                            [weakSelf.waitMsgTableView reloadData];
                            downloadFlag ++;
                            if (downloadFlag == _dataNumber) {
                                [weakSelf messageDeal:[[messageArray reverseObjectEnumerator] allObjects]];
                                downloadFlag = 0;
                            }
                        });
                    }];
                    [messageArray addObject:msgModel];
                }
                else if (msgType == 5) {//voice消息
                    RCMessageModel * msgModel = [WYTool createVoiceMsgModel:historyMsg];
                    [messageArray addObject:msgModel];
                    downloadFlag ++;
                    if (downloadFlag == _dataNumber) {
                        [weakSelf messageDeal:[[messageArray reverseObjectEnumerator] allObjects]];
                        downloadFlag = 0;
                    }
                }
                else if (msgType == 6) {
                    RCMessageModel * msgModel = [WYTool createCardMsgModel:historyMsg];
                    [messageArray addObject:msgModel];
                    downloadFlag ++;
                    if (downloadFlag == _dataNumber) {
                        [weakSelf messageDeal:[[messageArray reverseObjectEnumerator] allObjects]];
                        downloadFlag = 0;
                    }
                }
                else if (msgType == 7) {
                    RCMessageModel * msgModel = [WYTool createSingleCardMsgModel:historyMsg];
                    [messageArray addObject:msgModel];
                    downloadFlag ++;
                    if (downloadFlag == _dataNumber) {
                        [weakSelf messageDeal:[[messageArray reverseObjectEnumerator] allObjects]];
                        downloadFlag = 0;
                    }
                }
                else if (msgType == 10) {//text消息
                    RCMessageModel * msgModel = [WYTool createDateModel:historyMsg];
                    [messageArray addObject:msgModel];
                    downloadFlag ++;
                    if (downloadFlag == _dataNumber) {
                        [weakSelf messageDeal:[[messageArray reverseObjectEnumerator] allObjects]];
                        downloadFlag = 0;
                    }
                }

                else {
                    downloadFlag ++;
                    if (downloadFlag == _dataNumber) {
                        [weakSelf messageDeal:[[messageArray reverseObjectEnumerator] allObjects]];
                        downloadFlag = 0;
                    }
                }
            }
            if (_dataNumber == 0) {
                [self.waitMsgTableView.mj_header endRefreshing];
                _isRequestEnable = YES;
            }
        }
    }];
}

/**
 *  WY添加
 */
-(void)messageDeal:(NSArray *)__messageArray{
    
    [self.waitMsgTableView.mj_header endRefreshing];
    for (int i = 0; i < __messageArray.count; i++) {
        RCMessage *rcMsg = [__messageArray objectAtIndex:i];
        RCMessageModel *model = [[RCMessageModel alloc] initWithMessage:rcMsg];
        [self pushOldMessageModel:model];
    }
    [self.waitMsgTableView reloadData];
    _isRequestEnable = YES;
    if (!_isFirstTime) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:_dataNumber-1 inSection:0];
        [self.waitMsgTableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:NO];
    }else{
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:_dataNumber-1 inSection:0];
        [self.waitMsgTableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:NO];
    }
}

- (void)pushOldMessageModel:(RCMessageModel *)model {
    
    [self.msgListArray insertObject:model atIndex:0];
}

#pragma mark --- UITableViewDelegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.msgListArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    RCMessageModel *model = [self.msgListArray objectAtIndex:indexPath.row];
    RCMessageContent *messageContent = model.content;
    WYWaitUserHisMsgTableViewCell *cell = nil;
    
    if ([messageContent isKindOfClass:[RCTextMessage class]]) {
        cell = [tableView dequeueReusableCellWithIdentifier:WAIT_TEXTCELL_IDENTIFIER forIndexPath:indexPath];
        if (indexPath.row == self.msgListArray.count - 1) {
            cell.isLastCell = YES;
        }else {
            cell.isLastCell = NO;
        }
        cell.iconImage = self.iconImage;
        cell.userNickName = self.nickName;
        cell.model = model;
        cell.selectionStyle = UITableViewCellSeparatorStyleNone;
    } else if ([messageContent isKindOfClass:[RCImageMessage class]]) {
        cell = [tableView dequeueReusableCellWithIdentifier:WAIT_IMAGECELL_IDENTIFIER forIndexPath:indexPath];
        if (indexPath.row == self.msgListArray.count - 1) {
            cell.isLastCell = YES;
        }else {
            cell.isLastCell = NO;
        }
        cell.iconImage = self.iconImage;
        cell.userNickName = self.nickName;
        cell.model = model;
        cell.selectionStyle = UITableViewCellSeparatorStyleNone;
    } else if ([messageContent isKindOfClass:[RCVoiceMessage class]]) {
        cell = [tableView dequeueReusableCellWithIdentifier:WAIT_VOICECELL_IDENTIFIER forIndexPath:indexPath];
        if (indexPath.row == self.msgListArray.count - 1) {
            cell.isLastCell = YES;
        }else {
            cell.isLastCell = NO;
        }
        cell.iconImage = self.iconImage;
        cell.userNickName = self.nickName;
        cell.model = model;
        cell.selectionStyle = UITableViewCellSeparatorStyleNone;
    } else if ([messageContent isKindOfClass:[RCRichContentMessage class]]) {
        cell = [tableView dequeueReusableCellWithIdentifier:WAIT_CARDCELL_IDENTIFIER forIndexPath:indexPath];
        if (indexPath.row == self.msgListArray.count - 1) {
            cell.isLastCell = YES;
        }else {
            cell.isLastCell = NO;
        }
        cell.iconImage = self.iconImage;
        cell.userNickName = self.nickName;
        cell.model = model;
        cell.selectionStyle = UITableViewCellSeparatorStyleNone;
    }
    
    cell.wyWaitUserHisMsgTableViewCellDelegate = self;
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    RCMessageModel *model = [self.msgListArray objectAtIndex:indexPath.row];
    RCMessageContent *messageContent = model.content;
    
    CGFloat height = 8 + 21 + 4 + 4 + 16 + 8 + 10;
    CGFloat maxWidth = SCREENWIDTH - 12 - 32 - 12 - 12;
    
    if ([messageContent isKindOfClass:[RCTextMessage class]]) {
        NSMutableString *realTextString = [NSMutableString stringWithFormat:@""];
        if ([messageContent isKindOfClass:[RCTextMessage class]]) {
            NSString *text = ((RCTextMessage*)messageContent).content;
            NSString *pattern = @"<a href=\'(.*?)\'>(.*?)</a>";
            NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern options:0 error:nil];
            NSArray *matches = [regex matchesInString:text options:0 range:NSMakeRange(0, text.length)]; // 网址
            NSUInteger lastIdx = 0;
            NSMutableString *attributedString = [NSMutableString stringWithFormat:@""]; // 总字符串
            if (matches.count) { // 解析部分bug, 1.文字需要提出来, 2. 位置需要调整 matchs.count
                NSMutableArray *urlArrs = [[NSMutableArray alloc] init]; // 存url
                int i = 0;
                for (NSTextCheckingResult* match in matches)
                {
                    NSRange range = match.range;
                    if (range.location > lastIdx)
                    {
                        NSString *temp = [text substringWithRange:NSMakeRange(lastIdx, range.location - lastIdx)];
                        [attributedString appendString:temp];// 记录头
                        [realTextString appendString:temp];
                    }
                    NSString *title = [text substringWithRange:[match rangeAtIndex:2]]; // 标题
                    NSString *url = [text substringWithRange:[match rangeAtIndex:1]]; // 网址
                    lastIdx = range.location + range.length;
                    [attributedString appendFormat:@"<%d>%@</%d>",i,title,i];
                    [realTextString appendString:title];
                    [urlArrs addObject:url];
                    i++;
                }
                if (lastIdx < text.length)
                {
                    NSString *temp = [text substringFromIndex:lastIdx];
                    [attributedString appendString:temp];// 记录尾
                    [realTextString appendString:temp];
                }
            } else {
                [realTextString appendString:((RCTextMessage *)model.content).content];
            }
        }
        CGSize __textSize = CGSizeZero;
        __textSize = RC_MULTILINE_TEXTSIZE_GEIOS7(realTextString, [UIFont systemFontOfSize:14], CGSizeMake(maxWidth, 8000));
        __textSize = CGSizeMake(ceilf(__textSize.width), ceilf(__textSize.height));
        CGSize __labelSize = CGSizeMake(__textSize.width, __textSize.height);
        CGFloat __bubbleHeight = __labelSize.height + 7 + 7 < 28 ? 28 : (__labelSize.height + 7 + 7);
        height += __bubbleHeight;
        return height;
        
    } else if ([messageContent isKindOfClass:[RCImageMessage class]]) {
        RCImageMessage *_imageMessage = (RCImageMessage *)messageContent;
        CGSize imageSize = _imageMessage.thumbnailImage.size;
        //兼容240
        CGFloat imageWidth = 120;
        CGFloat imageHeight = 120;
        
        CGFloat imageWidthMin = 60;
        CGFloat imageHeightMin = 60;
        CGFloat imageWidthMax = [UIScreen mainScreen].bounds.size.width * 0.5/*0.8*/;
        CGFloat imageHeightMax = [UIScreen mainScreen].bounds.size.height * 0.3/*0.6*/;
        CGFloat scale = imageWidthMax * 1.0 / imageHeightMax;
        if (imageSize.width < imageWidthMin && imageSize.height < imageHeightMin) {
            if (imageSize.width >= imageSize.height) {
                if (imageSize.width * 1.0 / imageSize.height >= imageWidthMax * 1.0 / imageHeightMin) {
                    imageWidth = imageWidthMax;
                    imageHeight = imageSize.height *imageWidthMax * 1.0 / imageSize.width;
                } else {
                    imageHeight = imageHeightMin;
                    imageWidth = imageSize.width * imageHeightMin * 1.0 / imageSize.height;
                }
            } else {
                if (imageSize.height * 1.0 / imageSize.width >= imageHeightMax * 1.0 / imageWidthMin) {
                    imageHeight = imageHeightMax;
                    imageWidth = imageSize.width *imageHeightMax * 1.0 / imageSize.height;
                } else {
                    imageWidth = imageWidthMin;
                    imageHeight = imageSize.height * imageWidthMin * 1.0 / imageSize.width;
                }
            }
        } else if (imageSize.width > imageWidthMax || imageSize.height > imageHeightMax) {
            if (imageSize.width >= imageSize.height * scale) {
                imageWidth = imageWidthMax;
                imageHeight = imageSize.height *imageWidthMax * 1.0 / imageSize.width;
            } else {
                imageHeight = imageHeightMax;
                imageWidth = imageSize.width * imageHeightMax * 1.0 / imageSize.height;
            }
        } else {
            imageWidth = imageSize.width;
            imageHeight = imageSize.height;
        }
        imageSize = CGSizeMake(imageWidth, imageHeight);
        height += imageSize.height;
        return height;
    } else if ([messageContent isKindOfClass:[RCVoiceMessage class]]) {
        height += 21;
        return height;
    } else if ([messageContent isKindOfClass:[RCRichContentMessage class]]) {
        height += 28;
        return height;
    }
    return 0;
}

#pragma mark --- WYWaitUserHisMsgCollectionViewCellDelegate
-(void)pushToLoveViewController{
    
    WYChatRoomLoveViewController *chatRoom = [[WYChatRoomLoveViewController alloc]initWithConnectListModel:self.connectListModel andPage:self.page];
    chatRoom.listDataArray = [self.msgListArray copy];
    
    [self.navigationController pushViewController:chatRoom animated:YES];
}

#pragma mark --- 返回leftItem
-(void)popToVC{
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark --- 按钮点击事件
-(void)connectWaitingUser{
    
    __weak typeof(self) weakSelf = self;
    
    [[SDJNetworkTools sharedTools]joinConversationWithoutOtherGenius:[SDJUserAccount loadAccount].token user_id:self.userId force:@"1" finished:^(NSDictionary<NSString *,id> * _Nullable result, NSError * _Nullable error) {
        if (![result[@"error"] isEqual:[NSNull null]]) {//报错
            NSNumber *code = result[@"error"][@"error_code"];
            if (code.integerValue == 20000) {
                NSString *UserPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).firstObject stringByAppendingPathComponent:@"account.plist"];
                [[NSFileManager defaultManager]removeItemAtPath:UserPath error:nil];
                [[RCIMClient sharedRCIMClient] logout];
                // 发送通知让appdelegate去切换根视图为聊天列表界面
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"WYRootViewControllerSwitchNotification" object:@"0"];
                    return;
                });
            }
            else {
                __block MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:weakSelf.view];
                [weakSelf.view addSubview:HUD];
                HUD.mode = MBProgressHUDModeText;
                HUD.labelText = result[@"error"][@"error_name"];
                [HUD showAnimated:YES whileExecutingBlock:^{
                    sleep(2);
                } completionBlock:^{
                    [HUD removeFromSuperview];
                    HUD = nil;
                    [self.navigationController popViewControllerAnimated:YES];
                }];
            }
        } else {
            SDJChatRoomController *chatVC = [[SDJChatRoomController alloc]initWithConversationType:ConversationType_PRIVATE targetId:@"rcg0" connectListModel:_connectListModel];
            WYConnectViewController *connectVC = weakSelf.tabBarController.childViewControllers[0].childViewControllers[0];
            chatVC.unReadNumModel = connectVC.unReadNumModel;
            chatVC.headImage = _iconImage;
            chatVC.unReadNumStr = connectVC.tabBarItem.badgeValue;
            chatVC.isWaitngContect = YES;
            chatVC.hidesBottomBarWhenPushed = YES;
            [weakSelf.navigationController pushViewController:chatVC animated:YES];
        }
    }];
    //跳转到已接入
//    _isConnectFlag = YES;
//    [__weakSelf.navigationController popViewControllerAnimated:YES];
}

#pragma mark --- 懒加载
- (UITableView *)waitMsgTableView {
    
    if(_waitMsgTableView == nil) {
        _waitMsgTableView = [[UITableView alloc] init];
        _waitMsgTableView.delegate = self;
        _waitMsgTableView.dataSource = self;
        _waitMsgTableView.mj_header = [MJRefreshStateHeader headerWithRefreshingTarget:self refreshingAction:@selector(moreMsgData)];
        [_waitMsgTableView registerClass:[WYWaitUserHisMsgTableViewCell class] forCellReuseIdentifier:WAITCELLIDENTIFIER];
        [_waitMsgTableView registerClass:[WYWaitMsgTextCell class] forCellReuseIdentifier:WAIT_TEXTCELL_IDENTIFIER];
        [_waitMsgTableView registerClass:[WYWaitMsgImageCell class] forCellReuseIdentifier:WAIT_IMAGECELL_IDENTIFIER];
        [_waitMsgTableView registerClass:[WYWaitMsgVoiceCell class] forCellReuseIdentifier:WAIT_VOICECELL_IDENTIFIER];
        [_waitMsgTableView registerClass:[WYWaitMsgCardCell class] forCellReuseIdentifier:WAIT_CARDCELL_IDENTIFIER];
        _waitMsgTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self.view addSubview:_waitMsgTableView];
    }
    return _waitMsgTableView;
}

- (UIButton *)accessButton {
    
	if(_accessButton == nil) {
		_accessButton = [[UIButton alloc] init];
        _accessButton.backgroundColor = HEXCOLOR(0x17D17A);
        [_accessButton setTitle:@"接入用户" forState:UIControlStateNormal];
        [_accessButton setTitleColor:HEXCOLOR(0xffffff) forState:UIControlStateNormal];
        _accessButton.titleLabel.font = [UIFont systemFontOfSize:18];
        _accessButton.layer.cornerRadius = 22;
        _accessButton.layer.masksToBounds = YES;
        [_accessButton addTarget:self action:@selector(connectWaitingUser) forControlEvents:UIControlEventTouchUpInside];
        [self.separateView addSubview:_accessButton];
	}
	return _accessButton;
}

- (GradientView *)separateView {
    
	if(_separateView == nil) { 
		_separateView = [[GradientView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 72) type:TRANSPARENT_GRADIENT_TYPE];
        _separateView.userInteractionEnabled = YES;
        [self.view addSubview:_separateView];
	}
	return _separateView;
}

- (NSMutableArray *)msgListArray {
    
	if(_msgListArray == nil) {
		_msgListArray = [[NSMutableArray alloc] init];
	}
	return _msgListArray;
}

- (WYConnectListModel *)connectListModel {
    
	if(_connectListModel == nil) {
		_connectListModel = [[WYConnectListModel alloc] init];
	}
	return _connectListModel;
}

@end
