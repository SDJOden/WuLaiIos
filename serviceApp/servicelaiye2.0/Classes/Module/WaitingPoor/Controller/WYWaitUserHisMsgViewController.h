//
//  WYWaitUserHisMsgViewController.h
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/7/20.
//  Copyright © 2016年 shengdong. All rights reserved.
//

// 预览消息列表界面.在下面也有通用.
#import <UIKit/UIKit.h>

@class WYWaitingUserData, WYConnectListModel;

@protocol WYWaitUserHisMsgViewControllerDelegate <NSObject>

-(void)accessUserWithUserId:(NSString *)userId;

@end

@interface WYWaitUserHisMsgViewController : UIViewController

@property (nonatomic, strong) UIImage *iconImage;

@property (nonatomic, copy) NSString *nickName;

@property (nonatomic, copy) NSString *userId;

@property (nonatomic, strong) WYWaitingUserData *userData;

@property (nonatomic, strong) WYConnectListModel *connectListModel;

@property (nonatomic, weak) id <WYWaitUserHisMsgViewControllerDelegate> waitHisMsgDelegate;

-(void)rcinit;

-(void)loadNewMsgData;

@end
