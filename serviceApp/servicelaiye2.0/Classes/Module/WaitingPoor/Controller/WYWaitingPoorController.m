//
//  WYWaitingPoorController.m
//  servicelaiye2.0
//
//  Created by 盛东 on 15/12/9.
//  Copyright © 2015年 shengdong. All rights reserved.
//

#import "WYWaitingPoorController.h"
#import "WYWaitingUserData.h"
#import "WYWaitingUserTableViewCell.h"
#import "WYWaitingUserData.h"
#import "WYHistoryMessageView.h"
#import "WYAlertView.h"
#import "UIBarButtonItem+Extension.h"
#import "WYWaitUserHisMsgViewController.h"
#import "MJRefresh.h"
#import "JZTabBar.h"

@interface WYWaitingPoorController ()<UITableViewDataSource, UITableViewDelegate, UITabBarDelegate, UITabBarControllerDelegate, WYHistoryMessageViewDelegate, WYWaitingUserTableViewCellDelegate, WYWaitUserHisMsgViewControllerDelegate>
/**
 *  网络请求返回的模型数组
 */
@property (nonatomic, strong) NSMutableArray *dataArray;
/**
 *  多选的cell的下标数组
 */
@property (nonatomic, strong) NSMutableArray *cellSelectedArray;
/**
 *  定时器
 */
@property (nonatomic, strong) NSTimer *timer;

/**
 *  待接入列表
 */
@property (nonatomic, strong) UITableView *waitingListTableView;
@property (nonatomic, strong) UIView *footView;
@property (nonatomic, strong) UIView *separateLineView;
@property (nonatomic, strong) UIButton *accessButton;
@property (nonatomic, strong) UILabel *countLb;
//计数，成功接入数和错误数
@property (nonatomic, assign) NSInteger accessNetworkCount;
@property (nonatomic, assign) NSInteger errorNetworkCount;
//警告view用来提示接入用户时的成功情况
@property (nonatomic, strong) WYAlertView *alertView;
@property (nonatomic, strong) UIImageView *imageView;

@end

@implementation WYWaitingPoorController

-(instancetype)init{
    
    if (self = [super init]) {
        
        [self addNaviItem];
        
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(addWaitingUser:) name:@"addWaitingUser" object:nil];
        __weak typeof(self) __weakSelf = self;
#warning to do 请求不该放在这里. 放到viewdidload. 最起码放在函数里.
        [[SDJNetworkTools sharedTools]waitingUsers:[SDJUserAccount loadAccount].token finished:^(NSDictionary<NSString *,id> * _Nullable result, NSError * _Nullable error) {
            if (![result[@"error"] isEqual:[NSNull null]]) {//报错
                NSNumber *code = result[@"error"][@"error_code"];
                if (code.integerValue == 20000) {
                    NSString *UserPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).firstObject stringByAppendingPathComponent:@"account.plist"];
                    [[NSFileManager defaultManager]removeItemAtPath:UserPath error:nil];
                    // 其他平台登录，token改变需要先登出融云
                    [[RCIMClient sharedRCIMClient] logout];
                    // 发送通知让appdelegate去切换根视图为聊天列表界面
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"WYRootViewControllerSwitchNotification" object:@"0"];
                    });
                }
            }
            else{
                NSArray *userlistArray = result[@"data"];
                if (userlistArray.count == 0) {
                    __weakSelf.tabBarItem.badgeValue = nil;
                }else {
                    __weakSelf.tabBarItem.badgeValue = @(userlistArray.count).stringValue;
                }
            }
        }];
    }
    return self;
}

-(void)home{
    
    if (self.dataArray.count == 0) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"暂无待接入用户" message:nil delegate:self cancelButtonTitle:@"知道了" otherButtonTitles:nil];
        
        [alert show];
        
    } else {
        
        [self.waitingListTableView setEditing:NO animated:YES];
        
        [self.cellSelectedArray removeAllObjects];
        
        for (int i = 0; i < (self.dataArray.count < 2?self.dataArray.count:2) ; i++) {
            
            [self.cellSelectedArray addObject:@(i)];
        }
        
        [self accessUserWithRow];
    }
}

-(void)addNaviItem{
    
    UIBarButtonItem * leftItem = [UIBarButtonItem itemWithTitle:@"一键" target:self action:@selector(home) left:NO];
    self.navigationItem.leftBarButtonItem = leftItem;
    
    UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightBtn setTitle:@"批量接入" forState:UIControlStateNormal];
    [rightBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [rightBtn setTitleColor:[UIColor colorWithRed:0.6579 green:0.6579 blue:0.6579 alpha:1.0] forState:UIControlStateHighlighted];
    rightBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [rightBtn sizeToFit];
    rightBtn.frame = CGRectMake(0, 0, rightBtn.w, rightBtn.h);
    [rightBtn addTarget:self action:@selector(accessRows:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithCustomView:rightBtn];
    self.navigationItem.rightBarButtonItem = rightItem;
}

#pragma mark --- 导航栏按钮事件
-(void)accessRows:(UIButton *)sender{
    
    if (self.dataArray.count == 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"暂无待接入用户" message:nil delegate:self cancelButtonTitle:@"知道了" otherButtonTitles:nil];
        [alert show];
    } else {
        [self.waitingListTableView setEditing:!self.waitingListTableView.editing animated:YES];
        [sender setTitle:self.waitingListTableView.editing ? @"取消" : @"批量接入" forState:UIControlStateNormal];
        [sender sizeToFit];
        sender.frame = CGRectMake(0, 0, sender.w, sender.h);
        if (!self.waitingListTableView.editing) {
            [self.cellSelectedArray removeAllObjects];
            [self changeCount];
            _accessButton.enabled = NO;
            [UIView animateWithDuration:0.5 animations:^{
                [self.waitingListTableView mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.bottom.equalTo(self.view);
                }];
            }];
        } else {
            [UIView animateWithDuration:0.5 animations:^{
                [self.waitingListTableView mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.bottom.equalTo(self.view).offset(-44);
                }];
            }];
        }
    }
}

#pragma mark --- 按钮点击事件
-(void)access{
    
    if (self.waitingListTableView.editing) {
        [self accessUserWithRow];
        [self.waitingListTableView setEditing:NO animated:YES];
    }
}

#pragma mark --- 布局
-(void)addSubViews{
    
    [self.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.bottom.equalTo(self.view);
    }];
    
    [self.footView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.waitingListTableView.bottom);
        make.left.right.equalTo(self.view);
        make.height.equalTo(44);
    }];
    
    [self.waitingListTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.top.left.right.equalTo(self.view);
    }];
    
    [self.countLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(12);
        make.right.equalTo(self.accessButton.left).offset(-12);
        make.top.bottom.equalTo(self.footView);
    }];
    
    [self.separateLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.equalTo(self.footView);
        make.height.equalTo(0.5);
    }];
    
    [self.accessButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.footView);
        make.right.equalTo(-12);
        make.height.equalTo(30);
        make.width.equalTo(54);
    }];
}

-(void)changeCount{
    
    if (self.cellSelectedArray.count != 0) {
        NSString *countStr = [NSString stringWithFormat:@"已选择 %ld 个用户", (unsigned long)self.cellSelectedArray.count];
        NSMutableAttributedString *str = [[NSMutableAttributedString alloc]initWithString:countStr];
        [str addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:0.315 green:0.624 blue:1.000 alpha:1.000] range:NSMakeRange(4, @(self.cellSelectedArray.count).stringValue.length)];
        self.countLb.attributedText = str;
    }
    else {
        self.countLb.text = @"已选择 0 个用户";
    }
}

//网络请求待接入数据
-(void)refreshNetworking{
    
    __weak WYWaitingPoorController *__weakSelf = self;
    
    [[SDJNetworkTools sharedTools]waitingUsers:[SDJUserAccount loadAccount].token finished:^(NSDictionary<NSString *,id> * _Nullable result, NSError * _Nullable error) {
        if (error) {
            //----------------容错处理--------
#warning to do
            dispatch_async(dispatch_get_main_queue(), ^{
                [__weakSelf.waitingListTableView.mj_header endRefreshing];
            });
        }
        if (![result[@"error"] isEqual:[NSNull null]]) {//报错
            NSNumber *code = result[@"error"][@"error_code"];
            if (code.integerValue == 20000) {
                NSString *UserPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).firstObject stringByAppendingPathComponent:@"account.plist"];
                [[NSFileManager defaultManager]removeItemAtPath:UserPath error:nil];
                //其他平台登录，token改变需要先登出融云
                [[RCIMClient sharedRCIMClient] logout];
                // 发送通知让appdelegate去切换根视图为聊天列表界面
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"WYRootViewControllerSwitchNotification" object:@"0"];
                });
            }
        }
        else{
            dispatch_async(dispatch_get_main_queue(), ^{
                //清空数组中元素
                [__weakSelf.dataArray removeAllObjects];
                //json解析，保存到model
                NSArray *dataArray = result[@"data"];
                for (NSDictionary *dataDic in dataArray) {
                    WYWaitingUserData *userData = [WYWaitingUserData modelWithDict:dataDic];
                    [__weakSelf.dataArray addObject:userData];
                }
                //tableView刷新
#warning to do
                //---------------主线程刷新.-------------------
                [__weakSelf.waitingListTableView reloadData];
                [__weakSelf.waitingListTableView.mj_header endRefreshing];
                [__weakSelf.cellSelectedArray removeAllObjects];
                if (__weakSelf.dataArray.count > 0) {
                    [__weakSelf addNaviItem];
                    __weakSelf.imageView.hidden = YES;
                    __weakSelf.tabBarItem.badgeValue = @(__weakSelf.dataArray.count).stringValue;
                } else {
                    __weakSelf.imageView.hidden = NO;
                }
            });
        }
    }];
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self addSubViews];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    [self addNaviItem];
    [self refreshNetworking];
    [self rcinit];
}

-(void)clickRefreshList{
    
    [self refreshNetworking];
    [self rcinit];
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
    [self.waitingListTableView setEditing:NO animated:YES];
    [self.cellSelectedArray removeAllObjects];
    [self changeCount];
    _accessButton.enabled = NO;
#warning to do 这是干嘛的.并不能看到动画效果.
    [UIView animateWithDuration:0.5 animations:^{
        [self.waitingListTableView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self.view);
        }];
    }];
}

-(void)viewDidDisappear:(BOOL)animated{
    
    [super viewDidDisappear:YES];
    [self.timer invalidate];
    self.timer = nil;
}

-(void)rcinit{
    
    self.view.backgroundColor = HEXCOLOR(0xF3F5F6);
    self.accessNetworkCount = 0;
    self.errorNetworkCount = 0;
}

-(void)addWaitingUser:(NSNotification *)noti{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        if (((NSString *)noti.userInfo[@"content"]).intValue > 0) {
            self.tabBarItem.badgeValue = @(((NSString *)noti.userInfo[@"content"]).intValue).stringValue;
        }
        else {
            self.tabBarItem.badgeValue = nil;
        }
    });
}

//接入用户
-(void)accessUserWithRow{
    
    [MobClick event:@"waiting_connectRightNaviBarItem_click"];
    
    self.countLb.text = @"已选择 0 个用户";
    
    for (int i = 0; i < self.cellSelectedArray.count; i ++) {
        NSNumber *number = self.cellSelectedArray[i];
        WYWaitingUserData *userData = self.dataArray[number.intValue];
        __weak WYWaitingPoorController *__weakSelf = self;
        [[SDJNetworkTools sharedTools]joinConversationWithoutOtherGenius:[SDJUserAccount loadAccount].token user_id:userData.user_id.stringValue force:@"0" finished:^(NSDictionary<NSString *,id> * _Nullable result, NSError * _Nullable error) {
            if (![result[@"error"] isEqual:[NSNull null]]) {//报错
                NSNumber *code = result[@"error"][@"error_code"];
                if (code.integerValue == 20000) {
                    NSString *UserPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).firstObject stringByAppendingPathComponent:@"account.plist"];
                    [[NSFileManager defaultManager]removeItemAtPath:UserPath error:nil];
                    //其他平台登录，token改变需要先登出融云
                    [[RCIMClient sharedRCIMClient] logout];
                    // 发送通知让appdelegate去切换根视图为聊天列表界面
                    dispatch_async(dispatch_get_main_queue(), ^{
                        //                    [MobClick endEvent:@"registerView_time" label:@"登出"];
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"WYRootViewControllerSwitchNotification" object:@"0"];
                    });
                }
            }
            if ([result[@"error"]isKindOfClass:[NSNull class]]) {
                __weakSelf.accessNetworkCount ++;
            } else {
                __weakSelf.errorNetworkCount ++;
                if (__weakSelf.errorNetworkCount == 1) {
                    [__weakSelf.alertView setAlertTitleLb:result[@"error"][@"detail"] andAlertContentLb:result[@"error"][@"error_name"]];
                    [__weakSelf.tabBarController.view addSubview:__weakSelf.alertView];
                    [UIView animateWithDuration:1.0 animations:^{
                        __weakSelf.alertView.transform = CGAffineTransformMakeTranslation(0, 60 + 2);
                        __weakSelf.alertView.backgroundColor = [UIColor orangeColor];
                    } completion:^(BOOL finished) { // 向下移动的动画执行完毕后
                        // 建议:尽量使用animateWithDuration, 不要使用animateKeyframesWithDuration
                        [UIView animateWithDuration:1.0 delay:1.0 options:UIViewAnimationOptionCurveLinear animations:^{
                            __weakSelf.alertView.backgroundColor = [UIColor clearColor];
                            __weakSelf.alertView.transform = CGAffineTransformIdentity;
                        } completion:^(BOOL finished) {
                            // 将btn从内存中移除
                            [__weakSelf.alertView removeFromSuperview];
                            __weakSelf.alertView = nil;
                        }];
                    }];
                }
            }
            [__weakSelf changeTabIndex:__weakSelf];
        }];
    }
}

#pragma mark ---
-(void)accessUserWithUserId:(NSString *)userId{}

- (WYAlertView *)alertView {
    
    if(_alertView == nil) {
        _alertView = [[WYAlertView alloc] init];
        _alertView.frame = CGRectMake(2, -60, self.view.w - 4, 60);
        _alertView.backgroundColor = [UIColor clearColor];
    }
    return _alertView;
}

-(void)changeTabIndex:(WYWaitingPoorController *)waitingPoorController{
    //跳转到已接入
    if (waitingPoorController.accessNetworkCount + waitingPoorController.errorNetworkCount == waitingPoorController.cellSelectedArray.count) {
        if (waitingPoorController.errorNetworkCount != waitingPoorController.cellSelectedArray.count) {
            dispatch_async(dispatch_get_main_queue(), ^{
                waitingPoorController.tabBarController.selectedIndex = 0;
#warning to do 轻耦合
                for (UIView *view in waitingPoorController.tabBarController.tabBar.subviews) {
                    if ([view isKindOfClass:[JZTabbar class]]) {
                        [(JZTabbar *)view selectItemWithIndex:1];
                        break;
                    }
                }
            });
        }
        [waitingPoorController refreshNetworking];
        waitingPoorController.accessNetworkCount = 0;
        waitingPoorController.errorNetworkCount = 0;
    }
}

#pragma mark - Table view data source
//计算条目数量
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.dataArray.count;
}

//载入cell
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *Identifier = @"MyCell";
    WYWaitingUserTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier forIndexPath:indexPath];
    //根据模型解析cell样式
    [cell setContentByModel:self.dataArray[indexPath.row]];
    [cell changeTime];
    UILongPressGestureRecognizer * longPressGesture = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(cellLongPress:)];
    longPressGesture.minimumPressDuration = 1.5;
    longPressGesture.numberOfTouchesRequired = 1;
    longPressGesture.allowableMovement = 15;
    [cell addGestureRecognizer:longPressGesture];
    cell.waitingUserTableViewCellDelegate = self;
    return cell;
}

/**
 *  长按cell直接接入
 */
- (void)cellLongPress:(UIGestureRecognizer *)recognizer{
    
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        NSIndexPath *indexPath = [self.waitingListTableView indexPathForCell:(WYWaitingUserTableViewCell *)recognizer.view];
        [self.cellSelectedArray removeAllObjects];
        [self.cellSelectedArray addObject:@(indexPath.row)];
        [self accessUserWithRow];
    }
}

//点击tableView的方法
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //接入用户
    if (tableView.editing) {
        if (![self.cellSelectedArray containsObject:@(indexPath.row)]) {
            [self.cellSelectedArray addObject:@(indexPath.row)];
            [self changeCount];
            
        }
        if (self.cellSelectedArray.count > 0) {
            self.accessButton.enabled = YES;
        }else {
            self.accessButton.enabled = NO;
        }
    }
    else {
        WYWaitingUserTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        WYWaitUserHisMsgViewController *waitMsgVC = [[WYWaitUserHisMsgViewController alloc]init];
        WYWaitingUserData *userData = self.dataArray[indexPath.row];
        waitMsgVC.waitHisMsgDelegate = self;
        waitMsgVC.iconImage = cell.iconImageView.image;
        waitMsgVC.nickName = cell.nicknameLabel.text;
        waitMsgVC.userData = userData;
        waitMsgVC.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:waitMsgVC animated:YES];
    }
    
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (tableView.editing) {
        if ([self.cellSelectedArray containsObject:@(indexPath.row)]) {
            [self.cellSelectedArray removeObject:@(indexPath.row)];
            [self changeCount];
        }
        if (self.cellSelectedArray.count > 0) {
            self.accessButton.enabled = YES;
        }else {
            self.accessButton.enabled = NO;
        }
    }
}

//高度
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 90;
}

//分割线顶头
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]){
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    cell.preservesSuperviewLayoutMargins = NO;
}

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return YES;
}

//问题二：某一行支持那种编辑模式
-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    //返回两种样式，让cell同时支持删除和插入，使用或运算，多选，显示圆圈
    return UITableViewCellEditingStyleDelete | UITableViewCellEditingStyleInsert;
}

// 这里从来没有执行过.
//#pragma mark --- WYWaitingUserTableViewCell的代理方法
//-(void)nextBtnClickWithImage:(UIImage *)image andHistoryModel:(WYHistoryModel *)historyModel{
//    
//    [[RCIM sharedRCIM] setUserIcon:[UIImage imageNamed:@"chatroom_userIcon_xiaolai"]];
//    [[RCIM sharedRCIM] setGeniusIcon:image];
//    WYChatMsgViewController *chatMsgVC = [WYChatMsgViewController createChatMsgViewControllerWithWaiting:historyModel];
//    chatMsgVC.title = @"待接入";
//    chatMsgVC.hidesBottomBarWhenPushed = YES;
//    [self.navigationController pushViewController:chatMsgVC animated:YES];
//}

#pragma mark --- 懒加载
- (UIView *)footView {
    
    if(_footView == nil) {
        _footView = [[UIView alloc] init];
        _footView.backgroundColor = HEXCOLOR(0xffffff);
        [self.view addSubview:_footView];
    }
    return _footView;
}
- (UIView *)separateLineView {
    
    if(_separateLineView == nil) {
        _separateLineView = [[UIView alloc] init];
        _separateLineView.backgroundColor = [UIColor lightGrayColor];
        [self.footView addSubview:_separateLineView];
    }
    return _separateLineView;
}

- (UIButton *)accessButton {
    
    if(_accessButton == nil) {
        _accessButton = [[UIButton alloc] init];
        [_accessButton setTitle:@"接入" forState:UIControlStateNormal];
        [_accessButton setTitleColor:HEXCOLOR(0xffffff) forState:UIControlStateNormal];
        [_accessButton setBackgroundImage:[UIImage getRoundImageWithSize:CGSizeMake(54, 30) backgroundColor:HEXCOLOR(0x17D17A) lineWidth:0 cornerRadius:0 shouldFill:YES] forState:UIControlStateNormal];
        [_accessButton setBackgroundImage:[UIImage getRoundImageWithSize:CGSizeMake(54, 30) backgroundColor:HEXCOLOR(0xE0E0E0) lineWidth:0 cornerRadius:0 shouldFill:YES] forState:UIControlStateDisabled];
        _accessButton.enabled = NO;
        _accessButton.titleLabel.font = [UIFont systemFontOfSize:14];
        _accessButton.layer.cornerRadius = 3.0;
        _accessButton.layer.masksToBounds = YES;
        [_accessButton addTarget:self action:@selector(access) forControlEvents:UIControlEventTouchUpInside];
        [self.footView addSubview:_accessButton];
    }
    return _accessButton;
}

- (UILabel *)countLb {
    
    if(_countLb == nil) {
        _countLb = [[UILabel alloc] init];
        _countLb.font = [UIFont systemFontOfSize:13];
        _countLb.text = @"已选择 0 个用户";
        _countLb.textColor = HEXCOLOR(0x666666);
        [self.footView addSubview:_countLb];
    }
    return _countLb;
}

- (UITableView *)waitingListTableView {
    
    if(_waitingListTableView == nil) {
        _waitingListTableView = [[UITableView alloc] init];
        _waitingListTableView.dataSource = self;
        _waitingListTableView.delegate = self;
        _waitingListTableView.tableFooterView = [UIView new];
        [_waitingListTableView registerClass:[WYWaitingUserTableViewCell class] forCellReuseIdentifier:@"MyCell"];
        _waitingListTableView.backgroundColor = [UIColor clearColor];
        _waitingListTableView.mj_header = [MJRefreshStateHeader headerWithRefreshingTarget:self refreshingAction:@selector(refreshNetworking)];
        [self.view addSubview:_waitingListTableView];
    }
    return _waitingListTableView;
}

-(NSMutableArray *)dataArray{
    
    if (!_dataArray) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}

-(NSMutableArray *)cellSelectedArray{
    
    if (!_cellSelectedArray) {
        _cellSelectedArray = [NSMutableArray array];
    }
    return _cellSelectedArray;
}

- (void)dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (UIImageView *)imageView {
    
	if(_imageView == nil) {
        _imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"back_wait"]];
        _imageView.backgroundColor = HEXCOLOR(0xF3F5F6);
        _imageView.contentMode = UIViewContentModeCenter;
        [self.view addSubview:_imageView];
	}
	return _imageView;
}

@end
