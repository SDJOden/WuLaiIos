//
//  WYWaitMsgVoiceCell.m
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/7/21.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import "WYWaitMsgVoiceCell.h"
#import "RCVoicePlayer.h"
#import "RCMessageModel.h"

NSString *const wyNotificationStopVoicePlayer = @"wyNotificationStopVoicePlayer";

@interface WYWaitMsgVoiceCell () <RCVoicePlayerObserver>

@property (nonatomic, strong) UIView *voiceBackgroundView;

@property (nonatomic, strong) UIImageView *voiceImageView;

@property(nonatomic, strong) RCVoicePlayer *voicePlayer;

@property(nonatomic) int animationIndex;

@property(nonatomic, strong) NSTimer *animationTimer;

@end

static long s_messageId = 0;

@implementation WYWaitMsgVoiceCell

-(instancetype)init{
    
    if (self = [super init]) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(resetActiveEventInBackgroundMode) name:wyNotificationStopVoicePlayer object:nil];
    }
    return self;
}

-(void)setModel:(RCMessageModel *)model{
    
    [super setModel:model];
    self.voicePlayer = [RCVoicePlayer defaultPlayer];
    
    self.voiceBackgroundView.frame = CGRectMake(0, 0, 90, 21);
    self.voiceImageView.frame = CGRectMake(7, 4, 9, 13);
}

- (void)startPlayingVoiceData {
    
    RCVoiceMessage *_voiceMessage = (RCVoiceMessage *)self.model.content;
    
    if (_voiceMessage.wavAudioData) {
        
        [self stopPlayingVoiceData];
        
        BOOL bPlay = [self.voicePlayer playVoice:[@(self.model.messageId) stringValue] voiceData:_voiceMessage.wavAudioData observer:self];
        if (!bPlay) {
            [self stopPlayingVoiceData];
            [self disableCurrentAnimationTimer];
        }
        s_messageId = self.model.messageId;
    } else {}
}

- (void)stopPlayingVoiceData {
    
    if (self.voicePlayer.isPlaying) {
        [self.voicePlayer stopPlayVoice];
    }
}

- (void)disableCurrentAnimationTimer {
    
    if (self.animationTimer && [self.animationTimer isValid]) {
        [self.animationTimer invalidate];
        self.animationTimer = nil;
        self.animationIndex = 0;
    }
}

- (UIView *)voiceBackgroundView {
    
	if(_voiceBackgroundView == nil) {
		_voiceBackgroundView = [[UIView alloc] init];
        _voiceBackgroundView.backgroundColor = HEXCOLOR(0xF0F0F0);
        _voiceBackgroundView.layer.cornerRadius = 21*0.5;
        _voiceBackgroundView.layer.masksToBounds = YES;
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTouchWaitVoiceCell)];
        [_voiceBackgroundView addGestureRecognizer:tapGesture];
        [self.messageContentView addSubview:_voiceBackgroundView];
	}
	return _voiceBackgroundView;
}

- (UIImageView *)voiceImageView {
    
	if(_voiceImageView == nil) {
		_voiceImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"voice"]];
        [self.voiceBackgroundView addSubview:_voiceImageView];
	}
	return _voiceImageView;
}

-(void)didTouchWaitVoiceCell{
    
    if (self.voicePlayer.isPlaying) {
        [self stopPlayingVoiceData];
        [self disableCurrentAnimationTimer];
    }
    else {
        [self startPlayingVoiceData];
    }
}

#pragma mark - stop and disable timer during background mode.
- (void)resetActiveEventInBackgroundMode {
    
    [self stopPlayingVoiceData];
    [self disableCurrentAnimationTimer];
}

#pragma mark RCVoicePlayerObserver
- (void)PlayerDidFinishPlaying:(BOOL)isFinish {
    
    if (isFinish) {
        [self disableCurrentAnimationTimer];
    }
}

- (void)audioPlayerDecodeErrorDidOccur:(NSError *)error {
    
    [self disableCurrentAnimationTimer];
}

-(void)dealloc{
    
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

@end
