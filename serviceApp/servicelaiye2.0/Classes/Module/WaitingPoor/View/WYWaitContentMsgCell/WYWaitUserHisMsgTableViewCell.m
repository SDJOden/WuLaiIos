//
//  WYWaitUserHisMsgTableViewCell.m
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/7/20.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import "WYWaitUserHisMsgTableViewCell.h"
#import "RCMessageModel.h"
#define CELL_HORIAONTAL_MARGIN 12
#define CELL_IMAGAEWIDTH 32
#define CELL_VERTICAL_MARGIN 8
#define CELL_MSGCONTENT_MARGIN 4

@interface WYWaitUserHisMsgTableViewCell ()

@property (nonatomic, strong) UIView *separateLineView;
@property (nonatomic, strong) UIButton *iconButton;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *timeLabel;
@property (nonatomic, strong)UIImage *defaultImageOne;
@property (nonatomic, strong)UIImage *defaultImageTwo;

@end

@implementation WYWaitUserHisMsgTableViewCell

- (instancetype)init {
    
    if (self = [super init]) {
        
        self.defaultImageOne = [UIImage getRoundImageWithSize:CGSizeMake(CELL_IMAGAEWIDTH, CELL_IMAGAEWIDTH) backgroundColor:[UIColor colorWithRed:0.7451 green:0.8784 blue:0.9451 alpha:1.0] lineWidth:0 cornerRadius:9.0 shouldFill:YES];
        self.defaultImageTwo = [UIImage getRoundImageWithSize:CGSizeMake(CELL_IMAGAEWIDTH, CELL_IMAGAEWIDTH) backgroundColor:[UIColor colorWithRed:1.0 green:0.9137 blue:0.6196 alpha:1.0] lineWidth:0 cornerRadius:9.0 shouldFill:YES];
    }
    
    return self;
}

-(void)layout{
    
    if (_isLastCell) {
        self.separateLineView.frame = CGRectMake(CELL_HORIAONTAL_MARGIN + CELL_IMAGAEWIDTH * 0.5, 0, 0.5, 10);
    }else {
        self.separateLineView.frame = CGRectMake(CELL_HORIAONTAL_MARGIN + CELL_IMAGAEWIDTH * 0.5, 0, 0.5, self.contentView.h);
    }
    self.iconButton.frame = CGRectMake(CELL_HORIAONTAL_MARGIN, CELL_VERTICAL_MARGIN, CELL_IMAGAEWIDTH, CELL_IMAGAEWIDTH);
    [self.nameLabel sizeToFit];
    self.nameLabel.frame = CGRectMake(CELL_HORIAONTAL_MARGIN * 2 + CELL_IMAGAEWIDTH, CELL_VERTICAL_MARGIN, SCREENWIDTH - CELL_IMAGAEWIDTH - CELL_HORIAONTAL_MARGIN * 3, 21);
    self.messageContentView.frame = CGRectMake(CELL_HORIAONTAL_MARGIN * 2 + CELL_IMAGAEWIDTH, self.nameLabel.y + self.nameLabel.h + CELL_MSGCONTENT_MARGIN, SCREENWIDTH - (CELL_HORIAONTAL_MARGIN * 2 + CELL_IMAGAEWIDTH) - CELL_HORIAONTAL_MARGIN, self.contentView.h - CELL_VERTICAL_MARGIN * 2 - 21 - CELL_MSGCONTENT_MARGIN * 2 - 16);
    [self.timeLabel sizeToFit];
    self.timeLabel.frame = CGRectMake(CELL_HORIAONTAL_MARGIN * 2 + CELL_IMAGAEWIDTH, self.messageContentView.y + self.messageContentView.h + CELL_MSGCONTENT_MARGIN, self.timeLabel.w, 16);
    
#warning to do 尽量少用sizetofit来进行布局计算, 
    [self.loveButton sizeToFit];
    self.loveButton.frame = CGRectMake(SCREENWIDTH - self.loveButton.w - 10, self.messageContentView.y + self.messageContentView.h + CELL_MSGCONTENT_MARGIN, self.loveButton.w, self.loveButton.h);
    [self.contentView sendSubviewToBack:self.separateLineView];
}

-(void)setModel:(RCMessageModel *)model{
    
    _model = model;
    RCMessageContent *messageContent = model.content;
    self.timeLabel.text = [RCKitUtility ConvertChatMessageTime:model.sentTime / 1000];
    if (MessageDirection_RECEIVE == _model.messageDirection) {
        [self.iconButton setTitle:@"" forState:UIControlStateNormal];
        [self.iconButton setBackgroundImage:_iconImage forState:UIControlStateNormal];
        self.nameLabel.text = _userNickName;
        self.loveButton.hidden = YES;
    } else {
        self.loveButton.hidden = NO;
#warning to do
#warning to do 抽取出相同代码进行封装, 代码可读性, 可维护性都会好一些.
        if ([messageContent isKindOfClass:[RCTextMessage class]]) {
            NSDictionary *dict = (NSDictionary *)((RCTextMessage *)messageContent).extra;
            if ([dict[@"genius_info"][@"genius_id"] intValue] != 1896209 && [dict[@"genius_info"][@"genius_id"] intValue] != 1896208 && [dict[@"genius_info"][@"genius_id"] intValue] != 1896207 && [dict[@"genius_info"][@"genius_id"] intValue] != 1896206) {
                self.nameLabel.text = dict[@"genius_info"][@"realname"];
                [self.iconButton setTitle:[dict[@"genius_info"][@"username"] substringFromIndex:6] forState:UIControlStateNormal];
                [self.iconButton setBackgroundImage:self.defaultImageOne forState:UIControlStateNormal];
            }else {
                self.nameLabel.text = @"AI";
                [self.iconButton setTitle:@"AI" forState:UIControlStateNormal];
                [self.iconButton setBackgroundImage:self.defaultImageTwo forState:UIControlStateNormal];
            }
        } else if ([messageContent isKindOfClass:[RCImageMessage class]]) {
            NSDictionary *dict = (NSDictionary *)((RCImageMessage *)messageContent).extra;
            if ([dict[@"genius_info"][@"genius_id"] intValue] != 1896209 && [dict[@"genius_info"][@"genius_id"] intValue] != 1896208 && [dict[@"genius_info"][@"genius_id"] intValue] != 1896207 && [dict[@"genius_info"][@"genius_id"] intValue] != 1896206) {
                self.nameLabel.text = dict[@"genius_info"][@"realname"];
                [self.iconButton setTitle:[dict[@"genius_info"][@"username"] substringFromIndex:6] forState:UIControlStateNormal];
                [self.iconButton setBackgroundImage:self.defaultImageOne forState:UIControlStateNormal];
            }else {
                self.nameLabel.text = @"AI";
                [self.iconButton setTitle:@"AI" forState:UIControlStateNormal];
                [self.iconButton setBackgroundImage:self.defaultImageTwo forState:UIControlStateNormal];
            }
        } else if ([messageContent isKindOfClass:[RCVoiceMessage class]]) {
            NSDictionary *dict = (NSDictionary *)((RCVoiceMessage *)messageContent).extra;
            if ([dict[@"genius_info"][@"genius_id"] intValue] != 1896209 && [dict[@"genius_info"][@"genius_id"] intValue] != 1896208 && [dict[@"genius_info"][@"genius_id"] intValue] != 1896207 && [dict[@"genius_info"][@"genius_id"] intValue] != 1896206) {
                self.nameLabel.text = dict[@"genius_info"][@"realname"];
                [self.iconButton setTitle:[dict[@"genius_info"][@"username"] substringFromIndex:6] forState:UIControlStateNormal];
                [self.iconButton setBackgroundImage:self.defaultImageOne forState:UIControlStateNormal];
            }else {
                self.nameLabel.text = @"AI";
                [self.iconButton setTitle:@"AI" forState:UIControlStateNormal];
                [self.iconButton setBackgroundImage:self.defaultImageTwo forState:UIControlStateNormal];
            }
        } else if ([messageContent isKindOfClass:[RCRichContentMessage class]]) {
            NSDictionary *dict = (NSDictionary *)((RCRichContentMessage *)messageContent).extra;
            if ([dict[@"genius_info"][@"genius_id"] intValue] != 1896209 && [dict[@"genius_info"][@"genius_id"] intValue] != 1896208 && [dict[@"genius_info"][@"genius_id"] intValue] != 1896207 && [dict[@"genius_info"][@"genius_id"] intValue] != 1896206) {
                self.nameLabel.text = dict[@"genius_info"][@"realname"];
                [self.iconButton setTitle:[dict[@"genius_info"][@"username"] substringFromIndex:6] forState:UIControlStateNormal];
                [self.iconButton setBackgroundImage:self.defaultImageOne forState:UIControlStateNormal];
            }else {
                self.nameLabel.text = @"AI";
                [self.iconButton setTitle:@"AI" forState:UIControlStateNormal];
                [self.iconButton setBackgroundImage:self.defaultImageTwo forState:UIControlStateNormal];
            }
        }
    }
    [self layout];
}

-(void)didTouchLoveButton{
    
    if ([self.wyWaitUserHisMsgTableViewCellDelegate respondsToSelector:@selector(pushToLoveViewController)]) {
        [self.wyWaitUserHisMsgTableViewCellDelegate pushToLoveViewController];
    }
}

#pragma mark --- 懒加载
- (UIButton *)iconButton {
    
    if(_iconButton == nil) {
        _iconButton = [[UIButton alloc] init];
        _iconButton.titleLabel.font = [UIFont boldSystemFontOfSize:12];
        [_iconButton setTitleColor:HEXCOLOR(0x666666) forState:UIControlStateNormal];
        _iconButton.layer.cornerRadius = 16.0;
        _iconButton.layer.masksToBounds = YES;
        [self.contentView addSubview:_iconButton];
    }
    return _iconButton;
}

- (UIView *)separateLineView {
    
	if(_separateLineView == nil) {
		_separateLineView = [[UIView alloc] init];
        _separateLineView.backgroundColor = HEXCOLOR(0xD8D8D8);
        [self.contentView addSubview:_separateLineView];
	}
	return _separateLineView;
}

- (UILabel *)nameLabel {
    
	if(_nameLabel == nil) {
		_nameLabel = [[UILabel alloc] init];
        _nameLabel.textColor = HEXCOLOR(0x666666);
        _nameLabel.font = [UIFont boldSystemFontOfSize:15];
        _nameLabel.textAlignment = NSTextAlignmentLeft;
        [self.contentView addSubview:_nameLabel];
	}
	return _nameLabel;
}

- (UIView *)messageContentView {
    
	if(_messageContentView == nil) {
		_messageContentView = [[UIView alloc] init];
        _messageContentView.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:_messageContentView];
	}
	return _messageContentView;
}

- (UILabel *)timeLabel {
    
	if(_timeLabel == nil) {
		_timeLabel = [[UILabel alloc] init];
        _timeLabel.textColor = HEXCOLOR(0xD3D3D3);
        _timeLabel.font = [UIFont systemFontOfSize:13];
        _timeLabel.textAlignment = NSTextAlignmentLeft;
        [self.contentView addSubview:_timeLabel];
	}
	return _timeLabel;
}

- (UIButton *)loveButton {
    
    if(_loveButton == nil) {
        _loveButton = [[UIButton alloc] init];
        [_loveButton setImage:[UIImage imageNamed:@"hisLove"] forState:UIControlStateNormal];
        [_loveButton setTitle:@"点赞" forState:UIControlStateNormal];
        [_loveButton setTitleColor:HEXCOLOR(0xC9C9C9) forState:UIControlStateNormal];
        _loveButton.titleLabel.font = [UIFont systemFontOfSize:13];
        [_loveButton addTarget:self action:@selector(didTouchLoveButton) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_loveButton];
    }
    return _loveButton;
}

@end
