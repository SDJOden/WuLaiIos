//
//  WYWaitMsgCardCell.m
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/7/23.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import "WYWaitMsgCardCell.h"

@interface WYWaitMsgCardCell ()

@property (nonatomic, strong) UILabel *contentLabel;

@end

@implementation WYWaitMsgCardCell

-(void)setModel:(RCMessageModel *)model{
    
    [super setModel:model];
    
    self.contentLabel.text = @"[卡片]";
    
    //------------------masonry和手动布局不要混用---------------
#warning to do
    [self.contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.bottom.equalTo(self.messageContentView);
    }];
}

- (UILabel *)contentLabel {
    
    if(_contentLabel == nil) {
        _contentLabel = [[UILabel alloc] init];
        _contentLabel.font = [UIFont systemFontOfSize:14];
        _contentLabel.textColor = HEXCOLOR(0x666666);
        _contentLabel.numberOfLines = 0;
        _contentLabel.textAlignment = NSTextAlignmentLeft;
        [self.messageContentView addSubview:_contentLabel];
    }
    return _contentLabel;
}

@end
