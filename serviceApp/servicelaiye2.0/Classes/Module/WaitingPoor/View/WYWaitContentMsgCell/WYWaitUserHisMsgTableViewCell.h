//
//  WYWaitUserHisMsgTableViewCell.h
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/7/20.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol WYWaitUserHisMsgTableViewCellDelegate <NSObject>

-(void)pushToLoveViewController;

@end

@interface WYWaitUserHisMsgTableViewCell : UITableViewCell

@property (nonatomic, strong) UIImage *iconImage;

@property (nonatomic, strong) NSString *userNickName;

@property (nonatomic, strong) RCMessageModel *model;

@property (nonatomic, strong) UIView *messageContentView;

@property (nonatomic, assign) BOOL isLastCell;

@property (nonatomic, strong) UIButton *loveButton;

@property (nonatomic, weak) id <WYWaitUserHisMsgTableViewCellDelegate> wyWaitUserHisMsgTableViewCellDelegate;

@end
