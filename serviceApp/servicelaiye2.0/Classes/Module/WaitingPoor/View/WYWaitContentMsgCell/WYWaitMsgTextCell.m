//
//  WYWaitMsgTextCell.m
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/7/21.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import "WYWaitMsgTextCell.h"
#import "RCMessageModel.h"

@interface WYWaitMsgTextCell ()

@property (nonatomic, strong) UILabel *contentLabel;

@end

@implementation WYWaitMsgTextCell

-(void)setModel:(RCMessageModel *)model{
    
    [super setModel:model];
    
    NSMutableString *realTextString = [NSMutableString stringWithFormat:@""];
    
    RCTextMessage *textMsg = (RCTextMessage *)model.content;
    NSString *text = textMsg.content;
    NSString *pattern = @"<a href=\'(.*?)\'>(.*?)</a>";
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern options:0 error:nil];
    NSArray *matches = [regex matchesInString:text options:0 range:NSMakeRange(0, text.length)]; // 网址
    NSUInteger lastIdx = 0;
    NSMutableString *attributedString = [NSMutableString stringWithFormat:@""]; // 总字符串
    
    if (matches.count) { // 解析部分bug, 1.文字需要提出来, 2. 位置需要调整 matchs.count
        NSMutableArray *urlArrs = [[NSMutableArray alloc] init]; // 存url
        int i = 0;
        for (NSTextCheckingResult* match in matches)
        {
            NSRange range = match.range;
            if (range.location > lastIdx)
            {
                NSString *temp = [text substringWithRange:NSMakeRange(lastIdx, range.location - lastIdx)];
                [attributedString appendString:temp];// 记录头
                [realTextString appendString:temp];
            }
            NSString *title = [text substringWithRange:[match rangeAtIndex:2]]; // 标题
            NSString *url = [text substringWithRange:[match rangeAtIndex:1]]; // 网址
            lastIdx = range.location + range.length;
            [attributedString appendFormat:@"<%d>%@</%d>",i,title,i];
            [realTextString appendString:title];
            [urlArrs addObject:url];
            i++;
        }
        if (lastIdx < text.length)
        {
            NSString *temp = [text substringFromIndex:lastIdx];
            [attributedString appendString:temp];// 记录尾
            [realTextString appendString:temp];
        }
    } else {
        [realTextString appendString:textMsg.content];
    }
    
    //----------------- masonry 和 手动布局不要混用, 可能会导致错位.
#warning to do
    self.contentLabel.text = realTextString;
    [self.contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.bottom.equalTo(self.messageContentView);
    }];
}

- (UILabel *)contentLabel {
    
    if(_contentLabel == nil) {
        _contentLabel = [[UILabel alloc] init];
        _contentLabel.font = [UIFont systemFontOfSize:14];
        _contentLabel.textColor = HEXCOLOR(0x666666);
        _contentLabel.numberOfLines = 0;
        _contentLabel.textAlignment = NSTextAlignmentLeft;
        [self.messageContentView addSubview:_contentLabel];
    }
    return _contentLabel;
}

@end
