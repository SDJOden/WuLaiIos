//
//  WYWaitMsgImageCell.m
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/7/21.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import "WYWaitMsgImageCell.h"
#import "RCMessageModel.h"

@interface WYWaitMsgImageCell ()

@property (nonatomic, strong) UIImageView *contentImageView;

@end

@implementation WYWaitMsgImageCell

-(void)setModel:(RCMessageModel *)model{
    
    [super setModel:model];
    
    RCImageMessage *imageMsg = (RCImageMessage *)self.model.content;
    
    [self.contentImageView sd_setImageWithURL:[NSURL URLWithString:imageMsg.imageUrl] placeholderImage:[UIImage imageNamed:@"placeholder"]];
    
    RCImageMessage *_imageMessage = (RCImageMessage *)model.content;
    CGSize imageSize = _imageMessage.thumbnailImage.size;
    //兼容240
    CGFloat imageWidth = 120;
    CGFloat imageHeight = 120;
    
    CGFloat imageWidthMin = 60;
    CGFloat imageHeightMin = 60;
    CGFloat imageWidthMax = [UIScreen mainScreen].bounds.size.width * 0.5/*0.8*/;
    CGFloat imageHeightMax = [UIScreen mainScreen].bounds.size.height * 0.3/*0.6*/;
    CGFloat scale = imageWidthMax * 1.0 / imageHeightMax;
    if (imageSize.width < imageWidthMin && imageSize.height < imageHeightMin) {
        if (imageSize.width >= imageSize.height) {
            if (imageSize.width * 1.0 / imageSize.height >= imageWidthMax * 1.0 / imageHeightMin) {
                imageWidth = imageWidthMax;
                imageHeight = imageSize.height *imageWidthMax * 1.0 / imageSize.width;
            } else {
                imageHeight = imageHeightMin;
                imageWidth = imageSize.width * imageHeightMin * 1.0 / imageSize.height;
            }
        } else {
            if (imageSize.height * 1.0 / imageSize.width >= imageHeightMax * 1.0 / imageWidthMin) {
                imageHeight = imageHeightMax;
                imageWidth = imageSize.width *imageHeightMax * 1.0 / imageSize.height;
            } else {
                imageWidth = imageWidthMin;
                imageHeight = imageSize.height * imageWidthMin * 1.0 / imageSize.width;
            }
        }
    } else if (imageSize.width > imageWidthMax || imageSize.height > imageHeightMax) {
        if (imageSize.width >= imageSize.height * scale) {
            imageWidth = imageWidthMax;
            imageHeight = imageSize.height *imageWidthMax * 1.0 / imageSize.width;
        } else {
            imageHeight = imageHeightMax;
            imageWidth = imageSize.width * imageHeightMax * 1.0 / imageSize.height;
        }
    } else {
        imageWidth = imageSize.width;
        imageHeight = imageSize.height;
    }
    //图片half
    imageSize = CGSizeMake(imageWidth, imageHeight);
    
    self.contentImageView.frame = CGRectMake(0, 0, imageWidth, imageHeight);
}

- (UIImageView *)contentImageView {
    
	if(_contentImageView == nil) {
		_contentImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"placeholder"]];
        [self.messageContentView addSubview:_contentImageView];
	}
	return _contentImageView;
}

@end
