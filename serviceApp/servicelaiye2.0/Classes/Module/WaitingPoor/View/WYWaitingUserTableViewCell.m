//
//  WYWaitingUserTableViewCell.m
//  Laiye
//
//  Created by 汪洋 on 15/12/10.
//  Copyright © 2015年 wangyang. All rights reserved.
//

#import "WYWaitingUserTableViewCell.h"
#import "WYWaitingUserData.h"
#import "WYWaitingUsersWechat.h"
#import "WYWaitingUsersRegion.h"

@interface WYWaitingUserTableViewCell()

@property (nonatomic, strong) UIImageView *vipImageView;
@property (nonatomic, strong) UIView *costSignView;
@property (nonatomic, strong) UILabel *domainLabel;
@property (nonatomic, strong) UIView *separateLineView;
@property (strong, nonatomic) UILabel *waitingTimeLabel;
@property (strong, nonatomic) UILabel *cityLabel;
@property (strong, nonatomic) UILabel *userMsgLabel;
@property (nonatomic, strong) UIView *iconViews;
@property (nonatomic, assign) int min;
@property (nonatomic, assign) int second;
@property (nonatomic, assign) CGFloat costX;
@property (nonatomic, assign) CGFloat x;
@property (nonatomic, assign) CGFloat nicknameLabel_Width;

@end

@implementation WYWaitingUserTableViewCell

-(instancetype)initWithFrame:(CGRect)frame{
    
    if (self = [super initWithFrame:frame]) {
        [self rcinit];
    }
    return self;
}

-(void)rcinit{
    
    _min = 0;
    _second = 0;
    _costX = 0;
    _x = 0;
    _nicknameLabel_Width = 0;
}

-(void)changeTime{
    
    [self.timer invalidate];
    dispatch_sync(dispatch_get_global_queue(0, 0), ^{
        self.timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(addTime) userInfo:nil repeats:YES];
        [[NSRunLoop currentRunLoop] addTimer:self.timer forMode:NSDefaultRunLoopMode];
    });
}

-(void)addTime{
    
    if (self.second == 60) {
        self.second = 0;
        self.min ++;
    }
    self.waitingTimeLabel.text = [NSString stringWithFormat:@"%02d'%02d\"", self.min, self.second ++];
}

-(void)setContentByModel:(WYWaitingUserData *)userData{
    
    [self rcinit];
    
    //头像
    if (![userData.wechat.headimgurl isKindOfClass:[NSNull class]]) {
        [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:userData.wechat.headimgurl] placeholderImage:[UIImage imageNamed:@"portrait_default"]];
    }
    else {
        [self.iconImageView setImage:[UIImage imageNamed:@"portrait_default"]];
    }
    //昵称
    if(userData.wechat.nickname != nil){//存在微信名显示微信名
        self.nicknameLabel.text = userData.wechat.nickname;
    }
    else{//不存在微信名
        if (![userData.nickname isKindOfClass:[NSNull class]]) {//显示昵称
            self.nicknameLabel.text = userData.nickname;
        }
        else {
            if (![userData.user_id isKindOfClass:[NSNull class]]) {
                self.nicknameLabel.text = [NSString stringWithFormat:@"#%@", userData.user_id.stringValue];//不存在昵称，显示id
            }
        }
    }
    
    [self.nicknameLabel sizeToFit];
    
    self.nicknameLabel_Width = self.nicknameLabel.w >= ([UIScreen mainScreen].bounds.size.width-13-44-13)*0.5?([UIScreen mainScreen].bounds.size.width-13-44-13)*0.5:self.nicknameLabel.w;
    
    if (![userData.msg_domain isKindOfClass:[NSNull class]]) {
        for (NSString *service_id in [[SDJUserAccount loadAccount].serviceIdsDic allKeys]) {
            if ([userData.msg_domain[@"service_id"]intValue] == service_id.intValue) {
                self.domainLabel.text = [SDJUserAccount loadAccount].serviceIdsDic[service_id];
                break;
            }
        }
    }else {
        self.domainLabel.text = @"其他";
    }
    
    
    if (userData.region.province.length != 0) {
        self.cityLabel.text = userData.region.province;//显示定位
    }else {
        self.cityLabel.text = @"";
    }
    
    
    self.waitingTimeLabel.text = [self computTime:userData.user_ts.stringValue];
    if (_min >= 100) {
        self.waitingTimeLabel.text = [NSString stringWithFormat:@"%d'", _min];
    }
    
    for (__strong UIView *view in self.iconViews.subviews) {
        [view removeFromSuperview];
        view = nil;
    }
    
    for (__strong UIView *view in self.costSignView.subviews) {
        [view removeFromSuperview];
        view = nil;
    }
    
    //VIP判断
    if([userData.vip isKindOfClass:[NSNull class]]) {
        self.vipImageView.hidden = YES;
    } else {
        self.vipImageView.hidden = NO;
        if ([userData.vip[@"vip_level"] intValue] == 1) {
            UILabel *label = [self createSignLabelWithFrame:CGRectMake(0, 0, 0, 0) andText:@"Svip" andTextColor:HEXCOLOR(0xE6A25F)];
            [label sizeToFit];
            label.frame = CGRectMake(_costX, 0, label.w, 14);
            [self.costSignView addSubview:label];
            _costX += label.w+4;
        }
        else if ([userData.vip[@"vip_level"] intValue] == 2) {
            self.vipImageView.image = [UIImage imageNamed:@"vip_month"];
        } else if ([userData.vip[@"vip_level"] intValue] == 3) {
            self.vipImageView.image = [UIImage imageNamed:@"vip_season"];
        } else if ([userData.vip[@"vip_level"] intValue] >= 4) {
            self.vipImageView.image = [UIImage imageNamed:@"vip_year"];
        }
    }
    
    if (userData.wechat.qr_user_id.integerValue != 0) {
        UILabel *label = [self createSignLabelWithFrame:CGRectMake(_x, 0, 16, 16) andText:@"社" andTextColor:HEXCOLOR(0xFF9000)];
        _x += label.w + 4;
        [self.iconViews addSubview:label];
    }
    
    for (NSString *tag in userData.up_tags) {
        
        UILabel *label = [[UILabel alloc]init];
        
        if ([tag isEqualToString:@"potential"]) {
            label = [self createSignLabelWithFrame:CGRectMake(_x, 0, 16, 16) andText:@"潜" andTextColor:HEXCOLOR(0x5AB2C7)];
        }
        else if ([tag isEqualToString:@"hq"]) {
            label = [self createSignLabelWithFrame:CGRectMake(_x, 0, 16, 16) andText:@"优" andTextColor:HEXCOLOR(0x26C42E)];
        }
        else if ([tag isEqualToString:@"debt"]) {
            label = [self createSignLabelWithFrame:CGRectMake(_x, 0, 16, 16) andText:@"欠" andTextColor:HEXCOLOR(0xF66363)];
        }
        else if ([tag isEqualToString:@"new"]) {
            label = [self createSignLabelWithFrame:CGRectMake(_x, 0, 16, 16) andText:@"新" andTextColor:HEXCOLOR(0x4F6ADA)];
        }
        else if ([tag isEqualToString:@"scar"]) {
            label = [self createSignLabelWithFrame:CGRectMake(0, 0, 0, 0) andText:@"Car" andTextColor:HEXCOLOR(0x555555)];
            label.font = [UIFont boldSystemFontOfSize:11];
            [label sizeToFit];
            label.frame = CGRectMake(_costX, 0, label.w+4, 14);
            [self.costSignView addSubview:label];
            _costX += label.w + 4;
            continue;
        }
        else {
            label = [self createSignLabelWithFrame:CGRectMake(_x, 0, 16, 16) andText:tag andTextColor:HEXCOLOR(0x000000)];
            [label sizeToFit];
            label.frame = CGRectMake(_x, 0, label.w+4, 16);
        }
        [self.iconViews addSubview:label];
        _x += label.w + 4;
    }
    _x -= 4;
    _costX -= 4;
    
    if (userData.user_msg.count == 2) {
        self.userMsgLabel.text = [userData.user_msg.firstObject stringByAppendingFormat:@" -- %@", userData.user_msg.lastObject];
    }
    else if (userData.user_msg.count == 1) {
        self.userMsgLabel.text = userData.user_msg.firstObject;
    }else {
        self.userMsgLabel.text = @"";
    }
    [self layout];
}

//计算距离当前时间
-(NSString *)computTime:(NSString *)seconds{
    
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:seconds.doubleValue];
    NSTimeInterval time = (-1) * [date timeIntervalSinceNow];
    self.min = (int)time / 60;
    self.second = (int)time % 60;
    NSString *minStr = @(self.min).stringValue;
    NSString *secondsStr = @(self.second).stringValue;
    return [NSString stringWithFormat:@"%02d'%02d\"", minStr.intValue, secondsStr.intValue];
}

//布局子视图
-(void)layout{
    
    self.iconImageView.frame = CGRectMake(13, 10, 44, 44);
    
    self.vipImageView.frame = CGRectMake(13 + 22 - 18, 10 + 44 - 16, 36, 16);
    //-------------------①空格 ②布局方式-------------------
#warning to do
    self.nicknameLabel.frame = CGRectMake(13+44+13, 8.5, _nicknameLabel_Width, 20);
    
    self.costSignView.frame = CGRectMake(self.nicknameLabel.x+self.nicknameLabel.w+10, 8.5+10-7, _costX, 14);
    
    self.iconViews.frame = CGRectMake([UIScreen mainScreen].bounds.size.width-19-_x, 8.5+2, _x, 16);
    
    [self.domainLabel sizeToFit];
    self.domainLabel.frame = CGRectMake(self.iconImageView.x+self.iconImageView.w+13, self.nicknameLabel.y+self.nicknameLabel.h+3, self.domainLabel.w, 24);
    
    self.separateLineView.frame = CGRectMake(self.domainLabel.x+self.domainLabel.w+9.5, self.nicknameLabel.y+self.nicknameLabel.h+3, 0.5, 24);
    
    [self.cityLabel sizeToFit];
    self.cityLabel.frame = CGRectMake([UIScreen mainScreen].bounds.size.width-19-self.cityLabel.w, 8.5+20+3+(24-19)*0.5, self.cityLabel.w, 19);
    
    [self.waitingTimeLabel sizeToFit];
    self.waitingTimeLabel.frame = CGRectMake(self.domainLabel.x+self.domainLabel.w+20, self.domainLabel.y, 100, 24);
    
    self.userMsgLabel.frame = CGRectMake(13+44+13, 8.5+20+3+24+3, [UIScreen mainScreen].bounds.size.width-19-13-44-13, 20);
}

- (UIImageView *)iconImageView {
    
    if(_iconImageView == nil) {
        _iconImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"portrait_default"]];
        _iconImageView.layer.masksToBounds = YES;
        _iconImageView.layer.cornerRadius = 22.0;
        _iconImageView.layer.borderColor = [[UIColor lightGrayColor]CGColor];
        _iconImageView.layer.borderWidth = 0.5;
        [self.contentView addSubview:_iconImageView];
    }
    return _iconImageView;
}

- (UILabel *)nicknameLabel {
    
    if(_nicknameLabel == nil) {
        _nicknameLabel = [[UILabel alloc] init];
        _nicknameLabel.font = [UIFont boldSystemFontOfSize:14];
        _nicknameLabel.textColor = HEXCOLOR(0x666666);
        [self.contentView addSubview:_nicknameLabel];
    }
    return _nicknameLabel;
}

- (UILabel *)cityLabel {
    
    if(_cityLabel == nil) {
        _cityLabel = [[UILabel alloc] init];
        _cityLabel.textColor = HEXCOLOR(0x666666);
        _cityLabel.font = [UIFont systemFontOfSize:14];
        _cityLabel.textAlignment = NSTextAlignmentRight;
        [self.contentView addSubview:_cityLabel];
    }
    return _cityLabel;
}

- (UILabel *)waitingTimeLabel {
    
    if(_waitingTimeLabel == nil) {
        _waitingTimeLabel = [[UILabel alloc] init];
        _waitingTimeLabel.text = [NSString stringWithFormat:@"%02d'%02d\"", _min, _second];
        _waitingTimeLabel.font = [UIFont boldSystemFontOfSize:17];
        _waitingTimeLabel.textColor = HEXCOLOR(0xFF4949);
        _waitingTimeLabel.textAlignment = NSTextAlignmentLeft;
        [self.contentView addSubview:_waitingTimeLabel];
    }
    return _waitingTimeLabel;
}

- (UILabel *)userMsgLabel {
    
    if(_userMsgLabel == nil) {
        _userMsgLabel = [[UILabel alloc] init];
        _userMsgLabel.textColor = [UIColor lightGrayColor];
        _userMsgLabel.font = [UIFont systemFontOfSize:12];
        [self.contentView addSubview:_userMsgLabel];
    }
    return _userMsgLabel;
}

- (UIView *)iconViews {
    
    if(_iconViews == nil) {
        _iconViews = [[UIView alloc] init];
        [self.contentView addSubview:_iconViews];
    }
    return _iconViews;
}

-(void)dealloc{
    
    [self.timer invalidate];
    self.timer = nil;
}

- (UILabel *)domainLabel {
    
	if(_domainLabel == nil) {
		_domainLabel = [[UILabel alloc] init];
        _domainLabel.textColor = HEXCOLOR(0x878787);
        _domainLabel.font = [UIFont systemFontOfSize:17];
        _domainLabel.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:_domainLabel];
	}
	return _domainLabel;
}

- (UIImageView *)vipImageView {
    
	if(_vipImageView == nil) {
		_vipImageView = [[UIImageView alloc] init];
        [self.contentView addSubview:_vipImageView];
	}
	return _vipImageView;
}

- (UIView *)costSignView {
    
	if(_costSignView == nil) {
		_costSignView = [[UIView alloc] init];
        [self.contentView addSubview:_costSignView];
	}
	return _costSignView;
}

-(UILabel *)createSignLabelWithFrame:(CGRect)frame andText:(NSString *)text andTextColor:(UIColor *)color{
    
    UILabel *label = [[UILabel alloc]initWithFrame:frame];
    label.backgroundColor = [UIColor clearColor];
    label.text = text;
    label.textColor = color;
    label.font = [UIFont systemFontOfSize:11];
    label.textAlignment = NSTextAlignmentCenter;
    [label.layer setMasksToBounds:YES];
    [label.layer setCornerRadius:2.0]; //设置矩圆角半径
    label.layer.borderWidth = 1.0;
    label.layer.borderColor = [color CGColor];
    return label;
}

- (UIView *)separateLineView {
    
	if(_separateLineView == nil) {
		_separateLineView = [[UIView alloc] init];
        _separateLineView.backgroundColor = HEXCOLOR(0x878787);
        [self.contentView addSubview:_separateLineView];
	}
	return _separateLineView;
}

@end
