//
//  WYWaitingUserTableViewCell.h
//  Laiye
//
//  Created by 汪洋 on 15/12/10.
//  Copyright © 2015年 wangyang. All rights reserved.
//

#import <UIKit/UIKit.h>

@class WYWaitingUserTableViewCell;

@class WYHistoryModel;

@protocol WYWaitingUserTableViewCellDelegate <NSObject>

@optional

//-(void)nextBtnClickWithImage:(UIImage *)image andHistoryModel:(WYHistoryModel *)historyModel;

@end

@class WYWaitingUserData;

@interface WYWaitingUserTableViewCell : UITableViewCell

@property (strong, nonatomic) UIImageView *iconImageView;

@property (strong, nonatomic) UILabel *nicknameLabel;

-(void)setContentByModel:(WYWaitingUserData *)userData;

-(void)changeTime;

-(void)layoutLeftChange;

-(void)layoutRightChange;

@property (nonatomic, strong) NSTimer *timer;

@property(nonatomic,weak)id <WYWaitingUserTableViewCellDelegate> waitingUserTableViewCellDelegate;

@end
