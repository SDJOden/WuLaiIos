//
//  WYAlertView.h
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/1/7.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WYAlertView : UIView

-(void)setAlertTitleLb:(NSString *)title andAlertContentLb:(NSString *)content;

@end
