//
//  WYAlertView.m
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/1/7.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import "WYAlertView.h"

@interface WYAlertView ()

@property (nonatomic, strong) UILabel *alertTitleLb;

@property (nonatomic, strong) UILabel *alertContentLb;

@end

@implementation WYAlertView

-(void)setAlertTitleLb:(NSString *)title andAlertContentLb:(NSString *)content{
    
    self.alertTitleLb.text = title;
    self.alertContentLb.text = content;
}

-(void)layoutSubviews{
    
    [super layoutSubviews];
    
    [self.alertTitleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self);
        make.height.equalTo(20);
    }];
    
    [self.alertContentLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.alertTitleLb.bottom);
        make.left.right.equalTo(self);
        make.height.equalTo(40);
    }];
}

- (UILabel *)alertTitleLb {
    
    if(_alertTitleLb == nil) {
        _alertTitleLb = [[UILabel alloc] init];
        _alertTitleLb.textColor = [UIColor whiteColor];
        _alertTitleLb.font = [UIFont systemFontOfSize:10];
        [self addSubview:_alertTitleLb];
    }
    return _alertTitleLb;
}

- (UILabel *)alertContentLb {
    
    if(_alertContentLb == nil) {
        _alertContentLb = [[UILabel alloc] init];
        _alertContentLb.textColor = [UIColor whiteColor];
        _alertContentLb.font = [UIFont systemFontOfSize:14];
        _alertContentLb.textAlignment = NSTextAlignmentCenter;
        [self addSubview:_alertContentLb];
    }
    return _alertContentLb;
}

@end
