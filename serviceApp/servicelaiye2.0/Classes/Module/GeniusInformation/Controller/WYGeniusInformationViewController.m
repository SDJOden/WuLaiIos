//
//  WYGeniusInformationViewController.m
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/4/7.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import "WYGeniusInformationViewController.h"
#import "WYNavigationViewController.h"
#import "WYConnectViewController.h"
#import "WYLogStatuView.h"
#import "WYServiceStatuView.h"
#import "WYGeniusInfomationHeaderView.h"
#import "WYServiceStatuModel.h"
#import "WYNetworkTools.h"

@interface WYGeniusInformationViewController () <WYLogStatuViewDelegate>

@property (nonatomic, strong) WYGeniusInfomationHeaderView *geniusHeaderView;

@property (nonatomic, strong) WYLogStatuView *logStatuView;

@property (nonatomic, strong) WYServiceStatuView *serviceStatuView;

@property (nonatomic, strong) NSArray *serviceArr;

@property (nonatomic, strong) UILabel *titleLabel;

@end

@implementation WYGeniusInformationViewController

-(instancetype)init{
    
    if (self = [super init]) {
        self.view.backgroundColor = [UIColor colorWithRed:0.949 green:0.949 blue:0.949 alpha:1.0];
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.logStatuView rcinitWithStatus:[SDJUserAccount loadAccount].logStatu];
    [self requestServiceData];
    [self.serviceStatuView resetCell:self.serviceArr];
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
    
    [self servicesChange];
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self addNavigationItem];
    
    [self addSubViews];
}

-(void)requestServiceData{
    NSMutableArray *statusArr = [NSMutableArray array];
    [[WYNetworkTools shareTools]requestServiceCategoryFinished:^(id responseObject, NSError *error) {
        if (![responseObject[@"error"] isEqual:[NSNull null]]) {//报错
            NSNumber *code = responseObject[@"error"][@"error_code"];
            if (code.integerValue == 20000) {
                NSString *UserPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).firstObject stringByAppendingPathComponent:@"account.plist"];
                [[NSFileManager defaultManager]removeItemAtPath:UserPath error:nil];
                //其他平台登录，token改变需要先登出融云
                [[RCIMClient sharedRCIMClient] logout];
                // 发送通知让appdelegate去切换根视图为聊天列表界面
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"WYRootViewControllerSwitchNotification" object:@"0"];
                });
            }
        }
        else {
            for (NSString *keyStr in [SDJUserAccount loadAccount].serviceIdsDic.allKeys) {
                WYServiceStatuModel *statuModel = [[WYServiceStatuModel alloc]init];
                statuModel.serviceStatu = keyStr.integerValue;
                statuModel.serviceName = [SDJUserAccount loadAccount].serviceIdsDic[keyStr];
                statuModel.isSelected = NO;
                [statusArr addObject:statuModel];
                for (NSString *serviceId in responseObject[@"data"][@"service_list"]) {
                    if (keyStr.integerValue == serviceId.integerValue) {
                        statuModel.isSelected = YES;
                        break;
                    }
                }
            }
            
            NSArray *returnArr = [[statusArr copy] sortedArrayUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"serviceStatu" ascending:YES]]];
            if (((NSArray *)responseObject[@"data"][@"service_list"]).count == 0) {
                ((WYServiceStatuModel *)returnArr[0]).isSelected = YES;
            }
            self.serviceArr = returnArr;
            [self.serviceStatuView modelWithArray:self.serviceArr];
        }
    }];
}


/**
 *  增加导航按钮
 */
-(void)addNavigationItem{
    
    self.navigationItem.leftBarButtonItem = nil;
    UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightBtn setTitle:@"退出登录" forState:UIControlStateNormal];
    [rightBtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    [rightBtn setTitleColor:[UIColor colorWithRed:0.6579 green:0.6579 blue:0.6579 alpha:1.0] forState:UIControlStateHighlighted];
    rightBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    rightBtn.frame = CGRectMake(0, 0, 60, 30);
    [rightBtn addTarget:self action:@selector(logoutGenius) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithCustomView:rightBtn];
    self.navigationItem.rightBarButtonItem = rightItem;
}

-(void)logoutGenius{
    
    [MobClick event:@"my_logout_click"];
    WYConnectViewController *conVC = ((WYNavigationViewController *)self.tabBarController.childViewControllers[0]).childViewControllers[0];
    NSString *waringStr = conVC.dataArray.count == 0 ? @"要登出了吗，要注意好好休息哦^_^":[NSString stringWithFormat:@"目前已接入列表中存在%ld个用户，登出将关闭用户对话，是否继续登出?", conVC.dataArray.count];
    //提示框询问助理是否要登出账号
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:waringStr preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"登出" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self logoutNetworkRequest];
    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:okAction];
    [alertController addAction:cancelAction];
    [self presentViewController:alertController animated:YES completion:nil];
    
}

-(void)logoutNetworkRequest{
    
    [[SDJNetworkTools sharedTools]logout:[SDJUserAccount loadAccount].token finished:^(NSDictionary<NSString *,id> * _Nullable result, NSError * _Nullable error) {
        if (![result[@"error"] isEqual:[NSNull null]]) {//异端登陆
            NSNumber *code = result[@"error"][@"error_code"];
            if (code.integerValue == 20000) {
                NSString *UserPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).firstObject stringByAppendingPathComponent:@"account.plist"];
                [[NSFileManager defaultManager]removeItemAtPath:UserPath error:nil];
                [[RCIMClient sharedRCIMClient] logout];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"WYRootViewControllerSwitchNotification" object:@"0"];
                });
            }
        }
        else {
            [[RCIMClient sharedRCIMClient] logout];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[NSNotificationCenter defaultCenter] postNotificationName:@"WYRootViewControllerSwitchNotification" object:@"0"];
            });
        }
    }];
}

-(void)addSubViews{
    
    [self.geniusHeaderView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self.view);
        make.height.equalTo(70);
    }];
    
    [self.logStatuView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.geniusHeaderView.bottom).offset(10);
        make.left.right.equalTo(self.view);
        make.height.equalTo(30+44);
    }];
    
    [self.serviceStatuView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.logStatuView.bottom).offset(10);
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.view).offset(-10);
    }];
}

/**
 *  改变登陆状态
 */
-(void)buttonClickWithButtonTag:(NSInteger)tag{
    
    [self changeStatu:tag];
}

-(void)changeStatu:(NSInteger)statu{
    
    __weak typeof(self)__weakSelf = self;
    [[SDJNetworkTools sharedTools]changeState:[SDJUserAccount loadAccount].token limit:statu finished:^(NSDictionary<NSString *,id> * _Nullable result, NSError * _Nullable error) {
        if (![result[@"error"] isEqual:[NSNull null]]) {//报错
            NSNumber *code = result[@"error"][@"error_code"];
            if (code.integerValue == 20000) {
                NSString *UserPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).firstObject stringByAppendingPathComponent:@"account.plist"];
                [[NSFileManager defaultManager]removeItemAtPath:UserPath error:nil];
                //其他平台登录，token改变需要先登出融云
                [[RCIMClient sharedRCIMClient] logout];
                // 发送通知让appdelegate去切换根视图为聊天列表界面
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"WYRootViewControllerSwitchNotification" object:@"0"];
                });
                
            }
        }
        else {
            [SDJUserAccount loadAccount].logStatu = statu;
            [[SDJUserAccount loadAccount] savaAccount];
            [__weakSelf.logStatuView rcinitWithStatus:statu];
        }
    }];
}

-(void)servicesChange{
    
    NSString *service = [self getServiceValue];
    
    __weak typeof(self) __weakSelf = self;
    [[SDJNetworkTools sharedTools]updateServiceTypes:[SDJUserAccount loadAccount].token service_ids:service finished:^(NSDictionary<NSString *,id> * _Nullable result, NSError * _Nullable error) {
        if (![result[@"error"] isEqual:[NSNull null]]) {//报错
            NSNumber *code = result[@"error"][@"error_code"];
            if (code.integerValue == 20000) {
                NSString *UserPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).firstObject stringByAppendingPathComponent:@"account.plist"];
                [[NSFileManager defaultManager]removeItemAtPath:UserPath error:nil];
                //其他平台登录，token改变需要先登出融云
                [[RCIMClient sharedRCIMClient] logout];
                // 发送通知让appdelegate去切换根视图为聊天列表界面
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"WYRootViewControllerSwitchNotification" object:@"0"];
                });
            }
        }
        else {
            [__weakSelf.serviceStatuView changeTextWithArr:self.serviceArr];
        }
    }];
}

/**
 *  获得状态的详情
 */
-(NSString *)getServiceValue{
    
    self.serviceArr = [self.serviceStatuView getServiceViewArr];
    
    NSString *str = @"";
    for (int i = 0; i < self.serviceArr.count; i ++) {
        WYServiceStatuModel *statuModel = self.serviceArr[i];
        if (statuModel.isSelected) {
            if (i == 0) {
                return @"-1";
            }
            else {
                str = [NSString stringWithFormat:@"%@,%ld", str, statuModel.serviceStatu];
            }
        }
    }
    if (str.length > 0) {
        str = [str substringFromIndex:1];
    }
    else {
        str = @"-1";
    }
    return str;
}

#pragma mark --- 懒加载
- (WYGeniusInfomationHeaderView *)geniusHeaderView {
    
    if(_geniusHeaderView == nil) {
        _geniusHeaderView = [[WYGeniusInfomationHeaderView alloc] init];
        [self.view addSubview:_geniusHeaderView];
    }
    return _geniusHeaderView;
}

- (WYLogStatuView *)logStatuView {
    
    if(_logStatuView == nil) {
        _logStatuView = [[WYLogStatuView alloc] init];
        [_logStatuView rcinitWithStatus:[SDJUserAccount loadAccount].logStatu];
        _logStatuView.logStatuViewDelegate = self;
        [self.view addSubview:_logStatuView];
    }
    return _logStatuView;
}

- (WYServiceStatuView *)serviceStatuView {
    
    if(_serviceStatuView == nil) {
        _serviceStatuView = [WYServiceStatuView modelWithServiceArr:self.serviceArr];
        [self.view addSubview:_serviceStatuView];
    }
    return _serviceStatuView;
}

- (UILabel *)titleLabel {
    
    if(_titleLabel == nil) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.text = @"接单品类";
        _titleLabel.font = [UIFont systemFontOfSize:14];
        [self.view addSubview:_titleLabel];
    }
    return _titleLabel;
}

//- (NSArray *)serviceArr {
//
//    if(_serviceArr == nil) {
//        _serviceArr = [WYServiceStatuModel arrayOfServiceIds];
//    }
//    return _serviceArr;
//}

@end
