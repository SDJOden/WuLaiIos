//
//  WYGeniusInfomationHeaderView.m
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/4/7.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import "WYGeniusInfomationHeaderView.h"

@interface WYGeniusInfomationHeaderView ()

@property (nonatomic, strong) UIImageView *iconImageView;

@property (nonatomic, strong) UILabel *nameLabel;

@property (nonatomic, strong) UILabel *geniusLabel;

@property (nonatomic, strong) UILabel *versionLabel;

@property (nonatomic, strong) UIView *lineView;

@end

@implementation WYGeniusInfomationHeaderView

- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize {
    
    [self.iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(10);
        make.left.equalTo(20);
        make.width.height.equalTo(50);
    }];
    
    [self.nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(15);
        make.left.equalTo(self.iconImageView.right).offset(10);
        make.height.equalTo(20);
    }];
    
    [self.geniusLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.nameLabel.bottom);
        make.left.equalTo(self.iconImageView.right).offset(10);
        make.height.equalTo(20);
    }];
    
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.left.right.equalTo(self);
        make.height.equalTo(0.5);
    }];
    
    [self.versionLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.nameLabel.top);
        make.right.equalTo(self).offset(-20);
        make.height.equalTo(20);
    }];
}


- (UIImageView *)iconImageView {
    
    if(_iconImageView == nil) {
        _iconImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"chatroom_userIcon_xiaolai"]];
        _iconImageView.layer.borderWidth = 0.7;
        _iconImageView.layer.borderColor = [[UIColor grayColor]CGColor];
        [self addSubview:_iconImageView];
    }
    return _iconImageView;
}

- (UILabel *)nameLabel {
    
    if(_nameLabel == nil) {
        _nameLabel = [[UILabel alloc] init];
        _nameLabel.font = [UIFont boldSystemFontOfSize:14];
        _nameLabel.text = [SDJUserAccount loadAccount].real_name;
//        CGSize textSize = [_nameLabel.text boundingRectWithSize:CGSizeMake(0, 0) options:NSStringDrawingTruncatesLastVisibleLine attributes:nil context:nil].size;
//        _nameLabel.frame = CGRectMake(0, 0, textSize.width, textSize.height);
        [self addSubview:_nameLabel];
    }
    return _nameLabel;
}

- (UILabel *)geniusLabel {
    
    if(_geniusLabel == nil) {
        _geniusLabel = [[UILabel alloc] init];
        _geniusLabel.font = [UIFont systemFontOfSize:12];
        _geniusLabel.text = [SDJUserAccount loadAccount].username;
//        CGSize textSize = [_geniusLabel.text boundingRectWithSize:CGSizeMake(0, 0) options:NSStringDrawingTruncatesLastVisibleLine attributes:nil context:nil].size;
//        _geniusLabel.frame = CGRectMake(0, 0, textSize.width, textSize.height);
        [self addSubview:_geniusLabel];
    }
    return _geniusLabel;
}

- (UIView *)lineView {
    
	if(_lineView == nil) {
		_lineView = [[UIView alloc] init];
        _lineView.backgroundColor = [UIColor grayColor];
        [self addSubview:_lineView];
	}
	return _lineView;
}

- (UILabel *)versionLabel {
    
	if(_versionLabel == nil) {
		_versionLabel = [[UILabel alloc] init];
        _versionLabel.font = [UIFont systemFontOfSize:12];
        _versionLabel.text = [SDJUserAccount loadAccount].username;
        _versionLabel.text = [NSString stringWithFormat:@"版本号 : %@ (build : %@)", [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"], [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"]];
//        CGSize textSize = [_versionLabel.text boundingRectWithSize:CGSizeMake(0, 0) options:NSStringDrawingTruncatesLastVisibleLine attributes:nil context:nil].size;
//        _versionLabel.frame = CGRectMake(0, 0, textSize.width, textSize.height);
        [self addSubview:_versionLabel];
	}
	return _versionLabel;
}

@end
