//
//  WYCategoryViewController.m
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/6/2.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import "WYCategoryViewController.h"
#import "WYServiceStatuCollectionViewCell.h"
#import "WYLoveTypeModel.h"

@interface WYCategoryViewController () <UICollectionViewDelegate, UICollectionViewDataSource>

@property (nonatomic, strong) UIView *navigationTitleView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIButton *backButton;
@property (nonatomic, strong) UICollectionView *serviceCollectionView;
@property (nonatomic, strong) UICollectionViewFlowLayout *customFlowLayout;
@property (nonatomic, strong) NSArray *serviceViewArr;
@property (nonatomic, copy) NSString *categoryStr;

@end

@implementation WYCategoryViewController

-(instancetype)initWithCategoryArray:(NSArray *)array andCategory:(NSString *)category{
    if (self = [super init]) {
        self.serviceViewArr = array;
        [self rcinit];
        [self addSubViews];
#warning to do 代码可读性差, 这个处理不该放在init里面
//        for (int i = 0; i < self.serviceViewArr.count;i ++) {
//            WYLoveTypeModel *loveTypeModel = self.serviceViewArr[i];
//            if (loveTypeModel.selected == YES) {
//                [self.serviceCollectionView selectItemAtIndexPath:[NSIndexPath indexPathForItem:i inSection:0] animated:YES scrollPosition:UICollectionViewScrollPositionNone];
//            }
//        }
    }
    return self;
}

-(void)viewDidLoad{
    
    [super viewDidLoad];
}

-(void)rcinit{
    self.view.backgroundColor = [UIColor whiteColor];
    self.categoryStr = @"";
    
    for (int i = 0; i < self.serviceViewArr.count;i ++) {
        WYLoveTypeModel *loveTypeModel = self.serviceViewArr[i];
        if (loveTypeModel.selected == YES) {
            [self.serviceCollectionView selectItemAtIndexPath:[NSIndexPath indexPathForItem:i inSection:0] animated:YES scrollPosition:UICollectionViewScrollPositionNone];
        }
    }
}

-(void)addSubViews{
    self.navigationTitleView.frame = CGRectMake(0, 0, SCREENWIDTH, 64);
    self.backButton.frame = CGRectMake(16, 64 - 18.5 - 10, 18.5, 18.5);
    [self.titleLabel sizeToFit];
    self.titleLabel.frame = CGRectMake((SCREENWIDTH - self.titleLabel.w) * 0.5, 64 - self.titleLabel.h - 8, self.titleLabel.w, self.titleLabel.h);
    self.serviceCollectionView.frame = CGRectMake(self.view.x + 20, self.view.y + 64 + 20, self.view.w - 40, self.view.h - 40 - 64);
}

-(void)back{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark --- UICollectionViewDelgate
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return self.serviceViewArr.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    WYServiceStatuCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"serviceStatuCell" forIndexPath:indexPath];
    [cell createWithModel:self.serviceViewArr[indexPath.row]];
    return cell;
}

#pragma mark --- UICollectionViewDelegateFlowLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    return CGSizeMake((SCREENWIDTH - 3 * 10 - 2 * 20 - 1) / 4, 31);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    //修改当前选中的模型和cell样式
    WYLoveTypeModel *statuModel = self.serviceViewArr[indexPath.row];
    [self addPoint:statuModel.type];
    statuModel.selected = YES;
    WYServiceStatuCollectionViewCell *cell = (WYServiceStatuCollectionViewCell*)[collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:indexPath.row inSection:0]];
    [cell createWithModel:statuModel];
    if ([self.categoryVCDelegate respondsToSelector:@selector(changeCategory:)]) {
        [self.categoryVCDelegate changeCategory:[cell getLabelText]];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    WYLoveTypeModel *statuModel = self.serviceViewArr[indexPath.row];
    [self addPoint:statuModel.type];
    statuModel.selected = NO;
    
#warning to do
    //---------------------少用强转写法,---------------------------
    [((WYServiceStatuCollectionViewCell *)[collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:indexPath.row inSection:0]]) createWithModel:statuModel];
}

- (UICollectionView *)serviceCollectionView {
    
    if(_serviceCollectionView == nil) {
        _customFlowLayout = [[UICollectionViewFlowLayout alloc] init];
        _customFlowLayout.minimumLineSpacing = 20.0f;
        _customFlowLayout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
        _customFlowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
        _serviceCollectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:_customFlowLayout];
        _serviceCollectionView.showsVerticalScrollIndicator = NO;
        _serviceCollectionView.delegate = self;
        _serviceCollectionView.dataSource = self;
        _serviceCollectionView.backgroundColor = [UIColor clearColor];
        [_serviceCollectionView registerClass:[WYServiceStatuCollectionViewCell class] forCellWithReuseIdentifier:@"serviceStatuCell"];
        [self.view addSubview:_serviceCollectionView];
    }
    return _serviceCollectionView;
}

- (UIView *)navigationTitleView {
    
    if(_navigationTitleView == nil) {
        _navigationTitleView = [[UIView alloc] init];
        _navigationTitleView.backgroundColor = NAVIGATIONBAR_BACKGROUDCOLOR;
        [self.view addSubview:_navigationTitleView];
    }
    return _navigationTitleView;
}

- (UILabel *)titleLabel {
    
	if(_titleLabel == nil) {
		_titleLabel = [[UILabel alloc] init];
        _titleLabel.textColor = HEXCOLOR(0xffffff);
        _titleLabel.font = [UIFont systemFontOfSize:16];
        _titleLabel.text = @"选择品类";
        [self.navigationTitleView addSubview:_titleLabel];
	}
	return _titleLabel;
}

- (UIButton *)backButton {
    
	if(_backButton == nil) {
		_backButton = [[UIButton alloc] init];
        [_backButton setBackgroundImage:[UIImage imageNamed:@"fork"] forState:UIControlStateNormal];
        [_backButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
        [self.navigationTitleView addSubview:_backButton];
	}
	return _backButton;
}

-(void)addPoint:(NSString *)serviceName{
    
#warning to do
    //---------------用本地plist, 方便拓展, 以及后期转动态,-----------------
    if ([serviceName isEqualToString:@"不限"]) {
        [MobClick event:@"love_none_click"];
    } else if ([serviceName isEqualToString:@"用车"]) {
        [MobClick event:@"love_taxi_click"];
    } else if ([serviceName isEqualToString:@"外卖"]) {
        [MobClick event:@"love_orderTakeout_click"];
    } else if ([serviceName isEqualToString:@"代买"]) {
        [MobClick event:@"love_insteadBuy_click"];
    } else if ([serviceName isEqualToString:@"电影"]) {
        [MobClick event:@"love_movie_click"];
    } else if ([serviceName isEqualToString:@"保洁"]) {
        [MobClick event:@"love_clean_click"];
    } else if ([serviceName isEqualToString:@"代驾"]) {
        [MobClick event:@"love_designatedDriver_click"];
    } else if ([serviceName isEqualToString:@"美甲"]) {
        [MobClick event:@"love_manicure_click"];
    } else if ([serviceName isEqualToString:@"推拿"]) {
        [MobClick event:@"love_massage_click"];
    } else if ([serviceName isEqualToString:@"通用"]) {
        [MobClick event:@"love_normal_click"];
    } else if ([serviceName isEqualToString:@"机票"]) {
        [MobClick event:@"love_planeTicket_click"];
    } else if ([serviceName isEqualToString:@"车票"]) {
        [MobClick event:@"love_trainTicket_click"];
    } else if ([serviceName isEqualToString:@"酒店"]) {
        [MobClick event:@"love_hotel_click"];
    } else if ([serviceName isEqualToString:@"快递"]) {
        [MobClick event:@"love_express_click"];
    } else if ([serviceName isEqualToString:@"汽车"]) {
        [MobClick event:@"love_car_click"];
    } else if ([serviceName isEqualToString:@"订座"]) {
        [MobClick event:@"love_Seat_click"];
    } else if ([serviceName isEqualToString:@"鲜花"]) {
        [MobClick event:@"love_flower_click"];
    } else if ([serviceName isEqualToString:@"维修"]) {
        [MobClick event:@"love_fix_click"];
    } else if ([serviceName isEqualToString:@"送药"]) {
        [MobClick event:@"love_medicine_click"];
    } else if ([serviceName isEqualToString:@"跑腿"]) {
        [MobClick event:@"love_errand_click"];
    } else if ([serviceName isEqualToString:@"咖啡"]) {
        [MobClick event:@"love_coffee_click"];
    } else if ([serviceName isEqualToString:@"洗衣"]) {
        [MobClick event:@"love_wash_click"];
    } else if ([serviceName isEqualToString:@"搬家"]) {
        [MobClick event:@"love_move_click"];
    } else if ([serviceName isEqualToString:@"健康"]) {
        [MobClick event:@"love_health_click"];
    } else if ([serviceName isEqualToString:@"查询"]) {
        [MobClick event:@"love_search_click"];
    } else if ([serviceName isEqualToString:@"美容"]) {
        [MobClick event:@"love_cosmetology_click"];
    } else if ([serviceName isEqualToString:@"网购"]) {
        [MobClick event:@"love_shoppingOnline_click"];
    } else if ([serviceName isEqualToString:@"演出赛事"]) {
        [MobClick event:@"love_game_click"];
    } else if ([serviceName isEqualToString:@"宠物"]) {
        [MobClick event:@"love_pet_click"];
    } else if ([serviceName isEqualToString:@"美食"]) {
        [MobClick event:@"love_cate_click"];
    } else if ([serviceName isEqualToString:@"旅行"]) {
        [MobClick event:@"love_Travel_click"];
    } else if ([serviceName isEqualToString:@"女性"]) {
        [MobClick event:@"love_women_click"];
    } else if ([serviceName isEqualToString:@"惊喜"]) {
        [MobClick event:@"love_surprise_click"];
    } else if ([serviceName isEqualToString:@"拼团"]) {
        [MobClick event:@"love_groupBuy_click"];
    } else if ([serviceName isEqualToString:@"蛋糕"]) {
        [MobClick event:@"love_cake_click"];
    } else if ([serviceName isEqualToString:@"差额"]) {
        [MobClick event:@"love_difference_click"];
    } else if ([serviceName isEqualToString:@"包月用车"]) {
        [MobClick event:@"love_taxiForMonth_click"];
    } else if ([serviceName isEqualToString:@"邀请码"]) {
        [MobClick event:@"love_invite_click"];
    } else if ([serviceName isEqualToString:@"提醒"]) {
        [MobClick event:@"love_remind_click"];
    } else if ([serviceName isEqualToString:@"迎新"]) {
        [MobClick event:@"love_newCategory_click"];
    } else if ([serviceName isEqualToString:@"VIP"]) {
        [MobClick event:@"love_VIP_click"];
    }
}

@end
