//
//  WYLoveSessionViewController.m
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/5/30.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import "WYLoveSessionViewController.h"
#import "WYLoveSessionCell.h"
#import "WYLoveSessionModel.h"
#import "WYSessionTextModel.h"
#import "WYLoveTypeModel.h"
#import "WYCategoryViewController.h"
#import "WYRankLoveGeniusViewController.h"
#import "WYSearchRankGeniusViewController.h"
#import "WYLoveNaviView.h"
#import "WYCategoryPickerView.h"
#import "WYDatePickerView.h"
#import "WYSignModel.h"
#import "WYLoveSignCell.h"
#import "WYSignView.h"
#import "WYSignCategoryView.h"
#import "UIBarButtonItem+Extension.h"
#import "WYNetworkTools.h"
#import "MBProgressHUD.h"
#import "MJRefresh.h"

@interface WYLoveSessionViewController () <UICollectionViewDelegate, UICollectionViewDataSource, WYLoveSessionCellDelegate, WYLoveNaviViewDelegate, WYCategoryPickerViewDelegate, WYDatePickerViewDelegate, WYSignViewDelegate, WYCategoryViewControllerDelegate, WYSignCategoryViewDelegate>


@property (nonatomic, strong) UIScrollView *loveScrollView;

@property (nonatomic, strong) UICollectionView *loveSessionCollectionView;
@property (nonatomic, strong) UICollectionViewFlowLayout *loveSessionCollectionViewFlowlayout;
//上面的segmentControl点赞和标注
@property (nonatomic, strong) UISegmentedControl *segmentControl;
//导航栏标题
@property (nonatomic, strong) WYLoveNaviView *naviView;
@property (nonatomic, strong) UIView *separateView;
//为空的背景图片
@property (nonatomic, strong) UIImageView *nullImageView;
//自定义分类的PickerView
@property (nonatomic, strong) WYCategoryPickerView *categoryPickerView;
@property (nonatomic, strong) UIButton *geniusRankButton;
//标注为空的时候的背景图片
@property (nonatomic, strong) UIImageView *signNullImageView;
@property (nonatomic, strong) UIView *signTitleView;
@property (nonatomic, strong) UILabel *signNumTitleLabel;
@property (nonatomic, strong) UILabel *signNumLabel;
@property (nonatomic, strong) UILabel *dateLabel;
@property (nonatomic, strong) UIImageView *dateImageView;
@property (nonatomic, strong) UIButton *chooseDateButton;
//标注的日期控件
@property (nonatomic, strong) WYDatePickerView *datePickerView;
//自定义的标注选择品类的页面
@property (nonatomic, strong) WYSignCategoryView *signCategoryView;

@property (nonatomic, strong) NSArray *statuArr;
//请求点赞数据用到的参数，category服务品类；statusNum代表新，热门，精华等；pn代表已经请求的页面数
@property (nonatomic, copy) NSString *category;
@property (nonatomic, assign) int statuNum;
@property (nonatomic, assign) int pn;

@property (nonatomic, strong) NSMutableArray *loveDataArray;

@property (nonatomic, assign) BOOL isRefresh;
//更改品类的数组
@property (nonatomic, strong) NSArray *typeArr;
//标注日期
@property (nonatomic, strong) NSString *signDateStr;

@property (nonatomic, strong) NSMutableArray *signDataArray;
//自定义的可以滑动的标注动画页面
@property (nonatomic, strong) WYSignView *signView;

@property (nonatomic, assign) NSInteger signCategoryIndex;

@end

@implementation WYLoveSessionViewController

-(instancetype)init{
    if (self = [super init]) {
        [self rcinit];
        [self setViewData];
    }
    return self;
}

-(void)home{
    [super home];
    [MobClick event:@"love_setLeftBarItem_click"];
}

-(void)setViewData{
    self.dateLabel.text = [self showDateStrWithDateStr:_signDateStr];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    [self addSubViews];
    [self addNaviItem];
    [self requestData];
    [self requestGeniusLabel];
}

-(void)viewDidLoad{
    
    [super viewDidLoad];
}

-(void)addNaviItem{
    self.navigationItem.titleView = self.segmentControl;
    UIBarButtonItem *searchItem = [UIBarButtonItem itemWithImageNamed:@"search_navi" target:self action:@selector(searchLoveSession)];
    self.navigationItem.leftBarButtonItem = searchItem;
}

-(void)addSubViews {
    
    self.loveScrollView.frame = CGRectMake(0, 0, SCREENWIDTH, self.view.h);
    
    self.naviView.frame = CGRectMake(0, 0, SCREENWIDTH, 39.5);
    
    self.separateView.frame = CGRectMake(0, 39.5, SCREENWIDTH * 2, 0.5);
    
    self.nullImageView.frame = CGRectMake(0, 40, SCREENWIDTH, SCREENHEIGHT - 64 - 48 - 40);
    
    self.loveSessionCollectionView.frame = CGRectMake(0, 40, SCREENWIDTH, SCREENHEIGHT - 64 - 48 - 40);
    
    self.geniusRankButton.frame = CGRectMake(16, SCREENHEIGHT - 19 - 44 - 64 - 44.5, 40, 44.5);
    
    self.signTitleView.frame = CGRectMake(SCREENWIDTH, 0, SCREENWIDTH, 39.5);
    
#warning to do 少用sizetofit进行布局, 会导致耦合性高, 代码可读性低, 普通界面使用masonry布局.
#warning to do 运算符中间需要空格.
    [self.signNumTitleLabel sizeToFit];
    self.signNumTitleLabel.frame = CGRectMake(12, (39.5 - self.signNumTitleLabel.h) * 0.5, self.signNumTitleLabel.w, self.signNumTitleLabel.h);
    
    [self.signNumLabel sizeToFit];
    self.signNumLabel.frame = CGRectMake(self.signNumTitleLabel.x + self.signNumTitleLabel.w, self.signNumTitleLabel.y, self.signNumLabel.w + 20, self.signNumLabel.h);
    
    self.dateImageView.frame = CGRectMake(SCREENWIDTH - 20 - 5, (39.5 - 8.5) * 0.5, 5, 8.5);
    
    [self.dateLabel sizeToFit];
    self.dateLabel.frame = CGRectMake(self.dateImageView.x - 2.5 - self.dateLabel.w, (40 - self.dateLabel.h) * 0.5, self.dateLabel.w, self.dateLabel.h);
    
    self.chooseDateButton.frame = CGRectMake(self.dateLabel.x - 7.5, (39.5 - 26) * 0.5, self.dateLabel.w + self.dateImageView.w + 2.5 + 15, 26);
    
    self.signNullImageView.frame = CGRectMake(SCREENWIDTH, 40, SCREENWIDTH, SCREENHEIGHT - 64 - 48 - 40);
}

-(void)geniusLoveRank {
    
    [MobClick event:@"love_geniusRank_click"];
    
    WYRankLoveGeniusViewController *rankGeniusVC = [[WYRankLoveGeniusViewController alloc]init];
    rankGeniusVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:rankGeniusVC animated:YES];
}

-(void)searchLoveSession {
    
    WYSearchRankGeniusViewController *searchRank = [[WYSearchRankGeniusViewController alloc]init];
    [self presentViewController:searchRank animated:YES completion:nil];
}

-(void)rcinit {
    
    self.view.backgroundColor = [UIColor colorWithRed:0.95 green:0.95 blue:0.95 alpha:1.0];
    _statuNum = 0;
    _pn = 1;
    _category = @"-1";
    _isRefresh = YES;
    _signDateStr = [WYTool currentDateStrFromeDate:[NSDate dateWithTimeIntervalSinceNow:-(24*60*60)]];
    _signCategoryIndex = -1;
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(didClickCategoryButton) name:@"WYSignShowCategoryPickerView" object:nil];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(successSignCategory) name:@"wySignCategory" object:nil];
}

-(void)requestData{
    
    __block MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.view addSubview:HUD];
    HUD.mode = MBProgressHUDModeText;
    HUD.label.text = @"正在刷新";
    [HUD showAnimated:YES whileExecutingBlock:^{}];
    
    __weak typeof(self) __weakSelf = self;
    [[WYNetworkTools shareTools]loveSessionWithCategory:_category andStatu:_statuNum andPn:_pn finished:^(id responseObject, NSError *error) {

        [HUD removeFromSuperview];
        HUD = nil;
        
        if (!error) {
            if (_isRefresh) {
                [__weakSelf.loveDataArray removeAllObjects];
            }
            NSArray *dataArray = responseObject;
            for (NSDictionary *dataDic in dataArray) {
                WYLoveSessionModel *model = [WYLoveSessionModel createLoveSessionModelWithDict:dataDic];
                [__weakSelf.loveDataArray addObject:model];
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                [__weakSelf.loveSessionCollectionView reloadData];
                [__weakSelf.loveSessionCollectionView.mj_header endRefreshing];
                [__weakSelf.loveSessionCollectionView.mj_footer endRefreshing];
            });
        } else {
            if (_isRefresh) {
                [__weakSelf.loveDataArray removeAllObjects];
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                [__weakSelf.loveSessionCollectionView reloadData];
                [__weakSelf.loveSessionCollectionView.mj_header endRefreshing];
                [__weakSelf.loveSessionCollectionView.mj_footer endRefreshing];
            });
        }
        if (__weakSelf.loveDataArray.count == 0) {
            self.nullImageView.hidden = NO;
        } else {
            self.nullImageView.hidden = YES;
        }
    }];
}

-(void)requestGeniusLabel{
    
    if ([RCIM sharedRCIM].geniusLabelArray.count == 0) {
        [[SDJNetworkTools sharedTools]geniusLabel:[SDJUserAccount loadAccount].token force:1 finished:^(NSDictionary<NSString *,id> * _Nullable result, NSError * _Nullable error) {
            if (![result[@"error"] isEqual:[NSNull null]]) {//报错
                NSNumber *code = result[@"error"][@"error_code"];
                if (code.integerValue == 20000) {
                    NSString *UserPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).firstObject stringByAppendingPathComponent:@"account.plist"];
                    [[NSFileManager defaultManager]removeItemAtPath:UserPath error:nil];
                    [[RCIMClient sharedRCIMClient] logout];
                    // 发送通知让appdelegate去切换根视图为聊天列表界面
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"WYRootViewControllerSwitchNotification" object:@"0"];
                    });
                }
            }
            else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [RCIM sharedRCIM].geniusLabelArray = [WYTool geniusArrayOfArray:result[@"data"]];
                });
            }
        }];
    }
}

-(void)loadNewInformation{
    _pn = 1;
    _isRefresh = YES;
    [self requestData];
    if (self.loveDataArray.count > 0) {
        [self.loveSessionCollectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UICollectionViewScrollPositionTop animated:NO];
    }
}

-(void)moreInformation{
    
    _pn ++;
    _isRefresh = NO;
    [self requestData];
}

#pragma mark --- 标注网络请求
-(void)requestSignNum{
    
    [[WYNetworkTools shareTools]signNumWithGeniusIdFinished:^(id responseObject, NSError *error) {
        if (responseObject && ![responseObject containsObject:@"errno"]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                NSDictionary *dic = [responseObject lastObject];
                self.signNumLabel.text = [dic[@"label_time"]stringValue];
            });
        }
    }];
}

-(void)requestSignData{
    
    __weak typeof(self) __weakSelf = self;
    [[WYNetworkTools shareTools]signDataWithGeniusIdAndTime:[self requestDateStrWithDateStr:_signDateStr] finished:^(id responseObject, NSError *error) {
        if (responseObject && ![responseObject containsObject:@"errno"]) {
            [self.signDataArray removeAllObjects];
            for (NSDictionary *dic in responseObject) {
                WYSignModel *signModel = [WYSignModel createModelWithDic:dic];
                [__weakSelf.signDataArray addObject:signModel];
            }
            if (__weakSelf.signDataArray.count > 0) {
                CGFloat height = 295+24+50+44;
                if (IS_IPHONE_5) {
                    height = 295+24+30+44;
                }
                __weakSelf.signView.frame = CGRectMake(SCREENWIDTH+12, 40+(SCREENHEIGHT-64-40-44-(295+24+50+44))*0.5, SCREENWIDTH - 24, height);
                __weakSelf.signView.dataArray = [__weakSelf.signDataArray copy];
                __weakSelf.signNullImageView.hidden = YES;
            } else {
                [__weakSelf.signView removeFromSuperview];
                __weakSelf.signView = nil;
                __weakSelf.signNullImageView.hidden = NO;
            }
        }
    }];
}

#pragma mark --- WYLoveSessionCellDelegate
-(void)loveThisMsg:(WYLoveSessionCell *)cell{
    
    NSIndexPath *indexPath = [self.loveSessionCollectionView indexPathForCell:cell];
    __block WYLoveSessionModel *selectModel = self.loveDataArray[indexPath.row];
    [[WYNetworkTools shareTools]loveSessionWithDocId:selectModel.doc_id andCategory:@"" andTimeStamp:@((long long)[[NSDate date]timeIntervalSince1970] * 1000).stringValue andGeniusId:@([SDJUserAccount loadAccount].genius_id).stringValue finished:^(id responseObject, NSError *error) {
        if (!error) {
            selectModel.like = responseObject[@"like"];
        }
    }];
}

-(void)hateThisMsg:(WYLoveSessionCell *)cell{
    
    NSIndexPath *indexPath = [self.loveSessionCollectionView indexPathForCell:cell];
    __block WYLoveSessionModel *selectModel = self.loveDataArray[indexPath.row];
    [[WYNetworkTools shareTools]hateSessionWithDocId:selectModel.doc_id andTimeStamp:@((long long)[[NSDate date]timeIntervalSince1970] * 1000).stringValue andGeniusId:@([SDJUserAccount loadAccount].genius_id).stringValue finished:^(id responseObject, NSError *error) {
        [self.loveDataArray removeObject:selectModel];
        //--------------主线程-------------
#warning to do
        [self.loveSessionCollectionView deleteItemsAtIndexPaths:@[indexPath]];
    }];
}

-(void)changeCategoryThisMsg:(WYLoveSessionCell *)cell{
    
    __weak __typeof__(self) weakSelf = self;
    
    NSIndexPath *indexPath = [self.loveSessionCollectionView indexPathForCell:cell];
    __block WYLoveSessionModel *selectModel = self.loveDataArray[indexPath.row];
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"更改品类" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    for (WYSessionTextModel *typeModel in self.typeArr) {
        UIAlertAction *action = [UIAlertAction actionWithTitle:typeModel.type style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            if ([weakSelf.typeArr indexOfObject:typeModel] == 0) {
                [MobClick event:@"love_noneCategory_click"];
            }else {
                [MobClick event:@"love_anyCategory_click"];
            }
            [[WYNetworkTools shareTools]loveSessionWithDocId:selectModel.doc_id andCategory:typeModel.type andTimeStamp:@((long long)[[NSDate date]timeIntervalSince1970] * 1000).stringValue andGeniusId:@([SDJUserAccount loadAccount].genius_id).stringValue finished:^(id responseObject, NSError *error) {
                if (!error) {
                    selectModel.domain = typeModel.type;
                    [cell changeCategory:typeModel.type];
                }
            }];
        }];
        [alertController addAction:action];
    }
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        [MobClick event:@"love_cancelCategory_click"];
    }];
    [alertController addAction:cancelAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark --- UICollectionViewDelegate
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return self.loveDataArray.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    WYLoveSessionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    [cell setLoveSessionCellWithModel:self.loveDataArray[indexPath.row] withSearchText:@""];
    cell.loveSessionCellDelegate = self;
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat height = 44 + 44 + 10 + 12;
    
    WYLoveSessionModel *sessionModel = self.loveDataArray[indexPath.row];
    
    CGFloat loveContentHeight = 0;
    
    for (WYSessionTextModel *textModel in sessionModel.session) {
        
        CGSize rowSize = [WYTool labelHeightWithText:textModel.text andWidth:SCREENWIDTH  - 20 - 18 - 26.5 - 10];
        rowSize = CGSizeMake(ceil(rowSize.width), ceil(rowSize.height));
        if (rowSize.height < 26) {
            rowSize = CGSizeMake(ceil(rowSize.width), 26);
        }
        loveContentHeight += rowSize.height + 23;
    }
    loveContentHeight -= 23;
    
    height += loveContentHeight;
    
    return CGSizeMake(SCREENWIDTH - 20, height);
}

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    
    [UIView animateWithDuration:0.5 animations:^{
        self.geniusRankButton.frame = CGRectMake(-40, SCREENHEIGHT - 19 - 44 - 64 - 44.5, 40, 44.5);
    }];
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    [UIView animateWithDuration:0.5 animations:^{
        self.geniusRankButton.frame = CGRectMake(16, SCREENHEIGHT - 19 - 44 - 64 - 44.5, 40, 44.5);
    }];
}

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    
    if (!decelerate) {
        [UIView animateWithDuration:0.5 animations:^{
            self.geniusRankButton.frame = CGRectMake(16, SCREENHEIGHT - 19 - 44 - 64 - 44.5, 40, 44.5);
        }];
    }
}

#pragma mark --- WYCategoryPickerViewDelegate
-(void)closeCategoryPickerView{
    
    [self.categoryPickerView removeFromSuperview];
    self.categoryPickerView = nil;
}

-(void)chooseCategory:(NSString *)category andIndex:(NSInteger)index{
    
    [self.signView.selectCell signMsgWithCategory:category];
    self.signCategoryIndex = index;
    [self.signCategoryView removeFromSuperview];
    self.signCategoryView = nil;
}

#pragma mark --- WYDatePickerViewDelegate
-(void)chooseDate:(NSString *)dateStr{
    
    _signDateStr = [dateStr copy];
    self.dateLabel.text = [self showDateStrWithDateStr:_signDateStr];
    [self requestSignData];
    [self.datePickerView removeFromSuperview];
    self.datePickerView = nil;
}

-(void)closeDatePickerView{
    
    [self.datePickerView removeFromSuperview];
    self.datePickerView = nil;
}

#pragma mark --- WYLoveNaviViewDelegate
-(void)changeTagRequestData:(NSInteger)tag{
    
    self.statuNum = (int)tag - 1;
    [self loadNewInformation];
}

#pragma mark --- WYSignViewDelegate
-(void)requestNewData{
    
    [self requestSignData];
}

#pragma mark --- 按钮点击事件
-(void)successSignCategory{
    
    self.signNumLabel.text = [NSString stringWithFormat:@"%d", self.signNumLabel.text.intValue+1];
}

-(void)didClickCategoryButton{
    
    // -----------------不要混用布局-----------------
    [self.signCategoryView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.bottom.equalTo(0);
    }];
}

// 点击navi上面品类按钮
-(void)didClickNaviCategoryButton{
    
    WYCategoryViewController *categoryVC = [[WYCategoryViewController alloc]initWithCategoryArray:self.typeArr andCategory:_category];
    categoryVC.categoryVCDelegate = self;
    [self presentViewController:categoryVC animated:YES completion:nil];
}

// 点击标注品类自定义方法
-(void)didClickDateButton{
    
    [self.datePickerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.bottom.equalTo(self.tabBarController.view);
    }];
}

#pragma mark --- WYCategoryViewControllerDelegate
-(void)changeCategory:(NSString *)categoryStr{
    
    if (self.segmentControl.selectedSegmentIndex == 0) {
        if ([categoryStr isEqualToString:@"不限"]) {
            _category = @"所有品类";
        } else {
            _category = categoryStr;
        }
        [self.naviView changeLabelString:_category];
        [self loadNewInformation];
    }
    else if (self.segmentControl.selectedSegmentIndex == 1) {
        if ([categoryStr isEqualToString:@"不限"]) {
        }
        else {
            [self.signView.selectCell signMsgWithCategory:categoryStr];
        }
    }
}

-(void)closeSignCategoryView{
    
    [self.signCategoryView removeFromSuperview];
    self.signCategoryView = nil;
}

#pragma mark --- segmentChange
-(void)changeSegmentSign{
    
    switch (_segmentControl.selectedSegmentIndex) {
        case 0:{
            [_loveScrollView setContentOffset:CGPointMake(0, 0) animated:YES];
            break;
        }
        case 1:{
            [_loveScrollView setContentOffset:CGPointMake(SCREENWIDTH, 0) animated:YES];
            [self requestSignNum];
            [self requestSignData];
            break;
        }
        default:
            break;
    }
}

#pragma mark --- 懒加载
- (NSArray *)statuArr {
    
	if(_statuArr == nil) {
		_statuArr = @[@"新", @"热门", @"精华", @"我的推荐", @"历史"];
	}
	return _statuArr;
}

- (NSMutableArray *)loveDataArray {
    
	if(_loveDataArray == nil) {
		_loveDataArray = [[NSMutableArray alloc] init];
	}
	return _loveDataArray;
}

- (NSArray *)typeArr {
    
    if(_typeArr == nil) {
        _typeArr = [WYLoveTypeModel createLoveTypeModelWithArrayWithCategory:self.category];
    }
    return _typeArr;
}

- (UISegmentedControl *)segmentControl {
    
    if(_segmentControl == nil) {
        _segmentControl =[[UISegmentedControl alloc]initWithItems:@[@"点赞", @"标注"]];
        _segmentControl.backgroundColor = [UIColor clearColor];
        _segmentControl.selectedSegmentIndex = 0;
        _segmentControl.tintColor = HEXCOLOR(0xffffff);
        _segmentControl.frame = CGRectMake(0, 0, 122, 23);
        [_segmentControl addTarget:self action:@selector(changeSegmentSign) forControlEvents:UIControlEventValueChanged];
    }
    return _segmentControl;
}

#pragma mark --- loveView
- (UICollectionView *)loveSessionCollectionView {
    
    if(_loveSessionCollectionView == nil) {
        _loveSessionCollectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:self.loveSessionCollectionViewFlowlayout];
        _loveSessionCollectionView.delegate = self;
        _loveSessionCollectionView.dataSource = self;
        _loveSessionCollectionView.backgroundColor = [UIColor clearColor];
        _loveSessionCollectionView.mj_header = [MJRefreshStateHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadNewInformation)];
        _loveSessionCollectionView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(moreInformation)];
        [_loveSessionCollectionView registerClass:[WYLoveSessionCell class] forCellWithReuseIdentifier:@"cell"];
        [self.loveScrollView addSubview:_loveSessionCollectionView];
    }
    return _loveSessionCollectionView;
}

- (UICollectionViewFlowLayout *)loveSessionCollectionViewFlowlayout {
    
    if(_loveSessionCollectionViewFlowlayout == nil) {
        _loveSessionCollectionViewFlowlayout = [[UICollectionViewFlowLayout alloc] init];
        _loveSessionCollectionViewFlowlayout.minimumLineSpacing = 0.0f;
        _loveSessionCollectionViewFlowlayout.scrollDirection = UICollectionViewScrollDirectionVertical;
        
    }
    return _loveSessionCollectionViewFlowlayout;
}

- (UIScrollView *)loveScrollView {
    
    if(_loveScrollView == nil) {
        _loveScrollView = [[UIScrollView alloc] init];
        _loveScrollView.scrollEnabled = NO;
        _loveScrollView.pagingEnabled = YES;
        _loveScrollView.showsHorizontalScrollIndicator = NO;
        _loveScrollView.delegate = self;
        _loveScrollView.contentSize = CGSizeMake(2 * self.view.w, 0);
        [self.view addSubview:_loveScrollView];
    }
    return _loveScrollView;
}

- (UIImageView *)nullImageView {
    
	if(_nullImageView == nil) {
		_nullImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"loveNull"]];
        _nullImageView.hidden = YES;
        _nullImageView.contentMode = UIViewContentModeCenter;
        _nullImageView.backgroundColor = [UIColor colorWithRed:0.95 green:0.95 blue:0.95 alpha:1.0];
        [self.loveScrollView addSubview:_nullImageView];
	}
	return _nullImageView;
}

- (WYLoveNaviView *)naviView {
    
	if(_naviView == nil) {
		_naviView = [[WYLoveNaviView alloc] initWithFrame:CGRectMake(0, 0, SCREENWIDTH, 40)];
        _naviView.loveNaviViewDelegate = self;
        [self.loveScrollView addSubview:_naviView];
	}
	return _naviView;
}

- (WYCategoryPickerView *)categoryPickerView {
    
	if(_categoryPickerView == nil) {
        _categoryPickerView = [[WYCategoryPickerView alloc] initWithCategory:_category withArray:self.typeArr];
        _categoryPickerView.categoryPickerViewDelegate = self;
        [self.tabBarController.view addSubview:_categoryPickerView];
	}
	return _categoryPickerView;
}

- (UIButton *)geniusRankButton {
    
	if(_geniusRankButton == nil) {
		_geniusRankButton = [[UIButton alloc] init];
        [_geniusRankButton setImage:[UIImage imageNamed:@"cup"] forState:UIControlStateNormal];
        [_geniusRankButton addTarget:self action:@selector(geniusLoveRank) forControlEvents:UIControlEventTouchUpInside];
        [self.loveScrollView addSubview:_geniusRankButton];
	}
	return _geniusRankButton;
}

- (UIView *)separateView {
    
    if(_separateView == nil) {
        _separateView = [[UIView alloc] init];
        _separateView.backgroundColor = HEXCOLOR(0xDFDFDF);
        [self.loveScrollView addSubview:_separateView];
    }
    return _separateView;
}

#pragma mark --- signView
- (UIView *)signTitleView {
    
    if(_signTitleView == nil) {
        _signTitleView = [[UIView alloc] init];
        _signTitleView.backgroundColor = HEXCOLOR(0xffffff);
        [self.loveScrollView addSubview:_signTitleView];
    }
    return _signTitleView;
}

- (UILabel *)signNumTitleLabel {
    
    if(_signNumTitleLabel == nil) {
        _signNumTitleLabel = [[UILabel alloc] init];
        _signNumTitleLabel.font = [UIFont systemFontOfSize:15];
        _signNumTitleLabel.text = @"本周标注数：";
        _signNumTitleLabel.textColor = HEXCOLOR(0x505050);
        [self.signTitleView addSubview:_signNumTitleLabel];
    }
    return _signNumTitleLabel;
}

- (UILabel *)signNumLabel {
    
    if(_signNumLabel == nil) {
        _signNumLabel = [[UILabel alloc] init];
        _signNumLabel.font = [UIFont systemFontOfSize:15];
        _signNumLabel.text = @"0";
        _signNumLabel.textColor = HEXCOLOR(0xFFC000);
        [self.signTitleView addSubview:_signNumLabel];
    }
    return _signNumLabel;
}

- (UILabel *)dateLabel {
    
    if(_dateLabel == nil) {
        _dateLabel = [[UILabel alloc] init];
        _dateLabel.textColor = HEXCOLOR(0x9D9D9D);
        _dateLabel.font = [UIFont systemFontOfSize:14];
        [self.signTitleView addSubview:_dateLabel];
    }
    return _dateLabel;
}

- (UIImageView *)dateImageView {
    
    if(_dateImageView == nil) {
        _dateImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"upAndDown"]];
        [self.signTitleView addSubview:_dateImageView];
    }
    return _dateImageView;
}

- (UIButton *)chooseDateButton {
    
    if(_chooseDateButton == nil) {
        _chooseDateButton = [[UIButton alloc] init];
        _chooseDateButton.layer.cornerRadius = 3.0;
        _chooseDateButton.layer.masksToBounds = YES;
        _chooseDateButton.layer.borderWidth = 0.5;
        _chooseDateButton.layer.borderColor = [HEXCOLOR(0x9D9D9D)CGColor];
        [_chooseDateButton addTarget:self action:@selector(didClickDateButton) forControlEvents:UIControlEventTouchUpInside];
        [self.signTitleView addSubview:_chooseDateButton];
    }
    return _chooseDateButton;
}

- (WYDatePickerView *)datePickerView {
    
	if(_datePickerView == nil) {
		_datePickerView = [[WYDatePickerView alloc] init];
        _datePickerView.stringDate = _signDateStr;
        _datePickerView.wyDatePickerViewDelegate = self;
        [self.tabBarController.view addSubview:_datePickerView];
	}
	return _datePickerView;
}

-(NSString *)showDateStrWithDateStr:(NSString *)dateStr{
    
    NSString *year = [dateStr substringWithRange:NSMakeRange(0, 4)];
    NSString *month = [dateStr substringWithRange:NSMakeRange(4, 2)];
    NSString *day = [dateStr substringWithRange:NSMakeRange(6, 2)];
    
    return [NSString stringWithFormat:@"%@/%@/%@", day, month, year];
}

-(NSString *)requestDateStrWithDateStr:(NSString *)dateStr{
    
    NSString *year = [dateStr substringWithRange:NSMakeRange(0, 4)];
    NSString *month = [dateStr substringWithRange:NSMakeRange(4, 2)];
    NSString *day = [dateStr substringWithRange:NSMakeRange(6, 2)];
    NSString *str = [NSString stringWithFormat:@"%@.%@.%@", year, month, day];
    return str;
    
}

- (NSMutableArray *)signDataArray {
    
	if(_signDataArray == nil) {
		_signDataArray = [[NSMutableArray alloc] init];
	}
	return _signDataArray;
}

- (WYSignView *)signView {
    
	if(_signView == nil) {
		_signView = [[WYSignView alloc] init];
        _signView.wySignViewDelegate = self;
        [self.loveScrollView addSubview:_signView];
	}
	return _signView;
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

- (UIImageView *)signNullImageView {
	if(_signNullImageView == nil) {
		_signNullImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"signNull"]];
        _signNullImageView.hidden = YES;
        _signNullImageView.contentMode = UIViewContentModeCenter;
        [self.loveScrollView addSubview:_signNullImageView];
	}
	return _signNullImageView;
}

- (WYSignCategoryView *)signCategoryView {
    if(_signCategoryView == nil) {
        _signCategoryView = [[WYSignCategoryView alloc] initWithCategoryIndex:_signCategoryIndex];
        _signCategoryView.wySignCategoryViewDelegate = self;
        [self.tabBarController.view addSubview:_signCategoryView];
	}
	return _signCategoryView;
}

@end
