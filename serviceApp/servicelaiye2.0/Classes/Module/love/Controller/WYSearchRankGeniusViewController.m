//
//  WYSearchRankGeniusViewController.m
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/7/27.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import "WYSearchRankGeniusViewController.h"
#import "WYLoveSessionCell.h"
#import "WYSessionTextModel.h"
#import "WYLoveSessionModel.h"
#import "WYSessionTextModel.h"
#import "WYLoveTypeModel.h"
#import "WYLoveCandidateSearchView.h"
#import "WYSearchTitleLeftButton.h"
#import "WYPopoverView.h"
#import "WYNetworkTools.h"
#import "MJRefresh.h"

@interface WYSearchRankGeniusViewController () <UISearchBarDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, WYLoveSessionCellDelegate, WYLoveCandidateSearchViewDelegate, WYSearchTitleLeftButtonDelegate, WYPopoverViewDelegate>

@property (nonatomic, strong) WYLoveCandidateSearchView *candidateView;

@property (nonatomic, assign) CGFloat candidateViewHeight;

@property (nonatomic, strong) UIButton *blankButton;

@property (nonatomic, strong) UIView *navigationTitleView;

@property (nonatomic, strong) UISearchBar *searchBar;

@property (nonatomic, strong) UICollectionView *loveSessionCollectionView;

@property (nonatomic, strong) UICollectionViewFlowLayout *loveSessionCollectionViewFlowlayout;

@property (nonatomic, strong) UILabel *networkLabel;

@property (nonatomic, strong) NSMutableArray *dataArray;

@property (nonatomic, strong) NSArray *typeArr;

@property (nonatomic, assign) int page;

@property (nonatomic, copy) NSString *query;

@property (nonatomic, strong) WYSearchTitleLeftButton *searchTitleLeftButton;

@property (nonatomic, strong) WYPopoverView *popoverView;

@property (nonatomic, copy) NSString *queryType;

@end

@implementation WYSearchRankGeniusViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self rcinit];
    [self addSubviews];
    [self.searchBar becomeFirstResponder];
}

-(void)rcinit{
    
    self.view.backgroundColor = HEXCOLOR(0xeeeeee);
    _page = 1;
    _query = @"";
    _queryType = @"post";
}

-(void)requestHotwords{
    
    __weak __typeof__(self) weakSelf = self;
    
    [[WYNetworkTools shareTools]loveSearchImportWordsfinished:^(id responseObject, NSError *error) {
        if (responseObject) {
            NSMutableArray *array = [NSMutableArray array];
            for (NSDictionary *dic in responseObject) {
                [array addObject:dic[@"keys"]];
            }
            weakSelf.candidateView.hotWordsArray = [array copy];
            int row = ((int)weakSelf.candidateView.hotWordsArray.count) % 4 == 0 ? (int)weakSelf.candidateView.hotWordsArray.count / 4 : (int)weakSelf.candidateView.hotWordsArray.count / 4 + 1;
            CGFloat height = (row + 1) * 20 + row * 30;
            _candidateViewHeight = 34 + height;
            // ---------------主线程------------------------
            [weakSelf.candidateView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(weakSelf.navigationTitleView.bottom);
                make.left.right.equalTo(weakSelf.view);
                make.height.equalTo(_candidateViewHeight);
            }];
            [weakSelf.view bringSubviewToFront:weakSelf.candidateView];
        }
    }];
}

-(void)addSubviews{
    
    [self.navigationTitleView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self.view);
        make.height.equalTo(64);
    }];
    
    [self.searchBar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.navigationTitleView.bottom);
    }];
    
    [self.loveSessionCollectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.navigationTitleView.bottom);
        make.bottom.left.right.equalTo(self.view);
    }];
    
    [self.networkLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.view).offset(-27);
        make.centerX.equalTo(self.view);
        make.width.equalTo(116);
        make.height.equalTo(32);
    }];
}

#pragma mark --- 请求数据
-(void)requestLoveSessionData{
    
#warning to do
    //-------------weakself----------------
    __weak typeof(self) weakSelf = self;
    
    [[WYNetworkTools shareTools]searchLoveSessionWithPn:@(_page).stringValue withQuery:_query withQueryType:_queryType finished:^(id responseObject, NSError *error) {
        if (!error) {
            // ---------------主线程------------------------
            weakSelf.networkLabel.hidden = YES;
            if (_page == 1) {
                [weakSelf.dataArray removeAllObjects];
            }
            NSArray *dataArray = responseObject;
            for (NSDictionary *dataDic in dataArray) {
                WYLoveSessionModel *model = [WYLoveSessionModel createLoveSessionModelWithDict:dataDic];
                [weakSelf.dataArray addObject:model];
            }
            //tableView刷新
            [weakSelf.loveSessionCollectionView reloadData];
            [weakSelf.loveSessionCollectionView.mj_header endRefreshing];
            [weakSelf.loveSessionCollectionView.mj_footer endRefreshing];
        }else {
            weakSelf.networkLabel.hidden = NO;
            dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            dispatch_async(queue, ^(void) {
                sleep(3);
                dispatch_async(dispatch_get_main_queue(), ^(void) {
                    weakSelf.networkLabel.hidden = YES;
                });
            });
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakSelf.loveSessionCollectionView.mj_header endRefreshing];
                [weakSelf.loveSessionCollectionView.mj_footer endRefreshing];
            });
        }
    }];
}

#pragma mark --- 上拉下拉方法
-(void)loadNewInformation{
    
    _page = 1;
    [self requestLoveSessionData];
}

-(void)moreInformation{
    
    _page ++;
    [self requestLoveSessionData];
}

#pragma mark --- UISearchBarDelegate
-(BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar{
    
    [self.blankButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.navigationTitleView.bottom);
        make.bottom.left.right.equalTo(self.view);
    }];
    
    for (__strong id cc in [_searchBar.subviews[0] subviews]) {
        if ([cc isKindOfClass:NSClassFromString(@"UISearchBarTextField")]) {
            self.searchTitleLeftButton.frame = CGRectMake(0, 0, 66, 28);
            ((UITextField *)cc).leftView = self.searchTitleLeftButton;
        }
    }
    
    if (self.dataArray.count == 0) {
        [self requestHotwords];
    }
    return YES;
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    
    [self.searchBar endEditing:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    
    _page = 1;
    _query = self.searchBar.text;
    [self requestLoveSessionData];
    [self.searchBar endEditing:YES];
    
    for(id control in [self.searchBar.subviews[0] subviews])
    {
        if ([control isKindOfClass:[UIButton class]])
        {
            UIButton * btn =(UIButton *)control;
            btn.enabled = YES;
        }
    }
    [self.candidateView removeFromSuperview];
    self.candidateView = nil;
}

#pragma mark --- 点击空白背景按钮事件
-(void)hiddenKeyboard{
    
    [self.blankButton removeFromSuperview];
    self.blankButton = nil;
    
    [self.searchBar resignFirstResponder];
    for (__strong id cc in [_searchBar.subviews[0] subviews]) {
        if ([cc isKindOfClass:NSClassFromString(@"UISearchBarTextField")]) {
            ((UITextField *)cc).leftView = nil;
        }
    }
}

#pragma mark --- UICollectionViewDelegate
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return self.dataArray.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    WYLoveSessionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    [cell setLoveSessionCellWithModel:self.dataArray[indexPath.row] withSearchText:_query];
    cell.loveSessionCellDelegate = self;
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat height = 44 + 44 + 10 + 12;
    
    WYLoveSessionModel *sessionModel = self.dataArray[indexPath.row];
    
    CGFloat loveContentHeight = 0;
    
    for (WYSessionTextModel *textModel in sessionModel.session) {
        CGSize rowSize = [WYTool labelHeightWithText:textModel.text andWidth:SCREENWIDTH  - 20 - 18 - 26.5 - 10];
        rowSize = CGSizeMake(ceil(rowSize.width), ceil(rowSize.height));
        if (rowSize.height < 26) {
            rowSize = CGSizeMake(ceil(rowSize.width), 26);
        }
        loveContentHeight += rowSize.height + 23;
    }
    loveContentHeight -= 23;
    
    height += loveContentHeight;
    
    return CGSizeMake(SCREENWIDTH - 20, height);
}

#pragma mark --- WYLoveSessionCellDelegate
-(void)loveThisMsg:(WYLoveSessionCell *)cell{
    
    NSIndexPath *indexPath = [self.loveSessionCollectionView indexPathForCell:cell];
    __block WYLoveSessionModel *selectModel = self.dataArray[indexPath.row];
    [[WYNetworkTools shareTools]loveSessionWithDocId:selectModel.doc_id andCategory:@"" andTimeStamp:@((long long)[[NSDate date]timeIntervalSince1970] * 1000).stringValue andGeniusId:@([SDJUserAccount loadAccount].genius_id).stringValue finished:^(id responseObject, NSError *error) {
        if (!error) {
            selectModel.like = responseObject[@"like"];
        }
    }];
}

-(void)hateThisMsg:(WYLoveSessionCell *)cell{
    
    __weak __typeof__(self) weakSelf = self;
    
    NSIndexPath *indexPath = [self.loveSessionCollectionView indexPathForCell:cell];
    __block WYLoveSessionModel *selectModel = self.dataArray[indexPath.row];
    [[WYNetworkTools shareTools]hateSessionWithDocId:selectModel.doc_id andTimeStamp:@((long long)[[NSDate date]timeIntervalSince1970] * 1000).stringValue andGeniusId:@([SDJUserAccount loadAccount].genius_id).stringValue finished:^(id responseObject, NSError *error) {
        [weakSelf.dataArray removeObject:selectModel];
        [weakSelf.loveSessionCollectionView deleteItemsAtIndexPaths:@[indexPath]];
    }];
}

-(void)changeCategoryThisMsg:(WYLoveSessionCell *)cell{
    
    //------------------weakself-------------------------
#warning to do
    NSIndexPath *indexPath = [self.loveSessionCollectionView indexPathForCell:cell];
    __block WYLoveSessionModel *selectModel = self.dataArray[indexPath.row];
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"更改品类" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    for (WYSessionTextModel *typeModel in self.typeArr) {
        UIAlertAction *action = [UIAlertAction actionWithTitle:typeModel.type style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            if ([self.typeArr indexOfObject:typeModel] == 0) {
                [MobClick event:@"love_noneCategory_click"];
            }else {
                [MobClick event:@"love_anyCategory_click"];
            }
            [[WYNetworkTools shareTools]loveSessionWithDocId:selectModel.doc_id andCategory:typeModel.type andTimeStamp:@((long long)[[NSDate date]timeIntervalSince1970] * 1000).stringValue andGeniusId:@([SDJUserAccount loadAccount].genius_id).stringValue finished:^(id responseObject, NSError *error) {
                if (!error) {
                    selectModel.domain = typeModel.type;
                    [cell changeCategory:typeModel.type];
                }
            }];
        }];
        [alertController addAction:action];
    }
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        [MobClick event:@"love_cancelCategory_click"];
    }];
    [alertController addAction:cancelAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark --- WYLoveCandidateSearchViewDelegate
-(void)didTouchHotWordButtonWithTitle:(NSString *)title{
    
    self.searchBar.text = title;
    _query = title;
    [self requestLoveSessionData];
    
    [self.candidateView removeFromSuperview];
    self.candidateView = nil;
}

#pragma mark --- WYSearchTitleLeftButton
-(void)addPopView{
    
    [self.popoverView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.view);
        make.top.equalTo(64);
    }];
}

#pragma mark --- WYPopoverViewDelegate
-(void)chooseQueryType:(NSString *)queryType{
    
    if ([queryType isEqualToString:@"问题"]) {
        _queryType = @"post";
    } else if ([queryType isEqualToString:@"答案"]) {
        _queryType = @"reply";
    } else if ([queryType isEqualToString:@"关键字"]) {
        _queryType = @"key_words";
    }
    
    [self.searchTitleLeftButton changeButtonTitle:queryType];
    [self closePopoverView];
}

-(void)closePopoverView{
    
    [self.popoverView removeFromSuperview];
    self.popoverView = nil;
}

#pragma mark --- 懒加载
- (UIView *)navigationTitleView {
    
    if(_navigationTitleView == nil) {
        _navigationTitleView = [[UIView alloc] init];
        _navigationTitleView.backgroundColor = NAVIGATIONBAR_BACKGROUDCOLOR;
        [self.view addSubview:_navigationTitleView];
    }
    return _navigationTitleView;
}

- (UISearchBar *)searchBar {
    
    if(_searchBar == nil) {
        _searchBar = [[UISearchBar alloc] init];
        _searchBar.delegate = self;
        _searchBar.autocorrectionType = UITextAutocapitalizationTypeNone;
        _searchBar.keyboardType = UIKeyboardTypeDefault;
        _searchBar.placeholder = @"请输入关键字";
        [_searchBar setShowsCancelButton:YES animated:YES];
        for (__strong id cc in [_searchBar.subviews[0] subviews]) {
            if ([cc isKindOfClass:NSClassFromString(@"UISearchBarBackground")]) {
                [cc removeFromSuperview];
                cc = nil;
            }
            if ([cc isKindOfClass:NSClassFromString(@"UISearchBarTextField")]) {
                ((UITextField *)cc).font = [UIFont systemFontOfSize:14];
                self.searchTitleLeftButton.frame = CGRectMake(0, 0, 66, 28);
                ((UITextField *)cc).leftView = self.searchTitleLeftButton;
            }
            if ([cc isKindOfClass:NSClassFromString(@"UINavigationButton")]) {
                [(UIButton *)cc setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            }
        }
        [self.navigationTitleView addSubview:_searchBar];
    }
    return _searchBar;
}

- (UICollectionView *)loveSessionCollectionView {
    
    if(_loveSessionCollectionView == nil) {
        _loveSessionCollectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:self.loveSessionCollectionViewFlowlayout];
        _loveSessionCollectionView.delegate = self;
        _loveSessionCollectionView.dataSource = self;
        _loveSessionCollectionView.backgroundColor = [UIColor colorWithRed:0.95 green:0.95 blue:0.95 alpha:1.0];
        _loveSessionCollectionView.mj_header = [MJRefreshStateHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadNewInformation)];
        _loveSessionCollectionView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(moreInformation)];
        [_loveSessionCollectionView registerClass:[WYLoveSessionCell class] forCellWithReuseIdentifier:@"cell"];
        [_loveSessionCollectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderView"];
        [self.view addSubview:_loveSessionCollectionView];
    }
    return _loveSessionCollectionView;
}

- (UICollectionViewFlowLayout *)loveSessionCollectionViewFlowlayout {
    
    if(_loveSessionCollectionViewFlowlayout == nil) {
        _loveSessionCollectionViewFlowlayout = [[UICollectionViewFlowLayout alloc] init];
        _loveSessionCollectionViewFlowlayout.minimumLineSpacing = 0.0f;
        _loveSessionCollectionViewFlowlayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    }
    return _loveSessionCollectionViewFlowlayout;
}

- (NSMutableArray *)dataArray {
    
	if(_dataArray == nil) {
		_dataArray = [[NSMutableArray alloc] init];
	}
	return _dataArray;
}

- (NSArray *)typeArr {
    
    if(_typeArr == nil) {
        _typeArr = [WYLoveTypeModel createLoveTypeModelWithArrayWithCategory:@"-1"];
    }
    return _typeArr;
}

- (UILabel *)networkLabel {
    
	if(_networkLabel == nil) {
		_networkLabel = [[UILabel alloc] init];
        _networkLabel.hidden = YES;
        _networkLabel.backgroundColor = [UIColor colorWithRed:24.0/255.0 green:24.0/255.0 blue:24.0/255.0 alpha:0.8];
        _networkLabel.textColor = [UIColor whiteColor];
        _networkLabel.font = [UIFont systemFontOfSize:12];
        _networkLabel.layer.cornerRadius = 3.0;
        _networkLabel.layer.masksToBounds = YES;
        _networkLabel.textAlignment = NSTextAlignmentCenter;
        _networkLabel.text = @"网络状态不好";
        [self.view addSubview:_networkLabel];
	}
	return _networkLabel;
}

- (WYLoveCandidateSearchView *)candidateView {
    
    if(_candidateView == nil) {
        _candidateView = [[WYLoveCandidateSearchView alloc] init];
        _candidateView.loveCandidateSearchViewDelegate = self;
        [self.view addSubview:_candidateView];
    }
    return _candidateView;
}

- (UIButton *)blankButton {
    
	if(_blankButton == nil) {
		_blankButton = [[UIButton alloc] init];
        _blankButton.backgroundColor = [UIColor clearColor];
        [_blankButton addTarget:self action:@selector(hiddenKeyboard) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:_blankButton];
	}
	return _blankButton;
}

- (WYSearchTitleLeftButton *)searchTitleLeftButton {
    
    if(_searchTitleLeftButton == nil) {
        _searchTitleLeftButton = [[WYSearchTitleLeftButton alloc] init];
        _searchTitleLeftButton.wySearchTitleLeftButtonDelegate = self;
    }
    return _searchTitleLeftButton;
}

- (WYPopoverView *)popoverView {
	if(_popoverView == nil) {
		_popoverView = [[WYPopoverView alloc] init];
        _popoverView.wyPopoverViewDelegate = self;
        [self.view addSubview:_popoverView];
	}
	return _popoverView;
}

@end
