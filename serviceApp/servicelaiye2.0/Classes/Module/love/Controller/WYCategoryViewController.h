//
//  WYCategoryViewController.h
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/6/2.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import <UIKit/UIKit.h>
//品类选择页面，点击点赞页面导航上面按钮弹出此页面
@protocol WYCategoryViewControllerDelegate <NSObject>

-(void)changeCategory:(NSString *)categoryStr;

@end

@interface WYCategoryViewController : UIViewController

-(instancetype)initWithCategoryArray:(NSArray *)array andCategory:(NSString *)category;

@property (nonatomic, weak) id <WYCategoryViewControllerDelegate> categoryVCDelegate;

@end
