//
//  WYRankLoveGeniusViewController.m
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/6/2.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import "WYRankLoveGeniusViewController.h"
#import "WYGeniusRankTableViewCell.h"
#import "XZNaviV.h"
#import "WYGeniusModel.h"
#import "UIBarButtonItem+Extension.h"
#import "WYNetworkTools.h"
#import "MJRefresh.h"

@interface WYRankLoveGeniusViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UIImageView *firstImageView;
@property (nonatomic, strong) UIImageView *secondImageView;
@property (nonatomic, strong) UIImageView *thirdImageView;
@property (nonatomic, strong) UILabel *firstNameLabel;
@property (nonatomic, strong) UILabel *secondNameLabel;
@property (nonatomic, strong) UILabel *thirdNameLabel;
@property (nonatomic, strong) UILabel *firstNumLabel;
@property (nonatomic, strong) UILabel *secondNumLabel;
@property (nonatomic, strong) UILabel *thirdNumLabel;
@property (nonatomic, strong) UIImageView *firstPlatformImageView;
@property (nonatomic, strong) UIImageView *secondPlatformImageView;
@property (nonatomic, strong) UIImageView *thirdPlatformImageView;
@property (nonatomic, strong) XZNaviV *categoryNavi;
@property (nonatomic, strong) UIView *separateView;
@property (nonatomic, strong) UITableView *geniusRankTableView;
@property (nonatomic, strong) NSMutableArray *topThreeRankArray;
@property (nonatomic, strong) NSMutableArray *geniusRankArray;
@property (nonatomic, assign) BOOL isRankNotRanked;

@end

@implementation WYRankLoveGeniusViewController

-(instancetype)init{
    
    if (self = [super init]) {
    }
    return self;
}

-(void)viewDidLoad{
    
    [super viewDidLoad];
    [self rcinit];
    [self addNaviItem];
    [self addSubViews];
}

-(void)rcinit{
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.title = @"助理排行";
    _isRankNotRanked = YES;
}

-(void)addNaviItem{
    
    UIBarButtonItem *backItem = [UIBarButtonItem itemWithTitle:@"返回" target:self action:@selector(back) left:YES];
    self.navigationItem.leftBarButtonItem = backItem;
}

-(void)addSubViews{
    
    [self.categoryNavi mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.equalTo(self.view);
        make.height.equalTo(38.5);
    }];
    
    [self.separateView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.top.equalTo(self.categoryNavi.bottom);
        make.height.equalTo(0.5);
    }];
    
    [self.firstPlatformImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.separateView.bottom).offset(121);
        make.centerX.equalTo(self.view);
        make.width.equalTo(106);
        make.height.equalTo(83);
    }];
    
    [self.secondPlatformImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.firstPlatformImageView.left);
        make.width.equalTo(106);
        make.bottom.equalTo(self.firstPlatformImageView.bottom);
    }];
    
    [self.thirdPlatformImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.firstPlatformImageView.right);
        make.width.equalTo(106);
        make.bottom.equalTo(self.firstPlatformImageView.bottom);
    }];
    
    [self.firstNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.firstPlatformImageView.centerX);
        make.bottom.equalTo(self.firstPlatformImageView.top).offset(-6);
        make.height.equalTo(20);
    }];
    
    [self.secondNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.secondPlatformImageView.centerX);
        make.bottom.equalTo(self.secondPlatformImageView.top).offset(-6);
        make.height.equalTo(20);
    }];
    
    [self.thirdNumLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.thirdPlatformImageView.centerX);
        make.bottom.equalTo(self.thirdPlatformImageView.top).offset(-6);
        make.height.equalTo(20);
    }];
    
    [self.firstNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.firstPlatformImageView.centerX);
        make.bottom.equalTo(self.firstNumLabel.top);
        make.height.equalTo(22.5);
    }];
    
    [self.secondNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.secondPlatformImageView.centerX);
        make.bottom.equalTo(self.secondNumLabel.top);
        make.height.equalTo(22.5);
    }];
    
    [self.thirdNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.thirdPlatformImageView.centerX);
        make.bottom.equalTo(self.thirdNumLabel.top);
        make.height.equalTo(22.5);
    }];
    
    [self.firstImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.firstPlatformImageView.centerX);
        make.bottom.equalTo(self.firstNameLabel.top);
        make.height.width.equalTo(65);
    }];
    
    [self.secondImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.secondPlatformImageView.centerX);
        make.bottom.equalTo(self.secondNameLabel.top);
        make.height.width.equalTo(50);
    }];
    
    [self.thirdImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.thirdPlatformImageView.centerX);
        make.bottom.equalTo(self.thirdNameLabel.top);
        make.height.width.equalTo(50);
    }];
    
    [self.geniusTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.view);
        make.top.equalTo(self.firstPlatformImageView.bottom).offset(15);
    }];
}

-(void)back{
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)geniusNetWorking{
    
    _isRankNotRanked ? [self geniusNetWorkingWithType:0] : [self geniusNetWorkingWithType:1];
}

/**
 *  请求助理点赞排行数据
 */
-(void)geniusNetWorkingWithType:(int)index{
    
    [self.topThreeRankArray removeAllObjects];
    [self.geniusRankArray removeAllObjects];
    __weak typeof(self) __weakSelf = self;
    [[WYNetworkTools shareTools]loveRankWithRequestType:@(index).stringValue finished:^(id responseObject, NSError *error) {
        if (!error) {
            for (int i = 0; i < 3; i ++) {
                [__weakSelf.topThreeRankArray addObject:((NSArray *)responseObject)[i]];
            }
            for (int i = 3; i < [responseObject count]; i ++) {
                [__weakSelf.geniusRankArray addObject:((NSArray *)responseObject)[i]];
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                [__weakSelf changeTopThreeData];
                [__weakSelf.geniusRankTableView reloadData];
            });
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [__weakSelf.geniusRankTableView.mj_header endRefreshing];
        });
    }];
}

-(void)changeTopThreeData{
    
    self.firstNameLabel.text = @"";
    self.secondNameLabel.text = @"";
    self.thirdNameLabel.text = @"";
    
    for (WYGeniusModel *geniusModel in [RCIM sharedRCIM].geniusLabelArray) {
        if ([self.topThreeRankArray[0][@"genius_id"] isEqualToString:geniusModel.genius_id]) {
            [self.firstImageView sd_setImageWithURL:[NSURL URLWithString:geniusModel.avatar_url] placeholderImage:[UIImage imageNamed:@"portrait"]];
            self.firstNameLabel.text = geniusModel.real_name;
        }
        else if ([self.topThreeRankArray[1][@"genius_id"] isEqualToString:geniusModel.genius_id]) {
            [self.secondImageView sd_setImageWithURL:[NSURL URLWithString:geniusModel.avatar_url] placeholderImage:[UIImage imageNamed:@"portrait"]];
            self.secondNameLabel.text = geniusModel.real_name;
        }
        else if ([self.topThreeRankArray[2][@"genius_id"] isEqualToString:geniusModel.genius_id]) {
            [self.thirdImageView sd_setImageWithURL:[NSURL URLWithString:geniusModel.avatar_url] placeholderImage:[UIImage imageNamed:@"portrait"]];
            self.thirdNameLabel.text = geniusModel.real_name;
        }
    }
    if (self.firstNameLabel.text.length == 0) {
        self.firstNameLabel.text = self.topThreeRankArray[0][@"genius_id"];
    }
    if (self.secondNameLabel.text.length == 0) {
        self.secondNameLabel.text = self.topThreeRankArray[1][@"genius_id"];
    }
    if (self.thirdNameLabel.text.length == 0) {
        self.thirdNameLabel.text = self.topThreeRankArray[2][@"genius_id"];
    }
    
    self.firstNumLabel.text = [self.topThreeRankArray[0][@"freq"] stringValue];
    self.secondNumLabel.text = [self.topThreeRankArray[1][@"freq"] stringValue];
    self.thirdNumLabel.text = [self.topThreeRankArray[2][@"freq"] stringValue];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    [self geniusNetWorking];
}

#pragma mark --- UITableViewDelegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.geniusRankArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *Identifier = @"geniusRankCell";
    WYGeniusRankTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier forIndexPath:indexPath];
    [cell createCellWithDict:self.geniusRankArray[indexPath.row] andIndex:indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return self.view.h / 10;
}

//分割线顶头
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]){
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    cell.preservesSuperviewLayoutMargins = NO;
}

#pragma mark --- 懒加载
- (UITableView *)geniusTableView {
    
    if(_geniusRankTableView == nil) {
        _geniusRankTableView = [[UITableView alloc] init];
        [_geniusRankTableView registerClass:[WYGeniusRankTableViewCell class] forCellReuseIdentifier:@"geniusRankCell"];
        _geniusRankTableView.tableFooterView = [UIView new];
        _geniusRankTableView.delegate = self;
        _geniusRankTableView.dataSource = self;
        _geniusRankTableView.mj_header = [MJRefreshStateHeader headerWithRefreshingTarget:self refreshingAction:@selector(geniusNetWorking)];
        [self.view addSubview:_geniusRankTableView];
    }
    return _geniusRankTableView;
}

- (NSMutableArray *)topThreeRankArray {
    
    if(_topThreeRankArray == nil) {
        _topThreeRankArray = [[NSMutableArray alloc] init];
    }
    return _topThreeRankArray;
}

- (NSMutableArray *)geniusRankArray {
    
    if(_geniusRankArray == nil) {
        _geniusRankArray = [[NSMutableArray alloc] init];
    }
    return _geniusRankArray;
}

- (UIImageView *)firstImageView {
    
    if(_firstImageView == nil) {
        _firstImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"portrait"]];
        _firstImageView.layer.cornerRadius = 65*0.5;
        _firstImageView.layer.masksToBounds = YES;
        _firstImageView.layer.borderColor = [[UIColor lightGrayColor]CGColor];
        _firstImageView.layer.borderWidth = 0.5;
        [self.view addSubview:_firstImageView];
    }
    return _firstImageView;
}

- (UIImageView *)secondImageView {
    
    if(_secondImageView == nil) {
        _secondImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"portrait"]];
        _secondImageView.layer.cornerRadius = 50*0.5;
        _secondImageView.layer.masksToBounds = YES;
        _secondImageView.layer.borderColor = [[UIColor lightGrayColor]CGColor];
        _secondImageView.layer.borderWidth = 0.5;
        [self.view addSubview:_secondImageView];
    }
    return _secondImageView;
}

- (UIImageView *)thirdImageView {
    
    if(_thirdImageView == nil) {
        _thirdImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"portrait"]];
        _thirdImageView.layer.cornerRadius = 50*0.5;
        _thirdImageView.layer.masksToBounds = YES;
        _thirdImageView.layer.borderColor = [[UIColor lightGrayColor]CGColor];
        _thirdImageView.layer.borderWidth = 0.5;
        [self.view addSubview:_thirdImageView];
    }
    return _thirdImageView;
}

- (UILabel *)firstNameLabel {
    
    if(_firstNameLabel == nil) {
        _firstNameLabel = [[UILabel alloc] init];
        _firstNameLabel.textColor = HEXCOLOR(0x3D3D3D);
        _firstNameLabel.font = [UIFont systemFontOfSize:16];
        [self.view addSubview:_firstNameLabel];
    }
    return _firstNameLabel;
}

- (UILabel *)secondNameLabel {
    
    if(_secondNameLabel == nil) {
        _secondNameLabel = [[UILabel alloc] init];
        _secondNameLabel.textColor = HEXCOLOR(0x3D3D3D);
        _secondNameLabel.font = [UIFont systemFontOfSize:16];
        [self.view addSubview:_secondNameLabel];
    }
    return _secondNameLabel;
}

- (UILabel *)thirdNameLabel {
    
    if(_thirdNameLabel == nil) {
        _thirdNameLabel = [[UILabel alloc] init];
        _thirdNameLabel.font = [UIFont systemFontOfSize:16];
        _thirdNameLabel.textColor = HEXCOLOR(0x3D3D3D);
        [self.view addSubview:_thirdNameLabel];
    }
    return _thirdNameLabel;
}

- (UILabel *)firstNumLabel {
    
    if(_firstNumLabel == nil) {
        _firstNumLabel = [[UILabel alloc] init];
        _firstNumLabel.font = [UIFont systemFontOfSize:14];
        _firstNumLabel.textColor = HEXCOLOR(0xFFAB05);
        [self.view addSubview:_firstNumLabel];
    }
    return _firstNumLabel;
}

- (UILabel *)secondNumLabel {
    
    if(_secondNumLabel == nil) {
        _secondNumLabel = [[UILabel alloc] init];
        _secondNumLabel.font = [UIFont systemFontOfSize:14];
        _secondNumLabel.textColor = HEXCOLOR(0xFFAB05);
        [self.view addSubview:_secondNumLabel];
    }
    return _secondNumLabel;
}

- (UILabel *)thirdNumLabel {
    
    if(_thirdNumLabel == nil) {
        _thirdNumLabel = [[UILabel alloc] init];
        _thirdNumLabel.font = [UIFont systemFontOfSize:14];
        _thirdNumLabel.textColor = HEXCOLOR(0xFFAB05);
        [self.view addSubview:_thirdNumLabel];
    }
    return _thirdNumLabel;
}

- (XZNaviV *)categoryNavi {
    
    if(_categoryNavi == nil) {
        _categoryNavi = [[XZNaviV alloc] init];
        __weak typeof(self) weakSelf = self;
        _categoryNavi = [[XZNaviV alloc]initWithFrame:CGRectMake(10, 0, SCREENWIDTH, 39) ItemArray:@[@"点赞", @"被点赞"] itemClickBlock:^(NSInteger tag) {
            [weakSelf geniusNetWorkingWithType:(int)(tag-1)];
            _isRankNotRanked = tag == 1;
        } btnMoreBlock:^(UIButton *btnMore) {}];
#warning to do 循环引用
        //        [self.view addSubview:self.categoryNavi];
        [self.view addSubview:_categoryNavi];
    }
    return _categoryNavi;
}

- (UIImageView *)firstPlatformImageView {
    
    if(_firstPlatformImageView == nil) {
        _firstPlatformImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"first"]];
        [self.view addSubview:_firstPlatformImageView];
    }
    return _firstPlatformImageView;
}

- (UIImageView *)secondPlatformImageView {
    
    if(_secondPlatformImageView == nil) {
        _secondPlatformImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"second"]];
        [self.view addSubview:_secondPlatformImageView];
    }
    return _secondPlatformImageView;
}

- (UIImageView *)thirdPlatformImageView {
    
    if(_thirdPlatformImageView == nil) {
        _thirdPlatformImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"third"]];
        [self.view addSubview:_thirdPlatformImageView];
    }
    return _thirdPlatformImageView;
}

- (UIView *)separateView {
    
	if(_separateView == nil) {
		_separateView = [[UIView alloc] init];
        _separateView.backgroundColor = HEXCOLOR(0xDFDFDF);
        [self.view addSubview:_separateView];
	}
	return _separateView;
}

@end
