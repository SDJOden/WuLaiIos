//
//  WYLoveTypeModel.h
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/5/18.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WYLoveTypeModel : NSObject

@property (nonatomic, copy) NSString *key;

@property (nonatomic, copy) NSString *type;

@property (nonatomic, assign) BOOL selected;

+(NSArray *)createLoveTypeModelWithArrayWithCategory:(NSString *)category;

// --------------转模型需要规范化---------- 以下这种
//+(instancetype)createLoveSessionWithDict:(NSDictionary *)dict;
//
//-(instancetype)initLoveSessionWithDict:(NSDictionary *)dict;

@end
