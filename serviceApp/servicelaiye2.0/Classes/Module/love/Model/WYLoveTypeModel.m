//
//  WYLoveTypeModel.m
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/5/18.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import "WYLoveTypeModel.h"

@implementation WYLoveTypeModel

+(NSArray *)createLoveTypeModelWithArrayWithCategory:(NSString *)category{
    NSMutableArray *mutableArray = [NSMutableArray array];
    for (NSString *key in [NSMutableArray arrayWithArray:[[SDJUserAccount loadAccount].serviceIdsDic allKeys]]) {
        WYLoveTypeModel *loveTypeModel = [self createLoveTypeModel:[SDJUserAccount loadAccount].serviceIdsDic[key] andKey:key];
        if ([loveTypeModel.key isEqualToString:category]) {
            loveTypeModel.selected = YES;
        }
        [mutableArray addObject:loveTypeModel];
    }
    NSArray *returnArr = [[mutableArray copy] sortedArrayUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"key" ascending:YES]]];
    
    return returnArr;
}

+(instancetype)createLoveTypeModel:(NSString *)type andKey:(NSString *)key{
    
    return [[self alloc]initLoveTypeModel:type andKey:key];
}

-(instancetype)initLoveTypeModel:(NSString *)type andKey:(NSString *)key{
    
    if (self = [super init]) {
        _key = key;
        _type = type;
        _selected = NO;
    }
    return self;
}

@end
