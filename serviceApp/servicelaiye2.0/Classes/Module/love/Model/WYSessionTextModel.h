//
//  WYSessionTextModel.h
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/5/20.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WYSessionTextModel : NSObject

@property (nonatomic, copy) NSString *text;

@property (nonatomic, assign) int direction;

@property (nonatomic, copy) NSString *type;

@property (nonatomic, copy) NSString *identify;

@property (nonatomic, copy) NSString *time;

+(instancetype)createLoveSessionWithDict:(NSDictionary *)dict;

-(instancetype)initLoveSessionWithDict:(NSDictionary *)dict;

@end
