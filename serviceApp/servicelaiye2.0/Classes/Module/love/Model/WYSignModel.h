//
//  WYSignModel.h
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/8/16.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WYSignModel : NSObject

@property (nonatomic, copy) NSString *doc_id;

@property (nonatomic, copy) NSString *query;

@property (nonatomic, strong) NSArray *show_label;

@property (nonatomic, copy) NSString *time;

+(instancetype)createModelWithDic:(NSDictionary *)dic;

-(instancetype)initModelWithDic:(NSDictionary *)dic;

@end

