//
//  WYSessionTextModel.m
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/5/20.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import "WYSessionTextModel.h"

@implementation WYSessionTextModel

+(instancetype)createLoveSessionWithDict:(NSDictionary *)dict{
    
    return [[self alloc]initLoveSessionWithDict:dict];
}

-(instancetype)initLoveSessionWithDict:(NSDictionary *)dict{
    
    if (self = [super init]) {
        NSString *text = dict[@"text"];
        NSString *pattern = @"<a href=\'(.*?)\'>(.*?)</a>";
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern options:0 error:nil];
        NSArray *matches = [regex matchesInString:text options:0 range:NSMakeRange(0, text.length)]; // 网址
        NSUInteger lastIdx = 0;
        NSMutableString *attributedString = [NSMutableString stringWithFormat:@""]; // 总字符串
        NSMutableString *realTextString = [NSMutableString stringWithFormat:@""];
        if (matches.count) { // 解析部分bug, 1.文字需要提出来, 2. 位置需要调整 matchs.count
            NSMutableArray *urlArrs = [[NSMutableArray alloc] init]; // 存url
            int i = 0;
            for (NSTextCheckingResult* match in matches)
            {
                NSRange range = match.range;
                if (range.location > lastIdx)
                {
                    NSString *temp = [text substringWithRange:NSMakeRange(lastIdx, range.location - lastIdx)];
                    [attributedString appendString:temp];// 记录头
                    [realTextString appendString:temp];
                }
                NSString *title = [text substringWithRange:[match rangeAtIndex:2]]; // 标题
                NSString *url = [text substringWithRange:[match rangeAtIndex:1]]; // 网址
                lastIdx = range.location + range.length;
                [attributedString appendFormat:@"<%d>%@</%d>",i,title,i];
                [realTextString appendString:title];
                [urlArrs addObject:url];
                i++;
            }
            if (lastIdx < text.length)
            {
                NSString *temp = [text substringFromIndex:lastIdx];
                [attributedString appendString:temp];// 记录尾
                [realTextString appendString:temp];
            }
        } else {
            [realTextString appendString:dict[@"text"]];
        }
        _text = realTextString;
        _direction = ((NSNumber *)dict[@"direction"]).intValue;
        _type = dict[@"type"];
        _identify = dict[@"id"];
        _time = dict[@"time"];
    }
    return self;
}

@end
