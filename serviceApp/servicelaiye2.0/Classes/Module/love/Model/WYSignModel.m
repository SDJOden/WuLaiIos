//
//  WYSignModel.m
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/8/16.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import "WYSignModel.h"

@implementation WYSignModel

+(instancetype)createModelWithDic:(NSDictionary *)dic {
    
    return [[self alloc]initModelWithDic:dic];
}

-(instancetype)initModelWithDic:(NSDictionary *)dic {
    if (self = [super init]) {
#warning to do
        //-----------------直接导, 看用户端代码----------------------------------
        _doc_id = dic[@"doc_id"];
        _query = dic[@"query"];
        _show_label = dic[@"show_label"];
        _time = dic[@"time"];
    }
    return self;
}

@end
