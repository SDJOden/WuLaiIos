//
//  WYLoveSessionModel.h
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/5/17.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WYLoveSessionModel : NSObject

@property (nonatomic, copy) NSString *domain;

@property (nonatomic, copy) NSString *like;

@property (nonatomic, copy) NSString *reason;

@property (nonatomic, copy) NSString *timestamp;

@property (nonatomic, copy) NSMutableArray *session;

@property (nonatomic, copy) NSString *genius_id;

@property (nonatomic, copy) NSString *doc_id;

@property (nonatomic, assign) BOOL isEnabled;

@property (nonatomic, assign) BOOL isHate;

+(instancetype)createLoveSessionModelWithDict:(NSDictionary *)dict;

-(instancetype)initLoveSessionModelWithDict:(NSDictionary *)dict;

@end
