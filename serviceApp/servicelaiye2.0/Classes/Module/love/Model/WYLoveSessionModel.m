//
//  WYLoveSessionModel.m
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/5/17.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import "WYLoveSessionModel.h"
#import "WYSessionTextModel.h"

@implementation WYLoveSessionModel

+(instancetype)createLoveSessionModelWithDict:(NSDictionary *)dict{
    
    return [[self alloc]initLoveSessionModelWithDict:dict];
}

-(instancetype)initLoveSessionModelWithDict:(NSDictionary *)dict{
    
    if (self = [super init]) {
        
        for (NSDictionary *dic in dict[@"session"]) {
            [self.session addObject:[WYSessionTextModel createLoveSessionWithDict:dic]];
        }
        _domain = dict[@"domain"];
        _like = ((NSNumber *)dict[@"like"]).stringValue;
        _reason = dict[@"reason"];
        _timestamp = dict[@"timestamp"];
        if ([[dict allKeys]containsObject:@"genius_id"]) {
            _genius_id = dict[@"genius_id"];
        } else {
            _genius_id = @"0";
        }
        _doc_id = dict[@"doc_id"];
        _isEnabled = [dict[@"has_like"] boolValue];
        _isHate = NO;
    }
    return self;
}

- (NSMutableArray *)session {
    
    if(_session == nil) {
        _session = [[NSMutableArray alloc] init];
    }
    return _session;
}

@end
