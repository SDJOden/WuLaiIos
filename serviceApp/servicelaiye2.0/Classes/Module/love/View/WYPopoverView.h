//
//  WYPopoverView.h
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/8/18.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol WYPopoverViewDelegate <NSObject>

-(void)closePopoverView;

-(void)chooseQueryType:(NSString *)queryType;

@end

@interface WYPopoverView : UIView

@property (nonatomic, weak) id <WYPopoverViewDelegate> wyPopoverViewDelegate;

@end
