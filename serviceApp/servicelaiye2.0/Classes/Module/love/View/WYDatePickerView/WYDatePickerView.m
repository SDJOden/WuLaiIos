//
//  WYDatePickerView.m
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/8/11.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import "WYDatePickerView.h"

typedef enum : NSInteger {
    WYDAYS_28 = 28,
    WYDAYS_29 = 29,
    WYDAYS_30 = 30,
    WYDAYS_31 = 31
} WYDaysStyle;

@interface WYDatePickerView () <UIPickerViewDelegate, UIPickerViewDataSource>

@property (nonatomic, strong) NSMutableArray *yearArray;
@property (nonatomic, strong) NSMutableArray *monthArray;
@property (nonatomic, strong) NSMutableArray *dayArray;
@property (nonatomic, assign) int showYear;
@property (nonatomic, assign) int showMonth;
@property (nonatomic, assign) int showDay;
@property (nonatomic, assign) WYDaysStyle daysStyle;
@property (nonatomic, strong) NSDateComponents *dateComponents;
@property (nonatomic, strong) UIButton *coverButton;
@property (nonatomic, strong) UIView *backgroundView;
@property (nonatomic, strong) UIPickerView *pickerView;
@property (nonatomic, strong) UIButton *cancelButton;
@property (nonatomic, strong) UIButton *completeButton;

@end

@implementation WYDatePickerView

-(instancetype)init{
    
    if (self = [super init]) {
    }
    return self;
}

-(void)setStringDate:(NSString *)stringDate{
    
    _stringDate = [stringDate copy];
    [self layout];
    [self rcinit];
}

-(void)rcinit{
    
    NSDate *date = [WYTool dateFromeDateStr:_stringDate];
    [self yearAndMonthAndDayWithDate:date];
}

-(void)layout{
    
    [self.coverButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.bottom.equalTo(self);
    }];
    
    [self.backgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self);
        make.height.equalTo(208);
    }];
    
    [self.cancelButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.backgroundView);
        make.top.equalTo(self.backgroundView).offset(5);
        make.height.equalTo(30);
        make.width.equalTo(60);
    }];
    
    [self.completeButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.backgroundView);
        make.top.equalTo(self.backgroundView).offset(5);
        make.height.equalTo(30);
        make.width.equalTo(60);
    }];
    
    [self.pickerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.cancelButton.bottom);
        make.left.right.bottom.equalTo(self);
    }];
}

#pragma mark --- UIPickerViewDelegate
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    
    return 3;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
    if (component == 0) {
        return self.yearArray.count;
    } else if (component == 1) {
        return self.monthArray.count;
    } else {
        return self.dayArray.count;
    }
}

-(NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    if (component == 0) {
        return [self.yearArray[row] stringValue];
    } else if (component == 1) {
        return [NSString stringWithFormat:@"%@月", self.monthArray[row]];
    } else {
        return [NSString stringWithFormat:@"%@日", self.dayArray[row]];
    }
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
    if (component == 0) {
        _showYear = [self.yearArray[row] intValue];
        [self changeYearAutoChangeDayArray];
    } else if (component == 1) {
        _showMonth = [self.monthArray[row] intValue];
        [self changeMonthAutoChangeDayArray];
    } else {
        _showDay = [self.dayArray[row] intValue];
    }
}

-(BOOL)dayArrayChangeResource{
    
    if (_showYear%400 == 0 || (_showYear%100 != 0 && _showYear%4 == 0)) {
        if (_daysStyle != WYDAYS_29) {
            _daysStyle = WYDAYS_29;
            return YES;
        } else {
            return NO;
        }
    } else {
        if (_daysStyle != WYDAYS_28) {
            _daysStyle = WYDAYS_28;
            return YES;
        } else {
            return NO;
        }
    }
}

/**
 *  改变年份从而判断是否要改变日期的数据源
 */
-(void)changeYearAutoChangeDayArray{
    
    if (_showMonth == 2 && [self dayArrayChangeResource]) {
        [self changeDaysArrWith:_daysStyle];
    }
}

-(void)changeMonthAutoChangeDayArray{
    
    if (_showMonth == 1 || _showMonth == 3 || _showMonth == 5 || _showMonth == 7 || _showMonth == 8 || _showMonth == 10 || _showMonth == 12) {
        if (_daysStyle != WYDAYS_31) {
            _daysStyle = WYDAYS_31;
            [self changeDaysArrWith:_daysStyle];
        }
    } else if (_showMonth == 2) {
        if ([self dayArrayChangeResource]) {
            [self changeDaysArrWith:_daysStyle];
        }
    } else {
        if (_daysStyle != WYDAYS_30) {
            _daysStyle = WYDAYS_30;
            [self changeDaysArrWith:_daysStyle];
        }
    }
}

-(void)changeDaysArrWith:(WYDaysStyle)daysStyle{
    
    [self.dayArray removeAllObjects];
    for (int i = 1; i <= daysStyle; i ++) {
        [self.dayArray addObject:@(i)];
    }
    [self.pickerView reloadComponent:2];
    [self.pickerView selectRow:_showDay>daysStyle?daysStyle-1:_showDay-1 inComponent:2 animated:YES];
}

-(CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
    
    return 32.5;
}

#pragma mark --- 点击事件
-(void)closePickerView{
    
    if ([self.wyDatePickerViewDelegate respondsToSelector:@selector(closeDatePickerView)]) {
        [self.wyDatePickerViewDelegate closeDatePickerView];
    }
}

-(void)completeChooseCategory{
    
    if ([self.wyDatePickerViewDelegate respondsToSelector:@selector(chooseDate:)]) {
        [self.wyDatePickerViewDelegate chooseDate:[NSString stringWithFormat:@"%d%02d%02d", _showYear, _showMonth, _showDay]];
    }
}

#pragma mark --- 懒加载
- (UIPickerView *)pickerView {
    
    if(_pickerView == nil) {
        _pickerView = [[UIPickerView alloc] init];
        _pickerView.delegate = self;
        [self.backgroundView addSubview:_pickerView];
    }
    return _pickerView;
}

- (UIButton *)cancelButton {
    
    if(_cancelButton == nil) {
        _cancelButton = [[UIButton alloc] init];
        [_cancelButton setTitle:@"取消" forState:UIControlStateNormal];
        [_cancelButton setTitleColor:HEXCOLOR(0xACACAC) forState:UIControlStateNormal];
        _cancelButton.titleLabel.font = [UIFont systemFontOfSize:16];
        [_cancelButton addTarget:self action:@selector(closePickerView) forControlEvents:UIControlEventTouchUpInside];
        [self.backgroundView addSubview:_cancelButton];
    }
    return _cancelButton;
}

- (UIButton *)completeButton {
    
    if(_completeButton == nil) {
        _completeButton = [[UIButton alloc] init];
        [_completeButton setTitle:@"确定" forState:UIControlStateNormal];
        [_completeButton setTitleColor:HEXCOLOR(0x25C1FF) forState:UIControlStateNormal];
        _completeButton.titleLabel.font = [UIFont systemFontOfSize:16];
        [_completeButton addTarget:self action:@selector(completeChooseCategory) forControlEvents:UIControlEventTouchUpInside];
        [self.backgroundView addSubview:_completeButton];
    }
    return _completeButton;
}

- (UIButton *)coverButton {
    
    if(_coverButton == nil) {
        _coverButton = [[UIButton alloc] init];
        _coverButton.backgroundColor = HEXCOLOR(0x000000);
        _coverButton.alpha = 0.6;
        [_coverButton addTarget:self action:@selector(closePickerView) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_coverButton];
    }
    return _coverButton;
}

- (UIView *)backgroundView {
    
    if(_backgroundView == nil) {
        _backgroundView = [[UIView alloc] init];
        _backgroundView.backgroundColor = HEXCOLOR(0xffffff);
        [self addSubview:_backgroundView];
    }
    return _backgroundView;
}

-(void)yearAndMonthAndDayWithDate:(NSDate *)date{
    
    NSCalendar *greCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    _dateComponents = [greCalendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:date];
    
    _showYear = (int)self.dateComponents.year;
    _showMonth = (int)self.dateComponents.month;
    _showDay = (int)self.dateComponents.day;
    
    [self.yearArray removeAllObjects];
    [self.monthArray removeAllObjects];
    [self.dayArray removeAllObjects];
    
    for (int i = 2015; i <= _showYear; i++) {
        [self.yearArray addObject:@(i)];
    }
    
    for (int i = 1; i <= 12; i++) {
        [_monthArray addObject:@(i)];
    }
    
    if (_showMonth == 1 || _showMonth == 3 || _showMonth == 5 || _showMonth == 7 || _showMonth == 8 || _showMonth == 10 || _showMonth == 12) {
        _daysStyle = WYDAYS_31;
    } else if (_showMonth == 2) {
        if (_showYear%400 == 0 || (_showYear%100 != 0 && _showYear%4 == 0)) {
            _daysStyle = WYDAYS_29;
        }
        else {
            _daysStyle = WYDAYS_28;
        }
    } else {
        _daysStyle = WYDAYS_30;
        
    }
    for (int i = 1; i <= _daysStyle; i ++) {
        [self.dayArray addObject:@(i)];
    }
    
    [self.pickerView reloadAllComponents];
    [self.pickerView selectRow:_showYear-2015 inComponent:0 animated:NO];
    [self.pickerView selectRow:_showMonth-1 inComponent:1 animated:NO];
    [self.pickerView selectRow:_showDay-1 inComponent:2 animated:NO];
}

- (NSMutableArray *)yearArray {
    
	if(_yearArray == nil) {
		_yearArray = [[NSMutableArray alloc] init];
        for (int i = 2015; i <= self.dateComponents.year; i++) {
            [_yearArray addObject:@(i)];
        }
	}
	return _yearArray;
}

- (NSMutableArray *)monthArray {
    
	if(_monthArray == nil) {
		_monthArray = [[NSMutableArray alloc] init];
	}
	return _monthArray;
}

- (NSMutableArray *)dayArray {
    
	if(_dayArray == nil) {
		_dayArray = [[NSMutableArray alloc] init];
	}
	return _dayArray;
}

@end
