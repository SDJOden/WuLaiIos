//
//  WYDatePickerView.h
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/8/11.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol WYDatePickerViewDelegate <NSObject>

-(void)chooseDate:(NSString *)dateStr;

-(void)closeDatePickerView;

@end

@interface WYDatePickerView : UIView

@property (nonatomic, strong) NSString *stringDate;

@property (nonatomic, weak) id <WYDatePickerViewDelegate> wyDatePickerViewDelegate;

@end
