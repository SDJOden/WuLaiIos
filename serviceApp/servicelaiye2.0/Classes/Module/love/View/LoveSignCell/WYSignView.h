//
//  WYSignView.h
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/8/16.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import <UIKit/UIKit.h>

@class WYLoveSignCell;

@protocol WYSignViewDelegate <NSObject>

-(void)requestNewData;

@end

@interface WYSignView : UIView

@property (nonatomic, strong) WYLoveSignCell *selectCell;

@property (nonatomic, weak) id <WYSignViewDelegate> wySignViewDelegate;

@property (nonatomic, strong) NSArray *dataArray;

-(void)nextShowSignDataWithAnimate;

@end
