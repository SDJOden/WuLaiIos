//
//  WYSignView.m
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/8/16.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import "WYSignView.h"
#import "WYLoveSignCell.h"

#define MARGIN (IS_IPHONE_5?30:50)

@interface WYSignView ()

@property (nonatomic, strong) WYLoveSignCell *firstCell;

@property (nonatomic, strong) WYLoveSignCell *secondCell;

@property (nonatomic, strong) WYLoveSignCell *thirdCell;

@property (nonatomic, assign) int num;

@property (nonatomic, strong) UIButton *nextSignButton;

@end

@implementation WYSignView

-(instancetype)init{
    
    if (self = [super init]) {
        
        [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(nextSignButtonEnabel) name:@"wySignCategory" object:nil];
    }
    return self;
}

-(void)setDataArray:(NSArray *)dataArray{
    
    _dataArray = [dataArray copy];
    
    [self.thirdCell removeFromSuperview];
    [self.secondCell removeFromSuperview];
    [self.firstCell removeFromSuperview];
    self.thirdCell = nil;
    self.secondCell = nil;
    self.firstCell = nil;
    
    //---------------空格---------------
#warning to do
    if (self.dataArray.count>3) {
        _num = 3;
        self.thirdCell.frame = CGRectMake(0, 0, self.w, self.h-24-MARGIN-44);
        self.secondCell.frame = CGRectMake(0, 12, self.w, self.h-24-MARGIN-44);
        self.firstCell.frame = CGRectMake(0, 24, self.w, self.h-24-MARGIN-44);
        
        self.thirdCell.model = _dataArray[_num-1];
        self.secondCell.model = _dataArray[_num-2];
        self.firstCell.model = _dataArray[_num-3];
        
        self.thirdCell.frame = CGRectMake(24, 0, self.w - 48, self.h-24-MARGIN-44);
        self.secondCell.frame = CGRectMake(12, 12, self.w - 24, self.h-24-MARGIN-44);
        
        self.selectCell = self.firstCell;
    }
    
    CGFloat y = self.thirdCell.y+self.thirdCell.h+MARGIN+44-24;
    
    self.nextSignButton.frame = CGRectMake(0, y, SCREENWIDTH - 24, 44);
    
    self.nextSignButton.layer.cornerRadius = 22.0;
    self.nextSignButton.layer.masksToBounds = YES;
    self.nextSignButton.enabled = NO;
}

-(void)nextShowSignDataWithAnimate{
    
    if (self.dataArray.count == 0) {
        if ([self.wySignViewDelegate respondsToSelector:@selector(requestNewData)]) {
            [self.wySignViewDelegate requestNewData];
        }
        return ;
    }
    
    //---------------weakself
#warning to do
    [UIView animateWithDuration:0.2 animations:^{
        self.selectCell.transform = CGAffineTransformMakeRotation(-10*M_PI/180);
        self.selectCell.center = CGPointMake(-(SCREENWIDTH - 24)*0.5, _selectCell.center.y - MARGIN);
        [self.secondCell showToHidden];
    } completion:^(BOOL finished) {
        [self.selectCell removeFromSuperview];
        self.selectCell = nil;
        [self animateOfThreeCell];
    }];
}

-(void)animateOfThreeCell{
    
    //---------------weakself
#warning to do
    if (_num - self.dataArray.count == 0) {
        self.firstCell = self.secondCell;
        self.secondCell = self.thirdCell;
        self.thirdCell = self.selectCell;
        [self.thirdCell removeFromSuperview];
        self.thirdCell = nil;
        self.selectCell = self.firstCell;
        [UIView animateWithDuration:0.2 animations:^{
            self.firstCell.frame = CGRectMake(0, 24, self.w, self.h - 24-MARGIN-44);
            self.secondCell.frame = CGRectMake(12, 12, self.w - 24, self.h - 24-MARGIN-44);
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.5 animations:^{
                [self.firstCell hiddenToShow];
            }];
        }];
    } else if (_num - self.dataArray.count == 1) {
        self.firstCell = _secondCell;
        self.secondCell = _thirdCell;
        [self.secondCell removeFromSuperview];
        self.secondCell = nil;
        self.selectCell = self.firstCell;
        [UIView animateWithDuration:0.2 animations:^{
            self.firstCell.frame = CGRectMake(0, 24, self.w, self.h - 24-MARGIN-44);
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.5 animations:^{
                [self.firstCell hiddenToShow];
            }];
        }];
    } else if (_num - self.dataArray.count == 2) {
        [self.firstCell removeFromSuperview];
        self.firstCell = nil;
        if ([self.wySignViewDelegate respondsToSelector:@selector(requestNewData)]) {
            [self.wySignViewDelegate requestNewData];
        }
    } else {
        self.firstCell = self.secondCell;
        self.secondCell = self.thirdCell;
        self.thirdCell = self.selectCell;
        self.selectCell = self.firstCell;
        self.thirdCell.model = self.dataArray[_num];
        self.thirdCell.frame = CGRectMake(24, 0, self.w - 48, self.h - 24-MARGIN-44);
        [self sendSubviewToBack:self.thirdCell];
        [UIView animateWithDuration:0.2 animations:^{
            self.thirdCell.frame = CGRectMake(24, 0, self.w - 48, self.h - 24-MARGIN-44);
            self.secondCell.frame = CGRectMake(12, 12, self.w - 24, self.h - 24-MARGIN-44);
            self.firstCell.frame = CGRectMake(0, 24, self.w, self.h - 24-MARGIN-44);
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.5 animations:^{
                [self.firstCell hiddenToShow];
            }];
        }];
    }
    _num ++;
}

- (WYLoveSignCell *)firstCell {
    
	if(_firstCell == nil) {
		_firstCell = [[WYLoveSignCell alloc] initWithFrame:CGRectMake(0, 0, self.w, self.h-24-MARGIN-44)];
        [self addSubview:_firstCell];
	}
	return _firstCell;
}

- (WYLoveSignCell *)secondCell {
    
	if(_secondCell == nil) {
		_secondCell = [[WYLoveSignCell alloc] initWithFrame:CGRectMake(0, 0, self.w, self.h-24-MARGIN-44)];
        [self addSubview:_secondCell];
	}
	return _secondCell;
}

- (WYLoveSignCell *)thirdCell {
    
	if(_thirdCell == nil) {
		_thirdCell = [[WYLoveSignCell alloc] initWithFrame:CGRectMake(0, 0, self.w, self.h-24-MARGIN-44)];
        [self addSubview:_thirdCell];
	}
	return _thirdCell;
}

- (UIButton *)nextSignButton {
    
    if(_nextSignButton == nil) {
        _nextSignButton = [[UIButton alloc] init];
        [_nextSignButton setTitle:@"下一个" forState:UIControlStateNormal];
        [_nextSignButton setTitleColor:HEXCOLOR(0x3E3E3E) forState:UIControlStateNormal];
        [_nextSignButton setTitleColor:HEXCOLOR(0xFFFFFF) forState:UIControlStateDisabled];
        _nextSignButton.titleLabel.font = [UIFont systemFontOfSize:14];
        [_nextSignButton setBackgroundImage:[UIImage getRoundImageWithSize:CGSizeMake(SCREENWIDTH - 24, 44) backgroundColor:HEXCOLOR(0xFFD600) lineWidth:0 cornerRadius:0 shouldFill:YES] forState:UIControlStateNormal];
        [_nextSignButton setBackgroundImage:[UIImage getRoundImageWithSize:CGSizeMake(SCREENWIDTH - 24, 44) backgroundColor:HEXCOLOR(0xD3D3D3) lineWidth:0 cornerRadius:0 shouldFill:YES] forState:UIControlStateDisabled];
        [_nextSignButton addTarget:self action:@selector(nextCellAnimate) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_nextSignButton];
    }
    return _nextSignButton;
}

-(void)nextCellAnimate{
    
    self.nextSignButton.enabled = NO;
    [self nextShowSignDataWithAnimate];
}

-(void)nextSignButtonEnabel{
    
    self.nextSignButton.enabled = YES;
}

@end
