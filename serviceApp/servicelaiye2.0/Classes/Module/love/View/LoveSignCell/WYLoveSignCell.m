//
//  WYLoveSignCell.m
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/8/12.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import "WYLoveSignCell.h"
#import "WYSignModel.h"
#import "WYNetworkTools.h"

@interface WYLoveSignCell ()

@property (nonatomic, strong) UIImageView *signImageView;
@property (nonatomic, strong) UILabel *contentLabel;
@property (nonatomic, strong) UIView *separateView;
@property (nonatomic, strong) UIButton *firstCategoryButton;
@property (nonatomic, strong) UIButton *secondCategoryButton;
@property (nonatomic, strong) UIButton *thirdCategoryButton;
@property (nonatomic, strong) UIButton *nullCategoryButton;
@property (nonatomic, strong) UIButton *categoryButton;

@end

@implementation WYLoveSignCell

-(void)setModel:(WYSignModel *)model{
    
    _model = model;
    
    //-------------
    self.backgroundColor = [UIColor whiteColor];
    self.layer.cornerRadius = 4.0;
    self.layer.masksToBounds = YES;
    self.layer.borderColor = [HEXCOLOR(0xE3E3E3)CGColor];
    self.layer.borderWidth = 0.5;
    
    //--------------
    [self.firstCategoryButton removeFromSuperview];
    [self.secondCategoryButton removeFromSuperview];
    [self.thirdCategoryButton removeFromSuperview];
    [self.nullCategoryButton removeFromSuperview];
    [self.categoryButton removeFromSuperview];
    self.firstCategoryButton = nil;
    self.secondCategoryButton = nil;
    self.thirdCategoryButton = nil;
    self.nullCategoryButton = nil;
    self.categoryButton = nil;
    
    self.contentLabel.text = [NSString stringWithFormat:@"Q：%@", model.query];
    [self.firstCategoryButton setTitle:model.show_label[0] forState:UIControlStateNormal];
    [self.secondCategoryButton setTitle:model.show_label[1] forState:UIControlStateNormal];
    [self.thirdCategoryButton setTitle:model.show_label[2] forState:UIControlStateNormal];
    
    [self.firstCategoryButton sizeToFit];
    [self.secondCategoryButton sizeToFit];
    [self.thirdCategoryButton sizeToFit];
    [self.nullCategoryButton sizeToFit];
    [self.signImageView sizeToFit];
    self.signImageView.frame = CGRectMake(12, 12, self.signImageView.w, self.signImageView.h);
    
    CGFloat buttonWidthExtra = 24;
    if (IS_IPHONE_5) {
        buttonWidthExtra = 12;
    }
    
    CGFloat margin = (self.w - (self.firstCategoryButton.w + buttonWidthExtra) - (self.secondCategoryButton.w + buttonWidthExtra) - (self.thirdCategoryButton.w + buttonWidthExtra)- (self.nullCategoryButton.w + buttonWidthExtra) - (self.categoryButton.w + buttonWidthExtra))/6.0;
    self.firstCategoryButton.frame = CGRectMake(margin, self.h - 28 - 30, self.firstCategoryButton.w + buttonWidthExtra, 28);
    self.secondCategoryButton.frame = CGRectMake(self.firstCategoryButton.x + self.firstCategoryButton.w + margin, self.h - 28 - 30, self.secondCategoryButton.w + buttonWidthExtra, 28);
    self.thirdCategoryButton.frame = CGRectMake(self.secondCategoryButton.x + self.secondCategoryButton.w + margin, self.h - 28 - 30, self.thirdCategoryButton.w + buttonWidthExtra, 28);
    self.nullCategoryButton.frame = CGRectMake(self.thirdCategoryButton.x + self.thirdCategoryButton.w + margin, self.h - 28 - 30, self.nullCategoryButton.w + buttonWidthExtra, 28);
    self.categoryButton.frame = CGRectMake(self.nullCategoryButton.x + self.nullCategoryButton.w + margin, self.h - 28 - 30, self.categoryButton.w + buttonWidthExtra, 28);
    
#warning to do // 耦合性高,
    [_firstCategoryButton setBackgroundImage:[UIImage getRoundImageWithSize:CGSizeMake(_firstCategoryButton.w, _firstCategoryButton.h) backgroundColor:[UIColor clearColor] lineWidth:0 cornerRadius:0 shouldFill:YES] forState:UIControlStateNormal];
    [_firstCategoryButton setBackgroundImage:[UIImage getRoundImageWithSize:CGSizeMake(_firstCategoryButton.w, _firstCategoryButton.h) backgroundColor:HEXCOLOR(0xFFD600) lineWidth:0 cornerRadius:0 shouldFill:YES] forState:UIControlStateSelected];
    [_secondCategoryButton setBackgroundImage:[UIImage getRoundImageWithSize:CGSizeMake(_secondCategoryButton.w, _secondCategoryButton.h) backgroundColor:[UIColor clearColor] lineWidth:0 cornerRadius:0 shouldFill:YES] forState:UIControlStateNormal];
    [_secondCategoryButton setBackgroundImage:[UIImage getRoundImageWithSize:CGSizeMake(_secondCategoryButton.w, _secondCategoryButton.h) backgroundColor:HEXCOLOR(0xFFD600) lineWidth:0 cornerRadius:0 shouldFill:YES] forState:UIControlStateSelected];
    [_thirdCategoryButton setBackgroundImage:[UIImage getRoundImageWithSize:CGSizeMake(_thirdCategoryButton.w, _thirdCategoryButton.h) backgroundColor:[UIColor clearColor] lineWidth:0 cornerRadius:0 shouldFill:YES] forState:UIControlStateNormal];
    [_thirdCategoryButton setBackgroundImage:[UIImage getRoundImageWithSize:CGSizeMake(_thirdCategoryButton.w, _thirdCategoryButton.h) backgroundColor:HEXCOLOR(0xFFD600) lineWidth:0 cornerRadius:0 shouldFill:YES] forState:UIControlStateSelected];
    
    UIImage *image = [UIImage imageNamed:@"upAndDown"];
    [self.categoryButton setTitleEdgeInsets:UIEdgeInsetsMake(0, -image.size.width, 0, image.size.width)];
    CGSize categorySize = [WYTool labelHeightWithText:self.categoryButton.titleLabel.text andWidth:MAXFLOAT andFont:[UIFont systemFontOfSize:14]];
    [self.categoryButton setImageEdgeInsets:UIEdgeInsetsMake(0, categorySize.width + 5, 0, -categorySize.width - 5)];
    
    self.separateView.frame = CGRectMake(0, self.firstCategoryButton.y - 30, self.w, 0.5);
    
    self.contentLabel.frame = CGRectMake(12, self.signImageView.y + self.signImageView.h + 12, self.w - 24, self.separateView.y - self.signImageView.y - self.signImageView.h - 12 - 12);
}

#pragma mark --- 按钮点击事件
-(void)chooseSignCategory:(UIButton *)sender{
    
    __weak __typeof__(self) weakSelf = self;
    
    [[WYNetworkTools shareTools]signMessageWithDocId:self.model.doc_id andCategory:sender.titleLabel.text finished:^(id responseObject, NSError *error) {
        if (responseObject) {
            if (weakSelf.signImageView.hidden) {
                [[NSNotificationCenter defaultCenter]postNotificationName:@"wySignCategory" object:nil];
            }
            [weakSelf buttonChangeStyle:sender];
        }
    }];
}

-(void)buttonChangeStyle:(UIButton *)sender{
    
    self.firstCategoryButton.selected = NO;
    self.secondCategoryButton.selected = NO;
    self.thirdCategoryButton.selected = NO;
    self.nullCategoryButton.selected = NO;
    self.categoryButton.selected = NO;
    self.firstCategoryButton.layer.borderWidth = 0.5;
    self.secondCategoryButton.layer.borderWidth = 0.5;
    self.thirdCategoryButton.layer.borderWidth = 0.5;
    self.nullCategoryButton.layer.borderWidth = 0.5;
    self.categoryButton.layer.borderWidth = 0.5;
    sender.selected = YES;
    sender.layer.borderWidth = 0;
    
    __weak __typeof__(self) weakSelf = self;
    
    self.signImageView.transform = CGAffineTransformMakeScale(0.2, 0.2);
    [UIView animateWithDuration:0.2 animations:^{
        weakSelf.signImageView.transform = CGAffineTransformIdentity;
        weakSelf.signImageView.hidden = NO;
    }];
}

-(void)didClickCategoryButton{
    
    [[NSNotificationCenter defaultCenter]postNotificationName:@"WYSignShowCategoryPickerView" object:nil];
}

-(void)signMsgWithCategory:(NSString *)category{
    
    //-------------weakself
#warning to do
    [[WYNetworkTools shareTools]signMessageWithDocId:self.model.doc_id andCategory:category finished:^(id responseObject, NSError *error) {
        if (responseObject) {
            [self buttonChangeStyle:self.categoryButton];
            [self.categoryButton setTitle:category forState:UIControlStateNormal];
            UIImage *image = [UIImage imageNamed:@"upAndDown"];
            [self.categoryButton setTitleEdgeInsets:UIEdgeInsetsMake(0, -image.size.width, 0, image.size.width)];
            CGSize categorySize = [WYTool labelHeightWithText:self.categoryButton.titleLabel.text andWidth:MAXFLOAT andFont:[UIFont systemFontOfSize:14]];
            [self.categoryButton setImageEdgeInsets:UIEdgeInsetsMake(0, categorySize.width+5, 0, -categorySize.width-5)];
            [[NSNotificationCenter defaultCenter]postNotificationName:@"wySignCategory" object:nil];
        }
    }];
}

#pragma mark --- hiddenToShow
-(void)hiddenToShow{
    
    self.contentLabel.hidden = NO;
    self.separateView.hidden = NO;
    self.firstCategoryButton.hidden = NO;
    self.secondCategoryButton.hidden = NO;
    self.thirdCategoryButton.hidden = NO;
    self.nullCategoryButton.hidden = NO;
    self.categoryButton.hidden = NO;
}

-(void)showToHidden{
    
    self.contentLabel.hidden = YES;
    self.separateView.hidden = YES;
    self.firstCategoryButton.hidden = YES;
    self.secondCategoryButton.hidden = YES;
    self.thirdCategoryButton.hidden = YES;
    self.nullCategoryButton.hidden = YES;
    self.categoryButton.hidden = YES;
}

#pragma mark --- 懒加载
- (UIImageView *)signImageView {
    
    if(_signImageView == nil) {
        _signImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"isSign"]];
        _signImageView.hidden = YES;
        [self addSubview:_signImageView];
    }
    return _signImageView;
}

- (UILabel *)contentLabel {
    
	if(_contentLabel == nil) {
		_contentLabel = [[UILabel alloc] init];
        _contentLabel.numberOfLines = 4;
        _contentLabel.font = [UIFont systemFontOfSize:23];
        _contentLabel.textColor = HEXCOLOR(0x595959);
        [self addSubview:_contentLabel];
	}
	return _contentLabel;
}

- (UIView *)separateView {
    
	if(_separateView == nil) {
		_separateView = [[UIView alloc] init];
        _separateView.backgroundColor = HEXCOLOR(0xDFDFDF);
        [self addSubview:_separateView];
	}
	return _separateView;
}

- (UIButton *)firstCategoryButton {
    
	if(_firstCategoryButton == nil) {
		_firstCategoryButton = [[UIButton alloc] init];
        [_firstCategoryButton setTitleColor:HEXCOLOR(0xA0A0A0) forState:UIControlStateNormal];
        [_firstCategoryButton setTitleColor:HEXCOLOR(0x3E3E3E) forState:UIControlStateSelected];
        _firstCategoryButton.titleLabel.font = [UIFont systemFontOfSize:14];
        _firstCategoryButton.layer.cornerRadius = 14.0;
        _firstCategoryButton.layer.borderWidth = 0.5;
        _firstCategoryButton.layer.borderColor = [HEXCOLOR(0xA0A0A0)CGColor];
        _firstCategoryButton.layer.masksToBounds = YES;
        [_firstCategoryButton addTarget:self action:@selector(chooseSignCategory:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_firstCategoryButton];
	}
	return _firstCategoryButton;
}

- (UIButton *)secondCategoryButton {
    
	if(_secondCategoryButton == nil) {
		_secondCategoryButton = [[UIButton alloc] init];
        [_secondCategoryButton setTitleColor:HEXCOLOR(0xA0A0A0) forState:UIControlStateNormal];
        [_secondCategoryButton setTitleColor:HEXCOLOR(0x3E3E3E) forState:UIControlStateSelected];
        _secondCategoryButton.titleLabel.font = [UIFont systemFontOfSize:14];
        _secondCategoryButton.layer.cornerRadius = 14.0;
        _secondCategoryButton.layer.borderWidth = 0.5;
        _secondCategoryButton.layer.borderColor = [HEXCOLOR(0xA0A0A0)CGColor];
        _secondCategoryButton.layer.masksToBounds = YES;
        [_secondCategoryButton addTarget:self action:@selector(chooseSignCategory:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_secondCategoryButton];
	}
	return _secondCategoryButton;
}

- (UIButton *)thirdCategoryButton {
    
	if(_thirdCategoryButton == nil) {
		_thirdCategoryButton = [[UIButton alloc] init];
        [_thirdCategoryButton setTitleColor:HEXCOLOR(0xA0A0A0) forState:UIControlStateNormal];
        [_thirdCategoryButton setTitleColor:HEXCOLOR(0x3E3E3E) forState:UIControlStateSelected];
        _thirdCategoryButton.titleLabel.font = [UIFont systemFontOfSize:14];
        _thirdCategoryButton.layer.cornerRadius = 14.0;
        _thirdCategoryButton.layer.borderWidth = 0.5;
        _thirdCategoryButton.layer.borderColor = [HEXCOLOR(0xA0A0A0)CGColor];
        _thirdCategoryButton.layer.masksToBounds = YES;
        [_thirdCategoryButton addTarget:self action:@selector(chooseSignCategory:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_thirdCategoryButton];
	}
	return _thirdCategoryButton;
}

- (UIButton *)nullCategoryButton {
    
	if(_nullCategoryButton == nil) {
		_nullCategoryButton = [[UIButton alloc] init];
        [_nullCategoryButton setTitle:@"无" forState:UIControlStateNormal];
        [_nullCategoryButton sizeToFit];
        [_nullCategoryButton setBackgroundImage:[UIImage getRoundImageWithSize:CGSizeMake(_nullCategoryButton.w, _nullCategoryButton.h) backgroundColor:[UIColor clearColor] lineWidth:0 cornerRadius:0 shouldFill:YES] forState:UIControlStateNormal];
        [_nullCategoryButton setBackgroundImage:[UIImage getRoundImageWithSize:CGSizeMake(_nullCategoryButton.w, _nullCategoryButton.h) backgroundColor:HEXCOLOR(0xFFD600) lineWidth:0 cornerRadius:0 shouldFill:YES] forState:UIControlStateSelected];
        [_nullCategoryButton setTitleColor:HEXCOLOR(0xA0A0A0) forState:UIControlStateNormal];
        [_nullCategoryButton setTitleColor:HEXCOLOR(0x3E3E3E) forState:UIControlStateSelected];
        _nullCategoryButton.titleLabel.font = [UIFont systemFontOfSize:14];
        _nullCategoryButton.layer.cornerRadius = 14.0;
        _nullCategoryButton.layer.borderWidth = 0.5;
        _nullCategoryButton.layer.borderColor = [HEXCOLOR(0xA0A0A0)CGColor];
        _nullCategoryButton.layer.masksToBounds = YES;
        [_nullCategoryButton addTarget:self action:@selector(chooseSignCategory:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_nullCategoryButton];
    }
	return _nullCategoryButton;
}

- (UIButton *)categoryButton {
    
	if(_categoryButton == nil) {
		_categoryButton = [[UIButton alloc] init];
        [_categoryButton setTitle:@"自定义" forState:UIControlStateNormal];
        [_categoryButton setImage:[UIImage imageNamed:@"upAndDown"] forState:UIControlStateNormal];
        [_categoryButton sizeToFit];
        [_categoryButton setBackgroundImage:[UIImage getRoundImageWithSize:CGSizeMake(_categoryButton.w, _categoryButton.h) backgroundColor:[UIColor clearColor] lineWidth:0 cornerRadius:0 shouldFill:YES] forState:UIControlStateNormal];
        [_categoryButton setBackgroundImage:[UIImage getRoundImageWithSize:CGSizeMake(_categoryButton.w, _categoryButton.h) backgroundColor:HEXCOLOR(0xFFD600) lineWidth:0 cornerRadius:0 shouldFill:YES] forState:UIControlStateSelected];
        [_categoryButton setTitleColor:HEXCOLOR(0xA0A0A0) forState:UIControlStateNormal];
        [_categoryButton setTitleColor:HEXCOLOR(0x3E3E3E) forState:UIControlStateSelected];
        _categoryButton.titleLabel.font = [UIFont systemFontOfSize:14];
        _categoryButton.layer.cornerRadius = 14.0;
        _categoryButton.layer.borderWidth = 0.5;
        _categoryButton.layer.borderColor = [HEXCOLOR(0xA0A0A0)CGColor];
        _categoryButton.layer.masksToBounds = YES;
        [_categoryButton addTarget:self action:@selector(didClickCategoryButton) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_categoryButton];
	}
	return _categoryButton;
}

-(void)dealloc{
    
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

@end
