//
//  WYLoveSignCell.h
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/8/12.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import <UIKit/UIKit.h>

@class WYSignModel;

@interface WYLoveSignCell : UIView

@property (nonatomic, strong) WYSignModel *model;

-(void)signMsgWithCategory:(NSString *)category;

-(void)showToHidden;

-(void)hiddenToShow;

@end
