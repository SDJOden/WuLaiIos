//
//  WYGeniusRankTableViewCell.h
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/5/4.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WYGeniusRankTableViewCell : UITableViewCell

-(void)createCellWithDict:(NSDictionary *)dict andIndex:(NSInteger)index;

@end
