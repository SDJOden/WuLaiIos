//
//  WYGeniusRankTableViewCell.m
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/5/4.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import "WYGeniusRankTableViewCell.h"
#import "WYGeniusModel.h"

@interface WYGeniusRankTableViewCell ()

@property (nonatomic, strong) UILabel *rankLabel;

@property (nonatomic, strong) UIImageView *iconImageView;

@property (nonatomic, strong) UILabel *geniusNameLabel;

@property (nonatomic, strong) UILabel *numLabel;

@end

@implementation WYGeniusRankTableViewCell

- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initialize];
    }
    return self;
}

-(instancetype)init{
    
    self = [super init];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize {}

-(void)layout{
    
    [self.rankLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(20);
        make.top.bottom.equalTo(self.contentView);
        make.width.equalTo(20);
        make.height.equalTo(44);
    }];
    
    [self.iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(10);
        make.left.equalTo(self.rankLabel.right).offset(6);
        make.bottom.equalTo(self.contentView).offset(-10);
        make.width.equalTo(self.contentView.h - 20);
    }];
    
    [self.geniusNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.iconImageView.right).offset(30);
        make.top.bottom.equalTo(self.contentView);
        make.width.equalTo(200);
    }];
    
    [self.numLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(-10);
        make.top.bottom.equalTo(self.contentView);
        make.width.equalTo(60);
    }];
}

-(void)createCellWithDict:(NSDictionary *)dict andIndex:(NSInteger)index{
    
    self.rankLabel.text = @(index + 4).stringValue;
    self.geniusNameLabel.text = @"";
    self.iconImageView.image = [UIImage imageNamed:@"portrait"];
    for (WYGeniusModel *geniusModel in [RCIM sharedRCIM].geniusLabelArray) {
        if ([dict[@"genius_id"] isEqualToString:geniusModel.genius_id]) {
            [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:geniusModel.avatar_url] placeholderImage:[UIImage imageNamed:@"portrait"]];
            self.geniusNameLabel.text = geniusModel.real_name;
            break;
        }
    }
    if (self.geniusNameLabel.text.length == 0) {
        self.geniusNameLabel.text = dict[@"genius_id"];
    }
    
    self.numLabel.text = ((NSNumber *)dict[@"freq"]).stringValue;
    
    [self layout];
}

- (UILabel *)geniusNameLabel {
    
	if(_geniusNameLabel == nil) {
		_geniusNameLabel = [[UILabel alloc] init];
        _geniusNameLabel.font = [UIFont systemFontOfSize:14];
        _geniusNameLabel.textColor = [UIColor blackColor];
        _geniusNameLabel.textAlignment = NSTextAlignmentLeft;
        [self.contentView addSubview:_geniusNameLabel];
	}
	return _geniusNameLabel;
}

- (UILabel *)numLabel {
    
	if(_numLabel == nil) {
		_numLabel = [[UILabel alloc] init];
        _numLabel.font = [UIFont systemFontOfSize:14];
        _numLabel.textAlignment = NSTextAlignmentRight;
        _numLabel.textColor = HEXCOLOR(0x86C300);
        [self.contentView addSubview:_numLabel];
	}
	return _numLabel;
}

- (UIImageView *)iconImageView {
    
	if(_iconImageView == nil) {
		_iconImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"portrait"]];
        _iconImageView.layer.cornerRadius = 5.0;
        _iconImageView.layer.borderColor = [[UIColor lightGrayColor]CGColor];
        _iconImageView.layer.borderWidth = 1.0;
        _iconImageView.layer.masksToBounds = YES;
        [self.contentView addSubview:_iconImageView];
	}
	return _iconImageView;
}

- (UILabel *)rankLabel {
    
	if(_rankLabel == nil) {
		_rankLabel = [[UILabel alloc] init];
        _rankLabel.textColor = HEXCOLOR(0x3D3D3D);
        _rankLabel.font = [UIFont systemFontOfSize:14];
        [self.contentView addSubview:_rankLabel];
	}
	return _rankLabel;
}

@end
