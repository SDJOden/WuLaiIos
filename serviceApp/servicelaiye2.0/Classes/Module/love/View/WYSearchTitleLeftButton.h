//
//  WYSearchTitleLeftButton.h
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/8/17.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol WYSearchTitleLeftButtonDelegate <NSObject>

-(void)addPopView;

@end

@interface WYSearchTitleLeftButton : UIView

@property (nonatomic, weak) id <WYSearchTitleLeftButtonDelegate> wySearchTitleLeftButtonDelegate;

-(void)changeButtonTitle:(NSString *)title;

@end
