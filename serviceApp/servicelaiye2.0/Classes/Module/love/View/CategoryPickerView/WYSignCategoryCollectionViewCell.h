//
//  WYSignCategoryCollectionViewCell.h
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/8/20.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WYSignCategoryCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) UILabel *categoryLabel;

-(void)setContentText:(NSString *)text andSelect:(BOOL)select;

-(void)becomeSelectState;

-(void)dismissSelectState;

@end
