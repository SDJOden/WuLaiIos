//
//  WYSignCategoryView.m
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/8/20.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import "WYSignCategoryView.h"
#import "WYSignCategoryCollectionViewCell.h"

@interface WYSignCategoryView () <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@property (nonatomic, strong) UIButton *coverButton;
@property (nonatomic, strong) UIView *backgroundView;
@property (nonatomic, copy) UIButton *backButton;
@property (nonatomic, strong) NSArray *categoryArray;
@property (nonatomic, strong) UICollectionView *categoryCollectionView;
@property (nonatomic, strong) UICollectionViewFlowLayout *categoryCollectionViewFlowLayout;
@property (nonatomic, assign) NSInteger categoryIndex;

@end

@implementation WYSignCategoryView

-(instancetype)initWithCategoryIndex:(NSInteger)index{
    
    if (self = [super init]) {
        _categoryIndex = index;
        [self rcinit];
        [self layout];
    }
    return self;
}

-(instancetype)init{
    
    if (self = [super init]) {
        [self rcinit];
        [self layout];
    }
    return self;
}

-(void)rcinit{}

-(void)layout{
    
    [self.coverButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.bottom.equalTo(self);
    }];
    
    [self.backgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self);
        make.height.equalTo(208);
    }];
    
    [self.backButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.backgroundView);
        make.top.equalTo(self.backgroundView).offset(5);
        make.height.width.equalTo(30);
    }];
    
    [self.categoryCollectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self.backgroundView);
        make.width.equalTo(60*4+12*3);
        make.height.equalTo(30*3+20*2);
    }];
}

#pragma mark --- UICollectionViewDelgate
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return self.categoryArray.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    WYSignCategoryCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"signCategoryCell" forIndexPath:indexPath];
    [cell setContentText:self.categoryArray[indexPath.row] andSelect:_categoryIndex == indexPath.row];
    return cell;
}

#pragma mark --- UICollectionViewDelegateFlowLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    return CGSizeMake(60, 30);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    WYSignCategoryCollectionViewCell *cell = (WYSignCategoryCollectionViewCell*)[collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:indexPath.row inSection:0]];
    [cell becomeSelectState];
    _categoryIndex = indexPath.row;
    if ([self.wySignCategoryViewDelegate respondsToSelector:@selector(chooseCategory:andIndex:)]) {
        [self.wySignCategoryViewDelegate chooseCategory:cell.categoryLabel.text andIndex:_categoryIndex];
    }
}

-(void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    WYSignCategoryCollectionViewCell *cell = (WYSignCategoryCollectionViewCell*)[collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:indexPath.row inSection:0]];
    [cell dismissSelectState];
}

#pragma mark --- 按钮点击事件
-(void)didTouchBackButton{
    
    if ([self.wySignCategoryViewDelegate respondsToSelector:@selector(closeSignCategoryView)]) {
        [self.wySignCategoryViewDelegate closeSignCategoryView];
    }
}

#pragma mark --- 懒加载
- (UIButton *)coverButton {
    
	if(_coverButton == nil) {
		_coverButton = [[UIButton alloc] init];
        _coverButton.backgroundColor = HEXCOLOR(0x000000);
        _coverButton.alpha = 0.6;
        [_coverButton addTarget:self action:@selector(didTouchBackButton) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_coverButton];
	}
	return _coverButton;
}

- (UIView *)backgroundView {
    
	if(_backgroundView == nil) {
		_backgroundView = [[UIView alloc] init];
        _backgroundView.backgroundColor = HEXCOLOR(0xffffff);
        [self addSubview:_backgroundView];
	}
	return _backgroundView;
}

- (UIButton *)backButton {
    
	if(_backButton == nil) {
		_backButton = [[UIButton alloc]init];
        [_backButton setImage:[UIImage imageNamed:@"fork_black"] forState:UIControlStateNormal];
        [_backButton setTintColor:HEXCOLOR(0x000000)];
        [_backButton addTarget:self action:@selector(didTouchBackButton) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_backButton];
	}
	return _backButton;
}

- (UICollectionView *)categoryCollectionView {
    
	if(_categoryCollectionView == nil) {
		_categoryCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, 0, 0) collectionViewLayout:self.categoryCollectionViewFlowLayout];
        _categoryCollectionView.delegate = self;
        _categoryCollectionView.dataSource = self;
        _categoryCollectionView.backgroundColor = [UIColor clearColor];
        [_categoryCollectionView registerClass:[WYSignCategoryCollectionViewCell class] forCellWithReuseIdentifier:@"signCategoryCell"];
        [self.backgroundView addSubview:_categoryCollectionView];
	}
	return _categoryCollectionView;
}

- (UICollectionViewFlowLayout *)categoryCollectionViewFlowLayout {
    
	if(_categoryCollectionViewFlowLayout == nil) {
        _categoryCollectionViewFlowLayout = [[UICollectionViewFlowLayout alloc] init];
        _categoryCollectionViewFlowLayout.minimumLineSpacing = 20.0f;
        _categoryCollectionViewFlowLayout.minimumInteritemSpacing = 12.0f;
        _categoryCollectionViewFlowLayout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
        _categoryCollectionViewFlowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
	}
	return _categoryCollectionViewFlowLayout;
}

- (NSArray *)categoryArray {
    
	if(_categoryArray == nil) {
		_categoryArray = @[@"跑腿", @"代买", @"机票", @"提醒", @"鲜花", @"用车", @"咖啡", @"保洁", @"无", @"火车", @"售后"];
	}
	return _categoryArray;
}

@end
