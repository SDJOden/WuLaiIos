//
//  WYCategoryPickerView.m
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/8/9.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import "WYCategoryPickerView.h"
#import "WYLoveTypeModel.h"

@interface WYCategoryPickerView () <UIPickerViewDelegate, UIPickerViewDataSource>

@property (nonatomic, strong) UIButton *coverButton;
@property (nonatomic, strong) UIView *backgroundView;
@property (nonatomic, assign) NSInteger selectRow;
@property (nonatomic, copy) NSString *categoryStr;
@property (nonatomic, strong) NSArray *dataArray;
@property (nonatomic, strong) UIPickerView *pickerView;
@property (nonatomic, strong) UIButton *cancelButton;
@property (nonatomic, strong) UIButton *completeButton;

@end

@implementation WYCategoryPickerView

-(instancetype)initWithCategory:(NSString *)category withArray:(NSArray *)array{
    
    if (self = [super init]) {
        [self rcinit];
        _categoryStr = category;
        _dataArray = array;
        [self layout];
    }
    return self;
}

-(void)rcinit{
    
    _selectRow = 0;
}

-(void)layout{
    
    [self.coverButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.bottom.equalTo(self);
    }];
    
    [self.backgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self);
        make.height.equalTo(208);
    }];
    
    [self.cancelButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.backgroundView);
        make.top.equalTo(self.backgroundView).offset(5);
        make.height.equalTo(30);
        make.width.equalTo(60);
    }];
    
    [self.completeButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.backgroundView);
        make.top.equalTo(self.backgroundView).offset(5);
        make.height.equalTo(30);
        make.width.equalTo(60);
    }];
    
    [self.pickerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.cancelButton.bottom);
        make.left.right.bottom.equalTo(self);
    }];
    
    for (int i = 0; i < self.dataArray.count; i ++) {
        WYLoveTypeModel *model = self.dataArray[i];
        if (model.selected == YES) {
            [self.pickerView selectRow:i inComponent:0 animated:NO];
        }
    }
}

#pragma mark --- 点击事件
-(void)closePickerView{
    
    if ([self.categoryPickerViewDelegate respondsToSelector:@selector(closeCategoryPickerView)]) {
        [self.categoryPickerViewDelegate closeCategoryPickerView];
    }
}

-(void)completeChooseCategory{
    
    WYLoveTypeModel *loveModel = self.dataArray[_selectRow];
    for (WYLoveTypeModel *loveTypeModel in self.dataArray) {
        loveTypeModel.selected = NO;
    }
    loveModel.selected = YES;
    if ([self.categoryPickerViewDelegate respondsToSelector:@selector(chooseCategory:)]) {
        [self.categoryPickerViewDelegate chooseCategory:((WYLoveTypeModel *)self.dataArray[_selectRow]).type];
    }
}

#pragma mark --- UIPickerViewDelegate
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    
    return self.dataArray.count;
}

-(NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    
    WYLoveTypeModel *loveModel = self.dataArray[row];
    
    return loveModel.type;
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
    WYLoveTypeModel *loveModel = self.dataArray[row];
    self.categoryStr = loveModel.type;
    self.selectRow = row;
}

-(CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
    
    return 32.5;
}

- (UIPickerView *)pickerView {
    
	if(_pickerView == nil) {
		_pickerView = [[UIPickerView alloc] init];
        _pickerView.delegate = self;
        [self.backgroundView addSubview:_pickerView];
	}
	return _pickerView;
}

- (UIButton *)cancelButton {
    
	if(_cancelButton == nil) {
		_cancelButton = [[UIButton alloc] init];
        [_cancelButton setTitle:@"取消" forState:UIControlStateNormal];
        [_cancelButton setTitleColor:HEXCOLOR(0xACACAC) forState:UIControlStateNormal];
        _cancelButton.titleLabel.font = [UIFont systemFontOfSize:16];
        [_cancelButton addTarget:self action:@selector(closePickerView) forControlEvents:UIControlEventTouchUpInside];
        [self.backgroundView addSubview:_cancelButton];
	}
	return _cancelButton;
}

- (UIButton *)completeButton {
    
	if(_completeButton == nil) {
		_completeButton = [[UIButton alloc] init];
        [_completeButton setTitle:@"确定" forState:UIControlStateNormal];
        [_completeButton setTitleColor:HEXCOLOR(0x25C1FF) forState:UIControlStateNormal];
        _completeButton.titleLabel.font = [UIFont systemFontOfSize:16];
        [_completeButton addTarget:self action:@selector(completeChooseCategory) forControlEvents:UIControlEventTouchUpInside];
        [self.backgroundView addSubview:_completeButton];
	}
	return _completeButton;
}

- (UIButton *)coverButton {
    
    if(_coverButton == nil) {
        _coverButton = [[UIButton alloc] init];
        _coverButton.backgroundColor = HEXCOLOR(0x000000);
        _coverButton.alpha = 0.6;
        [_coverButton addTarget:self action:@selector(closePickerView) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_coverButton];
    }
    return _coverButton;
}

- (UIView *)backgroundView {
    
	if(_backgroundView == nil) {
		_backgroundView = [[UIView alloc] init];
        _backgroundView.backgroundColor = HEXCOLOR(0xffffff);
        [self addSubview:_backgroundView];
	}
	return _backgroundView;
}

@end
