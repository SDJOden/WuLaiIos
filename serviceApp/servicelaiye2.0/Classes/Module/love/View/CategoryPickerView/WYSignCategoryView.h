//
//  WYSignCategoryView.h
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/8/20.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol WYSignCategoryViewDelegate <NSObject>

-(void)closeSignCategoryView;

-(void)chooseCategory:(NSString *)category andIndex:(NSInteger)index;

@end

@interface WYSignCategoryView : UIView

@property (nonatomic, weak) id <WYSignCategoryViewDelegate> wySignCategoryViewDelegate;

-(instancetype)initWithCategoryIndex:(NSInteger)index;

@end
