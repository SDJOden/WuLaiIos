//
//  WYSignCategoryCollectionViewCell.m
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/8/20.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import "WYSignCategoryCollectionViewCell.h"

@interface WYSignCategoryCollectionViewCell ()

@end

@implementation WYSignCategoryCollectionViewCell

-(void)setContentText:(NSString *)text andSelect:(BOOL)select{
    
    self.contentView.backgroundColor = [UIColor clearColor];
    
    self.categoryLabel.text = text;
    [self.categoryLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.bottom.equalTo(self.contentView);
    }];
    
    self.categoryLabel.layer.cornerRadius = 15.0;
    self.categoryLabel.layer.masksToBounds = YES;
    
    if (select) {
        self.categoryLabel.layer.borderWidth = 0;
        self.categoryLabel.backgroundColor = HEXCOLOR(0xFFD600);
        self.categoryLabel.textColor = HEXCOLOR(0xffffff);
    }else {
        self.categoryLabel.layer.borderWidth = 0.5;
        self.categoryLabel.layer.borderColor = [HEXCOLOR(0x979797)CGColor];
        self.categoryLabel.backgroundColor = [UIColor clearColor];
        self.categoryLabel.textColor = HEXCOLOR(0xA0A0A0);
    }
}

-(void)becomeSelectState{
    
    self.categoryLabel.layer.borderWidth = 0;
    self.categoryLabel.backgroundColor = HEXCOLOR(0xFFD600);
    self.categoryLabel.textColor = HEXCOLOR(0xffffff);
}

-(void)dismissSelectState{
    
    self.categoryLabel.layer.borderWidth = 0.5;
    self.categoryLabel.layer.borderColor = [HEXCOLOR(0x979797)CGColor];
    self.categoryLabel.backgroundColor = [UIColor clearColor];
    self.categoryLabel.textColor = HEXCOLOR(0xA0A0A0);
}

- (UILabel *)categoryLabel {
    
	if(_categoryLabel == nil) {
		_categoryLabel = [[UILabel alloc] init];
        _categoryLabel.font = [UIFont systemFontOfSize:15];
        _categoryLabel.textColor = HEXCOLOR(0xA0A0A0);
        _categoryLabel.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:_categoryLabel];
	}
	return _categoryLabel;
}

@end
