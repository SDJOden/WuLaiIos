//
//  WYCategoryPickerView.h
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/8/9.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol WYCategoryPickerViewDelegate <NSObject>

-(void)chooseCategory:(NSString *)category;

-(void)closeCategoryPickerView;

@end

@interface WYCategoryPickerView : UIView

@property (nonatomic, weak) id <WYCategoryPickerViewDelegate> categoryPickerViewDelegate;

-(instancetype)initWithCategory:(NSString *)category withArray:(NSArray *)array;

@end
