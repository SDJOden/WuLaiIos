//
//  WYLoveCandidateSearchView.h
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/8/6.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol WYLoveCandidateSearchViewDelegate <NSObject>

-(void)didTouchHotWordButtonWithTitle:(NSString *)title;

@end

@interface WYLoveCandidateSearchView : UIView

@property (nonatomic, strong) NSArray *hotWordsArray;

@property (nonatomic, weak) id <WYLoveCandidateSearchViewDelegate> loveCandidateSearchViewDelegate;

@end
