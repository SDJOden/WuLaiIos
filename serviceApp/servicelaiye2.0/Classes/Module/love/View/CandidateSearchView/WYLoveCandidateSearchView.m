//
//  WYLoveCandidateSearchView.m
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/8/6.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import "WYLoveCandidateSearchView.h"

#define EDGE_MARGIN 12
#define ROW_MARGIN 22
#define ROW_SPACE 20
#define ROW_NUMBER 4
#define ROW_HEIGHT 30
#define ROW_WIDTH (SCREENWIDTH - EDGE_MARGIN * 2 - ROW_MARGIN * (ROW_NUMBER-1))/(ROW_NUMBER*1.0)

@interface WYLoveCandidateSearchView ()

@property (nonatomic, strong) UILabel *hotWordsTitleLabel;

@property (nonatomic, strong) UIView *backgroundView;

@end

@implementation WYLoveCandidateSearchView

-(instancetype)init{
    
    if (self = [super init]) {
    }
    return self;
}

-(void)setHotWordsArray:(NSArray *)hotWordsArray{
    
    _hotWordsArray = [hotWordsArray copy];
    
    int row = _hotWordsArray.count%4==0 ? hotWordsArray.count/ROW_NUMBER:hotWordsArray.count/ROW_NUMBER + 1;
    
    CGFloat height = (row+1) * ROW_SPACE + row * ROW_HEIGHT;
    
    self.hotWordsTitleLabel.frame = CGRectMake(12, 0, self.hotWordsTitleLabel.w, 34)
    ;
    self.backgroundView.frame = CGRectMake(0, 34, SCREENWIDTH, height);
    
    CGFloat x = EDGE_MARGIN;
    
    for (int i = 0; i < hotWordsArray.count; i ++) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setTitle:hotWordsArray[i] forState:UIControlStateNormal];
        [button setTitleColor:HEXCOLOR(0x7A7A7A) forState:UIControlStateNormal];
        [button setTitleColor:HEXCOLOR(0xffffff) forState:UIControlStateHighlighted];
        button.titleLabel.font = [UIFont systemFontOfSize:15];
        button.layer.cornerRadius = ROW_HEIGHT*0.5;
        button.layer.masksToBounds = YES;
        button.layer.borderColor = [HEXCOLOR(0x979797)CGColor];
        button.layer.borderWidth = 0.5;
        [button addTarget:self action:@selector(selectHotWord:) forControlEvents:UIControlEventTouchUpInside];
        [self.backgroundView addSubview:button];
        
        button.frame = CGRectMake(x, (i/ROW_NUMBER+1) * ROW_SPACE + i/ROW_NUMBER*ROW_HEIGHT, ROW_WIDTH, ROW_HEIGHT);
        if ((i+1)%ROW_NUMBER>0) {
            x += ROW_WIDTH + ROW_MARGIN;
        }
        else {
            x = EDGE_MARGIN;
        }
    }
}

#pragma mark --- 按钮点击方法
-(void)selectHotWord:(UIButton *)sender{
    
    if ([self.loveCandidateSearchViewDelegate respondsToSelector:@selector(didTouchHotWordButtonWithTitle:)]) {
        [self.loveCandidateSearchViewDelegate didTouchHotWordButtonWithTitle:sender.titleLabel.text];
    }
}

- (UIView *)backgroundView {
    
	if(_backgroundView == nil) {
		_backgroundView = [[UIView alloc] init];
        _backgroundView.backgroundColor = HEXCOLOR(0xffffff);
        [self addSubview:_backgroundView];
	}
	return _backgroundView;
}

- (UILabel *)hotWordsTitleLabel {
    
	if(_hotWordsTitleLabel == nil) {
		_hotWordsTitleLabel = [[UILabel alloc] init];
        _hotWordsTitleLabel.font = [UIFont systemFontOfSize:14];
        _hotWordsTitleLabel.textColor = HEXCOLOR(0x666666);
        _hotWordsTitleLabel.text = @"热门关键字";
        [_hotWordsTitleLabel sizeToFit];
        [self addSubview:_hotWordsTitleLabel];
	}
	return _hotWordsTitleLabel;
}

@end
