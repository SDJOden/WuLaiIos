//
//  WYLoveSesssionCell.m
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/5/30.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import "WYLoveSessionCell.h"
#import "WYLoveSessionModel.h"
#import "WYGeniusModel.h"
#import "WYLoveContentView.h"
#import "WYLoveSessionModel.h"
#import "WYSessionTextModel.h"

@interface WYLoveSessionCell ()

@property (nonatomic, strong) UIView *backView;
@property (nonatomic, strong) UIButton *categoryButton;
@property (nonatomic, strong) UIView *separateLineView;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *timeLabel;
@property (nonatomic, strong) NSArray *sessionArray;
@property (nonatomic, strong) WYLoveContentView *loveContentView;
@property (nonatomic, strong) UIButton *loveButton;
@property (nonatomic, strong) UIButton *hateButton;
@property (nonatomic, strong) UILabel *animationLoveLabel;
@property (nonatomic, strong) UILabel *animationHateLabel;
@property (nonatomic, assign) BOOL isRandom;
@property (nonatomic, strong) WYLoveSessionModel *model;

@end

@implementation WYLoveSessionCell

-(void)setLoveSessionCellWithModel:(WYLoveSessionModel *)model withSearchText:(NSString *)searchText{
    
    self.backView.frame = CGRectMake(0, 12, self.w, self.h-12);
    [self layoutWithModel:model withSearchText:searchText];
}

-(void)layoutWithModel:(WYLoveSessionModel *)model withSearchText:(NSString *)searchText{
    
    self.model = model;
    if (model.domain.length == 0) {
        [self.categoryButton setTitle:@"其他" forState:UIControlStateNormal];
    }
    else {
        [self.categoryButton setTitle:model.domain forState:UIControlStateNormal];
    }
    CGSize categorySize = [WYTool labelHeightWithText:self.categoryButton.titleLabel.text andWidth:MAXFLOAT];
    self.categoryButton.frame = CGRectMake(10, 10, categorySize.width, 24);
    
    self.separateLineView.frame = CGRectMake(0, 43.5, self.w, 0.5);
    
    //算高度
    CGFloat loveContentHeight = 0;
    for (WYSessionTextModel *textModel in model.session) {
        CGSize rowSize = [WYTool labelHeightWithText:textModel.text andWidth:SCREENWIDTH  - 20 - 18 - 26.5 - 10];
        rowSize = CGSizeMake(ceil(rowSize.width), ceil(rowSize.height));
        if (rowSize.height < 26) {
            rowSize = CGSizeMake(ceil(rowSize.width), 26);
        }
        loveContentHeight += rowSize.height + 23;
    }
    loveContentHeight -= 23;
    
    self.loveContentView.frame = CGRectMake(9, self.separateLineView.y + self.separateLineView.h+10, self.w - 18, loveContentHeight);
    [self.loveContentView addSubContentLabelWithArray:model.session withSearchText:searchText];
    
    NSString *dateStr = [model.timestamp substringToIndex:10];
    
    self.timeLabel.text = [NSString stringWithFormat:@"%@", dateStr];
    
    CGSize timeSize = [WYTool labelHeightWithText:self.timeLabel.text andWidth:MAXFLOAT andFont:[UIFont systemFontOfSize:14]];
    
    if (model.genius_id.integerValue == 0) {
        self.nameLabel.hidden = YES;
    } else {
        self.nameLabel.hidden = NO;
        self.nameLabel.text = @"";
        
        for (WYGeniusModel *geniusModel in [RCIM sharedRCIM].geniusLabelArray) {
            if ([geniusModel.genius_id isEqualToString:model.genius_id]) {
                self.nameLabel.text = geniusModel.real_name;
                break;
            }
        }
        if (self.nameLabel.text.length == 0) {
            self.nameLabel.text = model.genius_id;
        }
        
        self.nameLabel.text = [NSString stringWithFormat:@"%@推荐", self.nameLabel.text];
        
        self.timeLabel.frame = CGRectMake(self.loveContentView.x, self.loveContentView.y+self.loveContentView.h+10, timeSize.width, 24);
        CGSize nameSize = [WYTool labelHeightWithText:self.nameLabel.text andWidth:MAXFLOAT];
        self.nameLabel.frame = CGRectMake(self.timeLabel.x+self.timeLabel.w+10, self.loveContentView.y+self.loveContentView.h+10, nameSize.width, 24);
        
    }
    
    [self.loveButton setTitle:model.like forState:UIControlStateNormal];

    self.loveButton.selected = model.isEnabled;
    self.hateButton.selected = model.isHate;
    [self.hateButton setTitle:@"0" forState:UIControlStateNormal];
    
    self.loveButton.frame = CGRectMake(self.w - 54 - 12 - 54, self.loveContentView.y+self.loveContentView.h+9, 54, 26);
    self.hateButton.frame = CGRectMake(self.w - 54, self.loveContentView.y+self.loveContentView.h+9, 54, 26);
}

#pragma mark --- 按钮点击事件
-(void)loveButtonClick{
    
    if (self.loveButton.selected) {
        return;
    }
    self.model.isEnabled = YES;
    self.model.like = @(self.model.like.integerValue + 1).stringValue;
    self.loveButton.selected = YES;
    [self.loveButton setTitle:self.model.like forState:UIControlStateNormal];
    self.animationLoveLabel.frame = CGRectMake(self.w - 54 - 12 - 50, self.h - 35, 26, 26);
    [UIView animateWithDuration:0.5 animations:^{
        self.animationLoveLabel.transform = CGAffineTransformMakeTranslation(0, -10);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.5 animations:^{
            self.animationLoveLabel.alpha = 0.0;
        } completion:^(BOOL finished) {
            [self.animationLoveLabel removeFromSuperview];
            self.animationLoveLabel = nil;
        }];
    }];
    [MobClick event:@"love_love_click"];
    if ([self.loveSessionCellDelegate respondsToSelector:@selector(loveThisMsg:)]) {
        [self.loveSessionCellDelegate loveThisMsg:self];
    }
}

-(void)hateButtonClick{
    
    if (self.hateButton.selected) {
        return;
    }
    self.model.isHate = YES;
    self.hateButton.selected = YES;
    [self.hateButton setTitle:@"1" forState:UIControlStateNormal];
    self.animationHateLabel.frame = CGRectMake(self.w - 54, self.h - 45, 26, 26);
    
    // -------------weakself
    [UIView animateWithDuration:0.5 animations:^{
        self.animationHateLabel.transform = CGAffineTransformMakeTranslation(0, -10);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.5 animations:^{
            self.animationHateLabel.alpha = 0.0;
        } completion:^(BOOL finished) {
            [self.animationHateLabel removeFromSuperview];
            self.animationHateLabel = nil;
        }];
    }];
    [MobClick event:@"love_hate_click"];
    if ([self.loveSessionCellDelegate respondsToSelector:@selector(hateThisMsg:)]) {
        [self.loveSessionCellDelegate hateThisMsg:self];
    }
}

-(void)changeCategory{
    
    [MobClick event:@"love_cellChangeCategory_click"];
    if ([self.loveSessionCellDelegate respondsToSelector:@selector(changeCategoryThisMsg:)]) {
        [self.loveSessionCellDelegate changeCategoryThisMsg:self];
    }
}

-(void)changeCategory:(NSString *)category{
    
    self.model.domain = category;
    [self.categoryButton setTitle:category forState:UIControlStateNormal];
}

#pragma mark --- 懒加载

- (UILabel *)nameLabel {
    
	if(_nameLabel == nil) {
		_nameLabel = [[UILabel alloc] init];
        _nameLabel.textColor = HEXCOLOR(0xA9A9A9);
        _nameLabel.font = [UIFont systemFontOfSize:14];
        _nameLabel.textAlignment = NSTextAlignmentCenter;
        [self.backView addSubview:_nameLabel];
	}
	return _nameLabel;
}

- (UILabel *)timeLabel {
    
	if(_timeLabel == nil) {
		_timeLabel = [[UILabel alloc] init];
        _timeLabel.textColor = HEXCOLOR(0xA9A9A9);
        _timeLabel.font = [UIFont systemFontOfSize:14];
        _timeLabel.textAlignment = NSTextAlignmentCenter;
        [self.backView addSubview:_timeLabel];
	}
	return _timeLabel;
}

- (UIButton *)categoryButton {
    
    if(_categoryButton == nil) {
        _categoryButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_categoryButton setTitleColor:HEXCOLOR(0x9F9F9F) forState:UIControlStateNormal];
        _categoryButton.titleLabel.font = [UIFont systemFontOfSize:14];
        _categoryButton.titleLabel.textAlignment = NSTextAlignmentRight;
        [_categoryButton addTarget:self action:@selector(changeCategory) forControlEvents:UIControlEventTouchUpInside];
        [self.backView addSubview:_categoryButton];
    }
    return _categoryButton;
}

- (NSArray *)sessionArray {
    
	if(_sessionArray == nil) {
		_sessionArray = [[NSArray alloc] init];
	}
	return _sessionArray;
}

- (WYLoveContentView *)loveContentView {
    
    if(_loveContentView == nil) {
        _loveContentView = [[WYLoveContentView alloc] init];
        [self.backView addSubview:_loveContentView];
    }
    return _loveContentView;
}

- (UIButton *)loveButton {
    
	if(_loveButton == nil) {
		_loveButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _loveButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
        [_loveButton setImage:[UIImage imageNamed:@"up0"] forState:UIControlStateNormal];
        [_loveButton setImage:[UIImage imageNamed:@"up1"] forState:UIControlStateSelected];
        _loveButton.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 25);
        [_loveButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        _loveButton.titleLabel.font = [UIFont systemFontOfSize:14];
        [_loveButton addTarget:self action:@selector(loveButtonClick) forControlEvents:UIControlEventTouchUpInside];
        [self.backView addSubview:_loveButton];
	}
	return _loveButton;
}

- (UIButton *)hateButton {
    
	if(_hateButton == nil) {
		_hateButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _hateButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
        [_hateButton setImage:[UIImage imageNamed:@"down0"] forState:UIControlStateNormal];
        [_hateButton setImage:[UIImage imageNamed:@"down1"] forState:UIControlStateSelected];
        _hateButton.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 25);
        [_hateButton setTitle:@"0" forState:UIControlStateNormal];
        [_hateButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        _hateButton.titleLabel.font = [UIFont systemFontOfSize:14];
        [_hateButton addTarget:self action:@selector(hateButtonClick) forControlEvents:UIControlEventTouchUpInside];
        [self.backView addSubview:_hateButton];
	}
	return _hateButton;
}

- (UILabel *)animationLoveLabel {
    
	if(_animationLoveLabel == nil) {
		_animationLoveLabel = [[UILabel alloc] init];
        _animationLoveLabel.text = @"+1";
        _animationLoveLabel.font = [UIFont systemFontOfSize:14];
        _animationLoveLabel.textColor = [UIColor blackColor];
        _animationLoveLabel.textAlignment = NSTextAlignmentCenter;
        [self.backView addSubview:_animationLoveLabel];
	}
	return _animationLoveLabel;
}

- (UILabel *)animationHateLabel {
    
	if(_animationHateLabel == nil) {
		_animationHateLabel = [[UILabel alloc] init];
        _animationHateLabel.text = @"+1";
        _animationHateLabel.font = [UIFont systemFontOfSize:14];
        _animationHateLabel.textColor = [UIColor blackColor];
        _animationHateLabel.textAlignment = NSTextAlignmentCenter;
        [self.backView addSubview:_animationHateLabel];
	}
	return _animationHateLabel;
}

- (UIView *)separateLineView {
    
	if(_separateLineView == nil) {
		_separateLineView = [[UIView alloc] init];
        _separateLineView.backgroundColor = HEXCOLOR(0xDFDFDF);
        [self.backView addSubview:_separateLineView];
	}
	return _separateLineView;
}

- (UIView *)backView {
    
	if(_backView == nil) {
		_backView = [[UIView alloc] init];
        _backView.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:_backView];
	}
	return _backView;
}

@end
