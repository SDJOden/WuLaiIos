//
//  WYLoveContentView.m
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/6/1.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import "WYLoveContentView.h"
#import "WYSessionTextModel.h"

#define USERLABELWIDTH 26.5

@interface WYLoveContentView ()

@property (nonatomic, strong) NSArray *contentArr;

@end

@implementation WYLoveContentView

-(void)addSubContentLabelWithArray:(NSArray *)array withSearchText:(NSString *)searchText{
    
    CGFloat y = 0;
    for (__strong UIView *view in self.subviews) {
        if ([view isKindOfClass:[UILabel class]]) {
            [view removeFromSuperview];
            view = nil;
        }
    }
    
    for (WYSessionTextModel *model in array) {
        
        UILabel *label = [[UILabel alloc]init];
        
        NSMutableAttributedString *str = [[NSMutableAttributedString alloc]initWithString:model.text];
        
        [str addAttribute:NSForegroundColorAttributeName value:HEXCOLOR(0x5B5B5B) range:NSMakeRange(0, str.length)];
        [str addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14] range:NSMakeRange(0, str.length)];
        
        if (searchText.length != 0) {
            NSRange range = [model.text rangeOfString:searchText];
            if (range.location != NSNotFound) {
                [str addAttribute:NSForegroundColorAttributeName value:HEXCOLOR(0xF7AC0B) range:range];
            }
        }
        
        label.attributedText = str;
        label.font = [UIFont systemFontOfSize:14];
        label.numberOfLines = 0;
        CGSize rowSize = CGSizeMake(0, 0);
        
        UILabel *userLabel = [[UILabel alloc]init];
        userLabel.layer.masksToBounds = YES;
        userLabel.textColor = [UIColor colorWithRed:0.3419 green:0.3419 blue:0.3419 alpha:1.0];
        if ([model.type isEqualToString:@"User"]) {
            rowSize = [WYTool labelHeightWithText:model.text andWidth:SCREENWIDTH  - 20 - 18 - 26.5 - 10 andFont:[UIFont boldSystemFontOfSize:14]];
            userLabel.text = @"U";
            label.font = [UIFont boldSystemFontOfSize:14];
            userLabel.backgroundColor = [UIColor colorWithRed:0.75 green:0.75 blue:0.75 alpha:1.0];
        } else if ([model.identify isEqualToString:@"1896206"] || [model.identify isEqualToString:@"1896207"] || [model.identify isEqualToString:@"1896208"] || [model.identify isEqualToString:@"1896209"]) {
            rowSize = [WYTool labelHeightWithText:model.text andWidth:SCREENWIDTH  - 20 - 18 - 26.5 - 10 andFont:[UIFont systemFontOfSize:14]];
            userLabel.text = @"AI";
            userLabel.backgroundColor = [UIColor colorWithRed:1.0 green:0.9137 blue:0.6196 alpha:1.0];
        } else {
            rowSize = [WYTool labelHeightWithText:model.text andWidth:SCREENWIDTH  - 20 - 18 - 26.5 - 10 andFont:[UIFont systemFontOfSize:14]];
            userLabel.text = @"G";
            userLabel.backgroundColor = [UIColor colorWithRed:0.7451 green:0.8784 blue:0.9451 alpha:1.0];
        }
        
        if (rowSize.height < 26) {
            rowSize = CGSizeMake(ceil(rowSize.width), 26);
        } else {
            rowSize = CGSizeMake(ceil(rowSize.width), ceil(rowSize.height));
        }
        label.frame = CGRectMake(USERLABELWIDTH + 10, y, rowSize.width, rowSize.height);
        
        userLabel.textAlignment = NSTextAlignmentCenter;
        userLabel.font = [UIFont systemFontOfSize:14];
        userLabel.frame = CGRectMake(0, y, USERLABELWIDTH, USERLABELWIDTH);
        userLabel.layer.cornerRadius = 13;
        
        [self addSubview:label];
        [self addSubview:userLabel];
        
        y += rowSize.height + 23;
    }
}

-(CGSize)getSpaceLabelWithText:(NSAttributedString *)text withHeightFont:(UIFont*)font withWidth:(CGFloat)width withLineSpace:(CGFloat)space {
    
    NSMutableParagraphStyle *paraStyle = [[NSMutableParagraphStyle alloc] init];
    paraStyle.lineBreakMode = NSLineBreakByCharWrapping;
    paraStyle.alignment = NSTextAlignmentLeft;
    paraStyle.lineSpacing = space;
    paraStyle.hyphenationFactor = 1.0;
    paraStyle.firstLineHeadIndent = 0.0;
    paraStyle.paragraphSpacingBefore = 0.0;
    paraStyle.headIndent = 0;
    paraStyle.tailIndent = 0;

    CGSize size = [text boundingRectWithSize:CGSizeMake(width, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin context:nil].size;
    return size;
}

@end
