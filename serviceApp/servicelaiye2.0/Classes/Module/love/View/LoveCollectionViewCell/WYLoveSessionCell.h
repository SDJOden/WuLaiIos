//
//  WYLoveSesssionCell.h
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/5/30.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import <UIKit/UIKit.h>

@class WYLoveSessionModel;

@class WYLoveSessionCell;

@protocol WYLoveSessionCellDelegate <NSObject>

-(void)loveThisMsg:(WYLoveSessionCell *)cell;

-(void)hateThisMsg:(WYLoveSessionCell *)cell;

-(void)changeCategoryThisMsg:(WYLoveSessionCell *)cell;

@end

@interface WYLoveSessionCell : UICollectionViewCell

@property (nonatomic, weak) id <WYLoveSessionCellDelegate> loveSessionCellDelegate;

-(void)setLoveSessionCellWithModel:(WYLoveSessionModel *)model withSearchText:(NSString *)searchText;

-(void)changeCategory:(NSString *)category;

@end
