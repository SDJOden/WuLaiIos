//
//  WYLoveContentView.h
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/6/1.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WYLoveContentView : UIView

-(void)addSubContentLabelWithArray:(NSArray *)array withSearchText:(NSString *)searchText;

@end
