//
//  WYLoveNaviView.m
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/8/9.
//  Copyright © 2016年 shengdong. All rights reserved.
//



#import "WYLoveNaviView.h"
#import "XZNaviV.h"

#define NAVIHEIGHT self.bounds.size.height

@interface WYLoveNaviView ()

@property (nonatomic, strong) UILabel *categoryLabel;
@property (nonatomic, strong) UIImageView *menuImageView;
@property (nonatomic, strong) UIButton *categoryButton;
@property (nonatomic, strong) UIView *verticalSeparateView;
@property (nonatomic, strong) XZNaviV *naviView;
@property (nonatomic, strong) UIView *horizontalSeparateView;
@property (nonatomic, strong) NSArray *statusArr;

@end

@implementation WYLoveNaviView

-(instancetype)init{
    
    if (self = [super init]) {
    }
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame{
    
    if (self = [super initWithFrame:frame]) {
        [self rcinit];
        [self layout];
    }
    return self;
}

-(void)rcinit{
    
    self.backgroundColor = [UIColor whiteColor];
}

-(void)layout{
    
    self.categoryLabel.frame = CGRectMake(10, 0, 58, NAVIHEIGHT);
    self.menuImageView.frame = CGRectMake(self.categoryLabel.x + self.categoryLabel.w + 3, (NAVIHEIGHT - 8.5) * 0.5, 5, 8.5);
    self.categoryButton.frame = CGRectMake(0, 0, 83.5, NAVIHEIGHT);
    self.verticalSeparateView.frame = CGRectMake(83.5, 7.5, 0.5, NAVIHEIGHT - 7.5 - 6);
    self.naviView.frame = CGRectMake(84 + 10, 0, SCREENWIDTH - 84 - 10, NAVIHEIGHT);
}

-(void)changeLabelString:(NSString *)Str{
    
    self.categoryLabel.text = Str;
}

#pragma mark --- 按钮点击事件
-(void)chooseCategory{
    
    if ([self.loveNaviViewDelegate respondsToSelector:@selector(didClickNaviCategoryButton)]) {
        [self.loveNaviViewDelegate didClickNaviCategoryButton];
    }
}

#pragma mark --- 懒加载
- (UILabel *)categoryLabel {
    
	if(_categoryLabel == nil) {
		_categoryLabel = [[UILabel alloc] init];
        _categoryLabel.font = [UIFont systemFontOfSize:14];
        _categoryLabel.textColor = HEXCOLOR(0x505050);
        _categoryLabel.text = @"所有品类";
        _categoryLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:_categoryLabel];
	}
	return _categoryLabel;
}

- (UIImageView *)menuImageView {
    
	if(_menuImageView == nil) {
		_menuImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"upAndDown"]];
        [self addSubview:_menuImageView];
	}
	return _menuImageView;
}

- (UIButton *)categoryButton {
    
	if(_categoryButton == nil) {
		_categoryButton = [[UIButton alloc] init];
        [_categoryButton addTarget:self action:@selector(chooseCategory) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_categoryButton];
	}
	return _categoryButton;
}

- (UIView *)verticalSeparateView {
    
    if(_verticalSeparateView == nil) {
        _verticalSeparateView = [[UIView alloc] init];
        _verticalSeparateView.backgroundColor = HEXCOLOR(0xDFDFDF);
        [self addSubview:_verticalSeparateView];
    }
    return _verticalSeparateView;
}

- (XZNaviV *)naviView {
    
	if(_naviView == nil) {
		_naviView = [[XZNaviV alloc] initWithFrame:CGRectMake(84 + 10, 40, SCREENWIDTH - 84 - 10, NAVIHEIGHT) ItemArray:self.statusArr itemClickBlock:^(NSInteger tag) {
            if ([self.loveNaviViewDelegate respondsToSelector:@selector(changeTagRequestData:)]) {
                [self.loveNaviViewDelegate changeTagRequestData:tag];
            }
        } btnMoreBlock:^(UIButton *btnMore) {}];
        [self addSubview:_naviView];
	}
	return _naviView;
}

- (UIView *)horizontalSeparateView {
    
	if(_horizontalSeparateView == nil) {
		_horizontalSeparateView = [[UIView alloc] init];
        _horizontalSeparateView.backgroundColor = HEXCOLOR(0xDFDFDF);
        [self addSubview:_horizontalSeparateView];
	}
	return _horizontalSeparateView;
}

- (NSArray *)statusArr {
    
	if(_statusArr == nil) {
		_statusArr = @[@"新", @"热门", @"精华", @"我的推荐", @"历史"];
	}
	return _statusArr;
}

@end
