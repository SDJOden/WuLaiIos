//
//  WYLoveNaviView.h
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/8/9.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol WYLoveNaviViewDelegate <NSObject>

-(void)changeTagRequestData:(NSInteger)tag;

-(void)didClickNaviCategoryButton;

@end

@interface WYLoveNaviView : UIView

@property (nonatomic, weak) id <WYLoveNaviViewDelegate> loveNaviViewDelegate;

-(void)changeLabelString:(NSString *)Str;

@end
