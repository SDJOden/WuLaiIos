//
//  XZNaviV.m
//  XunZhi
//
//  Created by 李雷 on 16/5/15.
//  Copyright © 2016年 cn.edu.jlnu.cst. All rights reserved.
//

#import "XZNaviV.h"

#define lineHeight 23
#define naviHeight (self.frame.size.height - lineHeight)
#define commonItemFontSize [UIFont systemFontOfSize:14.0f]
#define selectedItemFontSize [UIFont systemFontOfSize:16.0f]

@interface XZNaviV ()

@property (nonatomic, strong) NSArray *itemArray;
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, assign) NSInteger presentBtnItemTag;
@property (nonatomic, strong) UIView *line;
@property (nonatomic, copy) void (^itemBlock)(NSInteger tag);
@property (nonatomic, copy) void (^btnMoreBlock)(UIButton *btnMore);

@end

@implementation XZNaviV

/**
 *  初始化方法
 *
 *  @param frame        设置页面布局
 *  @param itemArray    显示item内容数组
 *  @param itemBlock    导航条中item点击结果回调block
 *  @param btnMoreBlock 右侧更多按钮点击事件的回调block
 *
 *  @return 带有collectionView和分割线和加好按钮的View
 */
- (instancetype)initWithFrame:(CGRect)frame ItemArray:(NSArray *)itemArray itemClickBlock:(void (^)(NSInteger))itemBlock btnMoreBlock:(void (^)(UIButton *))btnMoreBlock {
    
    self = [super initWithFrame:frame];
    if (self) {
        self.itemArray = itemArray;
        [self createView];
        self.itemBlock = itemBlock;
        self.btnMoreBlock = btnMoreBlock;
    }
    return self;
}

#pragma mark - 自定义函数
/**
 *  创建页面view
 */
- (void)createView {
    /**
     *  创建scrollView
     */
    self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, 40)];
    
    CGFloat margin = 0;
    CGFloat itemWidthX = 0;
    CGFloat itemWidth = 0;
    
    for (int i = 0; i < _itemArray.count; i ++) {
        itemWidth += [[self class] widthForText:[_itemArray objectAtIndex:i]];
    }
    margin = (self.bounds.size.width - itemWidth) / _itemArray.count;
    itemWidth = 0;
    
    for (int i = 0; i < _itemArray.count; i ++) {
        itemWidth = [[self class] widthForText:[_itemArray objectAtIndex:i]] + margin;
        UIButton *btn_item = [UIButton buttonWithType:UIButtonTypeCustom];
        btn_item.frame = CGRectMake(itemWidthX, 0, itemWidth, 40);
        itemWidthX += itemWidth;
        [btn_item setTitle:_itemArray[i] forState:UIControlStateNormal];
        btn_item.titleLabel.font = commonItemFontSize;
        [btn_item setTitleColor:HEXCOLOR(0x505050) forState:UIControlStateNormal];
        btn_item.backgroundColor = [UIColor clearColor];
        [_scrollView addSubview:btn_item];
        [btn_item addTarget:self action:@selector(btn_itemAction:) forControlEvents:UIControlEventTouchUpInside];
        btn_item.tag = i+1;
    }
    // >设置_scrollView的滚动范围
    _scrollView.contentSize = CGSizeMake(itemWidthX, 0);
    _scrollView.bounces = NO;
    [self addSubview:_scrollView];
    // >默认选中第一个
    self.presentBtnItemTag = 1;
    // >修改默认选中的item的字体大小
    UIButton *btnPresentItem = (UIButton *)[self viewWithTag:_presentBtnItemTag];
    btnPresentItem.titleLabel.font = selectedItemFontSize;
    [btnPresentItem setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    CGSize size = [WYTool labelHeightWithText:btnPresentItem.titleLabel.text andWidth:MAXFLOAT andFont:selectedItemFontSize];
    size = CGSizeMake(ceil(size.width)+10, ceil(size.height));
    
    self.line.frame = CGRectMake((btnPresentItem.w - size.width)*0.5, (self.bounds.size.height - lineHeight)*0.5, size.width, lineHeight);
    [self.scrollView sendSubviewToBack:self.line];
}
/**
 *  文字宽度适配
 *
 *  @param text 文字内容
 *
 *  @return 文字所占宽度
 */
+ (CGFloat)widthForText:(NSString *)text {
    CGRect rect = [text boundingRectWithSize:CGSizeMake(0, 300)
                                     options:NSStringDrawingUsesLineFragmentOrigin
                                  attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:20.0f]}
                                     context:nil];
    CGFloat textW = rect.size.width;
    return textW;
}
/**
 *  导航条右侧按钮点击事件
 *
 *  @param btnMore 右侧的按钮
 */
- (void)btnMoreAction:(UIButton *)btnMore {
    // >更多按钮的点击事件通过btnMoreBlock进行回调, 在调用的页面里做处理
    self.btnMoreBlock(btnMore);
}
/**
 *  导航条上每一个item的点击事件
 *
 *  @param btn_item 被点击的item
 */
- (void)btn_itemAction:(UIButton *)btn_item {
    switch (_presentBtnItemTag) {
        case 0:
            [MobClick event:@"love_exploit_click"];
            break;
        case 1:
            [MobClick event:@"love_new_click"];
            break;
        case 2:
            [MobClick event:@"love_hot_click"];
            break;
        case 3:
            [MobClick event:@"love_excerption_click"];
            break;
        case 4:
            [MobClick event:@"love_me_click"];
            break;
        case 5:
            [MobClick event:@"love_operation_click"];
            break;
        default:
            break;
    }
    // >恢复上一个被选中的字体大小
    UIButton *btnPresentItem = (UIButton *)[self viewWithTag:_presentBtnItemTag];
    btnPresentItem.titleLabel.font = commonItemFontSize;
    [btnPresentItem setTitleColor:HEXCOLOR(0x505050) forState:UIControlStateNormal];
    // >修改当前被选中的字体大小
    btn_item.titleLabel.font = selectedItemFontSize;
    [btn_item setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    CGSize size = [WYTool labelHeightWithText:btn_item.titleLabel.text andWidth:MAXFLOAT andFont:selectedItemFontSize];
    size = CGSizeMake(ceil(size.width)+10, ceil(size.height));
    
    [UIView animateWithDuration:0.2 delay:0 usingSpringWithDamping:0.5 initialSpringVelocity:0 options:0 animations:^{
        self.line.frame = CGRectMake(btn_item.x + (btn_item.w - size.width)*0.5, (self.bounds.size.height - lineHeight)*0.5, size.width, lineHeight);
    } completion:^(BOOL finished) {}];
    
    // >修改_presentBtnItemTag值为当前被选中的item的tag
    _presentBtnItemTag = btn_item.tag;
    // >修改_scrollView当前偏移量, 将当前item移到屏幕中间
    if ((btn_item.frame.origin.x + btn_item.frame.size.width / 2) - _scrollView.contentOffset.x != SCREENWIDTH / 2) {
//        带动画出Bug
//        [_scrollView setContentOffset:CGPointMake(btn_item.frame.origin.x + btn_item.frame.size.width/2 - SCREENWIDTH/2, 0) animated:YES];
        _scrollView.contentOffset = CGPointMake(btn_item.frame.origin.x + btn_item.frame.size.width/2 - SCREENWIDTH/2, 0);
        if (btn_item.frame.origin.x + btn_item.frame.size.width/2 < SCREENWIDTH/2) {
            _scrollView.contentOffset = CGPointMake(0, 0);
        }
        if (_scrollView.contentSize.width - _scrollView.contentOffset.x < _scrollView.frame.size.width) {
            _scrollView.contentOffset = CGPointMake(_scrollView.contentSize.width - _scrollView.frame.size.width, 0);
        }
    }
    // >回传被点击的item的tag内容, 在调用它的控制器中进一步处理
    self.itemBlock(btn_item.tag);
}

- (UIView *)line {
	if(_line == nil) {
		_line = [[UIView alloc] init];
        _line.backgroundColor = HEXCOLOR(0x52555B);
        _line.layer.cornerRadius = 4.0;
        _line.layer.masksToBounds = YES;
        [self.scrollView addSubview:_line];
	}
	return _line;
}

@end
