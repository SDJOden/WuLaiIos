//
//  WYSearchTitleLeftButton.m
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/8/17.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import "WYSearchTitleLeftButton.h"

@interface WYSearchTitleLeftButton ()

@property (nonatomic, strong) UILabel *showLabel;

@property (nonatomic, strong) UIImageView *downImageView;

@property (nonatomic, strong) UIImageView *separateView;

@property (nonatomic, strong) UIButton *leftButton;

@end

@implementation WYSearchTitleLeftButton

-(instancetype)initWithFrame:(CGRect)frame{
    
    if (self = [super initWithFrame:frame]) {
        
        [self layout];
    }
    return self;
}

-(instancetype)init{
    
    if (self = [super init]) {
        
        [self layout];
    }
    return self;
}

-(void)changeButtonTitle:(NSString *)title{
    
    self.showLabel.text = title;
}

-(void)layout{
    
    [self.showLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.left.equalTo(self);
        make.width.equalTo(50);
    }];
    
    [self.downImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(5);
        make.height.equalTo(3);
        make.centerY.equalTo(self.showLabel.centerY);
        make.left.equalTo(self.showLabel.right).offset(3);
    }];
    
    [self.separateView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(3);
        make.bottom.equalTo(-3);
        make.right.equalTo(self);
        make.width.equalTo(0.5);
    }];
    
    [self.leftButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.left.right.equalTo(self);
    }];
}

-(void)buttonClick{
    
    if ([self.wySearchTitleLeftButtonDelegate respondsToSelector:@selector(addPopView)]) {
        [self.wySearchTitleLeftButtonDelegate addPopView];
    }
}

- (UILabel *)showLabel {
    
	if(_showLabel == nil) {
		_showLabel = [[UILabel alloc] init];
        _showLabel.text = @"问题";
        _showLabel.textAlignment = NSTextAlignmentCenter;
        _showLabel.textColor = HEXCOLOR(0x5D5D5D);
        _showLabel.font = [UIFont systemFontOfSize:15];
        [self addSubview:_showLabel];
	}
	return _showLabel;
}

- (UIImageView *)downImageView {
    
	if(_downImageView == nil) {
		_downImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Triangle_black"]];
        [self addSubview:_downImageView];
	}
	return _downImageView;
}

- (UIImageView *)separateView {
    
	if(_separateView == nil) {
		_separateView = [[UIImageView alloc] init];
        _separateView.backgroundColor = HEXCOLOR(0xDFDFDF);
        [self addSubview:_separateView];
	}
	return _separateView;
}

- (UIButton *)leftButton {
    
	if(_leftButton == nil) {
		_leftButton = [[UIButton alloc] init];
        [_leftButton addTarget:self action:@selector(buttonClick) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_leftButton];
	}
	return _leftButton;
}

@end
