//
//  WYPopoverView.m
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/8/18.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import "WYPopoverView.h"

@interface WYPopoverView () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UIButton *backgroundButton;

@property (nonatomic, strong) UIImageView *backgroundImageView;

@property (nonatomic, strong) UITableView *queryTableView;

@end

@implementation WYPopoverView

-(instancetype)init{
    
    if (self = [super init]) {
        [self rcinit];
        [self layout];
    }
    return self;
}

-(void)rcinit{
    
    self.backgroundColor = [UIColor clearColor];
}

-(void)layout{
    
    [self.backgroundButton mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.left.right.bottom.equalTo(self);
    }];
    
    [self.queryTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.backgroundImageView.bottom).offset(-1);
        make.left.equalTo(8);
        make.height.equalTo(105);
        make.width.equalTo(90);
    }];
    
    [self.backgroundImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(23);
        make.width.equalTo(10);
        make.height.equalTo(6);
        make.top.equalTo(self);
    }];
}

#pragma mark --- UITableViewDelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 3;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"queryTypeCell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.textLabel.font = [UIFont systemFontOfSize:14];
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    cell.textLabel.textColor = HEXCOLOR(0x5D5D5D);
    if (indexPath.row == 0) {
        cell.textLabel.text = @"问题";
    }
    else if (indexPath.row == 1) {
        cell.textLabel.text = @"答案";
    }
    else {
        cell.textLabel.text = @"关键字";
    }
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 35;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if ([self.wyPopoverViewDelegate respondsToSelector:@selector(chooseQueryType:)]) {
        [self.wyPopoverViewDelegate chooseQueryType:cell.textLabel.text];
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]){
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    cell.preservesSuperviewLayoutMargins = NO;
}

#pragma mark --- 按钮点击事件
-(void)hiddenPopoverView{
    
    if ([self.wyPopoverViewDelegate respondsToSelector:@selector(closePopoverView)]) {
        [self.wyPopoverViewDelegate closePopoverView];
    }
}

#pragma mark --- 懒加载
- (UIImageView *)backgroundImageView {
    
    if(_backgroundImageView == nil) {
        _backgroundImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"TriangleUp"]];
        [self addSubview:_backgroundImageView];
    }
    return _backgroundImageView;
}

- (UITableView *)queryTableView {
    
	if(_queryTableView == nil) {
		_queryTableView = [[UITableView alloc] init];
        _queryTableView.delegate = self;
        _queryTableView.dataSource = self;
        _queryTableView.bounces = NO;
        _queryTableView.layer.borderWidth = 0.5;
        _queryTableView.layer.cornerRadius = 5.0;
        _queryTableView.layer.borderColor = [[UIColor lightGrayColor] CGColor];
        [_queryTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"queryTypeCell"];
        [self addSubview:_queryTableView];
	}
	return _queryTableView;
}

- (UIButton *)backgroundButton {
    
	if(_backgroundButton == nil) {
		_backgroundButton = [[UIButton alloc] init];
        _backgroundButton.backgroundColor = HEXCOLOR(0x101010);
        _backgroundButton.alpha = 0.4;
        [_backgroundButton addTarget:self action:@selector(hiddenPopoverView) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_backgroundButton];
	}
	return _backgroundButton;
}

@end
