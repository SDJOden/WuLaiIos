//
//  SDJChatRoomController.h
//  geniuslaiyeOC1.0
//
//  Created by 盛东 on 15/10/8.
//  Copyright © 2015年 shengdong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RCChatSessionInputBarControl.h"  // 输入框
#import "SDJCommonSetting.h"
#import "SDJIntelligentBoardView.h"
#import "SDJBaseViewController.h"
#import "RCEmojiBoardView.h"

@class RCMessageModel;

@interface SDJChatRoomController : SDJBaseViewController

@property(nonatomic, strong) NSString *targetId;

@property(nonatomic, strong) NSString *userName;

@property(nonatomic, assign) NSInteger unReadMessage;

@property(nonatomic, strong) UIButton *unReadButton;

@property(nonatomic) RCConversationType conversationType;

@property(nonatomic, strong) UICollectionView *conversationMessageCollectionView; // 展示会话的CollectionView控件，可以修改这个控件的属性比如背景

@property(nonatomic, strong) NSMutableArray *conversationDataRepository; // 会话数据存储数组

@property(nonatomic, strong) UICollectionViewFlowLayout *customFlowLayout;

@property(nonatomic, strong) RCChatSessionInputBarControl *chatSessionInputBarControl;

#pragma mark - 新增左边切换智能提示键盘
// 增加emoji键盘, timeSug键盘,重做Sug键盘
@property(nonatomic, strong) SDJIntelligentBoardView *sugView;

@property(nonatomic, strong) RCEmojiBoardView *emojiView;

@property(nonatomic, strong) UIPickerView *timeSugView;

@property(nonatomic, assign) BOOL isTakeNewPhoto; // 选图片用

@property(nonatomic, strong) UILabel *unReadNewMessageLabel;

//  默认No，如果Yes，开启右上角和右下角未读个数icon。
@property(nonatomic, assign) BOOL enableUnreadMessageIcon;

//  默认No,如果Yes, 当消息不在最下方时显示 右下角新消息数图标
@property(nonatomic, assign) BOOL enableNewComingMessageIcon;

// 是否显示发送者的名字，YES显示，NO不显示，默认是YES。
@property(nonatomic) BOOL displayUserNameInCell;

// 默认输入框类型，值为文本或者语言，默认为文本。
@property(nonatomic) RCChatSessionInputBarInputType defaultInputType;

// 当会话为聊天室时获取的历史信息数目，默认值为10，在viewDidLoad之前设置
@property(nonatomic, assign) int defaultHistoryMessageCountOfChatRoom;

- (id)initWithConversationType:(RCConversationType)conversationType targetId:(NSString *)targetId;

- (void)setMessageAvatarStyle:(RCUserAvatarStyle)avatarStyle;

- (void)setMessagePortraitSize:(CGSize)size;

#pragma mark - new

- (void)setIntelligentViewWithArray: (NSArray *)intelligentMessages;

@end
