
//  SDJChatRoomController.m
//  geniuslaiyeOC1.0
//
//  Created by 盛东 on 15/10/8.
//  Copyright © 2015年 shengdong. All rights reserved.
//


#import "SDJChatRoomController.h"
#import "SDJBaseWebViewController.h"
#import "servicelaiye2_0-Swift.h"
#import "SDJNormalWebController.h"
#import "SDJOriginalWebViewController.h"
#import "MobClick.h"
#import "RCIM.h"
#import "SBJson4.h"
#import "RCMessageCell.h"
#import "SDJTextMessageCell.h"
#import "SDJImageMessageCell.h"
#import "RCMessageModel.h"
#import "RCImagePreviewController.h"
#import "RCKitUtility.h"
#import "RCSystemSoundPlayer.h"
#import "RCImagePreviewController.h"
#import <RongIMLib/RongIMLib.h>
#import "RCSystemSoundPlayer.h"
#import "UIView+SDJExtension.h"

#define MAS_SHORTHAND_GLOBALS
#define MAS_SHORTHAND
#import "Masonry.h"

#import "SDJCardEnlargeController.h"

#import "SDJCardTypeOneCell.h"
#import "SDJCardTypeTwoCell.h"
#import "SDJCardTypeThreeCell.h"
#import "SDJCardTypeFourCell.h"
#import "SDJCardTypeFiveCell.h"

#import "SRRefreshView.h"

#import "RCImagePickerViewController.h"
#import "RCAlbumListViewController.h"
#import "RCAssetHelper.h"

#import "RCLocationMessageCell.h"
#import "RCLocationPickerViewController.h"
#import "RCLocationViewController.h"

#import "RCVoiceMessageCell.h"
#import "RCVoicePlayer.h"
#import "RCVoiceCaptureControl.h"

#import <AiSpeechOpenAPI/AiSpeechEngine.h>
#import <AiSpeechOpenAPI/AIJSONKit.h>
#import <AiSpeechOpenAPI/DialogParams.h>

#define Height_IntelBoardView 270.0f
#define Height_TimeBoardView 270.0f
#define Height_EmojiBoardView 220.0f

#define AISpeechAppKey @"14495639738594d3"
#define AISpeechSecretKey @"ca58f5cdcc5e79074b819ab29c270f92"

#define MAX_PICKER_NUMBER 200

typedef NS_ENUM(NSInteger, KBottomBarStatus) {
    KBottomBarDefaultStatus = 0,
    KBottomBarKeyboardStatus,
    KBottomBarSugStatus,
    KBottomBarEmojiStatus,
    KBottomBarTimeSugStatus,
    KBottomBarVoiceStatus
};

@interface SDJChatRoomController () <
UICollectionViewDelegate, UICollectionViewDataSource,
UICollectionViewDelegateFlowLayout, RCEmojiViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate, RCMessageCellDelegate,
RCChatSessionInputBarControlDelegate, UIGestureRecognizerDelegate,
UIScrollViewDelegate,SDJIntelligentBoardViewDelegate, UITextFieldDelegate, SRRefreshDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, RCImagePickerViewControllerDelegate, RCAlbumListViewControllerDelegate, RCLocationPickerViewControllerDelegate, RCVoiceCaptureControlDelegate, AiSpeechEngineDelegate>

@property(nonatomic) KBottomBarStatus currentBottomBarStatus;
@property(nonatomic, strong) RCMessageModel *longPressSelectedModel;
@property(nonatomic, assign) BOOL isConversationAppear;
@property(nonatomic) BOOL isLoading;
@property(nonatomic, strong) RCMessage *tempMessage;
@property(nonatomic, assign) BOOL isNeedScrollToButtom;

@property (nonatomic, strong) UIImageView *unreadRightBottomIcon;
@property (nonatomic, assign) NSInteger unreadNewMsgCount;

@property (nonatomic, assign) NSInteger  scrollNum;
@property (nonatomic, assign) NSInteger  sendOrReciveMessageNum;//记录新收到和自己新发送的消息数，用于计算加载历史消息时插入“以上是历史消息”cell 的位置

@property (nonatomic, strong) MASConstraint *chatControlBottomConstraint;

@property (nonatomic, strong)SRRefreshView *slimeView;

@property (nonatomic, assign)int refreshFlag;

@property (nonatomic, assign)int receiveMessageFlag;

@property (nonatomic, assign)int testCardflag;

@property (nonatomic, strong) UIImagePickerController *currentPicker;

@property (nonatomic, strong) RCVoiceCaptureControl *voiceCaptureControl; // 语音工具类

@property (nonatomic, assign) BOOL isAudioRecoderTimeOut;

@property (nonatomic, strong)NSArray *timeSugArr;

// 语音识别
@property (nonatomic, strong)AiSpeechEngine *engine;

@property (nonatomic, strong)DialogParams *params;

@property (nonatomic, assign)BOOL isRunning;

@end

static NSString *const sdjtextCellIndentifier = @"sdjtextCellIndentifier";
static NSString *const sdjimageCellIndentifier = @"sdjimageCellIndentifier";
static NSString *const sdjCardTypeOneCellIndentifier = @"sdjCardTypeOneCellIndentifier";
static NSString *const sdjCardTypeTwoCellIndentifier = @"sdjCardTypeTwoCellIndentifier";
static NSString *const sdjCardTypeThreeCellIndentifier = @"sdjCardTypeThreeCellIndentifier";
static NSString *const sdjCardTypeFourCellIndentifier = @"sdjCardTypeFourCellIndentifier";
static NSString *const sdjCardTypeFiveCellIndentifier = @"sdjCardTypeFiveCellIndentifier";

static NSString *const sdjlocationCellIndentifier = @"sdjlocationCellIndentifier";
static NSString *const sdjvoiceCellIndentifier = @"sdjvoiceCellIndentifier";


@implementation SDJChatRoomController

- (id)initWithConversationType:(RCConversationType)conversationType
                      targetId:(NSString *)targetId {
    self = [super init];
    if (self) {
        [self rcinit];
        self.conversationType = conversationType;
        self.targetId = targetId;
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self rcinit];
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self rcinit];
    }
    return self;
}

- (void)rcinit {
    _isLoading = NO;
    _isConversationAppear = NO;
    self.conversationDataRepository = [[NSMutableArray alloc] init];
    self.conversationMessageCollectionView = nil;
    self.targetId = nil;
    self.userName = nil;
    self.currentBottomBarStatus = KBottomBarDefaultStatus;
    [self registerNotification];
    self.displayUserNameInCell = YES;
    self.defaultInputType = RCChatSessionInputBarInputText;
    self.defaultHistoryMessageCountOfChatRoom = 10;
    
    _enableNewComingMessageIcon = YES;
    _enableUnreadMessageIcon = YES;
}

- (void)registerNotification {
    
    //注册接收消息
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveMessageNotification:) name:RCKitDispatchMessageNotification object:nil];
}

- (void)registerClass:(Class)cellClass forCellWithReuseIdentifier:(NSString *)identifier {
    
    [self.conversationMessageCollectionView registerClass:cellClass forCellWithReuseIdentifier:identifier];
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
//    self.navigationController.navigationBarHidden = YES;
    
    [MobClick beginLogPageView:@"chatroom"];
    
//    [[RCSystemSoundPlayer defaultPlayer] setIgnoreConversationType:self.conversationType targetId:self.targetId];
    
    self.scrollNum = 0;

    NSString *draft = [[RCIMClient sharedRCIMClient] getTextMessageDraft:self.conversationType targetId:self.targetId];
    if (draft) {
        self.chatSessionInputBarControl.inputTextView.text = draft;
        // clear
        [[RCIMClient sharedRCIMClient] saveTextMessageDraft:self.conversationType targetId:self.targetId content:nil];
    }
    
    [self layoutBottomBarWithStatus:KBottomBarDefaultStatus];
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    [MobClick event:@"chatroom_click" label:@"viewAppear"];
    [MobClick beginEvent:@"chatroom_time" label:@"点击发送按钮"];
    [MobClick beginEvent:@"chatroom_time" label:@"点击输入框"];
    [MobClick beginEvent:@"chatroom_time" label:@"点击sug键盘"];
    [MobClick beginEvent:@"chatroom_time" label:@"点击用户中心"];
    [MobClick beginEvent:@"chatroom_time" label:@"点击订单列表"];
    
    _isConversationAppear = YES;
    [[RCIMClient sharedRCIMClient] clearMessagesUnreadStatus:self.conversationType
                                                    targetId:self.targetId];
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
    [MobClick endLogPageView:@"chatroom"];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationStopVoicePlayer object:nil]; // 停止语音播放器
    
    [[RCSystemSoundPlayer defaultPlayer] resetIgnoreConversation];
    
    [[RCIMClient sharedRCIMClient] clearMessagesUnreadStatus:self.conversationType
                                                    targetId:self.targetId];
    
    _isConversationAppear = NO;
    
    NSString *draft = [self.chatSessionInputBarControl.inputTextView.text
                       stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    [[RCIMClient sharedRCIMClient] saveTextMessageDraft:self.conversationType
                                               targetId:self.targetId
                                                content:draft];
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    _testCardflag = 0;
    
    self.isAudioRecoderTimeOut = NO;
    
    _isRunning = NO;
    [self createEngine];
    
    [self.timeSugView selectRow:MAX_PICKER_NUMBER inComponent:0 animated:YES];
    [self.timeSugView selectRow:MAX_PICKER_NUMBER inComponent:1 animated:YES];
    [self.timeSugView selectRow:MAX_PICKER_NUMBER inComponent:2 animated:YES];
    
    [self pickerView:self.timeSugView didSelectRow:MAX_PICKER_NUMBER inComponent:0];
    [self pickerView:self.timeSugView didSelectRow:MAX_PICKER_NUMBER inComponent:1];
    [self pickerView:self.timeSugView didSelectRow:MAX_PICKER_NUMBER inComponent:2];
    
    NSString *group = @"rc_group_";
    
    NSString *targetid = [group stringByAppendingFormat:@"%ld",[SDJUserAccount loadAccount].user_id];
    
    [[RCIMClient sharedRCIMClient] setConversationNotificationStatus:ConversationType_GROUP targetId:targetid isBlocked:YES success:^(RCConversationNotificationStatus nStatus) {
        
    } error:^(RCErrorCode status) {
        
    }];
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:NO];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    NSMutableArray *intelligentArr = [[NSMutableArray alloc] initWithCapacity:0];
    
    [[SDJNetworkTools sharedTools] getSpecialSuglist:[SDJUserAccount loadAccount].utoken version:@"1.0.0" finish:^(NSDictionary<NSString *,id> * result, NSError * error) {
        
        if (![[result[@"error"] allKeys] containsObject:@"error_code"]) {
            for (int i = 0; i < [result[@"data"] count]; i++) {
                [intelligentArr addObject:result[@"data"][i][@"sug_text"]];
            }
            self.sugView.inteligentMessages = intelligentArr;
            
            [RCIM sharedRCIM].inteligentMessages = intelligentArr;
            
        }  else {
            if ([result[@"error"][@"error_code"] isEqualToNumber:@20001]) {
                // 退出
                SDJUserAccount *account = [SDJUserAccount loadAccount];
                
                account.available = NO;
                
                [account savaAccount];
                
                [[RCIMClient sharedRCIMClient] logout];
                
                [MobClick endEvent:@"registerView_time" label:@"登出"];
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"SDJRootViewControllerSwitchNotification" object:@"0"];
            }
        }
    }];
    
    if (IOS_FSystenVersion >= 7.0) {
        self.extendedLayoutIncludesOpaqueBars = YES;
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    [self initializedSubViews];
    
    //加载历史数据
    if (!(self.conversationType == ConversationType_CHATROOM)) {
            [self loadLatestHistoryMessage];
    }
    
    if (ConversationType_CHATROOM == self.conversationType) {
        [[RCIMClient sharedRCIMClient] joinChatRoom:self.targetId messageCount:self.defaultHistoryMessageCountOfChatRoom success:^{} error:^(RCErrorCode status) {
            __weak SDJChatRoomController *weakSelf = self;
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakSelf loadErrorAlert:NSLocalizedString(@"JoinChatRoomFailed", @"")];
            });
        }];
    }
    
    [self layoutBottomBarWithStatus:KBottomBarDefaultStatus];
    
    [[RCIMClient sharedRCIMClient] clearMessagesUnreadStatus:self.conversationType targetId:self.targetId];
}

-(void)createEngine
{
    if (_engine != nil) {
        [_engine releaseEngine];
    }
    
    AiEngineConfig *engineCfg = [[AiEngineConfig alloc] init];
    [engineCfg setAppKey:AISpeechAppKey];
    [engineCfg setAppSecretKey:AISpeechSecretKey];
    
    NSLog(@"config--:\n\n%@\n\n",engineCfg) ;
    _engine = [[AiSpeechEngine alloc] initWithDelegate:engineCfg Delegate:self];
    
    _chatSessionInputBarControl.recordButton.enabled = NO;
}

- (void)loadErrorAlert:(NSString *)title {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:nil];
    [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(cancelAlertAndGoBack:) userInfo:alert repeats:NO];
    [alert show];
}

- (void)cancelAlertAndGoBack:(NSTimer *)scheduledTimer {
    
    UIAlertView *alert = (UIAlertView *)(scheduledTimer.userInfo);
    [alert dismissWithClickedButtonIndex:0 animated:NO];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)setIntelligentViewWithArray:(NSArray *)intelligentMessages {
    
    self.sugView.inteligentMessages = intelligentMessages;
}

#pragma mark - AiSpeechEngineDelegate

// 把语音切片发过去解析, 实现"一个字一个字往外面蹦"的效果
// 时间间隔timeSeparate, 一旦语音结束立即发送所有内容

//引擎创建成功与否回调
-(void)aiSpeechEngine:(AiSpeechEngine *)engine onInit:(NSNumber *)state {
    if([state isEqualToNumber:[NSNumber numberWithInt:OPT_SUCCESS]]) {
        _chatSessionInputBarControl.recordButton.enabled = YES ;
    }
}
//录音停止回调
-(void)aiSpeechEngineDidFinishRecording:(AiSpeechEngine *)this stopType:(int)stopType
{
    _isRunning = NO;
}

//响应回调
-(void)aiSpeechEngine:(AiSpeechEngine *)engine didReceive:(NSString *)recId responseJson:(NSString *)jsonString
{
    NSDictionary *resDic = [jsonString  objectFromAIJSONString];

    resDic = [resDic valueForKey:@"result"];

    if ([resDic valueForKey:@"input"]) {
        _chatSessionInputBarControl.recordButton.hidden = YES;
        _chatSessionInputBarControl.inputTextView.text = [resDic valueForKey:@"input"];
        _chatSessionInputBarControl.sendButton.enabled = YES;

        // 设置inputTextView.text后, 聊天框和输入框大小都随着变化
        [_chatSessionInputBarControl changeChatSessionInputBarFrame];
    }
    NSLog(@"AiSpeechEngine:didReceive:%@ responseJson:%@", recId, jsonString);
}

//异常回调
-(void)aiSpeechEngine:(AiSpeechEngine *)engine didReceive:(NSString *)recId errorJson:(NSString *)jsonString
{
    NSDictionary *respDict = [jsonString objectFromAIJSONString];
    NSNumber *errNum = (NSNumber*)[respDict objectForKey:@"errId"];
    NSInteger errId = [errNum integerValue] ;
    
    [self showError:errId] ;
}

//常见错误码的处理
-(void)showError:(NSInteger) errorId {
    NSString *showMsg = @"出错了";
    switch (errorId) {
        case 70200:
            showMsg = @"网络未连接";
            break;
        case 44001:
        case 70904:
            showMsg = @"没有检测到语音";
            break ;
        case 70906:
            showMsg = @"网络连接超时";
            break ;
        case 40003:
        case 70401:
            showMsg = @"网络连接超时" ;
            break ;
        case 70905:
            showMsg = @"音频时长超出阈值";
            break ;
        default:
            break;
    }
    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"提示" message:showMsg
                                                 delegate:self cancelButtonTitle:@"确定" otherButtonTitles: nil];
    [alert show];
}

#pragma mark - slimeRefresh delegate
- (void)slimeRefreshStartRefresh:(SRRefreshView *)refreshView
{
    [_slimeView performSelector:@selector(endRefresh) withObject:nil afterDelay:3 inModes:[NSArray arrayWithObject:NSRunLoopCommonModes]];
}

- (void)dealloc {
    self.conversationMessageCollectionView.dataSource = nil;
    self.conversationMessageCollectionView.delegate = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    if(_engine) {
        [_engine releaseEngine] ;
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

// 懒加载
- (RCEmojiBoardView *)emojiView {
    if (!_emojiView) {
        _emojiView = [[RCEmojiBoardView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, Height_EmojiBoardView)];
        _emojiView.delegate = self;
        [self.view addSubview:_emojiView];
    }
    return _emojiView;
}

- (UIPickerView *)timeSugView {
    if (!_timeSugView) {
        _timeSugView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, Height_TimeBoardView)];
        _timeSugView.delegate = self;
        _timeSugView.dataSource = self;
        _timeSugView.backgroundColor = HEXCOLOR(0xe7e2dd);
        [self.view addSubview:_timeSugView];
    }
    return _timeSugView;
}

- (NSArray *)timeSugArr {
    if (!_timeSugArr) {
        _timeSugArr = [[NSArray alloc] init];
    }
    return _timeSugArr;
}

#pragma mark UIPickerViewDatasource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    
    return 3; // _timeSugArr.count
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
    switch ((int)component) {
        case 0:
            return 10 * MAX_PICKER_NUMBER;
            break;
        case 1:
            return 5 * MAX_PICKER_NUMBER;
            break;
        case 2:
            return 8 * MAX_PICKER_NUMBER; // 从字典或本地解析
            break;
        default:
            return 0;
            break;
    }
}

#pragma mark UIPickerViewDelegate
- (NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component {
    
    NSString *title = @"";
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    
    switch (component) {
        case 0:
            title = @"heheda"; // _array1[row%200]
            [paragraphStyle setAlignment:NSTextAlignmentRight];
            break;
        case 1:
            title = @"memeda";
            [paragraphStyle setAlignment:NSTextAlignmentCenter];
            
            break;
        case 2:
            title = @"nubility";
            [paragraphStyle setAlignment:NSTextAlignmentLeft];
            break;
    }
    
    
    return [[NSAttributedString alloc] initWithString:title attributes:
            @{NSForegroundColorAttributeName: HEXCOLOR(0x403B36),
              NSFontAttributeName: [UIFont systemFontOfSize:18],
              NSParagraphStyleAttributeName: paragraphStyle}];
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component {
    
    return 44;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    
    switch ((int)component) {
        case 0:
            return 140;
            break;
        case 1:
            return 140;
            break;
        case 2:
            return 140;
            break;
        default:
            return 0;
            break;
    }
}

//- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view {
//    return [UIView new];
//}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
//    NSInteger indexOne = [self.timeSugView selectedRowInComponent:0];
//    NSInteger indexTwo = [self.timeSugView selectedRowInComponent:1];
//    NSInteger indexThree = [self.timeSugView selectedRowInComponent:2];
    
    NSString *str1 = @"heheda"; // _array1[indexOne]
    NSString *str2 = @"memeda";
    NSString *str3 = @"nubility";
    
    if (_currentBottomBarStatus == KBottomBarTimeSugStatus) {
        _chatSessionInputBarControl.inputTextView.text = [[str1 stringByAppendingString:str2] stringByAppendingString:str3];
        _chatSessionInputBarControl.sendButton.enabled = YES;
    }
}

- (UIImageView *)unreadRightBottomIcon {
    
    if (!_unreadRightBottomIcon) {
        
        UIImage *msgCountIcon = [RCKitUtility imageNamed:@"conversation_item_unreadcount_icon" ofBundle:@"RongCloud.bundle"];
        _unreadRightBottomIcon = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, msgCountIcon.size.width, msgCountIcon.size.height)];
        _unreadRightBottomIcon.userInteractionEnabled = YES;
        _unreadRightBottomIcon.image = msgCountIcon;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tabRightBottomMsgCountIcon:)];
        [_unreadRightBottomIcon addGestureRecognizer:tap];
        
        _unReadNewMessageLabel = [[UILabel alloc]initWithFrame:self.unreadRightBottomIcon.bounds];
        _unReadNewMessageLabel.backgroundColor = [UIColor clearColor];
        _unReadNewMessageLabel.font = [UIFont systemFontOfSize:12.0f];
        _unReadNewMessageLabel.textAlignment = NSTextAlignmentCenter;
        _unReadNewMessageLabel.textColor = [UIColor whiteColor];
        _unReadNewMessageLabel.center = CGPointMake(self.unReadNewMessageLabel.frame.size.width/2, self.unReadNewMessageLabel.frame.size.height/2 - 2 );
        [_unreadRightBottomIcon addSubview:self.unReadNewMessageLabel];
        _unreadRightBottomIcon.hidden = YES;
        
        [self.view addSubview:_unreadRightBottomIcon];
        [self.view bringSubviewToFront:_unreadRightBottomIcon];
    }
    return _unreadRightBottomIcon;
}

- (SDJIntelligentBoardView *)sugView {
    if (!_sugView) {
        
        _sugView = [[SDJIntelligentBoardView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, Height_IntelBoardView) andMessageArray: nil];
        _sugView.delegate = self;
        [self.view addSubview:_sugView];
    }
    return _sugView;
}

- (RCChatSessionInputBarControl *)chatSessionInputBarControl {
    if (!_chatSessionInputBarControl) {
        
        _chatSessionInputBarControl = [[RCChatSessionInputBarControl alloc]initWithFrame:CGRectMake(0, self.view.bounds.size.height - Height_ChatSessionInputBar, self.view.bounds.size.width, Height_ChatSessionInputBar) withContextView:self.view type:0 style:RC_CHAT_INPUT_BAR_STYLE_SWITCH_CONTAINER_EXTENTION];
        _chatSessionInputBarControl.delegate = self;
        [self.view addSubview:_chatSessionInputBarControl];
    }
    return _chatSessionInputBarControl;
}

- (UICollectionView *)conversationMessageCollectionView {
    
    if (!_conversationMessageCollectionView) {
        
        _customFlowLayout = [[UICollectionViewFlowLayout alloc] init];
        _customFlowLayout.minimumLineSpacing = 0.0f;
        _customFlowLayout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
        _customFlowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
        
        _conversationMessageCollectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:self.customFlowLayout];
        [_conversationMessageCollectionView setBackgroundColor:HEXCOLOR(0xECE8E5)];// MARK 单聊背景色
        _conversationMessageCollectionView.showsHorizontalScrollIndicator = NO;
        _conversationMessageCollectionView.alwaysBounceVertical = YES;
        
        _slimeView = [[SRRefreshView alloc] init];
        _slimeView.delegate = self;
        _slimeView.upInset = 0;
        _slimeView.slimeMissWhenGoingBack = YES;
        _slimeView.slime.bodyColor = [UIColor blackColor];
        _slimeView.slime.skinColor = [UIColor whiteColor];
        _slimeView.slime.lineWith = 1;
        _slimeView.slime.shadowBlur = 4;
        _slimeView.slime.shadowColor = [UIColor blackColor];
        [_conversationMessageCollectionView addSubview:_slimeView];
        _slimeView.frame = CGRectMake(0, -40, self.view.bounds.size.width, 40);
        
        [self registerClass:[SDJTextMessageCell class] forCellWithReuseIdentifier:sdjtextCellIndentifier];
        [self registerClass:[SDJImageMessageCell class] forCellWithReuseIdentifier:sdjimageCellIndentifier];
        [self registerClass:[RCLocationMessageCell class] forCellWithReuseIdentifier:sdjlocationCellIndentifier];
        [self registerClass:[RCVoiceMessageCell class] forCellWithReuseIdentifier:sdjvoiceCellIndentifier];
        
        [self registerClass:[SDJCardTypeOneCell class] forCellWithReuseIdentifier:sdjCardTypeOneCellIndentifier];
        [self registerClass:[SDJCardTypeTwoCell class] forCellWithReuseIdentifier:sdjCardTypeTwoCellIndentifier];
        [self registerClass:[SDJCardTypeThreeCell class] forCellWithReuseIdentifier:sdjCardTypeThreeCellIndentifier];
        [self registerClass:[SDJCardTypeFourCell class] forCellWithReuseIdentifier:sdjCardTypeFourCellIndentifier];
        [self registerClass:[SDJCardTypeFiveCell class] forCellWithReuseIdentifier:sdjCardTypeFiveCellIndentifier];
        
        self.conversationMessageCollectionView.dataSource = self;
        self.conversationMessageCollectionView.delegate = self;
        
        [self.view addSubview:_conversationMessageCollectionView];
        
        // 点击聊天页滚到底部的tap手势
        UITapGestureRecognizer *resetBottomTapGesture =
        [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap4ResetDefaultBottomBarStatus:)];
        [resetBottomTapGesture setDelegate:self];
        [_conversationMessageCollectionView addGestureRecognizer:resetBottomTapGesture];
    }
    return _conversationMessageCollectionView;
}

- (void)initializedSubViews {
    
    [self.chatSessionInputBarControl makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.right.equalTo(self.view);
        _chatControlBottomConstraint = make.bottom.equalTo(self.view);
        make.height.equalTo(Height_ChatSessionInputBar);
    }];
    
    [self.conversationMessageCollectionView makeConstraints:^(MASConstraintMaker *make) {
        
        make.top.equalTo(self.view).offset(0);
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(_chatSessionInputBarControl.top);
    }];
    
    [self.unreadRightBottomIcon makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.view).offset(-20);
        make.bottom.equalTo(self.view).offset(-10 - Height_ChatSessionInputBar);
        make.height.width.equalTo(37);
    }];
    
    [self.sugView makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(self.view);
        make.bottom.equalTo(self.view).offset(Height_IntelBoardView);
        make.height.equalTo(Height_IntelBoardView);
        make.width.equalTo(self.view);
    }];
    
    [self.emojiView makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(self.view);
        make.bottom.equalTo(self.view).offset(Height_EmojiBoardView);
        make.height.equalTo(Height_EmojiBoardView);
        make.width.equalTo(self.view);
    }];
    
    [self.timeSugView makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(self.view);
        make.bottom.equalTo(self.view).offset(Height_TimeBoardView);
        make.height.equalTo(Height_TimeBoardView);
        make.width.equalTo(self.view);
    }];
    
    [self.view layoutIfNeeded];
    
    [self slimeRefreshStartRefresh:_slimeView];
    
    _currentBottomBarStatus = KBottomBarDefaultStatus;
    
    if (_currentBottomBarStatus != KBottomBarDefaultStatus) {
        [self layoutBottomBarWithStatus:KBottomBarDefaultStatus];
    }
}

- (void)updateUnreadMsgCountLabel
{
    if (self.unreadNewMsgCount == 0) {
        
        self.unreadRightBottomIcon.hidden = YES;
    }
    else
    {
        self.unreadRightBottomIcon.hidden = NO;
        self.unReadNewMessageLabel.text = (self.unreadNewMsgCount > 99) ?  @"99+" : [NSString stringWithFormat:@"%li", (long)self.unreadNewMsgCount];
    }
}

- (void) checkVisiableCell
{
    NSIndexPath *lastPath = [self getLastIndexPathForVisibleItems];
    
    if (lastPath.row >= self.conversationDataRepository.count - self.unreadNewMsgCount || lastPath == nil || [self isAtTheBottomOfTableView] ) {
        
        self.unreadNewMsgCount = 0;
        [self updateUnreadMsgCountLabel];
    }
}

- (NSIndexPath *)getLastIndexPathForVisibleItems
{
    NSArray *visiblePaths = [self.conversationMessageCollectionView indexPathsForVisibleItems];
    
    if (visiblePaths.count == 0) {
        return nil;
    }else if(visiblePaths.count == 1) {
        return (NSIndexPath *)[visiblePaths firstObject];
    }
    
    NSArray *sortedIndexPaths = [visiblePaths sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        NSIndexPath *path1 = (NSIndexPath *)obj1;
        NSIndexPath *path2 = (NSIndexPath *)obj2;
        return [path1 compare:path2];
    }];
    
    return (NSIndexPath *)[sortedIndexPaths lastObject];
}

#pragma mark <UIScrollViewDelegate>
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    if (_currentBottomBarStatus != KBottomBarDefaultStatus) {
        [self layoutBottomBarWithStatus:KBottomBarDefaultStatus];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    // 是否显示右下未读icon
    if (self.enableNewComingMessageIcon == YES || self.unreadNewMsgCount != 0) {
        [self checkVisiableCell];
    }
    
    _refreshFlag++;
    if (_refreshFlag >1) {
        [_slimeView scrollViewDidScroll];
        if (scrollView.contentOffset.y < -5.0f) {
            [self slimeRefreshStartRefresh:_slimeView];
        } else {
            [_slimeView endRefresh];
        }
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView
                  willDecelerate:(BOOL)decelerate {
    [_slimeView scrollViewDidEndDraging];
    if (scrollView.contentOffset.y < -15) {
        [self performSelector:@selector(loadMoreHistoryMessage) withObject:nil afterDelay:0.4f];
    }
}

- (void)scrollToBottomAnimated:(BOOL)animated {
    
    if ([self.conversationMessageCollectionView numberOfSections] == 0) {return;}
    
    NSUInteger finalRow = MAX(0, [self.conversationMessageCollectionView numberOfItemsInSection:0] - 1);
    
    if (0 == finalRow) {return;}
    
    NSIndexPath *finalIndexPath = [NSIndexPath indexPathForItem:finalRow inSection:0];
    
    [self.conversationMessageCollectionView scrollToItemAtIndexPath:finalIndexPath atScrollPosition:UICollectionViewScrollPositionBottom animated:animated];
}

#pragma mark <UICollectionViewDataSource>
//定义展示的UICollectionViewCell的个数
- (NSInteger)collectionView:(UICollectionView *)collectionView
     numberOfItemsInSection:(NSInteger)section {
    return self.conversationDataRepository.count;
}

//每个UICollectionView展示的内容
#warning ---------------------------------------- 接收消息展示 ----------------------------------------
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    RCMessageModel *model =
    [self.conversationDataRepository objectAtIndex:indexPath.row];
    
    model.isDisplayNickname = NO;
    
    if (indexPath.row == 0) {
        
        model.isDisplayMessageTime = NO;
    }
    
    RCMessageContent *messageContent = model.content;
    
    RCMessageCell *cell = nil;
    if ([messageContent isMemberOfClass:[RCTextMessage class]]) {
        
        RCMessageCell *__cell;
        
        switch (_testCardflag) {
            case 0:
                __cell = [collectionView dequeueReusableCellWithReuseIdentifier:sdjtextCellIndentifier forIndexPath:indexPath];
                break;
            case 1:
                __cell = [collectionView dequeueReusableCellWithReuseIdentifier:sdjCardTypeOneCellIndentifier forIndexPath:indexPath];
                break;
            case 2:
                __cell = [collectionView dequeueReusableCellWithReuseIdentifier:sdjCardTypeTwoCellIndentifier forIndexPath:indexPath];
                break;
            case 3:
                __cell = [collectionView dequeueReusableCellWithReuseIdentifier:sdjCardTypeThreeCellIndentifier forIndexPath:indexPath];
                break;
            case 4:
                __cell = [collectionView dequeueReusableCellWithReuseIdentifier:sdjCardTypeFourCellIndentifier forIndexPath:indexPath];
                break;
            case 5:
                __cell = [collectionView dequeueReusableCellWithReuseIdentifier:sdjCardTypeFiveCellIndentifier forIndexPath:indexPath];
                break;
            default:
                break;
        }
        
        [__cell setDataModel:model];
        [__cell setDelegate:self];
        cell = __cell;
    } else if ([messageContent isMemberOfClass:[RCImageMessage class]]) {
        
        RCMessageCell *__cell = [collectionView dequeueReusableCellWithReuseIdentifier:sdjimageCellIndentifier
                                 forIndexPath:indexPath];
        [__cell setDataModel:model];
        [__cell setDelegate:self];
        cell = __cell;
    } else if ([messageContent isMemberOfClass:[RCLocationMessage class]]) {
        RCMessageCell *__cell = [collectionView dequeueReusableCellWithReuseIdentifier:sdjlocationCellIndentifier
                                 forIndexPath:indexPath];
        [__cell setDataModel:model];
        [__cell setDelegate:self];
        cell = __cell;
    } else if ([messageContent isMemberOfClass:[RCVoiceMessage class]]) {
        RCMessageCell *__cell = [collectionView
                                 dequeueReusableCellWithReuseIdentifier:sdjvoiceCellIndentifier
                                 forIndexPath:indexPath];
        [__cell setDataModel:model];
        [__cell setDelegate:self];
        cell = __cell;
    } else if ([messageContent isMemberOfClass:[RCRichContentMessage class]]) {
        
        RCRichContentMessage *message = (RCRichContentMessage *)messageContent;
        
        int title = [message.title intValue];
        
        RCMessageCell *__cell;
        
        switch (title) {
            case 6:
                __cell = [collectionView dequeueReusableCellWithReuseIdentifier:sdjCardTypeOneCellIndentifier forIndexPath:indexPath];
                break;
            case 7:
                __cell = [collectionView dequeueReusableCellWithReuseIdentifier:sdjCardTypeTwoCellIndentifier forIndexPath:indexPath];
                break;
            case 8:
                __cell = [collectionView dequeueReusableCellWithReuseIdentifier:sdjCardTypeFiveCellIndentifier forIndexPath:indexPath];
                break;
            default:
                break;
        }

        [__cell setDataModel:model];
        [__cell setDelegate:self];
        cell = __cell;
    }
    
    return cell;
}

#pragma mark <UICollectionViewDelegateFlowLayout>

//计算cell高度
- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    RCMessageModel *model =
    [self.conversationDataRepository objectAtIndex:indexPath.row];
    
    if (indexPath.row == 0) {
        model.isDisplayMessageTime = NO;
    }
    model.isDisplayNickname = NO;
    
    RCMessageContent *messageContent = model.content;
    if ([messageContent isMemberOfClass:[RCTextMessage class]] ||
        [messageContent isMemberOfClass:[RCImageMessage class]] ||
        [messageContent isMemberOfClass:[RCLocationMessage class]] ||
        [messageContent isMemberOfClass:[RCVoiceMessage class]] ||
        [messageContent isMemberOfClass:[RCRichContentMessage class]]) {
            
            CGSize _size = [self sizeForItem:collectionView atIndexPath:indexPath];
            return _size;
        }
    else {
        return CGSizeZero;
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    return CGSizeZero;
}

#pragma mark <UICollectionViewDelegate>
- (void)figureOutAllConversationDataRepository {
    for (int i = 0; i < self.conversationDataRepository.count; i++) {
        RCMessageModel *model = [self.conversationDataRepository objectAtIndex:i];
        if (0 == i) {
            model.isDisplayMessageTime = YES;
        } else if (i > 0) {
            RCMessageModel *pre_model = [self.conversationDataRepository objectAtIndex:i - 1];
            RCMessageDirection pre_messageDirection = pre_model.messageDirection;
            long long previous_time = (pre_messageDirection == MessageDirection_SEND)? pre_model.sentTime: pre_model.receivedTime;
            
            RCMessageDirection current_messageDirection = model.messageDirection;
            long long current_time = (current_messageDirection == MessageDirection_SEND)? model.sentTime: model.receivedTime;
            
            long long interval = current_time - previous_time > 0 ? current_time - previous_time : previous_time - current_time;
            if (interval / 1000 <= 60) {
                model.isDisplayMessageTime = NO;
            } else {
                model.isDisplayMessageTime = YES;
            }
        }
    }
}

- (void)figureOutLatestModel:(RCMessageModel *)model; // 找到最后一条信息
{
    if (_conversationDataRepository.count > 0) {
        
        RCMessageModel *pre_model = [self.conversationDataRepository objectAtIndex:_conversationDataRepository.count - 1];
        RCMessageDirection pre_messageDirection = pre_model.messageDirection;
        long long previous_time = (pre_messageDirection == MessageDirection_SEND) ? pre_model.sentTime : pre_model.receivedTime;
        
        RCMessageDirection current_messageDirection = model.messageDirection;
        long long current_time = (current_messageDirection == MessageDirection_SEND)? model.sentTime : model.receivedTime;
        
        long long interval = current_time - previous_time > 0 ? current_time - previous_time : previous_time - current_time;
        
        if (interval / 1000 <= 60) {
            model.isDisplayMessageTime = NO;
        } else {
            model.isDisplayMessageTime = YES;
        }
        
    } else {
        model.isDisplayMessageTime = YES;
    }
}

#warning 在这里截获收到的信息,解析其中的内容(intelligent部分内容, 然后设置给外部的ChatRoomController,由外部ChatRoomController设置intelligent版面的内容)
- (void)appendAndDisplayMessage:(RCMessage *)rcMessage {
    
    self.sendOrReciveMessageNum++;//记录新收到和自己新发送的消息数，用于计算加载历史消息时插入“以上是历史消息”cell 的位置
    RCMessageModel *model = [[RCMessageModel alloc] initWithMessage:rcMessage];
    if ([model.content isKindOfClass:[RCTextMessage class]]) {
        RCTextMessage *textMessage = (RCTextMessage *)model.content;
        
        if (textMessage.extra != nil) {
            NSData *jsonData = [textMessage.extra dataUsingEncoding:NSUTF8StringEncoding];
            NSError *err;
            NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData
                                                                options:NSJSONReadingMutableContainers
                                                                  error:&err];
            NSArray *arr = dic[@"msg_detail"][@"sug_list"];
            if (arr.count != 0) {
                NSMutableArray *arrM = [[NSMutableArray alloc] initWithCapacity:0];
                
                for (int i = 0; i < arr.count; i++) {
                    [arrM addObject:arr[i][@"sug_detail"]];
                }
                [self setIntelligentViewWithArray:arrM];
            } else {
                [self setIntelligentViewWithArray:[RCIM sharedRCIM].inteligentMessages];
            }
        }
    } else {
        [self setIntelligentViewWithArray:[RCIM sharedRCIM].inteligentMessages];
    }
    
    [self figureOutLatestModel:model];
    if ([self appendMessageModel:model]) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:self.conversationDataRepository.count - 1 inSection:0];
        if ([self.conversationMessageCollectionView numberOfItemsInSection:0] != self.conversationDataRepository.count - 1) {
            NSLog(@"Error, datasource and collectionview are inconsistent!!");
            [self.conversationMessageCollectionView reloadData];
            return;
        }
        [self.conversationMessageCollectionView insertItemsAtIndexPaths:[NSArray arrayWithObject:indexPath]];
        [self.conversationMessageCollectionView.collectionViewLayout invalidateLayout];
        
        if ([self isAtTheBottomOfTableView] || self.isNeedScrollToButtom) {
            [self scrollToBottomAnimated:YES];
            self.isNeedScrollToButtom=NO;
        }
    }
}

- (BOOL)appendMessageModel:(RCMessageModel *)model {
    
    long ne_wId = model.messageId;
    for (RCMessageModel *__item in self.conversationDataRepository) {
        //当id为－1时，不检查是否重复，直接插入
        if (ne_wId == -1) {
            break;
        }
        if (ne_wId == __item.messageId) {
            return NO;
        }
    }
    if (ne_wId != -1 &&!([[model.content class] persistentFlag] &MessagePersistent_ISPERSISTED)) {
            return NO;
    }
    
    if (model.messageDirection == MessageDirection_RECEIVE) {
        model.isDisplayNickname = self.displayUserNameInCell;
    } else {
        model.isDisplayNickname = NO;
    }
    [self.conversationDataRepository addObject:model];
    return YES;
}

- (void)pushOldMessageModel:(RCMessageModel *)model {
    if (!([[model.content class] persistentFlag] &
          MessagePersistent_ISPERSISTED)) {
        return;
    }
    
    long ne_wId = model.messageId;
    for (RCMessageModel *__item in self.conversationDataRepository) {
        
        if (ne_wId == __item.messageId) {
            return;
        }
    }
    if (model.messageDirection == MessageDirection_RECEIVE) {
        model.isDisplayNickname = self.displayUserNameInCell;
    } else {
        model.isDisplayNickname = NO;
    }
    [self.conversationDataRepository insertObject:model atIndex:0];
}

- (void)loadLatestHistoryMessage {
    NSArray *__messageArray =
    [[RCIMClient sharedRCIMClient] getLatestMessages:self.conversationType
                                            targetId:self.targetId
                                               count:10];
    for (int i = 0; i < __messageArray.count; i++) {
        RCMessage *rcMsg = [__messageArray objectAtIndex:i];
        RCMessageModel *model = [[RCMessageModel alloc] initWithMessage:rcMsg];
        [self pushOldMessageModel:model];
    }
    
    [self figureOutAllConversationDataRepository];
    [self.conversationMessageCollectionView reloadData];
}

- (void)loadMoreHistoryMessage { // 加载更多历史信息
    long lastMessageId = -1;
    if (self.conversationDataRepository.count > 0) {
        RCMessageModel *model = [self.conversationDataRepository objectAtIndex:0];
        lastMessageId = model.messageId;
    }
    
    NSArray *__messageArray =
    [[RCIMClient sharedRCIMClient] getHistoryMessages:_conversationType
                                             targetId:_targetId
                                      oldestMessageId:lastMessageId
                                                count:10];
    for (int i = 0; i < __messageArray.count; i++) {
        RCMessage *rcMsg = [__messageArray objectAtIndex:i];
        RCMessageModel *model = [[RCMessageModel alloc] initWithMessage:rcMsg];
        [self pushOldMessageModel:model];
    }
    
    self.scrollNum++;
    if (self.scrollNum * 10 + 10> self.unReadMessage) {
        
        self.unReadMessage = 0;
    }
    [self figureOutAllConversationDataRepository];
    [self.conversationMessageCollectionView reloadData];
    
    if (_conversationDataRepository != nil &&
        _conversationDataRepository.count > 0 &&
        [self.conversationMessageCollectionView numberOfItemsInSection:0] >= __messageArray.count - 1) {
        NSIndexPath *indexPath =[NSIndexPath indexPathForRow:__messageArray.count - 1 inSection:0];
        [self.conversationMessageCollectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionTop animated:NO];
    }
}

//点击cell
- (void)didTapMessageCell:(RCMessageModel *)model {
    
    if (nil == model) {
        return;
    }
    RCMessageContent *_messageContent = model.content;
    
    if ([_messageContent isMemberOfClass:[RCImageMessage class]]) { // 点击了图片
        [self presentImagePreviewController:model];
    } else if ([_messageContent isMemberOfClass:[RCVoiceMessage class]]) {
        for (RCMessageModel *msg in self.conversationDataRepository) {
            if (model.messageId == msg.messageId) {
                msg.receivedStatus = ReceivedStatus_LISTENED;
                break;
            }
        }
    } else if ([_messageContent isMemberOfClass:[RCLocationMessage class]]) {
        // Show the location view controller
        RCLocationMessage *locationMessage = (RCLocationMessage *)(_messageContent);
        [self presentLocationViewController:locationMessage];
    }
}

- (void)presentLocationViewController:(RCLocationMessage *)locationMessageContent {
    //默认方法跳转
    RCLocationViewController *locationViewController =
    [[RCLocationViewController alloc] init];
    locationViewController.locationName = locationMessageContent.locationName;
    locationViewController.location = locationMessageContent.location;
    UINavigationController *navc = [[UINavigationController alloc]
                                    initWithRootViewController:locationViewController];
    if (self.navigationController) {
        //导航和原有的配色保持一直
        UIImage *image = [self.navigationController.navigationBar backgroundImageForBarMetrics:UIBarMetricsDefault];
        
        [navc.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
    }
    [self presentViewController:navc animated:YES completion:NULL];
}

- (void)didTapPhoneNumberInMessageCell:(NSString *)phoneNumber
                                 model:(RCMessageModel *)model {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
}

//长按消息内容
- (void)didLongTouchMessageCell:(RCMessageModel *)model inView:(UIView *)view {
    
    self.chatSessionInputBarControl.inputTextView.disableActionMenu = YES;
    self.longPressSelectedModel = model;
    
    [self becomeFirstResponder];
    CGRect rect = [self.view convertRect:view.frame fromView:view.superview];
    
    UIMenuController *menu = [UIMenuController sharedMenuController];
    UIMenuItem *copyItem = [[UIMenuItem alloc]initWithTitle:NSLocalizedString(@"Copy", @"") action:@selector(onCopyMessage:)];
    UIMenuItem *deleteItem = [[UIMenuItem alloc]initWithTitle:NSLocalizedString(@"Delete", @"") action:@selector(onDeleteMessage:)];
    if ([model.content isMemberOfClass:[RCTextMessage class]]) {
        
        [menu setMenuItems:[NSArray arrayWithObjects:copyItem, deleteItem, nil]];
    } else {
        [menu setMenuItems:@[ deleteItem ]];
    }
    [menu setTargetRect:rect inView:self.view];
    [menu setMenuVisible:YES animated:YES];
}

// can
- (BOOL)canBecomeFirstResponder {
    return YES;
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    return [super canPerformAction:action withSender:sender];
}

- (NSInteger)findDataIndexFromMessageList:(RCMessageModel *)model {
    NSInteger index = 0;
    for (int i = 0; i < self.conversationDataRepository.count; i++) {
        RCMessageModel *msg = (self.conversationDataRepository)[i];
        if (msg.messageId == model.messageId) {
            index = i;
            break;
        }
    }
    return index;
}

- (void)resendMessage:(RCMessageContent *)messageContent {
    if ([messageContent isMemberOfClass:RCImageMessage.class]) {
        RCImageMessage *imageMessage = (RCImageMessage *)messageContent;
        imageMessage.originalImage = [UIImage imageWithContentsOfFile:imageMessage.imageUrl];
        [self sendImageMessage:imageMessage pushContent:nil];
    } else {
        [self sendMessage:messageContent pushContent:nil];
    }
}

- (void)didTapmessageFailedStatusViewForResend:(RCMessageModel *)model { // 失败消息重发
    
    RCMessageContent *content = model.content;
    long msgId = model.messageId;
    NSIndexPath *indexPath =
    [NSIndexPath indexPathForItem:[self findDataIndexFromMessageList:model]
                        inSection:0];
    [[RCIMClient sharedRCIMClient] deleteMessages:@[ @(msgId) ]];
    [self.conversationDataRepository removeObject:model];
    [self.conversationMessageCollectionView
     deleteItemsAtIndexPaths:[NSArray arrayWithObject:indexPath]];
    
    self.isNeedScrollToButtom=YES;
    [self resendMessage:content];
}

//  打开大图。开发者可以重写，自己下载并且展示图片。默认使用内置controller
- (void)presentImagePreviewController:(RCMessageModel *)model;
{
    RCImagePreviewController *_imagePreviewVC =
    [[RCImagePreviewController alloc] init];
    _imagePreviewVC.messageModel = model;
    
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:_imagePreviewVC];
    
    if (self.navigationController) {
        //导航和原有的配色保持一直
        UIImage *image = [self.navigationController.navigationBar backgroundImageForBarMetrics:UIBarMetricsDefault];
        
        [nav.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
    }
    
    [self presentViewController:nav animated:YES completion:nil];
}

- (void)sendImageMessage:(RCImageMessage *)imageMessage pushContent:(NSString *)pushContent {
    
    if (_targetId == nil) {
        return;
    }
    __weak typeof(&*self) __weakself = self;
    
    if (imageMessage == nil) {
        return;
    }
    
    RCMessage *rcMessage = [[RCIMClient sharedRCIMClient]sendImageMessage:self.conversationType targetId:self.targetId content:imageMessage pushContent:pushContent progress:^(int progress, long messageId) {
        RCMessageCellNotificationModel *notifyModel = [[RCMessageCellNotificationModel alloc] init];
        notifyModel.actionName = CONVERSATION_CELL_STATUS_SEND_PROGRESS;
        notifyModel.messageId = messageId;
        notifyModel.progress = progress;
        dispatch_async(dispatch_get_main_queue(), ^{
            [[NSNotificationCenter defaultCenter]postNotificationName:KNotificationMessageBaseCellUpdateSendingStatus object:notifyModel];
        });
    } success:^(long messageId) {
        RCMessageCellNotificationModel *notifyModel = [[RCMessageCellNotificationModel alloc] init];
        notifyModel.actionName = CONVERSATION_CELL_STATUS_SEND_SUCCESS;
        notifyModel.messageId = messageId;
        dispatch_async(dispatch_get_main_queue(), ^{
            for (RCMessageModel *model in self.conversationDataRepository) {
                if (model.messageId == messageId) {
                    model.sentStatus = SentStatus_SENT;
                    break;
                }
            }
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)),dispatch_get_main_queue(), ^{
                [[NSNotificationCenter defaultCenter]postNotificationName:KNotificationMessageBaseCellUpdateSendingStatus object:notifyModel];
            });
        });
    } error:^(RCErrorCode errorCode, long messageId) {
        RCMessageCellNotificationModel *notifyModel =[[RCMessageCellNotificationModel alloc] init];
        notifyModel.actionName = CONVERSATION_CELL_STATUS_SEND_FAILED;
        notifyModel.messageId = messageId;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, NSEC_PER_SEC * 0.3f),dispatch_get_main_queue(), ^{
            for (RCMessageModel *model in self.conversationDataRepository) {
                if (model.messageId == messageId) {
                    model.sentStatus = SentStatus_FAILED;
                    break;
                }
            }
            [[NSNotificationCenter defaultCenter]postNotificationName: KNotificationMessageBaseCellUpdateSendingStatus object:notifyModel];
        });
    }];
    
    if ([rcMessage.content isKindOfClass:[RCImageMessage class]]) {
        RCImageMessage *img = (RCImageMessage *)rcMessage.content;
        img.originalImage = nil;
    }
    self.tempMessage = rcMessage;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [__weakself appendAndDisplayMessage:__weakself.tempMessage];
    });
}

#pragma mark ---------------------------------------- 发送消息中改写数据包内容拼接 ----------------------------------------
- (void)sendMessage:(RCMessageContent *)messageContent pushContent:(NSString *)pushContent {
    
    if (_targetId == nil) {
        return;
    }
    __weak typeof(&*self) __weakself = self;
    
    if (messageContent == nil) {
        return;
    }
    
    RCTextMessage *msg = (RCTextMessage *)messageContent;
    NSLog(@"%@, %@", msg.extra, msg.content);
#warning ---------------------------------------- 改写messageContent数据格式（拼接） ----------------------------------------
    RCMessage *rcMessage = [[RCIMClient sharedRCIMClient] sendMessage:self.conversationType targetId:self.targetId content:messageContent pushContent:pushContent success:^(long messageId) {
        RCMessageCellNotificationModel *notifyModel = [[RCMessageCellNotificationModel alloc] init];
        notifyModel.actionName = CONVERSATION_CELL_STATUS_SEND_SUCCESS;
        notifyModel.messageId = messageId;
        dispatch_async(dispatch_get_main_queue(), ^{
            for (RCMessageModel *model in self.conversationDataRepository) {
                if (model.messageId == messageId) {
                    model.sentStatus = SentStatus_SENT;
                    break;
                }
            }
            [[NSNotificationCenter defaultCenter] postNotificationName:KNotificationMessageBaseCellUpdateSendingStatus object:notifyModel];
        });
    } error:^(RCErrorCode nErrorCode, long messageId) {
        RCMessageCellNotificationModel *notifyModel = [[RCMessageCellNotificationModel alloc] init];
        notifyModel.actionName = CONVERSATION_CELL_STATUS_SEND_FAILED;
        notifyModel.messageId = messageId;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, NSEC_PER_SEC * 0.1f),dispatch_get_main_queue(), ^{
            for (RCMessageModel *model in self.conversationDataRepository) {
                if (model.messageId == messageId) {
                    model.sentStatus = SentStatus_FAILED;
                    break;
                }
            }
            [[NSNotificationCenter defaultCenter]postNotificationName:KNotificationMessageBaseCellUpdateSendingStatus object:notifyModel];
        });
        
        RCInformationNotificationMessage *informationNotifiMsg = nil;
        if (NOT_IN_DISCUSSION == nErrorCode) {
            informationNotifiMsg = [RCInformationNotificationMessage notificationWithMessage:NSLocalizedString(@"NOT_IN_DISCUSSION", @"") extra:nil];
        } else if (NOT_IN_GROUP == nErrorCode) {
            informationNotifiMsg = [RCInformationNotificationMessage notificationWithMessage:NSLocalizedString(@"NOT_IN_GROUP", @"") extra:nil];
        } else if (NOT_IN_CHATROOM == nErrorCode) {
            informationNotifiMsg = [RCInformationNotificationMessage notificationWithMessage:NSLocalizedString(@"NOT_IN_CHATROOM", @"") extra:nil];
        } else if (REJECTED_BY_BLACKLIST == nErrorCode) {
            informationNotifiMsg = [RCInformationNotificationMessage notificationWithMessage:NSLocalizedString(@"Message rejected", @"") extra:nil];
        }
        if (nil != informationNotifiMsg) {
            RCMessage *tempMessage = [[RCIMClient sharedRCIMClient] insertMessage:self.conversationType targetId:self.targetId senderUserId:nil sendStatus:SentStatus_SENT content:informationNotifiMsg];
            dispatch_async(dispatch_get_main_queue(), ^{
                [__weakself appendAndDisplayMessage:tempMessage];
            });
        }
    }];
    
    self.tempMessage = rcMessage;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self.tempMessage) {
            [__weakself appendAndDisplayMessage:__weakself.tempMessage];
        }
    });
}

#warning  这里拿到通知信息, 最早的信息入口
- (void)didReceiveMessageNotification:(NSNotification *)notification {
    
    _receiveMessageFlag++;
    
    if (_receiveMessageFlag%2) { // 这里信息重复, 过滤掉一次,为什么重复?暂时未知
        return;
    }
    
    RCMessage *rcMessage = notification.object;
    RCMessageModel *model = [[RCMessageModel alloc] initWithMessage:rcMessage];
    if (model.conversationType == self.conversationType &&
        [model.targetId isEqual:self.targetId]) {
        
        if (self.isConversationAppear) {
            [[RCIMClient sharedRCIMClient]
             clearMessagesUnreadStatus:self.conversationType
             targetId:self.targetId];
        }
        __weak typeof(&*self) __blockSelf = self;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [__blockSelf appendAndDisplayMessage:rcMessage];
            
            UIMenuController *menu = [UIMenuController sharedMenuController];
            menu.menuVisible=NO;
            
            // 是否显示右下未读消息数
            if (self.enableNewComingMessageIcon == YES) {
                if (![self isAtTheBottomOfTableView]) {
                    self.unreadNewMsgCount ++ ;
                    [self updateUnreadMsgCountLabel];
                }
            }
        });
    }
}

- (void)chatSessionInputBarControlContentSizeChanged:(CGRect)frame;
{
    [_chatSessionInputBarControl updateConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(frame.size.height);
    }];
    
    [_conversationMessageCollectionView updateConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(_chatSessionInputBarControl.top);
    }];
    
    [self scrollToBottomAnimated:YES];
}

- (void)keyboardWillHide {
    
    if (self.chatSessionInputBarControl.inputTextView
        .isFirstResponder) //判断键盘打开
    {
        [self.chatSessionInputBarControl.inputTextView resignFirstResponder];
    }
    [self layoutBottomBarWithStatus:KBottomBarDefaultStatus];
}

- (void)didTouchSugButton:(UIButton *)sender { // 点击sug键盘入口按钮
    
    if (_sugView.y == self.view.h) {
        
        [MobClick endEvent:@"chatroom_time" label:@"点击sug键盘"];
        
        [MobClick beginEvent:@"intelligentView_time" label:@"返回"];
        [MobClick beginEvent:@"intelligentView_time" label:[RCIM sharedRCIM].inteligentMessages[0]];
        [MobClick beginEvent:@"intelligentView_time" label:[RCIM sharedRCIM].inteligentMessages[1]];
        [MobClick beginEvent:@"intelligentView_time" label:[RCIM sharedRCIM].inteligentMessages[2]];
        [MobClick beginEvent:@"intelligentView_time" label:[RCIM sharedRCIM].inteligentMessages[0]];
        
        if (_chatSessionInputBarControl.y == self.view.h - Height_ChatSessionInputBar) {
            if (_currentBottomBarStatus != KBottomBarSugStatus) {
                [self layoutBottomBarWithStatus:KBottomBarSugStatus];
            }
        } else {
            if (_currentBottomBarStatus != KBottomBarSugStatus) {
                [self animationLayoutBottomBarWithStatus:KBottomBarSugStatus];
            }
        }

    } else { // 招出键盘, 注销sug
        [_chatSessionInputBarControl.inputTextView becomeFirstResponder];
    }
}

- (void)didTouchEmojiButton:(UIButton *)sender {
    if (_currentBottomBarStatus != KBottomBarEmojiStatus) {
        [self animationLayoutBottomBarWithStatus:KBottomBarEmojiStatus];
    }
}

- (void)didTouchTimeSugButton:(UIButton *)sender {
    if (_currentBottomBarStatus != KBottomBarTimeSugStatus) {
        [self animationLayoutBottomBarWithStatus:KBottomBarTimeSugStatus];
    }
}

- (void)didTouchPictureButton:(UIButton *)sender { // 打开相册
    
    __block RCAlbumListViewController *albumListVC = [[RCAlbumListViewController alloc] init];
    albumListVC.delegate = self;
    UINavigationController *rootVC = [[UINavigationController alloc] initWithRootViewController:albumListVC];
    __weak typeof(&*self) weakself = self;
    
    RCAssetHelper *sharedAssetHelper = [RCAssetHelper shareAssetHelper];
    [sharedAssetHelper getGroupsWithALAssetsGroupType:ALAssetsGroupAll resultCompletion:^(ALAssetsGroup *assetGroup) {
        if (nil != assetGroup) {
            [albumListVC.libraryList insertObject:assetGroup atIndex:0];
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakself.navigationController presentViewController:rootVC animated:YES completion:^{}];
            });
        }
    }];
}

- (void)didTouchCameraButton:(UIButton *)sender { // 打开相机
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
#if TARGET_IPHONE_SIMULATOR
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
#else
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
#endif
    self.currentPicker = picker;
    self.currentPicker.delegate = self;
    [self presentViewController:picker animated:YES completion:nil];
}

- (void)didTouchLocationButton:(UIButton *)sender {
    
    RCLocationPickerViewController *picker =[[RCLocationPickerViewController alloc] init];
    picker.delegate = self;
    // 数据源为内部默认数据源
    [self.navigationController pushViewController:picker animated:YES];
}

// 语音
- (void)didTouchRecordButon:(UIButton *)sender event:(UIControlEvents)event {
    
    switch (event) {
        case UIControlEventTouchDown:
            if (_isRunning) {
                [_engine stop];
                _isRunning = NO;
            } else {
                _isRunning = YES;
                
                //        recordButton.title = @"Stop";
                
                if (_params == nil) {
                    _params = [[DialogParams alloc] init];
                }
                [_params setDlgType:[NSNumber numberWithInt:TYPE_AUDIO]];
                [_params setReqRes:@"robot"] ;//如果有进行资源定制,请设置对应的资源,否则为默认robot
                [_params setUseSex:[NSNumber numberWithInt:1]] ;
                
                [_engine start:_params];
            }
            break;
        case UIControlEventTouchUpInside:
            if (_isRunning) {
                [_engine stop];
                _isRunning = NO;
            }
            break;
        default:
            break;
    }
}

//语音消息开始录音
- (void)onBeginRecordEvent {
    self.voiceCaptureControl = [[RCVoiceCaptureControl alloc]
                                initWithFrame:CGRectMake(0, 20, [UIScreen mainScreen].bounds.size.width,
                                                         [UIScreen mainScreen].bounds.size.height)];
    self.voiceCaptureControl.delegate = self;
    [self.voiceCaptureControl startRecord];
}

//语音消息录音结束
- (void)onEndRecordEvent {
    NSData *recordData = [self.voiceCaptureControl stopRecord];
    if (self.voiceCaptureControl.duration > 1.0f && nil != recordData) {
        [self destoryVoiceCaptureControl];
        RCVoiceMessage *voiceMessage =
        [RCVoiceMessage messageWithAudio:recordData
                                duration:self.voiceCaptureControl.duration];
        [self sendMessage:voiceMessage pushContent:nil];
        
    } else {
        // message too short
        if (!self.isAudioRecoderTimeOut) {
            [self.voiceCaptureControl showMsgShortView];
            [self performSelector:@selector(destoryVoiceCaptureControl)
                       withObject:nil
                       afterDelay:1.0f];
        }
    }
}

- (void)destoryVoiceCaptureControl {
    [self.voiceCaptureControl removeFromSuperview];
    self.isAudioRecoderTimeOut = NO;
}
- (void)dragEnterRecordEvent {
    [self.voiceCaptureControl hideCancelView];
}
//滑出显示
- (void)dragExitRecordEvent {
    [self.voiceCaptureControl showCancelView];
}
- (void)onCancelRecordEvent {
    [self.voiceCaptureControl cancelRecord];
}

#pragma mark <RCVoiceCaptureControlDelegate>
- (void)RCVoiceCaptureControlTimeout:(BOOL)timeout
                        withDuration:(double)duration
                         withWavData:(NSData *)data {
    self.isAudioRecoderTimeOut = YES;
    [self destoryVoiceCaptureControl];
    RCVoiceMessage *voiceMessage =
    [RCVoiceMessage messageWithAudio:data duration:duration];
    [self sendMessage:voiceMessage pushContent:nil];
}

#pragma mark - RCAlbumListViewControllerDelegate
- (void)albumListViewController:(RCAlbumListViewController *)albumListViewController selectedImages:(NSArray *)selectedImages { // 选完图片
    
    _isTakeNewPhoto = NO;
    //耗时操作异步执行，以免阻塞主线程
    __weak SDJChatRoomController *weakSelf = self;
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        for (int i = 0; i < selectedImages.count; i++) {
            UIImage *image = [selectedImages objectAtIndex:i];
            RCImageMessage *imagemsg = [RCImageMessage messageWithImage:image];
            [weakSelf sendImageMessage:imagemsg pushContent:nil];
            [NSThread sleepForTimeInterval:0.5];
        }
    });
}

#pragma mark - UIImagePickerControllerDelegate method
//选择相册图片或者拍照回调
- (void)imagePickerController:(UIImagePickerController *)picker
        didFinishPickingImage:(UIImage *)image
                  editingInfo:(NSDictionary *)editingInfo {
    [picker dismissViewControllerAnimated:YES completion:nil];
    RCImageMessage *imageMessage = [RCImageMessage messageWithImage:image];
    
    _isTakeNewPhoto = YES;
    
    [self sendImageMessage:imageMessage pushContent:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark RCLocationPickerViewControllerDelegate
// 选取位置后回调
- (void)locationPicker:(RCLocationPickerViewController *)locationPicker didSelectLocation:(CLLocationCoordinate2D)location locationName:(NSString *)locationName mapScreenShot:(UIImage *)mapScreenShot {
    RCLocationMessage *locationMessage =[RCLocationMessage messageWithLocationImage:mapScreenShot location:location locationName:locationName];
    [self sendMessage:locationMessage pushContent:nil];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark <RCEmojiBoardViewDelegate>
- (void)didTouchEmojiView:(RCEmojiBoardView *)emojiView
             touchedEmoji:(NSString *)string {
    NSString *replaceString = self.chatSessionInputBarControl.inputTextView.text;
    
    if (nil == string) {
        [self.chatSessionInputBarControl.inputTextView deleteBackward];
    } else {
        
        replaceString = string;
        if (replaceString.length < 5000) {
            self.chatSessionInputBarControl.inputTextView.text =
            [self.chatSessionInputBarControl.inputTextView.text
             stringByAppendingString:replaceString];
            {
                CGFloat _inputTextview_height = 36.0f;
                if (_chatSessionInputBarControl.inputTextView.contentSize.height < 70 &&
                    _chatSessionInputBarControl.inputTextView.contentSize.height >
                    36.0f) {
                    _inputTextview_height =
                    _chatSessionInputBarControl.inputTextView.contentSize.height;
                }
                if (_chatSessionInputBarControl.inputTextView.contentSize.height >=
                    70) {
                    _inputTextview_height = 70;
                }
                CGRect intputTextRect = _chatSessionInputBarControl.inputTextView.frame;
                intputTextRect.size.height = _inputTextview_height;
                intputTextRect.origin.y = 7;
                [_chatSessionInputBarControl.inputTextView setFrame:intputTextRect];
                _chatSessionInputBarControl.inputTextview_height =
                _inputTextview_height;
                
                CGRect vRect = _chatSessionInputBarControl.frame;
                vRect.size.height =
                Height_ChatSessionInputBar + (_inputTextview_height - 36);
                vRect.origin.y = _chatSessionInputBarControl.originalPositionY -
                (_inputTextview_height - 36);
                _chatSessionInputBarControl.currentPositionY = vRect.origin.y;
                [self.chatSessionInputBarControl changeSize:intputTextRect];
                [self chatSessionInputBarControlContentSizeChanged:vRect];
            }
        }
    }
    
    UITextView *textView = self.chatSessionInputBarControl.inputTextView;
    {
        CGRect line = [textView caretRectForPosition:textView.selectedTextRange.start];
        CGFloat overflow = line.origin.y + line.size.height - (textView.contentOffset.y + textView.bounds.size.height -
         textView.contentInset.bottom - textView.contentInset.top);
        if (overflow > 0) {
            CGPoint offset = textView.contentOffset;
            offset.y += overflow + 7; // leave 7 pixels margin
            [UIView animateWithDuration:.2
                             animations:^{
                                 [textView setContentOffset:offset];
                             }];
        }
    }
}

- (void)didSendButtonEvent:(RCEmojiBoardView *)emojiView sendButton:(UIButton *)sendButton {
    
    NSString *_sendText = self.chatSessionInputBarControl.inputTextView.text;
    
    NSString *_formatString = [_sendText
                               stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if (0 == [_formatString length]) {
        UIAlertView *notAllowSendSpace = [[UIAlertView alloc]
                                          initWithTitle:nil
                                          message:NSLocalizedString(@"whiteSpaceMessage", @"")
                                          delegate:self
                                          cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                          otherButtonTitles:nil, nil];
        [notAllowSendSpace show];
        return;
    }
    
    RCTextMessage *rcTextMessage = [RCTextMessage
                                    messageWithContent:self.chatSessionInputBarControl.inputTextView.text];
    
    
    
    [self sendMessage:rcTextMessage pushContent:nil];
    
    self.chatSessionInputBarControl.inputTextView.text = @"";
    {
        CGFloat _inputTextview_height = 36.0f;
        CGRect intputTextRect = _chatSessionInputBarControl.inputTextView.frame;
        intputTextRect.size.height = _inputTextview_height;
        intputTextRect.origin.y = 7;
        [_chatSessionInputBarControl.inputTextView setFrame:intputTextRect];
        
        CGRect vRect = _chatSessionInputBarControl.frame;
        vRect.size.height = Height_ChatSessionInputBar + (_inputTextview_height - 36);
        vRect.origin.y = _chatSessionInputBarControl.originalPositionY - (_inputTextview_height - 36);
        _chatSessionInputBarControl.currentPositionY = vRect.origin.y;
        [self.chatSessionInputBarControl changeSize:intputTextRect];
        [self chatSessionInputBarControlContentSizeChanged:vRect];
    }
}

#pragma mark - new sendBtn
- (void)didTouchSendButton:(UIButton *)sender {
    
    [MobClick endEvent:@"chatroom_time" label:@"点击发送按钮"];
    
    // 试试直接发
    RCTextMessage *rcTextMessage = [RCTextMessage messageWithContent:self.chatSessionInputBarControl.inputTextView.text];
    
    [self sendMessage:rcTextMessage pushContent:nil];
    self.chatSessionInputBarControl.inputTextView.text= nil;
}

- (void)didTouchKeyboardReturnKey:(RCChatSessionInputBarControl *)inputControl text:(NSString *)text {
    RCTextMessage *rcTextMessage = [RCTextMessage messageWithContent:text];
    
#pragma mark _extra
    NSTimeInterval time=(long)([[NSDate date] timeIntervalSince1970]*1000);
    NSDictionary *dict = @{@"msg_detail":@{@"content":@"aaa"},@"inner_uid":@42699,@"msg_type":@1,@"src_uid":@"osK-xjtrBuVH0RuyPYsTLSqduQQw",@"user_gid":@"rc_group_42699",@"user_src":@1,@"msg_ts":@(time),@"direction":@1,@"genius_info":@{@"username":@"Genius003",@"realname":@"安波"}};
    
    SBJson4Writer *writer = [[SBJson4Writer alloc] init];
    NSString *jasonString = [writer stringWithObject:dict];
    rcTextMessage.extra = jasonString;
    
    
//    NSLog(@"%@", jasonString);
//    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:NSJSONWritingPrettyPrinted error:nil];
//    rcTextMessage.extra = {"msg_detail":{"content":"aaa"},"inner_uid":42699,"msg_type":1,"src_uid":"osK-xjtrBuVH0RuyPYsTLSqduQQw","user_gid":"rc_group_42699","user_src":1,"msg_ts":1450146085717,"direction":1,"genius_info":{"username":"Genius003","realname":"安波"}};
    rcTextMessage.content = @"text";
    [self sendMessage:rcTextMessage pushContent:nil];
    
}

- (void)didTapUrlInMessageCell:(NSString *)url model:(RCMessageModel *)model {
    
    // 微信链接替换
    NSString *str1 = @"http://m.laiye.com/m/index.php?";
    
    NSRange range1 = [url rangeOfString:str1];
    
    if (range1.length) {
        
        NSString *str2 = @"http://m.laiye.com/m-hybrid/_index.html";
        
        NSString *str3 = [url substringFromIndex:range1.length];
        
        url = [str2 stringByAppendingString:str3];
    }
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
}

- (void)didTouchCollectionCellAtIndex:(int)index WithModel:(SDJCard_Items *)model {
    
    SDJNormalWebController *vc = [[SDJNormalWebController alloc] init];
    
    vc.urlString = model.Url;
    
    vc.title = model.UrlTip;
    
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)didTouchUrl:(NSString *)urlString  {
    
    urlString = [urlString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    // http://
    NSString *httpIndex = [urlString substringToIndex:7];
    if (![httpIndex isEqualToString:@"http://"] && ![httpIndex isEqualToString:@"https:/"]) {
        urlString = [@"http://" stringByAppendingString:urlString];
    }
    
    NSString *str1 = @"http://m.laiye.com/m/index.php?";
    
    NSRange range1 = [urlString rangeOfString:str1];
    
    if (range1.length) {
        
        NSString *str2 = @"http://m.laiye.com/m-hybrid/_index.html";
        
        NSString *str3 = [urlString substringFromIndex:range1.length];
        
        urlString = [str2 stringByAppendingString:str3];
    }
    
    NSString *str = @"m.laiye.com";
    
    NSRange range = [urlString rangeOfString:str];
    
    if (range.length) {
        
        SDJOriginalWebViewController *vc = [[SDJOriginalWebViewController alloc] init];
        vc.urlString = urlString;
        [self.navigationController pushViewController:vc animated:YES];
        
    } else {
        
        SDJNormalWebController *vc = [[SDJNormalWebController alloc] init];
        vc.urlString = urlString;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

// 新增智能键盘
#pragma mark <SDJIntelligentBoardViewDelegate>
- (void)didSelectedIntelligentBoardView:(SDJIntelligentBoardView *)intelligentView textSelected:(NSString *)text withTag:(int)tag {
    
    [MobClick endEvent:@"intelligentView_time" label:text];

    _chatSessionInputBarControl.inputTextView.text = text;
    
    if (text.length !=0) {
        _chatSessionInputBarControl.sendButton.enabled = YES;
    }
}

// 点击未读信息滚动到最底部
- (void)tabRightBottomMsgCountIcon:(UIGestureRecognizer *)gesture {
    if (gesture.state == UIGestureRecognizerStateEnded) {
        
        [self scrollToBottomAnimated:YES];
    }
}

- (void)tap4ResetDefaultBottomBarStatus:
(UIGestureRecognizer *)gestureRecognizer {
    if (gestureRecognizer.state == UIGestureRecognizerStateEnded) {
        if (_currentBottomBarStatus != KBottomBarDefaultStatus) {
            [self animationLayoutBottomBarWithStatus:KBottomBarDefaultStatus];
        }
    }
}

- (void)animationLayoutBottomBarWithStatus:(KBottomBarStatus)bottomBarStatus { // 键盘收缩动画
    
    [UIView beginAnimations:@"Move_bar" context:nil];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
    [UIView setAnimationDuration:0.2f];
    [UIView setAnimationDelegate:self];
    [self layoutBottomBarWithStatus:bottomBarStatus];
    
    [UIView commitAnimations];
}

- (void)layoutBottomBarWithStatus:(KBottomBarStatus)bottomBarStatus { // 键盘高度切换
    
    if (bottomBarStatus != KBottomBarKeyboardStatus) {
        if (self.chatSessionInputBarControl.inputTextView.isFirstResponder) {
            [self.chatSessionInputBarControl.inputTextView resignFirstResponder];
        }
    }
    
    if (bottomBarStatus != KBottomBarVoiceStatus) {
        if (!_chatSessionInputBarControl.recordButton.hidden) {
            _chatSessionInputBarControl.recordButton.hidden = YES;
        }
    }
    
    if (_chatSessionInputBarControl.sendButton.enabled) {
        _chatSessionInputBarControl.sendButton.enabled = NO;
        _chatSessionInputBarControl.inputTextView.text = @"";
    }
    
    switch (bottomBarStatus) {
            
        case KBottomBarDefaultStatus: {
            
            [MobClick event:@"intelligentView_click" label:@"返回"];
            [MobClick endEvent:@"intelligentView_time" label:@"返回"];
            
            [_chatControlBottomConstraint uninstall];
            [_chatSessionInputBarControl updateConstraints:^(MASConstraintMaker *make) {
                _chatControlBottomConstraint = make.bottom.equalTo(self.view);
            }];
            
            [_sugView updateConstraints:^(MASConstraintMaker *make) {
                make.bottom.equalTo(self.view).offset(Height_IntelBoardView);
            }];
            
            [_emojiView updateConstraints:^(MASConstraintMaker *make) {
                make.bottom.equalTo(self.view).offset(Height_EmojiBoardView);
            }];
            
            [_timeSugView updateConstraints:^(MASConstraintMaker *make) {
                make.bottom.equalTo(self.view).offset(Height_TimeBoardView);
            }];
            
        } break;
            
        case KBottomBarKeyboardStatus: {
            
            [MobClick endEvent:@"chatroom_time" label:@"点击输入框"];
            
        } break;
            
        case KBottomBarSugStatus: {
            
            [_sugView updateConstraints:^(MASConstraintMaker *make) {
                make.bottom.equalTo(self.view);
            }];
            
            [_emojiView updateConstraints:^(MASConstraintMaker *make) {
                make.bottom.equalTo(self.view).offset(Height_EmojiBoardView);
            }];
            
            [_timeSugView updateConstraints:^(MASConstraintMaker *make) {
                make.bottom.equalTo(self.view).offset(Height_TimeBoardView);
            }];
            
            [_chatControlBottomConstraint uninstall];
            [_chatSessionInputBarControl updateConstraints:^(MASConstraintMaker *make) {
                _chatControlBottomConstraint = make.bottom.equalTo(_sugView.top);
            }];
            
        } break;
            
        case KBottomBarEmojiStatus: {
            
            [_emojiView updateConstraints:^(MASConstraintMaker *make) {
                make.bottom.equalTo(self.view);
            }];
            
            [_timeSugView updateConstraints:^(MASConstraintMaker *make) {
                make.bottom.equalTo(self.view).offset(Height_TimeBoardView);
            }];
            
            [_sugView updateConstraints:^(MASConstraintMaker *make) {
                make.bottom.equalTo(self.view).offset(Height_IntelBoardView);
            }];
            
            [_chatControlBottomConstraint uninstall];
            [_chatSessionInputBarControl updateConstraints:^(MASConstraintMaker *make) {
                _chatControlBottomConstraint = make.bottom.equalTo(_emojiView.top);
            }];
            
        } break;
            
        case KBottomBarTimeSugStatus: {
            
            [_emojiView updateConstraints:^(MASConstraintMaker *make) {
                make.bottom.equalTo(self.view).offset(Height_EmojiBoardView);
            }];
            
            [_timeSugView updateConstraints:^(MASConstraintMaker *make) {
                make.bottom.equalTo(self.view);
            }];
            
            [_sugView updateConstraints:^(MASConstraintMaker *make) {
                make.bottom.equalTo(self.view).offset(Height_IntelBoardView);
            }];
            
            [_chatControlBottomConstraint uninstall];
            [_chatSessionInputBarControl updateConstraints:^(MASConstraintMaker *make) {
                _chatControlBottomConstraint = make.bottom.equalTo(_timeSugView.top);
            }];
            
        } break;
            
        default:
            break;
    }
    
    [_conversationMessageCollectionView updateConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(_chatSessionInputBarControl.top);
    }];
    
    _currentBottomBarStatus = bottomBarStatus;
    
    [self.view layoutIfNeeded];
    [UIView animateWithDuration:0.3 animations:^{
        [self scrollToBottomAnimated:NO];
    }];
}

#pragma mark <RCChatSessionInputBarControlDelegate>
- (void)keyboardWillShowWithFrame:(CGRect)keyboardFrame {
    
    if (keyboardFrame.size.height != 0) {
        
        [_chatControlBottomConstraint uninstall];
        [_chatSessionInputBarControl updateConstraints:^(MASConstraintMaker *make) {
            _chatControlBottomConstraint = make.bottom.equalTo(- keyboardFrame.size.height);
        }];
        
        [_sugView updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self.view).offset(Height_IntelBoardView);
        }];
        
        [_emojiView updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self.view).offset(Height_EmojiBoardView);
        }];
        
        [_conversationMessageCollectionView updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(_chatSessionInputBarControl.top);
        }];
        
        _currentBottomBarStatus = KBottomBarKeyboardStatus;
        
        [UIView animateWithDuration:0.7 animations:^{
            [self.view layoutIfNeeded];
            [self scrollToBottomAnimated:YES];
        }];
    }
}

- (CGSize)sizeForItem:(UICollectionView *)collectionView //信息尺寸自定义
          atIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat __width = CGRectGetWidth(collectionView.frame);
    
    RCMessageModel *model =
    [self.conversationDataRepository objectAtIndex:indexPath.row];
    RCMessageContent *messageContent = model.content;
    
    model.isDisplayNickname = NO; // 不显示名字
    
    if (indexPath.row == 0) {
        model.isDisplayMessageTime = NO;
    }
    
    CGFloat __height = 0.0f;
    
    if ([messageContent isMemberOfClass:[RCTextMessage class]]) {
        
        RCTextMessage *_textMessage = (RCTextMessage *)messageContent;
        
        NSString *text = _textMessage.content;
        
        NSString *pattern = @"<a href=\'(.*?)\'>(.*?)</a>";
        
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern options:0 error:nil];
        
        NSArray *matches = [regex matchesInString:text options:0 range:NSMakeRange(0, text.length)]; // 网址
        
        NSUInteger lastIdx = 0;
        
        NSMutableString *attributedString = [NSMutableString stringWithCapacity:0]; // 总字符串
        
        NSMutableString *realTextString = [NSMutableString stringWithCapacity:0];
        
        if (matches.count) { // 解析部分bug, 1.文字需要提出来, 2. 位置需要调整 matchs.count
            
            NSMutableArray *urlArrs = [[NSMutableArray alloc] init]; // 存url
            int i = 0;
            
            for (NSTextCheckingResult* match in matches)
            {
                NSRange range = match.range;
                
                if (range.location > lastIdx)
                {
                    NSString *temp = [text substringWithRange:NSMakeRange(lastIdx, range.location - lastIdx)];
                    
                    [attributedString appendString:temp];// 记录头
                    [realTextString appendString:temp];
                }
                
                NSString *title = [text substringWithRange:[match rangeAtIndex:2]]; // 标题
                NSString *url = [text substringWithRange:[match rangeAtIndex:1]]; // 网址
                
                lastIdx = range.location + range.length;
                [attributedString appendFormat:@"<%d>%@</%d>",i,title,i];
                [realTextString appendString:title];
                [urlArrs addObject:url];
                
                i++;
            }
            
            if (lastIdx < text.length)
            {
                NSString  *temp = [text substringFromIndex:lastIdx];
                
                [attributedString appendString:temp];// 记录尾
                [realTextString appendString:temp];
            }
            
        } else {
            
            [realTextString appendString:_textMessage.content];
        }
        
        CGSize __textSize = CGSizeZero;
        if (IOS_FSystenVersion < 7.0) {
            __textSize = RC_MULTILINE_TEXTSIZE_LIOS7(realTextString, [UIFont systemFontOfSize:Text_Message_Font_Size], CGSizeMake(__width -
                                                                                                                                  (10 + [RCIM sharedRCIM].globalMessagePortraitSize.width + 5) * 2 - 5 -
                                                                                                                                  20 * 2,
                                                                                                                                  8000), NSLineBreakByTruncatingTail);
        }else {
            __textSize = RC_MULTILINE_TEXTSIZE_GEIOS7(realTextString, [UIFont systemFontOfSize:Text_Message_Font_Size], CGSizeMake(__width -
                                                                                                                                   (10 + [RCIM sharedRCIM].globalMessagePortraitSize.width + 5) * 2 - 5 -
                                                                                                                                   20 * 2,
                                                                                                                                   8000));
        }
        __textSize = CGSizeMake(ceilf(__textSize.width), ceilf(__textSize.height));
//        float maxWidth=__width -(5 + [RCIM sharedRCIM].globalMessagePortraitSize.width + 10) * 2 - 5 -20 * 2;
//        if(__textSize.width>maxWidth)
//        {
//            __textSize.width=maxWidth;
//        }
        CGSize __labelSize = CGSizeMake(__textSize.width, __textSize.height);
        CGFloat __bubbleHeight = __labelSize.height + 12 + 12 < 46 ? 46 : (__labelSize.height + 12 + 12);
        
        switch (_testCardflag) {
            case 0:
                __height = __bubbleHeight;
                break;
            case 1:
                __height = CARDONE_HEIGHT;
                break;
            case 2:
                __height = CARDTWO_HEIGHT;
                break;
            case 3:
                __height = CARDTHREE_HEIGHT * 3;
                break;
            case 4:
                __height = CARDFOUR_HEIGHT;
                break;
            case 5:
                __height = CARDFIVE_HEIGHT;
                break;
            default:
                break;
        }
    } else if ([messageContent isMemberOfClass:[RCLocationMessage class]]) {

        __height = 140;
    } else if ([messageContent isMemberOfClass:[RCVoiceMessage class]]) {
        
        __height = 46;

    } else if ([messageContent isMemberOfClass:[RCRichContentMessage class]]) {
        
        RCRichContentMessage *message = (RCRichContentMessage *)messageContent;
        
        int title = [message.title intValue];
        
        switch (title) {
            case 6:
                __height = CARDONE_HEIGHT;
                break;
            case 7:
                __height = CARDTWO_HEIGHT;
                break;
            case 8:
                __height = CARDFIVE_HEIGHT;
                break;
            default:
                break;
        }
        
    } else if ([messageContent isMemberOfClass:[RCImageMessage class]]) {
        RCImageMessage *_imageMessage = (RCImageMessage *)messageContent;
        
        CGSize imageSize = _imageMessage.thumbnailImage.size;
        //兼容240
        CGFloat imageWidth = 120;
        CGFloat imageHeight = 120;
        
        CGFloat imageWidthMin = 60;
        CGFloat imageHeightMin = 60;
        CGFloat imageWidthMax = [UIScreen mainScreen].bounds.size.width * 0.8;
        CGFloat imageHeightMax = [UIScreen mainScreen].bounds.size.height * 0.6;
        CGFloat scale = imageWidthMax * 1.0 / imageHeightMax;
        if (imageSize.width < imageWidthMin && imageSize.height < imageHeightMin) {
            if (imageSize.width >= imageSize.height) {
                if (imageSize.width * 1.0 / imageSize.height >= imageWidthMax * 1.0 / imageHeightMin) {
                    imageWidth = imageWidthMax;
                    imageHeight = imageSize.height *imageWidthMax * 1.0 / imageSize.width;
                } else {
                    imageHeight = imageHeightMin;
                    imageWidth = imageSize.width * imageHeightMin * 1.0 / imageSize.height;
                }
            } else {
                if (imageSize.height * 1.0 / imageSize.width >= imageHeightMax * 1.0 / imageWidthMin) {
                    imageHeight = imageHeightMax;
                    imageWidth = imageSize.width *imageHeightMax * 1.0 / imageSize.height;
                } else {
                    imageWidth = imageWidthMin;
                    imageHeight = imageSize.height * imageWidthMin * 1.0 / imageSize.width;
                }
            }
        } else if (imageSize.width > imageWidthMax || imageSize.height > imageHeightMax) {
            if (imageSize.width >= imageSize.height * scale) {
                imageWidth = imageWidthMax;
                imageHeight = imageSize.height *imageWidthMax * 1.0 / imageSize.width;
            } else {
                imageHeight = imageHeightMax;
                imageWidth = imageSize.width * imageHeightMax * 1.0 / imageSize.height;
            }
        } else {
            imageWidth = imageSize.width;
            imageHeight = imageSize.height;
        }
        
        //图片half
        imageSize = CGSizeMake(imageWidth, imageHeight);
        __height = imageSize.height;
        
    }
    
    if (__height < [RCIM sharedRCIM].globalMessagePortraitSize.height) {
        __height = [RCIM sharedRCIM].globalMessagePortraitSize.height;
    }
    
    //上边距
    __height = __height + 10;
    
    if (model.isDisplayMessageTime) {
        __height = __height + 20 + 10;
    }
    //下边距
    __height = __height + 10;
    
    return CGSizeMake(__width, __height);
}

// 复制
- (void)onCopyMessage:(id)sender { // 只能复制文本信息
    
    self.chatSessionInputBarControl.inputTextView.disableActionMenu = NO;
    
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    
    if ([_longPressSelectedModel.content isKindOfClass:[RCTextMessage class]]) {
        
        RCTextMessage *text = (RCTextMessage *)_longPressSelectedModel.content;
        
        [pasteboard setString:text.content];
    }
}

// 删除
- (void)onDeleteMessage:(id)sender {
    
    self.chatSessionInputBarControl.inputTextView.disableActionMenu = NO;
    
    RCMessageModel *model = _longPressSelectedModel;
    
    //删除消息时如果是当前播放的消息就停止播放
    if ([RCVoicePlayer defaultPlayer].isPlaying && [RCVoicePlayer defaultPlayer].messageId != nil && [[RCVoicePlayer defaultPlayer].messageId isEqualToString:[NSString stringWithFormat:@"%ld",model.messageId]] ) {
        [[RCVoicePlayer defaultPlayer] stopPlayVoice];
    }
    
    [self deleteMessage:model];
}

- (void)deleteMessage:(RCMessageModel *)model {
    
    long msgId = model.messageId;
    
    NSIndexPath *indexPath =
    [NSIndexPath indexPathForItem:[self findDataIndexFromMessageList:model]
                        inSection:0];
    
    [[RCIMClient sharedRCIMClient] deleteMessages:@[@(msgId)]];
    
    [self.conversationDataRepository removeObject:model];
    
    [self.conversationMessageCollectionView
     deleteItemsAtIndexPaths:[NSArray arrayWithObject:indexPath]];
}

// 设置头像样式
- (void)setMessageAvatarStyle:(RCUserAvatarStyle)avatarStyle {
    [RCIM sharedRCIM].globalMessageAvatarStyle = avatarStyle;
}

// 设置头像大小
- (void)setMessagePortraitSize:(CGSize)size {
    [RCIM sharedRCIM].globalMessagePortraitSize = size;
}

- (BOOL)isAtTheBottomOfTableView {
    
    if (self.conversationMessageCollectionView.contentSize.height <= self.conversationMessageCollectionView.frame.size.height) {
        return YES;
    }
    NSIndexPath *lastPath = [self getLastIndexPathForVisibleItems];
    if (lastPath.row >= self.conversationDataRepository.count -2) {
        return YES;
    }else{
        return NO;
    }
}

- (BOOL)prefersStatusBarHidden {
    
    return NO;
}

@end
