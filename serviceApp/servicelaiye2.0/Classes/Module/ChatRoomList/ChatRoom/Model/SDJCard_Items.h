//
//  SDJCard_Items.h
//  geniuslaiye2.0
//
//  Created by 盛东 on 15/12/8.
//  Copyright © 2015年 shengdong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SDJCard_Items : NSObject

@property (nonatomic, copy)NSString *Desc;

@property (nonatomic, copy)NSString *Effect;

@property (nonatomic, copy)NSString *ImageUrl;

@property (nonatomic, copy)NSString *Name;

@property (nonatomic, copy)NSString *ProviderId;

@property (nonatomic, copy)NSString *Url;

@property (nonatomic, copy)NSString *UrlTip;

- (instancetype)initWithDict:(NSDictionary *)dict;

+ (instancetype)modelWithDict:(NSDictionary *)dict;

@end
