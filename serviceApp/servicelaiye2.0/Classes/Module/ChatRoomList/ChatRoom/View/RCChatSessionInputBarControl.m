//
//  RCChatSessionInputBarControl.m
//  RongIMKit
//
//  Created by xugang on 15/2/12.
//  Copyright (c) 2015年 RongCloud. All rights reserved.
//

#import "RCChatSessionInputBarControl.h"
#import "RCKitUtility.h"
#import "SDJCommonSetting.h"

#define MAS_SHORTHAND_GLOBALS
#define MAS_SHORTHAND
#import "Masonry.h"

#import "SDJIntelligentBtn.h"

typedef void (^RCAnimationCompletionBlock)(BOOL finished);

@interface RCChatSessionInputBarControl () <UITextViewDelegate>

@end

@implementation RCChatSessionInputBarControl

- (id)initWithFrame:(CGRect)frame
    withContextView:(UIView *)contextView
               type:(RCChatSessionInputBarControlType)type
              style:(RCChatSessionInputBarControlStyle)style {
    if (self = [super initWithFrame:frame]) {
        [self setInputBarType:type style:style];
        self.currentPositionY = frame.origin.y;
        self.originalPositionY = frame.origin.y;
        
        self.inputTextview_height=36.0f;
        self.layer.borderColor = [UIColor colorWithRed:191 / 255.f green:191 / 255.f blue:191 / 255.f alpha:1].CGColor;
        self.layer.borderWidth = 0.5;
        self.backgroundColor = [UIColor whiteColor];
    }
    return self;
}

- (void)setInputBarType:(RCChatSessionInputBarControlType)type style:(RCChatSessionInputBarControlStyle)style {
    
    if (self.inputContainerView) {
        [self.inputContainerView removeFromSuperview];
        self.inputContainerView = nil;
    }
        
    self.inputContainerView = [[UIView alloc] initWithFrame:self.bounds];
    self.inputContainerView.backgroundColor = [UIColor colorWithRed:248 / 255.f green:248 / 255.f blue:248 / 255.f alpha:1];
    [self addSubview:_inputContainerView];
    
    [self setLayoutForInputContainerView:style];
    [self registerForNotifications];
}

// 懒加载
- (UIButton *)sendButton {
    if (!_sendButton) {
        _sendButton = [[UIButton alloc] initWithFrame:CGRectZero];
        [_sendButton setBackgroundImage:[UIImage imageNamed:@"chatroom_sendButtonIcon_highlighted"] forState:UIControlStateNormal];
        [_sendButton setBackgroundImage:[UIImage imageNamed:@"chatroom_sendButtonIcon_disabled"] forState:UIControlStateDisabled];
        [_sendButton addTarget:self action:@selector(sendButtonClicked) forControlEvents:UIControlEventTouchUpInside];
        [_sendButton setExclusiveTouch:YES];
        _sendButton.enabled = NO;
        [self.inputContainerView addSubview:_sendButton];
    }
    return _sendButton;
}

- (RCTextView *)inputTextView {
    if (!_inputTextView) {
        _inputTextView = [[RCTextView alloc] initWithFrame:CGRectZero];
        _inputTextView.delegate = self;
        [_inputTextView setExclusiveTouch:YES];
        [_inputTextView setTextColor:[UIColor blackColor]];
        [_inputTextView setFont:[UIFont systemFontOfSize:16]];
        [_inputTextView setReturnKeyType:UIReturnKeySend];
        _inputTextView.backgroundColor = [UIColor colorWithRed:248 / 255.f green:248 / 255.f blue:248 / 255.f alpha:1];
        _inputTextView.enablesReturnKeyAutomatically = YES;
        [_inputTextView setAccessibilityLabel:@"chat_input_textView"];
        [self.inputContainerView addSubview:_inputTextView];
    }
    return _inputTextView;
}

- (UIButton *)sugButton {
    if (!_sugButton) {
        _sugButton = [[SDJIntelligentBtn alloc] initWithFrame:CGRectZero];
        [_sugButton setBackgroundImage:[UIImage imageNamed:@"chatroom_intelligentButton"] forState:UIControlStateNormal];
        [_sugButton setBackgroundImage:[UIImage imageNamed:@"chatroom_intelligentButton"] forState:UIControlStateSelected];
        [_sugButton setExclusiveTouch:YES];
        [_sugButton addTarget:self action:@selector(didTouchSugButton:) forControlEvents:UIControlEventTouchUpInside];
        [self.inputContainerView addSubview:_sugButton];
    }
    return _sugButton;
}

- (UIButton *)pictureButton {
    if (!_pictureButton) {
        _pictureButton = [[SDJIntelligentBtn alloc] initWithFrame:CGRectZero];
        [_pictureButton setBackgroundImage:[UIImage imageNamed:@"registerVC_closeBtn"] forState:UIControlStateNormal];
        [_pictureButton setBackgroundImage:[UIImage imageNamed:@"chatroom_intelligentButton"] forState:UIControlStateSelected];
        [_pictureButton setExclusiveTouch:YES];
        [_pictureButton addTarget:self action:@selector(didTouchPictureButton:) forControlEvents:UIControlEventTouchUpInside];
        [self.inputContainerView addSubview:_pictureButton];
    }
    return _pictureButton;
}

- (UIButton *)cameraButton {
    if (!_cameraButton) {
        _cameraButton = [[SDJIntelligentBtn alloc] initWithFrame:CGRectZero];
        [_cameraButton setBackgroundImage:[UIImage imageNamed:@"registerVC_closeBtn"] forState:UIControlStateNormal];
        [_cameraButton setBackgroundImage:[UIImage imageNamed:@"chatroom_intelligentButton"] forState:UIControlStateSelected];
        [_cameraButton setExclusiveTouch:YES];
        [_cameraButton addTarget:self action:@selector(didTouchCameraButton:) forControlEvents:UIControlEventTouchUpInside];
        [self.inputContainerView addSubview:_cameraButton];
    }
    return _cameraButton;
}

- (UIButton *)voiceButton {
    if (!_voiceButton) {
        _voiceButton = [[UIButton alloc] initWithFrame:CGRectZero];
        [_voiceButton setBackgroundImage:[UIImage imageNamed:@"registerVC_sendButton"] forState:UIControlStateNormal];
        [_voiceButton setBackgroundImage:[UIImage imageNamed:@"chatroom_sendButtonIcon_disabled"] forState:UIControlStateDisabled];
        [_voiceButton addTarget:self action:@selector(didTouchVoiceButton:) forControlEvents:UIControlEventTouchUpInside];
        [_voiceButton setExclusiveTouch:YES];
        [self.inputContainerView addSubview:_voiceButton];
    }
    return _voiceButton;
}

- (UIButton *)recordButton {
    
    if (!_recordButton) {
        _recordButton = [[SDJIntelligentBtn alloc] initWithFrame:CGRectZero];
        [_recordButton setExclusiveTouch:YES];
        [_recordButton setHidden:YES];
        [_recordButton setBackgroundImage:IMAGE_BY_NAMED(@"press_for_audio") forState:UIControlStateNormal];
        [_recordButton setBackgroundImage:IMAGE_BY_NAMED(@"press_for_audio_down") forState:UIControlStateHighlighted];
        [_recordButton setTitle:NSLocalizedString(@"hold_to_talk_title", @"")
                       forState:UIControlStateNormal];
        [_recordButton setTitle:NSLocalizedString(@"release_to_send_title", @"")
                       forState:UIControlStateHighlighted];
        [_recordButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_recordButton addTarget:self
                          action:@selector(voiceRecordButtonTouchDown:)
                forControlEvents:UIControlEventTouchDown];
        [_recordButton addTarget:self
                          action:@selector(voiceRecordButtonTouchUpInside:)
                forControlEvents:UIControlEventTouchUpInside];
//        [_recordButton addTarget:self
//                          action:@selector(voiceRecordButtonTouchUpOutside:)
//                forControlEvents:UIControlEventTouchUpOutside];
//        [_recordButton addTarget:self
//                          action:@selector(voiceRecordButtonTouchDragExit:)
//                forControlEvents:UIControlEventTouchDragExit];
//        [_recordButton addTarget:self
//                          action:@selector(voiceRecordButtonTouchDragEnter:)
//                forControlEvents:UIControlEventTouchDragEnter];
//        [_recordButton addTarget:self
//                          action:@selector(voiceRecordButtonTouchCancel:)
//                forControlEvents:UIControlEventTouchCancel];
        [self.inputContainerView addSubview:_recordButton];
    }
    return _recordButton;
}

- (UIButton *)emojiButton {
    if (!_emojiButton) {
        _emojiButton = [[SDJIntelligentBtn alloc] initWithFrame:CGRectZero];
        [_emojiButton setBackgroundImage:[UIImage imageNamed:@"card_007"] forState:UIControlStateNormal];
        [_emojiButton setBackgroundImage:[UIImage imageNamed:@"chatroom_intelligentButton"] forState:UIControlStateSelected];
        [_emojiButton setExclusiveTouch:YES];
        [_emojiButton addTarget:self action:@selector(didTouchEmojiButton:) forControlEvents:UIControlEventTouchUpInside];
        [self.inputContainerView addSubview:_emojiButton];
    }
    return _emojiButton;
}

- (UIButton *)timeSugButton {
    if (!_timeSugButton) {
        _timeSugButton = [[SDJIntelligentBtn alloc] initWithFrame:CGRectZero];
        [_timeSugButton setBackgroundImage:[UIImage imageNamed:@"card_008"] forState:UIControlStateNormal];
        [_timeSugButton setBackgroundImage:[UIImage imageNamed:@"chatroom_intelligentButton"] forState:UIControlStateSelected];
        [_timeSugButton setExclusiveTouch:YES];
        [_timeSugButton addTarget:self action:@selector(didTouchTimeSugButton:) forControlEvents:UIControlEventTouchUpInside];
        [self.inputContainerView addSubview:_timeSugButton];
    }
    return _timeSugButton;
}

- (UIButton *)locationButton {
    if (!_locationButton) {
        _locationButton = [[SDJIntelligentBtn alloc] initWithFrame:CGRectZero];
        [_locationButton setBackgroundImage:[UIImage imageNamed:@"card_009"] forState:UIControlStateNormal];
        [_locationButton setBackgroundImage:[UIImage imageNamed:@"chatroom_intelligentButton"] forState:UIControlStateSelected];
        [_locationButton setExclusiveTouch:YES];
        [_locationButton addTarget:self action:@selector(didTouchLocationButton:) forControlEvents:UIControlEventTouchUpInside];
        [self.inputContainerView addSubview:_locationButton];
    }
    return _locationButton;
}

- (void)textViewDidChange:(UITextView *)textView {
    
    self.sendButton.enabled = (textView.text.length > 0);
}

#pragma mark - 新增按钮点击
- (void)sendButtonClicked {
    
    self.sendButton.enabled = NO;
    
    _inputTextview_height = 36.0f;
    
    CGRect intputTextRect = self.inputTextView.frame;
    intputTextRect.size.height = _inputTextview_height;
    intputTextRect.origin.y = 7;
    [_inputTextView setFrame:intputTextRect];
    
    CGRect vRect = self.frame;
    vRect.size.height = Height_ChatSessionInputBar;
    vRect.origin.y = _originalPositionY;
    _currentPositionY = _originalPositionY;
    [self changeSize:vRect];
    
    [self.delegate chatSessionInputBarControlContentSizeChanged:vRect];
    
    if ([self.delegate respondsToSelector:@selector(didTouchSendButton:)]) {
        [self.delegate didTouchSendButton:nil];
    }
}

- (void)setLayoutForInputContainerView:(RCChatSessionInputBarControlStyle)style {
    
    [self.inputContainerView makeConstraints:^(MASConstraintMaker *make) {
        make.right.left.top.bottom.equalTo(self);
    }];
    
    // new
    [self.sugButton makeConstraints:^(MASConstraintMaker *make) {
        make.height.width.equalTo(24);
        make.left.equalTo(self.inputContainerView).offset(15);
        make.bottom.equalTo(self.inputContainerView).offset(-10);
    }];
    
    [self.timeSugButton makeConstraints:^(MASConstraintMaker *make) {
        make.height.width.equalTo(24);
        make.left.equalTo(_sugButton.right).offset(15);
        make.bottom.equalTo(self.inputContainerView).offset(-10);
    }];

    [self.emojiButton makeConstraints:^(MASConstraintMaker *make) {
        make.height.width.equalTo(24);
        make.left.equalTo(_timeSugButton.right).offset(15);
        make.bottom.equalTo(self.inputContainerView).offset(-10);
    }];
    
    [self.pictureButton makeConstraints:^(MASConstraintMaker *make) {
        make.height.width.equalTo(24);
        make.left.equalTo(_emojiButton.right).offset(15);
        make.bottom.equalTo(self.inputContainerView).offset(-10);
    }];
    
    [self.cameraButton makeConstraints:^(MASConstraintMaker *make) {
        make.height.width.equalTo(24);
        make.left.equalTo(_pictureButton.right).offset(15);
        make.bottom.equalTo(self.inputContainerView).offset(-10);
    }];
    
    [self.locationButton makeConstraints:^(MASConstraintMaker *make) {
        make.height.width.equalTo(24);
        make.left.equalTo(_cameraButton.right).offset(15);
        make.bottom.equalTo(self.inputContainerView).offset(-10);
    }];
    
    [self.voiceButton makeConstraints:^(MASConstraintMaker *make) {
        make.height.width.equalTo(24);
        make.left.equalTo(_locationButton.right).offset(15);
        make.bottom.equalTo(self.inputContainerView).offset(-10);
    }];
    
    [self.sendButton makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.inputContainerView).offset(-15);
        make.height.width.equalTo(30);
        make.top.equalTo(self.inputContainerView).offset(10);
    }];
    
    [self.inputTextView makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.inputContainerView).offset(15);
        make.top.equalTo(self.inputContainerView).offset(7);
        make.right.equalTo(_sendButton.left).offset(-10);
        make.height.equalTo(36);
    }];
    
    [self.recordButton makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.right.height.equalTo(_inputTextView);
    }];

    [self layoutIfNeeded];
}

- (BOOL)canBecomeFirstResponder {
    
    return YES;
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {

    return NO; //隐藏系统默认的菜单项
}

#pragma mark - Notifications

- (void)registerForNotifications {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didReceiveKeyboardWillShowNotification:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didReceiveKeyboardWillHideNotification:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)didReceiveKeyboardWillShowNotification:(NSNotification *)notification {

    NSDictionary *userInfo = [notification userInfo];
    
    CGRect keyboardEndFrame = [userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    UIViewAnimationCurve animationCurve = [userInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue];
    NSInteger animationCurveOption = (animationCurve << 16);
    
    double animationDuration = [userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    [UIView animateWithDuration:animationDuration
                          delay:0.0
                        options:animationCurveOption
                     animations:^{
                         if ([self.delegate respondsToSelector:@selector(keyboardWillShowWithFrame:)]) {
                             [self.delegate keyboardWillShowWithFrame:keyboardEndFrame];
                         }
                     }
                     completion:^(BOOL finished) {}];
}

- (void)didReceiveKeyboardWillHideNotification:(NSNotification *)notification {
    
    if ([self.delegate respondsToSelector:@selector(keyboardWillHide)]) {
        [self.delegate keyboardWillHide];
    }
}

// 点击事件
- (void)didTouchSugButton:(UIButton *)sender {

    [self.delegate didTouchSugButton:sender];
}

- (void)didTouchPictureButton:(UIButton *)sender {

    [self.delegate didTouchPictureButton:sender];
}

- (void)didTouchCameraButton:(UIButton *)sender {
    
    [self.delegate didTouchCameraButton:sender];
}

- (void)didTouchEmojiButton:(UIButton *)sender {
    
    [self.delegate didTouchEmojiButton:sender];
}

- (void)didTouchTimeSugButton:(UIButton *)sender {
    
    [self.delegate didTouchTimeSugButton:sender];
}

- (void)didTouchLocationButton:(UIButton *)sender {
    
    [self.delegate didTouchLocationButton:sender];
}

- (void)didTouchVoiceButton:(UIButton *)sender {
    
    if (_recordButton.hidden) {
        _recordButton.hidden = NO;
    } else {
        _recordButton.hidden = YES;
    }
    
    _inputTextView.text = @"";
    _inputTextview_height = 36.0f;
    CGRect intputTextRect = self.inputTextView.frame;
    intputTextRect.size.height = _inputTextview_height;
    intputTextRect.origin.y = 7;
    [_inputTextView setFrame:intputTextRect];
    
    CGRect vRect = self.frame;
    vRect.size.height = Height_ChatSessionInputBar;
    vRect.origin.y = _originalPositionY;
    _currentPositionY = _originalPositionY;
    [self changeSize:vRect];
    [self.delegate chatSessionInputBarControlContentSizeChanged:vRect];
}

- (void)voiceRecordButtonTouchDown:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(didTouchRecordButon:event:)]) {
        [self.delegate didTouchRecordButon:sender event:UIControlEventTouchDown];
    }
}

- (void)voiceRecordButtonTouchUpInside:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(didTouchRecordButon:event:)]) {
        [self.delegate didTouchRecordButon:sender event:UIControlEventTouchUpInside];
    }
}

- (void)voiceRecordButtonTouchCancel:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(didTouchRecordButon:event:)]) {
        [self.delegate didTouchRecordButon:sender event:UIControlEventTouchCancel];
    }
}

- (void)voiceRecordButtonTouchDragExit:(UIButton *)sender {
    
    if ([self.delegate respondsToSelector:@selector(didTouchRecordButon:event:)]) {
        [self.delegate didTouchRecordButon:sender event:UIControlEventTouchDragExit];
    }
}

- (void)voiceRecordButtonTouchDragEnter:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(didTouchRecordButon:event:)]) {
        [self.delegate didTouchRecordButon:sender event:UIControlEventTouchDragEnter];
    }
}

- (void)voiceRecordButtonTouchUpOutside:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(didTouchRecordButon:event:)]) {
        [self.delegate didTouchRecordButon:sender event:UIControlEventTouchUpOutside];
    }
}

#pragma mark <UITextViewDelegate>
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if ([text isEqualToString:@"\n"]) {
        
        if ([self.delegate respondsToSelector:@selector(didTouchKeyboardReturnKey:text:)]) {
            NSString *_needToSendText = textView.text;
            NSString *_formatString =
            [_needToSendText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            if (0 == [_formatString length]) {
            } else {
                [self.delegate didTouchKeyboardReturnKey:self text:[_needToSendText copy]];
            }
        }
        self.inputTextView.text = @"";
        
        self.sendButton.enabled = NO;
        
        _inputTextview_height = 36.0f;
        
        CGRect intputTextRect = self.inputTextView.frame;
        intputTextRect.size.height = _inputTextview_height;
        intputTextRect.origin.y = 7;
        [_inputTextView setFrame:intputTextRect];
        
        CGRect vRect = self.frame;
        vRect.size.height = Height_ChatSessionInputBar;
        vRect.origin.y = _originalPositionY;
        _currentPositionY = _originalPositionY;
        [self changeSize:vRect];
        
        [self.delegate chatSessionInputBarControlContentSizeChanged:vRect];
        
        return NO;
    }
    
    _inputTextview_height = 36.0f;
    if (_inputTextView.contentSize.height < 70 && _inputTextView.contentSize.height > 36.0f) {
        _inputTextview_height = _inputTextView.contentSize.height;
    }
    if (_inputTextView.contentSize.height >= 70) {
        _inputTextview_height = 70;
    }
    
    CGSize textViewSize=[self TextViewAutoCalculateRectWith:text FontSize:16.0 MaxSize:CGSizeMake(_inputTextView.frame.size.width, 70)];
    
    CGSize textSize=[self TextViewAutoCalculateRectWith:textView.text FontSize:16.0 MaxSize:CGSizeMake(_inputTextView.frame.size.width, 70)];
    
    if (textViewSize.height<=36.0f&&range.location==0) {
        _inputTextview_height=36.0f;
    }
    else if(textViewSize.height>36.0f&&textViewSize.height<=55.0f)
    {
        _inputTextview_height=55.0f;
    }
    else if (textViewSize.height>55)
    {
        _inputTextview_height=70.0f;
    }
    
    if ([text isEqualToString:@""]&&range.location!=0) {
        if (textSize.width>=_inputTextView.frame.size.width&&range.length==1) {
            if (_inputTextView.contentSize.height < 70 && _inputTextView.contentSize.height > 36.0f) {
                _inputTextview_height = _inputTextView.contentSize.height;
            }
            if (_inputTextView.contentSize.height >= 70) {
                _inputTextview_height = 70;
            }
        }
        
        else
        {
            NSString *headString=[textView.text substringToIndex:range.location];
            NSString *lastString=[textView.text substringFromIndex:range.location+range.length];
            
            CGSize locationSize=[self TextViewAutoCalculateRectWith:[NSString stringWithFormat:@"%@%@",headString,lastString] FontSize:16.0 MaxSize:CGSizeMake(_inputTextView.frame.size.width, 70)];
            if (locationSize.height<=36.0) {
                _inputTextview_height=36.0;
                
            }
            if (locationSize.height>36.0&&locationSize.height<=55.0) {
                _inputTextview_height=55.0;
                
            }
            if (locationSize.height>55.0) {
                _inputTextview_height=70.0;
                
            }
        }
    }
    
    CGRect intputTextRect = self.inputTextView.frame;
    intputTextRect.size.height = _inputTextview_height;
    intputTextRect.origin.y = 7;
    [_inputTextView setFrame:intputTextRect];
    
    CGRect vRect = self.frame;
    vRect.size.height = Height_ChatSessionInputBar + (_inputTextview_height - 36);
    vRect.origin.y = _originalPositionY - (_inputTextview_height - 36);
    _currentPositionY = vRect.origin.y;
    [self changeSize:vRect];
    [self.delegate chatSessionInputBarControlContentSizeChanged:vRect];
    
    if (_inputTextview_height>70.0) {
        textView.contentOffset=CGPointMake(0, 100);
    }
    return YES;
}

- (void)changeChatSessionInputBarFrame {
    
    _inputTextview_height = 36.0f;
    if (_inputTextView.contentSize.height < 70 && _inputTextView.contentSize.height > 36.0f) {
        _inputTextview_height = _inputTextView.contentSize.height;
    }
    if (_inputTextView.contentSize.height >= 70) {
        _inputTextview_height = 70;
    }
    
    CGSize textViewSize=[self TextViewAutoCalculateRectWith:_inputTextView.text FontSize:16.0 MaxSize:CGSizeMake(_inputTextView.frame.size.width, 70)];
    
    if (textViewSize.height<=36.0f) {
        _inputTextview_height=36.0f;
    }
    else if(textViewSize.height>36.0f&&textViewSize.height<=55.0f)
    {
        _inputTextview_height=55.0f;
    }
    else if (textViewSize.height>55)
    {
        _inputTextview_height=70.0f;
    }
    
    CGRect intputTextRect = self.inputTextView.frame;
    intputTextRect.size.height = _inputTextview_height;
    intputTextRect.origin.y = 7;
    [_inputTextView setFrame:intputTextRect];
    
    CGRect vRect = self.frame;
    vRect.size.height = Height_ChatSessionInputBar + (_inputTextview_height - 36);
    vRect.origin.y = _originalPositionY - (_inputTextview_height - 36);
    _currentPositionY = vRect.origin.y;
    [self changeSize:vRect];
    [self.delegate chatSessionInputBarControlContentSizeChanged:vRect];
    
    if (_inputTextview_height>70.0) {
        _inputTextView.contentOffset=CGPointMake(0, 100);
    }
}

- (void)changeSize:(CGRect)rect {
    
    [_inputTextView updateConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(_inputTextview_height);
        make.top.equalTo(self.inputContainerView).offset(7);
    }];
}

- (CGSize)TextViewAutoCalculateRectWith:(NSString*)text FontSize:(CGFloat)fontSize MaxSize:(CGSize)maxSize
{
    NSMutableParagraphStyle* paragraphStyle = [[NSMutableParagraphStyle alloc]init];
    paragraphStyle.lineBreakMode=NSLineBreakByWordWrapping;
    NSDictionary* attributes =@{NSFontAttributeName:[UIFont systemFontOfSize:fontSize],NSParagraphStyleAttributeName:paragraphStyle.copy};
    if (text) {
        CGSize labelSize = [text boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading|NSStringDrawingTruncatesLastVisibleLine attributes:attributes context:nil].size;
        labelSize.height=ceil(labelSize.height);
        labelSize.width=ceil(labelSize.width);
        return labelSize;
    } else {
        return CGSizeZero;
    }
}

- (void)dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
