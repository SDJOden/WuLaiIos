//
//  RCAlbumListViewController.m
//  RongIMKit
//
//  Created by MiaoGuangfa on 6/4/15.
//  Copyright (c) 2015 RongCloud. All rights reserved.
//

#import "RCAlbumListViewController.h"
#import "RCAssetHelper.h"
#import "RCImagePickerViewController.h"

#import "RCAlbumListCell.h"

static NSString *cellReuseIdentifier = @"album.cell.reuse.index";

@interface RCAlbumListViewController () <RCImagePickerViewControllerDelegate>

@end

@implementation RCAlbumListViewController
- (instancetype)init
{
    self = [super init];
    if (self) {
        self.libraryList = [NSMutableArray new];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tableView registerClass:[RCAlbumListCell class] forCellReuseIdentifier:cellReuseIdentifier];
    self.title = NSLocalizedString(@"Albums", @"");
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Cancel", @"") style:UIBarButtonItemStyleDone target:self action:@selector(dismissCurrentModelViewController)];

    self.tableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
}

- (void)dismissCurrentModelViewController {
    [self dismissViewControllerAnimated:YES completion:^{}];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return [self.libraryList count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    RCAlbumListCell *cell = [tableView dequeueReusableCellWithIdentifier:cellReuseIdentifier forIndexPath:indexPath];
    [cell setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
    
    ALAssetsGroup *alAssetGroup_ = [self.libraryList objectAtIndex:indexPath.row];
    
    NSString * groupName_ = [alAssetGroup_ valueForProperty:ALAssetsGroupPropertyName];
    CGImageRef posterImage_CGImageRef_ = [alAssetGroup_ posterImage];
    UIImage *posterImage_ = [UIImage imageWithCGImage:posterImage_CGImageRef_];
    
    cell.imageView.image = posterImage_;
    cell.textLabel.text = [groupName_ stringByAppendingFormat:@" (%ld)", (long)[alAssetGroup_ numberOfAssets]];
    
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65.0f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    __weak typeof(&*self)weakSelf = self;
    
    ALAssetsGroup *selected_alAssetGroup_ = [self.libraryList objectAtIndex:indexPath.row];
    
    RCAssetHelper *sharedAssetHelper = [RCAssetHelper shareAssetHelper];
    
    [sharedAssetHelper getPhotosOfGroup:selected_alAssetGroup_ results:^(NSArray *photos) {
        
        if (nil != photos && [photos count] > 0) {
            dispatch_async(dispatch_get_main_queue(), ^{
                RCImagePickerViewController *imagePickerVC = [RCImagePickerViewController imagePickerViewController];
                imagePickerVC.photos = photos;
                imagePickerVC.delegate = weakSelf;
                
                NSString * groupName_ = [selected_alAssetGroup_ valueForProperty:ALAssetsGroupPropertyName];
                imagePickerVC.title = groupName_;
                
                [weakSelf.navigationController pushViewController:imagePickerVC animated:YES];
            });
        }
    }];
}

- (void)imagePickerViewController:(RCImagePickerViewController *)imagePickerViewController selectedImages:(NSArray *)selectedImages
{
    if ([self.delegate respondsToSelector:@selector(albumListViewController:selectedImages:)]) {
        [self.delegate albumListViewController:self selectedImages:selectedImages];
    }
}

@end
