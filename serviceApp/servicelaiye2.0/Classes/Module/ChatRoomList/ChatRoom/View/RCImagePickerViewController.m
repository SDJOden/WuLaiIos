//
//  RCImagePickerViewController.m
//
//  Created by Liv on 15/3/23.
//  Copyright (c) 2015年 RongCloud. All rights reserved.
//
#import "RCAssetHelper.h"
#import "RCImagePickerViewController.h"
#import "RCImagePickerCollectionViewCell.h"
@interface RCImagePickerViewController () <UICollectionViewDelegateFlowLayout>

- (void)dismissCurrentModelViewController;

@property(strong, nonatomic) UIImageView *ivPreview;

@property(strong, nonatomic) UIView *toolBar;

@end

@implementation RCImagePickerViewController

static NSString *const reuseIdentifier = @"Cell";

- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}
- (instancetype)initWithCollectionViewLayout:(UICollectionViewLayout *)layout
{
    self = [super initWithCollectionViewLayout:layout];
    if (self) {
        self.photos = [NSArray new];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"Cancel", @"") style:UIBarButtonItemStyleDone target:self action:@selector(dismissCurrentModelViewController)];

    // Register cell classes
    [self.collectionView registerClass:[RCImagePickerCollectionViewCell class]
            forCellWithReuseIdentifier:reuseIdentifier];

    // allow multiple selection
    self.collectionView.allowsMultipleSelection = YES;

    // long press for preview
    UILongPressGestureRecognizer *longTap =
        [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(onLongTapForPreview:)];
    longTap.minimumPressDuration = 0.15;
    [self.view addGestureRecognizer:longTap];

    // add preview
    _ivPreview = [[UIImageView alloc] initWithFrame:CGRectZero];
    _ivPreview.alpha = 0.0f;
    _ivPreview.contentMode = UIViewContentModeScaleAspectFit;
    _ivPreview.backgroundColor = [UIColor blackColor];
    _ivPreview.userInteractionEnabled = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTapPreview:)];
    [_ivPreview addGestureRecognizer:tap];
    [_ivPreview setTranslatesAutoresizingMaskIntoConstraints:NO];
    if (self.navigationController) {
        [self.navigationController.view addSubview:_ivPreview];
        [self.navigationController.view
            addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_ivPreview]|"
                                                                   options:kNilOptions
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings(_ivPreview)]];
        [self.navigationController.view
            addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_ivPreview]|"
                                                                   options:kNilOptions
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings(_ivPreview)]];
    } else {
        [self.view addSubview:_ivPreview];
        [self.view
            addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_ivPreview]|"
                                                                   options:kNilOptions
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings(_ivPreview)]];
        [self.view
            addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_ivPreview]|"
                                                                   options:kNilOptions
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings(_ivPreview)]];
    }

    // add bottom bar
    _toolBar = [[UIView alloc] initWithFrame:CGRectZero];
    [_toolBar setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:_toolBar];
    [_toolBar setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.view
        addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[_toolBar(40)]-0-|"
                                                               options:kNilOptions
                                                               metrics:nil
                                                                 views:NSDictionaryOfVariableBindings(_toolBar)]];
    [self.view
        addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_toolBar]|"
                                                               options:kNilOptions
                                                               metrics:nil
                                                                 views:NSDictionaryOfVariableBindings(_toolBar)]];

    // add butto for bottom bar
    UIButton *btnSend = [[UIButton alloc] initWithFrame:CGRectZero];
    [btnSend setTitle:NSLocalizedString(@"Send", @"") forState:UIControlStateNormal];
    [btnSend setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [btnSend addTarget:self action:@selector(btnSendCliced:) forControlEvents:UIControlEventTouchUpInside];
    [_toolBar addSubview:btnSend];

    [btnSend setTranslatesAutoresizingMaskIntoConstraints:NO];
    [_toolBar addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[btnSend(140)]"
                                                                     options:kNilOptions
                                                                     metrics:nil
                                                                       views:NSDictionaryOfVariableBindings(btnSend)]];
    [_toolBar addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[btnSend(33)]"
                                                                     options:kNilOptions
                                                                     metrics:nil
                                                                       views:NSDictionaryOfVariableBindings(btnSend)]];
    [_toolBar addConstraint:[NSLayoutConstraint constraintWithItem:btnSend
                                                         attribute:NSLayoutAttributeCenterY
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:_toolBar
                                                         attribute:NSLayoutAttributeCenterY
                                                        multiplier:1
                                                          constant:0]];
    [_toolBar addConstraint:[NSLayoutConstraint constraintWithItem:btnSend
                                                         attribute:NSLayoutAttributeCenterX
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:_toolBar
                                                         attribute:NSLayoutAttributeCenterX
                                                        multiplier:1
                                                          constant:0]];
    __weak RCImagePickerViewController *weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        NSIndexPath *path = [NSIndexPath indexPathForRow:weakSelf.photos.count - 1 inSection:0];
        [weakSelf.collectionView scrollToItemAtIndexPath:path atScrollPosition:UICollectionViewScrollPositionTop animated:NO];
    });
}

- (void)dismissCurrentModelViewController {
    [self dismissViewControllerAnimated:YES completion:^{}];
}

/**
 *  点击发送图片
 *
 *  @param sender sender description
 */
- (void)btnSendCliced:(UIButton *)sender {
    
    NSArray *indexPaths = [self.collectionView indexPathsForSelectedItems];
    NSMutableArray *selectedImages = [NSMutableArray new];
    for (NSIndexPath *indexPath in indexPaths) {
        ALAsset *asset = self.photos[indexPath.row];
        CGImageRef imgRef = [[asset defaultRepresentation] fullScreenImage];
        if (imgRef==nil) {
            imgRef=[asset thumbnail];
        }
        [selectedImages addObject:[UIImage imageWithCGImage:imgRef]];
    }
    if (self.delegate && [self.delegate respondsToSelector:@selector(imagePickerViewController:selectedImages:)]) {
        [self.delegate imagePickerViewController:self selectedImages:selectedImages];
    }
    [self dismissCurrentModelViewController];
}

/**
 *  long press for preview
 *
 *  @param sender sender description
 */
- (void)onLongTapForPreview:(UILongPressGestureRecognizer *)gesture {
    if (![gesture.view isKindOfClass:self.view.class])
        return;
    CGPoint point = [gesture locationInView:self.collectionView];
    NSIndexPath *indexPath = [self.collectionView indexPathForItemAtPoint:point];
    if (indexPath) {
        [self showPreviewAtIndexPath:indexPath];
    }
}

/**
 *  hide preview
 *
 *  @param gesture gesture description
 */
- (void)onTapPreview:(UITapGestureRecognizer *)gesture {
    _ivPreview.alpha = 0.0f;
}

/**
 *  show preview
 *
 *  @param indexPath indexPath description
 */
- (void)showPreviewAtIndexPath:(NSIndexPath *)indexPath {
    _ivPreview.alpha = 1.0;
    ALAsset *asset = _photos[indexPath.row];
    if (asset) {
        _ivPreview.image = [UIImage imageWithCGImage:[[asset defaultRepresentation] fullScreenImage]];
    }
    if (self.navigationController)
        [self.navigationController.view bringSubviewToFront:_ivPreview];
    else
        [self.view bringSubviewToFront:_ivPreview];
}

+ (instancetype)imagePickerViewController {
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
    RCImagePickerViewController *pickerViewController =
        [[RCImagePickerViewController alloc] initWithCollectionViewLayout:flowLayout];

    return pickerViewController;
}

#pragma mark -  <UICollectionViewDataSource>

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.photos.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    RCImagePickerCollectionViewCell *cell =
        [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];

    ALAsset *asset = self.photos[indexPath.row];
    if (asset) {
        [cell.imageView setImage:[UIImage imageWithCGImage:asset.thumbnail]];
    }
    return cell;
}

#pragma mark - <UICollectionViewDelegate>

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    //照片选择张数限制
    if (collectionView.indexPathsForSelectedItems.count > 9) {
        UIAlertView *alertView =
            [[UIAlertView alloc] initWithTitle:nil
                                       message:NSLocalizedString(@"MaxNumSelectPhoto", @"")
                                      delegate:nil
                             cancelButtonTitle:NSLocalizedString(@"OK", @"")
                             otherButtonTitles:nil, nil];
        [alertView show];
        [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    } else {
        RCImagePickerCollectionViewCell *cell =
            (RCImagePickerCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
        [cell setSelected:YES];
    }
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
    RCImagePickerCollectionViewCell *cell =
        (RCImagePickerCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    [cell setSelected:NO];
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView
                        layout:(UICollectionViewLayout *)collectionViewLayout
        insetForSectionAtIndex:(NSInteger)section {

    UICollectionViewFlowLayout *flowLayout = (UICollectionViewFlowLayout *)collectionViewLayout;

    flowLayout.minimumLineSpacing = 5;
    flowLayout.minimumInteritemSpacing = 5;
    return UIEdgeInsetsMake(5, 5, 5, 5);
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGRect bounds = collectionView.bounds;
    float width = (bounds.size.width - 25) / 4;
    float height = width;
    return CGSizeMake(width, height);
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                             layout:(UICollectionViewLayout *)collectionViewLayout
    referenceSizeForFooterInSection:(NSInteger)section {
    return CGSizeMake(self.view.bounds.size.width, 40);
}

@end
