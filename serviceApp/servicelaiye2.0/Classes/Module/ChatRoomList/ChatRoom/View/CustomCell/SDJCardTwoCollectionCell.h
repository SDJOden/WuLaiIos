//
//  SDJCardTwoCollectionCell.h
//  geniuslaiye2.0
//
//  Created by 盛东 on 15/12/1.
//  Copyright © 2015年 shengdong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RCMessageModel.h"

@interface SDJCardTwoCollectionCell : UICollectionViewCell

@property (nonatomic, strong)RCMessageModel *model;

@property (nonatomic, strong)UIImageView *backgroundImageView;

@end
