//
//  SDJCardTwoCollectionCell.m
//  geniuslaiye2.0
//
//  Created by 盛东 on 15/12/1.
//  Copyright © 2015年 shengdong. All rights reserved.
//

#import "SDJCardTwoCollectionCell.h"

#import "SDJCommonSetting.h"

#define MAS_SHORTHAND_GLOBALS
#define MAS_SHORTHAND
#import "Masonry.h"

@interface SDJCardTwoCollectionCell ()

@property (nonatomic, strong)UILabel *productLabel;

@property (nonatomic, strong)UILabel *detailLabel;

@property (nonatomic, strong)UILabel *priceLabel;

@end

@implementation SDJCardTwoCollectionCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize {
    // 清空操作
    NSArray *subviews = [[NSArray alloc] initWithArray:self.contentView.subviews];
    for (UIView *subview in subviews) {
        [subview removeFromSuperview];
    }
}

// 懒加载
- (UIImageView *)backgroundImageView {
    if (!_backgroundImageView) {
        _backgroundImageView = [[UIImageView alloc] init];
        [self.contentView addSubview:_backgroundImageView];
    }
    return _backgroundImageView;
}

- (UILabel *)productLabel {
    if (!_productLabel) {
        _productLabel = [[UILabel alloc] init];
        [_productLabel setTextColor:HEXCOLOR(0xffffff)];
        [_productLabel setFont:[UIFont systemFontOfSize:16]];
        [_productLabel setTextAlignment:1];
        [self.contentView addSubview:_productLabel];
    }
    return _productLabel;
}

- (UILabel *)detailLabel {
    if (!_detailLabel) {
        _detailLabel = [[UILabel alloc] init];
        [_detailLabel setTextColor:HEXCOLOR(0xffffff)];
        [_detailLabel setFont:[UIFont systemFontOfSize:12]];
        [_detailLabel setTextAlignment:1];
        [self.contentView addSubview:_detailLabel];
    }
    return _detailLabel;
}

- (UILabel *)priceLabel {
    if (!_priceLabel) {
        _priceLabel = [[UILabel alloc] init];
        [_priceLabel setTextColor:HEXCOLOR(0xffe600)];
        [_priceLabel setFont:[UIFont systemFontOfSize:14]];
        [_priceLabel setTextAlignment:1];
        [self.contentView addSubview:_priceLabel];
    }
    return _priceLabel;
}

- (void)setModel:(RCMessageModel *)model {
    
    // 设置控件信息, 设置布局
    self.productLabel.text = @"大杯热拿铁";
    self.detailLabel.text = [NSString stringWithFormat:@"%d人买过  %d人好评", 1820, 1820];
    self.priceLabel.text = [NSString stringWithFormat:@"￥%d",228];
    
    [self.detailLabel makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(10);
        make.bottom.equalTo(self.contentView).offset(-10);
    }];
    
    [self.productLabel makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(10);
        make.bottom.equalTo(_detailLabel.top).offset(-5);
    }];
    
    [self.priceLabel makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.contentView).offset(-15);
        make.bottom.equalTo(self.contentView).offset(-15);
    }];
    
    [self.backgroundImageView makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.bottom.equalTo(self.contentView);
    }];
}

@end
