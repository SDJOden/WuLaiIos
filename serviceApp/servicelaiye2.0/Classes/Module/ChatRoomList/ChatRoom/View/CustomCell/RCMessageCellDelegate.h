//
//  RCMessageCellDelegate.h
//  RongIMKit
//
//  Created by xugang on 3/14/15.
//  Copyright (c) 2015 RongCloud. All rights reserved.
//

#import "RCMessageModel.h"
#import "SDJCard_Items.h"

@protocol RCMessageCellDelegate <NSObject>

@optional
#pragma mark - new
- (void)didTouchCollectionCellAtIndex:(int)index WithModel:(SDJCard_Items *)model;

- (void)didTouchUrl: (NSString *)urlString;

// 点击消息内容
- (void)didTapMessageCell:(RCMessageModel *)model;

// 点击消息内容中的链接，此事件不会再触发didTapMessageCell
- (void)didTapUrlInMessageCell:(NSString *)url model:(RCMessageModel *)model;

// 点击消息内容中的电话号码，此事件不会再触发didTapMessageCell
- (void)didTapPhoneNumberInMessageCell:(NSString *) phoneNumber model:(RCMessageModel *)model;

// 点击头像事件
- (void)didTapCellPortrait:(NSString *)userId;

// 长按头像事件
- (void)didLongPressCellPortrait:(NSString *)userId;

// 长按消息内容
- (void)didLongTouchMessageCell:(RCMessageModel *)model inView:(UIView *)view;

// 点击消息发送失败视图事件
- (void)didTapmessageFailedStatusViewForResend:(RCMessageModel *)model;

@end
