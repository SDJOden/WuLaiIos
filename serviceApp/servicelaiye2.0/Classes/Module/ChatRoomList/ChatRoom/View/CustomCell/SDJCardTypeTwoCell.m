//
//  SDJCardTypeTwoCell.m
//  geniuslaiye2.0
//
//  Created by 盛东 on 15/11/30.
//  Copyright © 2015年 shengdong. All rights reserved.
//

#import "SDJCardTypeTwoCell.h"
#import "SDJCardTwoCollectionCell.h"
#import "SDJCardEnlargeController.h"

@interface SDJCardTypeTwoCell ()<UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic, strong)NSArray *array;

@property (nonatomic, strong)UICollectionView *collectionView;

@property (nonatomic, strong)UIImageView *iconView;

@property (nonatomic, strong)UILabel *serviceLabel;

@property (nonatomic, strong)UILabel *typeLabel;

@end

@implementation SDJCardTypeTwoCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)superButtonClicked {
    
    _chooseButton.enabled = NO;
}

// 布局
- (void)initialize { // 商品卡片（大图）
    
    // initialize的时候需要做一下清空操作, 否则cell重用的时候会把状态带过去
    
    // 一个头像, 两个label, 一个collectionView, 一个按钮, 按钮需要点击disable
}

// 控件懒加载
- (NSArray *)array {
    if (!_array) {
        _array = [[NSArray alloc] init];
    }
    return _array;
}

- (UIImageView *)iconView {
    
    if (!_iconView) {
        _iconView = [[UIImageView alloc] init];
        _iconView.image = [UIImage imageNamed:@"card_009"];
        [self.messageContentView addSubview:_iconView];
    }
    return _iconView;
}

- (UILabel *)typeLabel {
    
    if (!_typeLabel) {
        _typeLabel = [[UILabel alloc] init];
        [_typeLabel setTextColor:HEXCOLOR(0x333333)];
        [_typeLabel setFont:[UIFont systemFontOfSize:15]];
        [self.messageContentView addSubview:_typeLabel];
    }
    return _typeLabel;
}

- (UILabel *)serviceLabel {
    
    if (!_serviceLabel) {
        _serviceLabel = [[UILabel alloc] init];
        [_serviceLabel setTextColor:HEXCOLOR(0x999999)];
        [_serviceLabel setFont:[UIFont systemFontOfSize:11]];
        [self.messageContentView addSubview:_serviceLabel];
    }
    return _serviceLabel;
}

- (UIButton *)chooseButton {
    
    if (!_collectionView) {
        _chooseButton = [[UIButton alloc] init];
        [_chooseButton setBackgroundImage:[UIImage getImageWithSize:CGSizeMake(60, 40) backgroundColor:[UIColor colorWithRed:254/255.0 green:227/255.0 blue:12/255.0 alpha:1.0]] forState:UIControlStateNormal];
        [_chooseButton setBackgroundImage:[UIImage getImageWithSize:CGSizeMake(60, 40) backgroundColor:[UIColor grayColor]] forState:UIControlStateDisabled];
        [_chooseButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_chooseButton setTitle:@"来一份" forState:UIControlStateNormal];
        [_chooseButton setTitle:@"已选择" forState:UIControlStateDisabled];
        [_chooseButton addTarget:self action:@selector(superButtonClicked) forControlEvents:UIControlEventTouchUpInside];
        _chooseButton.layer.cornerRadius = 8; // 需要裁剪成圆角
        _chooseButton.layer.masksToBounds = YES;
        [self.messageContentView addSubview:_chooseButton];
    }
    return _chooseButton;
}

- (UICollectionView *)collectionView {
    
    if (!_collectionView) {
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc]init];
        flowLayout.itemSize = CGSizeMake(CARD_WIDTH - 20, (CARD_WIDTH - 20) * 0.5);
        flowLayout.minimumLineSpacing = 0;
        flowLayout.minimumInteritemSpacing = 0;
        flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        _collectionView = [[UICollectionView alloc]initWithFrame:self.frame collectionViewLayout:flowLayout];
        _collectionView.dataSource = self;
        _collectionView.backgroundColor = [UIColor whiteColor];
        _collectionView.delegate = self;
        _collectionView.pagingEnabled = YES;
        _collectionView.showsHorizontalScrollIndicator = NO;
        _collectionView.bounces = NO;
        _collectionView.layer.cornerRadius = 8; // 需要裁剪成圆角
        _collectionView.layer.masksToBounds = YES;
        [_collectionView registerClass:[SDJCardTwoCollectionCell class] forCellWithReuseIdentifier:@"SDJCardTwoCell"];
        [self.messageContentView addSubview:_collectionView];
    }
    return _collectionView;
}

- (void)setDataModel:(RCMessageModel *)model {
    
    // 设置按钮的点击状态
    self.chooseButton.enabled = YES;
    
    [super setDataModel:model];
    
    RCRichContentMessage *message = (RCRichContentMessage *)model.content; // 从这个message的extra中解析数据
    
    [self.statusContentView removeFromSuperview];
    [self.portraitImageView removeFromSuperview];
    
    // RCMessage基类来传递消息, 由后端来定数据解析方式和消息分辨方法
    
    CGRect messageContentViewRect = self.messageContentView.frame;
    messageContentViewRect.size.width = CARD_WIDTH;
    messageContentViewRect.size.height = CARDTWO_HEIGHT; // _array.count
    messageContentViewRect.origin.x = 15;
    self.messageContentView.frame = messageContentViewRect;

    self.messageContentView.backgroundColor = [UIColor whiteColor];
    self.messageContentView.layer.cornerRadius = 8; // 需要裁剪成圆角
    self.messageContentView.layer.masksToBounds = YES;
    
    // 设置数据
    self.typeLabel.text = @"连咖啡";
    self.serviceLabel.text = @"星巴克代购";
    
    // 清空cell设置
    self.chooseButton.enabled = YES; // 记不住状态, 按钮的点击问题.
    
    // 自动布局
    [self.iconView makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.equalTo(30);
        make.left.equalTo(self.messageContentView).offset(10);
        make.top.equalTo(self.messageContentView).offset(15);
    }];
    
    [self.typeLabel makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.messageContentView).offset(15);
        make.left.equalTo(_iconView.right).offset(5);
    }];
    
    [self.serviceLabel makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_iconView.right).offset(5);
        make.top.equalTo(_typeLabel.bottom).offset(3);
    }];
    
    [self.collectionView makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.messageContentView).offset(10);
        make.right.equalTo(self.messageContentView).offset(-10);
        make.top.equalTo(_iconView.bottom).offset(10);
        make.height.equalTo(([UIScreen mainScreen].bounds.size.width - 30 - 20) * 0.5);
    }];
    
    [self.chooseButton makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.messageContentView).offset(-15);
        make.left.equalTo(self.messageContentView).offset(10);
        make.right.equalTo(self.messageContentView).offset(-10);
        make.height.equalTo(44);
    }];
}

#pragma mark - dataSource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return 3; //_array.count
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    SDJCardTwoCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SDJCardTwoCell" forIndexPath:indexPath];
    
    cell.backgroundImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"card_00%d", (int)indexPath.item + 1]];
    
    cell.model = [RCMessageModel new];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnCollectionCell:)];
    [cell setUserInteractionEnabled:YES];
    
    [cell addGestureRecognizer:tap];
    
    return cell;
}

// 点击弹出大图展示
- (void)tapOnCollectionCell:(SDJCardTwoCollectionCell *)cell {
    
    if ([self.delegate respondsToSelector:@selector(didTouchCollectionCellAtIndex:WithModel:)]) {
        
        [self.delegate didTouchCollectionCellAtIndex:0 WithModel:nil];
        
    }
}

@end
