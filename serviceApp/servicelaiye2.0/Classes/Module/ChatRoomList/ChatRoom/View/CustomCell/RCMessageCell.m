//
//  RCMessageCommonCell.m
//  RongIMKit
//
//  Created by xugang on 15/1/28.
//  Copyright (c) 2015年 RongCloud. All rights reserved.
//

#import "RCMessageCell.h"
#import "RCTipLabel.h"

#import "UIImageView+WebCache.h"

NSString *const KNotificationMessageBaseCellUpdateSendingStatus = @"KNotificationMessageBaseCellUpdateSendingStatus";

#define COLOR_F0DEDA [UIColor colorWithRed:236/255.0 green:233/255.0 blue:229/255.0 alpha:1.0]

@interface RCMessageCell ()

@end

@implementation RCMessageCell

- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(messageCellUpdateSendingStatusEvent:)
                                                     name:KNotificationMessageBaseCellUpdateSendingStatus
                                                   object:nil];
        self.model = nil;
        self.messageTimeLabel = [RCTipLabel greyTipLabel];
        self.messageTimeLabel.backgroundColor = HEXCOLOR(0xECE8E5);
        [self.messageTimeLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:10]];
        self.baseContentView = [[UIView alloc] initWithFrame:CGRectZero];
        
        [self.contentView addSubview:self.messageTimeLabel];
        [self.contentView addSubview:_baseContentView];
        
        _isDisplayNickname = NO;
        self.delegate = nil;
    
        self.portraitImageView = [[UIImageView alloc] init];
  
        self.messageContentView = [[RCContentView alloc] initWithFrame:CGRectZero];
        self.statusContentView = [[UIView alloc] initWithFrame:CGRectZero];

        self.nicknameLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        self.nicknameLabel.backgroundColor = [UIColor clearColor];
        [self.nicknameLabel setFont:[UIFont systemFontOfSize:12.5f]];
        [self.nicknameLabel setTextColor:[UIColor grayColor]];

        //点击头像
        UITapGestureRecognizer *portraitTap =
            [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapUserPortaitEvent:)];
        portraitTap.numberOfTapsRequired = 1;
        portraitTap.numberOfTouchesRequired = 1;
        [self.portraitImageView addGestureRecognizer:portraitTap];

        UILongPressGestureRecognizer *portraitLongPress =
        [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressUserPortaitEvent:)];
        [self.portraitImageView addGestureRecognizer:portraitLongPress];

        self.portraitImageView.userInteractionEnabled = YES;

        [self.baseContentView addSubview:self.portraitImageView];
        [self.baseContentView addSubview:self.messageContentView];
        [self.baseContentView addSubview:self.statusContentView];
        [self.baseContentView addSubview:self.nicknameLabel];
        [self setPortraitStyle:[RCIM sharedRCIM].globalMessageAvatarStyle];

        self.statusContentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
        _statusContentView.backgroundColor = [UIColor clearColor];
        [self.baseContentView addSubview:_statusContentView];

        __weak typeof(&*self) __blockself = self;
        [self.messageContentView registerFrameChangedEvent:^(CGRect frame) {
          if (__blockself.model) {
              if (__blockself.model.messageDirection == MessageDirection_SEND) {
                  __blockself.statusContentView.frame = CGRectMake(
                      frame.origin.x - 10 - 25, frame.origin.y + (frame.size.height - 25) / 2.0f, 25, 25);
              } else {
                  __blockself.statusContentView.frame = CGRectZero;
              }
          }
        }];

        self.messageFailedStatusView = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
        [_messageFailedStatusView
            setImage:[RCKitUtility imageNamed:@"message_send_fail_status" ofBundle:@"RongCloud.bundle"]
            forState:UIControlStateNormal];
        [self.statusContentView addSubview:_messageFailedStatusView];
        _messageFailedStatusView.hidden = YES;
        [_messageFailedStatusView addTarget:self
                                 action:@selector(didclickMsgFailedView:)
                       forControlEvents:UIControlEventTouchUpInside];

//        self.messageActivityIndicatorView =
//            [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
//        [self.statusContentView addSubview:_messageActivityIndicatorView];
//        _messageActivityIndicatorView.hidden = YES;
    }
    return self;
}

- (void)setPortraitStyle:(RCUserAvatarStyle)portraitStyle {
    _portraitStyle = portraitStyle;

    if (_portraitStyle == RC_USER_AVATAR_RECTANGLE) {
        self.portraitImageView.layer.cornerRadius = [[RCIM sharedRCIM] portraitImageViewCornerRadius];
    }
    if (_portraitStyle == RC_USER_AVATAR_CYCLE) {
        self.portraitImageView.layer.cornerRadius = [[RCIM sharedRCIM] globalMessagePortraitSize].height/2;
    }
    self.portraitImageView.layer.masksToBounds = YES;
}

- (void)setDataModel:(RCMessageModel *)model {
    
    self.model = model;
    self.messageDirection = model.messageDirection;
    _isDisplayMessageTime = model.isDisplayMessageTime;
    self.messageTimeLabel.text = [RCKitUtility ConvertChatMessageTime:model.sentTime / 1000];

    _isDisplayNickname = model.isDisplayNickname;

    [self setCellAutoLayout];
}

- (void)setCellAutoLayout {
    
    //计算timelabel布局
    CGSize timeTextSize_ = CGSizeZero;
    if (IOS_FSystenVersion < 7.0) {
        timeTextSize_ = RC_MULTILINE_TEXTSIZE_LIOS7(self.messageTimeLabel.text, [UIFont systemFontOfSize:10], CGSizeMake(self.bounds.size.width, TIME_LABEL_HEIGHT), NSLineBreakByTruncatingTail);
    }else {
        timeTextSize_ = RC_MULTILINE_TEXTSIZE_GEIOS7(self.messageTimeLabel.text, [UIFont systemFontOfSize:10], CGSizeMake(self.bounds.size.width, TIME_LABEL_HEIGHT));
    }
    
    timeTextSize_ = CGSizeMake(ceilf(timeTextSize_.width + 20), ceilf(timeTextSize_.height));
    
    if (YES == self.isDisplayMessageTime) {
        self.messageTimeLabel.hidden = NO;
        [_baseContentView setFrame:CGRectMake(0, 10 + TIME_LABEL_HEIGHT + 10, self.bounds.size.width,
                                              self.bounds.size.height - (10 + 10 + TIME_LABEL_HEIGHT))];
    } else {
        self.messageTimeLabel.hidden = YES;
        [_baseContentView setFrame:CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height - (0))];
    }
    [self.messageTimeLabel setFrame:CGRectMake((self.bounds.size.width - timeTextSize_.width) / 2, 10, timeTextSize_.width,
                                               TIME_LABEL_HEIGHT)];
    
    // 设置头像布局
    _messageContentViewWidth = self.baseContentView.bounds.size.height - ([RCIM sharedRCIM].globalMessagePortraitSize.width + 10 + 5);
    
    if (MessageDirection_RECEIVE == self.messageDirection) {
        
        self.nicknameLabel.hidden = !self.isDisplayNickname;
        
        CGFloat portraitImageX = 10;
        CGFloat portraitImageY = self.baseContentView.bounds.size.height - [RCIM sharedRCIM].globalMessagePortraitSize.height - 10;
        self.portraitImageView.frame = CGRectMake(portraitImageX, portraitImageY, [RCIM sharedRCIM].globalMessagePortraitSize.width,
                                                  [RCIM sharedRCIM].globalMessagePortraitSize.height);
        
        CGFloat messageContentViewY = 10;
        self.messageContentView.frame =
            CGRectMake(portraitImageX + self.portraitImageView.bounds.size.width + 5, messageContentViewY,
                       _messageContentViewWidth, self.baseContentView.bounds.size.height - 10 * 2);
        
        self.portraitImageView.image = [RCIM sharedRCIM].geniusIcon;
        
    } else {
        
        CGFloat portraitImageX =
            self.baseContentView.bounds.size.width - ([RCIM sharedRCIM].globalMessagePortraitSize.width + 10);
        CGFloat portraitImageY = self.baseContentView.bounds.size.height - [RCIM sharedRCIM].globalMessagePortraitSize.height - 10;
        self.portraitImageView.frame = CGRectMake(portraitImageX, portraitImageY, [RCIM sharedRCIM].globalMessagePortraitSize.width,
                                                  [RCIM sharedRCIM].globalMessagePortraitSize.height);

        self.messageContentView.frame =
            CGRectMake(self.baseContentView.bounds.size.width -
                           (_messageContentViewWidth + 5 + [RCIM sharedRCIM].globalMessagePortraitSize.width + 10),
                       10, _messageContentViewWidth, self.baseContentView.bounds.size.height - 10 * 2);
        self.portraitImageView.image = [RCIM sharedRCIM].userIcon;
    }

    [self updateStatusContentView:self.model];
}

- (void)updateStatusContentView:(RCMessageModel *)model {
    
    if (model.messageDirection == MessageDirection_RECEIVE) {
        self.statusContentView.hidden = YES;
        return;
    } else {
        self.statusContentView.hidden = NO;
    }
    __weak typeof(&*self) __blockSelf = self;

    dispatch_async(dispatch_get_main_queue(), ^{

      if (__blockSelf.model.sentStatus == SentStatus_SENDING) {
          __blockSelf.messageFailedStatusView.hidden = YES;
          if (__blockSelf.messageActivityIndicatorView) {
              __blockSelf.messageActivityIndicatorView.hidden = NO;
              if (__blockSelf.messageActivityIndicatorView.isAnimating == NO) {
                  [__blockSelf.messageActivityIndicatorView startAnimating];
              }
          }

      } else if (__blockSelf.model.sentStatus == SentStatus_FAILED) {
          __blockSelf.messageFailedStatusView.hidden = NO;
          if (__blockSelf.messageActivityIndicatorView) {
              __blockSelf.messageActivityIndicatorView.hidden = YES;
              if (__blockSelf.messageActivityIndicatorView.isAnimating == YES) {
                  [__blockSelf.messageActivityIndicatorView stopAnimating];
              }
          }
      } else if (__blockSelf.model.sentStatus == SentStatus_SENT) {
          __blockSelf.messageFailedStatusView.hidden = YES;
          if (__blockSelf.messageActivityIndicatorView) {
              __blockSelf.messageActivityIndicatorView.hidden = YES;
              if (__blockSelf.messageActivityIndicatorView.isAnimating == YES) {
                  [__blockSelf.messageActivityIndicatorView stopAnimating];
              }
          }
      }
    });
}

#pragma mark private
- (void)tapUserPortaitEvent:(UIGestureRecognizer *)gestureRecognizer {
    __weak typeof(&*self) weakSelf = self;
    if ([self.delegate respondsToSelector:@selector(didTapCellPortrait:)]) {
        [self.delegate didTapCellPortrait:weakSelf.model.senderUserId];
    }
}

- (void)longPressUserPortaitEvent:(UIGestureRecognizer *)gestureRecognizer {
    __weak typeof(&*self) weakSelf = self;
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        if ([self.delegate respondsToSelector:@selector(didLongPressCellPortrait:)]) {
            [self.delegate didLongPressCellPortrait:weakSelf.model.senderUserId];
        }
    }
}

- (void)imageMessageSendProgressing:(NSInteger)progress {
    
}

- (void)messageCellUpdateSendingStatusEvent:(NSNotification *)notification {

    RCMessageCellNotificationModel *notifyModel = notification.object;

    if (self.model.messageId == notifyModel.messageId) {
        if ([notifyModel.actionName isEqualToString:CONVERSATION_CELL_STATUS_SEND_BEGIN]) {
            self.model.sentStatus = SentStatus_SENDING;
            [self updateStatusContentView:self.model];

        } else if ([notifyModel.actionName isEqualToString:CONVERSATION_CELL_STATUS_SEND_FAILED]) {
            self.model.sentStatus = SentStatus_FAILED;
            [self updateStatusContentView:self.model];
        } else if ([notifyModel.actionName isEqualToString:CONVERSATION_CELL_STATUS_SEND_SUCCESS]) {
            self.model.sentStatus = SentStatus_SENT;
            [self updateStatusContentView:self.model];
        } else if ([notifyModel.actionName isEqualToString:CONVERSATION_CELL_STATUS_SEND_PROGRESS]) {
            [self imageMessageSendProgressing:notifyModel.progress];
        }
    }
}

- (void)userInfoDidLoad:(NSNotification *)notification {
    __weak typeof(&*self) __blockSelf = self;

    RCUserInfo *userInfo = notification.object;

    if (userInfo && [userInfo.userId isEqualToString:self.model.senderUserId]) {
        self.model.userInfo = userInfo;
        dispatch_async(dispatch_get_main_queue(), ^{
          [__blockSelf.portraitImageView sd_setImageWithURL:[NSURL URLWithString:userInfo.portraitUri] placeholderImage:IMAGE_BY_NAMED(@"default_portrait")];
          [__blockSelf.nicknameLabel setText:userInfo.name];
        });
    }
}

- (void)userInfoFailToLoad:(NSNotification *)notification {

    __weak typeof(&*self) __blockSelf = self;

    RCUserInfo *userInfo = notification.object;

    if (userInfo) {
        self.model.userInfo = userInfo;
        dispatch_async(dispatch_get_main_queue(), ^{
          [__blockSelf.portraitImageView sd_setImageWithURL:[NSURL URLWithString:userInfo.portraitUri] placeholderImage:IMAGE_BY_NAMED(@"default_portrait")];
          [__blockSelf.nicknameLabel setText:userInfo.name];
        });
    }
}

- (void)didclickMsgFailedView:(UIButton *)button {
    if (self.delegate) {
        if ([self.delegate respondsToSelector:@selector(didTapmessageFailedStatusViewForResend:)]) {
            [self.delegate didTapmessageFailedStatusViewForResend:self.model];
        }
    }
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
