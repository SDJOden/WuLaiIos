//
//  SDJTextMessageCell.m
//  geniuslaiye2.0
//
//  Created by 盛东 on 15/12/2.
//  Copyright © 2015年 shengdong. All rights reserved.
//

#import "SDJTextMessageCell.h"
#import <CoreText/CoreText.h>
#import "NSString+WPAttributedMarkup.h"
#import "WPAttributedStyleAction.h"
#import "WPHotspotLabel.h"

@interface SDJTextMessageCell ()

@property (nonatomic, strong)WPHotspotLabel *presentLabel;

@end

@implementation SDJTextMessageCell

- (NSDictionary *)attributeDictionary {
    if (self.messageDirection == MessageDirection_SEND) {
        return @{
                 @(NSTextCheckingTypeLink) : @{NSForegroundColorAttributeName : HEXCOLOR(0x50ABF1), NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle), NSUnderlineColorAttributeName : HEXCOLOR(0x50ABF1)},
                 @(NSTextCheckingTypePhoneNumber) : @{NSForegroundColorAttributeName : HEXCOLOR(0x50ABF1)}
                 };
    } else {
        return @{
                 @(NSTextCheckingTypeLink) : @{NSForegroundColorAttributeName : HEXCOLOR(0x50ABF1), NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle), NSUnderlineColorAttributeName : HEXCOLOR(0x50ABF1)},
                 @(NSTextCheckingTypePhoneNumber) : @{NSForegroundColorAttributeName : HEXCOLOR(0x50ABF1)}
                 };
    }
    return nil;
}

- (NSDictionary *)highlightedAttributeDictionary { // 高亮颜色设置
    return [self attributeDictionary];
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize {
    self.bubbleBackgroundView = [[UIImageView alloc] initWithFrame:CGRectZero];
    [self.messageContentView addSubview:self.bubbleBackgroundView];
    
    [_presentLabel removeFromSuperview];
    
    self.textLabel = [[RCAttributedLabel alloc] initWithFrame:CGRectZero];
    self.textLabel.attributeDictionary = [self attributeDictionary];
    self.textLabel.highlightedAttributeDictionary = [self highlightedAttributeDictionary];
    [self.textLabel setFont:[UIFont systemFontOfSize:Text_Message_Font_Size]]; //
    
    self.textLabel.numberOfLines = 0;
    [self.textLabel setLineBreakMode:NSLineBreakByWordWrapping];
    [self.textLabel setTextAlignment:NSTextAlignmentLeft];
    
    self.textLabel.delegate=self;
    [self.bubbleBackgroundView addSubview:self.textLabel];
    self.bubbleBackgroundView.userInteractionEnabled = YES;
    
    UILongPressGestureRecognizer *longPress =
    [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressed:)];
    [self.bubbleBackgroundView addGestureRecognizer:longPress];
    
    UITapGestureRecognizer *textMessageTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapTextMessage:)];
    textMessageTap.numberOfTapsRequired = 1;
    textMessageTap.numberOfTouchesRequired = 1;
    [self.textLabel addGestureRecognizer:textMessageTap];
    self.textLabel.userInteractionEnabled = YES;
}

- (void)tapTextMessage:(UIGestureRecognizer *)gestureRecognizer {
    
    if (self.textLabel.currentTextCheckingType == NSTextCheckingTypeLink) {
        // open url
        NSString *urlString = [self.textLabel.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        
        if ([self.delegate respondsToSelector:@selector(didTapUrlInMessageCell:model:)]) {
            [self.delegate didTapUrlInMessageCell:urlString model:self.model];
            return;
        }
    } else if (self.textLabel.currentTextCheckingType == NSTextCheckingTypePhoneNumber) {
        // call phone number
        NSString *number = [@"tel://" stringByAppendingString:self.textLabel.text];
        if ([self.delegate respondsToSelector:@selector(didTapPhoneNumberInMessageCell:model:)]) {
            [self.delegate didTapPhoneNumberInMessageCell:number model:self.model];
            return;
        }
    }
    
    if ([self.delegate respondsToSelector:@selector(didTapMessageCell:)]) {
        [self.delegate didTapMessageCell:self.model];
    }
}

- (void)setDataModel:(RCMessageModel *)model {
    
    [super setDataModel:model];
    
    [self setAutoLayout];
}


#pragma mark - 聊天界面布局

- (void)setAutoLayout {
    
    [_presentLabel removeFromSuperview];
    //
    RCTextMessage *_textMessage = (RCTextMessage *)self.model.content;
    
    self.textLabel.text = _textMessage.content;
    
    NSString  *atstring = self.textLabel.text;
    
    NSMutableString *realTextString = [NSMutableString stringWithCapacity:0];
    
    if (_textMessage) {
        
        NSString *pattern = @"<a href=\'(.*?)\'>(.*?)</a>";
        
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern options:0 error:nil];
        
        NSArray *matches = [regex matchesInString:atstring options:0 range:NSMakeRange(0, atstring.length)]; // 网址
        
        NSUInteger lastIdx = 0;
        
        if (matches.count) { // 解析部分bug, 1.文字需要提出来, 2. 位置需要调整 matchs.count
            
            _presentLabel = [[WPHotspotLabel alloc] initWithFrame:CGRectZero]; // frame需要设置
            _presentLabel.numberOfLines = 0;
            [_presentLabel setLineBreakMode:NSLineBreakByWordWrapping];
            [_presentLabel setTextAlignment:NSTextAlignmentLeft];
            [self.bubbleBackgroundView addSubview:_presentLabel];
            [self.bubbleBackgroundView bringSubviewToFront:_presentLabel];
            if (MessageDirection_RECEIVE == self.messageDirection) {
                [_presentLabel setTextColor:[UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:1.0]];
            } else {
                [_presentLabel setTextColor:[UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0]];
            }
            
            NSMutableString *attributedString = [NSMutableString stringWithCapacity:0]; // 总字符串
            NSMutableArray *urlArrs = [[NSMutableArray alloc] initWithCapacity:0]; // 存url
            
            int i = 0;
            
            NSString *phoneNumber;
            
            for (NSTextCheckingResult* match in matches)
            {
                NSRange range = match.range;
                
                if (range.location > lastIdx)
                {
                    NSString *temp = [atstring substringWithRange:NSMakeRange(lastIdx, range.location - lastIdx)];
                    
                    [attributedString appendString:temp];// 记录头
                    [realTextString appendString:temp];
                }
                
                NSString *title = [atstring substringWithRange:[match rangeAtIndex:2]]; // 标题
                NSString *url = [atstring substringWithRange:[match rangeAtIndex:1]]; // 网址
                
                lastIdx = range.location + range.length;
                [attributedString appendFormat:@"<%d>%@</%d>",i,title,i];
                [realTextString appendFormat:@"%@",title];
                [urlArrs addObject:url];
                
                i++;
            }
            
            if (lastIdx < atstring.length)
            {
                NSString  *temp = [atstring substringFromIndex:lastIdx];
                
                [attributedString appendString:temp];// 记录尾
                [realTextString appendString:temp];
                
            }
            
            atstring = attributedString;
            _textLabel.text = @"";
            
            // 手机号
            NSString *patternTel = @"1[3|5|7|8|][0-9]{9}";
            NSRegularExpression *regularTel = [NSRegularExpression regularExpressionWithPattern:patternTel options:0 error:nil];
            NSArray *matchesTel = [regularTel matchesInString:atstring options:0 range:NSMakeRange(0, atstring.length)];
            lastIdx = 0;
            
            NSMutableString *attributedTelString = [NSMutableString stringWithCapacity:0];
            
            if (matchesTel.count) {
                
                for (NSTextCheckingResult* match in matchesTel)
                {
                    NSRange range = match.range;
                    
                    if (range.location > lastIdx)
                    {
                        NSString *temp = [atstring substringWithRange:NSMakeRange(lastIdx, range.location - lastIdx)];
                        
                        [attributedTelString appendString:temp];// 记录头
                    }
                    
                    NSString *title = [atstring substringWithRange:NSMakeRange(range.location, range.length)]; // 网址
                    
                    lastIdx = range.location + range.length;
                    [attributedTelString appendFormat:@"<tel>%@</tel>",title];
                    phoneNumber = title;
                }
                
                if (lastIdx < atstring.length)
                {
                    NSString  *temp = [atstring substringFromIndex:lastIdx];
                    
                    [attributedTelString appendString:temp];// 记录尾
                }
                attributedString = attributedTelString;
            }
            
            switch (urlArrs.count) {
                    
                case 1:
                {
                    if (phoneNumber) {
                        
                        NSDictionary* style = @{@"body":[UIFont fontWithName:@"HelveticaNeue" size:Text_Message_Font_Size],
                                                @"0":[WPAttributedStyleAction styledActionWithAction:^{
                                                    if ([self.delegate respondsToSelector:@selector(didTouchUrl:)]) {
                                                        [self.delegate didTouchUrl:urlArrs[0]];
                                                    }
                                                }],
                                                @"tel":[WPAttributedStyleAction styledActionWithAction:^{
                                                    NSString *number = [@"tel://" stringByAppendingString:phoneNumber];
                                                    if ([self.delegate respondsToSelector:@selector(didTapPhoneNumberInMessageCell:model:)]) {
                                                        [self.delegate didTapPhoneNumberInMessageCell:number model:nil];
                                                    }
                                                }],
                                                @"link": HEXCOLOR(0x50ABF1)};
                        _presentLabel.attributedText = [attributedString attributedStringWithStyleBook:style];
                    } else {
                        
                        NSDictionary* style = @{@"body":[UIFont fontWithName:@"HelveticaNeue" size:Text_Message_Font_Size],
                                                @"0":[WPAttributedStyleAction styledActionWithAction:^{
                                                    if ([self.delegate respondsToSelector:@selector(didTouchUrl:)]) {
                                                        [self.delegate didTouchUrl:urlArrs[0]];
                                                    }
                                                }],
                                                @"link": HEXCOLOR(0x50ABF1)};
                        _presentLabel.attributedText = [attributedString attributedStringWithStyleBook:style];
                    }
                }
                    break;
                case 2:
                {
                    if (phoneNumber) {
                        
                        NSDictionary* style = @{@"body":[UIFont fontWithName:@"HelveticaNeue" size:Text_Message_Font_Size],
                                                @"0":[WPAttributedStyleAction styledActionWithAction:^{
                                                    if ([self.delegate respondsToSelector:@selector(didTouchUrl:)]) {
                                                        
                                                        [self.delegate didTouchUrl:urlArrs[0]];
                                                    }
                                                }],
                                                @"1":[WPAttributedStyleAction styledActionWithAction:^{
                                                    if ([self.delegate respondsToSelector:@selector(didTouchUrl:)]) {
                                                        
                                                        [self.delegate didTouchUrl:urlArrs[1]];
                                                    }
                                                }],
                                                @"tel":[WPAttributedStyleAction styledActionWithAction:^{
                                                    NSString *number = [@"tel://" stringByAppendingString:phoneNumber];
                                                    if ([self.delegate respondsToSelector:@selector(didTapPhoneNumberInMessageCell:model:)]) {
                                                        [self.delegate didTapPhoneNumberInMessageCell:number model:nil];
                                                    }
                                                }],
                                                @"link": HEXCOLOR(0x50ABF1)};
                        _presentLabel.attributedText = [attributedString attributedStringWithStyleBook:style];
                    } else {
                        
                        NSDictionary* style = @{@"body":[UIFont fontWithName:@"HelveticaNeue" size:Text_Message_Font_Size],
                                                @"0":[WPAttributedStyleAction styledActionWithAction:^{
                                                    if ([self.delegate respondsToSelector:@selector(didTouchUrl:)]) {
                                                        
                                                        [self.delegate didTouchUrl:urlArrs[0]];
                                                    }
                                                }],
                                                @"1":[WPAttributedStyleAction styledActionWithAction:^{
                                                    if ([self.delegate respondsToSelector:@selector(didTouchUrl:)]) {
                                                        
                                                        [self.delegate didTouchUrl:urlArrs[1]];
                                                    }
                                                }],
                                                @"link": HEXCOLOR(0x50ABF1)};
                        _presentLabel.attributedText = [attributedString attributedStringWithStyleBook:style];
                    }
                }
                    break;
                default:
                    break;
            }
            
        } else {
            [_presentLabel removeFromSuperview];
            self.textLabel.text = _textMessage.content;
            [realTextString appendString:_textMessage.content];
            [self.bubbleBackgroundView bringSubviewToFront:_textLabel];
        }
        
    } else {
    }
    
    CGSize __textSize = CGSizeZero;
    if (IOS_FSystenVersion < 7.0) {
        __textSize = RC_MULTILINE_TEXTSIZE_LIOS7(realTextString, [UIFont systemFontOfSize:Text_Message_Font_Size], CGSizeMake(self.baseContentView.bounds.size.width -
                                                                                                                              (10 + [RCIM sharedRCIM].globalMessagePortraitSize.width + 5) * 2 - 5 -
                                                                                                                              20 * 2,
                                                                                                                              8000), NSLineBreakByTruncatingTail);
    }else {
        __textSize = RC_MULTILINE_TEXTSIZE_GEIOS7(realTextString, [UIFont systemFontOfSize:Text_Message_Font_Size], CGSizeMake(self.baseContentView.bounds.size.width -
                                                                                                                               (10 + [RCIM sharedRCIM].globalMessagePortraitSize.width + 5) * 2 - 5 -
                                                                                                                               20 * 2,
                                                                                                                               8000));
    }
    __textSize = CGSizeMake(ceilf(__textSize.width), ceilf(__textSize.height));

    CGSize __labelSize = CGSizeMake(__textSize.width, __textSize.height);
    CGFloat __bubbleWidth = __labelSize.width + 20 + 20 < 50 ? 50 : (__labelSize.width + 20 + 20);
    CGFloat __bubbleHeight = __labelSize.height + 12 + 12 < 46 ? 46 : (__labelSize.height + 12 + 12);
    CGSize __bubbleSize = CGSizeMake(__bubbleWidth, __bubbleHeight);
    
    CGRect messageContentViewRect = self.messageContentView.frame;
    
    if (MessageDirection_RECEIVE == self.messageDirection) {
        
        messageContentViewRect.size.width = __bubbleSize.width;
        self.messageContentView.frame = messageContentViewRect;
        self.bubbleBackgroundView.frame = CGRectMake(0, 0, __bubbleSize.width, __bubbleSize.height);
        
        self.textLabel.frame = CGRectMake(20, 12, __labelSize.width, __labelSize.height);
        [self.textLabel setTextColor:[UIColor colorWithRed:34/255.0 green:34/255.0 blue:34/255.0 alpha:1.0]];
        
        if (_presentLabel) {
            _presentLabel.frame = CGRectMake(20, 0, __labelSize.width, __bubbleSize.height);
        }
        
        self.bubbleBackgroundView.image =  [UIImage imageNamed:@"chatroom_chatroomBackground_xiaolai"];
        
    } else {
        
        messageContentViewRect.size.width = __bubbleSize.width;
        messageContentViewRect.origin.x =
        self.baseContentView.bounds.size.width -
        (messageContentViewRect.size.width + 5 + [RCIM sharedRCIM].globalMessagePortraitSize.width + 10);
        self.messageContentView.frame = messageContentViewRect;
        self.bubbleBackgroundView.frame = CGRectMake(0, 0, __bubbleSize.width, __bubbleSize.height);
        
        self.textLabel.frame = CGRectMake(20, 12, __labelSize.width, __labelSize.height);
        [self.textLabel setTextColor:[UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1.0]]; // 用户 白色
        
        if (_presentLabel) {
            _presentLabel.frame = CGRectMake(20, 0, __labelSize.width, __bubbleSize.height);
        }
        
        self.bubbleBackgroundView.image =  [UIImage imageNamed:@"chatroom_chatroomBackground_user"];
    }
}

- (void)dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)longPressed:(id)sender {
    UILongPressGestureRecognizer *press = (UILongPressGestureRecognizer *)sender;
    if (press.state == UIGestureRecognizerStateEnded) {
        return;
    } else if (press.state == UIGestureRecognizerStateBegan) {
        [self.delegate didLongTouchMessageCell:self.model inView:self.bubbleBackgroundView];
    }
}

- (void)attributedLabel:(RCAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url
{
    NSString *urlString=[url absoluteString];
    NSString *httpIndex = [[url absoluteString] substringToIndex:7];
    if (![httpIndex isEqualToString:@"http://"]) {
        urlString = [@"http://" stringByAppendingString:urlString];
    }
    if ([self.delegate respondsToSelector:@selector(didTapUrlInMessageCell:model:)]) {
        [self.delegate didTapUrlInMessageCell:urlString model:self.model];
        return;
    }
}

@end
