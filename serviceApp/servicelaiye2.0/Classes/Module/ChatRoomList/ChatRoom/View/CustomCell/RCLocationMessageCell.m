//
//  RCLocationMessageCell.m
//  RongIMKit
//
//  Created by xugang on 15/2/2.
//  Copyright (c) 2015年 RongCloud. All rights reserved.
//

#import "RCLocationMessageCell.h"

@interface RCLocationMessageCell ()

@end

@implementation RCLocationMessageCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize {
    
    self.pictureView = [[UIImageView alloc] initWithFrame:CGRectZero];
//    self.pictureView.contentMode = UIViewContentModeScaleAspectFill;
    self.pictureView.clipsToBounds = YES;
    self.pictureView.layer.cornerRadius = 10;
    self.pictureView.layer.masksToBounds = YES;

    self.locationNameLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    self.locationNameLabel.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.6];
    self.locationNameLabel.textAlignment = NSTextAlignmentCenter;
    self.locationNameLabel.textColor = [UIColor whiteColor];
    self.locationNameLabel.font = [UIFont systemFontOfSize:12.0f];

    [self.pictureView addSubview:self.locationNameLabel];

    [self.messageContentView addSubview:self.pictureView];
    UILongPressGestureRecognizer *longPress =
    [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressed:)];
    [self.pictureView addGestureRecognizer:longPress];
    UITapGestureRecognizer *pictureTap =
        [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapPicture:)];
    pictureTap.numberOfTapsRequired = 1;
    pictureTap.numberOfTouchesRequired = 1;
    [self.pictureView addGestureRecognizer:pictureTap];
    self.pictureView.userInteractionEnabled = YES;
}

- (void)tapPicture:(UIGestureRecognizer *)gestureRecognizer {
    if ([self.delegate respondsToSelector:@selector(didTapMessageCell:)]) {
        [self.delegate didTapMessageCell:self.model];
    }
}

#pragma mark override,
- (void)setDataModel:(RCMessageModel *)model {
    
    [super setDataModel:model];
    
    RCLocationMessage *_locationMessage = (RCLocationMessage *)model.content;
    if (_locationMessage) {
        
        self.locationNameLabel.text = _locationMessage.locationName;

        //写死尺寸 原来400 *230
        CGSize imageSize = CGSizeMake(180, 140);
        //图片half
        imageSize = CGSizeMake(imageSize.width, imageSize.height);
        
        self.locationNameLabel.frame = CGRectMake(0, imageSize.height - 20, imageSize.width, 20);

        CGRect messageContentViewRect = self.messageContentView.frame; // 把缩略图切一下
        if (model.messageDirection == MessageDirection_RECEIVE) {
            messageContentViewRect.size.width = imageSize.width;
            self.messageContentView.frame = messageContentViewRect;
            self.pictureView.frame = CGRectMake(0, 0, imageSize.width, imageSize.height);
            
            self.pictureView.image = [_locationMessage.thumbnailImage drawImagewithWidth:3.0 withColor:HEXCOLOR(0xd4d4d4) withDirection:NO];
        } else {
            messageContentViewRect.size.width = imageSize.width;
            messageContentViewRect.origin.x =
                self.baseContentView.bounds.size.width -
                (imageSize.width + 10 + [RCIM sharedRCIM].globalMessagePortraitSize.width + 10);
            self.messageContentView.frame = messageContentViewRect;
            self.pictureView.frame = CGRectMake(0, 0, imageSize.width, imageSize.height);
            
            self.pictureView.image = [_locationMessage.thumbnailImage drawImagewithWidth:3.0 withColor:HEXCOLOR(0xd4d4d4) withDirection:NO]; // 切图没用, 上面有label, 而且contentfillmode影响, 得切控件形式
        }
    } else {}

    [self setAutoLayout];
}

- (void)setAutoLayout {}

- (void)msgStatusViewTapEventHandler:(id)sender {}

- (void)longPressed:(id)sender {
    UILongPressGestureRecognizer *press = (UILongPressGestureRecognizer *)sender;
    if (press.state == UIGestureRecognizerStateEnded) {
        return;
    } else if (press.state == UIGestureRecognizerStateBegan) {
        [self.delegate didLongTouchMessageCell:self.model inView:self.pictureView];
    }
}

@end
