//
//  SDJCardThreeTableCell.h
//  geniuslaiye2.0
//
//  Created by 盛东 on 15/12/1.
//  Copyright © 2015年 shengdong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RCMessageModel.h"

@interface SDJCardThreeTableCell : UITableViewCell

@property (nonatomic, strong)RCMessageModel *model;

+ (instancetype)returnCellWithTableView:(UITableView *)tableView andWithCellStyle:(NSString *)cellStyle;

@end
