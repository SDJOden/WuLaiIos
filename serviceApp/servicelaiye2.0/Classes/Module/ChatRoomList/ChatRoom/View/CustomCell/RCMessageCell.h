//
//  RCMessageCommonCell.h
//  RongIMKit
//
//  Created by xugang on 15/1/28.
//  Copyright (c) 2015年 RongCloud. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <RongIMLib/RongIMLib.h>
#import "RCMessageModel.h"

#import "RCKitUtility.h"
#import "SDJCommonSetting.h"
#import "UIImage+SDJExtension.h"

#define MAS_SHORTHAND_GLOBALS
#define MAS_SHORTHAND
#import "Masonry.h"
#import "RCIM.h"
#import "RCMessageCellNotificationModel.h"
#import "RCMessageCellDelegate.h"
#import "RCContentView.h"

#define CARD_WIDTH [UIScreen mainScreen].bounds.size.width - 30
#define CARDONE_HEIGHT ([UIScreen mainScreen].bounds.size.width - 30) * 0.5
#define CARDTWO_HEIGHT ([UIScreen mainScreen].bounds.size.width - 30 - 20) * 0.5 + 124
#define CARDTHREE_HEIGHT 65
#define CARDFOUR_HEIGHT 83
#define CARDFIVE_HEIGHT 157

// 消息发送状态通知回调Key
UIKIT_EXTERN NSString *const KNotificationMessageBaseCellUpdateSendingStatus;
#define TIME_LABEL_HEIGHT 10

// cell内部是可以可以用自动布局的
@class RCCollectionCellAttributes;
@class RCTipLabel;

// MessageBaseCell子类，用于创建基本的Cell内容
@interface RCMessageCell : UICollectionViewCell

// 显示时间的Label
@property(strong, nonatomic) RCTipLabel *messageTimeLabel;

// 消息数据模型
@property(strong, nonatomic) RCMessageModel *model;

// 消息父视图区域
@property(strong, nonatomic) UIView *baseContentView;

// 消息方向
@property(nonatomic) RCMessageDirection messageDirection;

// 是否显示消息时间
@property(nonatomic, readonly) BOOL isDisplayMessageTime;

// 类初始化方法
- (instancetype)initWithFrame:(CGRect)frame;

// 消息发送状态通知
- (void)messageCellUpdateSendingStatusEvent:(NSNotification *)notification;

@property(nonatomic, weak) id<RCMessageCellDelegate> delegate;

@property(nonatomic, strong) UIImageView *portraitImageView; // 用户头像

@property(nonatomic, strong) UILabel *nicknameLabel; // 用户昵称

@property(nonatomic, strong) RCContentView *messageContentView; // 消息内容视图

@property(nonatomic, strong) UIView *statusContentView; // 消息状态视图

@property(nonatomic, strong) UIButton *messageFailedStatusView; // 消息发送失败状态视图

@property(nonatomic, strong) UIActivityIndicatorView *messageActivityIndicatorView; // 消息发送指示视图

@property(nonatomic, readonly) CGFloat messageContentViewWidth; // 消息内容视图宽度

@property(nonatomic, assign, setter=setPortraitStyle:) RCUserAvatarStyle portraitStyle; // 用户头像样式

@property(nonatomic, readonly) BOOL isDisplayNickname; // 是否显示用户昵称

- (void)setDataModel:(RCMessageModel *)model; // 设置数据模型

- (void)updateStatusContentView:(RCMessageModel *)model; // 更新消息发送状态视图

@end
