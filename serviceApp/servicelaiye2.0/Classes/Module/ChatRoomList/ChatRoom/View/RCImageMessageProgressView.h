//
//  RCNumberProgressView.h
//  RCIM
//
//  Created by xugang on 6/5/14.
//  Copyright (c) 2014 Heq.Shinoda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RCImageMessageProgressView : UIView

// 进度显示Label
@property(nonatomic, assign) UILabel *label;

// 进度指示
@property(nonatomic, strong) UIActivityIndicatorView *indicatorView;

- (void)updateProgress:(NSInteger)progress;

- (void)startAnimating;

- (void)stopAnimating;

@end
