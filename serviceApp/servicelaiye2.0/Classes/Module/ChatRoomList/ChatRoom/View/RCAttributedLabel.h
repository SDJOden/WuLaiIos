//
//  RCAttributedLabel.h
//  iOS-IMKit
//
//  Created by YangZigang on 14/10/29.
//  Copyright (c) 2014年 RongCloud. All rights reserved.
//

#import <UIKit/UIKit.h>

// 这里已经高亮了, 那么判断高亮范围的在哪里,也是在这里 // 用coreText进行高亮判断和混排

@interface RCAttributedLabelClickedTextInfo : NSObject

@property(nonatomic, assign) NSTextCheckingType textCheckingType;

@property(nonatomic, strong) NSString *text;

@end

@protocol RCAttributedDataSource <NSObject>

- (NSDictionary *)attributeDictionaryForTextType:(NSTextCheckingTypes)textType;

- (NSDictionary *)highlightedAttributeDictionaryForTextType:(NSTextCheckingType)textType;

@end

@protocol RCAttributedLabelDelegate;

@protocol RCAttributedLabel <NSObject>

@property (nonatomic, copy) id text;

@end

@interface RCAttributedLabel : UILabel <RCAttributedDataSource,UIGestureRecognizerDelegate>

// 可以通过设置attributeDataSource或者attributeDictionary、highlightedAttributeDictionary来自定义不同文本的字体颜色
@property(nonatomic, strong) id<RCAttributedDataSource> attributeDataSource;

// 设置点击事件，比如打开超链接等等
@property (nonatomic, assign) id <RCAttributedLabelDelegate> delegate;

@property(nonatomic, strong) NSDictionary *attributeDictionary;

@property(nonatomic, strong) NSDictionary *highlightedAttributeDictionary;

@property(nonatomic, assign) NSTextCheckingTypes textCheckingTypes;

@property(nonatomic, readonly, assign) NSTextCheckingType currentTextCheckingType;

- (void)setText:(NSString *)text dataDetectorEnabled:(BOOL)dataDetectorEnabled;

- (RCAttributedLabelClickedTextInfo *)textInfoAtPoint:(CGPoint)point;

- (void)setTextHighlighted:(BOOL)highlighted atPoint:(CGPoint)point;

@end

/**
 *  RCAttributedLabelDelegate
 */
@protocol RCAttributedLabelDelegate <NSObject>
@optional
//  打开超链接
- (void)attributedLabel:(RCAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url;
//  打开电话类型
- (void)attributedLabel:(RCAttributedLabel *)label didSelectLinkWithPhoneNumber:(NSString *)phoneNumber;
//  响应点击事件
- (void)attributedLabel:(RCAttributedLabel *)label didTapLabel:(NSString *)content;

@end

