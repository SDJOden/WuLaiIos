//
//  RCEmojiPageControl.m
//  iOS-IMKit
//
//  Created by Heq.Shinoda on 14-7-12.
//  Copyright (c) 2014年 Heq.Shinoda. All rights reserved.
//

#import "RCEmojiPageControl.h"
#import "SDJCommonSetting.h"
#import "RCKitUtility.h"

@implementation RCEmojiPageControl

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        activeImage = IMAGE_BY_NAMED(@"emoji_pagecontrol_selected");
        inactiveImage = IMAGE_BY_NAMED(@"emoji_pagecontrol_normol");

        [self setCurrentPage:1];
    }
    return self;
}

- (void)updateDots {

    for (int i = 0; i < [self.subviews count]; i++) {

        UIImageView *dot = (self.subviews)[i];

        if (i == self.currentPage) {

            if ([dot isKindOfClass:UIImageView.class]) {

                ((UIImageView *)dot).image = activeImage;
            } else {

                dot.backgroundColor = [UIColor colorWithPatternImage:activeImage];
            }
        } else {

            if ([dot isKindOfClass:UIImageView.class]) {

                ((UIImageView *)dot).image = inactiveImage;
            } else {

                dot.backgroundColor = [UIColor colorWithPatternImage:inactiveImage];
            }
        }
    }
}

- (void)setCurrentPage:(NSInteger)page {

    [super setCurrentPage:page];

    [self updateDots];
}

@end
