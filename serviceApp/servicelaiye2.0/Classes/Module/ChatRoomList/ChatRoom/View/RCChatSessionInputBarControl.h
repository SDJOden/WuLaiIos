//
//  RCChatSessionInputBarControl.h
//  RongIMKit
//
//  Created by xugang on 15/2/12.
//  Copyright (c) 2015年 RongCloud. All rights reserved.
//

#ifndef __RCChatSessionInputBarControl
#define __RCChatSessionInputBarControl
#import <UIKit/UIKit.h>
#import <RongIMLib/RongIMLib.h>
#import "RCTextView.h"
#define Height_ChatSessionInputBar /*90*/50

typedef NS_ENUM(NSInteger, RCChatSessionInputBarControlStyle) {

    RC_CHAT_INPUT_BAR_STYLE_SWITCH_CONTAINER_EXTENTION = 0,
};

typedef NS_ENUM(NSInteger, RCChatSessionInputBarControlType) {

    RCChatSessionInputBarControlDefaultType = 0,
};

typedef NS_ENUM(NSInteger, RCChatSessionInputBarInputType) {

    RCChatSessionInputBarInputText = 0,
};

@protocol RCChatSessionInputBarControlDelegate;

@interface RCChatSessionInputBarControl : UIView

@property(weak, nonatomic) id<RCChatSessionInputBarControlDelegate> delegate;

@property(strong, nonatomic) UIView *inputContainerView;

@property(strong, nonatomic) RCTextView *inputTextView;

@property(strong, nonatomic) UIButton *sugButton;

@property(strong, nonatomic) UIButton *pictureButton;

@property(strong, nonatomic) UIButton *cameraButton;

@property(strong, nonatomic) UIButton *voiceButton;

@property(strong, nonatomic) UIButton *recordButton;

@property(strong, nonatomic) UIButton *emojiButton;

@property(strong, nonatomic) UIButton *timeSugButton;

@property(strong, nonatomic) UIButton *locationButton;

#pragma mark - 新增发送按钮
@property(strong, nonatomic) UIButton *sendButton;

@property(assign, nonatomic) float currentPositionY; // currentPositionY 原始计算方式

@property(assign, nonatomic) float originalPositionY; // originalPositionY

@property(assign, nonatomic) float inputTextview_height; // inputTextview_height

// 初始化
- (id)initWithFrame:(CGRect)frame
    withContextView:(UIView *)contextView
               type:(RCChatSessionInputBarControlType)type
              style:(RCChatSessionInputBarControlStyle)style;

// 设置输入栏的样式 可以在viewdidload后，可以设置样式
- (void)setInputBarType:(RCChatSessionInputBarControlType)type style:(RCChatSessionInputBarControlStyle)style;

- (void)changeSize:(CGRect)rect;

- (void)changeChatSessionInputBarFrame;

@end

// delegate 回调
@protocol RCChatSessionInputBarControlDelegate <NSObject>

@optional

// 键盘frame事件
- (void)keyboardWillShowWithFrame:(CGRect)keyboardFrame;

//  键盘隐藏事件
- (void)keyboardWillHide;

// 输入框尺寸变化
- (void)chatSessionInputBarControlContentSizeChanged:(CGRect)frame;

#pragma mark - newSendBtn
- (void)didTouchKeyboardReturnKey:(RCChatSessionInputBarControl *)inputControl text:(NSString *)text;

- (void)didTouchSugButton:(UIButton *)sender;

- (void)didTouchSendButton:(UIButton *)sender;

- (void)didTouchEmojiButton:(UIButton *)sender;

- (void)didTouchPictureButton:(UIButton *)sender;

- (void)didTouchCameraButton:(UIButton *)sender;

- (void)didTouchTimeSugButton:(UIButton *)sender;

- (void)didTouchLocationButton:(UIButton *)sender;

- (void)didTouchRecordButon:(UIButton *)sender event:(UIControlEvents)event;

@end

#endif