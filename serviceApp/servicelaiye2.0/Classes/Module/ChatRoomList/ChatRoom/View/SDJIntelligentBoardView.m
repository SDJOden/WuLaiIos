//
//  SDJIntelligentBoardView.m
//  RongIMKit
//
//  Created by 盛东 on 15/10/12.
//  Copyright © 2015年 RongCloud. All rights reserved.
//

// 虚线是一张背景图, 自己建立UIImage的分类去绘制

#import "SDJIntelligentBoardView.h"

#import "SDJCommonSetting.h"
#import "RCKitUtility.h"

#define MAS_SHORTHAND_GLOBALS
#define MAS_SHORTHAND
#import "Masonry.h"

#define BTN_MAXROW 4
#define COLOR_F0DEDA [UIColor colorWithRed:236/255.0 green:233/255.0 blue:229/255.0 alpha:1.0]

#import "UIImage+SDJExtension.h"

// 不用自动布局

@interface SDJIntelligentBoardView ()

@property(nonatomic, assign) int btnTotal;

@property(nonatomic, strong) UIButton *selectedBtn;

@end

@implementation SDJIntelligentBoardView

- (instancetype)initWithFrame:(CGRect)frame andMessageArray:(NSArray *)array {
    
    if (self = [super initWithFrame:frame]) {
        
        // 界面初始化设置
        self.inteligentMessages = [[NSArray alloc] initWithArray:array];
        self.backgroundColor = COLOR_F0DEDA;
    }
    return self;
}

- (void)setInteligentMessages:(NSArray *)inteligentMessages {
    
    _inteligentMessages = [[NSArray alloc] initWithArray:inteligentMessages];
    
    [self loadBtnList];
}

- (void)loadBtnList {
    
    // 算页码
    if (nil == self.inteligentMessages ||  [self.inteligentMessages count] == 0) {
        self.btnTotal = 0;
    }else{
        self.btnTotal = (int)[self.inteligentMessages count];
    }
    
    // 设置按钮, 设置之前做一次清空
    for (UIButton *btn in self.subviews) {
        [btn removeFromSuperview];
    }
    
    int number = 0;
    
    CGFloat btn_height = (self.bounds.size.height - 2 * 27)/BTN_MAXROW;
    
    CGFloat btn_width = 0;
    
    CGFloat x = 15;
    
    CGFloat topX = 15;
    
    CGFloat topY = 15;
    
    CGFloat marginX = 10;
    
    CGFloat marginY = 12;
    
    CGFloat y;
    
    for (int i = 0; i < self.btnTotal; i++) {
        
        CGSize __textSize = CGSizeZero;
        if (IOS_FSystenVersion < 7.0) {
            __textSize = RC_MULTILINE_TEXTSIZE_LIOS7(self.inteligentMessages[i], [UIFont systemFontOfSize:15], CGSizeMake(self.bounds.size.width - 50, 8000), NSLineBreakByTruncatingTail);
        }else {
            __textSize = RC_MULTILINE_TEXTSIZE_GEIOS7(self.inteligentMessages[i], [UIFont systemFontOfSize:15], CGSizeMake(self.bounds.size.width - 50,8000));
        }
        __textSize = CGSizeMake(ceilf(__textSize.width), ceilf(__textSize.height));
        
        btn_width = __textSize.width + 12 + 12 + 5;
        
        if (x + marginX + btn_width >= [UIScreen mainScreen].bounds.size.width - topX) { // 下一个按钮超出, 那么这一个不能放在这一行
            x = topX;
            number++;
        }
        
        y = topY + (marginY + btn_height) * number; // 当前y
        
        if (number == 3) {
            break;
        }
        
        // 添加按钮
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(x, y, btn_width, btn_height)];
        [btn.titleLabel setFont:[UIFont systemFontOfSize:15]];
        [btn setBackgroundColor:COLOR_F0DEDA];
        btn.tag = 4200 + i;
        [btn setTitle:self.inteligentMessages[i] forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(btnClicked:) forControlEvents:UIControlEventTouchUpInside];
        [btn setTitleColor:HEXCOLOR(0x746c5e) forState:UIControlStateNormal];
        [btn setTitleColor:HEXCOLOR(0xffffff) forState:UIControlStateHighlighted];
        [btn setBackgroundImage:[UIImage drawSugButtonBackgroundImagewithSize:CGSizeMake(btn_width, btn_height) withWidth:1.0 withColor:HEXCOLOR(0x746C5E) withStatus:NO] forState:UIControlStateNormal];
        [btn setBackgroundImage:[UIImage drawSugButtonBackgroundImagewithSize:CGSizeMake(btn_width, btn_height) withWidth:1.0 withColor:HEXCOLOR(0x746C5E) withStatus:YES] forState:UIControlStateHighlighted];
        [self addSubview:btn];
        
        // 添加完按钮后修改x
        x = x + marginX + btn_width; // 下一个x
    }
}

- (void)btnClicked:(UIButton *)sender {
    
    if ([self.delegate respondsToSelector:@selector(didSelectedIntelligentBoardView:textSelected:withTag:)]) {
        [self.delegate didSelectedIntelligentBoardView:self textSelected:sender.titleLabel.text withTag:(int)sender.tag];
    }
}

@end