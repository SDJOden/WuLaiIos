//
//  RCConversationTableCell.m
//  RongIMKit
//
//  Created by xugang on 15/1/24.
//  Copyright (c) 2015年 RongCloud. All rights reserved.
//

#import "RCConversationCell.h"
#import "SDJCommonSetting.h"
#import "RCUserInfoLoader.h"
#import "RCKitUtility.h"
#import <RongIMLib/RongIMLib.h>
#import "RCIM.h"
#import "UIImageView+WebCache.h"

@interface RCConversationCell () <RCUserInfoLoaderObserver>

@property(nonatomic, strong) NSDictionary *cellSubViews;

- (void)layoutCellView;
- (void)setAutoLayout;
@end

@implementation RCConversationCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style
              reuseIdentifier:(NSString *)reuseIdentifier {
  self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
  if (self) {
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.enableNotification = YES;
    [self layoutCellView];
 }

  return self;
}

- (void)layoutCellView {
  self.headerImageViewBackgroundView = [[UIView alloc] init];
  self.headerImageViewBackgroundView.backgroundColor = [UIColor clearColor];

  self.headerImageView = [[UIImageView alloc]
      initWithFrame:CGRectMake(0, 0, [RCIM sharedRCIM]
                                         .globalConversationPortraitSize.width,
                               [RCIM sharedRCIM]
                                   .globalConversationPortraitSize.height)];
  self.headerImageView.layer.cornerRadius = 4;
  self.headerImageView.layer.masksToBounds = YES;
  self.headerImageView.contentMode = UIViewContentModeScaleAspectFill;
  self.headerImageView.image = IMAGE_BY_NAMED(@"default_portrait");
  self.headerImageView.userInteractionEnabled = YES;
  UITapGestureRecognizer *portraitTap = [[UITapGestureRecognizer alloc]
      initWithTarget:self
              action:@selector(tapUserPortaitEvent:)];

  [self.headerImageView addGestureRecognizer:portraitTap];
    UILongPressGestureRecognizer *portraitLongPress =
    [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressUserPortaitEvent:)];
    [self.headerImageView addGestureRecognizer:portraitLongPress];
  [self setHeaderImagePortraitStyle:[RCIM sharedRCIM]
                                        .globalMessageAvatarStyle];
  [self.headerImageViewBackgroundView addSubview:self.headerImageView];

  self.conversationTitle = [[UILabel alloc] init];
  self.conversationTitle.backgroundColor = [UIColor clearColor];
  self.conversationTitle.font =
      [UIFont boldSystemFontOfSize:16]; //[UIFont fontWithName:@"Heiti SC-Bold"
  // size:16];
  self.conversationTitle.textColor = HEXCOLOR(0x252525);

  self.messageContentLabel = [[UILabel alloc] init];
  self.messageContentLabel.backgroundColor = [UIColor clearColor];
  self.messageContentLabel.font = [UIFont systemFontOfSize:14];
  self.messageContentLabel.textColor = HEXCOLOR(0x8c8c8c);

  self.messageCreatedTimeLabel = [[UILabel alloc] init];
  self.messageCreatedTimeLabel.backgroundColor = [UIColor clearColor];
  self.messageCreatedTimeLabel.font = [UIFont systemFontOfSize:14];
  self.messageCreatedTimeLabel.textColor = [UIColor lightGrayColor];
  self.messageCreatedTimeLabel.textAlignment = NSTextAlignmentRight;

  self.conversationStatusImageView = [[UIImageView alloc] init];
  self.conversationStatusImageView.backgroundColor = [UIColor clearColor];
  self.conversationStatusImageView.image =
      IMAGE_BY_NAMED(@"block_notification");

  self.bubbleTipView = nil;

  self.headerImageViewBackgroundView.translatesAutoresizingMaskIntoConstraints =
      NO;
  self.conversationTitle.translatesAutoresizingMaskIntoConstraints = NO;
  self.messageContentLabel.translatesAutoresizingMaskIntoConstraints = NO;
  self.messageCreatedTimeLabel.translatesAutoresizingMaskIntoConstraints = NO;
  self.conversationStatusImageView.translatesAutoresizingMaskIntoConstraints =
      NO;

  [self.contentView addSubview:self.headerImageViewBackgroundView];
  [self.contentView addSubview:self.conversationTitle];
  [self.contentView addSubview:self.messageContentLabel];
  [self.contentView addSubview:self.messageCreatedTimeLabel];
  [self.contentView addSubview:self.conversationStatusImageView];

  self.cellSubViews = NSDictionaryOfVariableBindings(
      _headerImageViewBackgroundView, _conversationTitle, _messageContentLabel,
      _messageCreatedTimeLabel, _conversationStatusImageView);
  [self setAutoLayout];
}
- (void)tapUserPortaitEvent:(UIGestureRecognizer *)gestureRecognizer {
  __weak typeof(&*self) weakSelf = self;
  if ([self.delegate respondsToSelector:@selector(didTapCellPortrait:)]) {
    [self.delegate didTapCellPortrait:weakSelf.model];
  }
}

- (void)longPressUserPortaitEvent:(UIGestureRecognizer *)gestureRecognizer {
     __weak typeof(&*self) weakSelf = self;
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        if ([self.delegate respondsToSelector:@selector(didLongPressCellPortrait:)]) {
            [self.delegate didLongPressCellPortrait:weakSelf.model];
        }
    }
}
- (void)setAutoLayout {
  [self.contentView
      addConstraints:
          [NSLayoutConstraint
              constraintsWithVisualFormat:
                  @"H:|-13-[_headerImageViewBackgroundView(width)]-8-[_"
              @"conversationTitle]-5-[_messageCreatedTimeLabel(==80)]-9-"
              @"|" options:0 metrics:@{
                @"width" :
                    @([RCIM sharedRCIM].globalConversationPortraitSize.width)
              } views:self.cellSubViews]];
  [self.contentView
      addConstraints:
          [NSLayoutConstraint constraintsWithVisualFormat:
                                  @"V:|-10-[_headerImageViewBackgroundView]-10-"
                                  @"|" options:0 metrics:@{
            @"height" :
                @([RCIM sharedRCIM].globalConversationPortraitSize.height)
          } views:self.cellSubViews]];

  [self.contentView
      addConstraints:
          [NSLayoutConstraint
              constraintsWithVisualFormat:@"V:|-11-[_conversationTitle]-0-["
              @"_messageContentLabel]-10-|"
                                  options:0
                                  metrics:nil
                                    views:self.cellSubViews]];
  [self.contentView
      addConstraints:
          [NSLayoutConstraint
              constraintsWithVisualFormat:@"H:[_messageContentLabel]-28-|"
                                  options:0
                                  metrics:nil
                                    views:self.cellSubViews]];

  [self.contentView
      addConstraint:[NSLayoutConstraint constraintWithItem:_messageContentLabel
                                                 attribute:NSLayoutAttributeLeft
                                                 relatedBy:NSLayoutRelationEqual
                                                    toItem:_conversationTitle
                                                 attribute:NSLayoutAttributeLeft
                                                multiplier:1
                                                  constant:0]];

  [self.contentView
      addConstraints:[NSLayoutConstraint
                         constraintsWithVisualFormat:
                             @"V:|-11-[_messageCreatedTimeLabel(20)]"
                                             options:0
                                             metrics:nil
                                               views:self.cellSubViews]];

  [self.contentView
      addConstraints:[NSLayoutConstraint
                         constraintsWithVisualFormat:
                             @"H:[_conversationStatusImageView]-10-|"
                                             options:0
                                             metrics:nil
                                               views:self.cellSubViews]];
  [self.contentView
      addConstraint:[NSLayoutConstraint
                        constraintWithItem:_conversationStatusImageView
                                 attribute:NSLayoutAttributeTop
                                 relatedBy:NSLayoutRelationEqual
                                    toItem:_messageCreatedTimeLabel
                                 attribute:NSLayoutAttributeBottom
                                multiplier:1
                                  constant:5]];
}

- (void)setHeaderImagePortraitStyle:(RCUserAvatarStyle)portraitStyle {
  _portraitStyle = portraitStyle;
  if (_portraitStyle == RC_USER_AVATAR_RECTANGLE) {
    self.headerImageView.layer.cornerRadius = [[RCIM sharedRCIM] portraitImageViewCornerRadius];
  } else if (_portraitStyle == RC_USER_AVATAR_CYCLE) {
    self.headerImageView.layer.cornerRadius =
        [[RCIM sharedRCIM] globalConversationPortraitSize].height / 2;
  }
}

- (void)clearPreCellInfo {
  self.conversationTitle.text = nil;
  self.messageCreatedTimeLabel.text = nil;
  self.messageContentLabel.text = nil;
}

- (void)setDiscussionData:(RCDiscussion *)discussion
                    model:(RCConversationModel *)model {
  __weak typeof(&*self) __bloackself = self;
  if (discussion) {
    [__bloackself.conversationTitle setText:discussion.discussionName];
    __bloackself.model.conversationTitle = [_conversationTitle.text copy];
  }

  if (__bloackself.model.draft && __bloackself.model.draft.length > 0) {

    [__bloackself.messageContentLabel
        setText:NSLocalizedStringFromTable(@"Draft", @"RongCloudKit", nil)];
  } else {
    RCUserInfo *userInfo =
        [[RCUserInfoLoader sharedUserInfoLoader] loadUserInfo:model.senderUserId
                                                     observer:self];
    if (userInfo) {
      [self.messageContentLabel
          setText:[NSString
                      stringWithFormat:
                          @"%@:%@", userInfo.name,
                          [RCKitUtility formatMessage:model.lastestMessage]]];
    } else {
      [self.messageContentLabel
          setText:[RCKitUtility formatMessage:model.lastestMessage]];
    }
  }
  self.messageCreatedTimeLabel.text =
      [RCKitUtility ConvertChatMessageTime:__bloackself.model.sentTime / 1000];
}

- (void)setDataModel:(RCConversationModel *)model {

  [self clearPreCellInfo];
  self.model = model;

  if (self.enableNotification) {
    // notify, hidden
    self.conversationStatusImageView.hidden = YES;
  } else {
    // Not disturb, show
    self.conversationStatusImageView.hidden = NO;
  }
  if (self.model.isTop) {
    [self.contentView
        setBackgroundColor:self.topCellBackgroundColor];
  } else {
    [self.contentView setBackgroundColor:self.cellBackgroundColor];
  }
  //修改草稿颜色
  if (model.draft && [model.draft length]) {
    [self.messageContentLabel setTextColor:[UIColor colorWithRed:204 / 255.f
                                                           green:33 / 255.f
                                                            blue:33 / 255.f
                                                           alpha:1]];
  } else {
    self.messageContentLabel.textColor = HEXCOLOR(0x8c8c8c);
  }
  if (model.conversationModelType == RC_CONVERSATION_MODEL_TYPE_NORMAL) {
    //个人聊天，或者系统，客服
    if (model.conversationType == ConversationType_SYSTEM ||
        model.conversationType == ConversationType_PRIVATE ||
        model.conversationType == ConversationType_CUSTOMERSERVICE) {

      [[RCUserInfoLoader sharedUserInfoLoader] removeObserver:self];
      RCUserInfo *userInfo =
          [[RCUserInfoLoader sharedUserInfoLoader] loadUserInfo:model.targetId
                                                       observer:self];

      if (userInfo) {
          [self.headerImageView sd_setImageWithURL:[NSURL URLWithString:userInfo.portraitUri] placeholderImage:IMAGE_BY_NAMED(@"default_portrait_msg")];

        [self.conversationTitle setText:userInfo.name];
        self.model.conversationTitle = [_conversationTitle.text copy];
      }

      if (model.draft && model.draft.length > 0) {
        //[self.messageContentLabel setText:model.draft];
        [self.messageContentLabel
            setText:NSLocalizedStringFromTable(@"Draft", @"RongCloudKit", nil)];

      } else {
        [self.messageContentLabel
            setText:[RCKitUtility formatMessage:model.lastestMessage]];
      }
      self.messageCreatedTimeLabel.text =
          [RCKitUtility ConvertChatMessageTime:model.sentTime / 1000];
    }
    //群组
      if (model.draft && model.draft.length > 0) {
        [self.messageContentLabel
            setText:NSLocalizedStringFromTable(@"Draft", @"RongCloudKit", nil)];
      } else {
        RCUserInfo *userInfo = [[RCUserInfoLoader sharedUserInfoLoader]
            loadUserInfo:model.senderUserId
                observer:self];

        /**
         *  show the cache value if userInfo is NOT nil, or load them from
         * datasource
         */
        if (userInfo) {
          [self.messageContentLabel
              setText:[NSString stringWithFormat:
                                    @"%@:%@", userInfo.name,
                                    [RCKitUtility
                                        formatMessage:model.lastestMessage]]];
        } else {
          [self.messageContentLabel
              setText:[RCKitUtility formatMessage:model.lastestMessage]];
        }
      }

      self.messageCreatedTimeLabel.text =
          [RCKitUtility ConvertChatMessageTime:model.sentTime / 1000];
    }

  else if (model.conversationType == ConversationType_DISCUSSION) {
      __weak typeof(&*self) __bloackself = self;

      [[RCIMClient sharedRCIMClient] getDiscussion:model.targetId
          success:^(RCDiscussion *discussion) {
            NSLog(@"isMainThread > %d", [NSThread isMainThread]);
            if ([NSThread isMainThread]) {
              [self setDiscussionData:discussion model:model];
            } else {
              dispatch_async(dispatch_get_main_queue(), ^{
                [self setDiscussionData:discussion model:model];
              });
            }

          }
          error:^(RCErrorCode status) {
              __weak typeof(&*self) __bloackself = self;

              if ([NSThread isMainThread]) {
                  [__bloackself.conversationTitle setText:NSLocalizedString(@"DISCUSSION", @"")];
                  __bloackself.model.conversationTitle = [_conversationTitle.text copy];
              } else {
                  dispatch_async(dispatch_get_main_queue(), ^{
                      [__bloackself.conversationTitle setText:NSLocalizedString(@"DISCUSSION", @"")];
                      __bloackself.model.conversationTitle = [_conversationTitle.text copy];
                  });
              }
              
          }];

  } else if (model.conversationModelType ==
             RC_CONVERSATION_MODEL_TYPE_COLLECTION) {
    //还需要做图
    if (model.conversationType == ConversationType_PRIVATE) {
//      [self.headerImageView
//          setPlaceholderImage:IMAGE_BY_NAMED(@"default_portrait")];
      [self.conversationTitle
          setText:NSLocalizedStringFromTable(
                      @"conversation_private_collection_title", @"RongCloudKit",
                      nil)];
      self.model.conversationTitle = [_conversationTitle.text copy];
    } else if (model.conversationType == ConversationType_SYSTEM) {
//      [self.headerImageView
//          setPlaceholderImage:IMAGE_BY_NAMED(@"default_portrait")];
      [self.conversationTitle
          setText:NSLocalizedStringFromTable(
                      @"conversation_systemMessage_collection_title",
                      @"RongCloudKit", nil)];
      self.model.conversationTitle = [_conversationTitle.text copy];
    } else if (model.conversationType == ConversationType_CUSTOMERSERVICE) {
//      [self.headerImageView
//          setPlaceholderImage:IMAGE_BY_NAMED(@"portrait_kefu")];
      [self.conversationTitle
          setText:NSLocalizedStringFromTable(
                      @"conversation_customer_collection_title",
                      @"RongCloudKit", nil)];
      self.model.conversationTitle = [_conversationTitle.text copy];
    } else if (model.conversationType == ConversationType_DISCUSSION) {
//      [self.headerImageView
//          setPlaceholderImage:IMAGE_BY_NAMED(
//                                  @"default_discussion_collection_portrait")];
      [self.conversationTitle
          setText:NSLocalizedStringFromTable(
                      @"conversation_discussion_collection_title",
                      @"RongCloudKit", nil)];
      self.model.conversationTitle = [_conversationTitle.text copy];
    } else if (model.conversationType == ConversationType_GROUP) {
//      [self.headerImageView
//          setPlaceholderImage:IMAGE_BY_NAMED(@"default_collection_portrait")];
      [self.conversationTitle
          setText:NSLocalizedStringFromTable(
                      @"conversation_group_collection_title", @"RongCloudKit",
                      nil)];
      self.model.conversationTitle = [_conversationTitle.text copy];
    }

    // 统一设置
    if (model.draft && model.draft.length > 0) {
      //[self.messageContentLabel setText:model.draft];
      [self.messageContentLabel
          setText:NSLocalizedStringFromTable(@"Draft", @"RongCloudKit", nil)];
    } else {
      if (model.conversationType == ConversationType_GROUP) {

      } else if (model.conversationType == ConversationType_DISCUSSION) {
          [self.messageContentLabel
           setText:[RCKitUtility formatMessage:model.lastestMessage]];
        [[RCIMClient sharedRCIMClient] getDiscussion:model.targetId
            success:^(RCDiscussion *discussion) {
              dispatch_async(dispatch_get_main_queue(), ^{
                [self.messageContentLabel
                    setText:[NSString
                                stringWithFormat:
                                    @"%@:%@", discussion.discussionName,
                                    [RCKitUtility
                                        formatMessage:model.lastestMessage]]];
              });
            }
            error:^(RCErrorCode status) {
              dispatch_async(dispatch_get_main_queue(), ^{
                [self.messageContentLabel
                    setText:[RCKitUtility formatMessage:model.lastestMessage]];
              });
            }];

      } else {
        [self.messageContentLabel
            setText:[RCKitUtility formatMessage:model.lastestMessage]];
      }
    }
    self.messageCreatedTimeLabel.text =
        [RCKitUtility ConvertChatMessageTime:model.sentTime / 1000];

  } else if (model.conversationModelType ==
             RC_CONVERSATION_MODEL_TYPE_CUSTOMIZATION) {
  } else if (model.conversationModelType ==
             RC_CONVERSATION_MODEL_TYPE_PUBLIC_SERVICE) {

    RCPublicServiceProfile *serviceProfile = [[RCIMClient sharedRCIMClient]
        getPublicServiceProfile:(RCPublicServiceType)model.conversationType
                publicServiceId:model.targetId];


    if (model.draft && model.draft.length > 0) {
      //[self.messageContentLabel setText:model.draft];
      [self.messageContentLabel
          setText:NSLocalizedStringFromTable(@"Draft", @"RongCloudKit", nil)];

    } else {
      [self.messageContentLabel
          setText:[RCKitUtility formatMessage:model.lastestMessage]];
    }
    self.messageCreatedTimeLabel.text =
        [RCKitUtility ConvertChatMessageTime:model.sentTime / 1000];
  }

  if (nil == self.bubbleTipView) {
    self.bubbleTipView = [[RCMessageBubbleTipView alloc]
        initWithParentView:self.headerImageViewBackgroundView
                 alignment:RC_MESSAGE_BUBBLE_TIP_VIEW_ALIGNMENT_TOP_RIGHT];
  }
  [self.bubbleTipView setBubbleTipNumber:(int)model.unreadMessageCount];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
  [super setSelected:selected animated:animated];

  // Configure the view for the selected state
}

- (void)userInfoDidLoad:(NSNotification *)notification {
  __weak typeof(&*self) __blockSelf = self;

  RCUserInfo *userInfo = notification.object;
  dispatch_async(dispatch_get_main_queue(), ^{
    if (userInfo) {
      if (__blockSelf.model.conversationType !=ConversationType_DISCUSSION && __blockSelf.model.conversationType !=ConversationType_GROUP) {
          [__blockSelf.headerImageView sd_setImageWithURL:[NSURL URLWithString:userInfo.portraitUri] placeholderImage:IMAGE_BY_NAMED(@"default_portrait_msg")];
        [__blockSelf.conversationTitle setText:userInfo.name];
        __blockSelf.model.conversationTitle = [_conversationTitle.text copy];
      }
    }

    if (__blockSelf.model.draft && __blockSelf.model.draft.length > 0) {
      //[__blockSelf.messageContentLabel setText:__blockSelf.model.draft];
      [__blockSelf.messageContentLabel
          setText:NSLocalizedStringFromTable(@"Draft", @"RongCloudKit", nil)];
    } else {
      //r如果当前是群组或者讨论组，更新messageContentLabel为  “name:message”格式
      if (__blockSelf.model.conversationType == ConversationType_DISCUSSION||__blockSelf.model.conversationType == ConversationType_GROUP) {
        if (userInfo) {
          [__blockSelf.messageContentLabel
              setText:[NSString stringWithFormat:
                                    @"%@:%@", userInfo.name,
                                    [RCKitUtility
                                        formatMessage:__blockSelf.model
                                                          .lastestMessage]]];

        } else {
          [__blockSelf.messageContentLabel
              setText:[RCKitUtility
                          formatMessage:__blockSelf.model.lastestMessage]];
        }
      }
    }

    self.messageCreatedTimeLabel.text =
        [RCKitUtility ConvertChatMessageTime:__blockSelf.model.sentTime / 1000];
  });
}

- (void)userInfoFailToLoad:(NSNotification *)notification {
  __weak typeof(&*self) __blockSelf = self;

  RCUserInfo *userInfo = notification.object;
  dispatch_async(dispatch_get_main_queue(), ^{
      if (__blockSelf.model.conversationType !=ConversationType_DISCUSSION && __blockSelf.model.conversationType !=ConversationType_GROUP)
      {
          if (userInfo) {
              [__blockSelf.headerImageView sd_setImageWithURL:[NSURL URLWithString:userInfo.portraitUri] placeholderImage:IMAGE_BY_NAMED(@"default_portrait_msg")];
              [__blockSelf.conversationTitle setText:userInfo.name];
              __blockSelf.model.conversationTitle = [_conversationTitle.text copy];
          }
      }

    if (__blockSelf.model.draft && __blockSelf.model.draft.length > 0) {
      //[__blockSelf.messageContentLabel setText:__blockSelf.model.draft];
      [__blockSelf.messageContentLabel
          setText:NSLocalizedStringFromTable(@"Draft", @"RongCloudKit", nil)];
    } else {
      [self.messageContentLabel
          setText:[RCKitUtility
                      formatMessage:__blockSelf.model.lastestMessage]];
    }

    self.messageCreatedTimeLabel.text =
        [RCKitUtility ConvertChatMessageTime:__blockSelf.model.sentTime / 1000];
  });
}

//#pragma mark, No cache, fetch group info from database
//- (void)groupDidLoad:(NSNotification *)notification {
//  __weak typeof(&*self) __blockSelf = self;
//
//  RCGroup *groupInfo = notification.object;
//
//  dispatch_async(dispatch_get_main_queue(), ^{
//    if (groupInfo) {
//      if (__blockSelf.model.conversationModelType !=
//          RC_CONVERSATION_MODEL_TYPE_COLLECTION) {
//        [__blockSelf.headerImageView
//            setImageURL:[NSURL URLWithString:groupInfo.portraitUri]];
//        [__blockSelf.conversationTitle setText:groupInfo.groupName];
//        __blockSelf.model.conversationTitle = [_conversationTitle.text copy];
//      }
//    }
//
//    if (__blockSelf.model.draft && __blockSelf.model.draft.length > 0) {
//      //[__blockSelf.messageContentLabel setText:__blockSelf.model.draft];
//      [__blockSelf.messageContentLabel
//          setText:NSLocalizedStringFromTable(@"Draft", @"RongCloudKit", nil)];
//    } else {
//      if ((__blockSelf.model.conversationModelType !=
//          RC_CONVERSATION_MODEL_TYPE_COLLECTION )&&__blockSelf.model.conversationType != ConversationType_PRIVATE) {
//        RCUserInfo *userInfo = [[RCUserInfoLoader sharedUserInfoLoader]
//            loadUserInfo:__blockSelf.model.senderUserId
//                observer:self];
//        /**
//         *  show the cache value if userInfo is NOT nil, or load them from
//         * datasource
//         */
//        if (userInfo) {
//          [__blockSelf.messageContentLabel
//              setText:[NSString stringWithFormat:
//                                    @"%@:%@", userInfo.name,
//                                    [RCKitUtility
//                                        formatMessage:__blockSelf.model
//                                                          .lastestMessage]]];
//        } else {
//          [__blockSelf.messageContentLabel
//              setText:[RCKitUtility
//                          formatMessage:__blockSelf.model.lastestMessage]];
//        }
//      } else {
//        if (groupInfo) {
//          [self.messageContentLabel
//              setText:[NSString stringWithFormat:
//                                    @"%@:%@", groupInfo.groupName,
//                                    [RCKitUtility
//                                        formatMessage:__blockSelf.model
//                                                          .lastestMessage]]];
//        } else {
//          [self.messageContentLabel
//              setText:[RCKitUtility
//                          formatMessage:__blockSelf.model.lastestMessage]];
//        }
//      }
//
//      //[__blockSelf.messageContentLabel setText:[RCKitUtility
//      // formatMessage:__blockSelf.model.lastestMessage]];
//    }
//    __blockSelf.messageCreatedTimeLabel.text =
//        [RCKitUtility ConvertMessageTime:__blockSelf.model.sentTime / 1000];
//  });
//}
//- (void)groupFailToLoad:(NSNotification *)notification {
//  DebugLog(@"[RongIMKit]: %s", __FUNCTION__);
//  __weak typeof(&*self) __blockSelf = self;
//
//  RCGroup *groupInfo = notification.object;
//
//  dispatch_async(dispatch_get_main_queue(), ^{
//    if (groupInfo) {
//      [__blockSelf.headerImageView
//          setImageURL:[NSURL URLWithString:groupInfo.portraitUri]];
//      [__blockSelf.conversationTitle setText:groupInfo.groupName];
//      __blockSelf.model.conversationTitle = [_conversationTitle.text copy];
//    }
//
//    if (__blockSelf.model.draft && __blockSelf.model.draft.length > 0) {
//      [__blockSelf.messageContentLabel
//          setText:NSLocalizedStringFromTable(@"Draft", @"RongCloudKit", nil)];
//    } else {
//      [__blockSelf.messageContentLabel
//          setText:[RCKitUtility
//                      formatMessage:__blockSelf.model.lastestMessage]];
//    }
//    __blockSelf.messageCreatedTimeLabel.text =
//        [RCKitUtility ConvertMessageTime:__blockSelf.model.sentTime / 1000];
//  });
//}
@end
