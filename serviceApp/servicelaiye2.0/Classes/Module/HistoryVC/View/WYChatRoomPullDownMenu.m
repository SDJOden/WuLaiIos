//
//  WYChatRoomPullDownMenu.m
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/7/5.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import "WYChatRoomPullDownMenu.h"
#import "WYConnectListModel.h"

@interface WYChatRoomPullDownMenu ()

@property (nonatomic, strong) UIView *userIconView;
@property (nonatomic, strong) UILabel *userNameTitleLabel;
@property (nonatomic, strong) UILabel *userNameLabel;
@property (nonatomic, strong) UILabel *xiaoLaiNickNameTitleLabel;
@property (nonatomic, strong) UILabel *xiaoLaiNickNameLabel;
@property (nonatomic, strong) UILabel *userPhoneTitleLabel;
@property (nonatomic, strong) UILabel *userPhoneLabel;
@property (nonatomic, strong) UILabel *cityTitleLabel;
@property (nonatomic, strong) UILabel *cityLabel;
@property (nonatomic, strong) UILabel *userRemarksTitleLabel;
@property (nonatomic, strong) UIView *userRemarksView;

@end

@implementation WYChatRoomPullDownMenu

-(instancetype)init{
    
    if (self = [super init]) {
        [self rcinit];
    }
    return self;
}

-(void)setModel:(WYConnectListModel *)model{
    
    _model = model;
    self.userNameLabel.text = model.nickname;
    self.userPhoneLabel.text = model.phone;
    self.cityLabel.text = model.province;
    
    [self.userNameLabel sizeToFit];
    [self.userPhoneLabel sizeToFit];
    [self.cityLabel sizeToFit];
    
    [self addSubViews];
    CGFloat x = 0;
    for (NSString *tag in model.up_tags/*@[@"potential", @"hq", @"debt", @"new", @"scar", @"金卡", @"白金卡", @"白金砖石卡"]*/) {
        UILabel *label = [[UILabel alloc]init];
        label.text = tag;
        label.font = [UIFont systemFontOfSize:12];
        label.backgroundColor = [UIColor colorWithRed:0.0019 green:0.6583 blue:0.0029 alpha:1.0];
        label.textAlignment = NSTextAlignmentCenter;
        label.textColor = [UIColor whiteColor];
        [label.layer setMasksToBounds:YES];
        [label.layer setCornerRadius:2.0]; //设置矩圆角半径
        label.frame = CGRectMake(x, 0, 20, 20);
        if ([tag isEqualToString:@"potential"]) {
            label.text = @"潜";
        }
        else if ([tag isEqualToString:@"hq"]) {
            label.text = @"优";
        }
        else if ([tag isEqualToString:@"debt"]) {
            label.text = @"欠";
            label.backgroundColor = [UIColor orangeColor];
        }
        else if ([tag isEqualToString:@"new"]) {
            label.text = @"新";
        }
        else if ([tag isEqualToString:@"scar"]) {
            label.text = @"包";
        }
        else {
            label.text = tag;
            [label sizeToFit];
            label.frame = CGRectMake(x, 0, label.w, 20);
        }
        x += label.w+2;
        [self.userIconView addSubview:label];
    }
}

-(void)rcinit{
    
    self.backgroundColor = HEXCOLOR(0xffffff);
}

-(void)addSubViews{
    
    [self.userIconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(15);
        make.left.equalTo(10);
        make.right.equalTo(-10);
        make.height.equalTo(20);
    }];
    [self.userNameTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.userIconView.bottom).offset(10);
        make.left.equalTo(10);
        make.width.equalTo(70);
        make.height.equalTo(20);
    }];
    [self.userNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.userNameTitleLabel.top);
        make.left.equalTo(self.userNameTitleLabel.right);
        make.right.equalTo(-[UIScreen mainScreen].bounds.size.width*0.5-10);
        make.height.equalTo(20);
    }];
    [self.xiaoLaiNickNameTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.userNameTitleLabel.top);
        make.left.equalTo([UIScreen mainScreen].bounds.size.width*0.6);
        make.height.equalTo(20);
    }];
    [self.xiaoLaiNickNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.userNameTitleLabel.top);
        make.left.equalTo(self.xiaoLaiNickNameTitleLabel.right);
        make.right.equalTo(-10);
        make.height.equalTo(20);
    }];
    [self.userPhoneTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.userNameTitleLabel.bottom).offset(10);
        make.left.equalTo(10);
        make.height.equalTo(20);
    }];
    [self.userPhoneLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.userPhoneTitleLabel.top);
        make.left.equalTo(self.userPhoneTitleLabel.right);
        make.right.equalTo(-[UIScreen mainScreen].bounds.size.width*0.5-10);
        make.height.equalTo(20);
    }];
    [self.cityTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.userPhoneTitleLabel.top);
        make.left.equalTo([UIScreen mainScreen].bounds.size.width*0.6);
        make.height.equalTo(20);
    }];
    [self.cityLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.userPhoneTitleLabel.top);
        make.left.equalTo(self.cityTitleLabel.right);
        make.right.equalTo(-10);
        make.height.equalTo(20);
    }];
    [self.userRemarksTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.userPhoneTitleLabel.bottom).offset(10);
        make.left.equalTo(10);
        make.height.equalTo(20);
    }];
    [self.userRemarksView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.userRemarksTitleLabel.top);
        make.left.equalTo(self.userRemarksTitleLabel.right);
        make.right.equalTo(-10);
        make.height.equalTo(20);
    }];
}

#pragma mark --- 懒加载
- (UIView *)userIconView {
    
	if(_userIconView == nil) {
		_userIconView = [[UIView alloc] init];
        [self addSubview:_userIconView];
	}
	return _userIconView;
}

- (UILabel *)userNameTitleLabel {
    
	if(_userNameTitleLabel == nil) {
        _userNameTitleLabel = [[UILabel alloc] init];
        _userNameTitleLabel.text = @"用户姓名：";
        _userNameTitleLabel.textColor = HEXCOLOR(0xA3A3A3);
        _userNameTitleLabel.font = [UIFont systemFontOfSize:14];
        [self addSubview:_userNameTitleLabel];
	}
	return _userNameTitleLabel;
}

- (UILabel *)userNameLabel {
    
	if(_userNameLabel == nil) {
        _userNameLabel = [[UILabel alloc] init];
        _userNameLabel.textColor = HEXCOLOR(0x575757);
        _userNameLabel.text = @"小来微信";
        _userNameLabel.font = [UIFont systemFontOfSize:14];
        [self addSubview:_userNameLabel];
	}
	return _userNameLabel;
}

- (UILabel *)xiaoLaiNickNameTitleLabel {
    
	if(_xiaoLaiNickNameTitleLabel == nil) {
        _xiaoLaiNickNameTitleLabel = [[UILabel alloc] init];
        _xiaoLaiNickNameTitleLabel.text = @"用户昵称：";
        _xiaoLaiNickNameTitleLabel.textColor = HEXCOLOR(0xA3A3A3);
        _xiaoLaiNickNameTitleLabel.font = [UIFont systemFontOfSize:14];
        [self addSubview:_xiaoLaiNickNameTitleLabel];
	}
	return _xiaoLaiNickNameTitleLabel;
}

- (UILabel *)xiaoLaiNickNameLabel {
    
	if(_xiaoLaiNickNameLabel == nil) {
		_xiaoLaiNickNameLabel = [[UILabel alloc] init];
        _xiaoLaiNickNameLabel.text = @"";
        _xiaoLaiNickNameLabel.textColor = HEXCOLOR(0x575757);
        _xiaoLaiNickNameLabel.font = [UIFont systemFontOfSize:14];
        [self addSubview:_xiaoLaiNickNameLabel];
	}
	return _xiaoLaiNickNameLabel;
}

- (UILabel *)userPhoneTitleLabel {
    
	if(_userPhoneTitleLabel == nil) {
        _userPhoneTitleLabel = [[UILabel alloc] init];
        _userPhoneTitleLabel.text = @"用户手机：";
        _userPhoneTitleLabel.textColor = HEXCOLOR(0xA3A3A3);
        _userPhoneTitleLabel.font = [UIFont systemFontOfSize:14];
        [self addSubview:_userPhoneTitleLabel];
	}
	return _userPhoneTitleLabel;
}

- (UILabel *)userPhoneLabel {
    
	if(_userPhoneLabel == nil) {
		_userPhoneLabel = [[UILabel alloc] init];
        _userPhoneLabel.textColor = HEXCOLOR(0x575757);
        _userPhoneLabel.text = @"13000000000";
        _userPhoneLabel.font = [UIFont systemFontOfSize:14];
        [self addSubview:_userPhoneLabel];
	}
	return _userPhoneLabel;
}

- (UILabel *)cityTitleLabel {
    
	if(_cityTitleLabel == nil) {
		_cityTitleLabel = [[UILabel alloc] init];
        _cityTitleLabel.text = @"所在城市：";
        _cityTitleLabel.textColor = HEXCOLOR(0xA3A3A3);
        _cityTitleLabel.font = [UIFont systemFontOfSize:14];
        [self addSubview:_cityTitleLabel];
	}
	return _cityTitleLabel;
}

- (UILabel *)cityLabel {
    
	if(_cityLabel == nil) {
		_cityLabel = [[UILabel alloc] init];
        _cityLabel.text = @"广州";
        _cityLabel.textColor = HEXCOLOR(0x575757);
        _cityLabel.font = [UIFont systemFontOfSize:14];
        [self addSubview:_cityLabel];
	}
	return _cityLabel;
}

- (UILabel *)userRemarksTitleLabel {
    
	if(_userRemarksTitleLabel == nil) {
		_userRemarksTitleLabel = [[UILabel alloc] init];
        _userRemarksTitleLabel.text = @"用户标签：";
        _userRemarksTitleLabel.textColor = HEXCOLOR(0xA3A3A3);
        _userRemarksTitleLabel.font = [UIFont systemFontOfSize:14];
        [self addSubview:_userRemarksTitleLabel];
	}
	return _userRemarksTitleLabel;
}

- (UIView *)userRemarksView {
    
	if(_userRemarksView == nil) {
		_userRemarksView = [[UIView alloc] init];
        [self addSubview:_userRemarksView];
	}
	return _userRemarksView;
}

@end
