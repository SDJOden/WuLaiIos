//
//  WYCalendarView.h
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/4/12.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol WYCalendarViewDelegate <NSObject>

-(void)closeCalendarView:(NSString *)dateStr;

@end

@interface WYCalendarView : UIView

@property (nonatomic, weak) id <WYCalendarViewDelegate> calendarViewdelegate;

@end
