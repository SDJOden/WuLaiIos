//
//  WYCalendarView.m
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/4/12.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import "WYCalendarView.h"
#import "FDCalendar.h"

@interface WYCalendarView ()

@property (nonatomic, strong) UIView *backgroundView;

@property (nonatomic, strong) UILabel *titleLabel;

@property (nonatomic, strong) UIButton *exitBtn;

@property (nonatomic, strong) FDCalendar *calendarView;

@end

@implementation WYCalendarView

- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize {
    
    self.backgroundColor = [UIColor whiteColor];
    
    [self.backgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self);
        make.height.equalTo(64);
    }];
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.backgroundView).offset(-10);
        make.centerX.equalTo(self);
    }];
    
    [self.exitBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.backgroundView).offset(-5);
        make.right.equalTo(self.backgroundView).offset(-10);
    }];
    
    [self.calendarView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.backgroundView.bottom);
        make.left.right.equalTo(self);
        make.bottom.equalTo(self);
    }];
}

- (UIView *)backgroundView {
    
	if(_backgroundView == nil) {
		_backgroundView = [[UIView alloc] init];
        _backgroundView.backgroundColor = NAVIGATIONBAR_BACKGROUDCOLOR;
        [self addSubview:_backgroundView];
	}
	return _backgroundView;
}

- (UILabel *)titleLabel {
    
	if(_titleLabel == nil) {
		_titleLabel = [[UILabel alloc] init];
        _titleLabel.text = @"日历";
        _titleLabel.font = [UIFont systemFontOfSize:16];
        [_titleLabel sizeToFit];
        _titleLabel.textColor = [UIColor whiteColor];
        [self.backgroundView addSubview:_titleLabel];
	}
	return _titleLabel;
}

- (UIButton *)exitBtn {
    
	if(_exitBtn == nil) {
		_exitBtn = [UIButton buttonWithType:UIButtonTypeSystem];
        [_exitBtn setTitle:@"关闭" forState:UIControlStateNormal];
        [_exitBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _exitBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        [_exitBtn sizeToFit];
        [_exitBtn addTarget:self action:@selector(exitCalendarView) forControlEvents:UIControlEventTouchUpInside];
        [self.backgroundView addSubview:_exitBtn];
	}
	return _exitBtn;
}

- (FDCalendar *)calendarView {
    
	if(_calendarView == nil) {
		_calendarView = [[FDCalendar alloc] initWithCurrentDate:[NSDate date]];
        [self addSubview:_calendarView];
	}
	return _calendarView;
}

-(void)exitCalendarView{
    
    if ([self.calendarViewdelegate respondsToSelector:@selector(closeCalendarView:)]) {
        [self.calendarViewdelegate closeCalendarView:[self.calendarView stringForSelectedDate]];
    }
}

@end
