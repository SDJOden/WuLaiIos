//
//  WYHistorySegmentView.m
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/4/1.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import "WYHistorySegmentView.h"

@interface WYHistorySegmentView ()

@property (nonatomic, strong) UIView *lineView;

@property (nonatomic, strong) UISegmentedControl *segmentedControl;

@end

@implementation WYHistorySegmentView

- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize {
    
    self.backgroundColor = [UIColor whiteColor];
    
    [self.segmentedControl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.centerX.equalTo(self);
        make.height.equalTo(30);
        make.left.equalTo(20);
    }];
    
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.left.right.equalTo(self);
        make.height.equalTo(0.3);
    }];
}

- (UISegmentedControl *)segmentedControl {
    
	if(_segmentedControl == nil) {
        _segmentedControl =[[UISegmentedControl alloc]initWithItems:nil];
        _segmentedControl.backgroundColor = [UIColor colorWithRed:0.8118 green:0.8118 blue:0.8118 alpha:1.0];
        _segmentedControl.layer.masksToBounds = YES;
        _segmentedControl.layer.cornerRadius = 3.0;
        _segmentedControl.tintColor = [UIColor colorWithRed:0.5882 green:0.5882 blue:0.5882 alpha:1.0];
        NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:[UIColor blackColor],NSForegroundColorAttributeName, [UIFont systemFontOfSize:15],NSFontAttributeName, nil];
        [_segmentedControl setTitleTextAttributes:dic forState:UIControlStateSelected];
        [_segmentedControl setTitleTextAttributes:dic forState:UIControlStateNormal];
        [_segmentedControl addTarget:self action:@selector(change:) forControlEvents:UIControlEventValueChanged];
        [self addSubview:_segmentedControl];
	}
	return _segmentedControl;
}

-(void)setTitleArray:(NSArray *)titleArray{
    
    _titleArray = titleArray;
    
    for (int i = 0; i < titleArray.count; i ++) {
        [_segmentedControl insertSegmentWithTitle:titleArray[i] atIndex:i animated:NO];
    }
    _segmentedControl.selectedSegmentIndex = 0;
}

- (UIView *)lineView {
    
    if(_lineView == nil) {
        _lineView = [[UIView alloc] init];
        _lineView.backgroundColor = [UIColor lightGrayColor];
        [self addSubview:_lineView];
    }
    return _lineView;
}

-(void)change:(UISegmentedControl *)sender{
    
    if ([self.historySegmetViewDelegate respondsToSelector:@selector(changeSegmentSign:)]) {
        [self.historySegmetViewDelegate changeSegmentSign:sender.selectedSegmentIndex];
    }
}



@end
