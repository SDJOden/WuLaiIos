//
//  WYSearchMsgView.h
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/4/12.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import <UIKit/UIKit.h>

@class WYHistoryModel;

@protocol WYSearchMsgViewDelegate <NSObject>

-(void)pushChatTopView:(WYHistoryModel *)historyModel andIconImage:(UIImage *)iconImage;

@end

@interface WYSearchMsgView : UIView

@property (nonatomic, weak) id <WYSearchMsgViewDelegate> searchMsgViewDelegate;

-(void)backNaviView;

-(void)changeNaviView;

-(void)clickAnimate;

@end
