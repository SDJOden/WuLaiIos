//
//  WYChatHisMsgHeaderView.m
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/4/6.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import "WYChatHisMsgHeaderView.h"
#import "WYHistoryModel.h"
#import "WYConnectListModel.h"

#define MARGIN 25

@interface WYChatHisMsgHeaderView ()

@property (nonatomic, strong) UIImageView *iconImageView;

@property (nonatomic, strong) UILabel *userPhoneConnectLabel;

@property (nonatomic, strong) UILabel *userIdLabel;

@property (nonatomic, strong) UIView *lineView;

@property (nonatomic, strong) UIButton *refreshUserInforBtn;

@property (nonatomic, strong) UIButton *closeUserBtn;

@property (nonatomic, strong) UIButton *changeGeniuBtn;

@property (nonatomic, strong) UIButton *orderBtn;

@property (nonatomic, strong) UIButton *refreshBtn;

@property (nonatomic, copy) NSString *orderStr;

@property (nonatomic, strong) UIButton *userInformationButton;

@property (nonatomic, assign) BOOL isShow;

@end

@implementation WYChatHisMsgHeaderView

+(instancetype)createChatHisMsgHeaderView:(WYHistoryModel *)historyModel{
    
    return [[self alloc]initChatHisMsgHeaderView:historyModel];
}

-(instancetype)initChatHisMsgHeaderView:(WYHistoryModel *)historyModel{
    
    if (self = [super init]) {
        self.iconImageView.image = [[RCIM sharedRCIM] geniusIcon];
        [self rcinit];
        self.userIdLabel.text = [NSString stringWithFormat:@"ID:%@",@(historyModel.user_id).stringValue];
        NSString *phone = @"";
        if ([historyModel.phone isKindOfClass:[NSNumber class]]) {
            phone = [(NSNumber *)historyModel.phone stringValue];
        }
        else {
            phone = historyModel.phone;
        }
        if ([phone hasPrefix:@"10"] || phone.length == 0) {
            self.userPhoneConnectLabel.text = @"(用户未绑定手机号)";
        }
        else {
            self.userPhoneConnectLabel.text = @"(用户已绑定手机号)";
        }
        [self layout];
    }
    return self;
}

-(void)rcinit{
    
    _isShow = NO;
}

+(instancetype)createChatHeaderView:(WYConnectListModel *)connectListModel{
    
    return [[self alloc]initChatHeaderView:connectListModel];
}

-(instancetype)initChatHeaderView:(WYConnectListModel *)connectListModel{
    
    if (self = [super init]) {
        self.iconImageView.image = [[RCIM sharedRCIM] geniusIcon];
        [self rcinit];
        if ([connectListModel.phone hasPrefix:@"10"] || connectListModel.phone.length == 0) {
            self.userPhoneConnectLabel.text = @"(用户未绑定手机号)";
        }
        else {
            self.userPhoneConnectLabel.text = @"(用户已绑定手机号)";
        }
        self.userIdLabel.text = [NSString stringWithFormat:@"ID:%@",@(connectListModel.inner_uid).stringValue];
        [self layout];
    }
    return self;
}

-(void)layout{
    
    self.backgroundColor = HEXCOLOR(0xffffff);
    [self.iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self);
        make.left.equalTo(self).offset(10);
        make.width.height.equalTo(30);
    }];
    
    
    [self.userIdLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset((44-20)*0.5);
        make.left.equalTo(self.iconImageView.right).offset(5);
        make.height.equalTo(20);
    }];
    
    [self.userPhoneConnectLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset((44-20)*0.5);
        make.left.equalTo(self.userIdLabel.right).offset(5);
        make.height.equalTo(20);
    }];
    
    [self.refreshUserInforBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self).offset(-10);
        make.centerY.equalTo(self);
        make.width.height.equalTo(30);
    }];
    
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self);
        make.height.equalTo(0.5);
    }];
    
    [self.userInformationButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.bottom.equalTo(self);
        make.right.equalTo(self.userPhoneConnectLabel.right);
    }];
}

-(void)changeSubViewStyle{
    
    [MobClick event:@"chatroom_operate_click"];
    [self.iconImageView removeFromSuperview];
    [self.userIdLabel removeFromSuperview];
    [self.refreshUserInforBtn removeFromSuperview];
    [self.userPhoneConnectLabel removeFromSuperview];
    [self.userInformationButton removeFromSuperview];
    self.iconImageView = nil;
    self.userIdLabel = nil;
    self.refreshUserInforBtn = nil;
    self.userPhoneConnectLabel = nil;
    self.userInformationButton = nil;
    [self layoutOtherStyle];
}

-(void)restoreSubViewStyle:(WYConnectListModel *)connectListModel{
    
    [MobClick event:@"chatroom_cancel_click"];
    [self.closeUserBtn removeFromSuperview];
    [self.changeGeniuBtn removeFromSuperview];
    [self.orderBtn removeFromSuperview];
    [self.refreshBtn removeFromSuperview];
    self.closeUserBtn = nil;
    self.changeGeniuBtn = nil;
    self.orderBtn = nil;
    self.refreshBtn = nil;
    self.iconImageView.image = [[RCIM sharedRCIM] geniusIcon];
    if ([connectListModel.phone hasPrefix:@"10"] || connectListModel.phone.length == 0) {
        self.userPhoneConnectLabel.text = @"(用户未绑定手机号)";
    }
    else {
        self.userPhoneConnectLabel.text = @"(用户已绑定手机号)";
    }
    self.userIdLabel.text = [NSString stringWithFormat:@"user id:%@",@(connectListModel.inner_uid).stringValue];
    [self layout];
}

-(void)layoutOtherStyle{
    
    [self.closeUserBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self);
        make.left.equalTo(MARGIN);
        make.width.equalTo(self.changeGeniuBtn.width);
        make.height.equalTo(self);
    }];
    
    [self.changeGeniuBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self);
        make.left.equalTo(self.closeUserBtn.right).offset(MARGIN);
        make.width.equalTo(self.orderBtn.width);
        make.height.equalTo(self);
    }];
    
    [self.orderBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self);
        make.left.equalTo(self.changeGeniuBtn.right).offset(MARGIN);
        make.width.equalTo(self.refreshBtn.width);
        make.height.equalTo(self);
    }];
    
    [self.refreshBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self);
        make.left.equalTo(self.orderBtn.right).offset(MARGIN);
        make.right.equalTo(-MARGIN);
        make.height.equalTo(self);
    }];
}

#pragma mark --- 懒加载样式一控件
- (UIImageView *)iconImageView {
    
    if(_iconImageView == nil) {
        _iconImageView = [[UIImageView alloc] init];
        _iconImageView.layer.masksToBounds = YES;
        _iconImageView.layer.cornerRadius = 15.0;
        _iconImageView.layer.borderWidth = 0.5;
        _iconImageView.layer.borderColor = [[UIColor lightGrayColor]CGColor];
        [self addSubview:_iconImageView];
    }
    return _iconImageView;
}

- (UILabel *)userIdLabel {
    
	if(_userIdLabel == nil) {
		_userIdLabel = [[UILabel alloc] init];
        _userIdLabel.textColor = [UIColor blackColor];
        _userIdLabel.font = [UIFont systemFontOfSize:14];
        _userIdLabel.textAlignment = NSTextAlignmentLeft;
        [self addSubview:_userIdLabel];
	}
	return _userIdLabel;
}

- (UIButton *)refreshUserInforBtn {
    
    if(_refreshUserInforBtn == nil) {
        _refreshUserInforBtn = [[UIButton alloc]init];
        [_refreshUserInforBtn setImage:[UIImage imageNamed:@"refresh"] forState:UIControlStateNormal];
        [_refreshUserInforBtn addTarget:self action:@selector(refreshUserInfor) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_refreshUserInforBtn];
    }
    return _refreshUserInforBtn;
}
#pragma mark --- 懒加载样式二控件
- (UIButton *)closeUserBtn {
    
    if(_closeUserBtn == nil) {
        _closeUserBtn = [UIButton buttonWithType:UIButtonTypeSystem];
        [_closeUserBtn setTitle:@"关闭" forState:UIControlStateNormal];
        [_closeUserBtn setTintColor:[UIColor grayColor]];
        _closeUserBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        _closeUserBtn.tag = 0;
        [_closeUserBtn addTarget:self action:@selector(menuBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_closeUserBtn];
    }
    return _closeUserBtn;
}

- (UIButton *)changeGeniuBtn {
    
    if(_changeGeniuBtn == nil) {
        _changeGeniuBtn = [UIButton buttonWithType:UIButtonTypeSystem];
        [_changeGeniuBtn setTitle:@"转接" forState:UIControlStateNormal];
        [_changeGeniuBtn setTintColor:[UIColor grayColor]];
        _changeGeniuBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        _changeGeniuBtn.tag = 1;
        [_changeGeniuBtn addTarget:self action:@selector(menuBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_changeGeniuBtn];
        
    }
    return _changeGeniuBtn;
}

- (UIButton *)orderBtn {
    if(_orderBtn == nil) {
        
        _orderBtn = [UIButton buttonWithType:UIButtonTypeSystem];
        [_orderBtn setTitle:self.orderStr forState:UIControlStateNormal];
        [_orderBtn setTintColor:[UIColor grayColor]];
        _orderBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        _orderBtn.tag = 2;
        [_orderBtn addTarget:self action:@selector(menuBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_orderBtn];
    }
    return _orderBtn;
}

- (UIButton *)refreshBtn {
    if(_refreshBtn == nil) {
        
        _refreshBtn = [UIButton buttonWithType:UIButtonTypeSystem];
        [_refreshBtn setTitle:@"刷新" forState:UIControlStateNormal];
        [_refreshBtn setTintColor:[UIColor grayColor]];
        _refreshBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        _refreshBtn.tag = 3;
        [_refreshBtn addTarget:self action:@selector(menuBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_refreshBtn];
    }
    return _refreshBtn;
}

-(void)menuBtnClick:(UIButton *)sender{
    
    if ([self.chatHeaderViewDelegate respondsToSelector:@selector(titleMenuClick:)]) {
        [self.chatHeaderViewDelegate titleMenuClick:sender];
    }
}

-(void)changeOrderBtnTitle{
    
    if (self.orderBtn) {
        self.orderStr = @"对话";
        [self.orderBtn setTitle:[self.orderBtn.titleLabel.text isEqualToString:@"下单"] ? @"对话" : @"下单" forState:UIControlStateNormal];
    }
}

- (NSString *)orderStr {
    
	if(_orderStr == nil) {
		_orderStr = @"下单";
	}
	return _orderStr;
}

-(void)refreshUserInfor{
    
    if ([self.chatHeaderViewDelegate respondsToSelector:@selector(changeUserInfo)]) {
        [self.chatHeaderViewDelegate changeUserInfo];
    }
}

- (UIView *)lineView {
    
	if(_lineView == nil) {
		_lineView = [[UIView alloc] init];
        _lineView.backgroundColor = [UIColor lightGrayColor];
        [self addSubview:_lineView];
	}
	return _lineView;
}

- (UIButton *)userInformationButton {
    
	if(_userInformationButton == nil) {
		_userInformationButton = [[UIButton alloc] init];
        _userInformationButton.backgroundColor = [UIColor clearColor];
        [_userInformationButton addTarget:self action:@selector(showUserInformationView) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_userInformationButton];
	}
	return _userInformationButton;
}

-(void)showUserInformationView{
    
    _isShow = !_isShow;
    if ([self.chatHeaderViewDelegate respondsToSelector:@selector(didTouchShowUserInformationButton:)]) {
        [self.chatHeaderViewDelegate didTouchShowUserInformationButton:_isShow];
    }
}

- (UILabel *)userPhoneConnectLabel {
    
	if(_userPhoneConnectLabel == nil) {
        _userPhoneConnectLabel = [[UILabel alloc] init];
        _userPhoneConnectLabel.textColor = [UIColor blackColor];
        _userPhoneConnectLabel.font = [UIFont systemFontOfSize:14];
        _userPhoneConnectLabel.textAlignment = NSTextAlignmentLeft;
        [self addSubview:_userPhoneConnectLabel];
	}
	return _userPhoneConnectLabel;
}

@end
