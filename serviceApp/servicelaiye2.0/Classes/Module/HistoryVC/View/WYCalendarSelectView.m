//
//  WYCalendarSelectView.m
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/4/12.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import "WYCalendarSelectView.h"


@interface WYCalendarSelectView ()

@property (nonatomic, strong) UIButton *todayBtn;

@property (nonatomic, strong) UIButton *yesterdayBtn;

@property (nonatomic, strong) UIButton *tomorrowBtn;

@property (nonatomic, strong) UIButton *calendarBtn;

@property (nonatomic, strong) NSDate *showingDate;

@property (nonatomic, strong) NSDate *yestDate;

@property (nonatomic, strong) NSDate *tomDate;

@end

@implementation WYCalendarSelectView

- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize {
    
    [self rcinit];
    
    [self.yesterdayBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.bottom.equalTo(self);
        make.width.equalTo(self.todayBtn);
    }];
    
    [self.todayBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(self);
        make.left.equalTo(self.yesterdayBtn.right).offset(2);
        make.width.equalTo(self.tomorrowBtn);
    }];
    
    [self.tomorrowBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(self);
        make.left.equalTo(self.todayBtn.right).offset(2);
        make.right.equalTo(self.calendarBtn.left).offset(-2);
    }];
    
    [self.calendarBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.right.equalTo(self);
        make.width.equalTo(50);
    }];
}

-(void)rcinit{
    
    self.backgroundColor = [UIColor whiteColor];
    self.showingDate = [NSDate date];
    self.yestDate = [self getNewDateWithDate:self.showingDate withFlag:NO];
    self.tomDate = [self getNewDateWithDate:self.showingDate withFlag:YES];
}

#pragma mark --- 懒加载
- (UIButton *)todayBtn {
    
	if(_todayBtn == nil) {
		_todayBtn = [[UIButton alloc] init];
        [_todayBtn setTitle:[self currentDateStrFromDate:self.showingDate] forState:UIControlStateNormal];
        [_todayBtn setBackgroundColor:[UIColor colorWithRed:0.8118 green:0.8118 blue:0.8118 alpha:1.0]];
        [_todayBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        _todayBtn.titleLabel.font = [UIFont systemFontOfSize:12];
        [self addSubview:_todayBtn];
	}
	return _todayBtn;
}

- (UIButton *)yesterdayBtn {
    
	if(_yesterdayBtn == nil) {
		_yesterdayBtn = [[UIButton alloc] init];
        [_yesterdayBtn setTitle:[self currentDateStrFromDate:self.yestDate] forState:UIControlStateNormal];
        [_yesterdayBtn setBackgroundColor:[UIColor colorWithRed:0.8118 green:0.8118 blue:0.8118 alpha:1.0]];
        [_yesterdayBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        _yesterdayBtn.titleLabel.font = [UIFont systemFontOfSize:12];
        [_yesterdayBtn addTarget:self action:@selector(yesterdayBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_yesterdayBtn];
	}
	return _yesterdayBtn;
}

- (UIButton *)tomorrowBtn {
    
    if(_tomorrowBtn == nil) {
        _tomorrowBtn = [[UIButton alloc] init];
        [_tomorrowBtn setTitle:[self currentDateStrFromDate:self.tomDate] forState:UIControlStateNormal];
        [_tomorrowBtn setBackgroundColor:[UIColor colorWithRed:0.8118 green:0.8118 blue:0.8118 alpha:1.0]];
        [_tomorrowBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        _tomorrowBtn.titleLabel.font = [UIFont systemFontOfSize:12];
        [_tomorrowBtn addTarget:self action:@selector(tomorrowBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_tomorrowBtn];
    }
    return _tomorrowBtn;
}

- (UIButton *)calendarBtn {
    
	if(_calendarBtn == nil) {
		_calendarBtn = [[UIButton alloc] init];
        [_calendarBtn setTitle:@"查看日历" forState:UIControlStateNormal];
        [_calendarBtn setBackgroundColor:[UIColor colorWithRed:0.8118 green:0.8118 blue:0.8118 alpha:1.0]];
        [_calendarBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        _calendarBtn.titleLabel.font = [UIFont systemFontOfSize:12];
        [_calendarBtn addTarget:self action:@selector(changeDate:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_calendarBtn];
	}
	return _calendarBtn;
}

#pragma mark --- 点击事件
-(void)yesterdayBtnClicked:(UIButton *)sender{
    
    [self buttonDateChange:NO];
    if ([self.calendarSelectViewDelegate respondsToSelector:@selector(searchListChangeWithButtonClick:)]) {
        [self.calendarSelectViewDelegate searchListChangeWithButtonClick:[[self.todayBtn.titleLabel.text copy]stringByReplacingOccurrencesOfString:@"-" withString:@""]];
    }
}

-(void)tomorrowBtnClicked:(UIButton *)sender{
    
    [self buttonDateChange:YES];
}

/**
 *  通用点击事件
 */
-(void)changeDate:(UIButton *)sender{
    
    [MobClick event:@"geniusHistory_date_click"];
    if ([self.calendarSelectViewDelegate respondsToSelector:@selector(calendarSelectDidClicked:)]) {
        [self.calendarSelectViewDelegate calendarSelectDidClicked:sender];
    }
}


-(void)buttonDateChange:(BOOL)isNextDate{
    
    if (isNextDate) {//后一天
        self.yestDate = self.showingDate;
        self.showingDate = self.tomDate;
        self.tomDate = [self getNewDateWithDate:self.tomDate withFlag:YES];
    }
    else {//前一天
        self.tomDate = self.showingDate;
        self.showingDate = self.yestDate;
        self.yestDate = [self getNewDateWithDate:self.yestDate withFlag:NO];
    }
    [self showButtonsTitle];
}

-(void)showButtonsTitle{
    
    [self.yesterdayBtn setTitle:[self currentDateStrFromDate:self.yestDate] forState:UIControlStateNormal];
    [self.todayBtn setTitle:[self currentDateStrFromDate:self.showingDate] forState:UIControlStateNormal];
    [self.tomorrowBtn setTitle:[self currentDateStrFromDate:self.tomDate] forState:UIControlStateNormal];
}

-(void)changeSelectDate:(NSDate *)date{
    
    self.showingDate = date;
    self.yestDate = [self getNewDateWithDate:self.showingDate withFlag:NO];
    self.tomDate = [self getNewDateWithDate:self.showingDate withFlag:YES];
    [self showButtonsTitle];
}

-(NSDate *)getNewDateWithDate:(NSDate *)date withFlag:(BOOL)isNext{
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *adcomps = [[NSDateComponents alloc] init];
    [adcomps setDay:isNext ? 1 : -1];
    [calendar dateByAddingComponents:adcomps toDate:date options:0];
    return [calendar dateByAddingComponents:adcomps toDate:date options:0];
}

- (NSString *)currentDateStrFromDate:(NSDate *)date{
    
    NSDateComponents *componectsNow = [[NSCalendar currentCalendar] components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:date];
    NSDateComponents* components = [[NSCalendar currentCalendar] components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:[NSDate date]];
    NSDateFormatter *inputFormatter = [[NSDateFormatter alloc] init];
    [inputFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"zh_Hans_CN"]];
    
    if ([components year] == [componectsNow year]) {
        [inputFormatter setDateFormat:@"MM-dd"];
    }else {
        [inputFormatter setDateFormat:@"yyyy-MM-dd"];
    }
    
    if ([components year] == [componectsNow year] && [components month] == [componectsNow month]) {
        if ([components day] == [componectsNow day]) {
            return [NSString stringWithFormat:@"今天(%@)", [inputFormatter stringFromDate:date]];
        }
        else if ([components day] == [componectsNow day] - 1) {
            return [NSString stringWithFormat:@"明天(%@)", [inputFormatter stringFromDate:date]];
        }
        else if ([components day] == [componectsNow day] + 1) {
            return [NSString stringWithFormat:@"昨天(%@)", [inputFormatter stringFromDate:date]];
        }
    }
    return [inputFormatter stringFromDate:date];
}

@end
