//
//  WYHistoryView.m
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/4/1.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import "WYHistoryView.h"
#import "WYHistoryTableViewCell.h"
#import "WYHistoryModel.h"
#import "MJRefresh.h"

@interface WYHistoryView () <UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate>

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) UISearchBar *searchBar;

@property (nonatomic, assign) int page;

@property (nonatomic, strong) NSMutableArray *dataArray;

@end

@implementation WYHistoryView

- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize {
    
    [self.searchBar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(10);
        make.right.equalTo(self).offset(-10);
        make.top.equalTo(self);
        make.height.equalTo(44);
    }];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.searchBar.bottom);
        make.left.right.equalTo(self);
        make.bottom.equalTo(self);
    }];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(requestTableViewData:) name:@"wyRequestTableViewData" object:nil];
}

-(void)requestTableViewData:(NSNotification *)noti{
    
    if ([noti.object isEqualToString:@"0"]) {
        
        [self.tableView setContentOffset:CGPointMake(0, 0) animated:NO];
        self.page = 1;
        [self refreshHistoryModel:self.page];
    }
}

-(void)loadNewData{
    
    self.page = 1;
    
    [self refreshHistoryModel:self.page];
}

//更多列表
-(void)moreInformation{
    
    self.page ++;
    [self refreshHistoryModel:self.page];
}

-(void)refreshHistoryModel:(int)page{
    
    __weak typeof(self) __weakSelf = self;
    [[SDJNetworkTools sharedTools]recentUsers:[SDJUserAccount loadAccount].token p:@(page).stringValue ps:FreshCellOnceNumber finished:^(NSDictionary<NSString *,id> * _Nullable result, NSError * _Nullable error) {
        if (error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [__weakSelf.tableView.mj_header endRefreshing];
                [__weakSelf.tableView.mj_footer endRefreshing];
            });
        }
        if (![result[@"error"] isEqual:[NSNull null]]) {//报错
            NSNumber *code = result[@"error"][@"error_code"];
            if (code.integerValue == 20000) {
                NSString *UserPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).firstObject stringByAppendingPathComponent:@"account.plist"];
                [[NSFileManager defaultManager]removeItemAtPath:UserPath error:nil];
                [[RCIMClient sharedRCIMClient] logout];
                // 发送通知让appdelegate去切换根视图为聊天列表界面
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"WYRootViewControllerSwitchNotification" object:@"0"];
                });
            }
        }
        else{
            if (__weakSelf.page == 1) {
                [__weakSelf.dataArray removeAllObjects];
            }
            NSArray *resultDataArray = result[@"data"][@"list"];
            for (NSDictionary *dict in resultDataArray) {
                WYHistoryModel *historyModel = [WYTool createHistoryModelByDict:dict];
                [__weakSelf.dataArray addObject:historyModel];
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [__weakSelf.tableView reloadData];
                [__weakSelf.tableView.mj_header endRefreshing];
                [__weakSelf.tableView.mj_footer endRefreshing];
                if (resultDataArray.count < FreshCellOnceNumber.integerValue) {
                    [__weakSelf.tableView.mj_footer endRefreshingWithNoMoreData];
                }
            });
        }
    }];
}

#pragma mark UITableViewDelegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.dataArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *Identifier = @"historyCell";
    WYHistoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier forIndexPath:indexPath];
    WYHistoryModel *historyModel = self.dataArray[indexPath.row];
    [cell setContentByHistoryModel:historyModel];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 70;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [MobClick event:@"geniusHistory_userTableViewCell_click"];
    WYHistoryTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    //设置头像
    [[RCIM sharedRCIM] setUserIcon:[UIImage imageNamed:@"chatroom_userIcon_xiaolai"]];
    [[RCIM sharedRCIM] setGeniusIcon:[cell getImage]];
    if ([self.historyViewDelegate respondsToSelector:@selector(pushChatMsgView:andIconImage:)]) {
        [self.historyViewDelegate pushChatMsgView:self.dataArray[indexPath.row] andIconImage:[cell getImage]];
    }
}

//分割线顶头
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]){
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    cell.preservesSuperviewLayoutMargins = NO;
}

#pragma mark --- 懒加载
- (UITableView *)tableView {
    
    if(_tableView == nil) {
        _tableView = [[UITableView alloc]init];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        self.tableView.mj_header = [MJRefreshStateHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadNewData)];
        self.tableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(moreInformation)];
        self.tableView.tableFooterView = [UIView new];
        [self.tableView registerClass:[WYHistoryTableViewCell class] forCellReuseIdentifier:@"historyCell"];
        [self addSubview:_tableView];
    }
    
    return _tableView;
}

- (NSMutableArray *)dataArray {
    
    if(_dataArray == nil) {
        _dataArray = [[NSMutableArray alloc] init];
    }
    return _dataArray;
}

- (UISearchBar *)searchBar {
    
    if(_searchBar == nil) {
        _searchBar = [[UISearchBar alloc] init];
        _searchBar.delegate = self;
        _searchBar.autocorrectionType = UITextAutocapitalizationTypeNone;
        _searchBar.keyboardType = UIKeyboardTypeDefault;
        _searchBar.placeholder = @"请输入小来姓名或编号";
        [_searchBar setShowsCancelButton:NO];
        for (__strong id cc in [_searchBar.subviews[0] subviews]) {
            if ([cc isKindOfClass:NSClassFromString(@"UISearchBarBackground")]) {
                [cc removeFromSuperview];
                cc = nil;
            }
            if ([cc isKindOfClass:NSClassFromString(@"UISearchBarTextField")]) {
                ((UITextField *)cc).backgroundColor = [UIColor colorWithRed:0.7373 green:0.7373 blue:0.7373 alpha:1.0];
                ((UITextField *)cc).font = [UIFont systemFontOfSize:12];
            }
        }
        UIView *view = [UIView new];
        view.backgroundColor = [UIColor colorWithWhite:0.855 alpha:1.000];
        view.frame = CGRectMake(0, 43.5, [UIScreen mainScreen].bounds.size.width, 0.5);
        [self addSubview:view];
        [self addSubview:_searchBar];
    }
    return _searchBar;
}

#pragma mark --- UISearchBarDelegate
-(BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar{
    
    return YES;
}

-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    
    [MobClick event:@"geniusHistory_searchBar_click"];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"searchHis" object:self];
}

-(void)dealloc{
    
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

@end
