//
//  WYSearchHisView.m
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/4/9.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import "WYSearchHisView.h"
#import "WYCalendarSelectView.h"
#import "WYCalendarView.h"
#import "WYHistoryTableViewCell.h"
#import "WYSearchDataModel.h"
#import "WYGeniusModel.h"
#import "MJRefresh.h"

@interface WYSearchHisView () <UISearchBarDelegate, WYCalendarSelectViewDelegate, WYCalendarViewDelegate, UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UIView *searchBackView;

@property (nonatomic, strong) UISearchBar *searchBar;

@property (nonatomic, strong) WYCalendarSelectView *calendarSelectView;

@property (nonatomic, strong) WYCalendarView *calendarView;

@property (nonatomic, strong) UITableView *searchHisTableView;

@property (nonatomic, copy) NSString *dateStr;

@property (nonatomic, assign) int page;

@property (nonatomic, strong) WYSearchDataModel *searchDataModel;

@property (nonatomic, strong) NSMutableArray *geniusArray;

@property (nonatomic, strong) NSMutableArray *dataArray;

@property (nonatomic, assign) BOOL isSearch;

@end

@implementation WYSearchHisView

- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initialize];
    }
    return self;
}

-(void)rcinit{
    
    self.backgroundColor = [UIColor whiteColor];
    self.dateStr = [self currentDateStrFromeDate:[NSDate date]];
    self.isSearch = NO;
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(removeCalendarView:) name:@"wyHistoryCalendarChangeDate" object:nil];
}

// 布局
- (void)initialize {
    
    [self rcinit];
    
    [self.searchBackView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self);
        make.height.equalTo(44);
        make.top.equalTo(0);
    }];
    
    [self.searchBar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(8);
        make.right.equalTo(-8);
        make.bottom.equalTo(-8);
        make.height.equalTo(28);
    }];
    
    [self.calendarSelectView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.searchBackView.bottom);
        make.left.right.equalTo(self);
        make.height.equalTo(30);
    }];
    
    [self.searchHisTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.calendarSelectView.bottom);
        make.left.right.bottom.equalTo(self);
    }];
}

-(void)requestDataWithSearchModel:(WYSearchDataModel *)searchDataModel{
    //第一次的时候取助理labels
    if (self.geniusArray.count == 0) {
        [self requestGeniusLabels:searchDataModel];
        return;
    }
    [self dataWithSearchModel:searchDataModel];
}

-(void)requestGeniusLabels:(WYSearchDataModel *)searchDataModel{
    
    __weak typeof(self)__weakSelf = self;
    [[SDJNetworkTools sharedTools]geniusLabel:[SDJUserAccount loadAccount].token force:1 finished:^(NSDictionary<NSString *,id> * _Nullable result, NSError * _Nullable error) {
        if (![result[@"error"] isEqual:[NSNull null]]) {//报错
            NSNumber *code = result[@"error"][@"error_code"];
            if (code.integerValue == 20000) {
                NSString *UserPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).firstObject stringByAppendingPathComponent:@"account.plist"];
                [[NSFileManager defaultManager]removeItemAtPath:UserPath error:nil];
                [[RCIMClient sharedRCIMClient] logout];
                // 发送通知让appdelegate去切换根视图为聊天列表界面
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"WYRootViewControllerSwitchNotification" object:@"0"];
                });
            }
        }
        else {
            __weakSelf.geniusArray = [WYTool geniusArrayOfArray:result[@"data"]];
            [__weakSelf dataWithSearchModel:searchDataModel];
            [RCIM sharedRCIM].geniusLabelArray = __weakSelf.geniusArray;
        }
    }];
}

-(void)dataWithSearchModel:(WYSearchDataModel *)searchDataModel{
    
    if (![self.searchDataModel.date isEqualToString:searchDataModel.date] || ![self.searchDataModel.geniusName isEqualToString:searchDataModel.geniusName]) {
        self.page = 1;
        self.searchDataModel = searchDataModel;
    }
    
    if (searchDataModel.geniusName.integerValue == 0) {
        //遍历
        for (WYGeniusModel *geniusModel in self.geniusArray) {
            if ([searchDataModel.geniusName isEqualToString:geniusModel.real_name]) {
                searchDataModel.geniusName = geniusModel.genius_id;
                break;
            }
        }
    } else {
        for (WYGeniusModel *geniusModel in self.geniusArray) {
            if ([geniusModel.username rangeOfString:searchDataModel.geniusName].length > 0) {
                searchDataModel.geniusName = geniusModel.genius_id;
                break;
            }
        }
    }
    __weak typeof(self) __weakSelf = self;
    
    [self requestWithSelfModel:__weakSelf andWithSearchModel:searchDataModel];
}

-(void)requestWithSelfModel:(WYSearchHisView *)__weakSelf andWithSearchModel:(WYSearchDataModel *)searchDataModel{
    
    [[SDJNetworkTools sharedTools]userOfGeniu:[SDJUserAccount loadAccount].token date:self.dateStr genius_id:searchDataModel.geniusName p:@(__weakSelf.page).stringValue ps:FreshCellOnceNumber finished:^(NSDictionary<NSString *,id> * _Nullable result, NSError * _Nullable error) {
        if (error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [__weakSelf.searchHisTableView.mj_header endRefreshing];
                [__weakSelf.searchHisTableView.mj_footer endRefreshing];
            });
        }
        if (![result[@"error"] isEqual:[NSNull null]]) {//报错
            NSNumber *code = result[@"error"][@"error_code"];
            if (code.integerValue == 20000) {
                NSString *UserPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).firstObject stringByAppendingPathComponent:@"account.plist"];
                [[NSFileManager defaultManager]removeItemAtPath:UserPath error:nil];
                [[RCIMClient sharedRCIMClient] logout];
                // 发送通知让appdelegate去切换根视图为聊天列表界面
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"WYRootViewControllerSwitchNotification" object:@"0"];
                });
            }
        }
        else {
            [self loseEditingStatu];
            if (__weakSelf.page == 1) {
                [__weakSelf.dataArray removeAllObjects];
            }
            NSArray *resultDataArray = result[@"data"][@"list"];
            for (NSDictionary *dict in resultDataArray) {
                WYHistoryModel *historyModel = [WYTool createHistoryModelByDict:dict];
                [__weakSelf.dataArray addObject:historyModel];
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                [__weakSelf.searchHisTableView reloadData];
                [__weakSelf.searchHisTableView.mj_header endRefreshing];
                [__weakSelf.searchHisTableView.mj_footer endRefreshing];
                if (resultDataArray.count < FreshCellOnceNumber.integerValue) {
                    [__weakSelf.searchHisTableView.mj_footer endRefreshingWithNoMoreData];
                }
            });
        }
    }];
}

-(void)loadNewData{
    
    self.page = 1;
    [self requestWithSelfModel:self andWithSearchModel:self.searchDataModel];
}

-(void)moreInformation{
    
    self.page ++;
    [self requestWithSelfModel:self andWithSearchModel:self.searchDataModel];
}

#pragma mark --- UITableViewDelegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.dataArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *Identifier = @"historyCell";
    WYHistoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier forIndexPath:indexPath];
    WYHistoryModel *historyModel = self.dataArray[indexPath.row];
    [cell setContentByHistoryModel:historyModel];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 70;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [MobClick event:@"geniusHistory_userTableViewCell_click"];
    WYHistoryTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    //设置头像
    [[RCIM sharedRCIM] setUserIcon:[UIImage imageNamed:@"chatroom_userIcon_xiaolai"]];
    [[RCIM sharedRCIM] setGeniusIcon:[cell getImage]];
    if ([self.searchHisViewDelegate respondsToSelector:@selector(pushChatMsgView: andIconImage:)]) {
        [[NSNotificationCenter defaultCenter]postNotificationName:@"searchHisMove" object:nil];
        [self.searchHisViewDelegate pushChatMsgView:self.dataArray[indexPath.row] andIconImage:[cell getImage]];
    }
}

//分割线顶头
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]){
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    cell.preservesSuperviewLayoutMargins = NO;
}

- (NSString *)currentDateStrFromeDate:(NSDate *)date{
    
    NSDateFormatter *inputFormatter = [[NSDateFormatter alloc] init];
    [inputFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"zh_Hans_CN"]];
    [inputFormatter setDateFormat:@"yyyyMMdd"];
    return [inputFormatter stringFromDate:date];
}

-(void)loseEditingStatu{
    
    [self endEditing:YES];
    for(id control in [self.searchBar.subviews[0] subviews])
    {
        if ([control isKindOfClass:[UIButton class]])
        {
            UIButton * btn =(UIButton *)control;
            btn.enabled = YES;
        }
    }
}

#pragma mark --- WYCalendarSelectViewDelegate
-(void)searchListChangeWithButtonClick:(NSString *)dateStr{
    
    [self loseEditingStatu];
    self.dateStr = dateStr;
    if (_isSearch) {
        self.page = 1;
        [self requestDataWithSearchModel:[WYSearchDataModel searchDataModelWithGeniusName:self.searchBar.text andWithDate:self.dateStr]];
    }
}

-(void)removeCalendarView:(NSNotification *)noti{
    
    [self.calendarView removeFromSuperview];
    self.calendarView = nil;
    [self.calendarSelectView changeSelectDate:(NSDate *)noti.object];
    self.dateStr = [self currentDateStrFromeDate:(NSDate *)noti.object];
    if (_isSearch) {
        self.page = 1;
        [self requestDataWithSearchModel:[WYSearchDataModel searchDataModelWithGeniusName:self.searchBar.text andWithDate:self.dateStr]];
    }
}

#pragma mark --- WYCalendarViewDelegate
-(void)closeCalendarView:(NSString *)dateStr{
    
    [self.calendarView removeFromSuperview];
    self.calendarView = nil;
}

-(void)removeSubViews{
    
    [self.calendarSelectView removeFromSuperview];
    self.calendarSelectView = nil;
}

-(void)calendarSelectDidClicked:(UIButton *)sender{
    
    [self loseEditingStatu];
    self.calendarView.frame = [UIScreen mainScreen].bounds;
}
#pragma mark --- UISearchBarDelegate
/**
 *  搜索
 */
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    
    self.isSearch = YES;
    self.page = 1;
    [self requestDataWithSearchModel:[WYSearchDataModel searchDataModelWithGeniusName:self.searchBar.text andWithDate:self.dateStr]];
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    
    [self endEditing:YES];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"searchHisCancel" object:nil];
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    [self loseEditingStatu];
}

#pragma mark --- 外部动画
-(void)backNaviView{
    
    [self.searchBar setShowsCancelButton:NO animated:YES];
    self.searchBackView.backgroundColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1.0];
    for (id cc in [_searchBar.subviews[0] subviews]) {
        if ([cc isKindOfClass:NSClassFromString(@"UISearchBarTextField")]) {
            ((UITextField *)cc).backgroundColor = [UIColor colorWithRed:0.7373 green:0.7373 blue:0.7373 alpha:1.0];
        }
    }
    [self.searchBackView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(44);
    }];
}

-(void)changeNaviView{
    
    self.searchBackView.backgroundColor = [UIColor colorWithRed:0.1922 green:0.2078 blue:0.2392 alpha:1.0];
    
    for (id cc in [_searchBar.subviews[0] subviews]) {
        if ([cc isKindOfClass:NSClassFromString(@"UISearchBarTextField")]) {
            ((UITextField *)cc).backgroundColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1.0];
        }
    }
    
    [self.searchBackView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(64);
    }];
}

-(void)clickAnimate{
    
    [self.searchBar setShowsCancelButton:YES animated:YES];
    
    for (__strong id cc in [_searchBar.subviews[0] subviews]) {
        if ([cc isKindOfClass:NSClassFromString(@"UINavigationButton")]) {
            [(UIButton *)cc setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            break;
        }
    }
    
    [self.searchBar becomeFirstResponder];
}

#pragma mark --- 懒加载
- (UIView *)searchBackView {
    
    if(_searchBackView == nil) {
        _searchBackView = [[UIView alloc] init];
        _searchBackView.backgroundColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1.0];
        [self addSubview:_searchBackView];
    }
    return _searchBackView;
}

- (UISearchBar *)searchBar {
    
    if(_searchBar == nil) {
        _searchBar = [[UISearchBar alloc] init];
        _searchBar.delegate = self;
        _searchBar.autocorrectionType = UITextAutocapitalizationTypeNone;
        _searchBar.keyboardType = UIKeyboardTypeDefault;
        _searchBar.placeholder=@"请输入小来姓名或编号";
        [_searchBar setShowsCancelButton:NO];
        for (__strong id cc in [_searchBar.subviews[0] subviews]) {
            if ([cc isKindOfClass:NSClassFromString(@"UISearchBarBackground")]) {
                [cc removeFromSuperview];
                cc = nil;
            }
            if ([cc isKindOfClass:NSClassFromString(@"UISearchBarTextField")]) {
                ((UITextField *)cc).backgroundColor = [UIColor colorWithRed:0.7373 green:0.7373 blue:0.7373 alpha:1.0];
                ((UITextField *)cc).font = [UIFont systemFontOfSize:12];
            }
        }
        [self.searchBackView addSubview:_searchBar];
    }
    return _searchBar;
}

- (WYCalendarSelectView *)calendarSelectView {
    
    if(_calendarSelectView == nil) {
        _calendarSelectView = [[WYCalendarSelectView alloc] init];
        _calendarSelectView.calendarSelectViewDelegate = self;
        [self addSubview:_calendarSelectView];
    }
    return _calendarSelectView;
}

- (WYCalendarView *)calendarView {
    
    if(_calendarView == nil) {
        _calendarView = [[WYCalendarView alloc] init];
        self.calendarView.calendarViewdelegate = self;
        [self addSubview:_calendarView];
    }
    return _calendarView;
}

- (UITableView *)searchHisTableView {
    
    if(_searchHisTableView == nil) {
        _searchHisTableView = [[UITableView alloc]init];
        _searchHisTableView.dataSource = self;
        _searchHisTableView.delegate = self;
        _searchHisTableView.mj_header = [MJRefreshStateHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadNewData)];
        _searchHisTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(moreInformation)];
        _searchHisTableView.tableFooterView = [UIView new];
        [_searchHisTableView registerClass:[WYHistoryTableViewCell class] forCellReuseIdentifier:@"historyCell"];
        [self addSubview:_searchHisTableView];
    }
    return _searchHisTableView;
}

- (NSMutableArray *)geniusArray {
    
    if(_geniusArray == nil) {
        _geniusArray = [[NSMutableArray alloc] init];
    }
    return _geniusArray;
}

- (WYSearchDataModel *)searchDataModel {
    
	if(_searchDataModel == nil) {
		_searchDataModel = [[WYSearchDataModel alloc] init];
	}
	return _searchDataModel;
}

- (NSMutableArray *)dataArray {
    
	if(_dataArray == nil) {
		_dataArray = [[NSMutableArray alloc] init];
	}
	return _dataArray;
}

@end
