//
//  WYSourceView.h
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/4/1.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol WYSourceViewDelegate <NSObject>

-(void)pushChatTopView:(WYHistoryModel *)historyModel andIconImage:(UIImage *)iconImage;

@end

@interface WYSourceView : UIView

@property (nonatomic, weak) id <WYSourceViewDelegate> sourceViewDelegate;

-(void)loadNewData;

-(void)removeBlankView;

@end
