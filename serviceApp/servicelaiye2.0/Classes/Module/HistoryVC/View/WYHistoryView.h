//
//  WYHistoryView.h
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/4/1.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import <UIKit/UIKit.h>

@class WYHistoryModel;

@protocol WYHistoryViewDelegate <NSObject>

-(void)pushChatMsgView:(WYHistoryModel *)historyModel andIconImage:(UIImage *)iconImage;

@end

@interface WYHistoryView : UIView

@property (nonatomic, weak) id <WYHistoryViewDelegate> historyViewDelegate;

-(void)loadNewData;

@end
