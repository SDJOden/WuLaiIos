//
//  WYSourceView.m
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/4/1.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import "WYSourceView.h"
#import "WYHistoryTableViewCell.h"
#import "MJRefresh.h"

@interface WYSourceView () <UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource>
/**
 *  搜索view
 */
@property (nonatomic, strong) UISearchBar *searchBar;

/**
 *  tableView
 */
@property (nonatomic, strong) UITableView *listTableView;
/**
 *  状态tableView
 */
@property (nonatomic, strong) UITableView *statuTableView;
/**
 *  蒙版Btn
 */
@property (nonatomic, strong) UIButton *blankBtn;

/**
 *  请求的页数
 */
@property (nonatomic, assign) NSInteger page;
/**
 *  数据数组
 */
@property (nonatomic, strong) NSMutableArray *dataArray;
/**
 *  状态数组
 */
@property (nonatomic, strong) NSArray *statuArray;
/**
 *  选择的cell号码
 */
@property (nonatomic, assign) NSInteger numberOfSelectedCell;
/**
 *  选择的搜索状态cell号码
 */
@property (nonatomic, assign) NSInteger selectIndexOfStatu;

@end

@implementation WYSourceView

- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize {
    
    [self.searchBar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(10);
        make.right.equalTo(self).offset(-10);
        make.top.equalTo(self);
        make.height.equalTo(44);
    }];
    
    [self.listTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.searchBar.bottom);
        make.left.right.bottom.equalTo(self);
    }];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(requestTableViewData:) name:@"wyRequestTableViewData" object:nil];
}

-(void)requestTableViewData:(NSNotification *)noti{
    
    if ([noti.object isEqualToString:@"1"]) {
        [self.listTableView setContentOffset:CGPointMake(0, 0) animated:NO];
        [self rcinit];
        [self refreshData];
    }
}

-(void)refreshData{
    
    __weak typeof(self) __weakSelf = self;
    [[SDJNetworkTools sharedTools]recentListOfUsers:[SDJUserAccount loadAccount].token p:@(self.page).stringValue ps:FreshCellOnceNumber finished:^(NSDictionary<NSString *,id> * _Nullable result, NSError * _Nullable error) {
        if (error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [__weakSelf.listTableView.mj_header endRefreshing];
                [__weakSelf.listTableView.mj_footer endRefreshing];
            });
        }
        
        if (![result[@"error"] isEqual:[NSNull null]]) {//报错
            NSNumber *code = result[@"error"][@"error_code"];
            if (code.integerValue == 20000) {
                NSString *UserPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).firstObject stringByAppendingPathComponent:@"account.plist"];
                [[NSFileManager defaultManager]removeItemAtPath:UserPath error:nil];
                [[RCIMClient sharedRCIMClient] logout];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"WYRootViewControllerSwitchNotification" object:@"0"];
                });
            }
        }
        else{
            NSArray *historyListArray = result[@"data"][@"list"];
            [__weakSelf resetData:historyListArray andModel:__weakSelf];
        }
    }];
}

-(void)resetData:(NSArray *)historyListArray andModel:(WYSourceView *)__weakSelf{
    
    if (__weakSelf.page == 1) {
        [__weakSelf.dataArray removeAllObjects];
    }
    for (NSDictionary *dic in historyListArray) {
        WYHistoryModel *listModel = [WYTool createRecordListModelByDict:dic];
        [__weakSelf.dataArray addObject:listModel];
    }
    [__weakSelf.listTableView reloadData];
    [__weakSelf.listTableView.mj_header endRefreshing];
    [__weakSelf.listTableView.mj_footer endRefreshing];
    if (historyListArray.count < FreshCellOnceNumber.integerValue) {
        [__weakSelf.listTableView.mj_footer endRefreshingWithNoMoreData];
    }
}

-(void)loadNewData{
    
    self.page = 1;
    self.numberOfSelectedCell = -1;
    [self refreshData];
}

-(void)moreInformation{
    
    self.page ++;
    self.numberOfSelectedCell = -1;
    [self refreshData];
}

/**
 *  初始化参数
 */
-(void)rcinit{
    
    self.page = 1;
    self.numberOfSelectedCell = -1;
    self.selectIndexOfStatu = 0;
}

#pragma mark --- 懒加载
- (UISearchBar *)searchBar {
    
    if(_searchBar == nil) {
        _searchBar = [[UISearchBar alloc] init];
        _searchBar.delegate = self;
        _searchBar.autocorrectionType = UITextAutocapitalizationTypeNone;
        _searchBar.keyboardType = UIKeyboardTypeDefault;
        _searchBar.placeholder=@"请输入搜索内容";
        [_searchBar setShowsCancelButton:NO];
        for (__strong id cc in [_searchBar.subviews[0] subviews]) {
            if ([cc isKindOfClass:NSClassFromString(@"UISearchBarBackground")]) {
                [cc removeFromSuperview];
                cc = nil;
            }
            if ([cc isKindOfClass:NSClassFromString(@"UISearchBarTextField")]) {
                ((UITextField *)cc).backgroundColor = [UIColor colorWithRed:0.7373 green:0.7373 blue:0.7373 alpha:1.0];
                ((UITextField *)cc).font = [UIFont systemFontOfSize:12];
            }
        }
        UIView *view = [UIView new];
        view.backgroundColor = [UIColor colorWithWhite:0.855 alpha:1.000];
        view.frame = CGRectMake(0, 43.5, [UIScreen mainScreen].bounds.size.width, 0.5);
        [self addSubview:view];
        [self addSubview:_searchBar];
    }
    return _searchBar;
}

- (UITableView *)listTableView {
    
    if(_listTableView == nil) {
        _listTableView = [[UITableView alloc] init];
        _listTableView.delegate = self;
        _listTableView.dataSource = self;
        _listTableView.tableFooterView = [UIView new];
        _listTableView.mj_header = [MJRefreshStateHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadNewData)];
        _listTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(moreInformation)];
        [_listTableView registerClass:[WYHistoryTableViewCell class] forCellReuseIdentifier:@"listCell"];
        [self addSubview:_listTableView];
    }
    return _listTableView;
}

- (UITableView *)statuTableView {
    
    if(_statuTableView == nil) {
        _statuTableView = [[UITableView alloc] init];
        _statuTableView.delegate = self;
        _statuTableView.dataSource = self;
        _statuTableView.bounces = NO;
        _statuTableView.layer.borderWidth = 1;
        _statuTableView.layer.cornerRadius = 5.0;
        _statuTableView.layer.borderColor = [[UIColor lightGrayColor] CGColor];
        [_statuTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"statuCell"];
    }
    return _statuTableView;
}

- (UIButton *)blankBtn {
    
    if(_blankBtn == nil) {
        _blankBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _blankBtn.backgroundColor = [UIColor colorWithRed:0.4198 green:0.4198 blue:0.4198 alpha:0.0];
        [_blankBtn addTarget:self action:@selector(hiddenTableView) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.blankBtn];
    }
    return _blankBtn;
}

- (NSMutableArray *)dataArray {
    
    if(_dataArray == nil) {
        _dataArray = [NSMutableArray array];
    }
    return _dataArray;
}

- (NSArray *)statuArray {
    
    if(_statuArray == nil) {
        _statuArray = [[NSArray alloc] init];
        _statuArray = @[@"手机号", @"用户 ID", @"昵称", @"消息内容"];
    }
    return _statuArray;
}

-(void)removeBlankView{}

#pragma mark --- UITableViewDelegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (tableView == self.listTableView) {
        return self.dataArray.count;
    }
    else if (tableView == self.statuTableView) {
        return 4;
    }
    return 4;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (tableView == self.listTableView) {
        static NSString *identifier = @"listCell";
        WYHistoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
        [cell setRecordContentByHistoryModel:self.dataArray[indexPath.row]];
        return cell;
    }
    else if (tableView == self.statuTableView) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"statuCell" forIndexPath:indexPath];
        cell.textLabel.font = [UIFont systemFontOfSize:12];
        cell.textLabel.text = self.statuArray[indexPath.row];
        return cell;
    }
    return [UITableViewCell new];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (tableView == self.statuTableView) {
        self.selectIndexOfStatu = indexPath.row;
        [self hiddenTableView];
    }
    else if (tableView == self.listTableView) {
        [MobClick event:@"msgHistory_userTabelViewCell_click"];
        WYHistoryTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        [[RCIM sharedRCIM] setUserIcon:[UIImage imageNamed:@"chatroom_userIcon_xiaolai"]];
        [[RCIM sharedRCIM] setGeniusIcon:[cell getImage]];
        self.numberOfSelectedCell = indexPath.row;
        if ([self.sourceViewDelegate respondsToSelector:@selector(pushChatTopView:andIconImage:)]) {
            [self.sourceViewDelegate pushChatTopView:self.dataArray[indexPath.row] andIconImage:[cell getImage]];
        }
    }
}

-(void)hiddenTableView{
    
    [self endEditing:YES];
    [self.blankBtn removeFromSuperview];
    [self.statuTableView removeFromSuperview];
    self.blankBtn = nil;
    self.statuTableView = nil;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (tableView == self.listTableView) {
        return 70;
    }
    else if (tableView == self.statuTableView) {
        return 30;
    }
    return 10;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]){
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    cell.preservesSuperviewLayoutMargins = NO;
}

#pragma mark --- UISearchBarDelegate
-(BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar{
    
    return YES;
}

-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    
    [MobClick event:@"msgHistory_searchBar_click"];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"searchMsg" object:self];
}

-(void)dealloc{
    
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}


@end
