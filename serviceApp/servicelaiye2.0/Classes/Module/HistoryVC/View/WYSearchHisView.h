//
//  WYSearchHisView.h
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/4/9.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import <UIKit/UIKit.h>

@class WYHistoryModel;

@protocol WYSearchHisViewDelegate <NSObject>

-(void)pushChatMsgView:(WYHistoryModel *)historyModel andIconImage:(UIImage *)iconImage;

@end

@interface WYSearchHisView : UIView

@property (nonatomic, weak) id <WYSearchHisViewDelegate> searchHisViewDelegate;

-(void)removeSubViews;

-(void)backNaviView;

-(void)changeNaviView;

-(void)clickAnimate;

@end
