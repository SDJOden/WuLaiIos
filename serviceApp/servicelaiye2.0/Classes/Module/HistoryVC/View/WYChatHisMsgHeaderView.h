//
//  WYChatHisMsgHeaderView.h
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/4/6.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import <UIKit/UIKit.h>

@class WYHistoryModel;
@class WYConnectListModel;

@protocol WYChatHisMsgHeaderViewDelegate <NSObject>

-(void)titleMenuClick:(UIButton *)button;

-(void)changeUserInfo;

-(void)didTouchShowUserInformationButton:(BOOL)isShow;

@end

@interface WYChatHisMsgHeaderView : UIView

@property (nonatomic, weak) id <WYChatHisMsgHeaderViewDelegate> chatHeaderViewDelegate;

+(instancetype)createChatHisMsgHeaderView:(WYHistoryModel *)historyModel;

-(instancetype)initChatHisMsgHeaderView:(WYHistoryModel *)historyModel;

+(instancetype)createChatHeaderView:(WYConnectListModel *)connectListModel;

-(instancetype)initChatHeaderView:(WYConnectListModel *)connectListModel;

-(void)changeSubViewStyle;

-(void)restoreSubViewStyle:(WYConnectListModel *)connectListModel;

-(void)changeOrderBtnTitle;

@end
