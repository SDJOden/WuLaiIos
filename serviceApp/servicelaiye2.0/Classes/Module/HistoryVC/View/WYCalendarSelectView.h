//
//  WYCalendarSelectView.h
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/4/12.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol WYCalendarSelectViewDelegate <NSObject>

-(void)searchListChangeWithButtonClick:(NSString *)dateStr;

-(void)calendarSelectDidClicked:(UIButton *)sender;

@end

@interface WYCalendarSelectView : UIView

@property (nonatomic, weak) id <WYCalendarSelectViewDelegate> calendarSelectViewDelegate;

-(void)changeSelectDate:(NSDate *)date;

@end
