//
//  WYSearchMsgView.m
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/4/12.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import "WYSearchMsgView.h"
#import "WYHistoryTableViewCell.h"
#import "MJRefresh.h"

@interface WYSearchMsgView () <UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UIButton *statuBtn;

@property (nonatomic, strong) UITableView *statuTableView;

@property (nonatomic, strong) UIView *searchBackView;

@property (nonatomic, strong) UISearchBar *searchBar;

@property (nonatomic, strong) UITableView *searchHisTableView;

@property (nonatomic, assign) int page;

@property (nonatomic, strong) UITableView *listTableView;

@property (nonatomic, strong) NSMutableArray *dataArray;

@property (nonatomic, strong) NSArray *statuArray;
/**
 *  蒙版Btn
 */
@property (nonatomic, strong) UIButton *blankBtn;
/**
 *  选择的搜索状态cell号码
 */
@property (nonatomic, assign) NSInteger selectIndexOfStatu;

@end

@implementation WYSearchMsgView

- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initialize];
    }
    return self;
}

// 布局
- (void)initialize {
    
    self.backgroundColor = [UIColor whiteColor];
    
    [self.searchBackView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self);
        make.height.equalTo(44);
        make.top.equalTo(0);
    }];
    
    [self.searchBar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(8);
        make.right.equalTo(-8);
        make.bottom.equalTo(-8);
        make.height.equalTo(28);
    }];
    
    [self.listTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.searchBackView.bottom);
        make.left.right.bottom.equalTo(self);
    }];
}
/**
 *  初始化参数
 */
-(void)rcinit{
    
    self.page = 1;
    self.selectIndexOfStatu = 0;
}

-(NSDictionary *)getTextDict{
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithCapacity:4];
    switch (self.selectIndexOfStatu) {
        case 0:
            dict[@"phone"] = self.searchBar.text;
            break;
        case 1:
            dict[@"user_id"] = self.searchBar.text;
            break;
        case 2:
            dict[@"nickname"] = self.searchBar.text;
            break;
        case 3:
            dict[@"msg"] = self.searchBar.text;
        default:
            break;
    }
    return [dict copy];
}

-(void)requestMsgData{
    
    NSDictionary *dict = [self getTextDict];
    __weak typeof (self) __weakSelf = self;
    [[SDJNetworkTools sharedTools]messagesSearch:[SDJUserAccount loadAccount].token p:@(self.page).stringValue ps:FreshCellOnceNumber msg:dict[@"msg"] finished:^(NSDictionary<NSString *,id> * _Nullable result, NSError * _Nullable error) {
        if (error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [__weakSelf.searchHisTableView.mj_header endRefreshing];
                [__weakSelf.searchHisTableView.mj_footer endRefreshing];
            });
        }
        if (![result[@"error"] isEqual:[NSNull null]]) {//报错
            NSNumber *code = result[@"error"][@"error_code"];
            if (code.integerValue == 20000) {
                NSString *UserPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).firstObject stringByAppendingPathComponent:@"account.plist"];
                [[NSFileManager defaultManager]removeItemAtPath:UserPath error:nil];
                [[RCIMClient sharedRCIMClient] logout];
                // 发送通知让appdelegate去切换根视图为聊天列表界面
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"WYRootViewControllerSwitchNotification" object:@"0"];
                });
            }
        } else {
            NSArray *historyListArray = result[@"data"][@"list"];
            if (__weakSelf.page == 1) {
                [__weakSelf.dataArray removeAllObjects];
            }
            for (NSDictionary *dic in historyListArray) {
                WYHistoryModel *listModel = [WYTool createMsgSearchListModelByDict:dic];
                [__weakSelf.dataArray addObject:listModel];
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                [__weakSelf.listTableView reloadData];
                [__weakSelf.listTableView.mj_header endRefreshing];
                [__weakSelf.listTableView.mj_footer endRefreshing];
                if (__weakSelf.dataArray.count >= ((NSNumber *)result[@"data"][@"size"]).integerValue) {
                    [__weakSelf.listTableView.mj_footer endRefreshingWithNoMoreData];
                }
            });
        }
    }];
}

-(void)requestSearchData{
    
    NSDictionary *dict = [self getTextDict];
    __weak typeof(self) __weakSelf = self;
    [[SDJNetworkTools sharedTools]usersSearch:[SDJUserAccount loadAccount].token p:/*@"1"*/@(self.page).stringValue ps:FreshCellOnceNumber user_id:dict[@"user_id"] phone:dict[@"phone"] nickname:dict[@"nickname"] finished:^(NSDictionary<NSString *,id> * _Nullable result, NSError * _Nullable error) {
        if (error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [__weakSelf.searchHisTableView.mj_header endRefreshing];
                [__weakSelf.searchHisTableView.mj_footer endRefreshing];
            });
        }
        if (![result[@"error"] isEqual:[NSNull null]]) {//报错
            NSNumber *code = result[@"error"][@"error_code"];
            if (code.integerValue == 20000) {
                NSString *UserPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).firstObject stringByAppendingPathComponent:@"account.plist"];
                [[NSFileManager defaultManager]removeItemAtPath:UserPath error:nil];
                [[RCIMClient sharedRCIMClient] logout];
                // 发送通知让appdelegate去切换根视图为聊天列表界面
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"WYRootViewControllerSwitchNotification" object:@"0"];
                });
            }
        } else {
            NSArray *historyListArray = result[@"data"][@"list"];
            if (__weakSelf.page == 1) {
                [__weakSelf.dataArray removeAllObjects];
            }
            for (NSDictionary *dic in historyListArray) {
                WYHistoryModel *listModel = [WYTool createHistoryModelByDict:dic];
                [__weakSelf.dataArray addObject:listModel];
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                [__weakSelf.listTableView reloadData];
                [__weakSelf.listTableView.mj_header endRefreshing];
                [__weakSelf.listTableView.mj_footer endRefreshing];
                if (historyListArray.count < FreshCellOnceNumber.integerValue) {
                    [__weakSelf.listTableView.mj_footer endRefreshingWithNoMoreData];
                }
            });
        }
    }];
}

-(void)loadNewData{
    
    self.page = 1;
    if (self.selectIndexOfStatu == 3) {
        [self requestMsgData];
    }else {
        [self requestSearchData];
    }
}

-(void)moreInformation{
    
    self.page ++;
    if (self.selectIndexOfStatu == 3) {
        [self requestMsgData];
    }else {
        [self requestSearchData];
    }
}

-(void)loseEditingStatu{
    
    [self endEditing:YES];
    for(id control in [self.searchBar.subviews[0] subviews])
    {
        if ([control isKindOfClass:[UITextField class]]) {
            ((UITextField *)control).leftView = nil;
        }
        if ([control isKindOfClass:[UIButton class]])
        {
            UIButton * btn =(UIButton *)control;
            btn.enabled = YES;
        }
    }
}

#pragma mark --- UISearchBarDelegate
-(BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar{
    
    for(id control in [self.searchBar.subviews[0] subviews])
    {
        if ([control isKindOfClass:[UITextField class]]) {
            ((UITextField *)control).leftView = self.statuBtn;
        }
    }
    return YES;
}
/**
 *  搜索
 */
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    
    self.page = 1;
    if (self.selectIndexOfStatu == 3) {
        [self requestMsgData];
    }else {
        [self requestSearchData];
    }
    [self loseEditingStatu];
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    
    [self endEditing:YES];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"searchMsgCancel" object:nil];
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    [self loseEditingStatu];
}

-(void)backNaviView{
    
    [self.searchBar setShowsCancelButton:NO animated:YES];
    
    self.searchBackView.backgroundColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1.0];
    
    for (id cc in [_searchBar.subviews[0] subviews]) {
        if ([cc isKindOfClass:NSClassFromString(@"UISearchBarTextField")]) {
            ((UITextField *)cc).backgroundColor = [UIColor colorWithRed:0.7373 green:0.7373 blue:0.7373 alpha:1.0];
        }
    }
    
    [self.searchBackView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(44);
    }];
}

-(void)changeNaviView{
    
    self.searchBackView.backgroundColor = [UIColor colorWithRed:0.1922 green:0.2078 blue:0.2392 alpha:1.0];
    
    for (id cc in [_searchBar.subviews[0] subviews]) {
        if ([cc isKindOfClass:NSClassFromString(@"UISearchBarTextField")]) {
            ((UITextField *)cc).backgroundColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1.0];
        }
    }
    
    [self.searchBackView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(64);
    }];
}

-(void)clickAnimate{
    
    [self.searchBar setShowsCancelButton:YES animated:YES];
    
    for (__strong id cc in [_searchBar.subviews[0] subviews]) {
        if ([cc isKindOfClass:NSClassFromString(@"UINavigationButton")]) {
            [(UIButton *)cc setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            break;
        }
    }
    
    [self.searchBar becomeFirstResponder];
}

#pragma mark --- StatuBtn点击事件
-(void)addStatuView{
    
    [self addSubview:self.blankBtn];
    [self addSubview:self.statuTableView];
    [self.blankBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.bottom.equalTo(self);
    }];
    
    [self.statuTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.searchBar.mas_top);
        make.left.equalTo(self).offset(15);
        make.width.equalTo(70);
        make.height.equalTo(120);
    }];
}

#pragma mark --- UITableViewDelegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (tableView == self.listTableView) {
        return self.dataArray.count;
    }
    else if (tableView == self.statuTableView) {
        return 4;
    }
    return 4;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (tableView == self.listTableView) {
        static NSString *identifier = @"listCell";
        WYHistoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
        [cell setRecordContentByHistoryModel:self.dataArray[indexPath.row]];
        return cell;
    }
    else if (tableView == self.statuTableView) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"statuCell" forIndexPath:indexPath];
        cell.textLabel.font = [UIFont systemFontOfSize:12];
        cell.textLabel.text = self.statuArray[indexPath.row];
        return cell;
    }
    return [UITableViewCell new];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (tableView == self.statuTableView) {
        switch (indexPath.row) {
            case 0:
                [MobClick event:@"msgHistory_searchPhone_click"];
                break;
            case 1:
                [MobClick event:@"msgHistory_searchUserID_click"];
                break;
            case 2:
                [MobClick event:@"msgHistory_searchNickname_click"];
                break;
            case 3:
                [MobClick event:@"msgHistory_searchMsg_click"];
                break;
            default:
                break;
        }
        self.selectIndexOfStatu = indexPath.row;
        [self.statuBtn setTitle:self.statuArray[self.selectIndexOfStatu] forState:UIControlStateNormal];
        [self hiddenTableView];
    }
    else if (tableView == self.listTableView) {
        [MobClick event:@"msgHistory_userTabelViewCell_click"];
        WYHistoryTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        //设置头像
        [[RCIM sharedRCIM] setUserIcon:[UIImage imageNamed:@"chatroom_userIcon_xiaolai"]];
        [[RCIM sharedRCIM] setGeniusIcon:[cell getImage]];
        if ([self.searchMsgViewDelegate respondsToSelector:@selector(pushChatTopView:andIconImage:)]) {
            [[NSNotificationCenter defaultCenter]postNotificationName:@"searchMsgMove" object:nil];
            [self.searchMsgViewDelegate pushChatTopView:self.dataArray[indexPath.row] andIconImage:[cell getImage]];
        }
    }
}

-(void)hiddenTableView{
    
    [self.blankBtn removeFromSuperview];
    [self.statuTableView removeFromSuperview];
    self.blankBtn = nil;
    self.statuTableView = nil;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (tableView == self.listTableView) {
        return 70;
    }
    else if (tableView == self.statuTableView) {
        return 30;
    }
    return 10;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]){
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    cell.preservesSuperviewLayoutMargins = NO;
}

#pragma mark --- 懒加载
- (UIView *)searchBackView {
    
    if(_searchBackView == nil) {
        _searchBackView = [[UIView alloc] init];
        _searchBackView.backgroundColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1.0];
        [self addSubview:_searchBackView];
    }
    return _searchBackView;
}

- (UIButton *)statuBtn {
    
    if(_statuBtn == nil) {
        _statuBtn = [UIButton buttonWithType:UIButtonTypeSystem];
        [_statuBtn setTitle:@"手机号" forState:UIControlStateNormal];
        [_statuBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        _statuBtn.titleLabel.font = [UIFont systemFontOfSize:12];
        _statuBtn.backgroundColor = [UIColor clearColor];
        _statuBtn.frame = CGRectMake(0, 0, 50, 30);
        [_statuBtn addTarget:self action:@selector(addStatuView) forControlEvents:UIControlEventTouchUpInside];
    }
    return _statuBtn;
}

- (UISearchBar *)searchBar {
    
    if(_searchBar == nil) {
        _searchBar = [[UISearchBar alloc] init];
        _searchBar.delegate = self;
        _searchBar.autocorrectionType = UITextAutocapitalizationTypeNone;
        _searchBar.keyboardType = UIKeyboardTypeDefault;
        _searchBar.placeholder=@"请输入搜索内容";
        [_searchBar setShowsCancelButton:NO];
        for (__strong id cc in [_searchBar.subviews[0] subviews]) {
            if ([cc isKindOfClass:NSClassFromString(@"UISearchBarBackground")]) {
                [cc removeFromSuperview];
                cc = nil;
            }
            if ([cc isKindOfClass:NSClassFromString(@"UISearchBarTextField")]) {
                ((UITextField *)cc).backgroundColor = [UIColor colorWithRed:0.7373 green:0.7373 blue:0.7373 alpha:1.0];
                ((UITextField *)cc).font = [UIFont systemFontOfSize:12];
            }
        }
        [self.searchBackView addSubview:_searchBar];
    }
    return _searchBar;
}

- (UITableView *)statuTableView {
    
    if(_statuTableView == nil) {
        _statuTableView = [[UITableView alloc] init];
        _statuTableView.delegate = self;
        _statuTableView.dataSource = self;
        _statuTableView.bounces = NO;
        _statuTableView.layer.borderWidth = 1;
        _statuTableView.layer.cornerRadius = 5.0;
        _statuTableView.layer.borderColor = [[UIColor lightGrayColor] CGColor];
        [_statuTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"statuCell"];
    }
    return _statuTableView;
}

- (NSArray *)statuArray {
    
    if(_statuArray == nil) {
        _statuArray = [[NSArray alloc] init];
        _statuArray = @[@"手机号", @"用户 ID", @"昵称", @"消息内容"];
    }
    return _statuArray;
}

- (UIButton *)blankBtn {
    
    if(_blankBtn == nil) {
        _blankBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _blankBtn.backgroundColor = [UIColor clearColor];
        [_blankBtn addTarget:self action:@selector(hiddenTableView) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.blankBtn];
    }
    return _blankBtn;
}

- (UITableView *)listTableView {
    
    if(_listTableView == nil) {
        _listTableView = [[UITableView alloc] init];
        _listTableView.delegate = self;
        _listTableView.dataSource = self;
        _listTableView.tableFooterView = [UIView new];
        _listTableView.mj_header = [MJRefreshStateHeader headerWithRefreshingTarget:self refreshingAction:@selector(loadNewData)];
        _listTableView.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(moreInformation)];
        [_listTableView registerClass:[WYHistoryTableViewCell class] forCellReuseIdentifier:@"listCell"];
        [self addSubview:_listTableView];
    }
    return _listTableView;
}

- (NSMutableArray *)dataArray {
    
	if(_dataArray == nil) {
		_dataArray = [[NSMutableArray alloc] init];
	}
	return _dataArray;
}

@end
