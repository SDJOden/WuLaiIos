//
//  WYHistorySegmentView.h
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/4/1.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol WYHistorySegmentViewDelegate <NSObject>

-(void)changeSegmentSign:(NSInteger)selectedIndex;

@end

@interface WYHistorySegmentView : UIView

@property (nonatomic, strong) NSArray *titleArray;

@property (nonatomic, weak) id <WYHistorySegmentViewDelegate> historySegmetViewDelegate;

@end
