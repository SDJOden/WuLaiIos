//
//  WYHistoryMessageViewController.m
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/4/1.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import "WYHistoryMessageViewController.h"
#import "WYHistorySegmentView.h"
#import "WYHistoryView.h"
#import "WYSourceView.h"
#import "WYSearchHisView.h"
#import "WYSearchMsgView.h"
#import "WYHistoryModel.h"
#import "WYHistoryUserMsgViewController.h"

@interface WYHistoryMessageViewController () <UIScrollViewDelegate, WYHistorySegmentViewDelegate, WYHistoryViewDelegate, WYSourceViewDelegate, WYSearchHisViewDelegate, WYSearchMsgViewDelegate>

@property (nonatomic, strong) WYHistorySegmentView *segmentView;

@property (nonatomic, strong) UIScrollView *historyScorllView;

@property (nonatomic, strong) WYHistoryView *historyView;

@property (nonatomic, strong) WYSourceView *sourceView;

@property (nonatomic, strong) WYSearchHisView *searchHisView;

@property (nonatomic, strong) WYSearchMsgView *searchMsgView;

@end

@implementation WYHistoryMessageViewController

-(void)home{
    
    [self.view endEditing:YES];
    
    [self.sourceView removeBlankView];
    
    [super home];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    if (self.historyScorllView.contentOffset.x == [UIScreen mainScreen].bounds.size.width) {
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"wyRequestTableViewData" object:@"1"];
    }
    else if (self.historyScorllView.contentOffset.x == 0) {
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"wyRequestTableViewData" object:@"0"];
    }
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self setSubViews];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(searchHis:) name:@"searchHis" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(searchMsg:) name:@"searchMsg" object:nil];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(searchHisCancel:) name:@"searchHisCancel" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(searchMsgCancel:) name:@"searchMsgCancel" object:nil];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(searchHisMove:) name:@"searchHisMove" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(searchMsgMove:) name:@"searchMsgMove" object:nil];
    
//    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(searchViewBack:) name:@"searchViewBack" object:nil];
}

-(void)searchHis:(NSNotification *)noti{
    
    self.searchHisView.frame = CGRectMake(0, 64 + 44, [UIScreen mainScreen].bounds.size.width, 44);
    
    [UIView animateWithDuration:0.25f animations:^{
        self.searchHisView.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
        [self.searchHisView changeNaviView];
    } completion:^(BOOL finished) {
        [self.searchHisView clickAnimate];
    }];
}

-(void)searchMsg:(NSNotification *)noti{
    
    self.searchMsgView.frame = CGRectMake(0, 64 + 44, [UIScreen mainScreen].bounds.size.width, 44);
    [UIView animateWithDuration:0.25f animations:^{
        self.searchMsgView.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
        [self.searchMsgView changeNaviView];
    } completion:^(BOOL finished) {
        [self.searchMsgView clickAnimate];
    }];
}

-(void)searchHisMove:(NSNotification *)noti{
    
    [UIView animateWithDuration:0.25f animations:^{
        self.searchHisView.transform = CGAffineTransformMakeTranslation(- self.searchHisView.w, 0);
    }];
}

-(void)searchMsgMove:(NSNotification *)noti{
    
    [UIView animateWithDuration:0.25f animations:^{
        self.searchMsgView.transform = CGAffineTransformMakeTranslation(- self.searchMsgView.w, 0);
    }];
}

//-(void)searchViewBack:(NSNotification *)noti{
//    
//    if (_searchMsgView) {
//        [UIView animateWithDuration:0.25f animations:^{
//            _searchMsgView.transform = CGAffineTransformIdentity;
//        }];
//        return;
//    }
//    else if (_searchHisView) {
//        [UIView animateWithDuration:0.25f animations:^{
//            _searchHisView.transform = CGAffineTransformIdentity;
//        }];
//        return;
//    }
//}

-(void)searchHisCancel:(NSNotification *)noti {
    
    __weak __typeof__(self) weakSelf = self;
    
    self.searchHisView.frame = CGRectMake(0, 20, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    [self.searchHisView removeSubViews];
    
    [UIView animateWithDuration:0.25f animations:^{
        weakSelf.searchHisView.frame = CGRectMake(0, 64 + 44, [UIScreen mainScreen].bounds.size.width, 44);
        [weakSelf.searchHisView backNaviView];
    } completion:^(BOOL finished) {
        [weakSelf.searchHisView removeFromSuperview];
        weakSelf.searchHisView = nil;
    }];
    
}

-(void)searchMsgCancel:(NSNotification *)noti{
    
    __weak __typeof__(self) weakSelf = self;
    
    self.searchMsgView.frame = CGRectMake(0, 20, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
    
    [UIView animateWithDuration:0.25f animations:^{
        weakSelf.searchMsgView.frame = CGRectMake(0, 64 + 44, [UIScreen mainScreen].bounds.size.width, 44);
        [weakSelf.searchMsgView backNaviView];
    } completion:^(BOOL finished) {
        [weakSelf.searchMsgView removeFromSuperview];
        weakSelf.searchMsgView = nil;
    }];
}

-(void)setSubViews{
    
    [self.segmentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self.view);
        make.height.equalTo(44);
    }];
    
    [self.historyScorllView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.segmentView.bottom);
        make.left.right.equalTo(self.segmentView);
        make.bottom.equalTo(self.view);
    }];
}

-(void)changeSegmentSign:(NSInteger)selectedIndex{
    
    [self.view endEditing:YES];
    
    [self.sourceView removeBlankView];
    
    [MobClick event:@"history_msgHistorySegment_click"];
    
    switch (selectedIndex) {
        case 0:{
            [self.historyView loadNewData];
            [_historyScorllView setContentOffset:CGPointMake(0, 0) animated:YES];
            break;
        }
        case 1:{
            [self.sourceView loadNewData];
            [_historyScorllView setContentOffset:CGPointMake(self.view.w, 0) animated:YES];
            break;
        }
        default:
            break;
    }
}

- (WYHistorySegmentView *)segmentView {
    
	if(_segmentView == nil) {
		_segmentView = [[WYHistorySegmentView alloc] init];
        _segmentView.titleArray = @[@"历史",@"消息记录"];
        _segmentView.historySegmetViewDelegate = self;
        _segmentView.userInteractionEnabled = YES;
        [self.view addSubview:_segmentView];
	}
	return _segmentView;
}

- (UIScrollView *)historyScorllView {
    
	if(_historyScorllView == nil) {
		_historyScorllView = [[UIScrollView alloc] init];
        _historyScorllView.scrollEnabled = NO;
        _historyScorllView.pagingEnabled = YES;
        _historyScorllView.showsHorizontalScrollIndicator = NO;
        _historyScorllView.delegate = self;
        _historyScorllView.contentSize = CGSizeMake(2 * self.view.w, 0);
#warning to do 基本的代码素养, 在get里面调用get方法
        [_historyScorllView addSubview:self.historyView];
        [_historyScorllView addSubview:self.sourceView];
//        [self.historyScorllView addSubview:self.historyView];
//        [self.historyScorllView addSubview:self.sourceView];
        [self.view addSubview:_historyScorllView];
	}
	return _historyScorllView;
}

- (WYHistoryView *)historyView {
    if(_historyView == nil) {
        _historyView = [[WYHistoryView alloc] init];
        _historyView.frame = CGRectMake(0, 0, self.view.w, self.view.h - 44 - 64 - 48);
        _historyView.historyViewDelegate = self;
    }
    return _historyView;
}

- (WYSourceView *)sourceView {
    
	if(_sourceView == nil) {
		_sourceView = [[WYSourceView alloc] init];
        _sourceView.frame = CGRectMake(self.view.w, 0, self.view.w, self.view.h - 44 - 64 - 48);
        _sourceView.sourceViewDelegate = self;
	}
	return _sourceView;
}

- (WYSearchHisView *)searchHisView {
    
    if(_searchHisView == nil) {
        _searchHisView = [[WYSearchHisView alloc] init];
        _searchHisView.searchHisViewDelegate = self;
        [self.tabBarController.view addSubview:_searchHisView];
    }
    return _searchHisView;
}

- (WYSearchMsgView *)searchMsgView {
    
    if(_searchMsgView == nil) {
        _searchMsgView = [[WYSearchMsgView alloc] init];
        _searchMsgView.searchMsgViewDelegate = self;
        [self.tabBarController.view addSubview:_searchMsgView];
    }
    return _searchMsgView;
}

#pragma mark --- UIScorllViewDelegate
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
#warning  to do 这里有问题.
    if (scrollView.contentOffset.x == [UIScreen mainScreen].bounds.size.width) {
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"wyRequestTableViewData" object:@"1"];
    }
    else if (scrollView.contentOffset.x == 0) {
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"wyRequestTableViewData" object:@"0"];
    }
}

#pragma mark --- WYHisotryViewDelegate
-(void)pushChatMsgView:(WYHistoryModel *)historyModel andIconImage:(UIImage *)iconImage{
    [self.searchHisView removeFromSuperview];
    self.searchHisView = nil;
    WYHistoryUserMsgViewController *hisMsgVC = [[WYHistoryUserMsgViewController alloc]init];
    hisMsgVC.iconImage = iconImage;
    hisMsgVC.nickName = historyModel.nickname;
    hisMsgVC.historyModel = historyModel;
    hisMsgVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:hisMsgVC animated:YES];
    
}

-(void)pushChatTopView:(WYHistoryModel *)historyModel andIconImage:(UIImage *)iconImage{
    [self.searchMsgView removeFromSuperview];
    self.searchMsgView = nil;
    WYHistoryUserMsgViewController *hisMsgVC = [[WYHistoryUserMsgViewController alloc]init];
    hisMsgVC.iconImage = iconImage;
    hisMsgVC.nickName = historyModel.nickname;
    hisMsgVC.historyModel = historyModel;
    hisMsgVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:hisMsgVC animated:YES];
}

-(void)dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
