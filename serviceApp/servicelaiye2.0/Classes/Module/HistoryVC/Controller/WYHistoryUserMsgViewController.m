//
//  WYHistoryUserMsgViewController.m
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/8/19.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import "WYHistoryUserMsgViewController.h"
#import "WYHistoryModel.h"
#import "WYConnectListModel.h"
#import "UIBarButtonItem+Extension.h"

@interface WYHistoryUserMsgViewController ()

@end

@implementation WYHistoryUserMsgViewController

-(void)setHistoryModel:(WYHistoryModel *)historyModel{
    
    _historyModel = historyModel;
    
    [self.connectListModel makeModelWithHistoryModel:historyModel];
    
    self.userId = [@(historyModel.user_id).stringValue copy];
    
    [self rcinit];
    
    [self loadNewMsgData];
}

-(void)addLeftItem{
    
    UIBarButtonItem *leftItem = [UIBarButtonItem itemWithTitle:@"历史" target:self action:@selector(popToVC) left:YES];
    
    self.navigationItem.leftBarButtonItem = leftItem;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
}

@end
