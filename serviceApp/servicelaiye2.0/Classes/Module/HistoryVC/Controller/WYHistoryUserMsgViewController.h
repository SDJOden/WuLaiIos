//
//  WYHistoryUserMsgViewController.h
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/8/19.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import "WYWaitUserHisMsgViewController.h"

@class WYHistoryModel;

@interface WYHistoryUserMsgViewController : WYWaitUserHisMsgViewController

@property (nonatomic, strong) WYHistoryModel *historyModel;

@end
