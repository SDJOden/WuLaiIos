//
//  WYHistoryModel.h
//  servicelaiye2.0
//
//  Created by 汪洋 on 15/12/28.
//  Copyright © 2015年 shengdong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WYHistoryModel : NSObject

@property (nonatomic, copy) NSString *avatar_url;

@property (nonatomic, copy) NSString *db_insert_time;

@property (nonatomic, assign) int level;

@property (nonatomic, assign) int order_count;

@property (nonatomic, strong) NSString *rc_group_id;

@property (nonatomic, copy) NSString *rcu_id;

@property (nonatomic, strong) NSDictionary *region;

@property (nonatomic, assign) NSInteger up_score;

@property (nonatomic, strong) NSArray *up_tags;

@property (nonatomic, strong) NSDictionary *wechat;

@property (nonatomic, copy) NSString *wechat_openid;

@property (nonatomic, copy) NSString *headimgurl;

@property (nonatomic, assign) long inner_uid;

@property (nonatomic, copy) NSString *nickname;

@property (nonatomic, assign) long ts;

@property (nonatomic, assign) int user_id;

@property (nonatomic, copy) NSString *realname;

@property (nonatomic, copy) NSString *username;

@property (nonatomic, copy) NSDictionary *vip;

@property (nonatomic, copy) NSString *phone;

@end
