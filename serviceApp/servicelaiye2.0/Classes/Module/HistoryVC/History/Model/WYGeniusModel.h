//
//  WYGeniusModel.h
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/1/28.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import <Foundation/Foundation.h>

//助理模型

@interface WYGeniusModel : NSObject

@property (nonatomic, copy) NSString *avatar_url;

@property (nonatomic, copy) NSString *level;

@property (nonatomic, copy) NSString *status;

@property (nonatomic, copy) NSString *genius_id;

@property (nonatomic, copy) NSString *username;

@property (nonatomic, copy) NSString *real_name;

@end
