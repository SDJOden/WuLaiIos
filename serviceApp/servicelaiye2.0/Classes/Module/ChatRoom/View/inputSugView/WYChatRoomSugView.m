//
//  WYChatRoomSugView.m
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/7/5.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import "WYChatRoomSugView.h"

@interface WYChatRoomSugView ()

@property (nonatomic, strong) UIImageView *firstView;

@property (nonatomic, strong) UIImageView *secondView;

@property (nonatomic, strong) UIImageView *thirdView;

@property (nonatomic, strong) UIImageView *selectView;

@property (nonatomic, assign) CGFloat x;

@property (nonatomic, assign) CGFloat y;

@property (nonatomic, strong) UILabel *firstLabel;

@property (nonatomic, strong) UILabel *secondLabel;

@property (nonatomic, strong) UILabel *thirdLabel;

@property (nonatomic, assign) int num;

@property (nonatomic, assign) BOOL isMoveStatu;

@end

@implementation WYChatRoomSugView

-(instancetype)init{
    
    if (self = [super init]) {
        self.backgroundColor = [UIColor clearColor];
        _isMoveStatu = NO;
    }
    return self;
}

-(void)setContentArr:(NSArray *)contentArr{
    
    _sugTextArr = [contentArr mutableCopy];
    
    if (self.sugTextArr.count >= 3) {
        _num = 3;
        self.thirdView.frame = CGRectMake(20, 0, 135, 70);
        self.thirdLabel.frame = CGRectMake(5, 5, 125, 55);
        self.thirdLabel.text = self.sugTextArr[_num-1];
        self.secondView.frame = CGRectMake(10, 10, 135, 70);
        self.secondLabel.frame = CGRectMake(5, 5, 125, 55);
        self.secondLabel.text = self.sugTextArr[_num-2];
        self.firstView.frame = CGRectMake(0, 20, 135, 70);
        self.firstLabel.frame = CGRectMake(5, 5, 125, 55);
        self.firstLabel.text = self.sugTextArr[_num-3];
    } else if (self.sugTextArr.count == 2) {
        _num = 2;
        self.secondView.frame = CGRectMake(10, 10, 135, 70);
        self.secondLabel.frame = CGRectMake(5, 5, 125, 55);
        self.secondLabel.text = self.sugTextArr[_num-1];
        self.firstView.frame = CGRectMake(0, 20, 135, 70);
        self.firstLabel.frame = CGRectMake(5, 5, 125, 55);
        self.firstLabel.text = self.sugTextArr[_num-2];
    } else if (self.sugTextArr.count == 1) {
        _num = 1;
        self.firstView.frame = CGRectMake(0, 20, 135, 70);
        self.firstLabel.frame = CGRectMake(5, 5, 125, 55);
        self.firstLabel.text = self.sugTextArr[_num-1];
    }
    self.selectView = self.firstView;
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    UITouch *touch = [touches anyObject];
    CGPoint point = [touch locationInView:self.selectView];
    self.x = point.x;
    self.y = point.y;
}

-(void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{

    _isMoveStatu = YES;
    if (self.y < 0 || self.y > 135) {
        return ;
    }
    UITouch *touch = [touches anyObject];
    CGPoint point = [touch locationInView:self.selectView];
    
    _selectView.center = CGPointMake(_selectView.center.x + point.x - self.x, _selectView.center.y + point.y - self.y);
    CGFloat l = (_selectView.center.x-(135*0.5))*2;
    _selectView.transform = CGAffineTransformMakeRotation(180 * l / (M_PI * (50000+_selectView.center.y-55)));
    
    CGFloat distanceCenter = sqrt((_selectView.center.x - (135*0.5))*(_selectView.center.x - (135*0.5))+(_selectView.center.y - 55)*(_selectView.center.y - 55));
    _selectView.alpha = 1-1*distanceCenter/70;
}

-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    if (_num == self.sugTextArr.count + 3) {
        return ;
    }
    CGFloat distanceCenter = sqrt((_selectView.center.x - (135*0.5))*(_selectView.center.x - (135*0.5))+(_selectView.center.y - 55)*(_selectView.center.y - 55));
    if (distanceCenter>40) {
        if (_selectView.center.x<(135*0.5)) {
            [UIView animateWithDuration:0.2 animations:^{
                _selectView.center = CGPointMake(-30-135, _selectView.center.y);
            } completion:^(BOOL finished) {
                [self moveOldView];
            }];
        }else {
            [UIView animateWithDuration:0.2 animations:^{
                _selectView.center = CGPointMake([UIScreen mainScreen].bounds.size.width+30+135, _selectView.center.y);
            } completion:^(BOOL finished) {
                [self moveOldView];
            }];
        }
    }else {
        [UIView animateWithDuration:0.3 animations:^{
            _selectView.center = CGPointMake((135*0.5), 55);
            _selectView.transform = CGAffineTransformIdentity;
            _selectView.alpha = 1;
        }];
    }
    _isMoveStatu = NO;
}

-(void)moveOldView{
    
    if (_num - self.sugTextArr.count == 2) {
        [_firstView removeFromSuperview];
        _firstView = nil;
        [[NSNotificationCenter defaultCenter]postNotificationName:@"removeSugView" object:nil];
    } else if (_num - self.sugTextArr.count == 1) {
        _firstView = _secondView;
        _secondView = _thirdView;
        [_secondView removeFromSuperview];
        _secondView = nil;
        _selectView = _firstView;
        [UIView animateWithDuration:0.2 animations:^{
            _firstView.center = CGPointMake(_firstView.center.x-10, _firstView.center.y+10);
            _firstView.alpha = 1;
        } completion:^(BOOL finished) {}];
    } else if (_num == self.sugTextArr.count) {
        _firstView = _secondView;
        _secondView = _thirdView;
        _thirdView = _selectView;
        [_thirdView removeFromSuperview];
        _thirdView = nil;
        _selectView = _firstView;
        [UIView animateWithDuration:0.2 animations:^{
            _firstView.center = CGPointMake(_firstView.center.x-10, _firstView.center.y+10);
            _secondView.center = CGPointMake(_secondView.center.x-10, _secondView.center.y+10);
            _firstView.alpha = 1;
            _secondView.alpha = 0.66;
        } completion:^(BOOL finished) {}];
    } else {
        _firstView = _secondView;
        _secondView = _thirdView;
        _thirdView = _selectView;
        _thirdView.center = CGPointMake((135*0.5)+10+10+10, 35-10);
        _thirdView.transform = CGAffineTransformIdentity;
        _thirdView.alpha = 0;
        _selectView = _firstView;
        ((UILabel *)_thirdView.subviews[0]).text = self.sugTextArr[_num];
        [self sendSubviewToBack:_thirdView];
        [UIView animateWithDuration:0.2 animations:^{
            _firstView.center = CGPointMake(_firstView.center.x-10, _firstView.center.y+10);
            _secondView.center = CGPointMake(_secondView.center.x-10, _secondView.center.y+10);
            _thirdView.center = CGPointMake(_thirdView.center.x-10, _thirdView.center.y+10);
            _firstView.alpha = 1;
            _secondView.alpha = 0.66;
            _thirdView.alpha = 0;
        } completion:^(BOOL finished) {}];
    }
    _num ++;
}

- (UIImageView *)firstView {
    
    if(_firstView == nil) {
        _firstView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"chatroomPop"]];
        _firstView.alpha = 1;
        UITapGestureRecognizer* tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapped:)];
        [_firstView addGestureRecognizer:tapGesture];
        _firstView.userInteractionEnabled = YES;
        [self addSubview:_firstView];
    }
    return _firstView;
}

- (UIImageView *)secondView {
    
    if(_secondView == nil) {
        _secondView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"chatroomPop"]];
        _secondView.alpha = 0.66;
        UITapGestureRecognizer* tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapped:)];
        [_secondView addGestureRecognizer:tapGesture];
        _secondView.userInteractionEnabled = YES;
        [self addSubview:_secondView];
    }
    return _secondView;
}

- (UIImageView *)thirdView {
    
    if(_thirdView == nil) {
        _thirdView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"chatroomPop"]];
        _thirdView.alpha = 0;
        UITapGestureRecognizer* tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapped:)];
        [_thirdView addGestureRecognizer:tapGesture];
        _thirdView.userInteractionEnabled = YES;
        [self addSubview:_thirdView];
    }
    return _thirdView;
}

-(void)tapped:(UITapGestureRecognizer*)gesture{
    
    if (gesture.state == UIGestureRecognizerStateRecognized && _isMoveStatu == NO) {
        if ([self.wyChatRoomSugViewDelegate respondsToSelector:@selector(inputSugTextToTextField:)]) {
            [self.wyChatRoomSugViewDelegate inputSugTextToTextField:[_firstView.subviews.firstObject text]];
        }
    }
    else {
        [UIView animateWithDuration:0.3 animations:^{
            _selectView.center = CGPointMake((135*0.5), 55);
            _selectView.transform = CGAffineTransformIdentity;
            _selectView.alpha = 1;
        }];
        _isMoveStatu = NO;
    }
}

- (UILabel *)firstLabel {
    if(_firstLabel == nil) {
        _firstLabel = [[UILabel alloc] init];
        _firstLabel.font = [UIFont systemFontOfSize:12];
        _firstLabel.textColor = HEXCOLOR(0x7f7f7f);
        _firstLabel.numberOfLines = 3;
        _firstLabel.textAlignment = NSTextAlignmentCenter;
        [self.firstView addSubview:_firstLabel];
    }
    return _firstLabel;
}

- (UILabel *)secondLabel {
    
    if(_secondLabel == nil) {
        _secondLabel = [[UILabel alloc] init];
        _secondLabel.font = [UIFont systemFontOfSize:12];
        _secondLabel.textColor = HEXCOLOR(0x7f7f7f);
        _secondLabel.numberOfLines = 3;
        _secondLabel.textAlignment = NSTextAlignmentCenter;
        [self.secondView addSubview:_secondLabel];
    }
    return _secondLabel;
}

- (UILabel *)thirdLabel {
    
    if(_thirdLabel == nil) {
        _thirdLabel = [[UILabel alloc] init];
        _thirdLabel.textColor = HEXCOLOR(0x7f7f7f);
        _thirdLabel.font = [UIFont systemFontOfSize:12];
        _thirdLabel.numberOfLines = 3;
        _thirdLabel.textAlignment = NSTextAlignmentCenter;
        [self.thirdView addSubview:_thirdLabel];
    }
    return _thirdLabel;
}

- (NSArray *)sugTextArr {
    
	if(_sugTextArr == nil) {
		_sugTextArr = [[NSArray alloc] init];
	}
	return _sugTextArr;
}

@end
