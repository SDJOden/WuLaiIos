//
//  WYChatRoomSugView.h
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/7/5.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol WYChatRoomSugViewDelegate <NSObject>

-(void)inputSugTextToTextField:(NSString *)sugText;

-(void)closeInputSugView;

@end

@interface WYChatRoomSugView : UIView

@property (nonatomic, strong) NSArray *sugTextArr;

@property (nonatomic, weak) id <WYChatRoomSugViewDelegate> wyChatRoomSugViewDelegate;

-(void)setContentArr:(NSArray *)contentArr;

@end
