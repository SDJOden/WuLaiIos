//
//  SDJCardCollectionCell.h
//  geniuslaiye2.0
//
//  Created by 盛东 on 15/11/19.
//  Copyright © 2015年 shengdong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDJCard_Items.h"

@interface SDJCardOneCollectionCell : UICollectionViewCell

@property (nonatomic, strong)SDJCard_Items *model;

@end
