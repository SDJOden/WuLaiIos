//
//  SDJCardTwoCollectionCell.h
//  geniuslaiye2.0
//
//  Created by 盛东 on 15/12/1.
//  Copyright © 2015年 shengdong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SDJCard_Items.h"

@interface SDJCardTwoCollectionCell : UICollectionViewCell

@property (nonatomic, strong)SDJCard_Items *model;

@end
