//
//  SDJCardTypeFiveCell.m
//  geniuslaiye2.0
//
//  Created by 盛东 on 15/11/30.
//  Copyright © 2015年 shengdong. All rights reserved.
//

#import "SDJCardTypeFiveCell.h"

@interface SDJCardTypeFiveCell ()

@property (nonatomic, strong)UIImageView *iconView;

@property (nonatomic, strong)UILabel *typeLabel;

@property (nonatomic, strong)UILabel *timeLabel;

@property (nonatomic, strong)UILabel *payStatusLabel;

@property (nonatomic, strong)UIView *separateLine;

@property (nonatomic, strong)UILabel *productLabel;

@property (nonatomic, strong)UILabel *priceLabel;

@property (nonatomic, strong)UIButton *chooseButton;

@property (nonatomic, strong)UIView *backView;

@end

@implementation SDJCardTypeFiveCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initialize];
    }
    return self;
}

// 布局
- (void)initialize {
    // 订单卡片, 需要一个小图, 4个label, 一个支付按钮, 按钮点击可以disable
}

// 懒加载
-(UIImageView *)iconView {
    if (!_iconView) {
        _iconView = [[UIImageView alloc] init];
        [self.messageContentView addSubview:_iconView];
    }
    return _iconView;
}

- (UIView *)backView {
    if (!_backView) {
        _backView = [[UIView alloc] init];
        _backView.backgroundColor = HEXCOLOR(0xF5F0EB);
        [self.messageContentView addSubview:_backView];
    }
    return _backView;
}

- (UIButton *)chooseButton {
    if (!_chooseButton) {
        _chooseButton = [[UIButton alloc] initWithFrame:CGRectMake(20, 20, 60, 40)];
        [_chooseButton setBackgroundImage:[UIImage getImageWithSize:CGSizeMake(60, 40) backgroundColor:[UIColor colorWithRed:254/255.0 green:227/255.0 blue:12/255.0 alpha:1.0]] forState:UIControlStateNormal];
        [_chooseButton setBackgroundImage:[UIImage getImageWithSize:CGSizeMake(60, 40) backgroundColor:[UIColor grayColor]] forState:UIControlStateDisabled];
        [_chooseButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_chooseButton setTitle:@"立刻支付" forState:UIControlStateNormal];
        [_chooseButton setTitle:@"已经支付" forState:UIControlStateDisabled];
        _chooseButton.layer.cornerRadius = 8; // 需要裁剪成圆角
        _chooseButton.layer.masksToBounds = YES;
        [_chooseButton addTarget:self action:@selector(superButtonClicked) forControlEvents:UIControlEventTouchUpInside];
        [_backView addSubview:_chooseButton];
    }
    return _chooseButton;
}

- (UIView *)separateLine {
    if (!_separateLine) {
        _separateLine = [[UIView alloc] init];
        _separateLine.backgroundColor = HEXCOLOR(0xE3DEDA);
        [self.messageContentView addSubview:_separateLine];
    }
    return _separateLine;
}

- (UILabel *)typeLabel {
    if (!_typeLabel) {
        _typeLabel = [[UILabel alloc] init];
        [_typeLabel setFont:[UIFont systemFontOfSize:14]];
        [_typeLabel setTextColor:HEXCOLOR(0x333333)];
        [self.messageContentView addSubview:_typeLabel];
    }
    return _typeLabel;
}

- (UILabel *)timeLabel {
    if (!_timeLabel) {
        _timeLabel = [[UILabel alloc] init];
        [_timeLabel setFont:[UIFont systemFontOfSize:11]];
        [_timeLabel setTextColor:HEXCOLOR(0x999999)];
        [self.messageContentView addSubview:_timeLabel];
    }
    return _timeLabel;
}

- (UILabel *)payStatusLabel {
    if (!_payStatusLabel) {
        _payStatusLabel = [[UILabel alloc] init];
        [_payStatusLabel setFont:[UIFont systemFontOfSize:14]];
        [_payStatusLabel setTextColor:HEXCOLOR(0xff6800)];
        [self.messageContentView addSubview:_payStatusLabel];
    }
    return _payStatusLabel;
}

- (UILabel *)productLabel {
    if (!_productLabel) {
        _productLabel = [[UILabel alloc] init];
        [_productLabel setFont:[UIFont systemFontOfSize:15]];
        [_productLabel setTextColor:HEXCOLOR(0x333333)];
        [self.messageContentView addSubview:_productLabel];
    }
    return _productLabel;
}

- (UILabel *)priceLabel {
    if (!_priceLabel) {
        _priceLabel = [[UILabel alloc] init];
        [_priceLabel setFont:[UIFont systemFontOfSize:15]];
        [_priceLabel setTextColor:HEXCOLOR(0x333333)];
        [self.messageContentView addSubview:_priceLabel];
    }
    return _priceLabel;
}

- (void)superButtonClicked {
    
    _chooseButton.enabled = NO;
}

- (void)setDataModel:(RCMessageModel *)model {
    
    [super setDataModel:model];
    
    self.chooseButton.enabled = YES; // 需要从接口拉
    
//    RCRichContentMessage *message = (RCRichContentMessage *)model.content; // 从这个message的extra中解析数据
    
    [self.statusContentView removeFromSuperview];
    [self.portraitImageView removeFromSuperview];
    
    self.iconView.image = [UIImage imageNamed:@"card_009"];
    self.typeLabel.text = @"咖啡";
    self.timeLabel.text = @"15-11-27 21:18";
    self.payStatusLabel.text = @"等待支付";
    self.productLabel.text = @"热蔓越莓白巧克力风味摩卡";
    self.priceLabel.text = [NSString stringWithFormat:@"￥%d", 38];
    
    CGRect messageContentViewRect = self.messageContentView.frame;
    
    messageContentViewRect.size.width = CARD_WIDTH;
    messageContentViewRect.size.height = CARDFIVE_HEIGHT;
    messageContentViewRect.origin.x = 15;
    self.messageContentView.frame = messageContentViewRect;
    self.messageContentView.backgroundColor = [UIColor whiteColor];
    self.messageContentView.layer.cornerRadius = 8; // 需要裁剪成圆角
    self.messageContentView.layer.masksToBounds = YES;
    
    [self layout];
}

- (void)layout {
    
    [self.iconView makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.messageContentView).offset(10);
        make.left.equalTo(self.messageContentView).offset(10);
        make.width.height.equalTo(28);
    }];
    
    [self.typeLabel makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_iconView.right).offset(5);
        make.top.equalTo(_iconView);
    }];
    
    [self.timeLabel makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_typeLabel);
        make.bottom.equalTo(_iconView);
    }];
    
    [self.separateLine makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_iconView.bottom).offset(5);
        make.height.equalTo(1);
        make.left.equalTo(self.messageContentView).offset(10);
        make.right.equalTo(self.messageContentView).offset(-10);
    }];
    
    [self.payStatusLabel makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.messageContentView).offset(-10);
        make.top.equalTo(self.messageContentView).offset(10);
    }];
    
    [self.productLabel makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_iconView);
        make.top.equalTo(_separateLine.bottom).offset(15);
    }];
    
    [self.priceLabel makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_productLabel.right).offset(10);
        make.top.equalTo(_productLabel);
    }];
    
    [self.backView makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_productLabel.bottom).offset(15);
        make.left.right.bottom.equalTo(self.messageContentView);
    }];
    
    [self.chooseButton makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.equalTo(_backView).offset(10);
        make.right.equalTo(_backView).offset(-10);
        make.height.equalTo(44);
    }];
}

@end
