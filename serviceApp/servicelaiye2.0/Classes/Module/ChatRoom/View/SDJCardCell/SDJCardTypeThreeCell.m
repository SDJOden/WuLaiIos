//
//  SDJCardTypeThreeCell.m
//  geniuslaiye2.0
//
//  Created by 盛东 on 15/11/30.
//  Copyright © 2015年 shengdong. All rights reserved.
//

#import "SDJCardTypeThreeCell.h"
#import "SDJCardThreeTableCell.h"

@interface SDJCardTypeThreeCell ()<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong)UITableView *tableView;

@property (nonatomic, strong)NSArray *array;

@end

@implementation SDJCardTypeThreeCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initialize];
    }
    return self;
}

// 布局
- (void)initialize {}// 商品卡片（小图）

// 懒加载
- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] init];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        [self.messageContentView addSubview:_tableView];
    }
    return _tableView;
}

- (NSArray *)array {
    if (!_array) {
        _array = [[NSArray alloc] init];
    }
    return _array;
}

- (void)setDataModel:(RCMessageModel *)model {
    
    [super setDataModel:model];
    
    [self.statusContentView removeFromSuperview];
    [self.portraitImageView removeFromSuperview];
    
    // RCMessage基类来传递消息, 由后端来定数据解析方式和消息分辨方法
    
    CGRect messageContentViewRect = self.messageContentView.frame;
    messageContentViewRect.size.width = CARD_WIDTH;
    messageContentViewRect.size.height = CARDTHREE_HEIGHT * 3; // _array.count
    messageContentViewRect.origin.x = 15;
    self.messageContentView.frame = messageContentViewRect;
    
    self.messageContentView.backgroundColor = [UIColor whiteColor];
    self.messageContentView.layer.cornerRadius = 8; // 需要裁剪成圆角
    self.messageContentView.layer.masksToBounds = YES;
    
    // 设置数据
    self.tableView.frame = CGRectMake(0, 0, CARD_WIDTH, CARDTHREE_HEIGHT * 3); //_array.count
}

#pragma mark - datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    SDJCardThreeTableCell *cell = [SDJCardThreeTableCell returnCellWithTableView:tableView andWithCellStyle:@"SDJCardThreeTableCell"];
    
    cell.model = [RCMessageModel new];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 65;
}

@end
