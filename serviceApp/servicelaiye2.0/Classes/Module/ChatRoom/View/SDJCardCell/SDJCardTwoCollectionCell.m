//
//  SDJCardTwoCollectionCell.m
//  geniuslaiye2.0
//
//  Created by 盛东 on 15/12/1.
//  Copyright © 2015年 shengdong. All rights reserved.
//

#import "SDJCardTwoCollectionCell.h"

#import "SDJCommonSetting.h"

#define MAS_SHORTHAND_GLOBALS
#define MAS_SHORTHAND
#import "Masonry.h"

#import "UIImageView+WebCache.h"
#import "UIImage+SDJExtension.h"

@interface SDJCardTwoCollectionCell ()

@property (nonatomic, strong)UILabel *productLabel;

@property (nonatomic, strong)UILabel *detailLabel;

@property (nonatomic, strong)UILabel *priceLabel;

@property (nonatomic, strong)UIImageView *backgroundImageView;

@property (nonatomic, strong)UIImageView *iconView;

@property (nonatomic, strong)UILabel *serviceLabel;

@property (nonatomic, strong)UILabel *typeLabel;

@property (nonatomic, strong)UIButton *chooseButton;

@end

@implementation SDJCardTwoCollectionCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize {
    // 清空操作
    NSArray *subviews = [[NSArray alloc] initWithArray:self.contentView.subviews];
    for (UIView *subview in subviews) {
        [subview removeFromSuperview];
    }
}

// 懒加载
- (void)superButtonClicked {
    
    _chooseButton.enabled = NO;
}

- (UIImageView *)iconView {
    
    if (!_iconView) {
        _iconView = [[UIImageView alloc] init];
        [self.contentView addSubview:_iconView];
    }
    return _iconView;
}

- (UILabel *)typeLabel {
    
    if (!_typeLabel) {
        _typeLabel = [[UILabel alloc] init];
        [_typeLabel setTextColor:HEXCOLOR(0x333333)];
        [_typeLabel setFont:[UIFont systemFontOfSize:15]];
        [self.contentView addSubview:_typeLabel];
    }
    return _typeLabel;
}

- (UILabel *)serviceLabel {
    
    if (!_serviceLabel) {
        _serviceLabel = [[UILabel alloc] init];
        [_serviceLabel setTextColor:HEXCOLOR(0x999999)];
        [_serviceLabel setFont:[UIFont systemFontOfSize:11]];
        [self.contentView addSubview:_serviceLabel];
    }
    return _serviceLabel;
}

- (UIButton *)chooseButton {
    
    if (!_chooseButton) {
        _chooseButton = [[UIButton alloc] init];
        [_chooseButton setBackgroundImage:[UIImage getImageWithSize:CGSizeMake(60, 40) backgroundColor:[UIColor colorWithRed:254/255.0 green:227/255.0 blue:12/255.0 alpha:1.0]] forState:UIControlStateNormal];
        [_chooseButton setBackgroundImage:[UIImage getImageWithSize:CGSizeMake(60, 40) backgroundColor:[UIColor grayColor]] forState:UIControlStateDisabled];
        [_chooseButton.titleLabel setFont:[UIFont systemFontOfSize:16]];
        [_chooseButton setTitleColor:HEXCOLOR(0x333333) forState:UIControlStateNormal];
        [_chooseButton setTitle:@"来一份" forState:UIControlStateNormal];
        [_chooseButton setTitle:@"已选择" forState:UIControlStateDisabled];
        [_chooseButton addTarget:self action:@selector(superButtonClicked) forControlEvents:UIControlEventTouchUpInside];
        _chooseButton.layer.cornerRadius = 5; // 需要裁剪成圆角
        _chooseButton.layer.masksToBounds = YES;
        [self.contentView addSubview:_chooseButton];
    }
    return _chooseButton;
}

- (UIImageView *)backgroundImageView {
    if (!_backgroundImageView) {
        _backgroundImageView = [[UIImageView alloc] init];
        _backgroundImageView.layer.cornerRadius = 5; // 需要裁剪成圆角
        _backgroundImageView.layer.masksToBounds = YES;
        [self.contentView addSubview:_backgroundImageView];
    }
    return _backgroundImageView;
}

- (UILabel *)productLabel {
    if (!_productLabel) {
        _productLabel = [[UILabel alloc] init];
        [_productLabel setTextColor:HEXCOLOR(0xffffff)];
        [_productLabel setFont:[UIFont systemFontOfSize:16]];
        [_productLabel setTextAlignment:1];
        [self.contentView addSubview:_productLabel];
    }
    return _productLabel;
}

- (UILabel *)detailLabel {
    if (!_detailLabel) {
        _detailLabel = [[UILabel alloc] init];
        [_detailLabel setTextColor:HEXCOLOR(0xffffff)];
        [_detailLabel setFont:[UIFont systemFontOfSize:12]];
        [_detailLabel setTextAlignment:1];
        [self.contentView addSubview:_detailLabel];
    }
    return _detailLabel;
}

- (UILabel *)priceLabel {
    if (!_priceLabel) {
        _priceLabel = [[UILabel alloc] init];
        [_priceLabel setTextColor:HEXCOLOR(0xffe600)];
        [_priceLabel setFont:[UIFont systemFontOfSize:14]];
        [_priceLabel setTextAlignment:1];
        [self.contentView addSubview:_priceLabel];
    }
    return _priceLabel;
}

- (void)setModel:(SDJCard_Items *)model {
    
    _model = model;
    
    // 清空cell设置
    self.chooseButton.enabled = YES; // 记不住状态, 按钮的点击问题.
    
    // 设置控件信息, 设置布局
    __typeof__(self) __weak weakSelf = self;
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
    [self.backgroundImageView sd_setImageWithURL:[NSURL URLWithString:model.ImageUrl] placeholderImage:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        dispatch_async(dispatch_get_main_queue(), ^{
            weakSelf.backgroundImageView.image = [image imgWithBlur]; // 这种模糊不行, 太大?
        });
    }];

    [self.iconView sd_setImageWithURL:[NSURL URLWithString:model.ProviderLogoUrl] placeholderImage:[UIImage imageNamed:@"card_009"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {}];
    
    self.typeLabel.text = model.ProviderName;
    self.serviceLabel.text = @"暂无";
    
    // 自动布局
    [self.iconView makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.equalTo(30);
        make.left.equalTo(self.contentView).offset(10);
        make.top.equalTo(self.contentView).offset(15);
    }];
    
    [self.typeLabel makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(15);
        make.left.equalTo(_iconView.right).offset(5);
    }];
    
    [self.serviceLabel makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_iconView.right).offset(5);
        make.top.equalTo(_typeLabel.bottom).offset(3);
    }];
    
    [self.chooseButton makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.contentView).offset(-15);
        make.left.equalTo(self.contentView).offset(10);
        make.right.equalTo(self.contentView).offset(-10);
        make.height.equalTo(44);
    }];
    
    [self.backgroundImageView makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(10);
        make.right.equalTo(self.contentView).offset(-10);
        make.top.equalTo(_iconView.bottom).offset(10);
        make.height.equalTo(([UIScreen mainScreen].bounds.size.width - 30 - 20) * 0.5);
    }];
    
    // 设置控件信息, 设置布局
    self.productLabel.text = model.Name;
    self.detailLabel.text = [model.Desc isEqualToString:@""] ? @"暂无详细信息" : model.Desc;
    self.priceLabel.text = [NSString stringWithFormat:@"￥%d",[model.Amount intValue] / 100];
    
    [self.detailLabel makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.backgroundImageView).offset(10);
        make.bottom.equalTo(self.backgroundImageView).offset(-10);
    }];
    
    [self.productLabel makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.backgroundImageView).offset(10);
        make.bottom.equalTo(_detailLabel.top).offset(-5);
    }];
    
    [self.priceLabel makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.backgroundImageView).offset(-15);
        make.bottom.equalTo(self.backgroundImageView).offset(-15);
    }];
}

@end
