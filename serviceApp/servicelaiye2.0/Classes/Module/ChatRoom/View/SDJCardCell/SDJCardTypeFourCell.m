//
//  SDJCardTypeFourCell.m
//  geniuslaiye2.0
//
//  Created by 盛东 on 15/11/30.
//  Copyright © 2015年 shengdong. All rights reserved.
//

#import "SDJCardTypeFourCell.h"

#import "SDJCardPersonButton.h"

@interface SDJCardTypeFourCell ()

@property (nonatomic, strong)UILabel *nameLabel;

@property (nonatomic, strong)UILabel *telphoneLabel;

@property (nonatomic, strong)UILabel *detailLabel;

@property (nonatomic, strong)UIButton *telphoneButton;

@property (nonatomic, strong)UIButton *personButton;

@property (nonatomic, strong)UIButton *scoreButton;

@property (nonatomic, strong)UIImageView *starTop;

@property (nonatomic, strong)UIImageView *starBack;

@property (nonatomic, strong)UIView *starBackView;

@end

@implementation SDJCardTypeFourCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initialize];
    }
    return self;
}

// 懒加载
- (UILabel *)nameLabel {
    if (!_nameLabel) {
        _nameLabel = [[UILabel alloc] init];
        [_nameLabel setFont:[UIFont systemFontOfSize:16]];
        [_nameLabel setTextColor:HEXCOLOR(0x333333)];
        [self.messageContentView addSubview:_nameLabel];
    }
    return _nameLabel;
}

- (UILabel *)telphoneLabel {
    if (!_telphoneLabel) {
        _telphoneLabel = [[UILabel alloc] init];
        [_telphoneLabel setFont:[UIFont systemFontOfSize:12]];
        [_telphoneLabel setTextColor:[UIColor grayColor]];
        [self.messageContentView addSubview:_telphoneLabel];
    }
    return _telphoneLabel;
}

- (UILabel *)detailLabel {
    if (!_detailLabel) {
        _detailLabel = [[UILabel alloc] init];
        [_detailLabel setFont:[UIFont systemFontOfSize:12]];
        [_detailLabel setTextColor:[UIColor grayColor]];
        [self.messageContentView addSubview:_detailLabel];
    }
    return _detailLabel;
}

- (UIButton *)scoreButton {
    if (!_scoreButton) {
        _scoreButton = [[UIButton alloc] init];
        [_scoreButton setBackgroundColor:[UIColor redColor]];
        [_scoreButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_scoreButton.titleLabel setFont:[UIFont systemFontOfSize:10]];
        _scoreButton.layer.cornerRadius = 2; // 需要裁剪成圆角
        _scoreButton.layer.masksToBounds = YES;
        [self.messageContentView addSubview:_scoreButton];
    }
    return _scoreButton;
}

- (UIButton *)telphoneButton {
    if (!_telphoneButton) {
        _telphoneButton = [[UIButton alloc] init];
        [_telphoneButton addTarget:self action:@selector(telphoneButtonClicked) forControlEvents:UIControlEventTouchUpInside];
        [self.messageContentView addSubview:_telphoneButton];
    }
    return _telphoneButton;
}

- (UIButton *)personButton {
    if (!_personButton) {
        _personButton = [[SDJCardPersonButton alloc] init];
        [self.messageContentView addSubview:_personButton];
    }
    return _personButton;
}

- (UIView *)starBackView {
    if (!_starBackView) {
        _starBackView = [[UIView alloc] init];
        _starBackView.layer.cornerRadius = 0;
        _starBackView.layer.masksToBounds = YES;
        _starBackView.backgroundColor = [UIColor whiteColor];
        [self.messageContentView addSubview:_starBackView];
    }
    return _starBackView;
}

- (UIImageView *)starBack {
    if (!_starBack) {
        _starBack = [[UIImageView alloc] init];
        _starBack.image = [UIImage imageNamed:@"card_005"];
        [_starBackView addSubview:_starBack];
    }
    return _starBack;
}

- (UIImageView *)starTop {
    if (!_starTop) {
        _starTop = [[UIImageView alloc] init];
        _starTop.image = [UIImage imageNamed:@"card_006"];
        [_starBackView addSubview:_starTop];
    }
    return _starTop;
}

// 布局
- (void)initialize {} // 人物卡片

- (void)telphoneButtonClicked {
    
    NSString *number = [@"tel://" stringByAppendingString:@"13552669761"];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:number]];
}

- (void)setDataModel:(RCMessageModel *)model {
    
    [super setDataModel:model];
    
    [self.statusContentView removeFromSuperview];
    [self.portraitImageView removeFromSuperview];
    
    self.nameLabel.text = @"吴师傅";
    self.telphoneLabel.text = @"13552669761";
    self.detailLabel.text = @"快车 京Axxxx234";
    [self.scoreButton setTitle:@"4.9" forState:UIControlStateNormal];
    [self.telphoneButton setImage:[UIImage imageNamed:@"card_008"] forState:UIControlStateNormal];
    [self.personButton setBackgroundImage:[UIImage imageNamed:@"card_010"] forState:UIControlStateNormal];
    [self.personButton setImage:[[UIImage imageNamed:@"card_009"] cutCircleImagewithWidth:10 withColor:[UIColor whiteColor]] forState:UIControlStateNormal];
    
    CGRect messageContentViewRect = self.messageContentView.frame;
    messageContentViewRect.size.width = CARD_WIDTH;
    messageContentViewRect.size.height = CARDFOUR_HEIGHT; // _array.count
    messageContentViewRect.origin.x = 15;
    self.messageContentView.frame = messageContentViewRect;
    
    self.messageContentView.backgroundColor = [UIColor whiteColor];
    self.messageContentView.layer.cornerRadius = 8; // 需要裁剪成圆角
    self.messageContentView.layer.masksToBounds = YES;
    
    [self layout];
}

- (void)layout {
    
    [self.nameLabel makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.messageContentView).offset(15);
        make.left.equalTo(self.messageContentView).offset(10);
    }];
    
    [self.telphoneLabel makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_nameLabel.bottom).offset(6);
        make.left.equalTo(_nameLabel.left);
    }];
    
    [self.detailLabel makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_telphoneLabel.bottom).offset(6);
        make.left.equalTo(_nameLabel.left);
    }];
    
    [self.telphoneButton makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.equalTo(23);
        make.bottom.equalTo(self.messageContentView).offset(-15);
        make.right.equalTo(self.messageContentView).offset(-93);
    }];
    
    [self.starBackView makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(_nameLabel);
        make.left.equalTo(_nameLabel.right).offset(10);
        make.width.equalTo(78);
        make.height.equalTo(12);
    }];
    
    [self.starBack makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.left.top.equalTo(_starBackView);
    }];
    
    [self.starTop makeConstraints:^(MASConstraintMaker *make) {
        make.width.height.top.equalTo(_starBackView);
        make.left.equalTo(_starBackView).offset(-16); // 以16为倍数进行收缩移动
    }];
    
    [self.scoreButton makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(_starBackView);
        make.left.equalTo(_starBackView.right).offset(10);
        make.width.equalTo(24);
        make.height.equalTo(15);
    }];
    
    [self.personButton makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.right.equalTo(self.messageContentView);
        make.width.equalTo(83);
    }];
}

@end
