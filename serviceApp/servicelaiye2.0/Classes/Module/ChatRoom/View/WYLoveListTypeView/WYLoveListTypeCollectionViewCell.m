//
//  WYLoveListTypeCollectionViewCell.m
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/7/12.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import "WYLoveListTypeCollectionViewCell.h"
#import "WYLoveTypeModel.h"

@interface WYLoveListTypeCollectionViewCell ()

@property (nonatomic, strong) UILabel *typeLabel;

@end

@implementation WYLoveListTypeCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize {
    
    // 清空操作
    NSArray *subviews = [[NSArray alloc] initWithArray:self.contentView.subviews];
    for (__strong UIView *subview in subviews) {
        [subview removeFromSuperview];
        subview = nil;
    }
}

-(void)cellWithTitle:(WYLoveTypeModel *)model{
    
    self.typeLabel.text = model.type;
    if (model.selected == YES) {
        self.typeLabel.textColor = HEXCOLOR(0x81BCFF);
    }
    else {
        self.typeLabel.textColor = HEXCOLOR(0xffffff);
    }
    [self layout];
}

-(void)layout{
    
    [self.typeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.bottom.equalTo(self);
    }];
}

#pragma mark --- 懒加载
- (UILabel *)typeLabel {
    
	if(_typeLabel == nil) {
		_typeLabel = [[UILabel alloc]init];
        _typeLabel.backgroundColor = [UIColor clearColor];
        _typeLabel.textColor = [UIColor whiteColor];
        _typeLabel.font = [UIFont systemFontOfSize:15];
        _typeLabel.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:_typeLabel];
	}
	return _typeLabel;
}

@end
