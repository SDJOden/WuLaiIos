//
//  WYLoveListTypeView.m
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/7/12.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import "WYLoveListTypeView.h"
#import "WYLoveListTypeCollectionViewCell.h"
#import "WYLoveTypeModel.h"

@interface WYLoveListTypeView () <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@property (nonatomic, strong) UICollectionView *serviceCollectionView;

@property (nonatomic, strong) UICollectionViewFlowLayout *customFlowLayout;

@property (nonatomic, strong) NSArray *categoryArray;

@property (nonatomic, copy) NSString *categoryStr;

@property (nonatomic, strong) UIButton *closeButton;

@end

@implementation WYLoveListTypeView

-(instancetype)init{
    
    if (self = [super init]) {
    }
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame{
    
    if (self = [super initWithFrame:frame]) {
    }
    return self;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder{
    
    if (self = [super initWithCoder:aDecoder]) {
    }
    return self;
}

+(instancetype)modelWithServiceTypeArr:(NSArray *)serviceArr{
    
    return [[self alloc]initWithServiceTypeArr:serviceArr];
}

-(instancetype)initWithServiceTypeArr:(NSArray *)serviceArr{
    
    if (self = [super init]) {
        [self rcinit];
        [self.serviceCollectionView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(50);
            make.left.equalTo(10);
            make.right.equalTo(-10);
            make.bottom.equalTo(-80);
        }];
        [self.closeButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.serviceCollectionView.bottom).offset(10);
            make.centerX.equalTo(self);
            make.width.height.equalTo(30);
        }];
    }
    return self;
}

-(void)rcinit{
    
    self.backgroundColor = [UIColor colorWithRed:0.3333 green:0.3333 blue:0.3333 alpha:0.84];
    self.categoryStr = @"-1";
}

#pragma mark --- 懒加载
- (NSArray *)categoryArray {
    
    if(_categoryArray == nil) {
        _categoryArray = [WYLoveTypeModel createLoveTypeModelWithArrayWithCategory:self.categoryStr];
    }
    return _categoryArray;
}

- (UICollectionView *)serviceCollectionView {
    
    if(_serviceCollectionView == nil) {
        _customFlowLayout = [[UICollectionViewFlowLayout alloc] init];
        _customFlowLayout.minimumLineSpacing = 10.0f;
        _customFlowLayout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
        _customFlowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
        _serviceCollectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:_customFlowLayout];
        _serviceCollectionView.delegate = self;
        _serviceCollectionView.dataSource = self;
        _serviceCollectionView.backgroundColor = [UIColor clearColor];
        [_serviceCollectionView registerClass:[WYLoveListTypeCollectionViewCell class] forCellWithReuseIdentifier:@"loveListCell"];
        [self addSubview:_serviceCollectionView];
    }
    return _serviceCollectionView;
}

- (UIButton *)closeButton {
    
    if(_closeButton == nil) {
        _closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_closeButton setBackgroundImage:[UIImage imageNamed:@"LoveListClose"] forState:UIControlStateNormal];
        [_closeButton addTarget:self action:@selector(closeView) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_closeButton];
    }
    return _closeButton;
}

#pragma mark --- UICollectionViewDelgate
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return self.categoryArray.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    WYLoveListTypeCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"loveListCell" forIndexPath:indexPath];
    [cell cellWithTitle:self.categoryArray[indexPath.row]];
    return cell;
}

#pragma mark --- UICollectionViewDelegateFlowLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    return CGSizeMake(([UIScreen mainScreen].bounds.size.width - 2 * 10 - 4 * 10) / 5, 44);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    //修改当前选中的模型和cell样式
    WYLoveTypeModel *statuModel = self.categoryArray[indexPath.row];
    //判断是不是第一个cell，如果是遍历后面cell和模型进行修改
    if (indexPath.row == 0) {
        for (int i = 1; i < self.categoryArray.count; i ++) {
            WYLoveTypeModel *serviceModel = self.categoryArray[i];
            if (serviceModel.selected) {
                serviceModel.selected = NO;
                [((WYLoveListTypeCollectionViewCell *)[collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]]) cellWithTitle:serviceModel];
            }
        }
    }
    else {//如果不是第一个cell，修改第一个cell样式
        if (((WYLoveTypeModel *)self.categoryArray[0]).selected) {
            ((WYLoveTypeModel *)self.categoryArray[0]).selected = NO;
            [((WYLoveListTypeCollectionViewCell *)[collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]]) cellWithTitle:self.categoryArray[0]];
        }
    }
    statuModel.selected = !statuModel.selected;
    [((WYLoveListTypeCollectionViewCell *)[collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:indexPath.row  inSection:0]]) cellWithTitle:statuModel];
    self.categoryStr = [self getServiceStatu];
    if ([self.wyLoveListTypeViewDelegate respondsToSelector:@selector(changeCategoryStr:)]) {
        [self.wyLoveListTypeViewDelegate changeCategoryStr:self.categoryStr];
    }
}

-(NSString *)getServiceStatu{
    
    NSString *str = @"";
    for (int i = 0; i < self.categoryArray.count; i ++) {
        WYLoveTypeModel *serviceModel = self.categoryArray[i];
        if (serviceModel.selected) {
            if (i == 0) {
                return @"不限";
            }
            else {
                str = [NSString stringWithFormat:@"%@，%@", str, serviceModel.type];
            }
        }
    }
    if (str.length > 0) {
        str = [str substringFromIndex:1];
    }
    else {
        str = @"不限";
    }
    return str;
}

-(void)closeView{
    
    if ([self.wyLoveListTypeViewDelegate respondsToSelector:@selector(closeLoveListTypeView)]) {
        [self.wyLoveListTypeViewDelegate closeLoveListTypeView];
    }
}

@end
