//
//  WYLoveListTypeView.h
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/7/12.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol WYLoveListTypeViewDelegate <NSObject>

-(void)closeLoveListTypeView;

-(void)changeCategoryStr:(NSString *)categoryStr;

@end

@interface WYLoveListTypeView : UIView

@property (nonatomic, weak) id <WYLoveListTypeViewDelegate> wyLoveListTypeViewDelegate;

+(instancetype)modelWithServiceTypeArr:(NSArray *)serviceArr;

-(instancetype)initWithServiceTypeArr:(NSArray *)serviceArr;

@end
