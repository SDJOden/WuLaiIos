//
//  WYLoveListTypeCollectionViewCell.h
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/7/12.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import <UIKit/UIKit.h>

@class WYLoveTypeModel;

@interface WYLoveListTypeCollectionViewCell : UICollectionViewCell

-(void)cellWithTitle:(WYLoveTypeModel *)model;

@end
