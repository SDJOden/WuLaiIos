//
//  WYLoveListCell.m
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/7/5.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import "WYLoveListCell.h"
#import "RCMessageModel.h"
#import "WPHotspotLabel.h"
#import "RCMessageModel.h"

#define TIME_LABEL_HEIGHT 20
#define Text_Message_Font_Size 16

@interface WYLoveListCell ()

@property (nonatomic, strong) UILabel *timeLabel;

@property (nonatomic, strong) UILabel *geniusNameLabel;

@property (nonatomic, strong) UIImageView *bubbleBackgroundImageView;

@property (nonatomic, strong) UILabel *contentTextLabel;

@property (nonatomic, strong) UIImageView *selectImageView;

@property (nonatomic, strong) NSDictionary *extraDic;

@end

@implementation WYLoveListCell

-(instancetype)init{
    
    if (self = [super init]) {
    }
    return self;
}

-(void)setModel:(RCMessageModel *)model{
    
    _model = model;
    
    self.contentView.backgroundColor = HEXCOLOR(0xf7f7f7);
    
    self.timeLabel.text = [RCKitUtility ConvertChatMessageTime:model.sentTime / 1000];

    if ([model.content isKindOfClass:[RCTextMessage class]]) {
        if ([((RCTextMessage*)model.content).extra isKindOfClass:[NSDictionary class]]) {
            _extraDic = (NSDictionary *)((RCTextMessage*)model.content).extra;
        }
        else {
            _extraDic = [WYTool jsonStringToDictionary:((RCTextMessage*)model.content).extra];
            ((RCTextMessage*)model.content).extra = _extraDic;
        }
    }
    else if ([model.content isKindOfClass:[RCImageMessage class]]) {
        if ([((RCImageMessage*)model.content).extra isKindOfClass:[NSDictionary class]]) {
            _extraDic = (NSDictionary *)((RCImageMessage*)model.content).extra;
        }
        else {
            _extraDic = [WYTool jsonStringToDictionary:((RCImageMessage*)model.content).extra];
            ((RCImageMessage*)model.content).extra = _extraDic;
        }
    }
    else if ([model.content isKindOfClass:[RCRichContentMessage class]]) {
        if ([((RCRichContentMessage*)model.content).extra isKindOfClass:[NSDictionary class]]) {
            _extraDic = (NSDictionary *)((RCRichContentMessage*)model.content).extra;
        }
        else {
            _extraDic = [WYTool jsonStringToDictionary:((RCRichContentMessage *)model.content).extra];
            ((RCRichContentMessage *)model.content).extra = _extraDic;
        }
    }else if ([model.content isKindOfClass:[RCVoiceMessage class]]) {
        if ([((RCVoiceMessage*)model.content).extra isKindOfClass:[NSDictionary class]]) {
            _extraDic = (NSDictionary *)((RCVoiceMessage*)model.content).extra;
        }
        else {
            _extraDic = [WYTool jsonStringToDictionary:((RCVoiceMessage *)model.content).extra];
            ((RCVoiceMessage *)model.content).extra = _extraDic;
        }
    }
    
    if (model.messageDirection == MessageDirection_SEND) {
        if (((NSString *)_extraDic[@"genius_info"][@"username"]).length >= 9) {
            self.geniusNameLabel.text = [NSString stringWithFormat:@"%@ %@", [_extraDic[@"genius_info"][@"username"]substringToIndex:9], _extraDic[@"genius_info"][@"realname"]];
        }else {
            self.geniusNameLabel.text = [NSString stringWithFormat:@"%@ %@", _extraDic[@"genius_info"][@"username"], _extraDic[@"genius_info"][@"realname"]];
            if (((NSNumber *)_extraDic[@"msg_type"]).integerValue == 3) {
                self.geniusNameLabel.hidden = YES;
            }
        }
    }else {
        if (((NSNumber *)_extraDic[@"user_src"]).integerValue == 1) {
            self.geniusNameLabel.text = @"微信";
        }
        else  {
            self.geniusNameLabel.text = @"App";
        }
    }
    [self addSubViews];
}

-(void)addSubViews{
    
    CGSize timeTextSize_ = CGSizeZero;
    
    if (IOS_FSystenVersion < 7.0) {
        timeTextSize_ = RC_MULTILINE_TEXTSIZE_LIOS7(self.timeLabel.text, [UIFont systemFontOfSize:12], CGSizeMake(self.bounds.size.width, TIME_LABEL_HEIGHT), NSLineBreakByTruncatingTail);
    }else {
        timeTextSize_ = RC_MULTILINE_TEXTSIZE_GEIOS7(self.timeLabel.text, [UIFont systemFontOfSize:12], CGSizeMake(self.bounds.size.width, TIME_LABEL_HEIGHT));
    }
    timeTextSize_ = CGSizeMake(ceilf(timeTextSize_.width), ceilf(timeTextSize_.height));
    
    self.timeLabel.frame = CGRectMake(12, 12, timeTextSize_.width, 20);
    [self.geniusNameLabel sizeToFit];
    self.geniusNameLabel.frame = CGRectMake(12+timeTextSize_.width+10, 12, self.geniusNameLabel.w, 20);
    
    RCMessageContent *messageContent = _model.content;
    NSMutableString *realTextString = [NSMutableString stringWithFormat:@""];
    if ([messageContent isKindOfClass:[RCTextMessage class]]) {
        NSString *text = ((RCTextMessage*)messageContent).content;
        NSString *pattern = @"<a href=\'(.*?)\'>(.*?)</a>";
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern options:0 error:nil];
        NSArray *matches = [regex matchesInString:text options:0 range:NSMakeRange(0, text.length)]; // 网址
        NSUInteger lastIdx = 0;
        NSMutableString *attributedString = [NSMutableString stringWithFormat:@""]; // 总字符串
        
        if (matches.count) { // 解析部分bug, 1.文字需要提出来, 2. 位置需要调整 matchs.count
            NSMutableArray *urlArrs = [[NSMutableArray alloc] init]; // 存url
            int i = 0;
            for (NSTextCheckingResult* match in matches)
            {
                NSRange range = match.range;
                if (range.location > lastIdx)
                {
                    NSString *temp = [text substringWithRange:NSMakeRange(lastIdx, range.location - lastIdx)];
                    [attributedString appendString:temp];// 记录头
                    [realTextString appendString:temp];
                }
                NSString *title = [text substringWithRange:[match rangeAtIndex:2]]; // 标题
                NSString *url = [text substringWithRange:[match rangeAtIndex:1]]; // 网址
                lastIdx = range.location + range.length;
                [attributedString appendFormat:@"<%d>%@</%d>",i,title,i];
                [realTextString appendString:title];
                [urlArrs addObject:url];
                i++;
            }
            if (lastIdx < text.length)
            {
                NSString *temp = [text substringFromIndex:lastIdx];
                [attributedString appendString:temp];// 记录尾
                [realTextString appendString:temp];
            }
        } else {
            [realTextString appendString:((RCTextMessage *)messageContent).content];
        }
    }
    
    if ([messageContent isMemberOfClass:[RCImageMessage class]] || [messageContent isMemberOfClass:[RCLocationMessage class]] || [messageContent isMemberOfClass:[RCVoiceMessage class]] || [messageContent isMemberOfClass:[RCRichContentMessage class]]) {
        realTextString = [NSMutableString stringWithString:_model.objectName];
    }
    
    CGSize __textSize = CGSizeZero;
    if (IOS_FSystenVersion < 7.0) {
        __textSize = RC_MULTILINE_TEXTSIZE_LIOS7(realTextString, [UIFont systemFontOfSize:Text_Message_Font_Size], CGSizeMake(self.contentView.bounds.size.width - 12 - 121 - 40, 8000), NSLineBreakByTruncatingTail);
    }else {
        __textSize = RC_MULTILINE_TEXTSIZE_GEIOS7(realTextString, [UIFont systemFontOfSize:Text_Message_Font_Size], CGSizeMake(self.contentView.bounds.size.width - 12 - 121 - 40, 8000));
    }
    __textSize = CGSizeMake(ceilf(__textSize.width), ceilf(__textSize.height));
    
    CGSize __labelSize = CGSizeMake(__textSize.width, __textSize.height);
    CGFloat __bubbleWidth = __labelSize.width + 20 + 20 < 50 ? 50 : (__labelSize.width + 20 + 20);
    CGFloat __bubbleHeight = __labelSize.height + 12 + 12 < 46 ? 46 : (__labelSize.height + 12 + 12);
    CGSize __bubbleSize = CGSizeMake(__bubbleWidth, __bubbleHeight);
    
    self.bubbleBackgroundImageView.frame = CGRectMake(12, self.timeLabel.y+self.timeLabel.h+5, __bubbleSize.width, __bubbleSize.height);
    
    self.contentTextLabel.text = realTextString;
    self.contentTextLabel.frame = CGRectMake(self.bubbleBackgroundImageView.x+20, self.bubbleBackgroundImageView.y+12, __textSize.width, __textSize.height);
    
    
    self.selectImageView.frame = CGRectMake([UIScreen mainScreen].bounds.size.width - 25 - 12, self.bubbleBackgroundImageView.y, 25, 25);
    
    if (MessageDirection_RECEIVE == _model.messageDirection) {
        self.bubbleBackgroundImageView.image = [UIImage getRoundImageWithSize:__bubbleSize backgroundColor:HEXCOLOR(0xe4e4e4) lineWidth:0 cornerRadius:9.0 shouldFill:YES];
    } else {
        RCTextMessage *textMsg = (RCTextMessage *)self.model.content;
        if ([(NSDictionary *)textMsg.extra isKindOfClass:[NSString class]]) {
            // what's this?
            textMsg.extra = [WYTool jsonStringToDictionary:textMsg.extra];
        }
        
        if ([(NSDictionary *)textMsg.extra isKindOfClass:[NSDictionary class]]) {
            NSDictionary *dict = (NSDictionary *)textMsg.extra;
            if (((NSString *)dict[@"genius_info"][@"username"]).length >= 9) {
                self.bubbleBackgroundImageView.image = [UIImage getRoundImageWithSize:__bubbleSize backgroundColor:[UIColor colorWithRed:0.7451 green:0.8784 blue:0.9451 alpha:1.0] lineWidth:0 cornerRadius:9.0 shouldFill:YES];
            }else {
                self.bubbleBackgroundImageView.image = [UIImage getRoundImageWithSize:__bubbleSize backgroundColor:[UIColor colorWithRed:1.0 green:0.9137 blue:0.6196 alpha:1.0] lineWidth:0 cornerRadius:9.0 shouldFill:YES];
            }
        }
    }
}

#pragma mark --- 懒加载
- (UILabel *)timeLabel {
    
	if(_timeLabel == nil) {
		_timeLabel = [[UILabel alloc] init];
        _timeLabel.font = [UIFont systemFontOfSize:12];
        _timeLabel.textColor = HEXCOLOR(0x8B8B8B);
        _timeLabel.textAlignment = NSTextAlignmentLeft;
        [self.contentView addSubview:_timeLabel];
	}
	return _timeLabel;
}

- (UILabel *)geniusNameLabel {
    
	if(_geniusNameLabel == nil) {
		_geniusNameLabel = [[UILabel alloc] init];
        _geniusNameLabel.font = [UIFont systemFontOfSize:12];
        _geniusNameLabel.textColor = HEXCOLOR(0x8B8B8B);
        _geniusNameLabel.textAlignment = NSTextAlignmentLeft;
        [self.contentView addSubview:_geniusNameLabel];
	}
	return _geniusNameLabel;
}

- (UIImageView *)bubbleBackgroundImageView {
    
    if(_bubbleBackgroundImageView == nil) {
        _bubbleBackgroundImageView = [[UIImageView alloc] init];
        [self.contentView addSubview:_bubbleBackgroundImageView];
    }
    return _bubbleBackgroundImageView;
}

- (UILabel *)contentTextLabel {
    
	if(_contentTextLabel == nil) {
		_contentTextLabel = [[UILabel alloc] init];
        _contentTextLabel.font = [UIFont systemFontOfSize:16];
        _contentTextLabel.numberOfLines = 0;
        [self.contentView addSubview:_contentTextLabel];
	}
	return _contentTextLabel;
}

- (UIImageView *)selectImageView {
    
	if(_selectImageView == nil) {
		_selectImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"selector"]];
        [self.contentView addSubview:_selectImageView];
	}
	return _selectImageView;
}

-(void)setIsSelectImage:(BOOL)isSelectImage{
    
    _isSelectImage = isSelectImage;
    if (isSelectImage) {
        self.selectImageView.image = [UIImage imageNamed:@"selectedImage"];
    }else {
        self.selectImageView.image = [UIImage imageNamed:@"selector"];
    }
}

-(void)changeSelectImageView:(BOOL)isSelect{
    
    if (isSelect) {
        self.selectImageView.image = [UIImage imageNamed:@"selectedImage"];
    }else {
        self.selectImageView.image = [UIImage imageNamed:@"selector"];
    }
}

@end
