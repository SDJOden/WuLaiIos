//
//  WYLoveListCell.h
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/7/5.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RCMessageModel;

@interface WYLoveListCell : UITableViewCell

@property (nonatomic, strong) RCMessageModel *model;

@property (nonatomic, assign) BOOL isSelectImage;

-(void)changeSelectImageView:(BOOL)isSelect;

@end
