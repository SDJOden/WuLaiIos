//
//  RCContentView.h
//  RongIMKit
//
//  Created by xugang on 3/31/15.
//  Copyright (c) 2015 RongCloud. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RCContentView : UIView

@property(nonatomic, copy) void (^eventBlock)(CGRect frame);

- (void)registerFrameChangedEvent:(void (^)(CGRect frame))eventBlock;

@end
