//
//  VoiceCaptureView.h
//  iOS-IMKit
//
//  Created by xugang on 7/4/14.
//  Copyright (c) 2014 Heq.Shinoda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "RCVoiceRecorder.h"

@protocol RCVoiceCaptureControlDelegate <NSObject>

- (void)RCVoiceCaptureControlTimeout:(BOOL)timeout withDuration:(double)duration withWavData:(NSData *)data;

@end

@interface RCVoiceCaptureControl : UIView

@property(nonatomic, weak) id<RCVoiceCaptureControlDelegate> delegate;

@property(nonatomic, readonly, copy) NSData *stopRecord;

@property(nonatomic, readonly, assign) double duration;

/**
 *  设置倒计时时间
 *
 *  @param n 倒计时秒数
 */
- (void)setEscapeTime:(int)n;

- (void)startRecord;
- (void)cancelRecord;
- (void)showCancelView;
- (void)hideCancelView;
- (void)showMsgShortView;
@end
