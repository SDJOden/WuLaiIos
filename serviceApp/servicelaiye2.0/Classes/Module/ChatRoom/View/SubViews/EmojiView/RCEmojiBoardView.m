//
//  RCEmojiView.m
//  RCIM
//
//  Created by Heq.Shinoda on 14-5-29.
//  Copyright (c) 2014年 RongCloud. All rights reserved.
//

#import "RCEmojiBoardView.h"
#import "EmojiStringDefine.h"
#import "RCEmojiPageControl.h"

#define RC_EMOJI_WIDTH 38

@interface RCEmojiBoardView ()

@property(nonatomic, assign) int emojiTotal;
@property(nonatomic, assign) int emojiTotalPage;
@property(nonatomic, assign) int emojiColumn;
@property(nonatomic, assign) int emojiRow;
@property(nonatomic, assign) int emojiMaxCountPerPage;
@property(nonatomic, assign) CGFloat emojiMariginHorizontalMin;
@property(nonatomic, assign) CGFloat emojiSpanVertial;
@property(nonatomic, strong) NSArray *faceEmojiArray;

@end

@implementation RCEmojiBoardView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        
        NSString *resourcePath = [[NSBundle mainBundle] resourcePath];
        NSString *bundlePath = [resourcePath stringByAppendingPathComponent:@"Emoji.plist"];
        self.faceEmojiArray = [[NSArray alloc]initWithContentsOfFile:bundlePath];
        
        [self generateDefaultLayoutParameters];
        self.backgroundColor = HEXCOLOR(0xebebeb);

        self.emojiBackgroundView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        self.emojiBackgroundView.backgroundColor = HEXCOLOR(0xfafafa);
        self.emojiBackgroundView.pagingEnabled = YES;
        self.emojiBackgroundView.contentSize = CGSizeMake(self.emojiTotalPage * self.frame.size.width, self.frame.size.height);
        self.emojiBackgroundView.showsHorizontalScrollIndicator = NO;
        self.emojiBackgroundView.showsVerticalScrollIndicator = NO;
        self.emojiBackgroundView.delegate = self;
        [self addSubview:self.emojiBackgroundView];

        [self loadEmojiView];
    }
    return self;
}

- (void)generateDefaultLayoutParameters {
    self.emojiSpanVertial = 44;
    self.emojiRow = 5;
    self.emojiMariginHorizontalMin = 6;
    if (nil == self.faceEmojiArray ||  [self.faceEmojiArray count] == 0) {
        self.emojiTotal = 0;
    }else{
        self.emojiTotal = (int)[self.faceEmojiArray count];
    }
    self.emojiColumn = (int)self.frame.size.width / self.emojiSpanVertial;
    int left = (int)self.frame.size.width % 44;
    if (left < 12) {
        self.emojiColumn--;
        left += self.emojiSpanVertial;
    }
    self.emojiMaxCountPerPage = self.emojiColumn * self.emojiRow - 1;
    self.emojiMariginHorizontalMin = left / 2;
    self.emojiTotalPage =
        self.emojiTotal / self.emojiMaxCountPerPage + (self.emojiTotal % self.emojiMaxCountPerPage ? 1 : 0);
}

- (void)loadEmojiView {
    float startPos_X = self.emojiMariginHorizontalMin, startPos_Y = 12;
    for (int i = 0; i < self.emojiTotal; i++) {
        int pageIndex = i / self.emojiMaxCountPerPage;
        float emojiPosX = startPos_X + 44 * (i % self.emojiMaxCountPerPage % self.emojiColumn) + 4 +
                          pageIndex * self.frame.size.width;
        float emojiPosY = startPos_Y + 44 * (i % self.emojiMaxCountPerPage / self.emojiColumn) + 4;
        UIButton *emojiBtn =
            [[UIButton alloc] initWithFrame:CGRectMake(emojiPosX, emojiPosY, RC_EMOJI_WIDTH, RC_EMOJI_WIDTH)];
        emojiBtn.titleLabel.font = [UIFont systemFontOfSize:32];
        [emojiBtn setBackgroundColor:[UIColor clearColor]];
        
        [emojiBtn setTitle:self.faceEmojiArray[i] forState:UIControlStateNormal];
        [emojiBtn addTarget:self action:@selector(emojiBtnHandle:) forControlEvents:UIControlEventTouchUpInside];
        [self.emojiBackgroundView addSubview:emojiBtn];

        if (((i + 1) >= self.emojiMaxCountPerPage && (i + 1) % self.emojiMaxCountPerPage == 0) ||
            i == self.emojiTotal - 1) {
            CGRect frame = emojiBtn.frame;
            UIButton *deleteButton =
                [[UIButton alloc] initWithFrame:CGRectMake(self.frame.size.width - self.emojiMariginHorizontalMin - 44,
                                                           frame.origin.y, RC_EMOJI_WIDTH, RC_EMOJI_WIDTH)];
            deleteButton.titleLabel.font = [UIFont systemFontOfSize:32];
            [deleteButton setBackgroundColor:[UIColor clearColor]];
            [deleteButton addTarget:self
                             action:@selector(emojiBtnHandle:)
                   forControlEvents:UIControlEventTouchUpInside];
            frame.origin.x =
                self.frame.size.width - self.emojiMariginHorizontalMin - 44 + pageIndex * self.frame.size.width;
            frame.size = CGSizeMake(35, 23);
            frame.origin.y = emojiPosY + (RC_EMOJI_WIDTH - frame.size.height)/2;
            deleteButton.frame = frame;
            [deleteButton setBackgroundImage:IMAGE_BY_NAMED(@"emoji_btn_delete") forState:UIControlStateNormal];
            [self.emojiBackgroundView addSubview:deleteButton];
        }
    }
}

- (void)emojiBtnHandle:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(didTouchEmojiView:touchedEmoji:)]) {
        [self.delegate didTouchEmojiView:self touchedEmoji:sender.titleLabel.text];
    }
}

@end
