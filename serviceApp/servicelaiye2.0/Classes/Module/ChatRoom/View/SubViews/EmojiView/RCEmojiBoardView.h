//
//  RCEmojiView.h
//  RCIM
//
//  Created by Heq.Shinoda on 14-5-29.
//  Copyright (c) 2014年 RongCloud. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RCEmojiPageControl;
@class RCEmojiBoardView;

@protocol RCEmojiViewDelegate <NSObject>
@optional

- (void)didTouchEmojiView:(RCEmojiBoardView *)emojiView touchedEmoji:(NSString *)string;

- (void)didSendButtonEvent:(RCEmojiBoardView *)emojiView sendButton:(UIButton *)sendButton;
@end

@interface RCEmojiBoardView : UIView <UIScrollViewDelegate> {

    NSInteger currentIndex;
}

@property(nonatomic, strong) UIScrollView *emojiBackgroundView;

@property(nonatomic, strong) UILabel *emojiLabel;

@property(nonatomic, weak) id<RCEmojiViewDelegate> delegate;

@end
