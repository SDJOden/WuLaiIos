//
//  RCChatSessionInputBarControl.m
//  RongIMKit
//
//  Created by xugang on 15/2/12.
//  Copyright (c) 2015年 RongCloud. All rights reserved.
//

#import "RCChatSessionInputBarControl.h"

typedef void (^RCAnimationCompletionBlock)(BOOL finished);

@interface RCChatSessionInputBarControl () <UITextViewDelegate>

@property (nonatomic, strong) UIView *textViewBackgroundView;

@end

@implementation RCChatSessionInputBarControl

- (id)initWithFrame:(CGRect)frame
    withContextView:(UIView *)contextView
               type:(RCChatSessionInputBarControlType)type
              style:(RCChatSessionInputBarControlStyle)style {
    if (self = [super initWithFrame:frame]) {
        [self setInputBarType:type style:style];
        self.originalPositionY = frame.origin.y;
        
        self.inputTextview_height=36.0f;
        self.layer.borderColor = [UIColor colorWithRed:191 / 255.f green:191 / 255.f blue:191 / 255.f alpha:1].CGColor;
        self.layer.borderWidth = 0.5;
        self.backgroundColor = [UIColor whiteColor];
        _isInputSug = YES;
    }
    return self;
}

- (void)setInputBarType:(RCChatSessionInputBarControlType)type style:(RCChatSessionInputBarControlStyle)style {
    
    if (self.inputContainerView) {
        [self.inputContainerView removeFromSuperview];
        self.inputContainerView = nil;
    }
        
    self.inputContainerView = [[UIView alloc] initWithFrame:self.bounds];
    self.inputContainerView.backgroundColor = [UIColor colorWithRed:248 / 255.f green:248 / 255.f blue:248 / 255.f alpha:1];
    [self addSubview:_inputContainerView];
    
    [self setLayoutForInputContainerView:style];
    [self registerForNotifications];
}

#pragma mark --- 懒加载

- (RCTextView *)inputTextView {
    if (!_inputTextView) {
        _inputTextView = [[RCTextView alloc] initWithFrame:CGRectZero];
        _inputTextView.delegate = self;
        [_inputTextView setExclusiveTouch:YES];
        [_inputTextView setTextColor:[UIColor blackColor]];
        [_inputTextView setFont:[UIFont systemFontOfSize:16]];
        [_inputTextView setReturnKeyType:UIReturnKeySend];
        _inputTextView.backgroundColor = [UIColor clearColor];
        _inputTextView.enablesReturnKeyAutomatically = YES;
        [_inputTextView setAccessibilityLabel:@"chat_input_textView"];
        [self.inputContainerView addSubview:_inputTextView];
    }
    return _inputTextView;
}

- (SDJCommonBtn *)sugButton {
    if (!_sugButton) {
        _sugButton = [[SDJCommonBtn alloc] initWithFrame:CGRectZero];
        _sugButton.btnType = ImageOnly;
        _sugButton.ImageSize = CGSizeMake(30, 30);
        [_sugButton setBackgroundImage:[UIImage imageNamed:@"sugIconImage"] forState:UIControlStateNormal];
        [_sugButton setBackgroundImage:[UIImage imageNamed:@"sugIconImage_heighlight"] forState:UIControlStateSelected];
        [_sugButton setExclusiveTouch:YES];
        [_sugButton addTarget:self action:@selector(didTouchSugButton:) forControlEvents:UIControlEventTouchUpInside];
        [self.inputContainerView addSubview:_sugButton];
    }
    return _sugButton;
}

- (SDJCommonBtn *)pictureButton {
    if (!_pictureButton) {
        _pictureButton = [[SDJCommonBtn alloc] initWithFrame:CGRectZero];
        _pictureButton.btnType = ImageOnly;
        _pictureButton.ImageSize = CGSizeMake(30, 30);
        [_pictureButton setBackgroundImage:[UIImage imageNamed:@"picIconImage"] forState:UIControlStateNormal];
        [_pictureButton setBackgroundImage:[UIImage imageNamed:@"picIconImage"] forState:UIControlStateSelected];
        [_pictureButton setExclusiveTouch:YES];
        [_pictureButton addTarget:self action:@selector(didTouchPictureButton:) forControlEvents:UIControlEventTouchUpInside];
        [self.inputContainerView addSubview:_pictureButton];
    }
    return _pictureButton;
}

- (SDJCommonBtn *)emojiButton {
    if (!_emojiButton) {
        _emojiButton = [[SDJCommonBtn alloc] initWithFrame:CGRectZero];
        _emojiButton.btnType = ImageOnly;
        _emojiButton.ImageSize = CGSizeMake(30, 30);
        [_emojiButton setBackgroundImage:[UIImage imageNamed:@"emojiIconImage"] forState:UIControlStateNormal];
        [_emojiButton setBackgroundImage:[UIImage imageNamed:@"emojiIconImage_highlight"] forState:UIControlStateSelected];
        [_emojiButton setExclusiveTouch:YES];
        [_emojiButton addTarget:self action:@selector(didTouchEmojiButton:) forControlEvents:UIControlEventTouchUpInside];
        [self.inputContainerView addSubview:_emojiButton];
    }
    return _emojiButton;
}

- (UIView *)textViewBackgroundView {
    if(_textViewBackgroundView == nil) {
        _textViewBackgroundView = [[UIView alloc] init];
        _textViewBackgroundView.backgroundColor = [UIColor whiteColor];
        _textViewBackgroundView.layer.cornerRadius = 4.0;
        _textViewBackgroundView.layer.borderColor = [HEXCOLOR(0xD0D0D0)CGColor];
        _textViewBackgroundView.layer.borderWidth = 0.5;
        _textViewBackgroundView.layer.masksToBounds = YES;
        UITapGestureRecognizer *textViewBackgroundViewTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(textViewBackgroundTap:)];
        [_textViewBackgroundView addGestureRecognizer:textViewBackgroundViewTap];
        [self.inputContainerView addSubview:_textViewBackgroundView];
    }
    return _textViewBackgroundView;
}

- (UILabel *)placehoderLabel {
    if(_placehoderLabel == nil) {
        _placehoderLabel = [[UILabel alloc] init];
        _placehoderLabel.text = @"请输入文字";
        [_placehoderLabel setTextColor:[UIColor lightGrayColor]];
        [_placehoderLabel setFont:[UIFont systemFontOfSize:16]];
        _placehoderLabel.backgroundColor = [UIColor clearColor];
        _placehoderLabel.hidden = NO;
        [self.textViewBackgroundView addSubview:_placehoderLabel];
    }
    return _placehoderLabel;
}

- (void)textViewDidChange:(UITextView *)textView {
    [self changeChatSessionInputBarFrame];
    if (textView.text.length == 0 && [self.delegate respondsToSelector:@selector(cleanTextView)]) {
        [self.delegate cleanTextView];
        self.closeSugButton.enabled = NO;
        self.closeSugButton.alpha = 0.5;
    } else {
        self.closeSugButton.enabled = YES;
        self.closeSugButton.alpha = 1;
    }
    
    self.placehoderLabel.hidden = !(textView.text.length == 0);
    if (_isInputSug == YES) {
        if ([self.delegate respondsToSelector:@selector(chatInputTextSugText:)]) {
            [self.delegate chatInputTextSugText:textView.text];
        }
    }
}

- (void)setLayoutForInputContainerView:(RCChatSessionInputBarControlStyle)style {
    
    [self.inputContainerView makeConstraints:^(MASConstraintMaker *make) {
        make.right.left.top.bottom.equalTo(self);
    }];
    
    
    [self.sugButton makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.inputContainerView).offset(-13);
        make.height.width.equalTo(24);
        make.left.equalTo(self.inputContainerView).offset(10);
    }];
    
    [self.textViewBackgroundView makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.sugButton.right).offset(10);
        make.bottom.equalTo(self.inputContainerView).offset(-6.5);
        make.top.equalTo(self.inputContainerView).offset(6.5);
    }];
    
    [self.inputTextView makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.sugButton.right).offset(10+6);
        make.top.equalTo(self.inputContainerView).offset(7);
        make.bottom.equalTo(self.inputContainerView).offset(-7);
        make.right.equalTo(self.textViewBackgroundView.right).offset(-6);
    }];
    
    [self.emojiButton makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.inputContainerView).offset(-13);
        make.left.equalTo(self.textViewBackgroundView.right).offset(10);
        make.height.width.equalTo(24);
    }];
    
    [self.pictureButton makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.emojiButton.right).offset(10);
        make.bottom.equalTo(self.inputContainerView).offset(-13);
        make.height.width.equalTo(24);
    }];
    
    [self.closeSugButton makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.pictureButton.right).offset(10);
        make.bottom.equalTo(self.inputContainerView).offset(-13);
        make.right.equalTo(self.inputContainerView.right).offset(-5);
        make.height.equalTo(24);
        make.width.equalTo(60);
    }];
    
    [self.placehoderLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.textViewBackgroundView).offset(10);
        make.top.bottom.equalTo(self.textViewBackgroundView);
    }];
    
    [self layoutIfNeeded];
}

- (BOOL)canBecomeFirstResponder {
    
    return YES;
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {

    return NO; //隐藏系统默认的菜单项
}

#pragma mark - Notifications

- (void)registerForNotifications {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didReceiveKeyboardWillShowNotification:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didReceiveKeyboardWillHideNotification:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)didReceiveKeyboardWillShowNotification:(NSNotification *)notification {
    [MobClick event:@"chatroom_keyBoardSystem_click"];
    NSDictionary *userInfo = [notification userInfo];
    
    CGRect keyboardEndFrame = [userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    UIViewAnimationCurve animationCurve = [userInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue];
    NSInteger animationCurveOption = (animationCurve << 16);
    
    double animationDuration = [userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    [UIView animateWithDuration:animationDuration
                          delay:0.0
                        options:animationCurveOption
                     animations:^{
                         if ([self.delegate respondsToSelector:@selector(keyboardWillShowWithFrame:)]) {
                             [self.delegate keyboardWillShowWithFrame:keyboardEndFrame];
                         }
                     }
                     completion:^(BOOL finished) {}];
}

- (void)didReceiveKeyboardWillHideNotification:(NSNotification *)notification {
    
    if ([self.delegate respondsToSelector:@selector(keyboardWillHide)]) {
        [self.delegate keyboardWillHide];
    }
}

// 点击事件
- (void)didTouchSugButton:(UIButton *)sender {
    [MobClick event:@"chatroom_keyBoardSug_click"];
    [self.delegate didTouchSugButton:sender];
}

- (void)didTouchPictureButton:(UIButton *)sender {
    [MobClick event:@"chatroom_keyBoardPhoto_click"];
    [self.delegate didTouchPictureButton:sender];
}

- (void)didTouchEmojiButton:(UIButton *)sender {
    [MobClick event:@"chatroom_keyBoardExpression_click"];
    [self.delegate didTouchEmojiButton:sender];
}

#pragma mark <UITextViewDelegate>
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if([text length] == 0) {
        [MobClick event:@"chatroom_keyBoardDelete_click"];
        
        
    }
    
    if ([text isEqualToString:@"\n"]) {
        
        if ([self.delegate respondsToSelector:@selector(didTouchKeyboardReturnKey:text:)]) {
            NSString *_needToSendText = textView.text;
            NSString *_formatString =
            [_needToSendText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            if (0 == [_formatString length]) {
            } else {
                [self.delegate didTouchKeyboardReturnKey:self text:[_needToSendText copy]];
            }
        }
        self.inputTextView.text = @"";
        
        _inputTextview_height = 36.0f;
        
        CGRect intputTextRect = self.inputTextView.frame;
        intputTextRect.size.height = _inputTextview_height;
        intputTextRect.origin.y = 7;
        [_inputTextView setFrame:intputTextRect];
        
        CGRect vRect = self.frame;
        vRect.size.height = Height_ChatSessionInputBar;
        vRect.origin.y = _originalPositionY;
        [self changeSize:vRect];
        
        [self.delegate chatSessionInputBarControlContentSizeChanged:vRect];
        
        return NO;
    }
    
    _inputTextview_height = 36.0f;
    if (_inputTextView.contentSize.height < 70 && _inputTextView.contentSize.height > 36.0) {
        _inputTextview_height = _inputTextView.contentSize.height;
    }
    if (_inputTextView.contentSize.height >= 70) {
        _inputTextview_height = 70;
    }
    
    CGSize textViewSize=[self TextViewAutoCalculateRectWith:text FontSize:16.0 MaxSize:CGSizeMake(_inputTextView.frame.size.width, 70)];
    
    CGSize textSize=[self TextViewAutoCalculateRectWith:textView.text FontSize:16.0 MaxSize:CGSizeMake(_inputTextView.frame.size.width, 70)];
    
    if (textViewSize.height<=36.0f&&range.location==0) {
        _inputTextview_height=36.0f;
    }
    else if(textViewSize.height>36.0f&&textViewSize.height<=55.0f)
    {
        _inputTextview_height=55.0f;
    }
    else if (textViewSize.height>55)
    {
        _inputTextview_height=70.0f;
    }
    
    if ([text isEqualToString:@""]&&range.location!=0) {
        if (textSize.width>=_inputTextView.frame.size.width&&range.length==1) {
            if (_inputTextView.contentSize.height < 70 && _inputTextView.contentSize.height > 36.0f) {
                _inputTextview_height = _inputTextView.contentSize.height;
            }
            if (_inputTextView.contentSize.height >= 70) {
                _inputTextview_height = 70;
            }
        }
        
        else
        {
            NSString *headString=[textView.text substringToIndex:range.location];
            NSString *lastString=[textView.text substringFromIndex:range.location+range.length];
            
            CGSize locationSize=[self TextViewAutoCalculateRectWith:[NSString stringWithFormat:@"%@%@",headString,lastString] FontSize:16.0 MaxSize:CGSizeMake(_inputTextView.frame.size.width, 70)];
            if (locationSize.height<=36.0) {
                _inputTextview_height=36.0;
                
            }
            if (locationSize.height>36.0&&locationSize.height<=55.0) {
                _inputTextview_height=55.0;
                
            }
            if (locationSize.height>55.0) {
                _inputTextview_height=70.0;
                
            }
        }
    }
    
    CGRect intputTextRect = self.inputTextView.frame;
    intputTextRect.size.height = _inputTextview_height;
    intputTextRect.origin.y = 7;
    [_inputTextView setFrame:intputTextRect];
    
    CGRect vRect = self.frame;
    vRect.size.height = Height_ChatSessionInputBar + (_inputTextview_height - 36);
    vRect.origin.y = _originalPositionY - (_inputTextview_height - 36);
    [self changeSize:vRect];
    [self.delegate chatSessionInputBarControlContentSizeChanged:vRect];
    
    if (_inputTextview_height > 70.0) {
        textView.contentOffset = CGPointMake(0, 100);
    }
    return YES;
}

- (void)changeChatSessionInputBarFrame {
    
    _inputTextview_height = 36.0f;
    if (_inputTextView.contentSize.height < 70 && _inputTextView.contentSize.height > 36.0f) {
        _inputTextview_height = _inputTextView.contentSize.height;
    }
    if (_inputTextView.contentSize.height >= 70) {
        _inputTextview_height = 70;
    }
    
    CGSize textViewSize=[self TextViewAutoCalculateRectWith:_inputTextView.text FontSize:16.0 MaxSize:CGSizeMake(_inputTextView.frame.size.width, 70)];
    
    if (textViewSize.height<=36.0f) {
        _inputTextview_height=36.0f;
    }
    else if(textViewSize.height>36.0f&&textViewSize.height<=55.0f)
    {
        _inputTextview_height=55.0f;
    }
    else if (textViewSize.height>55)
    {
        _inputTextview_height=70.0f;
    }
    
    CGRect intputTextRect = self.inputTextView.frame;
    intputTextRect.size.height = _inputTextview_height;
    intputTextRect.origin.y = 7;
    [_inputTextView setFrame:intputTextRect];
    
    CGRect vRect = self.frame;
    vRect.size.height = Height_ChatSessionInputBar + (_inputTextview_height - 36);
    vRect.origin.y = _originalPositionY - (_inputTextview_height - 36);
    [self changeSize:vRect];
    [self.delegate chatSessionInputBarControlContentSizeChanged:vRect];
    
    if (_inputTextview_height>70.0) {
        _inputTextView.contentOffset=CGPointMake(0, 100);
    }
}

- (void)changeSize:(CGRect)rect {
    [_inputTextView updateConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(_inputTextview_height);
        make.top.equalTo(self.inputContainerView).offset(7);
    }];
}

- (CGSize)TextViewAutoCalculateRectWith:(NSString*)text FontSize:(CGFloat)fontSize MaxSize:(CGSize)maxSize
{
    NSMutableParagraphStyle* paragraphStyle = [[NSMutableParagraphStyle alloc]init];
    paragraphStyle.lineBreakMode=NSLineBreakByWordWrapping;
    NSDictionary* attributes =@{NSFontAttributeName:[UIFont systemFontOfSize:fontSize],NSParagraphStyleAttributeName:paragraphStyle.copy};
    if (text) {
        CGSize labelSize = [text boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading|NSStringDrawingTruncatesLastVisibleLine attributes:attributes context:nil].size;
        labelSize.height=ceil(labelSize.height);
        labelSize.width=ceil(labelSize.width);
        return labelSize;
    } else {
        return CGSizeZero;
    }
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (UIButton *)closeSugButton {
	if(_closeSugButton == nil) {
		_closeSugButton = [[UIButton alloc] init];
        [_closeSugButton setTitle:@"关闭SUG" forState:UIControlStateNormal];
        [_closeSugButton setTitleColor:HEXCOLOR(0x979797) forState:UIControlStateNormal];
        [_closeSugButton setTitleColor:HEXCOLOR(0xffffff) forState:UIControlStateHighlighted];
        _closeSugButton.titleLabel.font = [UIFont systemFontOfSize:12];
        _closeSugButton.layer.cornerRadius = 12.0;
        _closeSugButton.layer.borderWidth = 1;
        _closeSugButton.layer.borderColor = [HEXCOLOR(0x979797)CGColor];
        [_closeSugButton setBackgroundImage:[UIImage getRoundImageWithSize:CGSizeMake(60, 24) backgroundColor:[UIColor clearColor] lineWidth:0 cornerRadius:0 shouldFill:YES] forState:UIControlStateNormal];
        [_closeSugButton setBackgroundImage:[UIImage getRoundImageWithSize:CGSizeMake(60, 24) backgroundColor:HEXCOLOR(0x4B9EFE) lineWidth:0 cornerRadius:0 shouldFill:YES] forState:UIControlStateHighlighted];
        _closeSugButton.layer.masksToBounds = YES;
        [_closeSugButton addTarget:self action:@selector(closeSugView:) forControlEvents:UIControlEventTouchUpInside];
        _closeSugButton.enabled = NO;
        _closeSugButton.alpha = 0.5;
        [self.inputContainerView addSubview:_closeSugButton];
	}
	return _closeSugButton;
}

-(void)closeSugView:(UIButton *)sender{
    _isInputSug = NO;
    if ([self.delegate respondsToSelector:@selector(didTouchCloseSugButton:)]) {
        [self.delegate didTouchCloseSugButton:sender];
    }
}

-(void)textViewBackgroundTap:(UIGestureRecognizer *)gestureRecognizer{
    [self.inputTextView becomeFirstResponder];
}

@end
