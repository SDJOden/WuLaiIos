//
//  RCLocationViewController.h
//  iOS-IMKit
//
//  Created by YangZigang on 14/11/4.
//  Copyright (c) 2014年 RongCloud. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>

@interface RCLocationViewController : UIViewController <MKMapViewDelegate>

/** 需要显示的位置坐标 */
@property(nonatomic, assign) CLLocationCoordinate2D location;
/** 需要显示的位置名称 */
@property(nonatomic, copy) NSString *locationName;

@end
