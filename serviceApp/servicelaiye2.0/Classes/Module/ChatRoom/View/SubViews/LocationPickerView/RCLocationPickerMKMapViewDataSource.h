//
//  RCLocationPickerMKMapViewDataSource.h
//  iOS-IMKit
//
//  Created by YangZigang on 14/11/5.
//  Copyright (c) 2014年 RongCloud. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RCLocationPickerViewController.h"

@interface RCLocationPickerMKMapViewDataSource : NSObject <RCLocationPickerViewControllerDataSource, MKMapViewDelegate>

@end
