//
//  RCLocationPickerViewController.h
//  iOS-IMKit
//
//  Created by YangZigang on 14/10/31.
//  Copyright (c) 2014年 RongCloud. All rights reserved.
//

#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import <UIKit/UIKit.h>

/** POI搜索结束后用于回调的block

 参数： pois 需要显示的POI列表

 参数： clearPreviousResult 如果地图位置已经发生变化，需要清空之前的POI数据

 参数： hasMore 如果POI数据很多，可以进行“更多”显示

 参数： error  搜索POI出现错误时，返回错误信息
 */
typedef void (^OnPoiSearchResult)(NSArray *pois, BOOL clearPreviousResult, BOOL hasMore, NSError *error);

@protocol RCLocationPickerViewControllerDelegate;
@protocol RCLocationPickerViewControllerDataSource;

/**
 * 位置选取视图控制器
 */
@interface RCLocationPickerViewController: UIViewController <CLLocationManagerDelegate, UITableViewDataSource, UITableViewDelegate>

@property(nonatomic, weak) id<RCLocationPickerViewControllerDelegate> delegate;

@property(nonatomic, strong) id<RCLocationPickerViewControllerDataSource> dataSource;

@property(nonatomic, strong) IBOutlet UIView *mapViewContainer;

- (instancetype)initWithDataSource:(id<RCLocationPickerViewControllerDataSource>)dataSource;

@end

@protocol RCLocationPickerViewControllerDataSource <NSObject>

@optional
/**
 @return 在界面上显示的地图控件
 */
- (UIView *)mapView;

/**
 @return 界面上显示的中心点标记。如不想显示中心点标记，可以返回nil。
 */
- (CALayer *)annotationLayer;

/**
 *  位置标注名称
 */
- (NSString *)titleOfPlaceMark:(id)placeMark;

/**
 *  获取位置标注坐标
 */
- (CLLocationCoordinate2D)locationCoordinate2DOfPlaceMark:(id)placeMark;

/**
 @param location 地图中心点
 */
- (void)setMapViewCenter:(CLLocationCoordinate2D)location animated:(BOOL)animated;

/**
 @param coordinateRegion 地图显示区域
 */
- (void)setMapViewCoordinateRegion:(MKCoordinateRegion)coordinateRegion animated:(BOOL)animated;

/**
 *  @param placeMark 用户选择了某一个位置标注
 */
- (void)userSelectPlaceMark:(id)placeMark;

/**
 *  地图中心点坐标
 */
- (CLLocationCoordinate2D)mapViewCenter;

/**
 * 设置POI搜索完毕后的回调block
 */
- (void)setOnPoiSearchResult:(OnPoiSearchResult)poiSearchResult;

/**
 * 获取当前视野中POI
 */
- (void)beginFetchPoisOfCurrentLocation;

/**
 * @return 对当前地图进行截图，该截图的缩略图会包含在发送给好友的位置消息中。
 */
- (UIImage *)mapViewScreenShot;

@end

@protocol RCLocationPickerViewControllerDelegate <NSObject>

- (void)locationPicker:(RCLocationPickerViewController *)locationPicker
     didSelectLocation:(CLLocationCoordinate2D)location
          locationName:(NSString *)locationName
         mapScreenShot:(UIImage *)mapScreenShot;

@end
