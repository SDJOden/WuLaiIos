//
//  SDJIntelligentBoardView.h
//  RongIMKit
//
//  Created by 盛东 on 15/10/12.
//  Copyright © 2015年 RongCloud. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RCEmojiPageControl.h"

@class SDJIntelligentBoardView;

@protocol SDJIntelligentBoardViewDelegate <NSObject>

- (void)didSelectedIntelligentBoardView:(SDJIntelligentBoardView *)intelligentView textSelected:(NSString *)text withTag:(int)tag;

@end

@interface SDJIntelligentBoardView : UIView <UIScrollViewDelegate>

{
    RCEmojiPageControl *pageControl;
    NSInteger currentPage;
}

@property(nonatomic, strong) NSArray *inteligentMessages;

@property(nonatomic, weak) id<SDJIntelligentBoardViewDelegate> delegate;

// 初始化
- (instancetype)initWithFrame:(CGRect)frame andMessageArray:(NSArray *)array;

-(CGFloat)getViewHeight;

@end
