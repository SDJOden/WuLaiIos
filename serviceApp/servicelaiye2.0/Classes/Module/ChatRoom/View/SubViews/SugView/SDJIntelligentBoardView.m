//
//  SDJIntelligentBoardView.m
//  RongIMKit
//
//  Created by 盛东 on 15/10/12.
//  Copyright © 2015年 RongCloud. All rights reserved.
//

// 虚线是一张背景图, 自己建立UIImage的分类去绘制

#import "SDJIntelligentBoardView.h"
#define BTN_MAXROW 4
#define COLOR_F0DEDA HEXCOLOR(0xf7f7f7)

// 不用自动布局

@interface SDJIntelligentBoardView ()

@property (nonatomic, strong) UIScrollView *scrollView;

@property (nonatomic, strong) UIPageControl *pageControl;

@property(nonatomic, assign) int btnTotal;

@property (nonatomic, assign) CGFloat contentOffset;

@property (nonatomic, strong) NSMutableArray *mutableArray;

@end

@implementation SDJIntelligentBoardView

- (instancetype)initWithFrame:(CGRect)frame andMessageArray:(NSArray *)array {
    
    if (self = [super initWithFrame:frame]) {
        // 界面初始化设置
        self.inteligentMessages = [[NSArray alloc] initWithArray:array];
        self.backgroundColor = COLOR_F0DEDA;
    }
    return self;
}

- (void)setInteligentMessages:(NSArray *)inteligentMessages {
    
    for (NSString *str in inteligentMessages) {
        if ([str isEqualToString:@"default"]) {
            continue;
        }
        [self.mutableArray addObject:str];
    }
    _inteligentMessages = [[NSArray alloc] initWithArray:self.mutableArray];
    
    [self loadBtnList];
}

- (void)loadBtnList {
    
    [self.scrollView scrollsToTop];
    // 算页码
    if (nil == self.inteligentMessages ||  [self.inteligentMessages count] == 0) {
        self.btnTotal = 0;
    }else{
        self.btnTotal = (int)[self.inteligentMessages count];
    }
    
    // 设置按钮, 设置之前做一次清空
    for (__strong UIView *view in self.scrollView.subviews) {
        if ([view isKindOfClass:[UIButton class]]) {
            [view removeFromSuperview];
            view = nil;
        }
    }

    int number = 0;
    
    int page = 1;
    
    CGFloat btnCurrent_height = 0;
    
    CGFloat btn_width = 0;
    
    CGFloat x = 15;
    CGFloat y = 0;
    
    CGFloat topX = 15;//最开始的按钮距离左边距离
    
    CGFloat topY = 15;//最开始的按钮距离上边距离
    
    CGFloat marginX = 10;//横间距
    
    CGFloat marginY = 10;//纵间距
    
    CGFloat btn_height = (self.bounds.size.height - 45 - (BTN_MAXROW - 1) * marginY) / BTN_MAXROW;
    btn_height = ceil(btn_height);
    
    CGFloat contentOffset = 0;
    
    for (int i = 0; i < self.btnTotal; i++) {
        CGSize __textSize = CGSizeZero;
        __textSize = RC_MULTILINE_TEXTSIZE_GEIOS7(self.inteligentMessages[i], [UIFont systemFontOfSize:15], CGSizeMake(self.bounds.size.width - 50,8000));
        __textSize = CGSizeMake(ceilf(__textSize.width), ceilf(__textSize.height));
        
        btn_width = __textSize.width + 12 + 12;
        
        
        if (i == 0) {
            y = topY;
        }
        else {
            if (x + marginX + btn_width >= (2 * page - 1) * [UIScreen mainScreen].bounds.size.width - topX) { // 下一个按钮超出, 那么这一个不能放在这一行
                x = topX;
                number ++;
                y = y + btnCurrent_height + marginY;
                if (y + __textSize.height > 180 - 30) {//如果这一页放不开就放下一页
                    page ++;
                    topX += [UIScreen mainScreen].bounds.size.width;
                    x = topX;
                    y = topY;
                }
            }
        }
        
        
        btnCurrent_height = __textSize.height > btn_height ? __textSize.height + 20 : btn_height;
        
        // 添加按钮
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(x, y, btn_width, btnCurrent_height)];
        [btn.titleLabel setFont:[UIFont systemFontOfSize:15]];
        btn.titleLabel.numberOfLines = 0;
        
        btn.tag = 4200 + i;
        btn.contentHorizontalAlignment=UIControlContentHorizontalAlignmentLeft;
        btn.titleEdgeInsets = UIEdgeInsetsMake(0, 12, 0, 0);
        btn.titleLabel.numberOfLines = 3;
        [btn setTitle:self.inteligentMessages[i] forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(btnClicked:) forControlEvents:UIControlEventTouchUpInside];
        [btn setTitleColor:HEXCOLOR(0x8B8B8B) forState:UIControlStateNormal];
        [btn setTitleColor:HEXCOLOR(0xffffff) forState:UIControlStateHighlighted];
        [btn setBackgroundImage:[UIImage getRoundImageWithSize:CGSizeMake(btn_width, btnCurrent_height) backgroundColor:HEXCOLOR(0xffffff) lineWidth:0 cornerRadius:0 shouldFill:YES] forState:UIControlStateNormal];
        [btn setBackgroundImage:[UIImage getRoundImageWithSize:CGSizeMake(btn_width, btnCurrent_height) backgroundColor:HEXCOLOR(0x8B8B8B) lineWidth:0 cornerRadius:0 shouldFill:YES] forState:UIControlStateHighlighted];
        btn.layer.borderWidth = 0.5;
        btn.layer.borderColor = [[UIColor lightGrayColor]CGColor];
        btn.layer.cornerRadius = 5.0;
        btn.layer.masksToBounds = YES;
        [self.scrollView addSubview:btn];
        
        // 添加完按钮后修改x
        x = x + marginX + btn_width; // 下一个x
        
        if (x + marginX + btn_width >= [UIScreen mainScreen].bounds.size.width - topX && i != 0) {
            contentOffset += btnCurrent_height + marginY;
        }
    }
    contentOffset = page * [UIScreen mainScreen].bounds.size.width;
    
    self.pageControl.currentPage = 0;
    self.pageControl.numberOfPages = page;
    if (page == 1) {
        [self.pageControl removeFromSuperview];
        self.pageControl = nil;
    }
    self.scrollView.contentSize = CGSizeMake(contentOffset, 0);
    self.contentOffset = contentOffset;
}

- (void)btnClicked:(UIButton *)sender {
    
    sender.backgroundColor = HEXCOLOR(0xffffff);
    if ([self.delegate respondsToSelector:@selector(didSelectedIntelligentBoardView:textSelected:withTag:)]) {
        [self.delegate didSelectedIntelligentBoardView:self textSelected:sender.titleLabel.text withTag:(int)sender.tag];
    }
}

- (UIScrollView *)scrollView {
    
	if(_scrollView == nil) {
        _scrollView = [[UIScrollView alloc] initWithFrame:self.frame];
        _scrollView.delegate = self;
        _scrollView.showsHorizontalScrollIndicator = NO;
        _scrollView.backgroundColor = [UIColor clearColor];
        _scrollView.pagingEnabled = YES;
        [self addSubview:_scrollView];
	}
	return _scrollView;
}

- (UIPageControl *)pageControl {
    
    if(_pageControl == nil) {
        _pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, self.frame.size.height - 20, self.frame.size.width, 20)];
        _pageControl.userInteractionEnabled = NO;
        _pageControl.currentPageIndicatorTintColor = [UIColor grayColor];
        _pageControl.pageIndicatorTintColor = [UIColor lightGrayColor];
        [self addSubview:_pageControl];
    }
    return _pageControl;
}

-(CGFloat)getViewHeight{
    
    return self.contentOffset;
}

//当滚动视图发生滚动是就触发
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    CGPoint offset = scrollView.contentOffset;
    //    把值行进四舍五入，使用round()函数
    NSInteger index = round(offset.x / [UIScreen mainScreen].bounds.size.width);
    self.pageControl.currentPage = index;
}

- (NSMutableArray *)mutableArray {
    
	if(_mutableArray == nil) {
		_mutableArray = [[NSMutableArray alloc] init];
	}
	return _mutableArray;
}

@end