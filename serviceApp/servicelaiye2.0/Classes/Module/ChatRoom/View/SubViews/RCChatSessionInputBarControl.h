//
//  RCChatSessionInputBarControl.h
//  RongIMKit
//
//  Created by xugang on 15/2/12.
//  Copyright (c) 2015年 RongCloud. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RCTextView.h"
#define Height_ChatSessionInputBar 50
#import "SDJCommonBtn.h"

typedef NS_ENUM(NSInteger, RCChatSessionInputBarControlStyle) {

    RC_CHAT_INPUT_BAR_STYLE_SWITCH_CONTAINER_EXTENTION = 0,
};

typedef NS_ENUM(NSInteger, RCChatSessionInputBarControlType) {

    RCChatSessionInputBarControlDefaultType = 0,
};

typedef NS_ENUM(NSInteger, RCChatSessionInputBarInputType) {

    RCChatSessionInputBarInputText = 0,
};

@protocol RCChatSessionInputBarControlDelegate;

@interface RCChatSessionInputBarControl : UIView

@property(weak, nonatomic) id<RCChatSessionInputBarControlDelegate> delegate;

@property(strong, nonatomic) UIView *inputContainerView;

@property(strong, nonatomic) RCTextView *inputTextView;

@property(strong, nonatomic) SDJCommonBtn *sugButton;

@property(strong, nonatomic) SDJCommonBtn *pictureButton;

@property(strong, nonatomic) UIButton *recordButton;

@property(strong, nonatomic) SDJCommonBtn *emojiButton;

@property (nonatomic, strong) UIButton *closeSugButton;

@property(assign, nonatomic) float originalPositionY; // originalPositionY

@property(assign, nonatomic) float inputTextview_height; // inputTextview_height

@property (nonatomic, strong) UILabel *placehoderLabel;

@property (nonatomic, assign) BOOL isInputSug;
// 初始化
- (id)initWithFrame:(CGRect)frame
    withContextView:(UIView *)contextView
               type:(RCChatSessionInputBarControlType)type
              style:(RCChatSessionInputBarControlStyle)style;

// 设置输入栏的样式 可以在viewdidload后，可以设置样式
- (void)setInputBarType:(RCChatSessionInputBarControlType)type style:(RCChatSessionInputBarControlStyle)style;

- (void)changeSize:(CGRect)rect;

- (void)changeChatSessionInputBarFrame;

@end

// delegate 回调
@protocol RCChatSessionInputBarControlDelegate <NSObject>

@optional

-(void)chatInputTextSugText:(NSString *)InputText;

// 键盘frame事件
- (void)keyboardWillShowWithFrame:(CGRect)keyboardFrame;

//  键盘隐藏事件
- (void)keyboardWillHide;

// 输入框尺寸变化
- (void)chatSessionInputBarControlContentSizeChanged:(CGRect)frame;

#pragma mark - newSendBtn
- (void)didTouchKeyboardReturnKey:(RCChatSessionInputBarControl *)inputControl text:(NSString *)text;

- (void)didTouchSugButton:(UIButton *)sender;

- (void)didTouchSendButton:(UIButton *)sender;

- (void)didTouchEmojiButton:(UIButton *)sender;

- (void)didTouchPictureButton:(UIButton *)sender;

-(void)didTouchCloseSugButton:(UIButton *)sender;

-(void)cleanTextView;

@end
