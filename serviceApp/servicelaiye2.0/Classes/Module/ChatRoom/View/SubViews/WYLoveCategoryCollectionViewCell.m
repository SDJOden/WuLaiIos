//
//  WYLoveCategoryCollectionViewCell.m
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/5/17.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import "WYLoveCategoryCollectionViewCell.h"
#import "WYLoveTypeModel.h"

@interface WYLoveCategoryCollectionViewCell ()

@property (nonatomic, strong) UILabel *label;

@end

@implementation WYLoveCategoryCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initialize];
        
    }
    return self;
}

-(void)initialize{}

-(void)cellWithTitle:(WYLoveTypeModel *)model{
    
    if (model.selected) {
        self.contentView.backgroundColor = HEXCOLOR(0x4B9EFE);
    } else {
        self.contentView.backgroundColor = HEXCOLOR(0xffffff);
    }
    
    self.contentView.layer.cornerRadius = 10.0f;
    self.contentView.layer.borderWidth = 0.5f;
    self.contentView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.contentView.layer.masksToBounds = YES;
    
    self.label.text = model.type;
    
    [self.label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.bottom.equalTo(self);
    }];
}

- (UILabel *)label {
    
	if(_label == nil) {
		_label = [[UILabel alloc] init];
        _label.textColor = [UIColor blackColor];
        _label.backgroundColor = [UIColor clearColor];
        _label.font = [UIFont systemFontOfSize:12];
        _label.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:_label];
	}
	return _label;
}

@end
