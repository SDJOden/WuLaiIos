//
//  WYLoveCategoryCollectionViewCell.h
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/5/17.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import <UIKit/UIKit.h>

@class WYLoveTypeModel;

@interface WYLoveCategoryCollectionViewCell : UICollectionViewCell

-(void)cellWithTitle:(WYLoveTypeModel *)model;

@end
