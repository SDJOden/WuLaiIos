//
//  RCMessageTipLabel.h
//  iOS-IMKit
//
//  Created by Gang Li on 10/27/14.
//  Copyright (c) 2014 RongCloud. All rights reserved.
//

#ifndef __RCTipLabel
#define __RCTipLabel
#import <UIKit/UIKit.h>

@interface RCTipLabel : UILabel

@property(nonatomic, assign) UIEdgeInsets marginInsets;

+ (instancetype)greyTipLabel;

@end
#endif