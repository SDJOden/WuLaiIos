//
//  WYChangeGeniusTopView.h
//  servicelaiye2.0
//
//  Created by 汪洋 on 15/12/29.
//  Copyright © 2015年 shengdong. All rights reserved.
//

#import <UIKit/UIKit.h>

@class WYChangeGeniusTopView;

@protocol WYChangeGeniusTopViewDelegate <NSObject>

@optional

-(void)statuDidClicked:(WYChangeGeniusTopView *)changeGeniusTopView;

-(void)refreshDidClicked:(WYChangeGeniusTopView *)changeGeniusTopView;

@end

@interface WYChangeGeniusTopView : UIView

@property (nonatomic, strong) UITextField *chooseTextField;

@property (nonatomic, strong) UIButton *statuBtn;

@property (nonatomic,weak) id <WYChangeGeniusTopViewDelegate> changeGeniusTopDelegate;

-(void)changeBtnTitle:(NSString *)titleStr;

@end
