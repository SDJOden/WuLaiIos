//
//  WYChangeGeniusTopView.m
//  servicelaiye2.0
//
//  Created by 汪洋 on 15/12/29.
//  Copyright © 2015年 shengdong. All rights reserved.
//

#import "WYChangeGeniusTopView.h"

@interface WYChangeGeniusTopView () <UITextFieldDelegate>

@property (nonatomic, strong) UILabel *titleLb;

@property (nonatomic, strong) UILabel *connectStatuLb;

@property (nonatomic, strong) UIButton *refreshBtn;

@end

@implementation WYChangeGeniusTopView

-(void)layoutSubviews{
    
    [super layoutSubviews];
    
    [self.titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self);
        make.left.equalTo(self).offset(10);
        make.width.equalTo(70);
        make.height.equalTo(self);
    }];
    
    [self.chooseTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self);
        make.left.equalTo(self.titleLb.right).offset(10);
        make.height.equalTo(30);
    }];
    
    [self.connectStatuLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self);
        make.left.equalTo(self.chooseTextField.right).offset(10);
        make.width.equalTo(60);
        make.height.equalTo(30);
    }];
    
    [self.statuBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self);
        make.left.equalTo(self.connectStatuLb.right).offset(5);
        make.right.equalTo(self.refreshBtn.left).offset(-5);
        make.width.equalTo(60);
        make.height.equalTo(30);
    }];
    
    [self.refreshBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self);
        make.right.equalTo(self).offset(-10);
        make.width.equalTo(20);
        make.height.equalTo(20);
    }];
}

- (UILabel *)titleLb {
    
	if(_titleLb == nil) {
		_titleLb = [[UILabel alloc] init];
        _titleLb.text = @"助理列表";
        _titleLb.font = [UIFont boldSystemFontOfSize:16];
        [self addSubview:_titleLb];
	}
	return _titleLb;
}

- (UITextField *)chooseTextField {
    
	if(_chooseTextField == nil) {
		_chooseTextField = [[UITextField alloc] init];
        _chooseTextField.placeholder = @"筛选";
        _chooseTextField.delegate = self;
        _chooseTextField.borderStyle = UITextBorderStyleBezel;
        _chooseTextField.font = [UIFont systemFontOfSize:12];
        [self addSubview:_chooseTextField];
	}
	return _chooseTextField;
}

- (UILabel *)connectStatuLb {
    
	if(_connectStatuLb == nil) {
		_connectStatuLb = [[UILabel alloc] init];
        _connectStatuLb.text = @"接入状态";
        _connectStatuLb.font = [UIFont systemFontOfSize:12];
        [self addSubview:_connectStatuLb];
	}
	return _connectStatuLb;
}

- (UIButton *)statuBtn {
    
	if(_statuBtn == nil) {
        UIButton *statuBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        statuBtn.backgroundColor = [UIColor whiteColor];
        [statuBtn setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        [statuBtn setTitle:@"全部" forState:UIControlStateNormal];
        statuBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
        statuBtn.titleLabel.font = [UIFont systemFontOfSize:12];
        [statuBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        statuBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [statuBtn addTarget:self action:@selector(statuChange) forControlEvents:UIControlEventTouchUpInside];
        [statuBtn.layer setMasksToBounds:YES];
        [statuBtn.layer setCornerRadius:4.0]; //设置矩圆角半径
        [statuBtn.layer setBorderWidth:1.0];   //边框宽度
        CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
        CGColorRef colorref = CGColorCreate(colorSpace,(CGFloat[]){ 0.667, 0.667, 0.667, 1 });
        [statuBtn.layer setBorderColor:colorref];
        CGColorSpaceRelease(colorSpace);
        CGColorRelease(colorref);
        _statuBtn = statuBtn;
        [self addSubview:_statuBtn];
	}
	return _statuBtn;
}

- (UIButton *)refreshBtn {
    
    if(_refreshBtn == nil) {
        _refreshBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_refreshBtn setBackgroundImage:[UIImage imageNamed:@"refresh_hover"] forState:UIControlStateNormal];
        [_refreshBtn setBackgroundImage:[UIImage imageNamed:@"refresh"] forState:UIControlStateHighlighted];
        [_refreshBtn addTarget:self action:@selector(refreshData) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_refreshBtn];
    }
    return _refreshBtn;
}

-(void)changeBtnTitle:(NSString *)titleStr{
    
    [self.statuBtn setTitle:titleStr forState:UIControlStateNormal];
}

-(void)statuChange{
    
    [MobClick event:@"changeGenius_status_click"];
    if ([self.changeGeniusTopDelegate respondsToSelector:@selector(statuDidClicked:)]) {
        [self.changeGeniusTopDelegate statuDidClicked:self];
    }
}

-(void)refreshData{
    
    [MobClick event:@"changeGenius_refresh_click"];
    if ([self.changeGeniusTopDelegate respondsToSelector:@selector(refreshDidClicked:)]) {
        [self.changeGeniusTopDelegate refreshDidClicked:self];
    }
}

#pragma mark --- UITextViewDelegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    return [self.chooseTextField resignFirstResponder];
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"CallbackKeyboard" object:nil];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    [MobClick event:@"changeGenius_searchTextField_click"];
}

-(void)dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
