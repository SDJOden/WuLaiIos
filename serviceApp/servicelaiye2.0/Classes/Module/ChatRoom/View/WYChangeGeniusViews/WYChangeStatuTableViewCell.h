//
//  WYChangeStatuTableViewCell.h
//  servicelaiye2.0
//
//  Created by 汪洋 on 15/12/29.
//  Copyright © 2015年 shengdong. All rights reserved.
//

#import <UIKit/UIKit.h>

@class WYChangeGeniusListModel;

@interface WYChangeStatuTableViewCell : UITableViewCell

-(void)setContentByHistoryModel:(WYChangeGeniusListModel *)changeGeniusListModel;

@end
