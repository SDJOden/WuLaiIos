//
//  WYChangeStatuTableViewCell.m
//  servicelaiye2.0
//
//  Created by 汪洋 on 15/12/29.
//  Copyright © 2015年 shengdong. All rights reserved.
//

#import "WYChangeStatuTableViewCell.h"
#import "WYChangeGeniusListModel.h"

@interface WYChangeStatuTableViewCell ()

@property (nonatomic, strong) UIImageView *iconImageView;

@property (nonatomic, strong) UILabel *nicknameLb;

@property (nonatomic, strong) UILabel *geniusIdLb;

@property (nonatomic, strong) UILabel *phoneLb;

@property (nonatomic, strong) UILabel *connectNowLb;

@property (nonatomic, strong) UILabel *connectStatuLb;

@end

@implementation WYChangeStatuTableViewCell

-(void)setContentByHistoryModel:(WYChangeGeniusListModel *)changeGeniusListModel{
    
    self.nicknameLb.text = changeGeniusListModel.real_name;
    self.geniusIdLb.text = changeGeniusListModel.username;
    self.phoneLb.text = changeGeniusListModel.phone;
    self.connectNowLb.text = @(changeGeniusListModel.cur_accept_cnt).stringValue;
    if (changeGeniusListModel.accept_limit.intValue == -1) {
        self.connectStatuLb.text = @"关闭";
    } else if (changeGeniusListModel.accept_limit.intValue == 0) {
        self.connectStatuLb.text = @"接单";
    } else if (changeGeniusListModel.accept_limit.intValue > 0) {
        self.connectStatuLb.text = @"派单";
    }
}

-(void)layoutSubviews{
    
    [super layoutSubviews];
    
    self.backgroundColor = [UIColor whiteColor];
    
    [self.iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.left.equalTo(self.contentView).offset(5);
        make.width.equalTo(20);
        make.height.equalTo(20);
    }];
    
    [self.nicknameLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.bottom.equalTo(self.contentView);
        make.left.equalTo(self.iconImageView.right).offset(5);
    }];
    
    [self.geniusIdLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.bottom.equalTo(self.contentView);
        make.left.equalTo(self.nicknameLb.right).offset(5);
        make.width.equalTo(self.nicknameLb.width).offset(30);
    }];
    
    [self.phoneLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.bottom.equalTo(self.contentView);
        make.left.equalTo(self.geniusIdLb.right).offset(5);
        make.width.equalTo(self.nicknameLb.width).offset(30);
    }];
    
    [self.connectNowLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.bottom.equalTo(self.contentView);
        make.width.equalTo(50);
        make.left.equalTo(self.phoneLb.right).offset(5);
    }];
    
    [self.connectStatuLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.contentView);
        make.bottom.equalTo(self.contentView);
        make.left.equalTo(self.connectNowLb.right).offset(5);
        make.right.equalTo(self.contentView).offset(-5);
        make.width.equalTo(50);
    }];
}

- (UIImageView *)iconImageView {
    
    if(_iconImageView == nil) {
        _iconImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"portrait"]];
        [self.contentView addSubview:_iconImageView];
    }
    return _iconImageView;
}

- (UILabel *)nicknameLb {
    
    if(_nicknameLb == nil) {
        _nicknameLb = [[UILabel alloc] init];
        _nicknameLb.font = [UIFont systemFontOfSize:12];
        _nicknameLb.text = @"";
        [self.contentView addSubview:_nicknameLb];
    }
    return _nicknameLb;
}

- (UILabel *)geniusIdLb {
    
    if(_geniusIdLb == nil) {
        _geniusIdLb = [[UILabel alloc] init];
        _geniusIdLb.font = [UIFont systemFontOfSize:12];
        _geniusIdLb.text = @"";
        [self.contentView addSubview:_geniusIdLb];
    }
    return _geniusIdLb;
}

- (UILabel *)phoneLb {
    
    if(_phoneLb == nil) {
        _phoneLb = [[UILabel alloc] init];
        _phoneLb.font = [UIFont systemFontOfSize:12];
        _phoneLb.text = @"";
        [self.contentView addSubview:_phoneLb];
    }
    return _phoneLb;
}

- (UILabel *)connectNowLb {
    
    if(_connectNowLb == nil) {
        _connectNowLb = [[UILabel alloc] init];
        _connectNowLb.font = [UIFont systemFontOfSize:12];
        _connectNowLb.text = @"";
        _connectNowLb.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:_connectNowLb];
    }
    return _connectNowLb;
}

- (UILabel *)connectStatuLb {
    
    if(_connectStatuLb == nil) {
        _connectStatuLb = [[UILabel alloc] init];
        _connectStatuLb.font = [UIFont systemFontOfSize:12];
        _connectStatuLb.text = @"";
        _connectStatuLb.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:_connectStatuLb];
    }
    return _connectStatuLb;
}

@end
