//
//  WYChangeGeniusFooterView.m
//  servicelaiye2.0
//
//  Created by 汪洋 on 15/12/29.
//  Copyright © 2015年 shengdong. All rights reserved.
//

#import "WYChangeGeniusFooterView.h"

@interface WYChangeGeniusFooterView ()<UITextViewDelegate>

@property (nonatomic, strong) UILabel *placehorderLb;

@property (nonatomic, strong) UIButton *sureBtn;

@property (nonatomic, strong) UIButton *cancelBtn;

@end

@implementation WYChangeGeniusFooterView

-(void)layoutSubviews{
    
    [super layoutSubviews];
    
    [self.exegesisTextView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(10);
        make.left.equalTo(self).offset(10);
        make.right.equalTo(self).offset(-10);
        make.height.equalTo(60);
    }];
    
    [self.placehorderLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.exegesisTextView);
        make.left.equalTo(self.exegesisTextView).offset(5);
        make.width.equalTo(80);
        make.height.equalTo(30);
    }];
    
    [self.sureBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.exegesisTextView.bottom).offset(10);
        make.left.equalTo(self).offset(10);
        make.width.equalTo(100);
        make.bottom.equalTo(self).offset(-10);
    }];
    
    [self.cancelBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.exegesisTextView.bottom).offset(10);
        make.left.equalTo(self.sureBtn.right).offset(10);
        make.width.equalTo(100);
        make.bottom.equalTo(self).offset(-10);
    }];
}

- (UITextView *)exegesisTextView {
    
	if(_exegesisTextView == nil) {
		_exegesisTextView = [[UITextView alloc] init];
        _exegesisTextView.font = [UIFont systemFontOfSize:12];
        _exegesisTextView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        _exegesisTextView.delegate = self;
        _exegesisTextView.layer.borderColor = [[UIColor blackColor]CGColor];
        _exegesisTextView.layer.borderWidth = 1.0;
        [self addSubview:_exegesisTextView];
	}
	return _exegesisTextView;
}

- (UILabel *)placehorderLb {
    
    if(_placehorderLb == nil) {
        _placehorderLb = [[UILabel alloc] init];
        _placehorderLb.font = [UIFont systemFontOfSize:12];
        _placehorderLb.textColor = [UIColor lightGrayColor];
        _placehorderLb.text = @"转接附言";
        _placehorderLb.textAlignment = NSTextAlignmentLeft;
        [self.exegesisTextView addSubview:_placehorderLb];
    }
    return _placehorderLb;
}

- (UIButton *)sureBtn {
    
	if(_sureBtn == nil) {
        UIButton *sureBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        sureBtn.backgroundColor = [UIColor blackColor];
        [sureBtn setTitle:@"确定" forState:UIControlStateNormal];
        sureBtn.titleLabel.font = [UIFont systemFontOfSize:12];
        sureBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        [sureBtn addTarget:self action:@selector(sure) forControlEvents:UIControlEventTouchUpInside];
        _sureBtn = sureBtn;
        [self addSubview:_sureBtn];
	}
	return _sureBtn;
}

- (UIButton *)cancelBtn {
    
	if(_cancelBtn == nil) {
        UIButton *cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        cancelBtn.backgroundColor = [UIColor lightGrayColor];
        [cancelBtn setTitle:@"取消" forState:UIControlStateNormal];
        cancelBtn.titleLabel.font = [UIFont systemFontOfSize:12];
        cancelBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        [cancelBtn addTarget:self action:@selector(cancel) forControlEvents:UIControlEventTouchUpInside];
        _cancelBtn = cancelBtn;
        [self addSubview:_cancelBtn];
	}
	return _cancelBtn;
}

-(void)textViewDidChange:(UITextView *)textView {
    
    if (textView.text.length == 0) {
        _placehorderLb.text = @"转接附言";
    }else{
        _placehorderLb.text = @"";
    }
}

#pragma mark --- 按钮点击事件
-(void)sure{
    
    [MobClick event:@"changeGenius_ok_click"];
    if ([self.changeGeniusFooterViewDelegate respondsToSelector:@selector(sureDidClicked:)]) {
        [self.changeGeniusFooterViewDelegate sureDidClicked:self];
    }
}

-(void)cancel{
    
    [MobClick event:@"changeGenius_cancel_click"];
    if ([self.changeGeniusFooterViewDelegate respondsToSelector:@selector(cancelDidClicked:)]) {
        [self.changeGeniusFooterViewDelegate cancelDidClicked:self];
    }
}

#pragma mark --- UITextViewDelegate
- (void)textViewDidBeginEditing:(UITextView *)textView{
    
    [MobClick event:@"changeGenius_notesTextView_click"];
}

@end
