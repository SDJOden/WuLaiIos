//
//  WYChangeGeniusFooterView.h
//  servicelaiye2.0
//
//  Created by 汪洋 on 15/12/29.
//  Copyright © 2015年 shengdong. All rights reserved.
//

#import <UIKit/UIKit.h>

@class WYChangeGeniusFooterView;

@protocol WYChangeGeniusFooterViewDelegate <NSObject, UITextViewDelegate>

@optional

-(void)sureDidClicked:(WYChangeGeniusFooterView *)changeGeniusFooterView;

-(void)cancelDidClicked:(WYChangeGeniusFooterView *)changeGeniusFooterView;

@end

@interface WYChangeGeniusFooterView : UIView

@property (nonatomic, strong) UITextView *exegesisTextView;

@property (nonatomic,weak) id <WYChangeGeniusFooterViewDelegate> changeGeniusFooterViewDelegate;

@end
