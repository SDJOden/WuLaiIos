//
//  WYChangeGeniusHeaderView.m
//  servicelaiye2.0
//
//  Created by 汪洋 on 15/12/29.
//  Copyright © 2015年 shengdong. All rights reserved.
//

#import "WYChangeGeniusHeaderView.h"

@interface WYChangeGeniusHeaderView ()

@property (nonatomic, strong) UILabel *nicknameLb;

@property (nonatomic, strong) UILabel *geniusIdLb;

@property (nonatomic, strong) UILabel *phoneLb;

@property (nonatomic, strong) UILabel *connectNowLb;

@property (nonatomic, strong) UILabel *connectStatuLb;

@end

@implementation WYChangeGeniusHeaderView

-(void)layoutSubviews{
    
    [super layoutSubviews];
    
    self.backgroundColor = [UIColor colorWithWhite:0.848 alpha:1.000];
    
    [self.nicknameLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self);
        make.bottom.equalTo(self);
        make.left.equalTo(self).offset(5);
    }];
    
    [self.geniusIdLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self);
        make.bottom.equalTo(self);
        make.left.equalTo(self.nicknameLb.right).offset(5);
        make.width.equalTo(self.nicknameLb.width);
    }];
    
    [self.phoneLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self);
        make.bottom.equalTo(self);
        make.left.equalTo(self.geniusIdLb.right).offset(5);
        make.width.equalTo(self.nicknameLb.width);
    }];
    
    [self.connectNowLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self);
        make.bottom.equalTo(self);
        make.width.equalTo(50);
        make.left.equalTo(self.phoneLb.right).offset(10);
    }];
    
    [self.connectStatuLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self);
        make.bottom.equalTo(self);
        make.left.equalTo(self.connectNowLb.right).offset(5);
        make.right.equalTo(self).offset(-5);
        make.width.equalTo(50);
    }];
}

- (UILabel *)nicknameLb {
    
	if(_nicknameLb == nil) {
		_nicknameLb = [[UILabel alloc] init];
        _nicknameLb.font = [UIFont systemFontOfSize:12];
        _nicknameLb.text = @"   昵称";
        [self addSubview:_nicknameLb];
	}
	return _nicknameLb;
}

- (UILabel *)geniusIdLb {
    
	if(_geniusIdLb == nil) {
		_geniusIdLb = [[UILabel alloc] init];
        _geniusIdLb.font = [UIFont systemFontOfSize:12];
        _geniusIdLb.text = @"   工号";
        [self addSubview:_geniusIdLb];
	}
	return _geniusIdLb;
}

- (UILabel *)phoneLb {
    
	if(_phoneLb == nil) {
		_phoneLb = [[UILabel alloc] init];
        _phoneLb.font = [UIFont systemFontOfSize:12];
        _phoneLb.text = @"   手机号";
        [self addSubview:_phoneLb];
	}
	return _phoneLb;
}

- (UILabel *)connectNowLb {
    
	if(_connectNowLb == nil) {
		_connectNowLb = [[UILabel alloc] init];
        _connectNowLb.font = [UIFont systemFontOfSize:12];
        _connectNowLb.text = @"当前接入";
        [self addSubview:_connectNowLb];
	}
	return _connectNowLb;
}

- (UILabel *)connectStatuLb {
    
	if(_connectStatuLb == nil) {
		_connectStatuLb = [[UILabel alloc] init];
        _connectStatuLb.font = [UIFont systemFontOfSize:12];
        _connectStatuLb.text = @"设置接入";
        [self addSubview:_connectStatuLb];
	}
	return _connectStatuLb;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"CallbackKeyboard" object:nil];
}

@end
