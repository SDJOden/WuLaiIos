//
//  WYSeparateCollectionViewCell.h
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/3/25.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WYSeparateCollectionViewCell : UICollectionViewCell

- (void)setDataModel:(RCMessageModel *)model with:(BOOL)isMoreSelect;

@end
