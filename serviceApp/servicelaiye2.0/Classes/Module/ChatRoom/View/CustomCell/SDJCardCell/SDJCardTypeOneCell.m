//
//  SDJCardCell.m
//  geniuslaiye2.0
//
//  Created by 盛东 on 15/11/19.
//  Copyright © 2015年 shengdong. All rights reserved.
//

#import "SDJCardTypeOneCell.h"
#import "SDJCardOneCollectionCell.h"
#import "SDJCardEnlargeController.h"

#import "SDJCard_Items.h"
#import "SDJCard_UserInfo.h"

@interface SDJCardTypeOneCell ()<UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic, strong)UICollectionView *collectionView;

@property (nonatomic, strong)NSArray *array;

@property (nonatomic, strong)SDJCard_UserInfo *userInfoModel;

@end

@implementation SDJCardTypeOneCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize {
    
    // 需要做一下清空操作, 否则cell重用的时候会把状态带过去
}

// 懒加载
- (UICollectionView *)collectionView {
    if (!_collectionView) {
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc]init];
        flowLayout.itemSize = CGSizeMake(CARD_WIDTH, CARDONE_HEIGHT);
        flowLayout.minimumLineSpacing = 0;
        flowLayout.minimumInteritemSpacing = 0;
        flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        _collectionView = [[UICollectionView alloc]initWithFrame:self.frame collectionViewLayout:flowLayout];
        _collectionView.backgroundColor = [UIColor whiteColor];
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        _collectionView.pagingEnabled = YES;
        _collectionView.showsHorizontalScrollIndicator = NO;
        _collectionView.bounces = NO;
        _collectionView.layer.cornerRadius = 8; // collectionView需要裁剪成圆角
        _collectionView.layer.masksToBounds = YES;
        [_collectionView registerClass:[SDJCardOneCollectionCell class] forCellWithReuseIdentifier:@"SDJCardTypeOneCell"];
        [self.messageContentView addSubview:_collectionView];
    }
    return _collectionView;
}

- (NSArray *)array {
    if (!_array) {
        _array = [[NSArray alloc] init];
    }
    return _array;
}

- (void)setDataModel:(RCMessageModel *)model with:(BOOL)isMoreSelect{
    
    [super setDataModel:model with:isMoreSelect];
    
    RCRichContentMessage *message = (RCRichContentMessage *)model.content; // 从这个message的extra中解析数据
    
//    NSData *jsonData = [message.extra dataUsingEncoding:NSUTF8StringEncoding];
//    
//    NSError *err;
//    
//    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData
//                         
//                                                        options:NSJSONReadingMutableContainers
//                         
//                                                          error:&err];
    
    NSDictionary *dic = (NSDictionary *)message.extra;
    
    _userInfoModel = [SDJCard_UserInfo modelWithDict:dic[@"user_info"]];
    
    NSMutableArray *arr = [NSMutableArray array];
    NSArray *tempArr = dic[@"msg_data"][@"Items"];
    for (NSDictionary *dict in tempArr) {
        [arr addObject:[SDJCard_Items modelWithDict:dict]];
    }
    self.array = [arr copy];
    
    [self.statusContentView removeFromSuperview];
    self.statusContentView = nil;
//    [self.portraitImageView removeFromSuperview];
    
    [self layout];
}

- (void)layout {
    
    CGRect messageContentViewRect = self.messageContentView.frame;
    messageContentViewRect.size.width = CARD_WIDTH;
    messageContentViewRect.size.height = CARDONE_HEIGHT; // _array.count
    messageContentViewRect.origin.x = 15;
    self.messageContentView.frame = messageContentViewRect;
    
    [self.collectionView makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.bottom.equalTo(self.messageContentView);
    }];
}

#pragma mark - dataSource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.array.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    SDJCardOneCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SDJCardTypeOneCell" forIndexPath:indexPath];
    
    cell.model = self.array[indexPath.item];
    
//    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnCollectionCell:)];
//    [cell setUserInteractionEnabled:YES];
//    
//    [cell addGestureRecognizer:tap];
    
    return cell;
}

// 点击弹出大图展示
//- (void)tapOnCollectionCell:(UIGestureRecognizer *)gestureRecognizer {
//    int currentIndex = (int)[self.collectionView indexPathsForVisibleItems].lastObject.item;
//    
//    if ([self.delegate respondsToSelector:@selector(didTouchCollectionCellAtIndex:WithModel:)]) {
//        
//        [self.delegate didTouchCollectionCellAtIndex:0 WithModel:self.array[currentIndex]];
//        
//    }
//}

@end
