//
//  SDJCardCollectionCell.m
//  geniuslaiye2.0
//
//  Created by 盛东 on 15/11/19.
//  Copyright © 2015年 shengdong. All rights reserved.
//

#import "SDJCardOneCollectionCell.h"

@interface SDJCardOneCollectionCell ()

@property (nonatomic, strong)UILabel *serviceLabel;

@property (nonatomic, strong)UILabel *typeLabel;

@property (nonatomic, strong)UIImageView *backgroundImageView;

@property (nonatomic, strong)UILabel *blankLabel;

@end

@implementation SDJCardOneCollectionCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize {}

// 懒加载
- (UIImageView *)backgroundImageView {
    if (!_backgroundImageView) {
        _backgroundImageView = [[UIImageView alloc] init];
        [self.contentView addSubview:_backgroundImageView];
    }
    return _backgroundImageView;
}

- (UILabel *)serviceLabel {
    if (!_serviceLabel) {
        _serviceLabel = [[UILabel alloc] init];
        [_serviceLabel setTextColor:HEXCOLOR(0xFFFFFF)];
        [_serviceLabel setFont:[UIFont systemFontOfSize:20]];
        [_serviceLabel setTextAlignment:1];
        [self.contentView addSubview:_serviceLabel];
    }
    return _serviceLabel;
}

- (UILabel *)typeLabel {
    if (!_typeLabel) {
        _typeLabel = [[UILabel alloc] init];
        [_typeLabel setTextColor:HEXCOLOR(0xFFFFFF)];
        [_typeLabel setFont:[UIFont systemFontOfSize:12]];
        [_typeLabel setTextAlignment:1];
        [self.contentView addSubview:_typeLabel];
    }
    return _typeLabel;
}

- (void)setModel:(SDJCard_Items *)model {
    
    _model = model;
    
    // 设置控件信息, 设置布局
    [self.backgroundImageView sd_setImageWithURL:[NSURL URLWithString:model.ImageUrl] placeholderImage:nil];
    
    [self.backgroundImageView makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.bottom.equalTo(self.contentView);
    }];
    
    [self.blankLabel makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.contentView);
        make.height.equalTo(60);
    }];
    
    self.serviceLabel.text = model.Name; // 懒加载的时候需要注意层次顺序
    self.typeLabel.text = [model.Desc isEqualToString:@""] ? @"暂无简介" : model.Desc;
    
    [_typeLabel makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.contentView);
        make.bottom.equalTo(self.contentView).offset(-10);
    }];
    
    [_serviceLabel makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.contentView);
        make.bottom.equalTo(_typeLabel.top).offset(-5);
    }];
}

- (UILabel *)blankLabel {
	if(_blankLabel == nil) {
		_blankLabel = [[UILabel alloc] init];
        _blankLabel.backgroundColor = [UIColor colorWithRed:0.5 green:0.5 blue:0.5 alpha:0.5];
        [self.contentView addSubview:_blankLabel];
	}
	return _blankLabel;
}

@end
