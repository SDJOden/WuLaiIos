//
//  SDJCardTypeTwoCell.m
//  geniuslaiye2.0
//
//  Created by 盛东 on 15/11/30.
//  Copyright © 2015年 shengdong. All rights reserved.
//

#import "SDJCardTypeTwoCell.h"
#import "SDJCardTwoCollectionCell.h"
#import "SDJCardEnlargeController.h"
#import "SDJCard_UserInfo.h"
#import "SDJCard_Items.h"
#import "RCEmojiPageControl.h"


@interface SDJCardTypeTwoCell ()<UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic, strong)NSArray *array;

@property (nonatomic, strong)UICollectionView *collectionView;

@property (nonatomic, strong)SDJCard_UserInfo *userInfoModel;

@property (nonatomic, strong)RCEmojiPageControl *pageControl;

@end

@implementation SDJCardTypeTwoCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
    }
    return self;
}

// 控件懒加载
- (NSArray *)array {
    if (!_array) {
        _array = [[NSArray alloc] init];
    }
    return _array;
}

- (UICollectionView *)collectionView {
    
    if (!_collectionView) {
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc]init];
        flowLayout.itemSize = CGSizeMake(CARD_WIDTH, CARDTWO_HEIGHT);
        flowLayout.minimumLineSpacing = 0;
        flowLayout.minimumInteritemSpacing = 0;
        flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        _collectionView = [[UICollectionView alloc]initWithFrame:self.frame collectionViewLayout:flowLayout];
        _collectionView.dataSource = self;
        _collectionView.backgroundColor = [UIColor whiteColor];
        _collectionView.delegate = self;
        _collectionView.pagingEnabled = YES;
        _collectionView.showsHorizontalScrollIndicator = NO;
        _collectionView.bounces = NO;
        _collectionView.layer.cornerRadius = 8; // 需要裁剪成圆角
        _collectionView.layer.masksToBounds = YES;
        [_collectionView registerClass:[SDJCardTwoCollectionCell class] forCellWithReuseIdentifier:@"SDJCardTwoCell"];
        [self.messageContentView addSubview:_collectionView];
    }
    return _collectionView;
}

- (void)setDataModel:(RCMessageModel *)model with:(BOOL)isMoreSelect {
    
    // 设置按钮的点击状态
    
    [super setDataModel:model with:isMoreSelect];
    
    RCRichContentMessage *message = (RCRichContentMessage *)model.content; // 从这个message的extra中解析数据
//    NSData *jsonData = [message.extra dataUsingEncoding:NSUTF8StringEncoding];
//    NSError *err;
//    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData
//                                                        options:NSJSONReadingMutableContainers
//                                                          error:&err];
    NSDictionary *dic = (NSDictionary *)message.extra;
    
    _userInfoModel = [SDJCard_UserInfo modelWithDict:dic[@"user_info"]];
    
    NSMutableArray *arr = [NSMutableArray array];
    NSArray *tempArr = dic[@"msg_data"][@"Items"];
    for (NSDictionary *dict in tempArr) {
        [arr addObject:[SDJCard_Items modelWithDict:dict]];
    }
    self.array = [arr copy];
    
    [self.statusContentView removeFromSuperview];
    self.statusContentView = nil;
//    [self.portraitImageView removeFromSuperview];
    
    CGRect messageContentViewRect = self.messageContentView.frame;
    messageContentViewRect.size.width = CARD_WIDTH;
    messageContentViewRect.size.height = CARDTWO_HEIGHT;
    messageContentViewRect.origin.x = 15;
    self.messageContentView.frame = messageContentViewRect;

    self.messageContentView.backgroundColor = [UIColor whiteColor];
    self.messageContentView.layer.cornerRadius = 8; // 需要裁剪成圆角
    self.messageContentView.layer.masksToBounds = YES;
    
//    SDJCard_Items *tempModel = (SDJCard_Items *)_array[0];
    
    [self.collectionView makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.bottom.equalTo(self.messageContentView);
    }];
    
    [self.pageControl makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.messageContentView);
        make.height.equalTo(30);
    }];

}

#pragma mark - dataSource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.array.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    SDJCardTwoCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SDJCardTwoCell" forIndexPath:indexPath];
    
    cell.model = self.array[indexPath.item];
    
//    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnCollectionCell:)];
//    [cell setUserInteractionEnabled:YES];
//    
//    [cell addGestureRecognizer:tap];
    
    return cell;
}

// 点击弹出大图展示
//- (void)tapOnCollectionCell:(SDJCardTwoCollectionCell *)cell {
//    
//    int currentIndex = (int)[self.collectionView indexPathsForVisibleItems].lastObject.item;
//    
//    if ([self.delegate respondsToSelector:@selector(didTouchCollectionCellAtIndex:WithModel:)]) {
//        
//        [self.delegate didTouchCollectionCellAtIndex:0 WithModel:self.array[currentIndex]];
//        
//    }
//}

- (RCEmojiPageControl *)pageControl {
    if (!_pageControl) {
        _pageControl = [[RCEmojiPageControl alloc] initWithFrame:CGRectMake(0, CARDTWO_HEIGHT - 11 + 18, self.frame.size.width, 5)];
        _pageControl.numberOfPages = self.array.count;
        _pageControl.currentPage = 0;
        _pageControl.enabled = NO;
        [self.messageContentView addSubview:_pageControl];
    }
    return _pageControl;
}

//停止滚动的时候
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    
    //更新UIPageControl的当前页
    CGPoint offset = scrollView.contentOffset;
    CGRect bounds = scrollView.frame;
    [_pageControl setCurrentPage:offset.x / bounds.size.width];
}

@end

