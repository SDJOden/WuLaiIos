//
//  SDJCardThreeTableCell.m
//  geniuslaiye2.0
//
//  Created by 盛东 on 15/12/1.
//  Copyright © 2015年 shengdong. All rights reserved.
//

#import "SDJCardThreeTableCell.h"

@interface SDJCardThreeTableCell ()

@property (nonatomic, strong)UIImageView *iconView;

@property (nonatomic, strong)UILabel *titleLabel;

@property (nonatomic, strong)UILabel *serviceLabel;

@property (nonatomic, strong)UILabel *priceLabel;

@property (nonatomic, strong)UIButton *chooseButton;

@property (nonatomic, strong)UIView *bottomView;

@end

@implementation SDJCardThreeTableCell

+ (instancetype)returnCellWithTableView:(UITableView *)tableView andWithCellStyle:(NSString *)cellStyle
{
    SDJCardThreeTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SDJCardThreeTableCell"];
    if (cell == nil) {
        cell = [[SDJCardThreeTableCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SDJCardThreeTableCell"];
    }
    return cell;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {}
    
    return self;
}

// 懒加载
- (UIImageView *)iconView {
    if (!_iconView) {
        _iconView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 45, 45)];
        _iconView.layer.cornerRadius = 0;
        _iconView.layer.masksToBounds = YES;
        [self.contentView addSubview:_iconView];
    }
    return _iconView;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] init];
        [_titleLabel setFont:[UIFont systemFontOfSize:16]];
        _titleLabel.numberOfLines = 0;
        [_serviceLabel setTextColor:[UIColor blackColor]];
        [self.contentView addSubview:_titleLabel];
    }
    return _titleLabel;
}

- (UILabel *)serviceLabel {
    if (!_serviceLabel) {
        _serviceLabel = [[UILabel alloc] init];
        [_serviceLabel setFont:[UIFont systemFontOfSize:12]];
        [_serviceLabel setTextColor:[UIColor grayColor]];
        [self.contentView addSubview:_serviceLabel];
    }
    return _serviceLabel;
}

- (UILabel *)priceLabel {
    if (!_priceLabel) {
        _priceLabel = [[UILabel alloc] init];
        [_priceLabel setFont:[UIFont systemFontOfSize:13]];
        [_priceLabel setTextAlignment:1];
        [_priceLabel setTextColor:[UIColor blackColor]];
        [self.contentView addSubview:_priceLabel];
    }
    return _priceLabel;
}

- (UIButton *)chooseButton {
    if (!_chooseButton) {
        _chooseButton = [[UIButton alloc] initWithFrame:CGRectMake(20, 20, 60, 40)];
        [_chooseButton setBackgroundImage:[UIImage getImageWithSize:CGSizeMake(60, 40) backgroundColor:[UIColor colorWithRed:254/255.0 green:227/255.0 blue:12/255.0 alpha:1.0]] forState:UIControlStateNormal];
        [_chooseButton setBackgroundImage:[UIImage getImageWithSize:CGSizeMake(60, 40) backgroundColor:[UIColor grayColor]] forState:UIControlStateDisabled];
        [_chooseButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_chooseButton setTitle:@"来一份" forState:UIControlStateNormal];
        [_chooseButton setTitle:@"已选择" forState:UIControlStateDisabled];
        [_chooseButton.titleLabel setFont:[UIFont systemFontOfSize:12]];
        _chooseButton.layer.cornerRadius = 2; // 需要裁剪成圆角
        _chooseButton.layer.masksToBounds = YES;
        [_chooseButton addTarget:self action:@selector(superButtonClicked) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_chooseButton];
    }
    return _chooseButton;
}

- (UIView *)bottomView {
    if (!_bottomView) {
        _bottomView = [[UIView alloc] init];
        _bottomView.backgroundColor = HEXCOLOR(0xd4d4d4);
        [self.contentView addSubview:_bottomView];
    }
    return _bottomView;
}

- (void)superButtonClicked {
    
    _chooseButton.enabled = NO;
}

- (void)setModel:(RCMessageModel *)model {
    
    _model = model;
    
    self.chooseButton.enabled = YES;
    
    // 控件赋值
    self.iconView.image = [UIImage imageNamed:@"hongbao"];
    
    self.titleLabel.text = @"大杯(热)蔓越莓白巧克力风味拿铁";
    self.serviceLabel.text = @"连咖啡-星巴克代购";
    self.priceLabel.text = [NSString stringWithFormat:@"￥%d", 228];
    
    [self layout];
}

- (void)layout {
    // 控件布局
    [self.iconView makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(5);
        make.left.equalTo(self.contentView).offset(5);
        make.width.height.equalTo(55);
    }];
    
    [self.serviceLabel makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(_titleLabel.left);
        make.top.equalTo(_titleLabel.bottom).offset(3);
    }];
    
    [self.priceLabel makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.contentView.right).offset(- 65 * 0.5);
        make.top.equalTo(self.contentView).offset(10);
    }];
    
    [self.chooseButton makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(_priceLabel.centerX);
        make.width.equalTo(49);
        make.height.equalTo(24);
        make.top.equalTo(_priceLabel.bottom).offset(5);
    }];
    
    [self.titleLabel makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_iconView.top);
        make.left.equalTo(_iconView.right).offset(5);
        make.right.equalTo(_chooseButton.left).offset(-20);
    }];
    
    [self.bottomView makeConstraints:^(MASConstraintMaker *make) {
        make.width.bottom.equalTo(self.contentView);
        make.height.equalTo(1);
    }];
}

@end
