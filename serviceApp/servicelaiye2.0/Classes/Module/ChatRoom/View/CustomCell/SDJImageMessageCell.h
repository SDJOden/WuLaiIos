//
//  SDJImageMessageCell.h
//  geniuslaiye2.0
//
//  Created by 盛东 on 15/12/2.
//  Copyright © 2015年 shengdong. All rights reserved.
//

#import "RCMessageCell.h"
#import "RCImageMessageProgressView.h"

@interface SDJImageMessageCell : RCMessageCell

@property(nonatomic, strong) UIImageView *pictureView; // 图片视图

@property(nonatomic, strong) RCImageMessageProgressView *progressView; // 进度视图

@end
