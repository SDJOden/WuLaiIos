//
//  RCLocationMessageCell.h
//  RongIMKit
//
//  Created by xugang on 15/2/2.
//  Copyright (c) 2015年 RongCloud. All rights reserved.
//

#import "RCMessageCell.h"
#import "RCImageMessageProgressView.h"

@interface RCLocationMessageCell : RCMessageCell

@property(nonatomic, strong) UIImageView *pictureView; // 外部显示的缩略图

@property(nonatomic, strong) UILabel *locationNameLabel; // 缩略图下面的label

@end
