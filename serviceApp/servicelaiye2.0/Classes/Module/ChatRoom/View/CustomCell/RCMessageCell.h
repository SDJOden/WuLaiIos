//
//  RCMessageCommonCell.h
//  RongIMKit
//
//  Created by xugang on 15/1/28.
//  Copyright (c) 2015年 RongCloud. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RCMessageModel.h"
#import "RCMessageCellNotificationModel.h"
#import "RCMessageCellDelegate.h"
#import "RCContentView.h"

#define CARD_WIDTH [UIScreen mainScreen].bounds.size.width - 30
#define CARDONE_HEIGHT ([UIScreen mainScreen].bounds.size.width - 30) * 0.5
#define CARDTWO_HEIGHT ([UIScreen mainScreen].bounds.size.width - 30 - 20) * 0.5 + 124 - 59
#define CARDTHREE_HEIGHT 65
#define CARDFOUR_HEIGHT 83
#define CARDFIVE_HEIGHT 157

// 消息发送状态通知回调Key
UIKIT_EXTERN NSString *const KNotificationMessageBaseCellUpdateSendingStatus;
#define TIME_LABEL_HEIGHT 20

// cell内部是可以可以用自动布局的
@class RCCollectionCellAttributes;
@class RCTipLabel;
@class RCMessageCell;

@protocol WYMessageCellDelegate <NSObject>

-(void)cellDidClick:(RCMessageCell *)cell;

-(void)cellCancel:(RCMessageCell *)cell;

@end



// MessageBaseCell子类，用于创建基本的Cell内容
@interface RCMessageCell : UICollectionViewCell

// 显示时间的Label
//@property(strong, nonatomic) RCTipLabel *messageTimeLabel;
@property (nonatomic, strong) UILabel *messageTimeLb;

// 消息数据模型
@property(strong, nonatomic) RCMessageModel *model;

// 消息父视图区域
@property(strong, nonatomic) UIView *baseContentView;

@property (nonatomic, strong) UIView *msgInfoView;

// 消息方向
@property(nonatomic) RCMessageDirection messageDirection;

// 是否显示消息时间
@property(nonatomic, readonly) BOOL isDisplayMessageTime;

// 类初始化方法
- (instancetype)initWithFrame:(CGRect)frame;

// 消息发送状态通知
- (void)messageCellUpdateSendingStatusEvent:(NSNotification *)notification;

@property(nonatomic, weak) id<RCMessageCellDelegate> delegate;

//@property(nonatomic, strong) UIImageView *portraitImageView; // 用户头像

@property(nonatomic, strong) UILabel *nicknameLabel; // 用户昵称

@property(nonatomic, strong) RCContentView *messageContentView; // 消息内容视图

@property(nonatomic, strong) UIView *statusContentView; // 消息状态视图

@property(nonatomic, strong) UIButton *messageFailedStatusView; // 消息发送失败状态视图

@property(nonatomic, strong) UIActivityIndicatorView *messageActivityIndicatorView; // 消息发送指示视图

@property(nonatomic, readonly) CGFloat messageContentViewWidth; // 消息内容视图宽度

//@property(nonatomic, assign, setter=setPortraitStyle:) RCUserAvatarStyle portraitStyle; // 用户头像样式

@property(nonatomic, readonly) BOOL isDisplayNickname; // 是否显示用户昵称

//@property (nonatomic, strong) UIButton *loveBtn;
//
//@property (nonatomic, strong) UIButton *wrongBtn;

@property (nonatomic, strong) UIImageView *selectIconImageView;

@property (nonatomic, strong) UIButton *clickBtn;

@property (nonatomic, weak) id <WYMessageCellDelegate> messageCellDelegate;

- (void)setDataModel:(RCMessageModel *)model with:(BOOL)isMoreSelect; // 设置数据模型

- (void)updateStatusContentView:(RCMessageModel *)model; // 更新消息发送状态视图

- (void)cellSubViewsMoveToRight;

- (void)cellSubViewsBack;

@end
