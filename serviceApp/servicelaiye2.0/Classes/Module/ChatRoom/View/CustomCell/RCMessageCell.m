//
//  RCMessageCommonCell.m
//  RongIMKit
//
//  Created by xugang on 15/1/28.
//  Copyright (c) 2015年 RongCloud. All rights reserved.
//

#import "RCMessageCell.h"
#import "RCTipLabel.h"

#define AI_URL (CODESTATU?@"http://101.200.177.89:8851/sofa.pbrpc.test.DLServer.Dialogue":@"http://101.200.177.89:8853/sofa.pbrpc.test.DLServer.Dialogue")

#define WRONG_URL (CODESTATU?@"http://101.200.177.89:8851/static/p.gif":@"http://101.200.177.89:8853/static/p.gif")

#define LOG_TOKEN @"05uNkCKgzPkmFyACUj7Sbnt4xHS2lD6N"

NSString *const KNotificationMessageBaseCellUpdateSendingStatus = @"KNotificationMessageBaseCellUpdateSendingStatus";

#define COLOR_F0DEDA [UIColor colorWithRed:236/255.0 green:233/255.0 blue:229/255.0 alpha:1.0]

@interface RCMessageCell ()

@property (nonatomic, assign) BOOL isMoreSelect;

@end

@implementation RCMessageCell

- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(messageCellUpdateSendingStatusEvent:)
                                                     name:KNotificationMessageBaseCellUpdateSendingStatus
                                                   object:nil];
        self.model = nil;
        _isDisplayNickname = NO;
        self.delegate = nil;
        
        self.selectIconImageView.frame = CGRectMake(5, 10, 25, 25);
        self.baseContentView.backgroundColor = HEXCOLOR(0xf6f6f6);
        [self bringSubviewToFront:self.baseContentView];
        
        __weak typeof(&*self) __blockself = self;
        [self.messageContentView registerFrameChangedEvent:^(CGRect frame) {
          if (__blockself.model) {
              if (__blockself.model.messageDirection == MessageDirection_SEND) {
                  __blockself.statusContentView.frame = CGRectMake(
                      frame.origin.x - 10 - 25, frame.origin.y + (frame.size.height - 25) / 2.0f, 25, 25);
              } else {
                  __blockself.statusContentView.frame = CGRectZero;
              }
          }
        }];

        self.messageFailedStatusView = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
        [_messageFailedStatusView
            setImage:[RCKitUtility imageNamed:@"message_send_fail_status" ofBundle:@"RongCloud.bundle"]
            forState:UIControlStateNormal];
        [self.statusContentView addSubview:_messageFailedStatusView];
        _messageFailedStatusView.hidden = YES;
        [_messageFailedStatusView addTarget:self
                                 action:@selector(didclickMsgFailedView:)
                       forControlEvents:UIControlEventTouchUpInside];
    }
    return self;
}

- (void)setDataModel:(RCMessageModel *)model with:(BOOL)isMoreSelect{
    
    self.model = model;
    self.messageDirection = model.messageDirection;
    _isDisplayMessageTime = model.isDisplayMessageTime;
    self.messageTimeLb.text = [RCKitUtility ConvertChatMessageTime:model.sentTime / 1000];
    
    _isDisplayNickname = model.isDisplayNickname;
    NSDictionary *nickDict = [NSDictionary dictionary];
    
    if ([model.content isKindOfClass:[RCTextMessage class]]) {
        if ([((RCTextMessage*)model.content).extra isKindOfClass:[NSDictionary class]]) {
            nickDict = (NSDictionary *)((RCTextMessage*)model.content).extra;
        }
        else {
            nickDict = [WYTool jsonStringToDictionary:((RCTextMessage*)model.content).extra];
            ((RCTextMessage*)model.content).extra = (NSString *)nickDict;
        }
    }
    else if ([model.content isKindOfClass:[RCImageMessage class]]) {
        if ([((RCImageMessage*)model.content).extra isKindOfClass:[NSDictionary class]]) {
            nickDict = (NSDictionary *)((RCImageMessage*)model.content).extra;
        }
        else {
            nickDict = [WYTool jsonStringToDictionary:((RCImageMessage*)model.content).extra];
            ((RCImageMessage*)model.content).extra = (NSString *)nickDict;
        }
    }
    else if ([model.content isKindOfClass:[RCRichContentMessage class]]) {
        if ([((RCRichContentMessage*)model.content).extra isKindOfClass:[NSDictionary class]]) {
            nickDict = (NSDictionary *)((RCRichContentMessage*)model.content).extra;
        }
        else {
            nickDict = [WYTool jsonStringToDictionary:((RCRichContentMessage *)model.content).extra];
            ((RCRichContentMessage *)model.content).extra = (NSString *)nickDict;
        }
    }else if ([model.content isKindOfClass:[RCVoiceMessage class]]) {
        if ([((RCVoiceMessage*)model.content).extra isKindOfClass:[NSDictionary class]]) {
            nickDict = (NSDictionary *)((RCVoiceMessage*)model.content).extra;
        }
        else {
            nickDict = [WYTool jsonStringToDictionary:((RCVoiceMessage *)model.content).extra];
            ((RCVoiceMessage *)model.content).extra = (NSString *)nickDict;
        }
    }
    
    self.nicknameLabel.hidden = NO;
    if (model.messageDirection == MessageDirection_SEND) {
        if (((NSString *)nickDict[@"genius_info"][@"username"]).length >= 9) {
            self.nicknameLabel.text = [NSString stringWithFormat:@"%@ %@", [nickDict[@"genius_info"][@"username"]substringToIndex:9], nickDict[@"genius_info"][@"realname"]];
        }else {
            self.nicknameLabel.text = [NSString stringWithFormat:@"%@ %@", nickDict[@"genius_info"][@"username"], nickDict[@"genius_info"][@"realname"]];
            if (((NSNumber *)nickDict[@"msg_type"]).integerValue == 3) {
                self.nicknameLabel.hidden = YES;
            }
        }
    }else {
        if (((NSNumber *)nickDict[@"user_src"]).integerValue == 1) {
            self.nicknameLabel.text = @"微信"/*[NSString stringWithFormat:@"#%@%@", ((NSNumber *)nickDict[@"inner_uid"]).stringValue, @"(微信)"]*/;
        }
        else  {
            self.nicknameLabel.text = @"App"/*[NSString stringWithFormat:@"#%@%@", ((NSNumber *)nickDict[@"inner_uid"]).stringValue, @"(App)"]*/;
        }
    }
    
    [self setCellAutoLayoutWith:isMoreSelect];
}

- (void)setCellAutoLayoutWith:(BOOL)isMoreSelect{
    //计算timelabel布局
    CGSize timeTextSize_ = CGSizeZero;
    
    if (IOS_FSystenVersion < 7.0) {
        timeTextSize_ = RC_MULTILINE_TEXTSIZE_LIOS7(self.messageTimeLb.text, [UIFont systemFontOfSize:12], CGSizeMake(self.bounds.size.width, TIME_LABEL_HEIGHT), NSLineBreakByTruncatingTail);
    }else {
        timeTextSize_ = RC_MULTILINE_TEXTSIZE_GEIOS7(self.messageTimeLb.text, [UIFont systemFontOfSize:12], CGSizeMake(self.bounds.size.width, TIME_LABEL_HEIGHT));
    }
    
    timeTextSize_ = CGSizeMake(ceilf(timeTextSize_.width), ceilf(timeTextSize_.height));
    
    self.messageTimeLb.hidden = NO;
    
    if (isMoreSelect == NO) {
        [self.baseContentView setFrame:CGRectMake(0, 0/*10 + TIME_LABEL_HEIGHT + 10*/, self.bounds.size.width, self.bounds.size.height/* - (10 + TIME_LABEL_HEIGHT)*/)];
    }else{
        [self.baseContentView setFrame:CGRectMake(40, 0/*10 + TIME_LABEL_HEIGHT + 10*/, self.bounds.size.width - 40, self.bounds.size.height/* - (10 + TIME_LABEL_HEIGHT)*/)];
    }
    
//    if (self.model.selected) {
//        [self.selectIconImageView setImage:[UIImage imageNamed:@"card_007"]];
//    } else {
//        [self.selectIconImageView setImage:[UIImage imageNamed:@"circle"]];
//    }
    
    
    // 设置头像布局
    _messageContentViewWidth = self.baseContentView.bounds.size.width /*- ([RCIM sharedRCIM].globalMessagePortraitSize.width + 10 + 5)*/+ 10;
    [self.nicknameLabel sizeToFit];
    
    if (isMoreSelect) {
        self.clickBtn.frame = self.contentView.bounds;
    }
    else {
        [self.clickBtn removeFromSuperview];
        self.clickBtn = nil;
    }
    
    if (MessageDirection_RECEIVE == self.messageDirection) {
        self.messageContentView.frame = CGRectMake(10, 20+5, _messageContentViewWidth, self.baseContentView.bounds.size.height - (10 + TIME_LABEL_HEIGHT));
        
        [self.messageTimeLb setFrame:CGRectMake(10, 0, timeTextSize_.width, TIME_LABEL_HEIGHT)];
        self.nicknameLabel.textAlignment = NSTextAlignmentLeft;
        self.nicknameLabel.frame = CGRectMake(self.messageTimeLb.w + 10 + 10, 0, self.nicknameLabel.w, 20);
//        [self.loveBtn removeFromSuperview];
//        self.loveBtn = nil;
//        [self.wrongBtn removeFromSuperview];
//        self.wrongBtn = nil;
//        if ([self.model.content isKindOfClass:[RCTextMessage class]]) {
//            if (((NSArray *)((NSDictionary*)((RCTextMessage *)self.model.content).extra)[@"msg_detail"][@"sug_domain"]).count > 0) {
//                self.wrongBtn.frame = CGRectMake(self.nicknameLabel.x + self.nicknameLabel.w + 10, 0, self.wrongBtn.w, 20);
//            }
//        }
    } else { // owner
        self.messageContentView.frame = CGRectMake(self.baseContentView.bounds.size.width - (_messageContentViewWidth + 10), 20+5, _messageContentViewWidth, self.baseContentView.bounds.size.height - (10 + TIME_LABEL_HEIGHT));
        self.nicknameLabel.textAlignment = NSTextAlignmentRight;
//        self.loveBtn.frame = CGRectMake(self.baseContentView.w - 10 - self.loveBtn.w, 0, self.loveBtn.w, 20);
//        self.loveBtn.enabled = !self.model.loveBtnEnable;
        RCTextMessage *textMsg = (RCTextMessage *)self.model.content;
        if ([(NSDictionary *)textMsg.extra isKindOfClass:[NSDictionary class]]) {
            NSDictionary *dict = (NSDictionary *)textMsg.extra;
            if (((NSString *)dict[@"genius_info"][@"username"]).length >= 9) {
//                [self.wrongBtn removeFromSuperview];
//                self.wrongBtn = nil;
                self.nicknameLabel.frame = CGRectMake(self.baseContentView.w - 10 - self.nicknameLabel.w, 0, self.nicknameLabel.w, 20);
            }else {
//                self.wrongBtn.frame = CGRectMake(self.loveBtn.x - 10 - self.wrongBtn.w, 0, self.wrongBtn.w, 20);
                self.nicknameLabel.frame = CGRectMake(self.baseContentView.w - 10 - self.nicknameLabel.w, 0, self.nicknameLabel.w, 20);
            }
        }
        [self.messageTimeLb setFrame:CGRectMake(self.nicknameLabel.x - timeTextSize_.width - 5, 0, timeTextSize_.width, TIME_LABEL_HEIGHT)];
    }
    self.msgInfoView.frame = CGRectMake(0, 0/*self.messageContentView.y + self.messageContentView.h + 5*/, self.baseContentView.w, 20);
    [self updateStatusContentView:self.model];
}

- (void)updateStatusContentView:(RCMessageModel *)model {
    
    if (model.messageDirection == MessageDirection_RECEIVE) {
        self.statusContentView.hidden = YES;
        return;
    } else {
        self.statusContentView.hidden = NO;
    }
    __weak typeof(&*self) __blockSelf = self;

    dispatch_async(dispatch_get_main_queue(), ^{

      if (__blockSelf.model.sentStatus == SentStatus_SENDING) {
          __blockSelf.messageFailedStatusView.hidden = YES;
          if (__blockSelf.messageActivityIndicatorView) {
              __blockSelf.messageActivityIndicatorView.hidden = NO;
              if (__blockSelf.messageActivityIndicatorView.isAnimating == NO) {
                  [__blockSelf.messageActivityIndicatorView startAnimating];
              }
          }

      } else if (__blockSelf.model.sentStatus == SentStatus_FAILED) {
          __blockSelf.messageFailedStatusView.hidden = NO;
          if (__blockSelf.messageActivityIndicatorView) {
              __blockSelf.messageActivityIndicatorView.hidden = YES;
              if (__blockSelf.messageActivityIndicatorView.isAnimating == YES) {
                  [__blockSelf.messageActivityIndicatorView stopAnimating];
              }
          }
      } else if (__blockSelf.model.sentStatus == SentStatus_SENT) {
          __blockSelf.messageFailedStatusView.hidden = YES;
          if (__blockSelf.messageActivityIndicatorView) {
              __blockSelf.messageActivityIndicatorView.hidden = YES;
              if (__blockSelf.messageActivityIndicatorView.isAnimating == YES) {
                  [__blockSelf.messageActivityIndicatorView stopAnimating];
              }
          }
      }
    });
}

#pragma mark private
- (void)tapUserPortaitEvent:(UIGestureRecognizer *)gestureRecognizer {
    __weak typeof(&*self) weakSelf = self;
    if ([self.delegate respondsToSelector:@selector(didTapCellPortrait:)]) {
        [self.delegate didTapCellPortrait:weakSelf.model.senderUserId];
    }
}

- (void)longPressUserPortaitEvent:(UIGestureRecognizer *)gestureRecognizer {
    __weak typeof(&*self) weakSelf = self;
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        if ([self.delegate respondsToSelector:@selector(didLongPressCellPortrait:)]) {
            [self.delegate didLongPressCellPortrait:weakSelf.model.senderUserId];
        }
    }
}

- (void)imageMessageSendProgressing:(NSInteger)progress {}

- (void)messageCellUpdateSendingStatusEvent:(NSNotification *)notification {

    RCMessageCellNotificationModel *notifyModel = notification.object;

    if (self.model.messageId == notifyModel.messageId) {
        if ([notifyModel.actionName isEqualToString:CONVERSATION_CELL_STATUS_SEND_BEGIN]) {
            self.model.sentStatus = SentStatus_SENDING;
            [self updateStatusContentView:self.model];

        } else if ([notifyModel.actionName isEqualToString:CONVERSATION_CELL_STATUS_SEND_FAILED]) {
            self.model.sentStatus = SentStatus_FAILED;
            [self updateStatusContentView:self.model];
        } else if ([notifyModel.actionName isEqualToString:CONVERSATION_CELL_STATUS_SEND_SUCCESS]) {
            self.model.sentStatus = SentStatus_SENT;
            [self updateStatusContentView:self.model];
        } else if ([notifyModel.actionName isEqualToString:CONVERSATION_CELL_STATUS_SEND_PROGRESS]) {
            [self imageMessageSendProgressing:notifyModel.progress];
        }
    }
}

- (void)didclickMsgFailedView:(UIButton *)button {
    if (self.delegate) {
        if ([self.delegate respondsToSelector:@selector(didTapmessageFailedStatusViewForResend:)]) {
            [self.delegate didTapmessageFailedStatusViewForResend:self.model];
        }
    }
}

#pragma mark --- 懒加载
- (UIView *)baseContentView {
    if(_baseContentView == nil) {
        _baseContentView = [[UIView alloc] init];
        [self.contentView addSubview:_baseContentView];
    }
    return _baseContentView;
}

- (RCContentView *)messageContentView {
    if(_messageContentView == nil) {
        _messageContentView = [[RCContentView alloc] init];
        [self.baseContentView addSubview:self.messageContentView];
    }
    return _messageContentView;
}

- (UIView *)statusContentView {
    if(_statusContentView == nil) {
        _statusContentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
        _statusContentView.backgroundColor = [UIColor clearColor];
        [self.baseContentView addSubview:_statusContentView];
    }
    return _statusContentView;
}

- (UIView *)msgInfoView {
    if(_msgInfoView == nil) {
        _msgInfoView = [[UIView alloc] init];
        [self.baseContentView addSubview:_msgInfoView];
    }
    return _msgInfoView;
}

- (UILabel *)nicknameLabel {
    if(_nicknameLabel == nil) {
        _nicknameLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        _nicknameLabel.backgroundColor = [UIColor clearColor];
        [_nicknameLabel setFont:[UIFont systemFontOfSize:12]];
        [_nicknameLabel setTextColor:[UIColor grayColor]];
        [self.msgInfoView addSubview:_nicknameLabel];
    }
    return _nicknameLabel;
}

- (UILabel *)messageTimeLb {
    if(_messageTimeLb == nil) {
        _messageTimeLb = [[UILabel alloc]init];
        [_messageTimeLb setFont:[UIFont systemFontOfSize:12]];
        [_messageTimeLb setTextColor:[UIColor grayColor]];
        [_messageTimeLb sizeToFit];
        [self.msgInfoView addSubview:_messageTimeLb];
    }
    return _messageTimeLb;
}

- (UIImageView *)selectIconImageView {
    if(_selectIconImageView == nil) {
        _selectIconImageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"circle"]];
        [self.contentView addSubview:_selectIconImageView];
    }
    return _selectIconImageView;
}


- (void)cellSubViewsMoveToRight{
    __weak typeof(self)__weakSelf = self;
    //动画弹出选择框
    [UIView animateWithDuration:0.3 animations:^{
        CGRect rect = __weakSelf.baseContentView.frame;
        rect.origin.x += 40;
        CGSize size = rect.size;
        size.width -= 40;
        rect.size = size;
        __weakSelf.baseContentView.frame = rect;
        if (__weakSelf.model.messageDirection == MessageDirection_SEND) {
            CGRect msgInfoRect = __weakSelf.msgInfoView.frame;
            msgInfoRect.origin.x -= 40;
            __weakSelf.msgInfoView.frame = msgInfoRect;
            if (([__weakSelf.model.content isKindOfClass:[RCTextMessage class]] || [__weakSelf.model.content isKindOfClass:[RCImageMessage class]])) {
                CGRect messageContentViewRect = __weakSelf.messageContentView.frame;
                messageContentViewRect.origin.x -= 40;
                __weakSelf.messageContentView.frame = messageContentViewRect;
            }
        }
    } completion:^(BOOL finished) {
        [__weakSelf.clickBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.right.bottom.equalTo(__weakSelf.contentView);
        }];
    }];
}

- (void)cellSubViewsBack{
    __weak typeof(self)__weakSelf = self;
    
    [UIView animateWithDuration:0.3 animations:^{
        CGRect rect = __weakSelf.baseContentView.frame;
        rect.origin.x -= 40;
        CGSize size = rect.size;
        size.width += 40;
        rect.size = size;
        __weakSelf.baseContentView.frame = rect;
        if (__weakSelf.model.messageDirection == MessageDirection_SEND) {
            CGRect msgInfoRect = __weakSelf.msgInfoView.frame;
            msgInfoRect.origin.x += 40;
            __weakSelf.msgInfoView.frame = msgInfoRect;
            if (([__weakSelf.model.content isKindOfClass:[RCTextMessage class]] || [__weakSelf.model.content isKindOfClass:[RCImageMessage class]])) {
                CGRect messageContentViewRect = __weakSelf.messageContentView.frame;
                messageContentViewRect.origin.x += 40;
                __weakSelf.messageContentView.frame = messageContentViewRect;
            }
        }
    } completion:^(BOOL finished) {
        [__weakSelf.selectIconImageView setImage:[UIImage imageNamed:@"circle"]];
        [__weakSelf.clickBtn removeFromSuperview];
        __weakSelf.clickBtn = nil;
    }];
}

- (UIButton *)clickBtn {
    if(_clickBtn == nil) {
        _clickBtn = [[UIButton alloc] init];
        _clickBtn.backgroundColor = [UIColor clearColor];
        [_clickBtn addTarget:self action:@selector(selectCell) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_clickBtn];
    }
    return _clickBtn;
}

-(void)selectCell{
    if ([self.selectIconImageView.image isEqual:[UIImage imageNamed:@"circle"]]) {
        [self.selectIconImageView setImage:[UIImage imageNamed:@"card_007"]];
        if ([self.messageCellDelegate respondsToSelector:@selector(cellDidClick:)]) {
            [self.messageCellDelegate cellDidClick:self];
        }
    }
    else {
        [self.selectIconImageView setImage:[UIImage imageNamed:@"circle"]];
        if ([self.messageCellDelegate respondsToSelector:@selector(cellCancel:)]) {
            [self.messageCellDelegate cellCancel:self];
        }
    }
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
