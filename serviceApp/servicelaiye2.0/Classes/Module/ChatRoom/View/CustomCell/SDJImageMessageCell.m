//
//  SDJImageMessageCell.m
//  geniuslaiye2.0
//
//  Created by 盛东 on 15/12/2.
//  Copyright © 2015年 shengdong. All rights reserved.
//

#import "SDJImageMessageCell.h"

@implementation SDJImageMessageCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {}
    
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {}
    return self;
}

// 懒加载
- (RCImageMessageProgressView *)progressView {
    if (!_progressView) {
        _progressView = [[RCImageMessageProgressView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
        [self.messageContentView addSubview:_progressView];
        self.messageActivityIndicatorView = nil;
    }
    return _progressView;
}

- (UIImageView *)pictureView {
    
    if (!_pictureView) {
        _pictureView = [[UIImageView alloc] initWithFrame:CGRectZero];
        _pictureView.layer.cornerRadius = 2.0f;
        _pictureView.layer.masksToBounds = YES;
        [self.messageContentView addSubview:_pictureView];
        UILongPressGestureRecognizer *longPress =
        [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressed:)];
        [_pictureView addGestureRecognizer:longPress];
        UITapGestureRecognizer *pictureTap =
        [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapPicture:)];
        pictureTap.numberOfTapsRequired = 1;
        pictureTap.numberOfTouchesRequired = 1;
        [_pictureView addGestureRecognizer:pictureTap];
        _pictureView.userInteractionEnabled = YES;
    }
    return _pictureView;
}

- (void)tapPicture:(UIGestureRecognizer *)gestureRecognizer {
    if ([self.delegate respondsToSelector:@selector(didTapMessageCell:)]) {
        [self.delegate didTapMessageCell:self.model];
    }
}

- (void)setDataModel:(RCMessageModel *)model with:(BOOL)isMoreSelect{
    
    [super setDataModel:model with:isMoreSelect];
    
    RCImageMessage *_imageMessage = (RCImageMessage *)model.content;
    if (_imageMessage) {
        
        CGSize imageSize = _imageMessage.thumbnailImage.size;
        
        //兼容240
        CGFloat imageWidth = 120;
        CGFloat imageHeight = 120;
        
        CGFloat imageWidthMin = 60;
        CGFloat imageHeightMin = 60;
        CGFloat imageWidthMax = [UIScreen mainScreen].bounds.size.width * 0.5/*0.8*/;
        CGFloat imageHeightMax = [UIScreen mainScreen].bounds.size.height * 0.3/*0.6*/;
        
//        CGFloat imageWidthMax = self.superview.frame.size.width*0.8;
//        CGFloat imageHeightMax = self.superview.frame.size.height*0.6;
        CGFloat scale = imageWidthMax * 1.0 / imageHeightMax;
        //图片宽度和图片高度小于最小限制
        if (imageSize.width < imageWidthMin && imageSize.height < imageHeightMin) {
            //宽大于高
            if (imageSize.width >= imageSize.height) {
                //宽高比大于能显示的最大宽高比
                if (imageSize.width * 1.0 / imageSize.height >= imageWidthMax * 1.0 / imageHeightMin) {
                    imageWidth = imageWidthMax;
                    imageHeight = imageSize.height *imageWidthMax * 1.0 / imageSize.width;
                } else {
                    imageHeight = imageHeightMin;
                    imageWidth = imageSize.width * imageHeightMin * 1.0 / imageSize.height;
                }
            } else {
                if (imageSize.height * 1.0 / imageSize.width >= imageHeightMax * 1.0 / imageWidthMin) {
                    imageHeight = imageHeightMax;
                    imageWidth = imageSize.width *imageHeightMax * 1.0 / imageSize.height;
                } else {
                    imageWidth = imageWidthMin;
                    imageHeight = imageSize.height * imageWidthMin * 1.0 / imageSize.width;
                }
            }
        } else if (imageSize.width > imageWidthMax || imageSize.height > imageHeightMax) {
            if (imageSize.width >= imageSize.height * scale) {
                imageWidth = imageWidthMax;
                imageHeight = imageSize.height *imageWidthMax * 1.0 / imageSize.width;
            } else {
                imageHeight = imageHeightMax;
                imageWidth = imageSize.width * imageHeightMax * 1.0 / imageSize.height;
            }
        } else {
            imageWidth = imageSize.width;
            imageHeight = imageSize.height;
        }
        
        imageSize = CGSizeMake(imageWidth, imageHeight);
        CGRect messageContentViewRect = self.messageContentView.frame;
        
        // 图片需要做边框裁剪
        if (model.messageDirection == MessageDirection_RECEIVE) {
            messageContentViewRect.size.width = imageSize.width;
            messageContentViewRect.size.height = imageSize.height;
            messageContentViewRect.origin.x = /*5 + [RCIM sharedRCIM].globalMessagePortraitSize.width +*/ 15;
            self.messageContentView.frame = messageContentViewRect;
            self.pictureView.frame = CGRectMake(0, 0, imageSize.width, imageSize.height);
            
            self.pictureView.image = [_imageMessage.thumbnailImage drawImagewithWidth:3.0 withColor:HEXCOLOR(0xd4d4d4) withDirection:NO];
        } else {
            messageContentViewRect.size.width = imageSize.width;
            messageContentViewRect.size.height = imageSize.height;
            messageContentViewRect.origin.x = self.baseContentView.bounds.size.width - (imageSize.width + 10);
            self.messageContentView.frame = messageContentViewRect;
            self.pictureView.frame = CGRectMake(0, 0, imageSize.width, imageSize.height);
            self.pictureView.image = [_imageMessage.thumbnailImage drawImagewithWidth:3.0 withColor:HEXCOLOR(0xd4d4d4) withDirection:YES];
        }
    } else {}
    
    [self setAutoLayout];
    if (model.sentStatus == SentStatus_SENDING) {
        [self updateStatusContentView:self.model];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.pictureView addSubview:_progressView];
            [self.progressView setFrame:self.pictureView.bounds];
            [self.progressView startAnimating];
            self.pictureView.userInteractionEnabled = NO;
        });
    }
}

- (void)setAutoLayout {}

- (void)messageCellUpdateSendingStatusEvent:(NSNotification *)notification {
    
    [super messageCellUpdateSendingStatusEvent:notification];
    
    RCMessageCellNotificationModel *notifyModel = notification.object;
    
    NSInteger progress = notifyModel.progress;
    
    if (self.model.messageId == notifyModel.messageId) {
        if ([notifyModel.actionName isEqualToString:CONVERSATION_CELL_STATUS_SEND_BEGIN]) {
            self.model.sentStatus = SentStatus_SENDING;
            [self updateStatusContentView:self.model];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.pictureView addSubview:_progressView];
                [self.progressView setFrame:self.pictureView.bounds];
                [self.progressView startAnimating];
                self.pictureView.userInteractionEnabled = NO;
            });
            
        } else if ([notifyModel.actionName isEqualToString:CONVERSATION_CELL_STATUS_SEND_FAILED]) {
            self.model.sentStatus = SentStatus_FAILED;
            [self updateStatusContentView:self.model];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.progressView stopAnimating];
                [self.progressView removeFromSuperview];
                self.progressView = nil;
                self.pictureView.userInteractionEnabled = YES;
            });
        } else if ([notifyModel.actionName isEqualToString:CONVERSATION_CELL_STATUS_SEND_SUCCESS]) {
            self.model.sentStatus = SentStatus_SENT;
            [self updateStatusContentView:self.model];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.progressView stopAnimating];
                [self.progressView removeFromSuperview];
                self.progressView = nil;
                self.pictureView.userInteractionEnabled = YES;
            });
        } else if ([notifyModel.actionName isEqualToString:CONVERSATION_CELL_STATUS_SEND_PROGRESS]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.progressView updateProgress:progress];
            });
        }
    }
}

- (void)msgStatusViewTapEventHandler:(id)sender {}

- (void)longPressed:(id)sender {
    UILongPressGestureRecognizer *press = (UILongPressGestureRecognizer *)sender;
    if (press.state == UIGestureRecognizerStateEnded) {
        return;
    } else if (press.state == UIGestureRecognizerStateBegan) {
        [self.delegate didLongTouchMessageCell:self.model inView:self.pictureView];
    }
}

@end
