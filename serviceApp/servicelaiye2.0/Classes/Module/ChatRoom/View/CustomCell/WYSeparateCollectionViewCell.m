//
//  WYSeparateCollectionViewCell.m
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/3/25.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import "WYSeparateCollectionViewCell.h"
#import "WYSeparateModel.h"
#import "RCMessageModel.h"

@interface WYSeparateCollectionViewCell ()

@property (nonatomic, strong) UILabel *separateLabel;

@end

@implementation WYSeparateCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize {}

- (UILabel *)separateLabel {
    
	if(_separateLabel == nil) {
		_separateLabel = [[UILabel alloc] init];
        _separateLabel.textColor = [UIColor lightGrayColor];
        _separateLabel.font = [UIFont systemFontOfSize:12];
        _separateLabel.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:_separateLabel];
	}
	return _separateLabel;
}

-(void)layoutSubviews{
    
    [super layoutSubviews];
    [self.separateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.height.equalTo(self);
    }];
}

- (void)setDataModel:(RCMessageModel *)model with:(BOOL)isMoreSelect{
    
    self.separateLabel.text = ((WYSeparateModel *)model.content).textContent;
}

@end
