//
//  SDJTextMessageCell.h
//  geniuslaiye2.0
//
//  Created by 盛东 on 15/12/2.
//  Copyright © 2015年 shengdong. All rights reserved.
//

#import "RCMessageCell.h"
#import "RCAttributedLabel.h"

#define Text_Message_Font_Size 16

@interface SDJTextMessageCell : RCMessageCell<RCAttributedLabelDelegate>

@property(strong, nonatomic) RCAttributedLabel *textLabel; // 消息显示Label

@property(nonatomic, strong) UIImageView *bubbleBackgroundView; // 消息背景

- (void)setDataModel:(RCMessageModel *)model with:(BOOL)isMoreSelect; // 设置消息数据模型

@end
