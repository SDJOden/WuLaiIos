//
//  WYChangeGeniusViewController.m
//  servicelaiye2.0
//
//  Created by 汪洋 on 15/12/29.
//  Copyright © 2015年 shengdong. All rights reserved.
//

#import "WYChangeGeniusViewController.h"
#import "WYChangeGeniusTopView.h"
#import "WYChangeGeniusFooterView.h"
#import "WYChangeGeniusHeaderView.h"
#import "WYChangeStatuTableViewCell.h"
#import "WYChangeGeniusListModel.h"
#import "JZTabBar.h"

@interface WYChangeGeniusViewController ()<UITableViewDelegate, UITableViewDataSource, WYChangeGeniusTopViewDelegate, WYChangeGeniusFooterViewDelegate>

@property (nonatomic, strong) WYChangeGeniusTopView *topView;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) WYChangeGeniusFooterView *bottomView;
@property (nonatomic, strong) NSMutableArray *geniusListDataArray;
@property (nonatomic, strong) NSMutableArray *displayDataArray;
@property (nonatomic, strong) UIButton *blankBtn;
@property (nonatomic, strong) UITableView *statuTableView;
@property (nonatomic, strong) WYChangeGeniusListModel *listModel;

@end

@implementation WYChangeGeniusViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = YES;
    self.view.backgroundColor = [UIColor whiteColor];
    [self addSubViews];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    //通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textChanged:) name:UITextFieldTextDidChangeNotification object:self.topView.chooseTextField];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(callbackKeyboard) name:@"CallbackKeyboard" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(openKeyboard:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(closeKeyboard:) name:UIKeyboardWillHideNotification object:nil];
    [self refreshData];
}

-(void)refreshData {
    
    __weak typeof(self)__weakSelf = self;
    [[SDJNetworkTools sharedTools]onlineGeniusList:[SDJUserAccount loadAccount].token finished:^(NSDictionary<NSString *,id> * _Nullable result, NSError * _Nullable error) {
        if (![result[@"error"] isEqual:[NSNull null]]) {//报错
            NSNumber *code = result[@"error"][@"error_code"];
            if (code.integerValue == 20000) {
                NSString *UserPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).firstObject stringByAppendingPathComponent:@"account.plist"];
                [[NSFileManager defaultManager]removeItemAtPath:UserPath error:nil];
                //其他平台登录，token改变需要先登出融云
                [[RCIMClient sharedRCIMClient] logout];
                // 发送通知让appdelegate去切换根视图为聊天列表界面
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"WYRootViewControllerSwitchNotification" object:@"0"];
                });
            }
        } else{
            [self.geniusListDataArray removeAllObjects];
            NSArray *dataArray = result[@"data"];
            for (NSDictionary *dict in dataArray) {
                WYChangeGeniusListModel *geniusListModel = [WYTool createChangeGeniusListModelByDict:dict];
                //屏蔽自己的genius_id
                if ([geniusListModel.username isEqualToString:[[SDJUserAccount loadAccount].username substringToIndex:9]]) {
                    continue;
                }
                [__weakSelf.geniusListDataArray addObject:geniusListModel];
            }
            __weakSelf.displayDataArray = [__weakSelf.geniusListDataArray mutableCopy];
            [__weakSelf.tableView reloadData];
        }
    }];
}

-(void)addSubViews{
    
    [self.topView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(20);
        make.left.equalTo(self.view).offset(5);
        make.right.equalTo(self.view).offset(-5);
        make.height.equalTo(50);
    }];
    
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.left.right.equalTo(self.view);
        make.height.equalTo(120);
    }];
    
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.topView.bottom).offset(10);
        make.left.equalTo(self.view).offset(5);
        make.right.equalTo(self.view).offset(-5);
        make.bottom.equalTo(self.bottomView.top);
    }];
}

- (WYChangeGeniusTopView *)topView {
    
	if(_topView == nil) {
		_topView = [[WYChangeGeniusTopView alloc] init];
        _topView.changeGeniusTopDelegate = self;
        _topView.layer.borderWidth = 1;
        _topView.layer.cornerRadius = 5.0;
        _topView.layer.borderColor = [[UIColor lightGrayColor] CGColor];
        [self.view addSubview:_topView];
        
	}
	return _topView;
}

- (WYChangeGeniusFooterView *)bottomView {
    
    if(_bottomView == nil) {
        _bottomView = [[WYChangeGeniusFooterView alloc] init];
        _bottomView.changeGeniusFooterViewDelegate = self;
        [self.view addSubview:_bottomView];
    }
    return _bottomView;
}

- (UITableView *)tableView {
    
	if(_tableView == nil) {
		_tableView = [[UITableView alloc] init];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.tableFooterView = [UIView new];
        _tableView.layer.cornerRadius = 5.0;
        _tableView.bounces = NO;
        _tableView.showsVerticalScrollIndicator =
        NO;
        _tableView.layer.borderWidth = 1;
        _tableView.layer.borderColor = [[UIColor lightGrayColor] CGColor];
        [_tableView registerClass:[WYChangeStatuTableViewCell class] forCellReuseIdentifier:@"WYChangeStautTableViewCell"];
        
        [self.view addSubview:_tableView];
	}
	return _tableView;
}

- (NSMutableArray *)geniusListDataArray {
    
    if(_geniusListDataArray == nil) {
        _geniusListDataArray = [NSMutableArray array];
    }
    return _geniusListDataArray;
}

-(NSMutableArray *)displayDataArray{
    
    if(_displayDataArray == nil) {
        _displayDataArray = [NSMutableArray array];
    }
    return _displayDataArray;
}

- (UIButton *)blankBtn {
    
    if(_blankBtn == nil) {
        _blankBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _blankBtn.backgroundColor = [UIColor clearColor];
        [_blankBtn addTarget:self action:@selector(hiddenTableView:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _blankBtn;
}

- (UITableView *)statuTableView {
    
    if(_statuTableView == nil) {
        _statuTableView = [[UITableView alloc] init];
        _statuTableView.delegate = self;
        _statuTableView.dataSource = self;
        _statuTableView.bounces = NO;
        _statuTableView.layer.borderWidth = 1;
        _statuTableView.layer.cornerRadius = 5.0;
        _statuTableView.layer.borderColor = [[UIColor lightGrayColor] CGColor];
        [_statuTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"statuCell"];
    }
    return _statuTableView;
}

- (WYChangeGeniusListModel *)listModel {
    
    if(_listModel == nil) {
        _listModel = [[WYChangeGeniusListModel alloc] init];
    }
    return _listModel;
}

#pragma mark --- UITableViewDelegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (tableView == self.tableView) {
        return self.displayDataArray.count;
    }
    return 4;

}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (tableView == self.tableView) {
        static NSString *identifier = @"WYChangeStautTableViewCell";
        WYChangeStatuTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
        [cell setContentByHistoryModel:self.displayDataArray[indexPath.row]];
        return cell;
    }
    else if (tableView == self.statuTableView) {
        static NSString *statuIdentifier = @"statuCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:statuIdentifier forIndexPath:indexPath];
        cell.textLabel.font = [UIFont systemFontOfSize:12];
        switch (indexPath.row) {
            case 0:
                cell.textLabel.text = @"全部";
                break;
            case 1:
                cell.textLabel.text = @"关闭";
                break;
            case 2:
                cell.textLabel.text = @"接单";
                break;
            case 3:
                cell.textLabel.text = @"派单";
                break;
            default:
                break;
        }
        return cell;
    }
    return nil;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (tableView == self.tableView) {
        return 44;
    }
    else if (tableView == self.statuTableView) {
        return 20;
    }
    return 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    if (tableView == self.tableView) {
        return 44;
    }
    else if (tableView == self.statuTableView) {
        return 0;
    }
    return 0;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    if (tableView == self.tableView) {
        WYChangeGeniusHeaderView *headerView = [WYChangeGeniusHeaderView new];
        headerView.userInteractionEnabled = YES;
        return headerView;
    }
    return nil;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [self.view endEditing:YES];
    
    if (tableView == self.tableView) {
        self.listModel = self.displayDataArray[indexPath.row];
        WYChangeStatuTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        cell.contentView.backgroundColor = [UIColor colorWithRed:1.000 green:0.808 blue:0.000 alpha:1.000];
    }
    else if (tableView == self.statuTableView) {
        [self.displayDataArray removeAllObjects];
        [self.topView changeBtnTitle:[tableView cellForRowAtIndexPath:indexPath].textLabel.text];
        NSString *statuStr = [NSString string];
        switch (indexPath.row) {
            case 0:
                statuStr = @"全部";
                break;
            case 1:
                statuStr = @"关闭";
                break;
            case 2:
                statuStr = @"接单";
                break;
            case 3:
                statuStr = @"派单";
                break;
                
            default:
                break;
        }
        self.displayDataArray = [self search:statuStr];
        [self.tableView reloadData];
        [self hiddenTableView:self.blankBtn];
    }
}

-(NSMutableArray *)search:(NSString *)statu{
    
    NSMutableArray *tempArray = [NSMutableArray array];
    for (WYChangeGeniusListModel *listModel in self.geniusListDataArray) {
        if ([self.topView.chooseTextField.text isEqualToString:@""]) {
            [tempArray addObject:listModel];
            continue;
        }
        if ([listModel.real_name rangeOfString:self.topView.chooseTextField.text].location != NSNotFound && listModel.real_name.length != 0) {
            [tempArray addObject:listModel];
            continue;
        }
        if ([listModel.username rangeOfString:self.topView.chooseTextField.text].location != NSNotFound && listModel.username.length != 0) {
            [tempArray addObject:listModel];
            continue;
        }
        if ([listModel.phone rangeOfString:self.topView.chooseTextField.text].location != NSNotFound && listModel.phone.length != 0) {
            [tempArray addObject:listModel];
            continue;
        };
    }
    NSMutableArray *tempArrayStatu = [NSMutableArray array];
    for (WYChangeGeniusListModel *listModel in tempArray) {
        if ([statu isEqualToString:@"全部"]) {
            tempArrayStatu = [tempArray mutableCopy];
            continue;
        }
        else if ([statu isEqualToString:@"关闭"] && listModel.accept_limit.intValue == -1) {
            [tempArrayStatu addObject:listModel];
            continue;
        }
        else if ([statu isEqualToString:@"接单"] && listModel.accept_limit.intValue == 0) {
            [tempArrayStatu addObject:listModel];
            continue;
        }
        else if ([statu isEqualToString:@"派单"] && listModel.accept_limit.intValue > 0) {
            [tempArrayStatu addObject:listModel];
            continue;
        }
    }
    return [tempArrayStatu mutableCopy];
}

//分割线顶头
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]){
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    cell.preservesSuperviewLayoutMargins = NO;
}

#pragma mark --- TopViewDelegate
-(void)statuDidClicked:(WYChangeGeniusTopView *)changeGeniusTopView{

    [self.view addSubview:self.blankBtn];
    [self.view addSubview:self.statuTableView];
    [self addStatuTableView];
}

-(void)refreshDidClicked:(WYChangeGeniusTopView *)changeGeniusTopView{
    
    [self refreshData];
}

-(void)addStatuTableView{
    
    [_blankBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view);
        make.left.equalTo(self.view);
        make.right.equalTo(self.view);
        make.bottom.equalTo(self.view);
    }];
    
    [_statuTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(30);
        make.left.equalTo(self.view.right).offset(-90);
        make.right.equalTo(self.view).offset(-10);
        make.height.equalTo(80);
    }];
}

-(void)hiddenTableView:(UIButton *)sender{
    
    [self.statuTableView removeFromSuperview];
    [sender removeFromSuperview];
    self.statuTableView = nil;
    sender = nil;
}

#pragma mark --- FootViewDelegate
-(void)sureDidClicked:(WYChangeGeniusFooterView *)changeGeniusFooterView{
    
    [self changeGenius];
}

-(void)changeGenius{
    
    __weak typeof(self)__weakSelf = self;
    [[SDJNetworkTools sharedTools]changeUser:[SDJUserAccount loadAccount].token user_id:self.inner_uid to:self.listModel.genius_id extra_msg:self.bottomView.exegesisTextView.text finished:^(NSDictionary<NSString *,id> * _Nullable result, NSError * _Nullable error) {
        if (![result[@"error"] isEqual:[NSNull null]]) {//报错
            NSNumber *code = result[@"error"][@"error_code"];
            if (code.integerValue == 20000) {
                NSString *UserPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).firstObject stringByAppendingPathComponent:@"account.plist"];
                [[NSFileManager defaultManager]removeItemAtPath:UserPath error:nil];
                //其他平台登录，token改变需要先登出融云
                [[RCIMClient sharedRCIMClient] logout];
                // 发送通知让appdelegate去切换根视图为聊天列表界面
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"WYRootViewControllerSwitchNotification" object:@"0"];
                });
            }
        }
        else{
            if ([result[@"data"] isKindOfClass:[NSNull class]]) {
                [[NSNotificationCenter defaultCenter]postNotificationName:@"deleteConnectUser" object:nil userInfo:@{@"inner_uid":@(__weakSelf.inner_uid.integerValue)}];
                dispatch_async(dispatch_get_main_queue(), ^{
                    __weakSelf.navigationController.navigationBarHidden = NO;
                    [__weakSelf.navigationController popToRootViewControllerAnimated:YES];
                });
            }
        }
    }];
}

-(void)cancelDidClicked:(WYChangeGeniusFooterView *)changeGeniusFooterView{
    
    self.navigationController.navigationBarHidden = NO;
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark --- 监听textField
// textField内容改变执行下列方法
- (void)textChanged:(NSNotification *)noti {
    
    self.displayDataArray = [self search:self.topView.statuBtn.titleLabel.text];
    [self.tableView reloadData];
    
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    
    [self.view endEditing:YES];
}

#pragma mark --- 通知
-(void)openKeyboard:(NSNotification *)notification{
    
    //键盘弹出动画时长
    NSTimeInterval duration = [notification.userInfo[UIKeyboardAnimationDurationUserInfoKey]doubleValue];
    //键盘弹出的动画效果
    UIViewAnimationOptions option = [notification.userInfo[UIKeyboardAnimationCurveUserInfoKey]doubleValue];
    //键盘弹出后的位置
    CGRect endFrame = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    //动画移动 输入框容器
    [UIView animateWithDuration:duration delay:0 options:option animations:^{
        [self.bottomView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self.view).offset(endFrame.size.height * (-1));
        }];
    } completion:^(BOOL finished) {
        
    }];
    [self.view layoutIfNeeded];
}

/**键盘消失时触发*/
-(void)closeKeyboard:(NSNotification *)notification{
    
    //键盘消失动画时长
    NSTimeInterval duration = [notification.userInfo[UIKeyboardAnimationDurationUserInfoKey]doubleValue];
    //键盘消失的动画效果
    UIViewAnimationOptions option = [notification.userInfo[UIKeyboardAnimationCurveUserInfoKey]doubleValue];
    __weak typeof(self) __weakSelf = self;
    [UIView animateWithDuration:duration delay:0 options:option animations:^{
        [__weakSelf.bottomView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self.view);
        }];
    } completion:^(BOOL finished) {
        
    }];
    [self.view layoutIfNeeded];
}

-(void)callbackKeyboard{
    
    [self.view endEditing:YES];
}

-(void)dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)viewWillDisappear:(BOOL)animated{
#warning to do 轻耦合, 这里使用通知可能比较好.
    [super viewWillDisappear:animated];
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    UIViewController *vc = window.rootViewController.childViewControllers[0].childViewControllers[0];
    vc.tabBarController.selectedIndex = 0;
    //
    for (UIView *view in vc.tabBarController.tabBar.subviews) {
        if ([view isKindOfClass:[JZTabbar class]]) {
            [(JZTabbar *)view selectItemWithIndex:1];
            break;
        }
    }
}


@end
