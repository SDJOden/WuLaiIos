//
//  SDJCardEnlargeController.m
//  geniuslaiye2.0
//
//  Created by 盛东 on 15/11/19.
//  Copyright © 2015年 shengdong. All rights reserved.
//

#import "SDJCardEnlargeController.h"

@interface SDJCardEnlargeController ()

@end

@implementation SDJCardEnlargeController

- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor yellowColor];
    
    UIButton *closeBtn = [[UIButton alloc] init];
    [closeBtn setImage:[UIImage imageNamed:@"registerVC_closeBtn"] forState:UIControlStateNormal];
    [closeBtn addTarget:self action:@selector(close) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:closeBtn];
    closeBtn.frame = CGRectMake(self.view.bounds.size.width - 34, 5 + 20, 29, 29);
}

- (void)close {
    
    [self dismissViewControllerAnimated:YES completion:nil]; // 可以加动画
}

@end
