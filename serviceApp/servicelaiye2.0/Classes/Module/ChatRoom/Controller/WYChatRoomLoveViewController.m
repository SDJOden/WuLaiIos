//
//  WYChatRoomLoveViewController.m
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/7/5.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import "WYChatRoomLoveViewController.h"
#import "WYChatHisMsgHeaderView.h"
#import "WYLoveListCell.h"
#import "RCMessageModel.h"
#import "WYSeparateModel.h"
#import "WYConnectListModel.h"
#import "WYLoveListTypeView.h"
#import "WYLoveCategoryCollectionViewCell.h"
#import "WYLoveTypeModel.h"
#import "UIBarButtonItem+Extension.h"
#import "MBProgressHUD.h"
#import "WYNetworkTools.h"
#import "MJRefresh.h"

@interface WYChatRoomLoveViewController () <UITableViewDelegate, UITableViewDataSource, WYChatHisMsgHeaderViewDelegate, WYLoveListTypeViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource>

@property (nonatomic, strong) WYConnectListModel *connectListModel;
@property (nonatomic, strong) WYChatHisMsgHeaderView *chatHeaderView;
@property (nonatomic, strong) UITableView *loveListTabelView;
@property (nonatomic, strong) UIButton *cancelButton;
@property (nonatomic, strong) UIButton *completeButton;
@property (nonatomic, strong) WYLoveListTypeView *loveListTypeView;
@property (nonatomic, strong) NSMutableArray *selectDataArray;
@property (nonatomic, assign) NSInteger page;
@property (nonatomic, assign) NSInteger dataNumber;
@property (nonatomic, copy) NSString *categoryStr;
@property (nonatomic, strong) NSArray *serviceArr;
@property (nonatomic, strong) UIView *separateLineFirst;
@property (nonatomic, strong) UICollectionView *sessionTypeCollectionView;
@property (nonatomic, strong) UICollectionViewFlowLayout *sessionTypeFlowLayout;
@property (nonatomic, strong) UIView *separateLineSecond;

@end

@implementation WYChatRoomLoveViewController

-(instancetype)init{
    
    if (self = [super init]) {
    }
    return self;
}

-(instancetype)initWithConnectListModel:(WYConnectListModel *)connectListModel andPage:(NSInteger)page{
    
    if (self = [super init]) {
        [self rcinit];
        self.page = page;
        _connectListModel = connectListModel;
        [self addSubViews];
    }
    return self;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder{
    
    if (self = [super initWithCoder:aDecoder]) {
    }
    return self;
}

-(void)rcinit{
    
    self.title = @"点赞";
    self.view.backgroundColor = [UIColor whiteColor];
    UIBarButtonItem *leftItem = [UIBarButtonItem itemWithTitle:@"返回" target:self action:@selector(backToChatRoom) left:YES];
    self.navigationItem.leftBarButtonItem = leftItem;
    _categoryStr = @"-1";
}

-(void)setListDataArray:(NSMutableArray *)listDataArray{
    
    _listDataArray = [listDataArray mutableCopy];
    for (RCMessageModel *model in _listDataArray) {
        RCMessageContent *messageContent = model.content;
        if ([messageContent isMemberOfClass:[WYSeparateModel class]]) {
            [_listDataArray removeObject:model];
            break;
        }
    }
    [self addSubViews];
    [self.loveListTabelView reloadData];
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:self.listDataArray.count-1 inSection:0];
    [self.loveListTabelView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
}

-(void)viewDidAppear:(BOOL)animated{
    
    [super viewDidAppear:animated];
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
}

-(void)addSubViews{
    
    [self.chatHeaderView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self.view);
        make.height.equalTo(44);
    }];
    
    [self.view sendSubviewToBack:self.loveListTabelView];
    [self.loveListTabelView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.chatHeaderView.bottom);
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.view).offset(-88);
    }];
    
    [self.separateLineFirst mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.loveListTabelView.bottom);
        make.left.right.equalTo(self.view);
        make.height.equalTo(0.5);
    }];
    
    [self.sessionTypeCollectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.separateLineFirst.bottom);
        make.right.equalTo(self.view).offset(-10);
        make.left.equalTo(self.view).offset(10);
        make.height.equalTo(43.5);
    }];
    
    [self.separateLineSecond mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.sessionTypeCollectionView.bottom);
        make.left.right.equalTo(self.view);
        make.height.equalTo(0.5);
    }];
    
    [self.cancelButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.view);
        make.left.equalTo(self.view);
        make.width.equalTo([UIScreen mainScreen].bounds.size.width*0.5);
        make.height.equalTo(43.5);
    }];
    
    [self.completeButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.view);
        make.right.equalTo(self.view);
        make.left.equalTo(self.cancelButton.right);
        make.height.equalTo(43.5);
    }];
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
}

#pragma mark --- 点击事件
-(void)backToChatRoom{
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)changeServiceType{
    
    [self.loveListTypeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self.view);
        make.bottom.equalTo(-44);
    }];
}

-(void)completeLove{
    
    [self sortCellSelectArrOrderedDescending:self.selectDataArray];
    [self loveSession:self.selectDataArray];
}

/**
 *  降序
 */
-(void)sortCellSelectArrOrderedDescending:(NSMutableArray *)mutableArray{
    
    [mutableArray sortUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        if ([obj1 integerValue] > [obj2 integerValue]) {
            return (NSComparisonResult)NSOrderedAscending;
        }
        if ([obj1 integerValue] < [obj2 integerValue]) {
            return (NSComparisonResult)NSOrderedDescending;
        }
        return (NSComparisonResult)NSOrderedSame;
    }];
}

#pragma mark --- UITableViewDelegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.listDataArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *Identifier = @"loveListCell";
    WYLoveListCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    RCMessageModel *model = self.listDataArray[indexPath.row];
    RCMessageContent *messageContent = model.content;
    if ([messageContent isMemberOfClass:[WYSeparateModel class]]) {
        return nil;
    }
    cell.model = model;
    if ([self.selectDataArray containsObject:@((NSInteger)self.listDataArray.count - indexPath.row)]) {
        cell.isSelectImage = YES;
    }else {
        cell.isSelectImage = NO;
    }
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    RCMessageModel *model = self.listDataArray[indexPath.row];
    RCMessageContent *messageContent = model.content;
    if ([messageContent isMemberOfClass:[RCTextMessage class]]) {
        CGFloat height = 12+20+5+12;
        
        NSMutableString *realTextString = [NSMutableString stringWithFormat:@""];
        if ([messageContent isKindOfClass:[RCTextMessage class]]) {
            NSString *text = ((RCTextMessage*)messageContent).content;
            NSString *pattern = @"<a href=\'(.*?)\'>(.*?)</a>";
            NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern options:0 error:nil];
            NSArray *matches = [regex matchesInString:text options:0 range:NSMakeRange(0, text.length)]; // 网址
            NSUInteger lastIdx = 0;
            NSMutableString *attributedString = [NSMutableString stringWithFormat:@""]; // 总字符串
            if (matches.count) { // 解析部分bug, 1.文字需要提出来, 2. 位置需要调整 matchs.count
                NSMutableArray *urlArrs = [[NSMutableArray alloc] init]; // 存url
                int i = 0;
                for (NSTextCheckingResult* match in matches)
                {
                    NSRange range = match.range;
                    if (range.location > lastIdx)
                    {
                        NSString *temp = [text substringWithRange:NSMakeRange(lastIdx, range.location - lastIdx)];
                        [attributedString appendString:temp];// 记录头
                        [realTextString appendString:temp];
                    }
                    NSString *title = [text substringWithRange:[match rangeAtIndex:2]]; // 标题
                    NSString *url = [text substringWithRange:[match rangeAtIndex:1]]; // 网址
                    lastIdx = range.location + range.length;
                    [attributedString appendFormat:@"<%d>%@</%d>",i,title,i];
                    [realTextString appendString:title];
                    [urlArrs addObject:url];
                    i++;
                }
                if (lastIdx < text.length)
                {
                    NSString *temp = [text substringFromIndex:lastIdx];
                    [attributedString appendString:temp];// 记录尾
                    [realTextString appendString:temp];
                }
            } else {
                [realTextString appendString:((RCTextMessage *)model.content).content];
            }
        }
        CGSize __textSize = CGSizeZero;
        if (IOS_FSystenVersion < 7.0) {
            __textSize = RC_MULTILINE_TEXTSIZE_LIOS7(realTextString, [UIFont systemFontOfSize:16], CGSizeMake([UIScreen mainScreen].bounds.size.width - 12 - 121 - 40, 8000), NSLineBreakByTruncatingTail);
        }else {
            __textSize = RC_MULTILINE_TEXTSIZE_GEIOS7(realTextString, [UIFont systemFontOfSize:16], CGSizeMake([UIScreen mainScreen].bounds.size.width - 12 - 121 - 40, 8000));
        }
        __textSize = CGSizeMake(ceilf(__textSize.width), ceilf(__textSize.height));
        CGSize __labelSize = CGSizeMake(__textSize.width, __textSize.height);
        CGFloat __bubbleWidth = __labelSize.width + 20 + 20 < 50 ? 50 : (__labelSize.width + 20 + 20);
        CGFloat __bubbleHeight = __labelSize.height + 12 + 12 < 46 ? 46 : (__labelSize.height + 12 + 12);
        CGSize __bubbleSize = CGSizeMake(__bubbleWidth, __bubbleHeight);
        
        height += __bubbleSize.height;
        return height;
    } else if ([messageContent isMemberOfClass:[RCImageMessage class]] || [messageContent isMemberOfClass:[RCLocationMessage class]] || [messageContent isMemberOfClass:[RCVoiceMessage class]] || [messageContent isMemberOfClass:[RCRichContentMessage class]]) {
        return 12+20+5+46+12;
    }
    else if ([messageContent isMemberOfClass:[WYSeparateModel class]]) {
        return 0;
    }
    return 10;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    WYLoveListCell * cell = [tableView cellForRowAtIndexPath:indexPath];
    if (![self.selectDataArray containsObject:@((NSInteger)self.listDataArray.count - indexPath.row)]) {
        [self.selectDataArray addObject:@((NSInteger)self.listDataArray.count - indexPath.row)];
        [cell changeSelectImageView:YES];
    }
    else{
        [self.selectDataArray removeObject:@((NSInteger)self.listDataArray.count - indexPath.row)];
        [cell changeSelectImageView:NO];
    }
    if (self.selectDataArray.count != 0) {
        self.completeButton.enabled = YES;
    }else {
        self.completeButton.enabled = NO;
    }
}

-(void)moreData{
    
    self.page ++;
    __weak WYChatRoomLoveViewController *__weakSelf = self;
    __block NSInteger downloadFlag = 0;
    [[SDJNetworkTools sharedTools]userCurrentMessage:[SDJUserAccount loadAccount].token user_id:@(self.connectListModel.inner_uid).stringValue size:@"10" pn:@(self.page).stringValue finished:^(NSDictionary<NSString *,id> * _Nullable result, NSError * _Nullable error) {
        if (error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [__weakSelf.loveListTabelView.mj_header endRefreshing];
            });
        }
        if (![result[@"error"] isEqual:[NSNull null]]) {//异端登陆
            NSNumber *code = result[@"error"][@"error_code"];
            if (code.integerValue == 20000) {
                NSString *UserPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).firstObject stringByAppendingPathComponent:@"account.plist"];
                [[NSFileManager defaultManager]removeItemAtPath:UserPath error:nil];
                [[RCIMClient sharedRCIMClient] logout];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"WYRootViewControllerSwitchNotification" object:@"0"];
                });
            }
        }
        else {
            NSMutableArray *messageArray = [NSMutableArray array];
            __weakSelf.connectListModel.page = self.page;
            NSArray *historyMsgArray = result[@"data"][@"latest_msgs"];
            __weakSelf.dataNumber = historyMsgArray.count;
            for (NSDictionary *historyMsg in historyMsgArray) {
                int msgType = ((NSNumber *)historyMsg[@"extra"][@"msg_type"]).intValue;
                if (msgType == 1) {//text消息
                    RCMessageModel * msgModel = [WYTool createMsgModel:historyMsg];
                    [messageArray addObject:msgModel];
                    downloadFlag ++;
                    if (downloadFlag == __weakSelf.dataNumber) {
                        [__weakSelf messageDeal:[[messageArray reverseObjectEnumerator] allObjects]];
                        downloadFlag = 0;
                    }
                }
                else if (msgType == 2) {//image消息
                    RCMessageModel * msgModel = [WYTool createImageModel:historyMsg];
                    UIImageView *imageView = [[UIImageView alloc]init];
                    [imageView sd_setImageWithURL:[NSURL URLWithString:historyMsg[@"imageUri"]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            ((RCImageMessage *)msgModel.content).thumbnailImage = imageView.image;
                            ((RCImageMessage *)msgModel.content).originalImage = imageView.image;
                            [__weakSelf.loveListTabelView reloadData];
                            downloadFlag ++;
                            if (downloadFlag == __weakSelf.dataNumber) {
                                [__weakSelf messageDeal:[[messageArray reverseObjectEnumerator] allObjects]];
                                downloadFlag = 0;
                            }
                        });
                    }];
                    [messageArray addObject:msgModel];
                }
                else if (msgType == 5) {//text消息
#warning 后面可以增加语音功能在此地方
                    RCMessageModel * msgModel = [WYTool createVoiceMsgModel:historyMsg];
                    [messageArray addObject:msgModel];
                    downloadFlag ++;
                    if (downloadFlag == __weakSelf.dataNumber) {
                        [__weakSelf messageDeal:[[messageArray reverseObjectEnumerator] allObjects]];
                        downloadFlag = 0;
                    }
                }
                else if (msgType == 6) {
                    RCMessageModel * msgModel = [WYTool createCardMsgModel:historyMsg];
                    [messageArray addObject:msgModel];
                    downloadFlag ++;
                    if (downloadFlag == __weakSelf.dataNumber) {
                        [__weakSelf messageDeal:[[messageArray reverseObjectEnumerator] allObjects]];
                        downloadFlag = 0;
                    }
                }
                else if (msgType == 7) {
                    RCMessageModel * msgModel = [WYTool createSingleCardMsgModel:historyMsg];
                    [messageArray addObject:msgModel];
                    downloadFlag ++;
                    if (downloadFlag == __weakSelf.dataNumber) {
                        [__weakSelf messageDeal:[[messageArray reverseObjectEnumerator] allObjects]];
                        downloadFlag = 0;
                    }
                }
                else if (msgType == 10) {
                    RCMessageModel * msgModel = [WYTool createDateModel:historyMsg];
                    [messageArray addObject:msgModel];
                    downloadFlag ++;
                    if (downloadFlag == _dataNumber) {
                        [__weakSelf messageDeal:[[messageArray reverseObjectEnumerator] allObjects]];
                        downloadFlag = 0;
                    }
                }
                else {
                    downloadFlag ++;
                    if (downloadFlag == __weakSelf.dataNumber) {
                        [__weakSelf messageDeal:[[messageArray reverseObjectEnumerator] allObjects]];
                        downloadFlag = 0;
                    }
                }
            }
        }
    }];
}

/**
 *  WY添加
 */
-(void)messageDeal:(NSArray *)__messageArray{
    
    [self.loveListTabelView.mj_header endRefreshing];
    for (int i = 0; i < __messageArray.count; i++) {
        RCMessage *rcMsg = [__messageArray objectAtIndex:i];
        RCMessageModel *model = [[RCMessageModel alloc] initWithMessage:rcMsg];
        [self pushOldMessageModel:model];
    }
    [self.loveListTabelView reloadData];
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:self.dataNumber inSection:0];
    [self.loveListTabelView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:NO];
}

- (void)pushOldMessageModel:(RCMessageModel *)model {
    
    [self.listDataArray insertObject:model atIndex:0];
}

-(void)loveSession:(NSMutableArray *)mutableArray{
    
    if (mutableArray.count == 0) {
        return;
    } else {
        NSMutableArray *sessionArr = [[NSMutableArray alloc]init];
        for (NSNumber *num in mutableArray) {
            RCMessageModel *msgModel = self.listDataArray[self.listDataArray.count - num.integerValue];
            if ([msgModel.content isKindOfClass:[RCTextMessage class]]) {
                RCTextMessage *textMsg = (RCTextMessage *)msgModel.content;
                NSDictionary *dict = [NSDictionary dictionary];
                if ([textMsg.extra isKindOfClass:[NSString class]]) {
                    textMsg.extra = (NSString *)[WYTool jsonStringToDictionary:textMsg.extra];
                }
                if (msgModel.messageDirection == MessageDirection_RECEIVE) {
                    dict = @{@"text":textMsg.content, @"id":((NSNumber *)((NSDictionary *)textMsg.extra)[@"inner_uid"]).stringValue, @"time":@(msgModel.sentTime).stringValue, @"direction":@(0), @"type":@"User"};
                }
                else{
                    if ([[((NSDictionary *)textMsg.extra)[@"genius_info"] allKeys] containsObject:@"genius_id"]) {
                        dict = @{@"text":textMsg.content, @"id":((NSNumber *)((NSDictionary *)textMsg.extra)[@"genius_info"][@"genius_id"]).stringValue, @"time":@(msgModel.sentTime).stringValue, @"direction":@(1), @"type":@"Genius"};
                    } else {
                        dict = @{@"text":textMsg.content, @"id":@([SDJUserAccount loadAccount].genius_id).stringValue, @"time":@(msgModel.sentTime).stringValue, @"direction":@(1), @"type":@"Genius"};
                    }
                }
                [sessionArr addObject:dict];
            }
            else if ([msgModel.content isKindOfClass:[RCImageMessage class]]) {
                RCImageMessage *msg = (RCImageMessage *)msgModel.content;
                NSDictionary *dict = [NSDictionary dictionary];
                if ([msg.extra isKindOfClass:[NSString class]]) {
                    msg.extra = (NSString *)[WYTool jsonStringToDictionary:msg.extra];
                }
                if (msgModel.messageDirection == MessageDirection_RECEIVE) {
                    dict = @{@"text":@"[图片]", @"id":((NSNumber *)((NSDictionary *)msg.extra)[@"inner_uid"]).stringValue, @"time":@(msgModel.sentTime).stringValue, @"direction":@(0), @"type":@"User"};
                }
                else{
                    if ([[((NSDictionary *)msg.extra)[@"genius_info"] allKeys] containsObject:@"genius_id"]) {
                        dict = @{@"text":@"[图片]", @"id":((NSNumber *)((NSDictionary *)msg.extra)[@"genius_info"][@"genius_id"]).stringValue, @"time":@(msgModel.sentTime).stringValue, @"direction":@(1), @"type":@"Genius"};
                    } else {
                        dict = @{@"text":@"[图片]", @"id":@([SDJUserAccount loadAccount].genius_id).stringValue, @"time":@(msgModel.sentTime).stringValue, @"direction":@(1), @"type":@"Genius"};
                    }
                }
                [sessionArr addObject:dict];
            }
            else if ([msgModel.content isKindOfClass:[RCVoiceMessage class]]) {
                RCVoiceMessage *msg = (RCVoiceMessage *)msgModel.content;
                NSDictionary *dict = [NSDictionary dictionary];
                if ([msg.extra isKindOfClass:[NSString class]]) {
                    msg.extra = (NSString *)[WYTool jsonStringToDictionary:msg.extra];
                }
                if (msgModel.messageDirection == MessageDirection_RECEIVE) {
                    dict = @{@"text":@"[语音]", @"id":((NSNumber *)((NSDictionary *)msg.extra)[@"inner_uid"]).stringValue, @"time":@(msgModel.sentTime).stringValue, @"direction":@(0), @"type":@"User"};
                }
                else{
                    if ([[((NSDictionary *)msg.extra)[@"genius_info"] allKeys] containsObject:@"genius_id"]) {
                        dict = @{@"text":@"[语音]", @"id":((NSNumber *)((NSDictionary *)msg.extra)[@"genius_info"][@"genius_id"]).stringValue, @"time":@(msgModel.sentTime).stringValue, @"direction":@(1), @"type":@"Genius"};
                    } else {
                        dict = @{@"text":@"[语音]", @"id":@([SDJUserAccount loadAccount].genius_id).stringValue, @"time":@(msgModel.sentTime).stringValue, @"direction":@(1), @"type":@"Genius"};
                    }
                }
                [sessionArr addObject:dict];
            }
            else if ([msgModel.content isKindOfClass:[RCRichContentMessage class]]) {
                RCRichContentMessage *msg = (RCRichContentMessage *)msgModel.content;
                NSDictionary *dict = [NSDictionary dictionary];
                if ([msg.extra isKindOfClass:[NSString class]]) {
                    msg.extra = (NSString *)[WYTool jsonStringToDictionary:msg.extra];
                }
                if (msgModel.messageDirection == MessageDirection_RECEIVE) {
                    dict = @{@"text":@"[卡片]", @"id":((NSNumber *)((NSDictionary *)msg.extra)[@"inner_uid"]).stringValue, @"time":@(msgModel.sentTime).stringValue, @"direction":@(0), @"type":@"User"};
                }
                else{
                    if ([[((NSDictionary *)msg.extra)[@"genius_info"] allKeys] containsObject:@"genius_id"]) {
                        dict = @{@"text":@"[卡片]", @"id":((NSNumber *)((NSDictionary *)msg.extra)[@"genius_info"][@"genius_id"]).stringValue, @"time":@(msgModel.sentTime).stringValue, @"direction":@(1), @"type":@"Genius"};
                    } else {
                        dict = @{@"text":@"[卡片]", @"id":@([SDJUserAccount loadAccount].genius_id).stringValue, @"time":@(msgModel.sentTime).stringValue, @"direction":@(1), @"type":@"Genius"};
                    }
                }
                [sessionArr addObject:dict];
            }
            
        }
        NSDictionary *dic = @{@"session":sessionArr};
        NSMutableString *dictStr = [NSMutableString stringWithFormat:@"%@", [WYTool dictionaryToJson:dic]];
        NSString *lastStr = [NSString stringWithFormat:@"[%@]", dictStr];
        __weak WYChatRoomLoveViewController *weakSelf = self;
        [[WYNetworkTools shareTools] loveSessionWithSession:lastStr andGeniusId:@([SDJUserAccount loadAccount].genius_id).stringValue andTimeStamp:@((long long)[[NSDate date]timeIntervalSince1970] * 1000).stringValue andCategory:self.categoryStr andReason:@"" finished:^(id responseObject, NSError *error) {
            if (!error) {
                __block MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:weakSelf.view];
                [self.view addSubview:HUD];
                HUD.mode = MBProgressHUDModeText;
                HUD.label.text = @"操作成功";
                [HUD showAnimated:YES whileExecutingBlock:^{
                    sleep(2);
                } completionBlock:^{
                    [HUD removeFromSuperview];
                    HUD = nil;
                    [self.navigationController popViewControllerAnimated:YES];
                }];
            } else {
                __block MBProgressHUD *HUD = [[MBProgressHUD alloc] initWithView:weakSelf.view];
                [self.view addSubview:HUD];
                HUD.mode = MBProgressHUDModeText;
                HUD.label.text = @"操作失败";
                [HUD showAnimated:YES whileExecutingBlock:^{
                    sleep(2);
                } completionBlock:^{
                    [HUD removeFromSuperview];
                    HUD = nil;
                }];
            }
        }];
    }
}

-(void)closeLoveListTypeView{
    
    [self.loveListTypeView removeFromSuperview];
    self.loveListTypeView = nil;
}

-(void)changeCategoryStr:(NSString *)categoryStr{
    
    self.categoryStr = categoryStr;
}

#pragma mark --- 懒加载
- (WYChatHisMsgHeaderView *)chatHeaderView {
    
	if(_chatHeaderView == nil) {
		_chatHeaderView = [WYChatHisMsgHeaderView createChatHeaderView:self.connectListModel];
        _chatHeaderView.chatHeaderViewDelegate = self;
        [self.view addSubview:_chatHeaderView];
	}
	return _chatHeaderView;
}

- (UITableView *)loveListTabelView {
    
	if(_loveListTabelView == nil) {
		_loveListTabelView = [[UITableView alloc] init];
        _loveListTabelView.delegate = self;
        _loveListTabelView.dataSource = self;
        _loveListTabelView.backgroundColor = HEXCOLOR(0xf7f7f7);
        [_loveListTabelView registerClass:[WYLoveListCell class] forCellReuseIdentifier:@"loveListCell"];
        _loveListTabelView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _loveListTabelView.mj_header = [MJRefreshStateHeader headerWithRefreshingTarget:self refreshingAction:@selector(moreData)];
        [self.view addSubview:_loveListTabelView];
	}
	return _loveListTabelView;
}

- (UIButton *)cancelButton {
    
	if(_cancelButton == nil) {
		_cancelButton = [[UIButton alloc] init];
        [_cancelButton setTitle:@"取消" forState:UIControlStateNormal];
        [_cancelButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        _cancelButton.titleLabel.font = [UIFont systemFontOfSize:15];
        [_cancelButton setBackgroundImage:[UIImage getImageWithSize:CGSizeMake(110, 110) backgroundColor:HEXCOLOR(0xF7F7F7)] forState:UIControlStateDisabled];
        [_cancelButton addTarget:self action:@selector(backToChatRoom) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:_cancelButton];
	}
	return _cancelButton;
}

- (UIButton *)completeButton {
    
	if(_completeButton == nil) {
		_completeButton = [[UIButton alloc] init];
        [_completeButton setTitle:@"完成点赞" forState:UIControlStateNormal];
        [_completeButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _completeButton.titleLabel.font = [UIFont systemFontOfSize:15];
        _completeButton.enabled = NO;
        [_completeButton setBackgroundImage:[UIImage getImageWithSize:CGSizeMake(110, 110) backgroundColor:HEXCOLOR(0x4B9EFE)] forState:UIControlStateNormal];
        [_completeButton setBackgroundImage:[UIImage getImageWithSize:CGSizeMake(110, 110) backgroundColor:HEXCOLOR(0xCACACA)] forState:UIControlStateDisabled];
        [_completeButton addTarget:self action:@selector(completeLove) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:_completeButton];
	}
	return _completeButton;
}

- (NSMutableArray *)selectDataArray {
    
	if(_selectDataArray == nil) {
		_selectDataArray = [[NSMutableArray alloc] init];
	}
	return _selectDataArray;
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
}

- (WYLoveListTypeView *)loveListTypeView {
    
	if(_loveListTypeView == nil) {
		_loveListTypeView = [[WYLoveListTypeView alloc] initWithServiceTypeArr:self.serviceArr];
        _loveListTypeView.wyLoveListTypeViewDelegate = self;
        [self.view addSubview:_loveListTypeView];
	}
	return _loveListTypeView;
}

- (NSArray *)serviceArr {
    
    if(_serviceArr == nil) {
        _serviceArr = [WYLoveTypeModel createLoveTypeModelWithArrayWithCategory:self.categoryStr];
    }
    return _serviceArr;
}

- (UICollectionView *)sessionTypeCollectionView {
    
    if(_sessionTypeCollectionView == nil) {
        _sessionTypeFlowLayout = [[UICollectionViewFlowLayout alloc] init];
        _sessionTypeFlowLayout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
        _sessionTypeFlowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        _sessionTypeCollectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:_sessionTypeFlowLayout];
        _sessionTypeCollectionView.delegate = self;
        _sessionTypeCollectionView.dataSource = self;
        _sessionTypeCollectionView.backgroundColor = [UIColor whiteColor];
        [_sessionTypeCollectionView registerClass:[WYLoveCategoryCollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
        [self.view addSubview:_sessionTypeCollectionView];
    }
    return _sessionTypeCollectionView;
}

#pragma mark --- UICollectionViewDelegate
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return self.serviceArr.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    WYLoveCategoryCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    [cell cellWithTitle:self.serviceArr[indexPath.row]];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    WYLoveTypeModel *model = self.serviceArr[indexPath.row];
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont systemFontOfSize:12]};
    CGFloat width = [model.type boundingRectWithSize:CGSizeMake(2000, 320) options:NSStringDrawingUsesLineFragmentOrigin attributes:attributes context:nil].size.width;
    return CGSizeMake(width + 18, 24);
}

// cell点击变色
- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    //修改当前选中的模型和cell样式
    WYLoveTypeModel *statuModel = self.serviceArr[indexPath.row];
    //判断是不是第一个cell，如果是遍历后面cell和模型进行修改
    if (indexPath.row == 0) {
        for (int i = 1; i < self.serviceArr.count; i ++) {
            WYLoveTypeModel *serviceModel = self.serviceArr[i];
            if (serviceModel.selected) {
                serviceModel.selected = NO;
                [((WYLoveCategoryCollectionViewCell *)[collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]]) cellWithTitle:serviceModel];
            }
        }
    }
    else {//如果不是第一个cell，修改第一个cell样式
        if (((WYLoveTypeModel *)self.serviceArr[0]).selected) {
            ((WYLoveTypeModel *)self.serviceArr[0]).selected = NO;
            [((WYLoveCategoryCollectionViewCell *)[collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]]) cellWithTitle:self.serviceArr[0]];
        }
    }
    statuModel.selected = !statuModel.selected;
    [((WYLoveCategoryCollectionViewCell *)[collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:indexPath.row  inSection:0]]) cellWithTitle:statuModel];
    self.categoryStr = [self getServiceStatu];
}

- (UIView *)separateLineFirst {
    
	if(_separateLineFirst == nil) {
		_separateLineFirst = [[UIView alloc] init];
        _separateLineFirst.backgroundColor = [UIColor lightGrayColor];
        [self.view addSubview:_separateLineFirst];
	}
	return _separateLineFirst;
}

-(NSString *)getServiceStatu{
    
    NSString *str = @"";
    for (int i = 0; i < self.serviceArr.count; i ++) {
        WYLoveTypeModel *serviceModel = self.serviceArr[i];
        if (serviceModel.selected) {
            if (i == 0) {
                return @"不限";
            }
            else {
                str = [NSString stringWithFormat:@"%@，%@", str, serviceModel.type];
            }
        }
    }
    if (str.length > 0) {
        str = [str substringFromIndex:1];
    }
    else {
        str = @"不限";
    }
    return str;
}

- (UIView *)separateLineSecond {
    
	if(_separateLineSecond == nil) {
		_separateLineSecond = [[UIView alloc] init];
        _separateLineSecond.backgroundColor = [UIColor lightGrayColor];
        [self.view addSubview:_separateLineSecond];
	}
	return _separateLineSecond;
}

@end
