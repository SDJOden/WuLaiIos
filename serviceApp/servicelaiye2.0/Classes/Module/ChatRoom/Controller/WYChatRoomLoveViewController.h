//
//  WYChatRoomLoveViewController.h
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/7/5.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import <UIKit/UIKit.h>

@class WYConnectListModel;

@interface WYChatRoomLoveViewController : UIViewController

@property (nonatomic, strong) NSMutableArray *listDataArray;

-(instancetype)initWithConnectListModel:(WYConnectListModel *)connectListModel andPage:(NSInteger)page;

@end
