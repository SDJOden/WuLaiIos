
//  SDJChatRoomController.m
//  geniuslaiyeOC1.0
//
//  Created by 汪洋 on 15/10/8.
//  Copyright © 2015年 wangyang. All rights reserved.
//

#import "WXApi.h"

#import "SDJChatRoomController.h"
#import "SDJBaseWebViewController.h"
#import "SDJNormalWebController.h"
#import "SDJOriginalWebViewController.h"
#import "SBJson4.h"
#import "RCMessageCell.h"
#import "SDJTextMessageCell.h"
#import "SDJImageMessageCell.h"
#import "RCMessageModel.h"
#import "RCImagePreviewController.h"
#import "RCSystemSoundPlayer.h"
#import "RCImagePreviewController.h"
#import "RCSystemSoundPlayer.h"
#import "SDJCardEnlargeController.h"
#import "MBProgressHUD+MJ.h"
#import "SDJCardTypeOneCell.h"
#import "SDJCardTypeTwoCell.h"
#import "SDJCardTypeThreeCell.h"
#import "SDJCardTypeFourCell.h"
#import "SDJCardTypeFiveCell.h"
#import "SRRefreshView.h"
#import "RCImagePickerViewController.h"
#import "RCAlbumListViewController.h"
#import "RCAssetHelper.h"
#import "RCLocationMessageCell.h"
#import "RCLocationPickerViewController.h"
#import "RCLocationViewController.h"
#import "RCVoiceMessageCell.h"
#import "RCVoicePlayer.h"
#import "MBProgressHUD.h"
#import "JZTabBar.h"
#import "UIBarButtonItem+Extension.h"
#import "WYNetworkTools.h"
#import "WYConnectInformationModel.h"
#import "WYChangeGeniusViewController.h"
#import "WYConnectListModel.h"
#import "WYUnReadNumModel.h"
#import "WYSeparateCollectionViewCell.h"
#import "WYSeparateModel.h"
#import "WYChatHisMsgHeaderView.h"
#import "WYChatRoomPullDownMenu.h"
#import "WYChatRoomSugView.h"
#import "WYChatRoomLoveViewController.h"

#define ORDER_URL (CODESTATU?@"http://genius.g.laiye.com/user/index?user_id=":@"http://test.g.laiye.com/user/index?user_id=")

#define Height_IntelBoardView 180.0f/*238.0f*/
#define Height_EmojiBoardView 238.0f
#define Height_VoiceBoardView 238.0f
#define Height_PictureBoardView 190.0f/*238.0f*/
#define MAX_PICKER_NUMBER 200

#import "SBJson4.h"
#import "VoiceConverter.h"

typedef NS_ENUM(NSInteger, KBottomBarStatus) {
    KBottomBarDefaultStatus = 0,
    KBottomBarKeyboardStatus,
    KBottomBarSugStatus,
    KBottomBarEmojiStatus,
    KBottomBarPictureStatus,
    KBottomBarNull,
};

@interface SDJChatRoomController () <UICollectionViewDelegate, UICollectionViewDataSource,
UICollectionViewDelegateFlowLayout, RCEmojiViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate, RCMessageCellDelegate,RCChatSessionInputBarControlDelegate, UIGestureRecognizerDelegate,
UIScrollViewDelegate,SDJIntelligentBoardViewDelegate, UITextFieldDelegate, SRRefreshDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, RCImagePickerViewControllerDelegate, RCAlbumListViewControllerDelegate, RCLocationPickerViewControllerDelegate, AVAudioRecorderDelegate, UIWebViewDelegate, WYChatHisMsgHeaderViewDelegate, WYMessageCellDelegate, WYChatRoomSugViewDelegate>

@property(nonatomic, assign) KBottomBarStatus currentBottomBarStatus;
@property(nonatomic, strong) RCMessageModel *longPressSelectedModel;
@property(nonatomic, assign) BOOL isConversationAppear;
@property(nonatomic, strong) RCMessage *tempMessage;
@property(nonatomic, assign) BOOL isNeedScrollToButtom;
@property (nonatomic, strong) UIImageView *unreadRightBottomIcon;
@property (nonatomic, assign) NSInteger unreadNewMsgCount;
@property (nonatomic, assign) NSInteger scrollNum;
@property (nonatomic, assign) NSInteger sendOrReciveMessageNum;//记录新收到和自己新发送的消息数，用于计算加载历史消息时插入“以上是历史消息”cell 的位置
@property (nonatomic, strong) MASConstraint *chatControlBottomConstraint;
@property (nonatomic, strong) SRRefreshView *slimeView;
@property (nonatomic, assign) int refreshFlag;
@property (nonatomic, assign) int receiveMessageFlag;
@property (nonatomic, assign) int testCardflag;
@property (nonatomic, strong) UIImagePickerController *currentPicker;

@property (nonatomic, assign) NSInteger pageCount;
@property (nonatomic, assign) BOOL isFirstHistory;
@property (nonatomic, assign) int downloadFlag;
@property (nonatomic, assign) int imageMessageCount;
@property (nonatomic, assign) NSInteger dataNumber;
@property (nonatomic, strong) WYConnectInformationModel *informationModel;
@property (nonatomic, strong) UIWebView *orderWebView;

@property (nonatomic, strong) WYChatHisMsgHeaderView *chatHeaderView;
@property (nonatomic, strong) WYChatRoomPullDownMenu *menuView;
@property (nonatomic, assign) BOOL isMoreSelect;
@property (nonatomic, strong) NSMutableArray *cellSelectedArr;
@property (nonatomic, strong) UIButton *shareToWechatButton;
@property (nonatomic, copy) NSString *categoryStr;
@property (nonatomic, copy) NSString *reasonStr;
@property (nonatomic, assign) CGFloat changeY;
@property (nonatomic, strong) WYChatRoomSugView *chatRoomSugView;
@property (nonatomic, assign) BOOL isRefresh;
@property (nonatomic, assign) BOOL isBack;

@end

static NSString *const sdjtextCellIndentifier = @"sdjtextCellIndentifier";
static NSString *const sdjimageCellIndentifier = @"sdjimageCellIndentifier";
static NSString *const sdjCardTypeOneCellIndentifier = @"sdjCardTypeOneCellIndentifier";
static NSString *const sdjCardTypeTwoCellIndentifier = @"sdjCardTypeTwoCellIndentifier";
static NSString *const sdjCardTypeThreeCellIndentifier = @"sdjCardTypeThreeCellIndentifier";
static NSString *const sdjCardTypeFourCellIndentifier = @"sdjCardTypeFourCellIndentifier";
static NSString *const sdjCardTypeFiveCellIndentifier = @"sdjCardTypeFiveCellIndentifier";
static NSString *const sdjlocationCellIndentifier = @"sdjlocationCellIndentifier";
static NSString *const sdjvoiceCellIndentifier = @"sdjvoiceCellIndentifier";
static NSString *const wyseparateCellIndentifier = @"wyseparateCellIndentifier";

@implementation SDJChatRoomController

- (id)initWithConversationType:(RCConversationType)conversationType targetId:(NSString *)targetId connectListModel:(WYConnectListModel *)connectListModel{
    
    self = [super init];
    if (self) {
        [self initialization];
        self.conversationType = conversationType;
        self.targetId = targetId;
        self.connectListModel = connectListModel;
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initialization];
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self initialization];
    }
    return self;
}

- (void)initialization {
    
    _isBack = NO;
    _isWaitngContect = NO;
    _isRefresh = NO;
    self.isFirstHistory = NO;
    _dataNumber = 0;
    _downloadFlag = 0;
    _imageMessageCount = 0;
    _isConversationAppear = NO;
    self.conversationDataRepository = [[NSMutableArray alloc]init];
    self.conversationMessageCollectionView = nil;
    self.targetId = nil;
    self.userName = nil;
    self.currentBottomBarStatus = KBottomBarDefaultStatus;
    self.defaultInputType = RCChatSessionInputBarInputText;
    self.defaultHistoryMessageCountOfChatRoom = 10;
    _enableNewComingMessageIcon = YES;
    _enableUnreadMessageIcon = YES;
    _isMoreSelect = NO;
    _categoryStr = @"";
    _reasonStr = @"";
    
    //注册接收消息
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveMessageNotification:) name:RCKitDispatchMessageNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(closeInputSugView) name:@"removeSugView" object:nil];
}

//改变右上角数字
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    
    WYUnReadNumModel *unReadNumModel = (WYUnReadNumModel *)object;
    if (unReadNumModel.inner_uid == self.connectListModel.inner_uid) {
        return;
    }
    if (unReadNumModel.changeMore) {
        self.unReadNumStr = @(self.unReadNumStr.integerValue + 1).stringValue;
    }
    else {
        self.unReadNumStr = @(self.unReadNumStr.integerValue - 1).stringValue;
        
    }
    
    self.navigationItem.leftBarButtonItem = nil;
    UIButton *leftBtn = (UIButton *)self.navigationItem.leftBarButtonItem.customView;
    if ([self.unReadNumStr isEqualToString:@"0"]) {
        self.navigationItem.leftBarButtonItem = [UIBarButtonItem itemWithTitle:@"已接入" target:self action:@selector(popViewControllerToConnect) left:YES];
    }else {
        self.navigationItem.leftBarButtonItem = [UIBarButtonItem itemWithTitle:[NSString stringWithFormat:@"(%d)已接入", self.unReadNumStr.intValue] target:self action:@selector(popViewControllerToConnect) left:YES];
    }
    [leftBtn sizeToFit];
}

- (void)registerClass:(Class)cellClass forCellWithReuseIdentifier:(NSString *)identifier {
    
    [self.conversationMessageCollectionView registerClass:cellClass forCellWithReuseIdentifier:identifier];
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    self.scrollNum = 0;

    self.chatSessionInputBarControl.inputTextView.text = @"";
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    
    _isConversationAppear = YES;
    [[RCIMClient sharedRCIMClient] clearMessagesUnreadStatus:self.conversationType
                                                    targetId:self.targetId];
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationStopVoicePlayer object:nil];
    [[RCSystemSoundPlayer defaultPlayer] resetIgnoreConversation]; // 停止语音播放器
    [[RCIMClient sharedRCIMClient] clearMessagesUnreadStatus:self.conversationType
                                                    targetId:self.targetId];
    _isConversationAppear = NO;
    
    if (_isBack) {
        UIWindow *window = [UIApplication sharedApplication].keyWindow;
        UIViewController *vc = window.rootViewController.childViewControllers[0].childViewControllers[0];
        vc.tabBarController.selectedIndex = 0;
        for (UIView *view in vc.tabBarController.tabBar.subviews) {
            if ([view isKindOfClass:[JZTabbar class]]) {
                [(JZTabbar *)view selectItemWithIndex:1];
                break;
            }
        }
    }
}

-(void)viewDidDisappear:(BOOL)animated{
    
    [super viewDidDisappear:animated];
}

-(void)setConnectListModel:(WYConnectListModel *)connectListModel{
    
    _connectListModel = connectListModel;
    self.informationModel = [connectListModel.informationModel copy];
    self.pageCount = connectListModel.page;
    [self.conversationDataRepository addObjectsFromArray:connectListModel.historyArray];
    
    for (RCMessageModel *msgModel in connectListModel.messageArray) {
        RCMessageContent *msgContent = msgModel.content;
        if ([msgContent isKindOfClass:[RCTextMessage class]]) {
            if ([((RCTextMessage *)msgContent).extra isKindOfClass:[NSString class]]) {
                NSDictionary *extraDict = [WYTool jsonStringToDictionary:((RCTextMessage *)msgContent).extra];
                ((RCTextMessage *)msgContent).content = extraDict[@"msg_detail"][@"content"];
                ((RCTextMessage *)msgContent).extra = (NSString *)extraDict;
            }
        }
        else if ([msgContent isKindOfClass:[RCImageMessage class]]) {
            if ([((RCImageMessage *)msgContent).extra isKindOfClass:[NSString class]]) {
                NSDictionary *extraDict = [WYTool jsonStringToDictionary:((RCImageMessage *)msgContent).extra];
                UIImageView *imageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"placeholder"]];
                ((RCImageMessage *)msgContent).originalImage = imageView.image;
                ((RCImageMessage *)msgContent).thumbnailImage = imageView.image;
                [imageView sd_setImageWithURL:[NSURL URLWithString:((RCImageMessage *)msgContent).imageUrl]placeholderImage:[UIImage imageNamed:@"placeholder"] options:SDWebImageRetryFailed completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                    if (!error) {
                        ((RCImageMessage *)msgContent).originalImage = imageView.image;
                        ((RCImageMessage *)msgContent).thumbnailImage = imageView.image;
                    }
                }];
                ((RCImageMessage *)msgContent).extra = (NSString *)extraDict;
            }
        }
        else if ([msgContent isKindOfClass:[RCVoiceMessage class]]) {
            if ([((RCVoiceMessage *)msgContent).extra isKindOfClass:[NSString class]]) {
                NSDictionary *extraDict = [WYTool jsonStringToDictionary:((RCVoiceMessage *)msgContent).extra];
                //声音文件地址
                NSString *audioStr = extraDict[@"msg_detail"][@"voice_url"];
                NSURL *url = [[NSURL alloc]initWithString:audioStr];
                NSData * wavData = [VoiceConverter amrToWavWithAmrData:[NSData dataWithContentsOfURL:url]];
                ((RCVoiceMessage *)msgContent).wavAudioData = wavData;
            }
        }
    }
    [self.conversationDataRepository addObjectsFromArray:connectListModel.messageArray];
    self.title = connectListModel.nickname;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self.unReadNumModel addObserver:self forKeyPath:@"totalUnReadNum" options:NSKeyValueObservingOptionNew context:nil];
    
    [[RCIM sharedRCIM] setUserIcon:[UIImage imageNamed:@"chatroom_userIcon_xiaolai"]];
    [[RCIM sharedRCIM] setGeniusIcon:self.headImage];
    
    NSString *str = self.unReadNumStr.integerValue == 0 ? @"已接入":[NSString stringWithFormat:@"(%d)已接入", self.unReadNumStr.intValue];
    UIBarButtonItem *leftItem = [UIBarButtonItem itemWithTitle:str target:self action:@selector(popViewControllerToConnect) left:YES];
    self.navigationItem.leftBarButtonItem = leftItem;
    
    UIBarButtonItem *rightItem = [UIBarButtonItem itemWithTitle:@"操作" target:self action:@selector(operateMenuList) left:NO];
    self.navigationItem.rightBarButtonItem = rightItem;
    
    NSString *group = @"rc_group_";
    
    NSString *targetid = [group stringByAppendingFormat:@"%ld",[SDJUserAccount loadAccount].user_id];
    
    [[RCIMClient sharedRCIMClient] setConversationNotificationStatus:ConversationType_GROUP targetId:targetid isBlocked:YES success:^(RCConversationNotificationStatus nStatus) {
        
    } error:^(RCErrorCode status) {
        
    }];
    
    if (IOS_FSystenVersion >= 7.0) {
        self.extendedLayoutIncludesOpaqueBars = YES;
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    [self initializedSubViews];
    
    //加载历史数据
    if (!(self.conversationType == ConversationType_CHATROOM)) {
        if (self.connectListModel.historyArray.count == 0) {
            [self loadLatestHistoryMessage];
        }
        else {
            //根据最后一条消息的来源修改informationModel的值 -> self.conversationDataRepository
            for (NSInteger i = self.conversationDataRepository.count - 1; i >= 0; i --) {
                RCMessageModel *msgModel = self.conversationDataRepository[i];
                //                NSLog(@"%@", msgModel);
                if (msgModel.messageDirection == 2) {
                    if ([msgModel.content isKindOfClass:[RCTextMessage class]]) {
                        self.informationModel = [WYTool modelWithHistoryDict:((RCTextMessage *)msgModel.content).extra];
                    }
                    else if ([msgModel.content isKindOfClass:[RCImageMessage class]]) {
                        self.informationModel = [WYTool modelWithHistoryDict:((RCImageMessage *)msgModel.content).extra];
                    }
                    break;
                }
            }
            
            //检索self.conversationDataRepository修改sug键盘
            for (NSInteger i = self.conversationDataRepository.count - 1; i >= 0; i --) {
                RCMessageModel *msgModel = self.conversationDataRepository[i];
                //                NSLog(@"%@", msgModel);
                if (msgModel.messageDirection == 1) {
                    continue;
                }
                if ([msgModel.content isKindOfClass:[RCTextMessage class]]) {
                    NSArray *sugListArray = ((NSDictionary *)((RCTextMessage *)msgModel.content).extra)[@"msg_detail"][@"sug_list"];
                    NSMutableArray *inteligentMessage = [NSMutableArray array];
                    for (NSDictionary *sugListDict in sugListArray) {
                        [inteligentMessage addObject:sugListDict[@"sug_detail"]];
                    }
                    self.sugView.inteligentMessages = inteligentMessage;
                    break;
                }
            }
            self.connectListModel.informationModel = [self.informationModel copy];
        }
    }
    if (ConversationType_CHATROOM == self.conversationType) {
        [[RCIMClient sharedRCIMClient] joinChatRoom:self.targetId messageCount:self.defaultHistoryMessageCountOfChatRoom success:^{} error:^(RCErrorCode status) {
            __weak typeof(self) weakSelf = self;
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakSelf loadErrorAlert:NSLocalizedString(@"JoinChatRoomFailed", @"")];
            });
        }];
    }
    
    [[RCIMClient sharedRCIMClient] clearMessagesUnreadStatus:self.conversationType targetId:self.targetId];
    [self layoutBottomBarWithStatus:KBottomBarDefaultStatus];
}

- (void)loadErrorAlert:(NSString *)title {
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:nil];
    [NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(cancelAlertAndGoBack:) userInfo:alert repeats:NO];
    [alert show];
}

- (void)cancelAlertAndGoBack:(NSTimer *)scheduledTimer {
    
    UIAlertView *alert = (UIAlertView *)(scheduledTimer.userInfo);
    [alert dismissWithClickedButtonIndex:0 animated:NO];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)cleanTextView{
    
    [self closeInputSugView];
    self.chatSessionInputBarControl.placehoderLabel.hidden = NO;
}

//------------------------------- 输入sug ------------------------------
-(void)chatInputTextSugText:(NSString *)inputText{
    
    if (inputText.length != 0) {
        [[WYNetworkTools shareTools]chatNewInputTextSugTextWithQuery:inputText andUser_id:@(self.connectListModel.inner_uid).stringValue andGenius_id:@([SDJUserAccount loadAccount].genius_id).stringValue source:@(self.connectListModel.informationModel.user_src).stringValue finished:^(id responseObject, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                if (responseObject) {
                    [self closeInputSugView];
                    if ([responseObject[@"rec_list"] isKindOfClass:[NSArray class]] && self.chatSessionInputBarControl.inputTextView.text.length != 0) {
                        [self.chatRoomSugView setContentArr:responseObject[@"rec_list"]];
                    }
                } else {
                    [self closeInputSugView];
                }
            });
        }];
    }
}

-(void)popViewControllerToConnect{
    
    _isBack = YES;
    [MobClick event:@"chatroom_ruturn_click"];
    RCMessageModel *model = self.conversationDataRepository.lastObject;
    int service_id = 0;
    NSString *timeStr = [NSString string];
    for (NSInteger i = self.conversationDataRepository.count - 1; i >= 0; i --) {
        RCMessageModel *msgModel = self.conversationDataRepository[i];
        if (msgModel.messageDirection == 2) {
            if ([msgModel.content isKindOfClass:[RCTextMessage class]]) {
                if (![((RCTextMessage *)msgModel.content).extra isKindOfClass:[NSDictionary class]]) {
                    ((RCTextMessage *)msgModel.content).extra = (NSString *)[WYTool jsonStringToDictionary:((RCTextMessage *)msgModel.content).extra];
                }
                NSArray *sugDomainArr = ((NSDictionary *)((RCTextMessage *)msgModel.content).extra)[@"msg_detail"][@"sug_domain"];
                if (sugDomainArr.count > 0) {
                    for (NSDictionary *dict in sugDomainArr) {
                        if (((NSNumber *)dict[@"service_id"]).intValue < 10005) {
                            service_id = ((NSNumber *)dict[@"service_id"]).intValue;
                            break;
                        }
                    }
                    break;
                }
                
            }
        }
    }
    for (NSInteger i = self.conversationDataRepository.count - 1; i >= 0; i --) {
        RCMessageModel *msgModel = self.conversationDataRepository[i];
        if (msgModel.messageDirection == 2) {
            if ([msgModel.content isKindOfClass:[RCTextMessage class]] && ![msgModel.content isKindOfClass:[WYSeparateModel class]]) {
                timeStr = [self computeTime:msgModel.sentTime/1000];
                break;
            }
        }
    }
    
    //发通知隐藏红点
    NSDictionary *selectedDict = @{@"innerUid":@(self.connectListModel.inner_uid),@"messageDirection":@(model.messageDirection - 1), @"ts":timeStr,@"service_id":@(service_id)};
    [[NSNotificationCenter defaultCenter]postNotificationName:@"hiddenCellCount" object:nil userInfo:selectedDict];
    
    if (_isWaitngContect) {
        [self.navigationController popToRootViewControllerAnimated:YES];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

-(void)operateMenuList{
    
    UIButton *rightBtn = (UIButton *)self.navigationItem.rightBarButtonItem.customView;
    rightBtn.selected = !rightBtn.selected;
    [rightBtn setTitle:@"操作" forState:UIControlStateNormal];
    [rightBtn setTitle:@"取消" forState:UIControlStateSelected];
    rightBtn.selected ? [self.chatHeaderView changeSubViewStyle] : [self.chatHeaderView restoreSubViewStyle:self.connectListModel];
}

-(NSString *)computeTime:(long)ts{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    dateFormatter.dateFormat = @"HH:mm:ss";//大写HH是24小时制，小写hh是12小时制
    NSDate *tsData = [NSDate dateWithTimeIntervalSince1970:ts];
    return [dateFormatter stringFromDate:tsData];
}

-(void)refreshDialog{
    
    self.pageCount = 0;
    [self.connectListModel.messageArray removeAllObjects];
    [self.connectListModel.historyArray removeAllObjects];
    [self.conversationDataRepository removeAllObjects];
    [self loadLatestHistoryMessage];
}

-(void)changeGenius{
    
    WYChangeGeniusViewController *changeGeniusViewController = [[WYChangeGeniusViewController alloc]init];
    changeGeniusViewController.inner_uid = @(self.connectListModel.inner_uid).stringValue;
    changeGeniusViewController.hidesBottomBarWhenPushed = YES;
    changeGeniusViewController.navigationController.navigationBarHidden = NO;
    [self.navigationController pushViewController:changeGeniusViewController animated:YES];
}

-(void)createOrder:(UIButton *)button{
    
    [self.view endEditing:YES];
    [self.chatHeaderView changeOrderBtnTitle];
    if ([button.titleLabel.text isEqualToString:@"下单"]) {
        self.orderWebView.transform = CGAffineTransformMakeTranslation(- self.view.frame.size.width, 0);
    }
    else{
        self.orderWebView.transform = CGAffineTransformMakeTranslation(self.view.frame.size.width, 0);
    }
}

#pragma mark - 懒加载
- (RCEmojiBoardView *)emojiView {
    
    if (!_emojiView) {
        _emojiView = [[RCEmojiBoardView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, Height_EmojiBoardView)];
        _emojiView.delegate = self;
        [self.view addSubview:_emojiView];
    }
    return _emojiView;
}

- (UIImageView *)unreadRightBottomIcon {
    
    if (!_unreadRightBottomIcon) {
        UIImage *msgCountIcon = [RCKitUtility imageNamed:@"conversation_item_unreadcount_icon" ofBundle:@"RongCloud.bundle"];
        _unreadRightBottomIcon = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, msgCountIcon.size.width, msgCountIcon.size.height)];
        _unreadRightBottomIcon.userInteractionEnabled = YES;
        _unreadRightBottomIcon.image = msgCountIcon;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tabRightBottomMsgCountIcon:)];
        [_unreadRightBottomIcon addGestureRecognizer:tap];
        _unReadNewMessageLabel = [[UILabel alloc]initWithFrame:self.unreadRightBottomIcon.bounds];
        _unReadNewMessageLabel.backgroundColor = [UIColor clearColor];
        _unReadNewMessageLabel.font = [UIFont systemFontOfSize:12.0f];
        _unReadNewMessageLabel.textAlignment = NSTextAlignmentCenter;
        _unReadNewMessageLabel.textColor = [UIColor whiteColor];
        _unReadNewMessageLabel.center = CGPointMake(self.unReadNewMessageLabel.frame.size.width/2, self.unReadNewMessageLabel.frame.size.height/2 - 2 );
        [_unreadRightBottomIcon addSubview:self.unReadNewMessageLabel];
        _unreadRightBottomIcon.hidden = YES;
        [self.view addSubview:_unreadRightBottomIcon];
        [self.view bringSubviewToFront:_unreadRightBottomIcon];
    }
    return _unreadRightBottomIcon;
}

- (SDJIntelligentBoardView *)sugView {
    
    if (!_sugView) {
        _sugView = [[SDJIntelligentBoardView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, Height_IntelBoardView) andMessageArray: nil];
        _sugView.delegate = self;
        [self.view addSubview:_sugView];
    }
    return _sugView;
}

- (RCChatSessionInputBarControl *)chatSessionInputBarControl {
    
    if (!_chatSessionInputBarControl) {
        _chatSessionInputBarControl = [[RCChatSessionInputBarControl alloc]initWithFrame:CGRectMake(0, self.view.bounds.size.height - Height_ChatSessionInputBar, self.view.bounds.size.width, Height_ChatSessionInputBar) withContextView:self.view type:0 style:RC_CHAT_INPUT_BAR_STYLE_SWITCH_CONTAINER_EXTENTION];
        _chatSessionInputBarControl.delegate = self;
        [self.view addSubview:_chatSessionInputBarControl];
    }
    return _chatSessionInputBarControl;
}


- (UICollectionView *)conversationMessageCollectionView {
    
    if (!_conversationMessageCollectionView) {
        _customFlowLayout = [[UICollectionViewFlowLayout alloc] init];
        _customFlowLayout.minimumLineSpacing = 0.0f;
        _customFlowLayout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
        _customFlowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
        _conversationMessageCollectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:self.customFlowLayout];
        [_conversationMessageCollectionView setBackgroundColor:HEXCOLOR(0xf6f6f6)];// MARK 单聊背景色
        _conversationMessageCollectionView.showsHorizontalScrollIndicator = NO;
        _conversationMessageCollectionView.alwaysBounceVertical = YES;
        _slimeView = [[SRRefreshView alloc] init];
        _slimeView.delegate = self;
        _slimeView.upInset = 0;
        _slimeView.slimeMissWhenGoingBack = YES;
        _slimeView.slime.bodyColor = [UIColor blackColor];
        _slimeView.slime.skinColor = [UIColor whiteColor];
        _slimeView.slime.lineWith = 1;
        _slimeView.slime.shadowBlur = 4;
        _slimeView.slime.shadowColor = [UIColor blackColor];
        [_conversationMessageCollectionView addSubview:_slimeView];
        _slimeView.frame = CGRectMake(0, -40, self.view.bounds.size.width, 40);
        
        [self registerClass:[SDJTextMessageCell class] forCellWithReuseIdentifier:sdjtextCellIndentifier];
        [self registerClass:[SDJImageMessageCell class] forCellWithReuseIdentifier:sdjimageCellIndentifier];
        [self registerClass:[RCLocationMessageCell class] forCellWithReuseIdentifier:sdjlocationCellIndentifier];
        [self registerClass:[RCVoiceMessageCell class] forCellWithReuseIdentifier:sdjvoiceCellIndentifier];
        [self registerClass:[SDJCardTypeOneCell class] forCellWithReuseIdentifier:sdjCardTypeOneCellIndentifier];
        [self registerClass:[SDJCardTypeTwoCell class] forCellWithReuseIdentifier:sdjCardTypeTwoCellIndentifier];
        [self registerClass:[SDJCardTypeThreeCell class] forCellWithReuseIdentifier:sdjCardTypeThreeCellIndentifier];
        [self registerClass:[SDJCardTypeFourCell class] forCellWithReuseIdentifier:sdjCardTypeFourCellIndentifier];
        [self registerClass:[SDJCardTypeFiveCell class] forCellWithReuseIdentifier:sdjCardTypeFiveCellIndentifier];
        [self registerClass:[WYSeparateCollectionViewCell class] forCellWithReuseIdentifier:wyseparateCellIndentifier];
        
        self.conversationMessageCollectionView.dataSource = self;
        self.conversationMessageCollectionView.delegate = self;
        [self.view addSubview:_conversationMessageCollectionView];
        
        // 点击聊天页滚到底部的tap手势
        UITapGestureRecognizer *resetBottomTapGesture =
        [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap4ResetDefaultBottomBarStatus:)];
        [resetBottomTapGesture setDelegate:self];
        [_conversationMessageCollectionView addGestureRecognizer:resetBottomTapGesture];
        
    }
    return _conversationMessageCollectionView;
}

- (WYUnReadNumModel *)unReadNumModel {
    
    if(_unReadNumModel == nil) {
        _unReadNumModel = [[WYUnReadNumModel alloc] init];
    }
    return _unReadNumModel;
}

- (WYChatHisMsgHeaderView *)chatHeaderView {
    
    if(_chatHeaderView == nil) {
        _chatHeaderView = [WYChatHisMsgHeaderView createChatHeaderView:self.connectListModel];
        _chatHeaderView.chatHeaderViewDelegate = self;
        [self.view addSubview:_chatHeaderView];
    }
    return _chatHeaderView;
}

- (WYChatRoomPullDownMenu *)menuView {
    
    if(_menuView == nil) {
        _menuView = [[WYChatRoomPullDownMenu alloc] init];
        _menuView.model = self.connectListModel;
        [self.view addSubview:_menuView];
    }
    return _menuView;
}

#pragma mark --- WYChatHeaderViewDelegate
-(void)titleMenuClick:(UIButton *)button{
    
    switch (button.tag) {
        case 0:
            _isBack = YES;
            [self.navigationController popToRootViewControllerAnimated:YES];
            [MobClick event:@"chatroom_clsoe_click"];
            break;
        case 1:
            [self changeGenius];
            [MobClick event:@"chatroom_changeGenius_click"];
            break;
        case 2:
            [self createOrder:button];
            [MobClick event:@"chatroom_order_click"];
            break;
        case 3:
            [self refreshDialog];
            [MobClick event:@"chatroom_refresh_click"];
            break;
        default:
            break;
    }
}

-(void)changeUserInfo{
    
    [[SDJNetworkTools sharedTools]userInformation:[SDJUserAccount loadAccount].token user_id:@(self.connectListModel.inner_uid).stringValue force:@"1" finished:^(NSDictionary<NSString *,id> * _Nullable result, NSError * _Nullable error) {
        if (![result[@"error"] isEqual:[NSNull null]]) {//异端登陆
            NSNumber *code = result[@"error"][@"error_code"];
            if (code.integerValue == 20000) {
                NSString *UserPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).firstObject stringByAppendingPathComponent:@"account.plist"];
                [[NSFileManager defaultManager]removeItemAtPath:UserPath error:nil];
                [[RCIMClient sharedRCIMClient] logout];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"WYRootViewControllerSwitchNotification" object:@"0"];
                });
            }
        }
        else {
            [self.connectListModel refreshWithDict:result[@"data"]];
            self.informationModel = [self.connectListModel.informationModel copy];
        }
    }];
}

-(void)didTouchShowUserInformationButton:(BOOL)isShow{
    
    if (isShow) {
        [UIView animateWithDuration:0.3 animations:^{
            self.menuView.transform = CGAffineTransformMakeTranslation(0, self.menuView.h);
        }];
    }else {
        [UIView animateWithDuration:0.3 animations:^{
            self.menuView.transform = CGAffineTransformIdentity;
        }];
    }
}

#pragma mark --- WYInputSugViewDelegate
-(void)inputSugTextToTextField:(NSString *)sugText{

    RCTextMessage *rcTextMessage = [RCTextMessage messageWithContent:sugText];
    [self sendMessage:rcTextMessage pushContent:nil andText:sugText];
    self.chatSessionInputBarControl.inputTextView.text= nil;
    [self closeInputSugView];
}

-(void)closeInputSugView{
    
    self.chatSessionInputBarControl.placehoderLabel.hidden = YES;
    [self.chatRoomSugView removeFromSuperview];
    self.chatRoomSugView = nil;
}

- (void)initializedSubViews {
    
    [self.chatSessionInputBarControl makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        _chatControlBottomConstraint = make.bottom.equalTo(self.view);
        make.height.equalTo(Height_ChatSessionInputBar);
    }];
    
    [self.conversationMessageCollectionView makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(64+44);
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(_chatSessionInputBarControl.top);
    }];
    
    [self.menuView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.chatHeaderView.bottom).offset(-140);
        make.left.right.equalTo(self.view);
        make.height.equalTo(140);
    }];
    
    [self.chatHeaderView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(64);
        make.left.right.equalTo(self.view);
        make.height.equalTo(44);
    }];
    
    [self.unreadRightBottomIcon makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.view).offset(-20);
        make.bottom.equalTo(self.view).offset(-10 - Height_ChatSessionInputBar);
        make.height.width.equalTo(37);
    }];
    
    [self.sugView makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view);
        make.bottom.equalTo(self.view).offset(Height_IntelBoardView);
        make.height.equalTo(Height_IntelBoardView);
        make.width.equalTo(self.view);
    }];
    
    [self.emojiView makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(self.view);
        make.bottom.equalTo(self.view).offset(Height_EmojiBoardView);
        make.height.equalTo(Height_EmojiBoardView);
        make.width.equalTo(self.view);
    }];
    
    [self.view layoutIfNeeded];
    
    [self slimeRefreshStartRefresh:_slimeView];
    
    _currentBottomBarStatus = KBottomBarDefaultStatus;
    
    if (_currentBottomBarStatus != KBottomBarDefaultStatus) {
        [self layoutBottomBarWithStatus:KBottomBarDefaultStatus];
    }
}

- (void)setIntelligentViewWithArray:(NSArray *)intelligentMessages {
    
    self.sugView.inteligentMessages = intelligentMessages;
}

#pragma mark - slimeRefresh delegate
- (void)slimeRefreshStartRefresh:(SRRefreshView *)refreshView
{
    [_slimeView performSelector:@selector(endRefresh) withObject:nil afterDelay:3 inModes:[NSArray arrayWithObject:NSRunLoopCommonModes]];
}

- (void)dealloc {
    
    self.conversationMessageCollectionView.dataSource = nil;
    self.conversationMessageCollectionView.delegate = nil;
    [self.unReadNumModel removeObserver:self forKeyPath:@"totalUnReadNum"];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark UIPickerViewDatasource
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    
    return 3;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
    switch ((int)component) {
        case 0:
            return 10 * MAX_PICKER_NUMBER;
            break;
        case 1:
            return 5 * MAX_PICKER_NUMBER;
            break;
        case 2:
            return 8 * MAX_PICKER_NUMBER; // 从字典或本地解析
            break;
        default:
            return 0;
            break;
    }
}

#pragma mark <UIScrollViewDelegate>
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    
    if (_currentBottomBarStatus != KBottomBarDefaultStatus) {
        [self layoutBottomBarWithStatus:KBottomBarDefaultStatus];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    // 是否显示右下未读icon
    if (self.enableNewComingMessageIcon == YES || self.unreadNewMsgCount != 0) {
        [self checkVisiableCell];
    }
    if (_refreshFlag == 0) {
        _refreshFlag ++;
        return;
    }
    if (_refreshFlag >= 1) {
        [_slimeView scrollViewDidScroll];
        if (scrollView.contentOffset.y < -5.0f) {
            [self slimeRefreshStartRefresh:_slimeView];
        } else {
            [_slimeView endRefresh];
        }
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    
    [_slimeView scrollViewDidEndDraging];
    if (scrollView.contentOffset.y < -15) {
        [self performSelector:@selector(loadMoreHistoryMessage) withObject:nil afterDelay:0.4f];
    }
}

- (void)scrollToBottomAnimated:(BOOL)animated {
    
    NSUInteger finalRow = MAX(0, [self.conversationMessageCollectionView numberOfItemsInSection:0] - 1);
    
    if (0 == finalRow) {return;}
    
    NSIndexPath *finalIndexPath = [NSIndexPath indexPathForItem:finalRow inSection:0];
    
    [self.conversationMessageCollectionView scrollToItemAtIndexPath:finalIndexPath atScrollPosition:UICollectionViewScrollPositionBottom animated:animated];
}

#pragma mark <UICollectionViewDataSource>
//定义展示的UICollectionViewCell的个数
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return self.conversationDataRepository.count;
}

//每个UICollectionView展示的内容
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    RCMessageModel *model = [self.conversationDataRepository objectAtIndex:indexPath.row];
    
    RCMessageContent *messageContent = model.content;
    
    RCMessageCell *cell = nil;
    
    if ([messageContent isMemberOfClass:[RCTextMessage class]]) {
        
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:sdjtextCellIndentifier forIndexPath:indexPath];
        
        [cell setDataModel:model with:_isMoreSelect];
        [cell setDelegate:self];
        
    } else if ([messageContent isMemberOfClass:[RCImageMessage class]]) {
        
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:sdjimageCellIndentifier
                                                         forIndexPath:indexPath];
        [cell setDataModel:model with:_isMoreSelect];
        [cell setDelegate:self];
        
    } else if ([messageContent isMemberOfClass:[RCLocationMessage class]]) {
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:sdjlocationCellIndentifier
                                                         forIndexPath:indexPath];
        [cell setDataModel:model with:_isMoreSelect];
        [cell setDelegate:self];
        
    } else if ([messageContent isMemberOfClass:[RCVoiceMessage class]]) {
        cell = [collectionView
                dequeueReusableCellWithReuseIdentifier:sdjvoiceCellIndentifier
                forIndexPath:indexPath];
        [cell setDataModel:model with:_isMoreSelect];
        [cell setDelegate:self];
        
    } else if ([messageContent isMemberOfClass:[RCRichContentMessage class]]) {
        
        RCRichContentMessage *message = (RCRichContentMessage *)messageContent;
        
        int title = [message.title intValue];
        
        switch (title) {
            case 6:
                cell = [collectionView dequeueReusableCellWithReuseIdentifier:sdjCardTypeOneCellIndentifier forIndexPath:indexPath];
                break;
            case 7:
                cell = [collectionView dequeueReusableCellWithReuseIdentifier:sdjCardTypeTwoCellIndentifier forIndexPath:indexPath];
                break;
            case 8:
                cell = [collectionView dequeueReusableCellWithReuseIdentifier:sdjCardTypeFiveCellIndentifier forIndexPath:indexPath];
                break;
            default:
                break;
        }
        [cell setDataModel:model with:_isMoreSelect];
        [cell setDelegate:self];
    }
    else if ([messageContent isMemberOfClass:[WYSeparateModel class]]) {
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:wyseparateCellIndentifier forIndexPath:indexPath];
        [cell setDataModel:model with:_isMoreSelect];
        return cell;
    }
    cell.messageCellDelegate = self;
    if ([self.cellSelectedArr containsObject:@(self.conversationDataRepository.count - indexPath.row)]) {
        [cell.selectIconImageView setImage:[UIImage imageNamed:@"card_007"]];
    }else {
        [cell.selectIconImageView setImage:[UIImage imageNamed:@"circle"]];
    }
    return cell;
}

#pragma mark <UICollectionViewDelegateFlowLayout>
//计算cell高度
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    RCMessageModel *model = [self.conversationDataRepository objectAtIndex:indexPath.row];
    
    model.isDisplayNickname = NO;
    if ([model.content isKindOfClass:[RCTextMessage class]]) {
        ((RCTextMessage*)model.content).content = [NSMutableString stringWithString:[((RCTextMessage*)model.content).content stringByReplacingOccurrencesOfString:@"\\n" withString:@"\n"]];
    }
    RCMessageContent *messageContent = model.content;
    if ([messageContent isMemberOfClass:[RCTextMessage class]] ||
        [messageContent isMemberOfClass:[RCImageMessage class]] ||
        [messageContent isMemberOfClass:[RCLocationMessage class]] ||
        [messageContent isMemberOfClass:[RCVoiceMessage class]] ||
        [messageContent isMemberOfClass:[RCRichContentMessage class]]) {
        return [self sizeForItem:collectionView atIndexPath:indexPath];
    }
    else if ([messageContent isMemberOfClass:[WYSeparateModel class]]) {
        return CGSizeMake([UIScreen mainScreen].bounds.size.width, 30);
    }
    else {
        return CGSizeZero;
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    
    return CGSizeZero;
}

- (CGSize)sizeForItem:(UICollectionView *)collectionView atIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat __width = CGRectGetWidth(collectionView.frame);
    
    RCMessageModel *model = [self.conversationDataRepository objectAtIndex:indexPath.row];
    RCMessageContent *messageContent = model.content;
    
    CGFloat __height = 0.0f;
    
    if ([messageContent isMemberOfClass:[RCTextMessage class]]) {
        
        RCTextMessage *_textMessage = (RCTextMessage *)messageContent;
        
        NSString *text = _textMessage.content;
        
        NSString *pattern = @"<a href=\'(.*?)\'>(.*?)</a>";
        
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern options:0 error:nil];
        
        NSArray *matches = [regex matchesInString:text options:0 range:NSMakeRange(0, text.length)]; // 网址
        
        NSUInteger lastIdx = 0;
        
        NSMutableString *attributedString = /*[NSMutableString stringWithCapacity:0];*/[NSMutableString stringWithFormat:@""]; // 总字符串
        
        NSMutableString *realTextString = /*[NSMutableString stringWithCapacity:0];*/[NSMutableString stringWithFormat:@""];
        
        if (matches.count) { // 解析部分bug, 1.文字需要提出来, 2. 位置需要调整 matchs.count
            
            NSMutableArray *urlArrs = [[NSMutableArray alloc] init]; // 存url
            int i = 0;
            
            for (NSTextCheckingResult* match in matches)
            {
                NSRange range = match.range;
                
                if (range.location > lastIdx)
                {
                    NSString *temp = [text substringWithRange:NSMakeRange(lastIdx, range.location - lastIdx)];
                    [attributedString appendString:temp];// 记录头
                    [realTextString appendString:temp];
                }
                NSString *title = [text substringWithRange:[match rangeAtIndex:2]]; // 标题
                NSString *url = [text substringWithRange:[match rangeAtIndex:1]]; // 网址
                
                lastIdx = range.location + range.length;
                [attributedString appendFormat:@"<%d>%@</%d>",i,title,i];
                [realTextString appendString:title];
                [urlArrs addObject:url];
                i++;
            }
            if (lastIdx < text.length)
            {
                NSString  *temp = [text substringFromIndex:lastIdx];
                [attributedString appendString:temp];// 记录尾
                [realTextString appendString:temp];
            }
        } else {
            [realTextString appendString:_textMessage.content];
        }
        CGSize __textSize = CGSizeZero;
        if (IOS_FSystenVersion < 7.0) {
            __textSize = RC_MULTILINE_TEXTSIZE_LIOS7(realTextString, [UIFont systemFontOfSize:Text_Message_Font_Size], CGSizeMake(__width - (10 /* + [RCIM sharedRCIM].globalMessagePortraitSize.width + 5*/) * 2 - [RCIM sharedRCIM].globalMessagePortraitSize.width - 20 * 2, 8000),NSLineBreakByTruncatingTail);
        }else {
            __textSize = RC_MULTILINE_TEXTSIZE_GEIOS7(realTextString, [UIFont systemFontOfSize:Text_Message_Font_Size], CGSizeMake(__width - (10 /* + [RCIM sharedRCIM].globalMessagePortraitSize.width + 5*/) * 2 - [RCIM sharedRCIM].globalMessagePortraitSize.width - 20 * 2, 8000));
        }
        __textSize = CGSizeMake(ceilf(__textSize.width), ceilf(__textSize.height));
        CGSize __labelSize = CGSizeMake(__textSize.width, __textSize.height);
        CGFloat __bubbleHeight = __labelSize.height + 12 + 12 < 46 ? 46 : (__labelSize.height + 12 + 12);
        __height = __bubbleHeight+15;
        //        __height = __textSize.height;
    } else if ([messageContent isMemberOfClass:[RCLocationMessage class]]) {
        
        __height = 140;
    } else if ([messageContent isMemberOfClass:[RCVoiceMessage class]]) {
        
        __height = 46;
        
    } else if ([messageContent isMemberOfClass:[RCRichContentMessage class]]) {
        
        RCRichContentMessage *message = (RCRichContentMessage *)messageContent;
        
        int title = [message.title intValue];
        
        switch (title) {
            case 6:
                __height = CARDONE_HEIGHT;
                break;
            case 7:
                __height = CARDTWO_HEIGHT;
                break;
            case 8:
                __height = CARDFIVE_HEIGHT;
                break;
            default:
                break;
        }
        
    } else if ([messageContent isMemberOfClass:[RCImageMessage class]]) {
        RCImageMessage *_imageMessage = (RCImageMessage *)messageContent;
        CGSize imageSize = _imageMessage.thumbnailImage.size;
        //兼容240
        CGFloat imageWidth = 120;
        CGFloat imageHeight = 120;
        
        CGFloat imageWidthMin = 60;
        CGFloat imageHeightMin = 60;
        CGFloat imageWidthMax = [UIScreen mainScreen].bounds.size.width * 0.5/*0.8*/;
        CGFloat imageHeightMax = [UIScreen mainScreen].bounds.size.height * 0.3/*0.6*/;
        CGFloat scale = imageWidthMax * 1.0 / imageHeightMax;
        if (imageSize.width < imageWidthMin && imageSize.height < imageHeightMin) {
            if (imageSize.width >= imageSize.height) {
                if (imageSize.width * 1.0 / imageSize.height >= imageWidthMax * 1.0 / imageHeightMin) {
                    imageWidth = imageWidthMax;
                    imageHeight = imageSize.height *imageWidthMax * 1.0 / imageSize.width;
                } else {
                    imageHeight = imageHeightMin;
                    imageWidth = imageSize.width * imageHeightMin * 1.0 / imageSize.height;
                }
            } else {
                if (imageSize.height * 1.0 / imageSize.width >= imageHeightMax * 1.0 / imageWidthMin) {
                    imageHeight = imageHeightMax;
                    imageWidth = imageSize.width *imageHeightMax * 1.0 / imageSize.height;
                } else {
                    imageWidth = imageWidthMin;
                    imageHeight = imageSize.height * imageWidthMin * 1.0 / imageSize.width;
                }
            }
        } else if (imageSize.width > imageWidthMax || imageSize.height > imageHeightMax) {
            if (imageSize.width >= imageSize.height * scale) {
                imageWidth = imageWidthMax;
                imageHeight = imageSize.height *imageWidthMax * 1.0 / imageSize.width;
            } else {
                imageHeight = imageHeightMax;
                imageWidth = imageSize.width * imageHeightMax * 1.0 / imageSize.height;
            }
        } else {
            imageWidth = imageSize.width;
            imageHeight = imageSize.height;
        }
        
        //图片half
        imageSize = CGSizeMake(imageWidth, imageHeight);
        __height = imageSize.height;
    }
    
    
    //上边距
    __height = __height + 10;
    //下边距
    __height = __height + 5+10;
    
    return CGSizeMake(__width, __height);
}

- (void)chatSessionInputBarControlContentSizeChanged:(CGRect)frame {
    
    [_chatSessionInputBarControl updateConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(frame.size.height);
    }];
    
    [_conversationMessageCollectionView updateConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(_chatSessionInputBarControl.top);
    }];
    
    [self scrollToBottomAnimated:YES];
}

- (void)keyboardWillHide {
    
    if (self.chatSessionInputBarControl.inputTextView.isFirstResponder) //判断键盘打开
    {
        [self.chatSessionInputBarControl.inputTextView resignFirstResponder];
    }
    [self layoutBottomBarWithStatus:KBottomBarDefaultStatus];
    
}

- (void)didTouchSugButton:(UIButton *)sender { // 点击sug键盘入口按钮
    
    if (_sugView.y == self.view.h) {
        if (_chatSessionInputBarControl.y == self.view.h - Height_ChatSessionInputBar) {
            if (_currentBottomBarStatus != KBottomBarSugStatus) {
                [self layoutBottomBarWithStatus:KBottomBarSugStatus];
            }
        } else {
            if (_currentBottomBarStatus != KBottomBarSugStatus) {
                [self animationLayoutBottomBarWithStatus:KBottomBarSugStatus];
            }
        }
    } else { // 招出键盘, 注销sug
        [_chatSessionInputBarControl.inputTextView becomeFirstResponder];
    }
}

- (void)didTouchEmojiButton:(UIButton *)sender {
    
    if (_currentBottomBarStatus != KBottomBarEmojiStatus) {
        [self animationLayoutBottomBarWithStatus:KBottomBarEmojiStatus];
    }
}

- (void)didTouchPictureButton:(UIButton *)sender {
    
    [self closeInputSugView];
    if (_currentBottomBarStatus != KBottomBarPictureStatus) {
        [self animationLayoutBottomBarWithStatus:KBottomBarPictureStatus];
    }
}

- (void)openSystemPicture { // 打开相册
    
    [MobClick event:@"chatroom_photo_click"];
    __block RCAlbumListViewController *albumListVC = [[RCAlbumListViewController alloc] init];
    albumListVC.delegate = self;
    UINavigationController *rootVC = [[UINavigationController alloc] initWithRootViewController:albumListVC];
    __weak typeof(&*self) weakself = self;
    
    RCAssetHelper *sharedAssetHelper = [RCAssetHelper shareAssetHelper];
    [sharedAssetHelper getGroupsWithALAssetsGroupType:ALAssetsGroupAll resultCompletion:^(ALAssetsGroup *assetGroup) {
        if (nil != assetGroup) {
            [albumListVC.libraryList insertObject:assetGroup atIndex:0];
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakself.navigationController presentViewController:rootVC animated:YES completion:^{}];
            });
        }
    }];
}

- (void)didTouchCameraButton:(UIButton *)sender { // 打开相机
    
    [MobClick event:@"chatroom_camera_click"];
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
#if TARGET_IPHONE_SIMULATOR
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
#else
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
#endif
    self.currentPicker = picker;
    self.currentPicker.delegate = self;
    [self presentViewController:picker animated:YES completion:nil];
}

#pragma mark - RCAlbumListViewControllerDelegate
- (void)albumListViewController:(RCAlbumListViewController *)albumListViewController selectedImages:(NSArray *)selectedImages { // 选完图片
    
    _isTakeNewPhoto = NO;
    __weak SDJChatRoomController *weakSelf = self;
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        for (int i = 0; i < selectedImages.count; i++) {
            UIImage *image = [selectedImages objectAtIndex:i];
            RCImageMessage *imagemsg = [RCImageMessage messageWithImage:image];
            [weakSelf sendImageMessage:imagemsg pushContent:nil];
            [NSThread sleepForTimeInterval:0.5];
        }
    });
}

#pragma mark - UIImagePickerControllerDelegate method
//选择相册图片或者拍照回调
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo {
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    RCImageMessage *imageMessage = [RCImageMessage messageWithImage:image];
    
    _isTakeNewPhoto = YES;
    
    [self sendImageMessage:imageMessage pushContent:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark RCLocationPickerViewControllerDelegate
// 选取位置后回调
- (void)locationPicker:(RCLocationPickerViewController *)locationPicker didSelectLocation:(CLLocationCoordinate2D)location locationName:(NSString *)locationName mapScreenShot:(UIImage *)mapScreenShot {
    
    RCLocationMessage *locationMessage =[RCLocationMessage messageWithLocationImage:mapScreenShot location:location locationName:locationName];
    [self sendMessage:locationMessage pushContent:nil];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark <RCEmojiBoardViewDelegate>
- (void)didTouchEmojiView:(RCEmojiBoardView *)emojiView touchedEmoji:(NSString *)string {
    
    NSString *replaceString;
    
    if (nil == string) {
        [self.chatSessionInputBarControl.inputTextView deleteBackward];
    } else {
        [self.chatSessionInputBarControl.inputTextView becomeFirstResponder];
        self.chatSessionInputBarControl.placehoderLabel.hidden = YES;
        replaceString = string;
        if (replaceString.length < 5000) {
            self.chatSessionInputBarControl.inputTextView.text =
            [self.chatSessionInputBarControl.inputTextView.text
             stringByAppendingString:replaceString];
            {
                CGFloat _inputTextview_height = 36.0f;
                if (_chatSessionInputBarControl.inputTextView.contentSize.height < 70 &&
                    _chatSessionInputBarControl.inputTextView.contentSize.height >
                    36.0f) {
                    _inputTextview_height =
                    _chatSessionInputBarControl.inputTextView.contentSize.height;
                }
                if (_chatSessionInputBarControl.inputTextView.contentSize.height >=
                    70) {
                    _inputTextview_height = 70;
                }
                CGRect intputTextRect = _chatSessionInputBarControl.inputTextView.frame;
                intputTextRect.size.height = _inputTextview_height;
                intputTextRect.origin.y = 7;
                [_chatSessionInputBarControl.inputTextView setFrame:intputTextRect];
                _chatSessionInputBarControl.inputTextview_height =
                _inputTextview_height;
                
                CGRect vRect = _chatSessionInputBarControl.frame;
                vRect.size.height =
                Height_ChatSessionInputBar + (_inputTextview_height - 36);
                vRect.origin.y = _chatSessionInputBarControl.originalPositionY -
                (_inputTextview_height - 36);
                [self.chatSessionInputBarControl changeSize:intputTextRect];
                [self chatSessionInputBarControlContentSizeChanged:vRect];
            }
        }
    }
    
    UITextView *textView = self.chatSessionInputBarControl.inputTextView;
    {
        CGRect line = [textView caretRectForPosition:textView.selectedTextRange.start];
        CGFloat overflow = line.origin.y + line.size.height - (textView.contentOffset.y + textView.bounds.size.height -
                                                               textView.contentInset.bottom - textView.contentInset.top);
        if (overflow > 0) {
            CGPoint offset = textView.contentOffset;
            offset.y += overflow + 7; // leave 7 pixels margin
            [UIView animateWithDuration:.2 animations:^{[textView setContentOffset:offset];}];
        }
    }
}

- (void)didSendButtonEvent:(RCEmojiBoardView *)emojiView sendButton:(UIButton *)sendButton {
    
    NSString *_sendText = self.chatSessionInputBarControl.inputTextView.text;
    
    NSString *_formatString = [_sendText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if (0 == [_formatString length]) {
        UIAlertView *notAllowSendSpace = [[UIAlertView alloc]
                                          initWithTitle:nil
                                          message:NSLocalizedString(@"whiteSpaceMessage", @"")
                                          delegate:self
                                          cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                          otherButtonTitles:nil, nil];
        [notAllowSendSpace show];
        return;
    }
    
    RCTextMessage *rcTextMessage = [RCTextMessage messageWithContent:self.chatSessionInputBarControl.inputTextView.text];
    [self sendMessage:rcTextMessage pushContent:nil];
    
    self.chatSessionInputBarControl.inputTextView.text = @"";
    {
        CGFloat _inputTextview_height = 36.0f;
        CGRect intputTextRect = _chatSessionInputBarControl.inputTextView.frame;
        intputTextRect.size.height = _inputTextview_height;
        intputTextRect.origin.y = 7;
        [_chatSessionInputBarControl.inputTextView setFrame:intputTextRect];
        
        CGRect vRect = _chatSessionInputBarControl.frame;
        vRect.size.height = Height_ChatSessionInputBar + (_inputTextview_height - 36);
        vRect.origin.y = _chatSessionInputBarControl.originalPositionY - (_inputTextview_height - 36);
        [self.chatSessionInputBarControl changeSize:intputTextRect];
        [self chatSessionInputBarControlContentSizeChanged:vRect];
    }
}

#pragma mark - new sendBtn
- (void)didTouchSendButton:(UIButton *)sender {
    // 试试直接发
    RCTextMessage *rcTextMessage = [RCTextMessage messageWithContent:self.chatSessionInputBarControl.inputTextView.text];
    
    [self sendMessage:rcTextMessage pushContent:nil];
    self.chatSessionInputBarControl.inputTextView.text= nil;
    [self closeInputSugView];
}

- (void)didTouchKeyboardReturnKey:(RCChatSessionInputBarControl *)inputControl text:(NSString *)text {
    
    RCTextMessage *rcTextMessage = [RCTextMessage messageWithContent:text];
    [self sendMessage:rcTextMessage pushContent:nil];
    self.chatSessionInputBarControl.isInputSug = YES;
}

- (void)didTapUrlInMessageCell:(NSString *)url model:(RCMessageModel *)model {
    
    // 微信链接替换
    NSString *str1 = @"http://m.laiye.com/m/index.php?";
    
    NSRange range1 = [url rangeOfString:str1];
    
    if (range1.length) {
        
        NSString *str2 = @"http://m.laiye.com/m-hybrid/_index.html";
        
        NSString *str3 = [url substringFromIndex:range1.length];
        
        url = [str2 stringByAppendingString:str3];
    }
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
}

- (void)didTouchCollectionCellAtIndex:(int)index WithModel:(SDJCard_Items *)model {
    
    SDJNormalWebController *vc = [[SDJNormalWebController alloc] init];
    
    vc.urlString = model.Url;
    
    vc.title = model.UrlTip;
    
    [self.navigationController pushViewController:vc animated:YES];
}

//点击cell
- (void)didTapMessageCell:(RCMessageModel *)model {
    
    if (nil == model) {
        return;
    }
    RCMessageContent *_messageContent = model.content;
    
    if ([_messageContent isMemberOfClass:[RCImageMessage class]]) { // 点击了图片
        [self presentImagePreviewController:model];
    } else if ([_messageContent isMemberOfClass:[RCVoiceMessage class]]) {
        for (RCMessageModel *msg in self.conversationDataRepository) {
            if (model.messageId == msg.messageId) {
                msg.receivedStatus = ReceivedStatus_LISTENED;
                break;
            }
        }
    } else if ([_messageContent isMemberOfClass:[RCLocationMessage class]]) {
        RCLocationMessage *locationMessage = (RCLocationMessage *)(_messageContent);
        [self presentLocationViewController:locationMessage];
    }
}

- (void)presentLocationViewController:(RCLocationMessage *)locationMessageContent {
    
    //默认方法跳转
    RCLocationViewController *locationViewController = [[RCLocationViewController alloc] init];
    locationViewController.locationName = locationMessageContent.locationName;
    locationViewController.location = locationMessageContent.location;
    UINavigationController *navc = [[UINavigationController alloc] initWithRootViewController:locationViewController];
    if (self.navigationController) {
        //导航和原有的配色保持一直
        UIImage *image = [self.navigationController.navigationBar backgroundImageForBarMetrics:UIBarMetricsDefault];
        
        [navc.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
    }
    [self presentViewController:navc animated:YES completion:NULL];
}

- (void)didTapPhoneNumberInMessageCell:(NSString *)phoneNumber model:(RCMessageModel *)model {
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
}

//长按消息内容
- (void)didLongTouchMessageCell:(RCMessageModel *)model inView:(UIView *)view {
    
    [self closeInputSugView];
    
    [MobClick event:@"chatroom_longPress_click"];
    self.chatSessionInputBarControl.inputTextView.disableActionMenu = YES;
    self.longPressSelectedModel = model;
    
    [self becomeFirstResponder];
    CGRect rect = [self.view convertRect:view.frame fromView:view.superview];
    
    UIMenuController *menu = [UIMenuController sharedMenuController];
    UIMenuItem *loveItem = [[UIMenuItem alloc]initWithTitle:NSLocalizedString(@"点赞", @"") action:@selector(loveChatRoomMsg)];
    UIMenuItem *copyItem = [[UIMenuItem alloc]initWithTitle:NSLocalizedString(@"复制", @"") action:@selector(onCopyMessage:)];
    UIMenuItem *deleteItem = [[UIMenuItem alloc]initWithTitle:NSLocalizedString(@"删除", @"") action:@selector(onDeleteMessage:)];
    UIMenuItem *moreSelect = [[UIMenuItem alloc]initWithTitle:NSLocalizedString(@"分享", @"") action:@selector(onSelectModelMessage:)];
    
    if ([model.content isMemberOfClass:[RCTextMessage class]] || [model.content isMemberOfClass:[RCImageMessage class]]) {
        [menu setMenuItems:[NSArray arrayWithObjects:loveItem, copyItem, deleteItem, moreSelect, nil]];
    } else {
        [menu setMenuItems:@[deleteItem]];
    }
    
    [menu setTargetRect:rect inView:self.view];
    [menu setMenuVisible:YES animated:YES];
}

- (void)didTouchUrl:(NSString *)urlString  {
    
    urlString = [urlString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    // http://
    NSString *httpIndex = [urlString substringToIndex:7];
    if (![httpIndex isEqualToString:@"http://"] && ![httpIndex isEqualToString:@"https:/"]) {
        urlString = [@"http://" stringByAppendingString:urlString];
    }
    
    NSString *str1 = @"http://m.laiye.com/m/index.php?";
    
    NSRange range1 = [urlString rangeOfString:str1];
    
    if (range1.length) {
        
        NSString *str2 = @"http://m.laiye.com/m-hybrid/_index.html";
        
        NSString *str3 = [urlString substringFromIndex:range1.length];
        
        urlString = [str2 stringByAppendingString:str3];
    }
    
    NSString *str = @"m.laiye.com";
    
    NSRange range = [urlString rangeOfString:str];
    
    if (range.length) {
        
        SDJOriginalWebViewController *vc = [[SDJOriginalWebViewController alloc] init];
        vc.urlString = urlString;
        [self.navigationController pushViewController:vc animated:YES];
        
    } else {
        SDJNormalWebController *vc = [[SDJNormalWebController alloc] init];
        vc.urlString = urlString;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

// 新增智能键盘
#pragma mark <SDJIntelligentBoardViewDelegate>
- (void)didSelectedIntelligentBoardView:(SDJIntelligentBoardView *)intelligentView textSelected:(NSString *)text withTag:(int)tag {
    
    RCTextMessage *rcTextMessage = [RCTextMessage messageWithContent:text];
    [self sendMessage:rcTextMessage pushContent:nil andText:text];
    [self closeInputSugView];
}

#pragma mark <RCChatSessionInputBarControlDelegate>
- (void)keyboardWillShowWithFrame:(CGRect)keyboardFrame {
    
    if (!_isMoreSelect) {
        _chatSessionInputBarControl.emojiButton.selected = NO;
        _chatSessionInputBarControl.pictureButton.selected = NO;
        _chatSessionInputBarControl.sugButton.selected = NO;
        
        if (keyboardFrame.size.height != 0) {
            
            [_chatControlBottomConstraint uninstall];
            [_chatSessionInputBarControl updateConstraints:^(MASConstraintMaker *make) {
                _chatControlBottomConstraint = make.bottom.equalTo(- keyboardFrame.size.height);
            }];
            
            [_sugView updateConstraints:^(MASConstraintMaker *make) {
                make.bottom.equalTo(self.view).offset(Height_IntelBoardView);
            }];
            
            [_emojiView updateConstraints:^(MASConstraintMaker *make) {
                make.bottom.equalTo(self.view).offset(Height_EmojiBoardView);
            }];
            
            [_conversationMessageCollectionView updateConstraints:^(MASConstraintMaker *make) {
                make.bottom.equalTo(_chatSessionInputBarControl.top);
            }];
            
            _currentBottomBarStatus = KBottomBarKeyboardStatus;
            
            [self.view layoutIfNeeded];
            
            [UIView animateWithDuration:0.7 animations:^{
                [self scrollToBottomAnimated:YES];
            }];
            
        }
    } else {}
}

- (void)animationLayoutBottomBarWithStatus:(KBottomBarStatus)bottomBarStatus { // 键盘收缩动画
    
    [UIView beginAnimations:@"Move_bar" context:nil];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
    [UIView setAnimationDuration:0.2f];
    [UIView setAnimationDelegate:self];
    [self layoutBottomBarWithStatus:bottomBarStatus];
    [UIView commitAnimations];
    if (bottomBarStatus == KBottomBarPictureStatus) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        __weak typeof(self) weakSelf = self;
        UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"选择照片" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [weakSelf openSystemPicture];
        }];//自选地址
        UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"拍摄照片" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [weakSelf didTouchCameraButton:nil];
        }];//重叫
        [alertController addAction:action1];
        [alertController addAction:action2];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {}];
        [alertController addAction:cancelAction];
        [self presentViewController:alertController animated:YES completion:nil];
        _currentBottomBarStatus = KBottomBarNull;
    }
}

-(void)layoutBottomBarInLong{
    
    if (_isMoreSelect) {
        return;
    }
    
    if (self.chatSessionInputBarControl.inputTextView.isFirstResponder) {
        [self.chatSessionInputBarControl.inputTextView resignFirstResponder];
    }
    _chatSessionInputBarControl.emojiButton.selected = NO;
    _chatSessionInputBarControl.pictureButton.selected = NO;
    _chatSessionInputBarControl.sugButton.selected = NO;
    
    [_sugView updateConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.view).offset(Height_IntelBoardView);
    }];
    
    [_emojiView updateConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.view).offset(Height_EmojiBoardView);
    }];
    
    [_chatControlBottomConstraint uninstall];
    [_chatSessionInputBarControl updateConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(Height_ChatSessionInputBar);
        _chatControlBottomConstraint = make.bottom.equalTo(self.view);
    }];
    [_conversationMessageCollectionView updateConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(_chatSessionInputBarControl.top);
    }];
    
    _currentBottomBarStatus = KBottomBarDefaultStatus;
}

- (void)layoutBottomBarWithStatus:(KBottomBarStatus)bottomBarStatus { // 键盘高度切换
    
    if (_isMoreSelect) {
        return;
    }
    
    if (bottomBarStatus == KBottomBarSugStatus || bottomBarStatus == KBottomBarEmojiStatus || bottomBarStatus == KBottomBarPictureStatus || bottomBarStatus == KBottomBarDefaultStatus) {
        if (self.chatSessionInputBarControl.inputTextView.isFirstResponder) {
            [self.chatSessionInputBarControl.inputTextView resignFirstResponder];
        }
        _chatSessionInputBarControl.emojiButton.selected = NO;
        _chatSessionInputBarControl.pictureButton.selected = NO;
        _chatSessionInputBarControl.sugButton.selected = NO;
    }
    
    switch (bottomBarStatus) {
            
        case KBottomBarDefaultStatus: {
            
            [_sugView updateConstraints:^(MASConstraintMaker *make) {
                make.bottom.equalTo(self.view).offset(Height_IntelBoardView);
            }];
            
            [_emojiView updateConstraints:^(MASConstraintMaker *make) {
                make.bottom.equalTo(self.view).offset(Height_EmojiBoardView);
            }];
            
            [_chatControlBottomConstraint uninstall];
            [_chatSessionInputBarControl updateConstraints:^(MASConstraintMaker *make) {
                make.height.equalTo(Height_ChatSessionInputBar);
                _chatControlBottomConstraint = make.bottom.equalTo(self.view);
            }];
            
        } break;
            
        case KBottomBarKeyboardStatus: {
        
        } break;
            
        case KBottomBarSugStatus: {
            
            [_sugView updateConstraints:^(MASConstraintMaker *make) {
                make.bottom.equalTo(self.view);
            }];
            
            [_emojiView updateConstraints:^(MASConstraintMaker *make) {
                make.bottom.equalTo(self.view).offset(Height_EmojiBoardView);
            }];

            [_chatControlBottomConstraint uninstall];
            [_chatSessionInputBarControl updateConstraints:^(MASConstraintMaker *make) {
                make.height.equalTo(Height_ChatSessionInputBar);
                _chatControlBottomConstraint = make.bottom.equalTo(_sugView.top);
            }];
            
            _chatSessionInputBarControl.sugButton.selected = YES;
            
        } break;
            
        case KBottomBarEmojiStatus: {
            
            [_emojiView updateConstraints:^(MASConstraintMaker *make) {
                make.bottom.equalTo(self.view);
            }];
            
            [_sugView updateConstraints:^(MASConstraintMaker *make) {
                make.bottom.equalTo(self.view).offset(Height_IntelBoardView);
            }];

            [_chatControlBottomConstraint uninstall];
            [_chatSessionInputBarControl updateConstraints:^(MASConstraintMaker *make) {
                make.height.equalTo(Height_ChatSessionInputBar);
                _chatControlBottomConstraint = make.bottom.equalTo(_emojiView.top);
            }];
            
            _chatSessionInputBarControl.emojiButton.selected = YES;
            
        } break;
            
        case KBottomBarPictureStatus: {
            
            [_emojiView updateConstraints:^(MASConstraintMaker *make) {
                make.bottom.equalTo(self.view).offset(Height_EmojiBoardView);
            }];
            
            [_sugView updateConstraints:^(MASConstraintMaker *make) {
                make.bottom.equalTo(self.view).offset(Height_IntelBoardView);
            }];

            [_chatControlBottomConstraint uninstall];
            [_chatSessionInputBarControl updateConstraints:^(MASConstraintMaker *make) {
                make.height.equalTo(Height_ChatSessionInputBar);
                _chatControlBottomConstraint = make.bottom.equalTo(_emojiView.top);
            }];
            
            _chatSessionInputBarControl.pictureButton.selected = YES;
            
        } break;
        default:
            break;
    }
    
    [_conversationMessageCollectionView updateConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(_chatSessionInputBarControl.top);
    }];
    
    _currentBottomBarStatus = bottomBarStatus;
    __weak typeof(self) __weakSelf = self;
    [UIView animateWithDuration:0.3 animations:^{
        [__weakSelf.view layoutIfNeeded];
        [__weakSelf scrollToBottomAnimated:NO];
    }];
}

#warning  这里拿到通知信息,最早的信息入口
- (void)didReceiveMessageNotification:(NSNotification *)notification {
    
    _receiveMessageFlag++;
    if (_receiveMessageFlag % 2) { // 这里信息重复, 过滤掉一次,为什么重复?暂时未知
        return;
    }
    
    RCMessage *rcMessage = notification.object;
    RCMessageModel *model = [[RCMessageModel alloc] initWithMessage:rcMessage];
    
    [self.informationModel modelByRCMessage:rcMessage];
    
    self.connectListModel.informationModel = [self.informationModel copy];
    
    if (model.conversationType == ConversationType_GROUP/*self.conversationType*/ &&
        [model.targetId isEqual:self.informationModel.user_gid /*self.targetId*/]) {
        if (self.isConversationAppear) {
            [[RCIMClient sharedRCIMClient]
             clearMessagesUnreadStatus:self.conversationType
             targetId:self.targetId];
        }
        
        __weak typeof(&*self) __blockSelf = self;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [__blockSelf appendAndDisplayMessage:rcMessage];
            
            UIMenuController *menu = [UIMenuController sharedMenuController];
            menu.menuVisible = NO;
            // 是否显示右下未读消息数
            if (self.enableNewComingMessageIcon == YES) {
                if (![self isAtTheBottomOfTableView]) {
                    self.unreadNewMsgCount ++ ;
                    [self updateUnreadMsgCountLabel];
                }
            }
        });
    }
}

- (void)updateUnreadMsgCountLabel
{
    if (self.unreadNewMsgCount == 0) {
        self.unreadRightBottomIcon.hidden = YES;
    }
    else
    {
        self.unreadRightBottomIcon.hidden = NO;
        self.unReadNewMessageLabel.text = (self.unreadNewMsgCount > 99) ?  @"99+" : [NSString stringWithFormat:@"%li", (long)self.unreadNewMsgCount];
    }
}

- (void) checkVisiableCell
{
    NSIndexPath *lastPath = [self getLastIndexPathForVisibleItems];
    
    if (lastPath.row >= self.conversationDataRepository.count - self.unreadNewMsgCount || lastPath == nil || [self isAtTheBottomOfTableView] ) {
        
        self.unreadNewMsgCount = 0;
        [self updateUnreadMsgCountLabel];
    }
}

- (NSIndexPath *)getLastIndexPathForVisibleItems
{
    NSArray *visiblePaths = [self.conversationMessageCollectionView indexPathsForVisibleItems];
    
    if (visiblePaths.count == 0) {
        return nil;
    }else if(visiblePaths.count == 1) {
        return (NSIndexPath *)[visiblePaths firstObject];
    }
    
    NSArray *sortedIndexPaths = [visiblePaths sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        NSIndexPath *path1 = (NSIndexPath *)obj1;
        NSIndexPath *path2 = (NSIndexPath *)obj2;
        return [path1 compare:path2];
    }];
    return (NSIndexPath *)[sortedIndexPaths lastObject];
}

- (void)figureOutAllConversationDataRepository {
    
    for (int i = 0; i < self.conversationDataRepository.count; i++) {
        
        RCMessageModel *model = [self.conversationDataRepository objectAtIndex:i];
        model.isDisplayMessageTime = YES;
    }
}

- (void)figureOutLatestModel:(RCMessageModel *)model; // 找到最后一条信息
{
    if (_conversationDataRepository.count > 0) {
        model.isDisplayMessageTime = YES;
    }
}

- (void)appendAndDisplayMessage:(RCMessage *)rcMessage {
    
    //如果接收到消息，且是对方发过来的键盘在最下面自动弹出sug键盘
    if (rcMessage.messageDirection == 2) {
        if (self.chatSessionInputBarControl.frame.origin.y == self.view.frame.size.height - self.chatSessionInputBarControl.frame.size.height) {
            [self layoutBottomBarWithStatus:KBottomBarSugStatus];
        }
    }
    __weak typeof(self) __weakSelf = self;
    if ([rcMessage.content isKindOfClass:[RCTextMessage class]]) {
        if ([((RCTextMessage *)rcMessage.content).content isEqualToString:@"text"]) {
            if ([((RCTextMessage *)rcMessage.content).extra isKindOfClass:[NSDictionary class]]) {
                ((RCTextMessage *)rcMessage.content).content = ((NSDictionary *)((RCTextMessage *)rcMessage.content).extra)[@"msg_detail"][@"content"];
            }
            else {
                ((RCTextMessage *)rcMessage.content).content = [WYTool jsonStringToDictionary:((RCTextMessage *)rcMessage.content).extra][@"msg_detail"][@"content"];
            }
        }
        if ([((RCTextMessage *)rcMessage.content).content isEqualToString:@"echo"] && [((RCTextMessage *)rcMessage.content).extra isKindOfClass:[NSString class]]) {
            RCTextMessage *msg = (RCTextMessage *)rcMessage.content;
            NSDictionary *msgJSONDic = [WYTool jsonStringToDictionary:msg.extra];
            ((RCTextMessage *)rcMessage.content).content = msgJSONDic[@"msg_detail"][@"content"];
            ((RCTextMessage *)rcMessage.content).extra = (NSString *)msgJSONDic;
        }
    }
    
    else if ([rcMessage.content isKindOfClass:[RCImageMessage class]]) {
        RCImageMessage *imageMessage = (RCImageMessage *)rcMessage.content;
        if ([rcMessage.objectName isEqualToString:@"RC:ImgMsg"]) {
            rcMessage.objectName = @"[图片]";
        }
        if (imageMessage.originalImage == nil && imageMessage.thumbnailImage == nil) {
            UIImageView *imageView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"placeholder"]];
            imageMessage.originalImage = imageView.image;
            imageMessage.thumbnailImage = imageView.image;
            [imageView sd_setImageWithURL:[NSURL URLWithString:imageMessage.imageUrl]placeholderImage:[UIImage imageNamed:@"placeholder"] options:SDWebImageRetryFailed completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                if (!error) {
                    imageMessage.originalImage = imageView.image;
                    imageMessage.thumbnailImage = imageView.image;
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [__weakSelf.conversationMessageCollectionView reloadData];
                        [__weakSelf layoutBottomBarWithStatus:KBottomBarDefaultStatus];
                    });
                }
            }];
        }
    }
    else if ([rcMessage.content isKindOfClass:[RCVoiceMessage class]]) {
        if ([rcMessage.objectName isEqualToString:@"RC:VcMsg"]) {
            rcMessage.objectName = @"[语音]";
        }
        if ( [((RCVoiceMessage *)rcMessage.content).extra isKindOfClass:[NSDictionary class]]) {
            //转化成字典
            NSDictionary * dic = [WYTool jsonStringToDictionary:((RCVoiceMessage *)rcMessage.content).extra];
            //声音文件地址
            NSString *audioStr = dic[@"msg_detail"][@"voice_url"];
            NSURL *url = [[NSURL alloc]initWithString:audioStr];
            NSData * wavData = [VoiceConverter amrToWavWithAmrData:[NSData dataWithContentsOfURL:url]];
            ((RCVoiceMessage *)rcMessage.content).wavAudioData = wavData;
        }
    }
    
    self.sendOrReciveMessageNum ++;//记录新收到和自己新发送的消息数，用于计算加载历史消息时插入“以上是历史消息”cell 的位置
    RCMessageModel *model = [[RCMessageModel alloc] initWithMessage:rcMessage];
    if ([model.content isKindOfClass:[RCTextMessage class]]) {
        RCTextMessage *textMessage = (RCTextMessage *)model.content;
        NSDictionary *dic = nil;
        if (textMessage.extra != nil && ![textMessage.extra isKindOfClass:[NSDictionary class]]) {
            NSData *jsonData = [textMessage.extra dataUsingEncoding:NSUTF8StringEncoding];
            NSError *err;
            dic = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&err];
        }
        else {
            dic = (NSDictionary *)textMessage.extra;
        }
        if (model.messageDirection == MessageDirection_RECEIVE) {
            NSArray *arr = dic[@"msg_detail"][@"sug_list"];
            if (arr.count != 0) {
                NSMutableArray *arrM = [[NSMutableArray alloc] initWithCapacity:0];
                for (int i = 0; i < arr.count; i++) {
                    [arrM addObject:arr[i][@"sug_detail"]];
                }
                [self setIntelligentViewWithArray:arrM];
            } else {
                [self setIntelligentViewWithArray:[RCIM sharedRCIM].inteligentMessages];
            }
        }
    }
    else {
        [self setIntelligentViewWithArray:[RCIM sharedRCIM].inteligentMessages];
    }
    
    [self figureOutLatestModel:model];
    if ([self appendMessageModel:model]) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:self.conversationDataRepository.count - 1 inSection:0];
        if ([self.conversationMessageCollectionView numberOfItemsInSection:0] != self.conversationDataRepository.count - 1) {
            NSLog(@"Error, datasource and collectionview are inconsistent!!");
            [self.conversationMessageCollectionView reloadData];
            return;
        }
        [self.conversationMessageCollectionView insertItemsAtIndexPaths:[NSArray arrayWithObject:indexPath]];
        [self.conversationMessageCollectionView.collectionViewLayout invalidateLayout];
        
        if ([self isAtTheBottomOfTableView] || self.isNeedScrollToButtom) {
            [self scrollToBottomAnimated:YES];
            self.isNeedScrollToButtom = NO;
        }
    }
}

- (BOOL)appendMessageModel:(RCMessageModel *)model {
    
    long ne_wId = model.messageId;
    for (RCMessageModel *__item in self.conversationDataRepository) {
        //当id为－1时，不检查是否重复，直接插入
        if (ne_wId == -1) {
            break;
        }
        if (ne_wId == __item.messageId) {
            return NO;
        }
    }
    if (ne_wId != -1 &&!([[model.content class] persistentFlag] & MessagePersistent_ISPERSISTED)) {
        return NO;
    }
    [self.conversationDataRepository addObject:model];
    return YES;
}

- (void)pushOldMessageModel:(RCMessageModel *)model {
    
    [self.connectListModel.historyArray insertObject:model atIndex:0];
    [self.conversationDataRepository insertObject:model atIndex:0];
}
/**
 *  WY修改
 */
- (void)loadLatestHistoryMessage {
    
    self.isFirstHistory = YES;
    self.pageCount ++;
    NSMutableArray *__messageArray = [NSMutableArray array];
    [self historyMsg:__messageArray];
}

- (void)loadMoreHistoryMessage { //加载更多历史信息
    
    if (_isRefresh == NO) {
        return;
    }
    _isRefresh = NO;
    self.isFirstHistory = NO;
    self.pageCount ++;
    NSMutableArray *__messageArray = [NSMutableArray array];
    [self historyMsg:__messageArray];
    
    self.scrollNum++;
    if (self.scrollNum * 10 + 10> self.unReadMessage) {
        self.unReadMessage = 0;
    }
}

/**
 *  WY添加
 */
-(void)historyMsg:(NSMutableArray *)messageArray{
    
    __weak SDJChatRoomController *__wearSelf = self;
    [[SDJNetworkTools sharedTools]userCurrentMessage:[SDJUserAccount loadAccount].token user_id:@(self.connectListModel.inner_uid).stringValue size:@"10" pn:@(self.pageCount).stringValue finished:^(NSDictionary<NSString *,id> * _Nullable result, NSError * _Nullable error) {
        if (error) {
            _isRefresh = YES;
            return ;
        }
        if (![result[@"error"] isEqual:[NSNull null]]) {//异端登陆
            NSNumber *code = result[@"error"][@"error_code"];
            if (code.integerValue == 20000) {
                NSString *UserPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).firstObject stringByAppendingPathComponent:@"account.plist"];
                [[NSFileManager defaultManager]removeItemAtPath:UserPath error:nil];
                [[RCIMClient sharedRCIMClient] logout];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"WYRootViewControllerSwitchNotification" object:@"0"];
                });
            }
        }
        else {
            NSLog(@"%@", result);
            __wearSelf.connectListModel.page = self.pageCount;
            NSArray *historyMsgArray = result[@"data"][@"latest_msgs"];
            if(__wearSelf.pageCount == 1) {
                for (NSInteger i = historyMsgArray.count - 1; i >= 0; i --) {
                    NSDictionary *hisDict = historyMsgArray[i];
                    __wearSelf.informationModel = [WYTool modelWithHistoryDict:hisDict[@"extra"]];
                    if (((NSNumber *)hisDict[@"extra"][@"direction"]).integerValue == 0) {
                        break;
                    }
                }
                if (__wearSelf.informationModel == nil) {
                    __wearSelf.informationModel = [WYTool modelWithHistoryDict:historyMsgArray.lastObject[@"extra"]];
                    if (![__wearSelf.informationModel.user_gid hasPrefix:@"rc_group_"]) {
                        __wearSelf.informationModel.user_src = 2;
                    }
                }
                self.connectListModel.informationModel = [self.informationModel copy];
            }
            __wearSelf.dataNumber = historyMsgArray.count;
            for (NSDictionary *historyMsg in historyMsgArray) {
                int msgType = ((NSNumber *)historyMsg[@"extra"][@"msg_type"]).intValue;
                if (msgType == 1) {//text消息
                    RCMessageModel * msgModel = [WYTool createMsgModel:historyMsg];
                    [messageArray addObject:msgModel];
                    _downloadFlag ++;
                    if (_downloadFlag == __wearSelf.dataNumber) {
                        [__wearSelf messageDeal:[[messageArray reverseObjectEnumerator] allObjects]];
                        _downloadFlag = 0;
                    }
                }
                else if (msgType == 2) {//image消息
                    RCMessageModel * msgModel = [WYTool createImageModel:historyMsg];
                    UIImageView *imageView = [[UIImageView alloc]init];
                    [imageView sd_setImageWithURL:[NSURL URLWithString:historyMsg[@"imageUri"]] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            ((RCImageMessage *)msgModel.content).thumbnailImage = imageView.image;
                            ((RCImageMessage *)msgModel.content).originalImage = imageView.image;
                            _downloadFlag ++;
                            if (_downloadFlag == __wearSelf.dataNumber) {
                                [__wearSelf messageDeal:[[messageArray reverseObjectEnumerator] allObjects]];
                                _downloadFlag = 0;
                            }
                        });
                    }];
                    [messageArray addObject:msgModel];
                    
                }
                else if (msgType == 5) {//text消息
#warning 后面可以增加语音功能在此地方
                    RCMessageModel * msgModel = [WYTool createVoiceMsgModel:historyMsg];
                    [messageArray addObject:msgModel];
                    _downloadFlag ++;
                    if (_downloadFlag == __wearSelf.dataNumber) {
                        [__wearSelf messageDeal:[[messageArray reverseObjectEnumerator] allObjects]];
                        _downloadFlag = 0;
                    }
                }
                else if (msgType == 6) {
                    RCMessageModel * msgModel = [WYTool createCardMsgModel:historyMsg];
                    [messageArray addObject:msgModel];
                    _downloadFlag ++;
                    if (_downloadFlag == __wearSelf.dataNumber) {
                        [__wearSelf messageDeal:[[messageArray reverseObjectEnumerator] allObjects]];
                        _downloadFlag = 0;
                    }
                }
                else if (msgType == 7) {
                    RCMessageModel * msgModel = [WYTool createSingleCardMsgModel:historyMsg];
                    [messageArray addObject:msgModel];
                    _downloadFlag ++;
                    if (_downloadFlag == __wearSelf.dataNumber) {
                        [__wearSelf messageDeal:[[messageArray reverseObjectEnumerator] allObjects]];
                        _downloadFlag = 0;
                    }
                }
                else if (msgType == 10) {
                    RCMessageModel * msgModel = [WYTool createDateModel:historyMsg];
                    [messageArray addObject:msgModel];
                    _downloadFlag ++;
                    if (_downloadFlag == _dataNumber) {
                        [__wearSelf messageDeal:[[messageArray reverseObjectEnumerator] allObjects]];
                        _downloadFlag = 0;
                    }
                }
                else {
                    _downloadFlag ++;
                    if (_downloadFlag == __wearSelf.dataNumber) {
                        [__wearSelf messageDeal:[[messageArray reverseObjectEnumerator] allObjects]];
                        _downloadFlag = 0;
                    }
                }
            }
        }
    }];

}

/**
 *  WY添加
 */
-(void)messageDeal:(NSArray *)__messageArray{
    
    if(self.pageCount == 1) {
        WYSeparateModel *sepa = [WYSeparateModel modelWithDict:@{@"textContent":@"------------以上是历史消息------------"}];
        RCMessageModel *separateModel = [[RCMessageModel alloc]init];
        separateModel.content = sepa;
        [self pushOldMessageModel:separateModel];
    }
    
    for (int i = 0; i < __messageArray.count; i++) {
        RCMessage *rcMsg = [__messageArray objectAtIndex:i];
        RCMessageModel *model = [[RCMessageModel alloc] initWithMessage:rcMsg];
        [self pushOldMessageModel:model];
    }
    
    if (self.isFirstHistory == YES) {
        for (NSInteger i = 0; i < __messageArray.count - 1; i ++) {
            RCMessageModel *msgModel = (RCMessageModel *)__messageArray[i];
            if (msgModel.messageDirection == MessageDirection_SEND) {
                continue;
            }
            else {
                RCMessageContent *msgContent = msgModel.content;
                if ([msgContent isKindOfClass:[RCTextMessage class]]) {
                    NSArray *sugListArray = ((NSDictionary *)((RCTextMessage *)msgContent).extra)[@"msg_detail"][@"sug_list"];
                    NSMutableArray *inteligentMessage = [NSMutableArray array];
                    for (NSDictionary *sugListDict in sugListArray) {
                        [inteligentMessage addObject:sugListDict[@"sug_detail"]];
                    }
                    self.sugView.inteligentMessages = inteligentMessage;
                    break;
                }
                else if ([msgContent isKindOfClass:[RCImageMessage class]]) {
                    continue;
                }
            }
        }
    }
    
    [self figureOutAllConversationDataRepository];
    [self.conversationMessageCollectionView reloadData];
    _isRefresh = YES;
    
    if (self.isFirstHistory) {
        [self layoutBottomBarWithStatus:/*KBottomBarDefaultStatus*/KBottomBarSugStatus];
    }
    else {
        [self.conversationMessageCollectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:self.dataNumber inSection:0] atScrollPosition:UICollectionViewScrollPositionTop animated:NO];
    }
}

- (BOOL)canBecomeFirstResponder {
    
    return YES;
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    
    return [super canPerformAction:action withSender:sender];
}

- (NSInteger)findDataIndexFromMessageList:(RCMessageModel *)model {
    
    NSInteger index = 0;
    for (int i = 0; i < self.conversationDataRepository.count; i++) {
        RCMessageModel *msg = (self.conversationDataRepository)[i];
        if (msg.messageId == model.messageId) {
            index = i;
            break;
        }
    }
    return index;
}

- (void)didTapmessageFailedStatusViewForResend:(RCMessageModel *)model { // 失败消息重发
    
    RCMessageContent *content = model.content;
    long msgId = model.messageId;
    NSIndexPath *indexPath =
    [NSIndexPath indexPathForItem:[self findDataIndexFromMessageList:model]
                        inSection:0];
    [[RCIMClient sharedRCIMClient] deleteMessages:@[ @(msgId) ]];
    [self.conversationDataRepository removeObject:model];
    [self.conversationMessageCollectionView
     deleteItemsAtIndexPaths:[NSArray arrayWithObject:indexPath]];
    
    self.isNeedScrollToButtom=YES;
    [self resendMessage:content];
}

- (void)resendMessage:(RCMessageContent *)messageContent {
    
    if ([messageContent isMemberOfClass:RCImageMessage.class]) {
        RCImageMessage *imageMessage = (RCImageMessage *)messageContent;
        imageMessage.originalImage = [UIImage imageWithContentsOfFile:imageMessage.imageUrl];
        [self sendImageMessage:imageMessage pushContent:nil];
    } else {
        [self sendMessage:messageContent pushContent:nil];
    }
}

//  打开大图。开发者可以重写，自己下载并且展示图片。默认使用内置controller
- (void)presentImagePreviewController:(RCMessageModel *)model;
{
    [self closeInputSugView];
    RCImagePreviewController *_imagePreviewVC =
    [[RCImagePreviewController alloc] init];
    _imagePreviewVC.messageModel = model;
    
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:_imagePreviewVC];
    
    if (self.navigationController) {
        //导航和原有的配色保持一直
        UIImage *image = [self.navigationController.navigationBar backgroundImageForBarMetrics:UIBarMetricsDefault];
        
        [nav.navigationBar setBackgroundImage:image forBarMetrics:UIBarMetricsDefault];
    }
    [self presentViewController:nav animated:YES completion:nil];
}

- (void)sendImageMessage:(RCImageMessage *)imageMessage pushContent:(NSString *)pushContent {
    
    if (_targetId == nil) {
        return;
    }
    __weak typeof(&*self) __weakself = self;
    
    if (imageMessage == nil) {
        return;
    }
    
    NSData * imageData = UIImageJPEGRepresentation(imageMessage.originalImage, 1.0);
    double length = [imageData length] / (1024 * 1024 * 1.0);
    if (length > 1) {
        double scale = sqrt(length/0.9);
        CGSize originalSize = imageMessage.originalImage.size;
        CGSize nowOriginalSize = CGSizeMake(round(originalSize.width/scale), round(originalSize.height/scale));
        imageMessage.originalImage = [self imageByScalingAndCroppingForSize:nowOriginalSize andImage:imageMessage.originalImage];
    }
    
    UIImage *image = imageMessage.originalImage;
    RCMessage *rcMessage = [[RCIMClient sharedRCIMClient] sendImageMessage:self.conversationType targetId:self.targetId content:imageMessage pushContent:pushContent pushData:nil uploadPrepare:^(RCUploadImageStatusListener *uploadListener) {
        __block RCImageMessage * msg = (RCImageMessage *)imageMessage;
        NSTimeInterval time=(long)([[NSDate date] timeIntervalSince1970]*1000);
        __block NSDictionary *imageDict = [WYTool msgWithUserData:self.informationModel andTime:time];
        msg.extra = (NSString *)imageDict;
        [[SDJNetworkTools sharedTools]imageUpload:[SDJUserAccount loadAccount].token image:image finished:^(NSDictionary<NSString *,id> * _Nullable result, NSError * _Nullable error) {
            NSLog(@"imageupload:result:%@, error:%@", result, error);
            if (![result[@"error"] isEqual:[NSNull null]]) {//报错
                NSNumber *code = result[@"error"][@"error_code"];
                if (code.integerValue == 20000) {
                    NSString *UserPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).firstObject stringByAppendingPathComponent:@"account.plist"];
                    [[NSFileManager defaultManager]removeItemAtPath:UserPath error:nil];
                    //其他平台登录，token改变需要先登出融云
                    [[RCIMClient sharedRCIMClient] logout];
                    // 发送通知让appdelegate去切换根视图为聊天列表界面
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"WYRootViewControllerSwitchNotification" object:@"0"];
                    });
                }
            }
            else{
                NSString *urlStr = result[@"data"][@"raw"];
                imageDict = [WYTool msgWithImageURLString:urlStr andDic:imageDict];
                msg.imageUrl = urlStr;
                uploadListener.updateBlock(100);
                [NSThread sleepForTimeInterval:0.2];
                uploadListener.successBlock(urlStr);
            }
        }];
    } progress:^(int progress, long messageId) {
        RCMessageCellNotificationModel *notifyModel = [[RCMessageCellNotificationModel alloc] init];
        notifyModel.actionName = CONVERSATION_CELL_STATUS_SEND_PROGRESS;
        notifyModel.messageId = messageId;
        notifyModel.progress = progress;
        dispatch_async(dispatch_get_main_queue(), ^{
            [[NSNotificationCenter defaultCenter] postNotificationName:KNotificationMessageBaseCellUpdateSendingStatus
                                                                object:notifyModel];
        });
    } success:^(long messageId) {
        RCMessageCellNotificationModel *notifyModel = [[RCMessageCellNotificationModel alloc] init];
        notifyModel.actionName = CONVERSATION_CELL_STATUS_SEND_SUCCESS;
        notifyModel.messageId = messageId;
        dispatch_async(dispatch_get_main_queue(), ^{
            for (RCMessageModel *model in self.conversationDataRepository) {
                if (model.messageId == messageId) {
                    [self.connectListModel.messageArray addObject:model];
                    model.sentStatus = SentStatus_SENT;
                    break;
                }
            }
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)),dispatch_get_main_queue(), ^{
                [[NSNotificationCenter defaultCenter]postNotificationName:KNotificationMessageBaseCellUpdateSendingStatus object:notifyModel];
            });
        });
        
    } error:^(RCErrorCode errorCode, long messageId) {
        RCMessageCellNotificationModel *notifyModel =[[RCMessageCellNotificationModel alloc] init];
        notifyModel.actionName = CONVERSATION_CELL_STATUS_SEND_FAILED;
        notifyModel.messageId = messageId;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, NSEC_PER_SEC * 0.3f),dispatch_get_main_queue(), ^{
            for (RCMessageModel *model in self.conversationDataRepository) {
                if (model.messageId == messageId) {
                    model.sentStatus = SentStatus_FAILED;
                    break;
                }
            }
            [[NSNotificationCenter defaultCenter]postNotificationName: KNotificationMessageBaseCellUpdateSendingStatus object:notifyModel];
        });
    }];
    __weakself.tempMessage = rcMessage;
    dispatch_async(dispatch_get_main_queue(), ^{
        [__weakself appendAndDisplayMessage:__weakself.tempMessage];
    });
}

//图片压缩到指定大小
- (UIImage*)imageByScalingAndCroppingForSize:(CGSize)targetSize andImage:(UIImage *)image {
    
    UIImage *sourceImage = image;
    UIImage *newImage = nil;
    CGSize imageSize = sourceImage.size;
    CGFloat width = imageSize.width;
    CGFloat height = imageSize.height;
    CGFloat targetWidth = targetSize.width;
    CGFloat targetHeight = targetSize.height;
    CGFloat scaleFactor = 0.0;
    CGFloat scaledWidth = targetWidth;
    CGFloat scaledHeight = targetHeight;
    CGPoint thumbnailPoint = CGPointMake(0.0,0.0);
    
    if (CGSizeEqualToSize(imageSize, targetSize) == NO)
    {
        CGFloat widthFactor = targetWidth / width;
        CGFloat heightFactor = targetHeight / height;
        
        if (widthFactor > heightFactor)
            scaleFactor = widthFactor; // scale to fit height
        else
            scaleFactor = heightFactor; // scale to fit width
        scaledWidth= width * scaleFactor;
        scaledHeight = height * scaleFactor;
        
        // center the image
        if (widthFactor > heightFactor)
        {
            thumbnailPoint.y = (targetHeight - scaledHeight) * 0.5;
        }
        else if (widthFactor < heightFactor)
        {
            thumbnailPoint.x = (targetWidth - scaledWidth) * 0.5;
        }
    }
    
    UIGraphicsBeginImageContext(targetSize); // this will crop
    
    CGRect thumbnailRect = CGRectZero;
    thumbnailRect.origin = thumbnailPoint;
    thumbnailRect.size.width= scaledWidth;
    thumbnailRect.size.height = scaledHeight;
    
    [sourceImage drawInRect:thumbnailRect];
    
    newImage = UIGraphicsGetImageFromCurrentImageContext();
    if(newImage == nil)
        NSLog(@"could not scale image");
    
    //pop the context to get back to the default
    UIGraphicsEndImageContext();
    return newImage;
}

- (void)sendMessage:(RCMessageContent *)messageContent pushContent:(NSString *)pushContent andText:(NSString *)text{
    
    if (_targetId == nil) {
        return;
    }
    __weak typeof(&*self) __weakself = self;
    
    if (messageContent == nil) {
        return;
    }
    
    /**
     *  WY添加
     */
    NSTimeInterval time=(long)([[NSDate date] timeIntervalSince1970]*1000);
    
    NSDictionary *dict = [WYTool msgWithText:text andUserData:self.informationModel andTime:time];
    //拼字典
    RCTextMessage *msg = (RCTextMessage *)messageContent;
    msg.content = @"text";
    msg.extra = (NSString *)dict;
    
    [self closeInputSugView];
    
    RCMessage *rcMessage = [[RCIMClient sharedRCIMClient] sendMessage:self.conversationType targetId:self.targetId content:messageContent pushContent:pushContent success:^(long messageId) {
        RCMessageCellNotificationModel *notifyModel = [[RCMessageCellNotificationModel alloc] init];
        notifyModel.actionName = CONVERSATION_CELL_STATUS_SEND_SUCCESS;
        notifyModel.messageId = messageId;
        dispatch_async(dispatch_get_main_queue(), ^{
            for (RCMessageModel *model in self.conversationDataRepository) {
                if (model.messageId == messageId) {
                    [self.connectListModel.messageArray addObject:model];
                    model.sentStatus = SentStatus_SENT;
                    break;
                }
            }
            [[NSNotificationCenter defaultCenter] postNotificationName:KNotificationMessageBaseCellUpdateSendingStatus object:notifyModel];
        });
    } error:^(RCErrorCode nErrorCode, long messageId) {
        RCMessageCellNotificationModel *notifyModel = [[RCMessageCellNotificationModel alloc] init];
        notifyModel.actionName = CONVERSATION_CELL_STATUS_SEND_FAILED;
        notifyModel.messageId = messageId;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, NSEC_PER_SEC * 0.1f),dispatch_get_main_queue(), ^{
            for (RCMessageModel *model in self.conversationDataRepository) {
                if (model.messageId == messageId) {
                    model.sentStatus = SentStatus_FAILED;
                    break;
                }
            }
            [[NSNotificationCenter defaultCenter]postNotificationName:KNotificationMessageBaseCellUpdateSendingStatus object:notifyModel];
        });
        
        RCInformationNotificationMessage *informationNotifiMsg = nil;
        if (NOT_IN_DISCUSSION == nErrorCode) {
            informationNotifiMsg = [RCInformationNotificationMessage notificationWithMessage:NSLocalizedString(@"NOT_IN_DISCUSSION", @"") extra:nil];
        } else if (NOT_IN_GROUP == nErrorCode) {
            informationNotifiMsg = [RCInformationNotificationMessage notificationWithMessage:NSLocalizedString(@"NOT_IN_GROUP", @"") extra:nil];
        } else if (NOT_IN_CHATROOM == nErrorCode) {
            informationNotifiMsg = [RCInformationNotificationMessage notificationWithMessage:NSLocalizedString(@"NOT_IN_CHATROOM", @"") extra:nil];
        } else if (REJECTED_BY_BLACKLIST == nErrorCode) {
            informationNotifiMsg = [RCInformationNotificationMessage notificationWithMessage:NSLocalizedString(@"Message rejected", @"") extra:nil];
        }
        if (nil != informationNotifiMsg) {
            RCMessage *tempMessage = [[RCIMClient sharedRCIMClient] insertMessage:self.conversationType targetId:self.targetId senderUserId:nil sendStatus:SentStatus_SENT content:informationNotifiMsg];
            dispatch_async(dispatch_get_main_queue(), ^{
                [__weakself appendAndDisplayMessage:tempMessage];
            });
        }
    }];
    
    self.tempMessage = rcMessage;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self.tempMessage) {
            [__weakself appendAndDisplayMessage:__weakself.tempMessage];
        }
    });
}

- (void)sendMessage:(RCMessageContent *)messageContent pushContent:(NSString *)pushContent {
    
    if (_targetId == nil) {
        return;
    }
    __weak typeof(&*self) __weakself = self;
    
    if (messageContent == nil) {
        return;
    }
    
    /**
     *  WY添加
     */
    NSTimeInterval time=(long)([[NSDate date] timeIntervalSince1970]*1000);
    
    NSDictionary *dict = [WYTool msgWithText:self.chatSessionInputBarControl.inputTextView.text andUserData:self.informationModel andTime:time];
    //拼字典
    RCTextMessage *msg = (RCTextMessage *)messageContent;
    msg.content = @"text";
    msg.extra = (NSString *)dict;
    
    [self closeInputSugView];
    
    RCMessage *rcMessage = [[RCIMClient sharedRCIMClient] sendMessage:self.conversationType targetId:self.targetId content:messageContent pushContent:pushContent success:^(long messageId) {
        RCMessageCellNotificationModel *notifyModel = [[RCMessageCellNotificationModel alloc] init];
        notifyModel.actionName = CONVERSATION_CELL_STATUS_SEND_SUCCESS;
        notifyModel.messageId = messageId;
        dispatch_async(dispatch_get_main_queue(), ^{
            for (RCMessageModel *model in self.conversationDataRepository) {
                if (model.messageId == messageId) {
                    [self.connectListModel.messageArray addObject:model];
                    model.sentStatus = SentStatus_SENT;
                    break;
                }
            }
            [[NSNotificationCenter defaultCenter] postNotificationName:KNotificationMessageBaseCellUpdateSendingStatus object:notifyModel];
        });
    } error:^(RCErrorCode nErrorCode, long messageId) {
        RCMessageCellNotificationModel *notifyModel = [[RCMessageCellNotificationModel alloc] init];
        notifyModel.actionName = CONVERSATION_CELL_STATUS_SEND_FAILED;
        notifyModel.messageId = messageId;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, NSEC_PER_SEC * 0.1f),dispatch_get_main_queue(), ^{
            for (RCMessageModel *model in self.conversationDataRepository) {
                if (model.messageId == messageId) {
                    model.sentStatus = SentStatus_FAILED;
                    break;
                }
            }
            [[NSNotificationCenter defaultCenter]postNotificationName:KNotificationMessageBaseCellUpdateSendingStatus object:notifyModel];
        });
        
        RCInformationNotificationMessage *informationNotifiMsg = nil;
        if (NOT_IN_DISCUSSION == nErrorCode) {
            informationNotifiMsg = [RCInformationNotificationMessage notificationWithMessage:NSLocalizedString(@"NOT_IN_DISCUSSION", @"") extra:nil];
        } else if (NOT_IN_GROUP == nErrorCode) {
            informationNotifiMsg = [RCInformationNotificationMessage notificationWithMessage:NSLocalizedString(@"NOT_IN_GROUP", @"") extra:nil];
        } else if (NOT_IN_CHATROOM == nErrorCode) {
            informationNotifiMsg = [RCInformationNotificationMessage notificationWithMessage:NSLocalizedString(@"NOT_IN_CHATROOM", @"") extra:nil];
        } else if (REJECTED_BY_BLACKLIST == nErrorCode) {
            informationNotifiMsg = [RCInformationNotificationMessage notificationWithMessage:NSLocalizedString(@"Message rejected", @"") extra:nil];
        }
        if (nil != informationNotifiMsg) {
            RCMessage *tempMessage = [[RCIMClient sharedRCIMClient] insertMessage:self.conversationType targetId:self.targetId senderUserId:nil sendStatus:SentStatus_SENT content:informationNotifiMsg];
            dispatch_async(dispatch_get_main_queue(), ^{
                [__weakself appendAndDisplayMessage:tempMessage];
            });
        }
    }];
    
    self.tempMessage = rcMessage;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self.tempMessage) {
            [__weakself appendAndDisplayMessage:__weakself.tempMessage];
        }
    });
}

// 复制
- (void)onCopyMessage:(id)sender { // 只能复制文本信息
    
    [MobClick event:@"chatroom_copyMsg_click"];
    self.chatSessionInputBarControl.inputTextView.disableActionMenu = NO;
    
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    
    if ([_longPressSelectedModel.content isKindOfClass:[RCTextMessage class]]) {
        
        RCTextMessage *text = (RCTextMessage *)_longPressSelectedModel.content;
        
        [pasteboard setString:text.content];
        
    } else if ([_longPressSelectedModel.content isKindOfClass:[RCImageMessage class]]) {
        RCImageMessage *image = (RCImageMessage *)_longPressSelectedModel.content;
        [pasteboard setImage:image.originalImage];
    }
}

// 删除
- (void)onDeleteMessage:(id)sender {
    
    [MobClick event:@"chatroom_deleteMsg_click"];
    self.chatSessionInputBarControl.inputTextView.disableActionMenu = NO;
    
    RCMessageModel *model = _longPressSelectedModel;
    
    //删除消息时如果是当前播放的消息就停止播放
    if ([RCVoicePlayer defaultPlayer].isPlaying && [RCVoicePlayer defaultPlayer].messageId != nil && [[RCVoicePlayer defaultPlayer].messageId isEqualToString:[NSString stringWithFormat:@"%ld",model.messageId]] ) {
        [[RCVoicePlayer defaultPlayer] stopPlayVoice];
    }
    
    [self deleteMessage:model];
}

-(void)loveChatRoomMsg{
    
    [self cellClickLoveButton];
}

- (void)onSelectModelMessage:(id)sender{
    
    [self layoutBottomBarInLong];
    [MobClick event:@"chatroom_multiSelect_click"];
    for (NSIndexPath *indexPath in [self.conversationMessageCollectionView indexPathsForVisibleItems]) {
        _isMoreSelect = YES;
        RCMessageCell *messageCell = (RCMessageCell *)[self.conversationMessageCollectionView cellForItemAtIndexPath:indexPath];
        if ([messageCell isKindOfClass:[WYSeparateCollectionViewCell class]]) {
            continue;
        }
        [messageCell cellSubViewsMoveToRight];
    }
    UIBarButtonItem *leftItem = [UIBarButtonItem itemWithTitle:@"取消" target:self action:@selector(quitMoreSelected) left:YES];
    self.navigationItem.leftBarButtonItem = leftItem;
    self.navigationItem.rightBarButtonItem = nil;
    
    [self.shareToWechatButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.chatSessionInputBarControl.bottom);
        make.left.equalTo(self.chatSessionInputBarControl);
        make.width.equalTo(self.chatSessionInputBarControl.width);
        make.height.equalTo(50);
    }];
    __weak typeof(self) __weakSelf = self;
    [UIView animateWithDuration:0.3 animations:^{
        __weakSelf.shareToWechatButton.transform = CGAffineTransformMakeTranslation(0, -self.chatSessionInputBarControl.h);
    }];
    
    [_conversationMessageCollectionView updateConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(_chatSessionInputBarControl.top);
    }];
}

-(void)quitMoreSelected{
    
    for (NSIndexPath *indexPath in [self.conversationMessageCollectionView indexPathsForVisibleItems]) {
        _isMoreSelect = NO;
        RCMessageCell *messageCell = (RCMessageCell *)[self.conversationMessageCollectionView cellForItemAtIndexPath:indexPath];
        if ([messageCell isKindOfClass:[WYSeparateCollectionViewCell class]]) {
            continue;
        }
        [messageCell cellSubViewsBack];
    }
    NSString *str = self.unReadNumStr.integerValue == 0 ? @"已接入":[NSString stringWithFormat:@"(%d)已接入", self.unReadNumStr.intValue];
    UIBarButtonItem *leftItem = [UIBarButtonItem itemWithTitle:str target:self action:@selector(popViewControllerToConnect) left:YES];
    self.navigationItem.leftBarButtonItem = leftItem;
    
    UIBarButtonItem *rightItem = [UIBarButtonItem itemWithTitle:@"操作" target:self action:@selector(operateMenuList) left:NO];
    self.navigationItem.rightBarButtonItem = rightItem;
    [self.cellSelectedArr removeAllObjects];
    __weak typeof(self) __weakSelf = self;
    [UIView animateWithDuration:0.3 animations:^{
        __weakSelf.shareToWechatButton.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished) {
        [__weakSelf.shareToWechatButton removeFromSuperview];
        __weakSelf.shareToWechatButton = nil;
    }];
    [_chatControlBottomConstraint uninstall];
    [_chatSessionInputBarControl updateConstraints:^(MASConstraintMaker *make) {
        _chatControlBottomConstraint = make.bottom.equalTo(0);
    }];
}

#pragma mark --- WYMessageCellDelegate
-(void)cellDidClick:(RCMessageCell *)cell{
    
    NSIndexPath * indexPath = [self.conversationMessageCollectionView indexPathForCell:cell];
    
    [self.cellSelectedArr addObject:@((NSInteger)self.conversationDataRepository.count - indexPath.row)];
}

-(void)cellCancel:(RCMessageCell *)cell{
    
    NSIndexPath * indexPath = [self.conversationMessageCollectionView indexPathForCell:cell];
    
    [self.cellSelectedArr removeObject:@((NSInteger)self.conversationDataRepository.count - indexPath.row)];
}

- (void)deleteMessage:(RCMessageModel *)model {
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:[self findDataIndexFromMessageList:model] inSection:0];
    
    [self.conversationDataRepository removeObject:model];
    
    [self.conversationMessageCollectionView deleteItemsAtIndexPaths:[NSArray arrayWithObject:indexPath]];
}

// 设置头像样式
- (void)setMessageAvatarStyle:(RCUserAvatarStyle)avatarStyle {
    
    [RCIM sharedRCIM].globalMessageAvatarStyle = avatarStyle;
}

// 设置头像大小
- (void)setMessagePortraitSize:(CGSize)size {
    
    [RCIM sharedRCIM].globalMessagePortraitSize = size;
}

// 点击未读信息滚动到最底部
- (void)tabRightBottomMsgCountIcon:(UIGestureRecognizer *)gesture {
    
    if (gesture.state == UIGestureRecognizerStateEnded) {
        [self scrollToBottomAnimated:YES];
    }
}

- (void)tap4ResetDefaultBottomBarStatus:(UIGestureRecognizer *)gestureRecognizer {
    
    if (gestureRecognizer.state == UIGestureRecognizerStateEnded) {
        if (_currentBottomBarStatus != KBottomBarDefaultStatus) {
            [self layoutBottomBarWithStatus:KBottomBarDefaultStatus];
        }
    }
}

- (BOOL)isAtTheBottomOfTableView {
    
    if (self.conversationMessageCollectionView.contentSize.height <= self.conversationMessageCollectionView.frame.size.height) {
        return YES;
    }
    NSIndexPath *lastPath = [self getLastIndexPathForVisibleItems];
    if (lastPath.row >= self.conversationDataRepository.count -2) {
        return YES;
    }else{
        return NO;
    }
}

- (UIWebView *)orderWebView {
    
    if(_orderWebView == nil) {
        _orderWebView = [[UIWebView alloc]init];
        _orderWebView.delegate = self;
        NSString *urlString = [NSString stringWithFormat:@"%@%d", /*TEST_ORDER_URL*/ORDER_URL, self.connectListModel.inner_uid];
        NSURL *url = [NSURL URLWithString:urlString];
        [_orderWebView loadRequest:[NSURLRequest requestWithURL:url]];
        [self.view addSubview:_orderWebView];
        [self.orderWebView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.view.right);
            make.width.bottom.equalTo(self.view);
            make.top.equalTo(self.view).offset(64 + 44);
        }];
    }
    return _orderWebView;
}

- (void) webViewDidStartLoad:(UIWebView *)webView {
    
    [MBProgressHUD showMessage:@"正在加载..." toView:self.orderWebView];
}

- (void) webViewDidFinishLoad:(UIWebView *)webView {
    
    // 隐藏提醒框
    [MBProgressHUD hideHUDForView:self.orderWebView animated:YES];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    
    if (webView.isLoading) {
        return;
    }
    // 隐藏提醒框
    [MBProgressHUD hideHUDForView:self.orderWebView animated:YES];
}

- (NSMutableArray *)cellSelectedArr {
    
    if(_cellSelectedArr == nil) {
        _cellSelectedArr = [[NSMutableArray alloc] init];
    }
    return _cellSelectedArr;
}

- (UIButton *)shareToWechatButton {
    
    if(_shareToWechatButton == nil) {
        _shareToWechatButton = [UIButton buttonWithType:UIButtonTypeSystem];
        _shareToWechatButton.backgroundColor = [UIColor whiteColor];
        [_shareToWechatButton setTitle:@"转发给微信好友" forState:UIControlStateNormal];
        [_shareToWechatButton setTitleColor:[UIColor colorWithRed:0.1686 green:0.5176 blue:1.0 alpha:1.0] forState:UIControlStateNormal];
        _shareToWechatButton.titleLabel.font = [UIFont systemFontOfSize:16];
        [_shareToWechatButton addTarget:self action:@selector(shareToWechat) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:_shareToWechatButton];
    }
    return _shareToWechatButton;
}

#pragma mark --- 转发到微信
-(void)shareToWechat{
    
    [self sortCellSelectArrOrderedDescending:self.cellSelectedArr];
    [self shareMsg:self.cellSelectedArr];
    [self quitMoreSelected];
}

// 降序
-(void)sortCellSelectArrOrderedDescending:(NSMutableArray *)mutableArray{
    
    [mutableArray sortUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
        if ([obj1 integerValue] > [obj2 integerValue]) {
            return (NSComparisonResult)NSOrderedAscending;
        }
        if ([obj1 integerValue] < [obj2 integerValue]) {
            return (NSComparisonResult)NSOrderedDescending;
        }
        return (NSComparisonResult)NSOrderedSame;
    }];
}

-(void)shareMsg:(NSMutableArray *)mutableArray{
    
    if (mutableArray.count == 1) {
        RCMessageModel *msgModel = self.conversationDataRepository[self.conversationDataRepository.count - ((NSNumber *)mutableArray.lastObject).integerValue];
        if ([msgModel.content isKindOfClass:[RCImageMessage class]]) {
            WXMediaMessage *message = [WXMediaMessage message];
            WXImageObject *ext = [WXImageObject object];
            ext.imageData = UIImagePNGRepresentation(((RCImageMessage *)msgModel.content).originalImage);
            message.mediaObject = ext;
            SendMessageToWXReq* req = [[SendMessageToWXReq alloc] init];
            req.bText = NO;
            req.message = message;
            req.scene = WXSceneSession;
            [WXApi sendReq:req];
        }
    } else if (mutableArray.count > 1) {
    }
    NSMutableString *str = [NSMutableString string];
    for (NSNumber *num in mutableArray) {
        RCMessageModel *msgModel = self.conversationDataRepository[self.conversationDataRepository.count - num.integerValue];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:msgModel.sentTime/1000];
        NSString *dateStr = [formatter stringFromDate:date];
        
        if ([msgModel.content isKindOfClass:[RCTextMessage class]]) {
            if (msgModel.messageDirection == MessageDirection_RECEIVE) {
                [str appendFormat:@"%@\nUser:%@\n", dateStr,((RCTextMessage *)msgModel.content).content];
            }
            else {
                NSInteger geniusId = ((NSNumber *)((NSDictionary *)((RCTextMessage *)msgModel.content).extra)[@"genius_info"][@"genius_id"]).integerValue;
                if (geniusId == 1896206 || geniusId == 1896207 || geniusId == 1896208 || geniusId == 1896209) {
                    [str appendFormat:@"%@\nAI:%@\n", dateStr, ((RCTextMessage *)msgModel.content).content];
                } else {
                    [str appendFormat:@"%@\nGenius:%@\n", dateStr, ((RCTextMessage *)msgModel.content).content];
                }
            }
        }
    }
    SendMessageToWXReq* req = [[SendMessageToWXReq alloc] init];
    req.text = str;
    req.bText = YES;
    req.scene = WXSceneSession;
    [WXApi sendReq:req];
}

- (WYChatRoomSugView *)chatRoomSugView {
    
	if(_chatRoomSugView == nil) {
		_chatRoomSugView = [[WYChatRoomSugView alloc] init];
        [self.view addSubview:_chatRoomSugView];
        _chatRoomSugView.wyChatRoomSugViewDelegate = self;
        _chatRoomSugView.frame = CGRectMake([UIScreen mainScreen].bounds.size.width - 30 - 135, self.chatSessionInputBarControl.y-90, 30+135, 90);
        [_chatRoomSugView makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.chatSessionInputBarControl.top).offset(-90);
            make.right.equalTo(self.view);
            make.width.equalTo(30+135);
            make.height.equalTo(90);
        }];
	}
	return _chatRoomSugView;
}

-(void)cellClickLoveButton{
    
    WYChatRoomLoveViewController *chatRoom = [[WYChatRoomLoveViewController alloc]initWithConnectListModel:self.connectListModel andPage:self.pageCount];
    chatRoom.listDataArray = [self.conversationDataRepository copy];
    [self.navigationController pushViewController:chatRoom animated:YES];
}

-(void)didTouchCloseSugButton:(UIButton *)sender{
    
    [self closeInputSugView];
}

@end
