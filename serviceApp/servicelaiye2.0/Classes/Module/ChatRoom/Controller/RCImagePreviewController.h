//
//  PreviewViewController.h
//  RCIM
//
//  Created by Heq.Shinoda on 14-5-27.
//  Copyright (c) 2014年 Heq.Shinoda. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RCImageMessageProgressView;

@class RCMessageModel;

@interface RCImagePreviewController : UIViewController

// 原始图片视图
@property(nonatomic, strong) UIImageView *originalImageView;

// message数据模型
@property(nonatomic, strong) RCMessageModel *messageModel;

//  message 图片进图视图
@property(nonatomic, strong) RCImageMessageProgressView *rcImageProressView;

@end