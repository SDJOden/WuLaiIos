//
//  WYSeparateModel.m
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/3/26.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import "WYSeparateModel.h"

@implementation WYSeparateModel

- (instancetype)initWithDict:(NSDictionary *)dict
{
    if (self = [super init]) {
        [self setValuesForKeysWithDictionary:dict];
    }
    return self;
}

+ (instancetype)modelWithDict:(NSDictionary *)dict
{
    return [[self alloc]initWithDict:dict];
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key {
}

- (NSString *)description {
    
    return [NSString stringWithFormat:@"{textContent=%@}", _textContent];
}

@end
