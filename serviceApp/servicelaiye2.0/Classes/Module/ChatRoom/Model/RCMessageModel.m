//
//  RongMessageModel.m
//  RongIMKit
//
//  Created by xugang on 15/1/22.
//  Copyright (c) 2015年 RongCloud. All rights reserved.
//

#import "RCMessageModel.h"

@implementation RCMessageModel

- (instancetype)initWithMessage:(RCMessage *)rcMessage {
    
    self = [super init];
    if (self) {
        self.conversationType = rcMessage.conversationType;
        self.targetId = rcMessage.targetId;
        self.messageId = rcMessage.messageId;
        self.messageDirection = rcMessage.messageDirection;
        self.senderUserId = rcMessage.senderUserId;
        self.receivedStatus = rcMessage.receivedStatus;
        self.sentStatus = rcMessage.sentStatus;
        self.sentTime = rcMessage.sentTime;
        self.objectName = rcMessage.objectName;
        self.content = rcMessage.content;
        self.isDisplayMessageTime = NO;
        self.userInfo = nil;
        self.receivedTime = rcMessage.receivedTime;
        self.extra = rcMessage.extra;
    }

    return self;
}

-(instancetype)initWithDict:(NSDictionary *)dict{
    
    if (self = [super init]) {
        self.isDisplayMessageTime = YES;
        self.isDisplayNickname = YES;
        self.userInfo = [[RCUserInfo alloc]init];
        self.conversationType = ConversationType_PRIVATE;
        self.targetId = @"rcg0";
        self.messageId = ((NSNumber *)dict[@"extra"][@"msg_id"]).longLongValue;
        self.messageDirection = 2 - ((NSNumber *)dict[@"extra"][@"direction"]).intValue;
        self.senderUserId = @"rcg0";
        self.receivedStatus = 1;
        self.sentStatus = 30;
        self.receivedTime = ((NSNumber *)dict[@"extra"][@"msg_ts"]).longLongValue;
        self.sentTime = ((NSNumber *)dict[@"extra"][@"msg_ts"]).longLongValue;
    }
    return self;
}

@end
