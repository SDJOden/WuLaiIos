//
//  SDJCard_Items.m
//  geniuslaiye2.0
//
//  Created by 盛东 on 15/12/8.
//  Copyright © 2015年 shengdong. All rights reserved.
//

#import "SDJCard_Items.h"

@implementation SDJCard_Items

- (instancetype)initWithDict:(NSDictionary *)dict
{
    if (self = [super init]) {
        [self setValuesForKeysWithDictionary:dict];
    }
    return self;
}

+ (instancetype)modelWithDict:(NSDictionary *)dict
{
    return [[self alloc]initWithDict:dict];
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key {
}

- (NSString *)description {
    
    return [NSString stringWithFormat:@"{Desc=%@,Effect=%@,ImageUrl=%@, Name=%@,ProviderId=%@,Url=%@, ProviderLogoUrl=%@,ProviderName=%@,Amount=%@}", _Desc, _Effect, _ImageUrl, _Name, _ProviderId, _Url, _ProviderLogoUrl, _ProviderName, _Amount];
}

@end
