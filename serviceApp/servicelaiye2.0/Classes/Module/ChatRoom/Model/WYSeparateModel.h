//
//  WYSeparateModel.h
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/3/26.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WYSeparateModel : RCMessageContent

@property (nonatomic, copy) NSString *textContent;

+ (instancetype)modelWithDict:(NSDictionary *)dict;

- (instancetype)initWithDict:(NSDictionary *)dict;

@end
