//
//  RongMessageModel.h
//  RongIMKit
//
//  Created by xugang on 15/1/22.
//  Copyright (c) 2015年 RongCloud. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RCMessageModel : NSObject

@property (nonatomic, assign) BOOL loveBtnEnable;

@property(nonatomic, assign) BOOL isDisplayMessageTime;

@property(nonatomic, assign) BOOL isDisplayNickname;
/** 用户信息 */
@property(nonatomic, strong) RCUserInfo *userInfo;
/** 会话类型 */
@property(nonatomic, assign) RCConversationType conversationType;
/** 目标ID，如讨论组ID, 群ID, 聊天室ID */
@property(nonatomic, copy) NSString *targetId;
/** 消息ID */
@property(nonatomic, assign) long messageId;
/** 消息方向 */
@property(nonatomic, assign) RCMessageDirection messageDirection;
/** 发送者ID */
@property(nonatomic, copy) NSString *senderUserId;
/** 接受状态 */
@property(nonatomic, assign) RCReceivedStatus receivedStatus;
/**发送状态 */
@property(nonatomic, assign) RCSentStatus sentStatus;
/** 接收时间 */
@property(nonatomic, assign) long long receivedTime;
/**发送时间 */
@property(nonatomic, assign) long long sentTime;
/** 消息体名称 */
@property(nonatomic, copy) NSString *objectName;
/** 消息内容 */
@property(nonatomic, strong) RCMessageContent *content;
/** 附加字段 */
@property(nonatomic, copy) NSString *extra;

- (instancetype)initWithMessage:(RCMessage *)rcMessage;

-(instancetype)initWithDict:(NSDictionary *)dict;

@end
