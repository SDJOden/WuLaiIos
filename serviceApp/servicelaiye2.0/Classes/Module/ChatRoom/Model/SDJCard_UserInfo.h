//
//  SDJCard_UserInfo.h
//  geniuslaiye2.0
//
//  Created by 盛东 on 15/12/8.
//  Copyright © 2015年 shengdong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SDJCard_UserInfo : NSObject

@property (nonatomic, copy)NSString *MsgId;

@property (nonatomic, copy)NSString *Ts;

@property (nonatomic, copy)NSString *UserId;

@property (nonatomic, copy)NSString *GeniusId;

@property (nonatomic, copy)NSString *Source;

@property (nonatomic, copy)NSString *SourceUserId;

@property (nonatomic, copy)NSString *UserLevel;

@property (nonatomic, copy)NSString *GroupId;

- (instancetype)initWithDict:(NSDictionary *)dict;

+ (instancetype)modelWithDict:(NSDictionary *)dict;

@end
