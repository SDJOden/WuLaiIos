//
//  SDJCard_UserInfo.m
//  geniuslaiye2.0
//
//  Created by 盛东 on 15/12/8.
//  Copyright © 2015年 shengdong. All rights reserved.
//

#import "SDJCard_UserInfo.h"

@implementation SDJCard_UserInfo

- (instancetype)initWithDict:(NSDictionary *)dict
{
    if (self = [super init]) {
        
        [self setValuesForKeysWithDictionary:dict];
    }
    return self;
}

+ (instancetype)modelWithDict:(NSDictionary *)dict
{
    return [[self alloc]initWithDict:dict];
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key {
}

- (NSString *)description {
    
    return [NSString stringWithFormat:@"{MsgId=%@,Ts=%@,UserId=%@}", _MsgId, _Ts, _UserId];
}

@end
