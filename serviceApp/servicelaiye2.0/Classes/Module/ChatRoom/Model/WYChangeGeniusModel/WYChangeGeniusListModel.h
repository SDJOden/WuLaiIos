//
//  WYChangeGeniusListModel.h
//  servicelaiye2.0
//
//  Created by 汪洋 on 15/12/29.
//  Copyright © 2015年 shengdong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WYChangeGeniusListModel : NSObject

@property (nonatomic, copy) NSString *accept_limit;

@property (nonatomic, assign) int cur_accept_cnt;

@property (nonatomic, assign) int genius_id;

@property (nonatomic, copy) NSString *phone;

@property (nonatomic, copy) NSString *real_name;

@property (nonatomic, copy) NSString *username;

@end
