//
//  WYMenuViewController.m
//  laiye
//
//  Created by 汪洋 on 15/12/2.
//  Copyright © 2015年 汪洋. All rights reserved.
//

#import "WYMenuViewController.h"
#import "WYWaitingPoorController.h"
#import "WYConnectViewController.h"
#import "WYNavigationViewController.h"
#import "WYHistoryMessageViewController.h"
#import "WYGeniusInformationViewController.h"
#import "WYLoveSessionViewController.h"
#import "JZTabbar.h"

@interface WYMenuViewController () <JZTabbarDelegate>
//自己定义的tabBar替代原生的
@property(nonatomic, weak) JZTabbar * customTabbar;

//@property(nonatomic, strong)NSMutableArray *array;

@end

@implementation WYMenuViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.tabBar.translucent = NO;
    // 设置tabBar
    [self setTabar];
    // 初始化所有的子控制器
    [self setupAllChildViewControllers];
    
    //获取导航条最高权限
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:18], NSForegroundColorAttributeName:[UIColor whiteColor]}];
    [[UINavigationBar appearance] setBarTintColor:NAVIGATIONBAR_BACKGROUDCOLOR];
    
    [[UITabBar appearance] setTintColor:TABBAT_TINTCOLOR];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    for (UIView * child in self.tabBar.subviews)
    {
        // 删除系统自动生成的tabbar
        if ([child isKindOfClass:[UIControl class]])
        {
            [child removeFromSuperview];
        }
    }
}

/**
 * 加载tabbar
 */
-(void)setTabar
{
    JZTabbar * customTabbar = [[JZTabbar alloc]init];
    customTabbar.delegate = self;
    customTabbar.frame = self.tabBar.bounds;
    [self.tabBar addSubview:customTabbar];
    self.customTabbar = customTabbar;
}

/**
 * 实现代理方法
 */
-(void)tabBar:(JZTabbar *)tabBar didSelectedButtonFrom:(NSInteger)from to:(NSInteger)to
{
    self.selectedIndex = to;
    // 这个应该注释掉.
//    [self.customTabbar selectItemWithIndex:1];
}

-(void)setupAllChildViewControllers{
    
    WYConnectViewController *connectVC = [[WYConnectViewController alloc]init];
    [self setupChildViewController:connectVC title:@"当前" imageName:@"now_tab" selectedImageName:@"now_tab_selected"];
    WYWaitingPoorController *waitUserVC = [[WYWaitingPoorController alloc]init];
    [self setupChildViewController:waitUserVC title:@"待接入" imageName:@"wait_tab" selectedImageName:@"wait_tab_selected"];
    WYLoveSessionViewController *loveVC = [[WYLoveSessionViewController alloc]init];
    [self setupChildViewController:loveVC title:@"点赞" imageName:@"love_tab" selectedImageName:@"love_tab_selected"];
    WYHistoryMessageViewController *historyMsgVC = [[WYHistoryMessageViewController alloc]init];
    [self setupChildViewController:historyMsgVC title:@"历史" imageName:@"history_tab" selectedImageName:@"history_tab_selected"];
    WYGeniusInformationViewController *geniusInfoVC = [[WYGeniusInformationViewController alloc]init];
    [self setupChildViewController:geniusInfoVC title:@"我的" imageName:@"me_tab" selectedImageName:@"me_tab_selected"];
}

- (void)setupChildViewController:(UIViewController *)childVc title:(NSString *)title imageName:(NSString *)imageName selectedImageName:(NSString *)selectedImageName
{
    // 1.设置控制器的属性
    childVc.title = title;
    // 设置图标
    childVc.tabBarItem.image = [UIImage imageNamed:imageName];
    // 设置选中的图标
    UIImage *selectedImage = [UIImage imageNamed:selectedImageName];
    childVc.tabBarItem.selectedImage = selectedImage;
    // 2.包装一个导航控制器
    WYNavigationViewController *nav = [[WYNavigationViewController alloc] initWithRootViewController:childVc];
    [self addChildViewController:nav];
    // 添加tabbar内部的按钮
    [self.customTabbar addTabBarWithItem:nav.tabBarItem];
}

-(void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item{
    
    //点击tabBarItem执行的方法
    NSLog(@"%ld", [self.tabBar.items indexOfObject:item]);
    
    switch (self.selectedIndex) {
        case 0:
            switch ([self.tabBar.items indexOfObject:item]) {
                case 1:
                    [MobClick event:@"connect_waitingTabBarItem_click"];
                    break;
                case 2:
                    [MobClick event:@"connect_historyTabBarItem_click"];
                    break;
                case 3:
                    [MobClick event:@"connect_loveTabBarItem_click"];
                    break;
                case 4:
                    [MobClick event:@"connect_myTabBarItem_click"];
                    break;
                default:
                    break;
            }
            break;
        case 1:
            switch ([self.tabBar.items indexOfObject:item]) {
                case 0:
                    [MobClick event:@"waiting_chatroom_connect_click"];
                    break;
                case 2:
                    [MobClick event:@"waiting_historyTabBarItem_click"];
                    break;
                case 3:
                    [MobClick event:@"waiting_loveTabBarItem_click"];
                    break;
                case 4:
                    [MobClick event:@"waiting_myTabBarItem_click"];
                    break;
                default:
                    break;
            }
            break;
        case 2:
            switch ([self.tabBar.items indexOfObject:item]) {
                case 0:
                    [MobClick event:@"history_connectTabBarItem_click"];
                    break;
                case 1:
                    [MobClick event:@"history_waitingTabBarItem_click"];
                    break;
                case 3:
                    [MobClick event:@"history_historyTabBarItem_click"];
                    break;
                case 4:
                    [MobClick event:@"history_myTabBarItem_click"];
                    break;
                default:
                    break;
            }
            break;
        case 3:
            switch ([self.tabBar.items indexOfObject:item]) {
                case 0:
                    [MobClick event:@"love_connectTabBarItem_click"];
                    break;
                case 1:
                    [MobClick event:@"love_waitingTabBarItem_click"];
                    break;
                case 2:
                    [MobClick event:@"love_historyTabBarItem_click"];
                    break;
                case 4:
                    [MobClick event:@"love_myTabBarItem_click"];
                    break;
                default:
                    break;
            }
            break;
        case 4:
            switch ([self.tabBar.items indexOfObject:item]) {
                case 0:
                    [MobClick event:@"my_connectTabBarItem_click"];
                    break;
                case 1:
                    [MobClick event:@"my_waitingTabBarItem_click"];
                    break;
                case 2:
                    [MobClick event:@"my_historyTabBarItem_click"];
                    break;
                case 3:
                    [MobClick event:@"my_loveTabBarItem_click"];
                    break;
                default:
                    break;
            }
            break;
        default:
            break;
    }
}

@end
