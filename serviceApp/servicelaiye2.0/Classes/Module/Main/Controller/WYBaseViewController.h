//
//  WYBaseViewController.h
//  ReconstructionQQSlide
//
//  Created by LingLi on 15/12/24.
//  Copyright © 2015年 LingLi. All rights reserved.
//

#import <UIKit/UIKit.h>
//左右的控制器类都继承此基类

@interface WYBaseViewController : UIViewController
//左上角按钮点击方法
- (void)home;

@end
