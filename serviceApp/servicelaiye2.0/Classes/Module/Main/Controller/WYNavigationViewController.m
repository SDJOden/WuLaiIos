//
//  WYNavigationViewController.m
//  laiye
//
//  Created by 汪洋 on 15/12/3.
//  Copyright © 2015年 汪洋. All rights reserved.
//

#import "WYNavigationViewController.h"

@interface WYNavigationViewController ()

@end

@implementation WYNavigationViewController

-(instancetype)init{
    
    if (self = [super init]) {
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationBar.translucent = NO;
}

//在推出控制器的时候退药修改去除自带的tabBar，不然会有重影的现象
-(NSArray<UIViewController *> *)popToRootViewControllerAnimated:(BOOL)animated{
    [super popToRootViewControllerAnimated:animated];

    for (UIView * child in self.tabBarController.tabBar.subviews)
    {
        if ([child isKindOfClass:[UIControl class]])
        {
            [child removeFromSuperview];
        }
    }
    return nil;
}


@end
