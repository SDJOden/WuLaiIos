//
//  WYBaseViewController.m
//  ReconstructionQQSlide
//
//  Created by LingLi on 15/12/24.
//  Copyright © 2015年 LingLi. All rights reserved.
//

#import "WYBaseViewController.h"
#import "WYGeniusView.h"
#import "UIBarButtonItem+Extension.h"

@interface WYBaseViewController () <WYGeniusViewDelegate>

@property (nonatomic, strong) WYGeniusView *geniusView;

@end

@implementation WYBaseViewController 

- (void)viewDidLoad {

    [super viewDidLoad];
    
    UIBarButtonItem *leftItem = [UIBarButtonItem itemWithTitle:@"设置" target:self action:@selector(home) left:NO];
    
    self.navigationItem.leftBarButtonItem = leftItem;
}

//推出geniusView
- (void)home{
    
    [self.tabBarController.view addSubview:self.geniusView];
}

- (WYGeniusView *)geniusView {
    
	if(_geniusView == nil) {
		_geniusView = [[WYGeniusView alloc] init];
        _geniusView.frame = CGRectMake(0, -[UIScreen mainScreen].bounds.size.height, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
        _geniusView.backgroundColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.95];
        _geniusView.geniusViewDelegate = self;
#warning to do 这种代码是不该放在懒加载里面的.
        [UIView animateWithDuration:1 animations:^{
            _geniusView.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height);
        }];
	}
	return _geniusView;
}

-(void)exitGeniusView{
    
    __weak __typeof__(self) weakSelf = self;
    
    [UIView animateWithDuration:1 animations:^{
        weakSelf.geniusView.transform = CGAffineTransformMakeTranslation(0, -weakSelf.geniusView.h);
    } completion:^(BOOL finished) {
        [weakSelf.geniusView removeFromSuperview];
        weakSelf.geniusView = nil;
    }];
}
@end
