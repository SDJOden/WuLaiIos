//
//  SDJNormalWebController.m
//  geniuslaiyeOC1.0
//
//  Created by 盛东 on 15/10/24.
//  Copyright © 2015年 shengdong. All rights reserved.
//

#import "SDJNormalWebController.h"

#import "SDJCommonBtn.h"
@interface SDJNormalWebController ()

@end

@implementation SDJNormalWebController

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)close {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    UIWebView *web = [[UIWebView alloc] initWithFrame:self.view.bounds];
    
    NSMutableURLRequest *quest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:self.urlString]];

    [web loadRequest:quest];
    
    [self.view addSubview:web];
    
    SDJCommonBtn *closeBtn = [[SDJCommonBtn alloc] init];
    closeBtn.btnType = ImageOnly;
    closeBtn.ImageSize = CGSizeMake(27, 27);
    [closeBtn setImage:[UIImage imageNamed:@"registerVC_closeBtn"] forState:UIControlStateNormal];
    [closeBtn addTarget:self action:@selector(close) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:closeBtn];
    closeBtn.frame = CGRectMake(self.view.bounds.size.width - 34, 5 + 20, 29, 29);
}

@end
