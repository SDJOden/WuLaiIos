//
//  SDJNormalWebController.h
//  geniuslaiyeOC1.0
//
//  Created by 盛东 on 15/10/24.
//  Copyright © 2015年 shengdong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SDJNormalWebController : UIViewController<UIWebViewDelegate>

@property (nonatomic, copy) NSString *urlString;

@end
