//
//  SDJOriginalWebViewController.m
//  geniuslaiyeOC1.0
//
//  Created by 盛东 on 15/10/27.
//  Copyright © 2015年 shengdong. All rights reserved.
//

#import "SDJOriginalWebViewController.h"

@interface SDJOriginalWebViewController ()

@end

@implementation SDJOriginalWebViewController

- (void)viewWillLayoutSubviews { // 带导航栏, 加导航栏
    
    [super viewWillLayoutSubviews];
    
    self.view.frame = CGRectMake(0, -44, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height + 44);
}

@end
