//
//  SDJBaseWebViewController.h
//  geniuslaiyeOC1.0
//
//  Created by 盛东 on 15/10/17.
//  Copyright © 2015年 shengdong. All rights reserved.
//

// 需要内部cookie登录验证的WebView

#import <UIKit/UIKit.h>

#import "WXApi.h"

@interface SDJBaseWebViewController : UIViewController <UIWebViewDelegate, WXApiDelegate>

@property (nonatomic, strong)NSString *urlString;

@end
