//
//  SDJBaseWebViewController.m
//  geniuslaiyeOC1.0
//
//  Created by 盛东 on 15/10/17.
//  Copyright © 2015年 shengdong. All rights reserved.
//

#import "SDJBaseWebViewController.h"

#import "WebViewJavascriptBridge.h"
#import "SDJOriginalWebViewController.h"
#import "SDJNormalWebController.h"
#import "Pingpp.h"
#include <sys/socket.h> // Per msqr
#include <sys/sysctl.h>
#include <net/if.h>
#include <net/if_dl.h>
#import "SBJson4.h"
#define kUrlScheme  @"ServiceLaiye" // 这个是你定义的 URL Scheme，支付宝、微信支付和测试模式需要。

// 支付宝支付完成通知
#define SDJAliPayCompletionSuccessNotification @"SDJAliPayCompletionSuccessNotification"

@interface SDJBaseWebViewController ()

@property (nonatomic, strong)UIWebView *web;

@property WebViewJavascriptBridge* bridge;

@property (nonatomic, copy)NSString *order_id;

@end

@implementation SDJBaseWebViewController

- (void)viewWillLayoutSubviews { // 带导航栏, 不加导航栏
    
    [super viewWillLayoutSubviews];
    
    self.view.frame = CGRectMake(0, 20, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height -20);
}

- (void)loadView {
    
    UIWebView *web = [[UIWebView alloc] init];
    
    self.view = web;
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    
    self.navigationController.navigationBarHidden = NO;
    
    if (_bridge) { return; }
    
    _web = (UIWebView *)self.view;
    
    _web.delegate = self;
    
    [Pingpp setDebugMode:YES];
    
    [WebViewJavascriptBridge enableLogging];
    
    // connect
    _bridge = [WebViewJavascriptBridge bridgeForWebView:_web webViewDelegate:self handler:^(id data, WVJBResponseCallback responseCallback) {
        
        SDJBaseWebViewController * __weak weakSelf = self;
        
        if ([data[@"action"] isEqualToString:@"closeWindow"]) {
            [self dismissViewControllerAnimated:YES completion:nil];
        } else if ([data[@"action"] isEqualToString:@"openWindow"]) {
            
            SDJNormalWebController *vc = [[SDJNormalWebController alloc] init];
            vc.urlString = data[@"data"][@"url"];
            vc.title = data[@"data"][@"title"];
            
            [self.navigationController pushViewController:vc animated:YES];
            
        } else if ([data[@"action"] isEqualToString:@"logout"]) {
            // 退出
            SDJUserAccount *account = [SDJUserAccount loadAccount];
            
            account.available = NO;
            
            [account savaAccount];
            
            [[RCIMClient sharedRCIMClient] logout];
            
            [[RCIMClient sharedRCIMClient] clearMessages:ConversationType_PRIVATE targetId:[SDJUserAccount loadAccount].virtual_genius_id];
            
            [MobClick endEvent:@"registerView_time" label:@"登出"];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"SDJRootViewControllerSwitchNotification" object:@"0"];
            
        } else if ([data[@"action"] isEqualToString:@"wxShare"]) {
            
            // 分享给朋友圈
            // 网页 WXWebpageObject
            WXWebpageObject *pageObject = [[WXWebpageObject alloc] init];
            pageObject.webpageUrl = data[@"data"][@"url"]; // url参数?
        
            // 信息 WXM
            WXMediaMessage *msg = [[WXMediaMessage alloc] init];
            msg.mediaObject = pageObject;
            msg.description = data[@"data"][@"description"];
            msg.title = data[@"data"][@"title"];
            [msg setThumbImage:[UIImage imageNamed:@"hongbao"]];
            
            // Req
            SendMessageToWXReq *req = [[SendMessageToWXReq alloc] init];
            NSString *number = [NSString stringWithFormat:@"%@",data[@"data"][@"shareType"]];
            if ([number isEqualToString:@"1"]) {
                req.scene = WXSceneTimeline;
            } else if ([number isEqualToString:@"2"]) {
                req.scene = WXSceneSession;
            }
            req.message = msg;
            [WXApi sendReq:req];
            
        }else if ([data[@"action"] isEqualToString:@"callPay"]) {
            
            self.order_id = data[@"data"][@"order_no"];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [Pingpp createPayment:data[@"data"] viewController:weakSelf appURLScheme:kUrlScheme withCompletion:^(NSString *result, PingppError *error) {
                    
                    NSString *status = result;
                    
                    NSDictionary *dic = @{@"status": status, @"order_id": _order_id};
                    
                    SBJson4Writer *writer = [[SBJson4Writer alloc] init];
                    
                    NSString *jsonString = [writer stringWithObject:dic];
                    
                    [_bridge callHandler:@"onPaid" data:jsonString responseCallback:^(id responseData) {
                        
                        NSLog(@"%@", responseData);
                    }];
                }];
            });
        }
    }];
    
    // 两个协议, 退出和
    [_bridge registerHandler:@"logout" handler:^(id data, WVJBResponseCallback responseCallback) {
        // 退出
        SDJUserAccount *account = [SDJUserAccount loadAccount];
        
        account.available = NO;
        
        [account savaAccount];
        
        [[RCIMClient sharedRCIMClient] clearMessages:ConversationType_PRIVATE targetId:[SDJUserAccount loadAccount].virtual_genius_id];
        
        [[RCIM sharedRCIM] logout];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"SDJRootViewControllerSwitchNotification" object:@"0"];
    }];
    
    //打开新窗口
    [_bridge registerHandler:@"openWindow" handler:^(id data, WVJBResponseCallback responseCallback) {
        
        if (data != nil) {
            
            SDJNormalWebController *vc = [[SDJNormalWebController alloc] init];
            
            vc.title = data[@"title"];
            
            vc.urlString = data[@"url"];
            
            [self.navigationController pushViewController:vc animated:YES];
        }
    }];
    
    [self loadExamplePage:_web];
}

- (void)loadExamplePage:(UIWebView*)webView {
    
    NSString * utoken = [SDJUserAccount loadAccount].utoken;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd MM yyyy HH:mm:ss"];
    
    int time = [[NSDate date] timeIntervalSince1970];
    
    NSString *utoken2 = [utoken stringByAppendingString:[NSString stringWithFormat:@"-%d",time]];
    
    NSHTTPCookieStorage *cookies = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    NSArray *_tmpArray = [NSArray arrayWithArray:[cookies cookies]];
    for (id obj in _tmpArray) {
        [cookies deleteCookie:obj];
    }
    NSMutableDictionary *cookieProperties = [NSMutableDictionary dictionary];
    
    // 截取url, 需要正则
    NSURL *url = [NSURL URLWithString: self.urlString];
    NSString *host = url.host;
    if (url == nil || host == nil) {
        return;
    }
    [cookieProperties setObject:host forKey:NSHTTPCookieDomain];
    [cookieProperties setObject:@"utoken" forKey:NSHTTPCookieName];
    [cookieProperties setObject:utoken2 forKey:NSHTTPCookieValue];
    [cookieProperties setObject:[NSDate dateWithTimeIntervalSinceNow:60*60*60] forKey:NSHTTPCookieExpires];
    [cookieProperties setObject:@"/" forKey:NSHTTPCookiePath];
    [cookieProperties setObject:@"1" forKey:NSHTTPCookieVersion];
    
    NSHTTPCookie *cookie = [NSHTTPCookie cookieWithProperties:cookieProperties];
    [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookie:cookie];
    
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:self.urlString]];
    [webView loadRequest:request];
}

- (void)dealloc {

    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)setUrlString:(NSString *)urlString {
    
    _urlString = urlString;
}

@end
