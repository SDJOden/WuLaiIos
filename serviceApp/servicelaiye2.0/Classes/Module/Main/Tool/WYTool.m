//
//  WYTool.m
//  servicelaiye
//
//  Created by 汪洋 on 15/12/17.
//  Copyright © 2015年 汪洋. All rights reserved.
//

#import "WYTool.h"
#import "RCMessageModel.h"
#import "WYConnectInformationModel.h"
#import "WYHistoryModel.h"
#import "WYChangeGeniusListModel.h"
#import "WYGeniusModel.h"
#import "VoiceConverter.h"
#import<CommonCrypto/CommonDigest.h>

@implementation WYTool

// 以下转模型都应该放到对应的类里面去处理.
+(WYConnectInformationModel *)modelWithHistoryDict:(NSDictionary *)historyDict{
    
    //extra
    WYConnectInformationModel *connect = [[WYConnectInformationModel alloc]init];
    connect.isHasSmallRightView = NO;
    connect.unReadNum = 1;
    connect.msg_ts = [self computeTime:historyDict[@"msg_ts"]];
    connect.inner_uid = ((NSNumber *)historyDict[@"inner_uid"]).intValue;
    connect.src_uid = historyDict[@"src_uid"];
    connect.user_src = ((NSNumber *)historyDict[@"user_src"]).intValue;
    connect.direction = ((NSNumber *)historyDict[@"direction"]).intValue;
    connect.user_gid = [NSString stringWithFormat:@"rc_group_%d", connect.inner_uid] ;
    if ([SDJUserAccount loadAccount]) {
        connect.username = [SDJUserAccount loadAccount].username;
        connect.realname = [SDJUserAccount loadAccount].real_name;
    }
    return connect;
}

+(WYHistoryModel *)createHistoryModelByDict:(NSDictionary *)dict{
    
    WYHistoryModel *historyModel = [[WYHistoryModel alloc]init];
    if (![dict[@"wechat"] isKindOfClass:[NSNull class]] && ![dict[@"wechat"][@"headimgurl"] isKindOfClass:[NSNull class]]) {
        historyModel.headimgurl = dict[@"wechat"][@"headimgurl"];
    }
    if (![dict[@"inner_uid"]isKindOfClass:[NSNull class]]) {
        historyModel.inner_uid = ((NSNumber *)dict[@"inner_uid"]).longValue;
    }
    if (![dict[@"wechat"]isKindOfClass:[NSNull class]] && ![dict[@"wechat"][@"nickname"]isKindOfClass:[NSNull class]]) {
        historyModel.nickname = dict[@"wechat"][@"nickname"];
    }
    if (![dict[@"ts"]isKindOfClass:[NSNull class]]) {
        historyModel.ts = ((NSNumber *)dict[@"ts"]).longValue;
    }
    if (![dict[@"user_id"]isKindOfClass:[NSNull class]]) {
        historyModel.user_id = ((NSNumber *)dict[@"user_id"]).intValue;
    }
    if (![dict[@"phone"]isKindOfClass:[NSNull class]]) {
        historyModel.phone = ((NSNumber *)dict[@"phone"]).stringValue;
    }
    
    historyModel.vip = dict[@"vip"];
    historyModel.avatar_url = dict[@"avatar_url"];
    historyModel.db_insert_time = dict[@"db_insert_time"];
    historyModel.level = [dict[@"level"]intValue];
    historyModel.order_count = [dict[@"order_count"]intValue];
    historyModel.rc_group_id = dict[@"rc_group_id"];
    historyModel.rcu_id = dict[@"rcu_id"];
    historyModel.region = dict[@"region"];
    historyModel.up_score = [dict[@"up_score"]integerValue];
    historyModel.up_tags = dict[@"up_tags"];
    historyModel.wechat = dict[@"wechat"];
    historyModel.wechat_openid = dict[@"wechat_openid"];
    
    return historyModel;
}

+(WYChangeGeniusListModel *)createChangeGeniusListModelByDict:(NSDictionary *)dict{
    
    WYChangeGeniusListModel *changeGeniusListModel = [WYChangeGeniusListModel new];
    changeGeniusListModel.accept_limit = dict[@"accept_limit"];
    changeGeniusListModel.cur_accept_cnt = ((NSNumber *)dict[@"cur_accept_cnt"]).intValue;
    if (![dict[@"phone"]isKindOfClass:[NSNull class]]) {
        changeGeniusListModel.phone = ((NSNumber *)dict[@"phone"]).stringValue;
    }
    changeGeniusListModel.real_name = dict[@"real_name"];
    changeGeniusListModel.genius_id = ((NSNumber *)dict[@"genius_id"]).intValue;
    NSString *usernameStr = dict[@"username"];
    NSRange range = [usernameStr rangeOfString:@"@zhulilaiye"];
    changeGeniusListModel.username = [usernameStr substringToIndex:range.location];
    return changeGeniusListModel;
}

+(WYHistoryModel *)createMsgSearchListModelByDict:(NSDictionary *)dict{
    
    WYHistoryModel *listModel = [WYHistoryModel new];
    listModel.inner_uid = ((NSNumber *)dict[@"u"][@"inner_uid"]).longValue;
    listModel.headimgurl = dict[@"wx"][@"headimgurl"];
    listModel.nickname = dict[@"u"][@"wechat"][@"nickname"];
    listModel.ts = ((NSNumber *)dict[@"timestamp"]).longValue;
    listModel.user_id = ((NSNumber *)dict[@"user_id"]).intValue;
    listModel.username = dict[@"genius_username"];
    listModel.vip = dict[@"u"][@"vip"];
    listModel.phone = dict[@"u"][@"phone"];
    return listModel;
}


+(WYHistoryModel *)createRecordListModelByDict:(NSDictionary *)dict{
    
    WYHistoryModel *listModel = [WYHistoryModel new];
    listModel.inner_uid = ((NSNumber *)dict[@"u"][@"inner_uid"]).longValue;
    listModel.headimgurl = dict[@"u"][@"wechat"][@"headimgurl"];
    listModel.nickname = dict[@"u"][@"wechat"][@"nickname"];
    listModel.ts = ((NSNumber *)dict[@"user_ts"]).longValue;
    listModel.user_id = ((NSNumber *)dict[@"u"][@"user_id"]).intValue;
    listModel.realname = dict[@"genius_info"][@"realname"];
    listModel.username = dict[@"genius_info"][@"username"];
    listModel.vip = dict[@"u"][@"vip"];
    listModel.phone = [dict[@"u"][@"phone"]stringValue];
    return listModel;
}

+(NSMutableArray *)geniusArrayOfArray:(NSArray *)array{
    
    NSMutableArray *geniusArray = [NSMutableArray array];
    for (NSDictionary *dict in array) {
        WYGeniusModel *geniusModel = [[WYGeniusModel alloc]init];
        geniusModel.avatar_url = dict[@"avatar_url"];
        geniusModel.level = ((NSNumber *)dict[@"level"]).stringValue;
        geniusModel.status = dict[@"status"];
        geniusModel.genius_id = ((NSNumber *)dict[@"genius_id"]).stringValue;
        geniusModel.username = dict[@"username"];
        geniusModel.real_name = dict[@"real_name"];
        [geniusArray addObject:geniusModel];
    }
    return geniusArray;
}

+(RCMessageModel *)createMsgModel:(NSDictionary *)dict{
    
    RCMessageModel *msgModel = [[RCMessageModel alloc]initWithDict:dict];
    msgModel.objectName = @"[文字]";
    msgModel.content = [[RCTextMessage alloc]init];
    ((RCTextMessage *)msgModel.content).content = dict[@"extra"][@"msg_detail"][@"content"];
    NSDictionary *extraDict = @{@"genius_info":@{@"genius_id":dict[@"extra"][@"genius_info"][@"genius_id"],@"realname":dict[@"extra"][@"genius_info"][@"realname"],@"username":dict[@"extra"][@"genius_info"][@"username"]}, @"msg_detail":@{@"content":@"",@"sug_list":dict[@"extra"][@"msg_detail"][@"sug_list"], @"sug_domain":dict[@"extra"][@"msg_detail"][@"sug_domain"],@"action":@(1),@"effect":@""},@"routing":@(0),@"inner_uid":dict[@"extra"][@"inner_uid"],@"msg_type":dict[@"extra"][@"msg_type"],@"msg_ts":dict[@"extra"][@"msg_ts"],@"user_src":dict[@"extra"][@"user_src"],@"user_gid":dict[@"extra"][@"user_gid"],@"src_uid":dict[@"extra"][@"src_uid"]};
    ((RCTextMessage *)msgModel.content).extra = (NSString *)extraDict;
    return msgModel;
}

+(RCMessageModel *)createImageModel:(NSDictionary *)dict{
    
    RCMessageModel *msgModel = [[RCMessageModel alloc]initWithDict:dict];
    msgModel.objectName = @"[图片]";
    msgModel.content = [[RCImageMessage alloc]init];
    NSDictionary *extraDict = [[dict[@"extra"][@"msg_detail"]allKeys]containsObject:@"sug_list"] ? @{@"genius_info":@{@"genius_id":dict[@"extra"][@"genius_info"][@"genius_id"],@"realname":dict[@"extra"][@"genius_info"][@"realname"],@"username":dict[@"extra"][@"genius_info"][@"username"]}, @"msg_detail":@{@"content":dict[@"extra"][@"msg_detail"][@"content"],@"sug_list":dict[@"extra"][@"msg_detail"][@"sug_list"],@"action":@(2),@"effect":@""},@"routing":@(0),@"inner_uid":dict[@"extra"][@"inner_uid"],@"msg_type":dict[@"extra"][@"msg_type"],@"msg_ts":dict[@"extra"][@"msg_ts"],@"user_src":dict[@"extra"][@"user_src"],@"user_gid":dict[@"extra"][@"user_gid"],@"src_uid":dict[@"extra"][@"src_uid"]}:
    @{@"genius_info":@{@"genius_id":dict[@"extra"][@"genius_info"][@"genius_id"],@"realname":dict[@"extra"][@"genius_info"][@"realname"],@"username":dict[@"extra"][@"genius_info"][@"username"]}, @"msg_detail":@{@"content":dict[@"extra"][@"msg_detail"][@"content"],@"action":@(2),@"effect":@""},@"routing":@(0),@"inner_uid":dict[@"extra"][@"inner_uid"],@"msg_type":dict[@"extra"][@"msg_type"],@"msg_ts":dict[@"extra"][@"msg_ts"],@"user_src":dict[@"extra"][@"user_src"],@"user_gid":dict[@"extra"][@"user_gid"],@"src_uid":dict[@"extra"][@"src_uid"]};
    ((RCImageMessage *)msgModel.content).extra = (NSString *)extraDict;
    ((RCImageMessage *)msgModel.content).imageUrl = dict[@"imageUri"];
    ((RCImageMessage *)msgModel.content).full = NO;
    ((RCImageMessage *)msgModel.content).thumbnailImage = [UIImage imageNamed:@"placeholder"];
    ((RCImageMessage *)msgModel.content).originalImage = [UIImage imageNamed:@"placeholder"];
    return msgModel;
}

+(RCMessageModel *)createVoiceMsgModel:(NSDictionary *)dict{
    
    RCMessageModel *msgModel = [[RCMessageModel alloc]initWithDict:dict];
    msgModel.objectName = @"[语音]";
    msgModel.content = [[RCVoiceMessage alloc]init];
    NSString *audioStr = dict[@"extra"][@"msg_detail"][@"voice_url"];
    NSURL *url = [[NSURL alloc]initWithString:audioStr];
    ((RCVoiceMessage *)msgModel.content).wavAudioData = [VoiceConverter amrToWavWithAmrData:[NSData dataWithContentsOfURL:url]];
    ((RCVoiceMessage *)msgModel.content).duration = 5;
    NSDictionary *extraDict = @{@"genius_info":@{@"genius_id":dict[@"extra"][@"genius_info"][@"genius_id"],@"realname":dict[@"extra"][@"genius_info"][@"realname"],@"username":dict[@"extra"][@"genius_info"][@"username"]}, @"msg_detail":@{@"content":@"",@"action":@(5),@"effect":@""},@"routing":@(0),@"inner_uid":dict[@"extra"][@"inner_uid"],@"msg_type":dict[@"extra"][@"msg_type"],@"msg_ts":dict[@"extra"][@"msg_ts"],@"user_src":dict[@"extra"][@"user_src"],@"user_gid":dict[@"extra"][@"user_gid"],@"src_uid":dict[@"extra"][@"src_uid"]};
    ((RCTextMessage *)msgModel.content).extra = (NSString *)extraDict;
    return msgModel;
}

+(RCMessageModel *)createCardMsgModel:(NSDictionary *)dict{
    
    RCMessageModel *msgModel = [[RCMessageModel alloc]initWithDict:dict];
    msgModel.objectName = @"[图文]";
    msgModel.content = [[RCRichContentMessage alloc]init];
    ((RCRichContentMessage *)msgModel.content).title = @"6";
    ((RCRichContentMessage *)msgModel.content).digest = @"";
    ((RCRichContentMessage *)msgModel.content).imageURL = @"";
    ((RCRichContentMessage *)msgModel.content).url = @"";
    NSDictionary *msgDetailDict = ((NSArray *)dict[@"extra"][@"msg_detail"][@"Items"]).firstObject;
    NSDictionary *extraDict = @{@"genius_info":@{@"genius_id":dict[@"extra"][@"genius_info"][@"genius_id"],@"realname":dict[@"extra"][@"genius_info"][@"realname"],@"username":dict[@"extra"][@"genius_info"][@"username"]}, @"msg_data":@{@"Items":@[@{@"ImageUrl":msgDetailDict[@"ImageUrl"],@"Effect":msgDetailDict[@"Effect"],@"ProviderId":msgDetailDict[@"ProviderId"],@"UrlTip":msgDetailDict[@"UrlTip"],@"Url":msgDetailDict[@"Url"],@"Name":msgDetailDict[@"Name"],@"Desc":msgDetailDict[@"Desc"]}]},@"routing":@(0),@"inner_uid":dict[@"extra"][@"inner_uid"],@"msg_type":dict[@"extra"][@"msg_type"],@"msg_ts":dict[@"extra"][@"msg_ts"],@"user_gid":dict[@"extra"][@"user_gid"],@"src_uid":dict[@"extra"][@"src_uid"]};
    ((RCTextMessage *)msgModel.content).extra = (NSString *)extraDict;
    return msgModel;
}

+(RCMessageModel *)createSingleCardMsgModel:(NSDictionary *)dict{
    
    RCMessageModel *msgModel = [[RCMessageModel alloc]initWithDict:dict];
    msgModel.objectName = @"[图文]";
    msgModel.content = [[RCRichContentMessage alloc]init];
    ((RCRichContentMessage *)msgModel.content).title = @"7";
    ((RCRichContentMessage *)msgModel.content).digest = @"";
    ((RCRichContentMessage *)msgModel.content).imageURL = @"";
    ((RCRichContentMessage *)msgModel.content).url = @"";
    NSDictionary *extraDict = @{@"genius_info":@{@"genius_id":dict[@"extra"][@"genius_info"][@"genius_id"],@"realname":dict[@"extra"][@"genius_info"][@"realname"],@"username":dict[@"extra"][@"genius_info"][@"username"]}, @"msg_data":@{@"Items":dict[@"extra"][@"msg_detail"][@"Items"]},@"routing":@(0),@"inner_uid":dict[@"extra"][@"inner_uid"],@"msg_type":dict[@"extra"][@"msg_type"],@"msg_ts":dict[@"extra"][@"msg_ts"],@"user_gid":dict[@"extra"][@"user_gid"],@"src_uid":dict[@"extra"][@"src_uid"]};
    ((RCTextMessage *)msgModel.content).extra = (NSString *)extraDict;
    return msgModel;
}

+(RCMessageModel *)createDateModel:(NSDictionary *)dict{
    RCMessageModel *msgModel = [[RCMessageModel alloc]initWithDict:dict];
    msgModel.objectName = @"[文字]";
    msgModel.content = [[RCTextMessage alloc]init];
    ((RCTextMessage *)msgModel.content).content = dict[@"extra"][@"msg_detail"][@"content"];
    
    NSDictionary *extraDict = @{@"genius_info":@{@"genius_id":dict[@"extra"][@"genius_info"][@"genius_id"],@"realname":dict[@"extra"][@"genius_info"][@"realname"],@"username":dict[@"extra"][@"genius_info"][@"username"]}, @"msg_detail":@{@"content":@"",@"sug_list":@[], @"sug_domain":@[],@"action":@(1),@"effect":@""},@"routing":@(0),@"inner_uid":dict[@"extra"][@"inner_uid"],@"msg_type":dict[@"extra"][@"msg_type"],@"msg_ts":dict[@"extra"][@"msg_ts"],@"user_src":dict[@"extra"][@"user_src"],@"user_gid":dict[@"extra"][@"user_gid"],@"src_uid":dict[@"extra"][@"src_uid"]};
    ((RCTextMessage *)msgModel.content).extra = (NSString *)extraDict;
    return msgModel;
}


//+(NSString *)contentWithUserData:(WYConnectInformationModel *)userData{
//
//    return @"";
//}
#warning to do
// 放单例.
+(NSDictionary *)msgWithText:(NSString *)text andUserData:(WYConnectInformationModel *)userData andTime:(long)time{
    
    NSDictionary *dict = @{@"msg_detail":@{@"content":text},@"inner_uid":@(userData.inner_uid),@"msg_type":@1,@"src_uid":userData.src_uid,@"user_gid":userData.user_gid,@"user_src":@(userData.user_src),@"msg_ts":@((long)time),@"direction":@(0/*userData.direction*/),@"genius_info":@{@"username":userData.username,@"realname":userData.realname}};
    return dict;
}

+(NSDictionary *)msgWithUserData:(WYConnectInformationModel *)userData andTime:(long)time{
    
    NSDictionary *dict = @{@"inner_uid":@(userData.inner_uid),@"msg_type":@2,@"src_uid":userData.src_uid,@"user_gid":userData.user_gid,@"user_src":@(userData.user_src),@"msg_ts":@((long)time),@"genius_info":@{@"username":userData.username,@"realname":userData.realname},@"direction":@(userData.direction)};
    return dict;
}

+(NSDictionary *)msgWithImageURLString:(NSString *)imageURLString andDic:(NSDictionary *)dict{
    
    NSMutableDictionary *mutaleDict = [NSMutableDictionary dictionaryWithDictionary:dict];
    NSDictionary *urlDict = @{@"content":imageURLString};
    [mutaleDict setObject:urlDict forKey:@"msg_detail"];
    return [mutaleDict copy];
}

+ (CGSize)labelHeightWithText:(NSString *)text andWidth:(CGFloat)width andFont:(UIFont *)font{
    
    CGSize textSize = CGSizeZero;
    textSize = RC_MULTILINE_TEXTSIZE_GEIOS7(text, font, CGSizeMake(width, 8000));
    return textSize;
}

+ (CGSize)labelHeightWithText:(NSString *)text andWidth:(CGFloat)width{
    
    CGSize textSize = CGSizeZero;
    textSize = RC_MULTILINE_TEXTSIZE_GEIOS7(text, [UIFont systemFontOfSize:14], CGSizeMake(width, 8000));
    return textSize;
}

#warning to do
// 扔RCKitUtility去, 没必要为这个新建类
+(NSDictionary *)jsonStringToDictionary:(NSString *)jsonString{
    
    if (jsonString == nil) {
        return nil;
    }
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSError *err;
    NSDictionary *jsonDic = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&err];
    if(err) {
        NSLog(@"json解析失败：%@",err);
        return nil;
    }
    return jsonDic;
}

+ (NSString*)dictionaryToJson:(NSDictionary *)dic {
    
    NSError *parseError = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dic options:NSJSONWritingPrettyPrinted error:&parseError];
    return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
}

//+ (NSString*) sha1:(NSString*)input{
//    
//    const char *cstr = [input cStringUsingEncoding:NSUTF8StringEncoding];
//    NSData *data = [NSData dataWithBytes:cstr length:input.length];
//    uint8_t digest[CC_SHA1_DIGEST_LENGTH];
//    CC_SHA1(data.bytes, data.length, digest);
//    NSMutableString* output = [NSMutableString stringWithCapacity:CC_SHA1_DIGEST_LENGTH * 2];
//    for(int i = 0; i < CC_SHA1_DIGEST_LENGTH; i++) {
//        [output appendFormat:@"%02x", digest[i]];
//    }
//    return [self toLower:output];
//}

//+ (NSString *)toLower:(NSString *)str{
//    
//    for (NSInteger i=0; i<str.length; i++) {
//        if ([str characterAtIndex:i]>='A'&[str characterAtIndex:i]<='Z') {
//            char  temp=[str characterAtIndex:i]+32;
//            NSRange range=NSMakeRange(i, 1);
//            str=[str stringByReplacingCharactersInRange:range withString:[NSString stringWithFormat:@"%c",temp]];
//        }
//    }
//    return str;
//}

// 不清楚为什么封装
// ①需要改成单例, 需要成员
#warning to do
+(NSString *)computeTime:(NSString *)timeStr{ // 调用三个地方
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    dateFormatter.dateFormat = @"HH:mm:ss";//大写HH是24小时制，小写hh是12小时制
    return [dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:timeStr.integerValue/1000]];
}

+ (NSString *)today{ // 没有调用过
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";//大写HH是24小时制，小写hh是12小时制
    return [dateFormatter stringFromDate:[NSDate date]];
}

+ (NSDate *)dateFromeDateStr:(NSString *)string{ // 只调用一次
    
    NSDateFormatter *inputFormatter = [[NSDateFormatter alloc] init];
    [inputFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"zh_Hans_CN"]];
    [inputFormatter setDateFormat:@"yyyyMMdd"];
    NSDate *date = [inputFormatter dateFromString:string];
    return date;
}

+ (NSString *)currentDateStrFromeDate:(NSDate *)date{ // 只调用一次.
    
    NSDateFormatter *inputFormatter = [[NSDateFormatter alloc] init];
    [inputFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"zh_Hans_CN"]];
    [inputFormatter setDateFormat:@"yyyyMMdd"];
    return [inputFormatter stringFromDate:date];
}

@end
