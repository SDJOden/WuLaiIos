//
//  WYNetworkTools.m
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/5/3.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import "WYNetworkTools.h"

#define BASE_URL (CODESTATU ? @"http://m.laiye.com/" : @"http://test.m.laiye.com/")

#define BASE_URL_M (CODESTATU ? @"http://101.200.177.89:8851/" : @"http://101.200.177.89:8853/")

#define BASE_SIGN_URL (CODESTATU ? @"http://m.laiye.com/" : @"http://101.200.163.166:9007/")

#define RONGCLOUD_IM_APPKEY (CODESTATU?@"http://genius.laiye.com":@"http://101.200.177.95:8229")

@interface WYNetworkTools ()

@property (nonatomic, strong) NSArray *statuArr;

@end

@implementation WYNetworkTools

+(instancetype)shareTools{
    
    static WYNetworkTools *tools;
    
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        
        tools =[[self alloc]init];
        
        tools.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", nil];
    });
    
    return tools;
}

-(void)chatNewInputTextSugTextWithQuery:(NSString *)query andUser_id:(NSString *)user_id andGenius_id:(NSString *)genius_id source:(NSString *)source finished:(void (^)(id responseObject,NSError *error))finished{
    
    NSDictionary *params = @{@"query" : query, @"user_id" : user_id, @"genius_id" : genius_id, @"source":source};
    
    NSString *urlStr = [NSString stringWithFormat:@"%@api/nlp/v2/search/genius/pr/", BASE_URL];
    
    [self request:GET urlString:urlStr parameters:params finished:^(id responseObject, NSError *error) {
        
        finished(responseObject,error);
    }];
}

-(void)chatSugViewWithQuery:(NSString *)query andUser_id:(NSString *)user_id andGenius_id:(NSString *)genius_id source:(NSString *)source finished:(void (^)(id responseObject,NSError *error))finished{
    
    NSDictionary *params = @{@"query" : query, @"user_id" : user_id, @"genius_id" : genius_id, @"source":source};
    
    NSString *urlStr = [NSString stringWithFormat:@"%@api/nlp/v2/search/pr/", BASE_URL];
    
    [self request:GET urlString:urlStr parameters:params finished:^(id responseObject, NSError *error) {
        
        finished(responseObject,error);
    }];
}

-(void)loveWithUserId:(NSString *)user_id andMsgId:(NSString *)msg_id andGeniusId:(NSString *)genius_id andUserMsg:(NSString *)user_msg andTimeStamp:(NSString *)timestamp finished:(void (^)(id responseObject,NSError *error))finished{
    
    NSDictionary *params = @{@"user_id":user_id, @"msg_id":msg_id, @"genius_id":genius_id, @"user_msg":user_msg, @"timestamp":timestamp};
    
    NSString *urlStr = [NSString stringWithFormat:@"%@api/nlp/index/", BASE_URL];
    
    [self request:GET urlString:urlStr parameters:params finished:^(id responseObject, NSError *error) {
        
        finished(responseObject,error);
    }];
}

-(void)shutUp:(BOOL)isShutUp andInnerUid:(NSString *)inner_uid finished:(void (^)(id responseObject,NSError *error))finished{
    
    NSString *request = [NSString stringWithFormat:@"{\"post\":\"EVENT_NOTICE:SET_DOMAIN:%@\",\"userid\":\"%@\"}", isShutUp?@"20003":@"20004", inner_uid];
    
    NSDictionary *params = @{@"request":request};
    
    NSString *urlStr = [NSString stringWithFormat:@"%@sofa.pbrpc.test.DLServer.Dialogue", BASE_URL_M];
    
    [self request:GET urlString:urlStr parameters:params finished:^(id responseObject, NSError *error) {
        
        finished(responseObject,error);
    }];
}

-(void)todayLoveListWithCategory:(NSString *)category finished:(void (^)(id responseObject,NSError *error))finished{
    
    NSString *urlStr = [NSString stringWithFormat:@"%@api/nlp/pr/", BASE_URL];
    
    NSDictionary *params = [NSDictionary dictionary];
    
    if (category.length > 0 && ![category isEqualToString:@"不限"]) {
        
        params = @{@"category":category};
    }
    
    [self request:GET urlString:urlStr parameters:params finished:^(id responseObject, NSError *error) {
        
        finished(responseObject, error);
    }];
}

-(void)todayLoveSessionWithCategory:(NSString *)category finished:(void (^)(id responseObject,NSError *error))finished{
    
    NSString *urlStr = [NSString stringWithFormat:@"%@api/nlp/session/", BASE_URL];
    
    NSDictionary *params = [NSDictionary dictionary];
    
    if (category.length > 0 && ![category isEqualToString:@"不限"]) {
        
        params = @{@"category":category};
    }
    
    [self request:GET urlString:urlStr parameters:params finished:^(id responseObject, NSError *error) {
        
        finished(responseObject, error);
    }];
}

-(void)loveRankWithRequestType:(NSString *)requestType finished:(void (^)(id responseObject,NSError *error))finished{
    
    NSString *urlStr = [NSString stringWithFormat:@"%@api/nlp/v2/rank/genius/", BASE_URL];
    
    NSDictionary *params = @{@"request_type":requestType};
    
    [self request:GET urlString:urlStr parameters:params finished:^(id responseObject, NSError *error) {
        
        finished(responseObject, error);
    }];
}

-(void)lastDayLoveFinished:(void (^)(id responseObject,NSError *error))finished{
    
    NSString *urlStr = [NSString stringWithFormat:@"%@api/nlp/lstday/like/", BASE_URL];
    
    [self request:GET urlString:urlStr parameters:nil finished:^(id responseObject, NSError *error) {
        
        finished(responseObject, error);
    }];
}

-(void)lastDayLoveSessionFinished:(void (^)(id responseObject,NSError *error))finished{
    
    NSString *urlStr = [NSString stringWithFormat:@"%@api/nlp/lstday/session/like", BASE_URL];
    
    [self request:GET urlString:urlStr parameters:nil finished:^(id responseObject, NSError *error) {
        
        finished(responseObject, error);
    }];
}

-(void)loveThisMsgQuery:(NSString *)query andReply:(NSString *)reply andGeniusId:(NSString *)genius_id andTimeStamp:(NSString *)timestamp finished:(void (^)(id responseObject,NSError *error))finished{
    
    NSDictionary *params = @{@"query":query, @"reply":reply, @"genius_id":genius_id, @"timestamp":timestamp};
    
    NSString *urlStr = [NSString stringWithFormat:@"%@api/nlp/update/like/", BASE_URL];
    
    [self request:GET urlString:urlStr parameters:params finished:^(id responseObject, NSError *error) {
        
        finished(responseObject, error);
    }];
}

-(void)hateThisMsgQuery:(NSString *)query andReply:(NSString *)reply andGeniusId:(NSString *)genius_id andTimeStamp:(NSString *)timestamp finished:(void (^)(id responseObject,NSError *error))finished{
    
    NSDictionary *params = @{@"query":query, @"reply":reply, @"genius_id":genius_id, @"timestamp":timestamp, @"like":@"-1"};
    
    NSString *urlStr = [NSString stringWithFormat:@"%@api/nlp/update/like/", BASE_URL];
    
    [self request:GET urlString:urlStr parameters:params finished:^(id responseObject, NSError *error) {
        
        finished(responseObject, error);
    }];
}

-(void)updateThisMsgInfo:(NSString *)query andReply:(NSString *)reply andGeniusId:(NSString *)genius_id andTimeStamp:(NSString *)timestamp andCategory:(NSString *)category finished:(void (^)(id responseObject,NSError *error))finished{
    
    NSDictionary *params = @{@"query":query, @"reply":reply, @"genius_id":genius_id, @"timestamp":timestamp, @"category":category};
    
    NSString *urlStr = [NSString stringWithFormat:@"%@api/nlp/update/like/", BASE_URL];
    
    [self request:GET urlString:urlStr parameters:params finished:^(id responseObject, NSError *error) {
        
        finished(responseObject, error);
    }];
}

-(void)loveSessionWithSession:(NSString *)session andGeniusId:(NSString *)genius_id andTimeStamp:(NSString *)timestamp andCategory:(NSString *)category andReason:(NSString *)reason finished:(void (^)(id responseObject,NSError *error))finished{
    
    NSDictionary *params = [NSDictionary dictionary];
    
    if (category.length == 0) {
        
        params = @{@"session":session, @"genius_id":genius_id, @"timestamp":timestamp, @"reason":reason};
    } else {
        
        params = @{@"session":session, @"genius_id":genius_id, @"timestamp":timestamp, @"category":category, @"reason":reason};
    }
    
    NSString *urlStr = [NSString stringWithFormat:@"%@api/nlp/v2/update/session/", BASE_URL];
    
    [self request:POST urlString:urlStr parameters:params finished:^(id responseObject, NSError *error) {
        
        finished(responseObject, error);
    }];
}

-(void)loveSessionWithDocId:(NSString *)doc_id andCategory:(NSString *)category andTimeStamp:(NSString *)timestamp andGeniusId:(NSString *)genius_id finished:(void (^)(id responseObject,NSError *error))finished{
    
//    NSLog(@"id:%@\ntime:%@\ncategory:%@\ngeniusId:%@", doc_id, timestamp, category, genius_id);
    
    NSDictionary *params = [NSDictionary dictionary];
    
    if (category.length == 0) {
        
        params = @{@"doc_id":doc_id, @"timestamp":timestamp, @"genius_id":genius_id};
    } else {
        
        params = @{@"doc_id":doc_id, @"category":category, @"timestamp":timestamp, @"genius_id":genius_id};
    }
    
    NSString *urlStr = [NSString stringWithFormat:@"%@api/nlp/v2/update/session/", BASE_URL];
    
    [self request:POST urlString:urlStr parameters:params finished:^(id responseObject, NSError *error) {
        
        finished(responseObject, error);
    }];
}

-(void)hateSessionWithDocId:(NSString *)doc_id andTimeStamp:(NSString *)timestamp andGeniusId:(NSString *)genius_id finished:(void (^)(id responseObject,NSError *error))finished{
    
    NSDictionary *params = @{@"doc_id":doc_id, @"timestamp":timestamp, @"genius_id":genius_id, @"like":@"-1"};
    
    NSString *urlStr = [NSString stringWithFormat:@"%@api/nlp/v2/update/session/", BASE_URL];
    
    [self request:POST urlString:urlStr parameters:params finished:^(id responseObject, NSError *error) {
        
        finished(responseObject, error);
    }];
}

-(void)request:(TKRequestMethod)method urlString:(NSString *)urlString parameters:(id)parameters finished:(void (^)(id responseObject,NSError *error))finished{
    
    NSURLSessionDataTask * task = [[NSURLSessionDataTask alloc]init];
    
    if (method == GET) {
        
        task = [self GET:urlString parameters:parameters progress:^(NSProgress * _Nonnull downloadProgress) {} success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            
            finished(responseObject,nil);
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            
            finished(nil,error);
        }];
    } else {
        
        task = [self POST:urlString parameters:parameters progress:^(NSProgress * _Nonnull downloadProgress) {} success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
            
            finished(responseObject,nil);
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            
            finished(nil,error);
        }];
    }
}

/**
 *  新接口
 */
-(void)loveSessionWithCategory:(NSString *)category andStatu:(int)statu andPn:(int)pn finished:(void (^)(id responseObject,NSError *error))finished{
    
    NSString *urlStr = [NSString stringWithFormat:@"%@api/nlp/v2/session/", BASE_URL];
    
    NSDictionary *params = [NSDictionary dictionary];
    
    if (category.length > 0 && ![category isEqualToString:@"-1"]) {
        
        params = @{@"request_type":self.statuArr[statu], @"pn":@(pn).stringValue, @"category":category, @"genius_id":@([SDJUserAccount loadAccount].genius_id).stringValue};
    } else {
        
        params = @{@"request_type":self.statuArr[statu], @"pn":@(pn).stringValue, @"genius_id":@([SDJUserAccount loadAccount].genius_id).stringValue};
    }
    
    [self request:GET urlString:urlStr parameters:params finished:^(id responseObject, NSError *error) {
        
        finished(responseObject, error);
    }];
}

-(void)searchLoveSessionWithPn:(NSString *)pn withQuery:(NSString *)query withQueryType:(NSString *)queryType finished:(void (^)(id responseObject,NSError *error))finished{
    
    NSDictionary *params = @{@"request_type":@"search", @"genius_id":@([SDJUserAccount loadAccount].genius_id).stringValue, @"pn":pn, @"query":query, @"search_field":queryType};
    
    NSString *urlStr = [NSString stringWithFormat:@"%@api/nlp/v2/session/", BASE_URL];
    
    [self request:GET urlString:urlStr parameters:params finished:^(id responseObject, NSError *error) {
        
        finished(responseObject, error);
    }];
}

-(void)signNumWithGeniusIdFinished:(void (^)(id responseObject,NSError *error))finished{
    
    NSString *urlStr = [NSString stringWithFormat:@"%@api/nlp/v2/label/interface/", BASE_SIGN_URL];
    
    NSDictionary *params = @{@"request_type":@"rank", @"genius_id":@([SDJUserAccount loadAccount].genius_id).stringValue};
    
    [self request:GET urlString:urlStr parameters:params finished:^(id responseObject, NSError *error) {
        
        finished(responseObject, error);
    }];
}

//标注内容
-(void)signDataWithGeniusIdAndTime:(NSString *)time finished:(void (^)(id responseObject,NSError *error))finished{
    
    NSString *urlStr = [NSString stringWithFormat:@"%@api/nlp/v2/label/interface/", BASE_SIGN_URL];
    
    NSDictionary *params = [NSDictionary dictionary];
    
    if (time.length > 0) {
        
        params = @{@"request_type":@"display", @"genius_id":@([SDJUserAccount loadAccount].genius_id).stringValue, @"time":time};
    } else {
        
        params = @{@"request_type":@"display", @"genius_id":@([SDJUserAccount loadAccount].genius_id).stringValue};
    }
    
    [self request:GET urlString:urlStr parameters:params finished:^(id responseObject, NSError *error) {
        
        finished(responseObject, error);
    }];
}

-(void)signMessageWithDocId:(NSString *)doc_id andCategory:(NSString *)category finished:(void (^)(id responseObject,NSError *error))finished{
    
    NSString *urlStr = [NSString stringWithFormat:@"%@api/nlp/v2/label/interface/", BASE_SIGN_URL];
    
    NSDictionary *params = @{@"request_type":@"update", @"doc_id":doc_id, @"genius_id":@([SDJUserAccount loadAccount].genius_id).stringValue, @"label_val":category};
    
    [self request:GET urlString:urlStr parameters:params finished:^(id responseObject, NSError *error) {
        
        finished(responseObject, error);
    }];
}

-(void)loveSearchImportWordsfinished:(void (^)(id responseObject,NSError *error))finished{
    
    NSString *urlStr = [NSString stringWithFormat:@"%@api/nlp/v2/hot/keys/", BASE_SIGN_URL];
    
    [self request:GET urlString:urlStr parameters:nil finished:^(id responseObject, NSError *error) {
        
        finished(responseObject, error);
    }];
}

-(void)requestServiceCategoryFinished:(void (^)(id responseObject,NSError *error))finished{
    NSDictionary *params = @{@"gt":[[SDJUserAccount loadAccount]token]};
    NSString *urlStr = [NSString stringWithFormat:@"%@/dkf/api/genius/service-types", RONGCLOUD_IM_APPKEY];
    [self request:GET urlString:urlStr parameters:params finished:^(id responseObject, NSError *error) {
        finished(responseObject, error);
    }];
}

- (NSArray *)statuArr {
    
	if(_statuArr == nil) {
        
        _statuArr = @[@"new", @"lst", @"history", @"mine", @"my_history"];
	}
    
	return _statuArr;
}

@end
