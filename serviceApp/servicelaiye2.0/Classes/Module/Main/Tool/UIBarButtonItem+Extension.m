//
//  UIBarButtonItem+Extension.m
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/4/9.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import "UIBarButtonItem+Extension.h"

@implementation UIBarButtonItem (Extension)

+(instancetype)itemWithTitle:(NSString*)title target:(id)target action:(SEL)action left:(BOOL)isLeft{
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [btn setTitle:title forState:UIControlStateNormal];
    
    if (isLeft) {
        [btn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
        btn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        btn.titleEdgeInsets = UIEdgeInsetsMake(0, 8, 0, 0);
        [btn sizeToFit];
        btn.bounds = CGRectMake(0, 0, btn.w + 8, btn.h);
    }else {
        [btn sizeToFit];
    }
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor colorWithRed:0.6579 green:0.6579 blue:0.6579 alpha:1.0] forState:UIControlStateHighlighted];
    btn.titleLabel.font = [UIFont systemFontOfSize:14];
    [btn addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    return [[UIBarButtonItem alloc]initWithCustomView:btn];
}

+(instancetype)itemWithImageNamed:(NSString *)imageName target:(id)target action:(SEL)action{
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setBackgroundImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
    [btn sizeToFit];
    [btn addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    return [[UIBarButtonItem alloc]initWithCustomView:btn];
}

@end

