//
//  WYServiceStatuCollectionViewCell.m
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/4/5.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import "WYServiceStatuCollectionViewCell.h"
#import "WYServiceStatuModel.h"
#import "WYLoveTypeModel.h"

@interface WYServiceStatuCollectionViewCell ()

@property (nonatomic, strong) UILabel *label;

@end

@implementation WYServiceStatuCollectionViewCell

- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize {
    // 清空操作
    NSArray *subviews = [[NSArray alloc] initWithArray:self.contentView.subviews];
    for (__strong UIView *subview in subviews) {
        [subview removeFromSuperview];
        subview = nil;
    }
}

-(void)setDataModel:(WYServiceStatuModel *)model{
    
    self.label.text = model.serviceName;
    if (model.serviceName.length>2) {
        self.label.font = [UIFont systemFontOfSize:14];
    } else {
        self.label.font = [UIFont systemFontOfSize:15];
    }
    
    if (model.isSelected == YES) {
        self.label.backgroundColor = HEXCOLOR(0x17D17A);
        self.label.layer.borderColor = [[UIColor clearColor]CGColor];
        self.label.textColor = [UIColor whiteColor];
    }
    else {
        self.label.backgroundColor = [UIColor clearColor];
        self.label.layer.borderColor = [[UIColor lightGrayColor]CGColor];
        self.label.textColor = [UIColor blackColor];
    }
    [self layout];
}

-(void)createWithModel:(WYLoveTypeModel *)model{
    
    self.label.text = model.type;
    if (model.type.length>4) {
        self.label.font = [UIFont systemFontOfSize:12];
    } else if (model.type.length>2) {
        self.label.font = [UIFont systemFontOfSize:14];
    } else {
        self.label.font = [UIFont systemFontOfSize:15];
    }
    if (model.selected == YES) {
        self.label.backgroundColor = HEXCOLOR(0x17D17A);
        self.label.layer.borderColor = [[UIColor clearColor]CGColor];
        self.label.textColor = [UIColor whiteColor];
    }
    else {
        self.label.backgroundColor = [UIColor clearColor];
        self.label.layer.borderColor = [[UIColor lightGrayColor]CGColor];
        self.label.textColor = [UIColor blackColor];
    }
    
//    if (model.type.length >) {
//        <#statements#>
//    }
    
    [self layout];
}

-(void)layout{
    
    [self.label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.bottom.equalTo(self);
    }];
}

- (UILabel *)label {
    
	if(_label == nil) {
		_label = [[UILabel alloc] init];
        _label.textColor = [UIColor blackColor];
        _label.textAlignment = NSTextAlignmentCenter;
        _label.font = [UIFont systemFontOfSize:15];
        _label.layer.borderWidth = 0.5;
        _label.layer.borderColor = [[UIColor lightGrayColor]CGColor];
        _label.layer.cornerRadius = 15.5;
        _label.layer.masksToBounds = YES;
        [self.contentView addSubview:_label];
	}
	return _label;
}

-(NSString *)getLabelText{
    
    return self.label.text;
}

@end
