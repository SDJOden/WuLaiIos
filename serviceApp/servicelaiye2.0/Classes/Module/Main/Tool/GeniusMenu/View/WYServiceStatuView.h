//
//  WYServiceStatuView.h
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/4/5.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import <UIKit/UIKit.h>

//助理的服务品类的view
@interface WYServiceStatuView : UIView

-(NSArray *)getServiceViewArr;

-(void)resetCell:(NSArray *)serviceArr;

-(void)resetData;

-(void)changeTextWithArr:(NSArray *)modelArr;

-(void)modelWithArray:(NSArray *)array;

+(instancetype)modelWithServiceArr:(NSArray *)serviceArr;

-(instancetype)initWithServiceArr:(NSArray *)serviceArr;

@end
