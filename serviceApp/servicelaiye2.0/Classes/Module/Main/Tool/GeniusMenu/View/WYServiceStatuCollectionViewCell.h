//
//  WYServiceStatuCollectionViewCell.h
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/4/5.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import <UIKit/UIKit.h>

@class WYServiceStatuModel;

@class WYLoveTypeModel;


@interface WYServiceStatuCollectionViewCell : UICollectionViewCell

- (void)setDataModel:(WYServiceStatuModel *)model;

-(void)createWithModel:(WYLoveTypeModel *)model;

-(NSString *)getLabelText;

@end
