//
//  WYLogStatuView.m
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/3/2.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import "WYLogStatuView.h"

typedef enum : NSInteger {
    WYLoginStatuClose = -1,
    WYLoginStatuCanConnect = 0,
    WYLoginStatuCanUse = 5
} WYLoginStatu;

@interface WYLogStatuView ()

@property (nonatomic, strong) UILabel *titleLabel;

@property (nonatomic, strong) UIButton *closeBtn;

@property (nonatomic, strong) UIButton *connectBtn;

@property (nonatomic, strong) UIButton *allowBtn;

@end


@implementation WYLogStatuView

-(void)rcinitWithStatus:(NSInteger)statu{
    switch (statu) {
        case WYLoginStatuClose:
            [self changeStatus:self.closeBtn];
            break;
        case WYLoginStatuCanConnect:
            [self changeStatus:self.connectBtn];
            break;
        case WYLoginStatuCanUse:
            [self changeStatus:self.allowBtn];
            break;
        default:
            break;
    }
}

-(void)layoutSubviews{
    
    [super layoutSubviews];
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self);
        make.height.equalTo(30);
        make.left.equalTo(20);
        make.width.equalTo(80);
    }];
    
    [self.closeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.bottom);
        make.height.equalTo(44);
        make.left.equalTo(20);
        make.width.equalTo(self.connectBtn.width);
    }];
    
    [self.connectBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.bottom);
        make.height.equalTo(44);
        make.left.equalTo(self.closeBtn.right).offset(30);
        make.width.equalTo(self.allowBtn.width);
    }];
    
    [self.allowBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.bottom);
        make.height.equalTo(44);
        make.left.equalTo(self.connectBtn.right).offset(30);
        make.right.equalTo(self).offset(-20);
    }];
}

- (UILabel *)titleLabel {
    
    if(_titleLabel == nil) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.textAlignment = NSTextAlignmentLeft;
        _titleLabel.text = @"接单状态";
        _titleLabel.textColor = [UIColor grayColor];
        _titleLabel.font = [UIFont systemFontOfSize:12];
        [self addSubview:_titleLabel];
    }
    return _titleLabel;
}

- (UIButton *)closeBtn {
    
    if(_closeBtn == nil) {
        _closeBtn = [[UIButton alloc] init];
        [_closeBtn setTitle:@"关闭" forState:UIControlStateNormal];
        [_closeBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_closeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        _closeBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        _closeBtn.layer.cornerRadius = 20.0;
        _closeBtn.layer.masksToBounds = YES;
        _closeBtn.layer.borderWidth = 0.5;
        _closeBtn.layer.borderColor = [[UIColor lightGrayColor] CGColor];
        _closeBtn.tag = WYLoginStatuClose;
        [_closeBtn addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_closeBtn];
    }
    return _closeBtn;
}

- (UIButton *)connectBtn {
    
    if(_connectBtn == nil) {
        _connectBtn = [[UIButton alloc] init];
        [_connectBtn setTitle:@"接单" forState:UIControlStateNormal];
        [_connectBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_connectBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        _connectBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        _connectBtn.layer.cornerRadius = 20.0;
        _connectBtn.layer.masksToBounds = YES;
        _connectBtn.tag = WYLoginStatuCanConnect;
        _connectBtn.layer.borderWidth = 0.5;
        _connectBtn.layer.borderColor = [[UIColor lightGrayColor] CGColor];
        [_connectBtn addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_connectBtn];
    }
    return _connectBtn;
}

- (UIButton *)allowBtn {
    
    if(_allowBtn == nil) {
        _allowBtn = [[UIButton alloc] init];
        [_allowBtn setTitle:@"派单" forState:UIControlStateNormal];
        [_allowBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_allowBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        _allowBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        _allowBtn.layer.cornerRadius = 20.0;
        _allowBtn.layer.masksToBounds = YES;
        _allowBtn.tag = WYLoginStatuCanUse;
        _allowBtn.layer.borderWidth = 0.5;
        _allowBtn.layer.borderColor = [[UIColor lightGrayColor] CGColor];
        [_allowBtn addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_allowBtn];
    }
    return _allowBtn;
}

-(void)buttonClick:(UIButton *)sender{
    
    if (sender.isSelected == NO) {
        if ([self.logStatuViewDelegate respondsToSelector:@selector(buttonClickWithButtonTag:)]) {
            [self.logStatuViewDelegate buttonClickWithButtonTag:sender.tag];
        }
        [self changeStatus:sender];
    }
}

-(void)changeStatus:(UIButton *)sender{
    
    _closeBtn.selected = NO;
    _connectBtn.selected = NO;
    _allowBtn.selected = NO;
    _closeBtn.backgroundColor = [UIColor clearColor];
    _connectBtn.backgroundColor = [UIColor clearColor];
    _allowBtn.backgroundColor = [UIColor clearColor];
    _closeBtn.layer.borderWidth = 0.5;
    _connectBtn.layer.borderWidth = 0.5;
    _allowBtn.layer.borderWidth = 0.5;
    sender.selected = YES;
    sender.layer.borderWidth = 0;
    sender.backgroundColor = HEXCOLOR(0x17D17A);
}

@end
