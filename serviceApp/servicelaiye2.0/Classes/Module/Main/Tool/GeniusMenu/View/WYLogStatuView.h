//
//  WYLogStatuView.h
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/3/2.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol WYLogStatuViewDelegate <NSObject>

@optional

-(void)buttonClickWithButtonTag:(NSInteger)tag;

@end

@interface WYLogStatuView : UIView

@property (nonatomic, weak) id <WYLogStatuViewDelegate> logStatuViewDelegate;

-(void)rcinitWithStatus:(NSInteger)statu;

@end
