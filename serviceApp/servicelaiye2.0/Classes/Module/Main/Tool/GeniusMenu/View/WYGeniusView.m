//
//  WYGeniusView.m
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/3/4.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import "WYGeniusView.h"
#import "WYNavigationViewController.h"
#import "WYConnectViewController.h"
#import "WYServiceStatuModel.h"
#import "WYLogStatuView.h"
#import "WYServiceStatuView.h"
#import "BBFlashCtntLabel.h"
#import "WYNetworkTools.h"

#define kScreenHeight  [UIScreen mainScreen].bounds.size.height
#define kScreenWidth  [UIScreen mainScreen].bounds.size.width

typedef enum : NSInteger {
    WYLoginStatuClose = -1,
    WYLoginStatuCanConnect = 0,
    WYLoginStatuCanUse = 5
} WYLoginStatu;


@interface WYGeniusView () <WYLogStatuViewDelegate>

@property (nonatomic, strong) UILabel *pageViewLabel;

@property (nonatomic, strong) UIButton *exitBtn;

@property (nonatomic, strong) UIButton *resetBtn;

@property (nonatomic, strong) WYLogStatuView *logStatuView;

@property (nonatomic, strong) WYServiceStatuView *serviceStatuView;

@property (nonatomic, strong) NSArray *serviceArr;

@property (nonatomic, strong) UILabel *titleLabel;

@end

@implementation WYGeniusView

- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        [self requestData];
        [self initialize];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self requestData];
        [self initialize];
    }
    return self;
}

-(void)requestData{
    NSMutableArray *statusArr = [NSMutableArray array];
    [[WYNetworkTools shareTools]requestServiceCategoryFinished:^(id responseObject, NSError *error) {
        if (![responseObject[@"error"] isEqual:[NSNull null]]) {//报错
            NSNumber *code = responseObject[@"error"][@"error_code"];
            if (code.integerValue == 20000) {
                NSString *UserPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).firstObject stringByAppendingPathComponent:@"account.plist"];
                [[NSFileManager defaultManager]removeItemAtPath:UserPath error:nil];
                //其他平台登录，token改变需要先登出融云
                [[RCIMClient sharedRCIMClient] logout];
                // 发送通知让appdelegate去切换根视图为聊天列表界面
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"WYRootViewControllerSwitchNotification" object:@"0"];
                });
            }
        }
        else {
            for (NSString *keyStr in [SDJUserAccount loadAccount].serviceIdsDic.allKeys) {
                WYServiceStatuModel *statuModel = [[WYServiceStatuModel alloc]init];
                statuModel.serviceStatu = keyStr.integerValue;
                statuModel.serviceName = [SDJUserAccount loadAccount].serviceIdsDic[keyStr];
                statuModel.isSelected = NO;
                [statusArr addObject:statuModel];
                for (NSString *serviceId in responseObject[@"data"][@"service_list"]) {
                    if (keyStr.integerValue == serviceId.integerValue) {
                        statuModel.isSelected = YES;
                        break;
                    }
                }
            }
            
            NSArray *returnArr = [[statusArr copy] sortedArrayUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"serviceStatu" ascending:YES]]];
            if (((NSArray *)responseObject[@"data"][@"service_list"]).count == 0) {
                ((WYServiceStatuModel *)returnArr[0]).isSelected = YES;
            }
            self.serviceArr = returnArr;
            [self.serviceStatuView modelWithArray:self.serviceArr];
        }
    }];
}

// 布局
- (void)initialize {
    
    [self.pageViewLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(10);
        make.centerX.equalTo(self);
        make.width.equalTo(50);
        make.height.equalTo(40);
    }];
    
    [self.exitBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(20);
        make.right.equalTo(self).offset(-20);
        make.width.height.equalTo(30);
    }];
    
    [self.resetBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(20);
        make.left.equalTo(self).offset(20);
        make.width.equalTo(70);
        make.height.equalTo(30);
    }];
    
    [self.logStatuView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.resetBtn.bottom).offset(10);
        make.left.right.equalTo(self);
        make.height.equalTo(30+44);
    }];
    
    [self.serviceStatuView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.logStatuView.bottom).offset(10);
        make.left.right.equalTo(self);
        make.bottom.equalTo(self).offset(-10);
    }];
}

-(void)layoutSubviews{
    
    [super layoutSubviews];
    self.backgroundColor = [UIColor colorWithRed:0.949 green:0.949 blue:0.949 alpha:1.0];
}

/**
 *  改变登陆状态
 */
-(void)buttonClickWithButtonTag:(NSInteger)tag{
    
    [self changeStatu:tag];
}

-(void)changeStatu:(NSInteger)statu{
    
    __weak typeof(self)__weakSelf = self;
    [[SDJNetworkTools sharedTools]changeState:[SDJUserAccount loadAccount].token limit:statu finished:^(NSDictionary<NSString *,id> * _Nullable result, NSError * _Nullable error) {
        if (![result[@"error"] isEqual:[NSNull null]]) {//报错
            NSNumber *code = result[@"error"][@"error_code"];
            if (code.integerValue == 20000) {
                NSString *UserPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).firstObject stringByAppendingPathComponent:@"account.plist"];
                [[NSFileManager defaultManager]removeItemAtPath:UserPath error:nil];
                //其他平台登录，token改变需要先登出融云
                [[RCIMClient sharedRCIMClient] logout];
                // 发送通知让appdelegate去切换根视图为聊天列表界面
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"WYRootViewControllerSwitchNotification" object:@"0"];
                });
            }
        }
        else {
            [SDJUserAccount loadAccount].logStatu = statu;
            [[SDJUserAccount loadAccount] savaAccount];
            [__weakSelf.logStatuView rcinitWithStatus:statu];
        }
    }];
}

/**
 *  获得状态的详情
 */
-(NSString *)getServiceValue{
    
    NSString *str = @"";
    for (int i = 0; i < self.serviceArr.count; i ++) {
        WYServiceStatuModel *statuModel = self.serviceArr[i];
        if (statuModel.isSelected) {
            if (i == 0) {
                return @"-1";
            }
            else {
                str = [NSString stringWithFormat:@"%@,%ld", str, statuModel.serviceStatu];
            }
        }
    }
    if (str.length > 0) {
        str = [str substringFromIndex:1];
    }
    else {
        str = @"-1";
    }
    return str;
}

-(void)resetLogStatu:(UIButton *)sender{
    
    [self changeStatu:WYLoginStatuClose];
    [self servicesChange:sender];
}

-(void)servicesChange:(UIButton *)sender{
    
    NSString *service = @"";
    if (sender.tag == 1) {
        service = [self getServiceValue];
    }else {
        service = @"-1";
    }
    __weak typeof(self) __weakSelf = self;
    [[SDJNetworkTools sharedTools]updateServiceTypes:[SDJUserAccount loadAccount].token service_ids:service finished:^(NSDictionary<NSString *,id> * _Nullable result, NSError * _Nullable error) {
        if (![result[@"error"] isEqual:[NSNull null]]) {//报错
            NSNumber *code = result[@"error"][@"error_code"];
            if (code.integerValue == 20000) {
                NSString *UserPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).firstObject stringByAppendingPathComponent:@"account.plist"];
                [[NSFileManager defaultManager]removeItemAtPath:UserPath error:nil];
                //其他平台登录，token改变需要先登出融云
                [[RCIMClient sharedRCIMClient] logout];
                // 发送通知让appdelegate去切换根视图为聊天列表界面
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"WYRootViewControllerSwitchNotification" object:@"0"];
                });
            }
        }
        else {
            if (sender.tag == 1) {
                [__weakSelf.serviceStatuView changeTextWithArr:self.serviceArr];
            }else {
                if (((WYServiceStatuModel *)__weakSelf.serviceArr[0]).serviceStatu == -1 && ((WYServiceStatuModel *)__weakSelf.serviceArr[0]).isSelected == NO) {
                    [__weakSelf.serviceStatuView resetData];
                }
            }
        }
    }];
}

-(void)exitGeniusView:(UIButton *)sender{
    
    [self servicesChange:sender];
    if ([self.geniusViewDelegate respondsToSelector:@selector(exitGeniusView)]) {
        [self.geniusViewDelegate exitGeniusView];
    }
}

/**
 *  一键重置
 */
- (UIButton *)resetBtn {
    
    if(_resetBtn == nil) {
        _resetBtn = [UIButton buttonWithType:UIButtonTypeSystem];
        [_resetBtn setTitle:@"重置状态" forState:UIControlStateNormal];
        _resetBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        _resetBtn.tag = 0;
        [_resetBtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        [_resetBtn addTarget:self action:@selector(resetLogStatu:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_resetBtn];
    }
    return _resetBtn;
}

- (WYLogStatuView *)logStatuView {
    
    if(_logStatuView == nil) {
        _logStatuView = [[WYLogStatuView alloc] init];
        [_logStatuView rcinitWithStatus:[SDJUserAccount loadAccount].logStatu];
        _logStatuView.logStatuViewDelegate = self;
        [self addSubview:_logStatuView];
    }
    return _logStatuView;
}

- (WYServiceStatuView *)serviceStatuView {
    
    if(_serviceStatuView == nil) {
        _serviceStatuView = [[WYServiceStatuView alloc]init];
        [self addSubview:_serviceStatuView];
    }
    return _serviceStatuView;
}

- (UILabel *)pageViewLabel {
    
    if(_pageViewLabel == nil) {
        _pageViewLabel = [[UILabel alloc] init];
        _pageViewLabel.text = @"设置";
        _pageViewLabel.textAlignment = NSTextAlignmentCenter;
        _pageViewLabel.textColor = [UIColor blackColor];
        _pageViewLabel.font = [UIFont systemFontOfSize:14];
        [self addSubview:_pageViewLabel];
    }
    return _pageViewLabel;
}

- (UILabel *)titleLabel {
    if(_titleLabel == nil) {
        _titleLabel = [[UILabel alloc] init];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.text = @"接单品类";
        _titleLabel.font = [UIFont systemFontOfSize:14];
        [self addSubview:_titleLabel];
    }
    return _titleLabel;
}

- (UIButton *)exitBtn {
    if(_exitBtn == nil) {
        _exitBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _exitBtn.tag = 1;
        [_exitBtn setImage:[UIImage imageNamed:@"registerVC_closeBtn"] forState:UIControlStateNormal];
        [_exitBtn addTarget:self action:@selector(exitGeniusView:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_exitBtn];
    }
    return _exitBtn;
}

//- (NSArray *)serviceArr {
//
//    if(_serviceArr == nil) {
//        _serviceArr = [WYServiceStatuModel arrayOfServiceIds];
//    }
//    return _serviceArr;
//}

- (void)dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
