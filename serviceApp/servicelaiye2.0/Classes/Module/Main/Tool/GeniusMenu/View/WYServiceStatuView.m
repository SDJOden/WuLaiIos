//
//  WYServiceStatuView.m
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/4/5.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import "WYServiceStatuView.h"
#import "WYServiceStatuModel.h"
#import "WYServiceStatuCollectionViewCell.h"
#import "BBFlashCtntLabel.h"

@interface WYServiceStatuView () <UICollectionViewDelegate, UICollectionViewDataSource>

@property (nonatomic, strong) UILabel *titleLabel;

@property (nonatomic, strong) UICollectionView *serviceCollectionView;

@property (nonatomic, strong) UICollectionViewFlowLayout *customFlowLayout;

@property (nonatomic, strong) NSArray *serviceViewArr;

//跑马灯的label
@property (nonatomic, strong) BBFlashCtntLabel *serviceLabel;

@end

@implementation WYServiceStatuView

-(void)resetCell:(NSArray *)serviceArr{
    self.serviceViewArr = [serviceArr copy];
    [self.serviceCollectionView reloadData];
    [self.serviceLabel reloadView];
}

+(instancetype)modelWithServiceArr:(NSArray *)serviceArr{
    return [[self alloc]initWithServiceArr:serviceArr];
}

-(void)modelWithArray:(NSArray *)array{
    self.serviceViewArr = [array copy];
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self);
        make.left.equalTo(self).offset(20);
        make.width.equalTo(60);
        make.height.equalTo(20);
    }];
    
    [self.serviceCollectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.bottom).offset(10);
        make.left.equalTo(self).offset(20);
        make.right.equalTo(self).offset(-20);
        make.bottom.equalTo(self);
    }];
    
    [self.serviceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self);
        make.left.equalTo(self.titleLabel.right).offset(10);
        make.right.equalTo(self).offset(-10);
        make.height.equalTo(20);
    }];
    
    [self.serviceLabel reloadView];
    [self.serviceCollectionView reloadData];
}

-(instancetype)initWithServiceArr:(NSArray *)serviceArr{
    
    if (self = [super init]) {
        self.serviceViewArr = [serviceArr copy];
        
        [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self);
            make.left.equalTo(self).offset(20);
            make.width.equalTo(60);
            make.height.equalTo(20);
        }];
        
        [self.serviceCollectionView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.titleLabel.bottom).offset(10);
            make.left.equalTo(self).offset(20);
            make.right.equalTo(self).offset(-20);
            make.bottom.equalTo(self);
        }];
        
        [self.serviceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self);
            make.left.equalTo(self.titleLabel.right).offset(10);
            make.right.equalTo(self).offset(-10);
            make.height.equalTo(20);
        }];
        
        [self.serviceLabel reloadView];
    }
    return self;
}

-(void)changeTextWithArr:(NSArray *)modelArr{
    
    self.serviceViewArr = [modelArr copy];
    self.serviceLabel.text = [self getServiceStatu];
}

-(void)resetData{
    
    [self collectionView:self.serviceCollectionView didSelectItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
}

- (UILabel *)titleLabel {
    
	if(_titleLabel == nil) {
        _titleLabel = [[UILabel alloc]init];
        _titleLabel.textColor = [UIColor grayColor];
        _titleLabel.font = [UIFont systemFontOfSize:12];
        _titleLabel.text = @"接单品类";
        [self addSubview:_titleLabel];
	}
	return _titleLabel;
}

- (NSArray *)serviceViewArr {
    
	if(_serviceViewArr == nil) {
		_serviceViewArr = [[NSArray alloc] init];
	}
	return _serviceViewArr;
}

- (UICollectionView *)serviceCollectionView {
    
	if(_serviceCollectionView == nil) {
        _customFlowLayout = [[UICollectionViewFlowLayout alloc] init];
        _customFlowLayout.minimumLineSpacing = 20.0f;
        _customFlowLayout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
        _customFlowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
		_serviceCollectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:_customFlowLayout];
        _serviceCollectionView.showsVerticalScrollIndicator = NO;
        _serviceCollectionView.delegate = self;
        _serviceCollectionView.dataSource = self;
        _serviceCollectionView.backgroundColor = [UIColor clearColor];
        [_serviceCollectionView registerClass:[WYServiceStatuCollectionViewCell class] forCellWithReuseIdentifier:@"serviceStatuCell"];
        [self addSubview:_serviceCollectionView];
	}
	return _serviceCollectionView;
}

#pragma mark --- UICollectionViewDelgate
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return self.serviceViewArr.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    WYServiceStatuCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"serviceStatuCell" forIndexPath:indexPath];
    [cell setDataModel:self.serviceViewArr[indexPath.row]];
    return cell;
}

#pragma mark --- UICollectionViewDelegateFlowLayout
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    return CGSizeMake(([UIScreen mainScreen].bounds.size.width - 2 * 20 - 2 * 20) / 3, 44);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    //修改当前选中的模型和cell样式
    WYServiceStatuModel *statuModel = self.serviceViewArr[indexPath.row];
    [self addPoint:statuModel.serviceName];
    //判断是不是第一个cell，如果是遍历后面cell和模型进行修改
    if (indexPath.row == 0) {
        for (int i = 1; i < self.serviceViewArr.count; i ++) {
            WYServiceStatuModel *serviceModel = self.serviceViewArr[i];
            if (serviceModel.isSelected) {
                serviceModel.isSelected = NO;
                [((WYServiceStatuCollectionViewCell *)[collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]]) setDataModel:serviceModel];
            }
        }
    }
    else {//如果不是第一个cell，修改第一个cell样式
        if (((WYServiceStatuModel *)self.serviceViewArr[0]).isSelected) {
            ((WYServiceStatuModel *)self.serviceViewArr[0]).isSelected = NO;
            [((WYServiceStatuCollectionViewCell *)[collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]]) setDataModel:self.serviceViewArr[0]];
        }
    }
    statuModel.isSelected = !statuModel.isSelected;
    [((WYServiceStatuCollectionViewCell *)[collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:indexPath.row inSection:0]]) setDataModel:statuModel];
    self.serviceLabel.text = [self getServiceStatu];
}

- (BBFlashCtntLabel *)serviceLabel {
    
    if(_serviceLabel == nil) {
        _serviceLabel = [[BBFlashCtntLabel alloc] init];
        _serviceLabel.text = [self getServiceStatu];
        _serviceLabel.font = [UIFont systemFontOfSize:12];
        _serviceLabel.textColor = [UIColor grayColor];
        _serviceLabel.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width - 10 - 60 - 10, 20);
        [self addSubview:_serviceLabel];
    }
    return _serviceLabel;
}

-(NSString *)getServiceStatu{
    
    NSString *str = @"";
    for (int i = 0; i < self.serviceViewArr.count; i ++) {
        WYServiceStatuModel *statuModel = self.serviceViewArr[i];
        if (statuModel.isSelected) {
            if (i == 0) {
                return @"不限";
            }
            else {
                str = [NSString stringWithFormat:@"%@，%@", str, statuModel.serviceName];
            }
        }
    }
    if (str.length > 0) {
        str = [str substringFromIndex:1];
    }
    else {
        str = @"不限";
    }
    return str;
}

-(NSArray *)getServiceViewArr{
    
    return [self.serviceViewArr copy];
}

-(void)addPoint:(NSString *)serviceName{
    
    if ([serviceName isEqualToString:@"不限"]) {
        [MobClick event:@"my_changeNone_click"];
    } else if ([serviceName isEqualToString:@"用车"]) {
        [MobClick event:@"my_changeTzxiStatu_click"];
    } else if ([serviceName isEqualToString:@"外卖"]) {
        [MobClick event:@"my_changeOrderTakeoutStatu_click"];
    } else if ([serviceName isEqualToString:@"代买"]) {
        [MobClick event:@"my_changeInsteadBuyStatu_click"];
    } else if ([serviceName isEqualToString:@"电影"]) {
        [MobClick event:@"my_changeMovieStatu_click"];
    } else if ([serviceName isEqualToString:@"保洁"]) {
        [MobClick event:@"my_changeCleanStatu_click"];
    } else if ([serviceName isEqualToString:@"代驾"]) {
        [MobClick event:@"my_changeDesignatedDriverStatu_click"];
    } else if ([serviceName isEqualToString:@"美甲"]) {
        [MobClick event:@"my_changeManicureStatu_click"];
    } else if ([serviceName isEqualToString:@"推拿"]) {
        [MobClick event:@"my_changeMassageStatu_click"];
    } else if ([serviceName isEqualToString:@"通用"]) {
        [MobClick event:@"my_changeNormalStatu_click"];
    } else if ([serviceName isEqualToString:@"机票"]) {
        [MobClick event:@"my_changePlaneTicketStatu_click"];
    } else if ([serviceName isEqualToString:@"车票"]) {
        [MobClick event:@"my_changeTrainTicketStatu_click"];
    } else if ([serviceName isEqualToString:@"酒店"]) {
        [MobClick event:@"my_changeHotelStatu_click"];
    } else if ([serviceName isEqualToString:@"快递"]) {
        [MobClick event:@"my_changeExpressStatu_click"];
    } else if ([serviceName isEqualToString:@"汽车"]) {
        [MobClick event:@"my_changeCarStatu_click"];
    } else if ([serviceName isEqualToString:@"订座"]) {
        [MobClick event:@"my_changeSeatStatu_click"];
    } else if ([serviceName isEqualToString:@"鲜花"]) {
        [MobClick event:@"my_changeFlowerStatu_click"];
    } else if ([serviceName isEqualToString:@"维修"]) {
        [MobClick event:@"my_changeFixStatu_click"];
    } else if ([serviceName isEqualToString:@"送药"]) {
        [MobClick event:@"my_changeMedicineStatu_click"];
    } else if ([serviceName isEqualToString:@"跑腿"]) {
        [MobClick event:@"my_changeErrandStatu_click"];
    } else if ([serviceName isEqualToString:@"咖啡"]) {
        [MobClick event:@"my_changeCoffeeStatu_click"];
    } else if ([serviceName isEqualToString:@"洗衣"]) {
        [MobClick event:@"my_changeWashStatu_click"];
    } else if ([serviceName isEqualToString:@"搬家"]) {
        [MobClick event:@"my_changeMoveStatu_click"];
    } else if ([serviceName isEqualToString:@"健康"]) {
        [MobClick event:@"my_changeHealthStatu_click"];
    } else if ([serviceName isEqualToString:@"查询"]) {
        [MobClick event:@"my_changeSearchStatu_click"];
    } else if ([serviceName isEqualToString:@"美容"]) {
        [MobClick event:@"my_changeCosmetologyStatu_click"];
    } else if ([serviceName isEqualToString:@"网购"]) {
        [MobClick event:@"my_changeShoppingOnlineStatu_click"];
    } else if ([serviceName isEqualToString:@"演出赛事"]) {
        [MobClick event:@"my_changeGameStatu_click"];
    } else if ([serviceName isEqualToString:@"宠物"]) {
        [MobClick event:@"my_changePetStatu_click"];
    } else if ([serviceName isEqualToString:@"美食"]) {
        [MobClick event:@"my_changeCateStatu_click"];
    } else if ([serviceName isEqualToString:@"旅行"]) {
        [MobClick event:@"my_changeTravelStatu_click"];
    } else if ([serviceName isEqualToString:@"女性"]) {
        [MobClick event:@"my_changeWomenStatu_click"];
    } else if ([serviceName isEqualToString:@"惊喜"]) {
        [MobClick event:@"my_changeSurpriseStatu_click"];
    } else if ([serviceName isEqualToString:@"拼团"]) {
        [MobClick event:@"my_changeGroupBuyStatu_click"];
    } else if ([serviceName isEqualToString:@"蛋糕"]) {
        [MobClick event:@"my_changeCakeStatu_click"];
    } else if ([serviceName isEqualToString:@"差额"]) {
        [MobClick event:@"my_changeMarginStatu_click"];
    } else if ([serviceName isEqualToString:@"包月用车"]) {
        [MobClick event:@"my_changeTaxiForMonthStatu_click"];
    } else if ([serviceName isEqualToString:@"邀请码"]) {
        [MobClick event:@"my_changeInviteStatu_click"];
    } else if ([serviceName isEqualToString:@"提醒"]) {
        [MobClick event:@"my_changeRemindStatu_click"];
    } else if ([serviceName isEqualToString:@"迎新"]) {
        [MobClick event:@"my_changeNewStatu_click"];
    } else if ([serviceName isEqualToString:@"VIP"]) {
        [MobClick event:@"my_changeVIPStatu_click"];
    }
}

@end
