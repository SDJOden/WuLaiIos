//
//  WYGeniusView.h
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/3/4.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol WYGeniusViewDelegate <NSObject>

-(void)exitGeniusView;

@end

//选择的genius状态的view
@interface WYGeniusView : UIView

@property (nonatomic, weak) id <WYGeniusViewDelegate> geniusViewDelegate;

@end
