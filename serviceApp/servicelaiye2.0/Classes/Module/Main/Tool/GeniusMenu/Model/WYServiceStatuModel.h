//
//  WYServiceStatuModel.h
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/3/2.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WYServiceStatuModel : NSObject

@property (nonatomic, assign) NSInteger serviceStatu;

@property (nonatomic, copy) NSString *serviceName;

@property (nonatomic, assign) BOOL isSelected;

//+(NSArray *)arrayOfServiceIds;

@end
