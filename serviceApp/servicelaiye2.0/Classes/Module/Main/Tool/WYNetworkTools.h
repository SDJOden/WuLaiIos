//
//  WYNetworkTools.h
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/5/3.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>

typedef enum : NSUInteger {
    GET,
    POST,
} TKRequestMethod;

@interface WYNetworkTools : AFHTTPSessionManager

+(instancetype)shareTools;
/**
 *  输入sug
 *
 *  @param query     对话
 *  @param user_id   user_id
 *  @param genius_id genius_id
 *  @param finished  回调
 */
-(void)chatNewInputTextSugTextWithQuery:(NSString *)query andUser_id:(NSString *)user_id andGenius_id:(NSString *)genius_id source:(NSString *)source finished:(void (^)(id responseObject,NSError *error))finished;

-(void)chatSugViewWithQuery:(NSString *)query andUser_id:(NSString *)user_id andGenius_id:(NSString *)genius_id source:(NSString *)source finished:(void (^)(id responseObject,NSError *error))finished;
/**
 *  点赞
 *
 *  @param user_id   userId
 *  @param msg_id    msgId
 *  @param genius_id geniusId
 *  @param user_msg  userMsg
 *  @param timestamp 时间戳
 *  @param finished  回调
 */
-(void)loveWithUserId:(NSString *)user_id andMsgId:(NSString *)msg_id andGeniusId:(NSString *)genius_id andUserMsg:(NSString *)user_msg andTimeStamp:(NSString *)timestamp finished:(void (^)(id responseObject,NSError *error))finished;
/**
 *  AI闭嘴
 */
-(void)shutUp:(BOOL)isShutUp andInnerUid:(NSString *)inner_uid finished:(void (^)(id responseObject,NSError *error))finished;
/**
 *  当日点赞排行
 */
-(void)todayLoveListWithCategory:(NSString *)category finished:(void (^)(id responseObject,NSError *error))finished;
/**
 *  当日点赞会话排行
 */
-(void)todayLoveSessionWithCategory:(NSString *)category finished:(void (^)(id responseObject,NSError *error))finished;
/**
 *  点赞助理排行
 */
-(void)loveRankWithRequestType:(NSString *)requestType finished:(void (^)(id responseObject,NSError *error))finished;
/**
 *  点赞语句排行
 */
-(void)lastDayLoveFinished:(void (^)(id responseObject,NSError *error))finished;
/**
 *  点赞会话排行 http://101.200.177.95/api/nlp/lstday/session/like
 */
-(void)lastDayLoveSessionFinished:(void (^)(id responseObject,NSError *error))finished;
/**
 *  点赞
 */
-(void)loveThisMsgQuery:(NSString *)query andReply:(NSString *)reply andGeniusId:(NSString *)genius_id andTimeStamp:(NSString *)timestamp finished:(void (^)(id responseObject,NSError *error))finished;
/**
 *  踩 http://101.200.177.95/api/nlp/update/like/?query=小来帮我买份粥吧&reply=好的，除了粥还需要其他的吗&genius_id=2&timestamp=1461646204000&like=-1
 */
-(void)hateThisMsgQuery:(NSString *)query andReply:(NSString *)reply andGeniusId:(NSString *)genius_id andTimeStamp:(NSString *)timestamp finished:(void (^)(id responseObject,NSError *error))finished;
/**
 *  更改品类 http://101.200.177.95/api/nlp/update/like/?query=小来帮我买份粥吧&reply=好的，除了粥还需要其他的吗&genius_id=2&timestamp=1461646204000&category=外卖
 */
-(void)updateThisMsgInfo:(NSString *)query andReply:(NSString *)reply andGeniusId:(NSString *)genius_id andTimeStamp:(NSString *)timestamp andCategory:(NSString *)category finished:(void (^)(id responseObject,NSError *error))finished;

-(void)loveSessionWithSession:(NSString *)session andGeniusId:(NSString *)genius_id andTimeStamp:(NSString *)timestamp andCategory:(NSString *)category andReason:(NSString *)reason finished:(void (^)(id responseObject,NSError *error))finished;
/**
 *  点赞会话 http://101.200.177.95/api/nlp/update/session/?doc_id=84be7e85bcd41a238d3e0f2256220ba0&category=用车&timestamp=1463127295000&genius_id=1
 */
-(void)loveSessionWithDocId:(NSString *)doc_id andCategory:(NSString *)category andTimeStamp:(NSString *)timestamp andGeniusId:(NSString *)genius_id finished:(void (^)(id responseObject,NSError *error))finished;
/**
 *  Session踩
 */
-(void)hateSessionWithDocId:(NSString *)doc_id andTimeStamp:(NSString *)timestamp andGeniusId:(NSString *)genius_id finished:(void (^)(id responseObject,NSError *error))finished;

-(void)request:(TKRequestMethod)method urlString:(NSString *)urlString parameters:(id)parameters finished:(void (^)(id responseObject,NSError *error))finished;

-(void)loveSessionWithCategory:(NSString *)category andStatu:(int)statu andPn:(int)pn finished:(void (^)(id responseObject,NSError *error))finished;

-(void)searchLoveSessionWithPn:(NSString *)pn withQuery:(NSString *)query withQueryType:(NSString *)queryType finished:(void (^)(id responseObject,NSError *error))finished;
/**
 *  标注数
 */
-(void)signNumWithGeniusIdFinished:(void (^)(id responseObject,NSError *error))finished;
/**
 *  标注内容
 */
-(void)signDataWithGeniusIdAndTime:(NSString *)time finished:(void (^)(id responseObject,NSError *error))finished;

-(void)signMessageWithDocId:(NSString *)doc_id andCategory:(NSString *)category finished:(void (^)(id responseObject,NSError *error))finished;

-(void)loveSearchImportWordsfinished:(void (^)(id responseObject,NSError *error))finished;
-(void)requestServiceCategoryFinished:(void (^)(id responseObject,NSError *error))finished;

@end
