//
//  UIBarButtonItem+Extension.h
//  servicelaiye2.0
//
//  Created by 汪洋 on 16/4/9.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIBarButtonItem (Extension)

+(instancetype)itemWithImageNamed:(NSString *)imageName target:(id)target action:(SEL)action;

+(instancetype)itemWithTitle:(NSString*)title target:(id)target action:(SEL)action left:(BOOL)isLeft;

@end
