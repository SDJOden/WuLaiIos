//
//  WYTool.h
//  servicelaiye
//
//  Created by 汪洋 on 15/12/17.
//  Copyright © 2015年 汪洋. All rights reserved.
//

#import <Foundation/Foundation.h>

#warning to do 这个类耦合得太厉害了.

@class RCMessageModel;
@class ChatUserDataModel;
@class WYWaitingUserData;
@class WYConnectInformationModel;
@class WYHistoryModel;
@class SDJUserAccount;
@class WYChangeGeniusListModel;
@class WYRecordListModel;

@interface WYTool : NSObject

+(WYConnectInformationModel *)modelWithHistoryDict:(NSDictionary *)historyDict;

+(NSDictionary *)msgWithText:(NSString *)text andUserData:(WYConnectInformationModel *)userData andTime:(long)time;

+(NSDictionary *)msgWithUserData:(WYConnectInformationModel *)userData andTime:(long)time;

+(NSDictionary *)msgWithImageURLString:(NSString *)imageURLString andDic:(NSDictionary *)dict;

//+(NSString *)contentWithUserData:(WYConnectInformationModel *)userData;

+(RCMessageModel *)createMsgModel:(NSDictionary *)dict;

+(RCMessageModel *)createImageModel:(NSDictionary *)dict;

+(RCMessageModel *)createVoiceMsgModel:(NSDictionary *)dict;

+(RCMessageModel *)createCardMsgModel:(NSDictionary *)dict;

+(RCMessageModel *)createSingleCardMsgModel:(NSDictionary *)dict;

+(RCMessageModel *)createDateModel:(NSDictionary *)dict;
//+(RCMessageModel *)createNoticeModel:(NSDictionary *)dict;

+(WYHistoryModel *)createMsgSearchListModelByDict:(NSDictionary *)dict;

+(WYHistoryModel *)createHistoryModelByDict:(NSDictionary *)dict;

+(WYHistoryModel *)createRecordListModelByDict:(NSDictionary *)dict;

+(WYChangeGeniusListModel *)createChangeGeniusListModelByDict:(NSDictionary *)dict;

+(NSDictionary *)jsonStringToDictionary:(NSString *)jsonString;

+(NSString*)dictionaryToJson:(NSDictionary *)dic;

+(NSMutableArray *)geniusArrayOfArray:(NSArray *)array;
/**
 *  SHA1加密算法
 */
//+ (NSString*) sha1:(NSString*)input;

+ (CGSize)labelHeightWithText:(NSString *)text andWidth:(CGFloat)width andFont:(UIFont *)font;

+ (CGSize)labelHeightWithText:(NSString *)text andWidth:(CGFloat)width;

+ (NSString *)today;

+ (NSDate *)dateFromeDateStr:(NSString *)string;

+ (NSString *)currentDateStrFromeDate:(NSDate *)date;

@end
