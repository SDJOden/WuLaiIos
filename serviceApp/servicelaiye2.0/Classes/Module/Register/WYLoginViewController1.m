//
//  WYLoginViewController.m
//  Laiye
//
//  Created by 汪洋 on 15/12/9.
//  Copyright © 2015年 wangyang. All rights reserved.
//
#import "WYLoginViewController.h"
#import "WYMenuViewController.h"
#define BtnHeight 44
#define BtnWidth self.view.width * 0.8
#define SpaceWithSuperview self.view.width * 0.1
#define SpaceBetweenTextBtnAndWarningLabel 10
#define SpaceBetweenWarningAndLabelTextBtn 20
#define SpaceCommon 60
#define LabelHeight 20


typedef enum : NSInteger {
    WYLoginStatuClose = -1,
    WYLoginStatuCanConnect = 0,
    WYLoginStatuCanUse = 5/*接入人数上限为5人，线上版本修改成15，后期后端可能会做策略获取*/
} WYLoginStatu;

@interface WYLoginViewController () <UITableViewDelegate, UITableViewDataSource>
/**
 *  背景图
 */
@property (nonatomic, strong) UIButton *titleBtn;
/**
 *  用户名背景按钮
 */
@property (nonatomic, strong) UIButton *userBackBtn;
/**
 *  邮箱第一个label
 */
@property (nonatomic, strong) UILabel *geniusLabel;
/**
 *  用户名输入框
 */
@property (strong, nonatomic) UITextField *usernameTextField;
/**
 *  邮箱第二个label
 */
@property (nonatomic, strong) UILabel *userBtnLastLabel;
/**
 *  用户名是否为空的提示label
 */
@property (strong, nonatomic) UILabel *userWarningLabel;
/**
 *  密码背景
 */
@property (nonatomic, strong) UIButton *passBackBtn;  // mark
/**
 *  密码输入框
 */
@property (strong, nonatomic) UITextField *passwordTextField;
/**
 *  密码是否为空的提示label
 */
@property (strong, nonatomic) UILabel *passWarningLabel;
/**
 *  连接状态label
 */
@property (nonatomic, strong) UILabel *connectLabel;
/**
 *  连接状态选择按钮
 */
@property (nonatomic, strong) UIButton *statuBtn;
/**
 *  登陆按钮
 */
@property (nonatomic, strong) UIButton *loginBtn;
/**
 *  连接检测label
 */
@property (strong, nonatomic) UILabel *connectWarningLabel;
/**
 *  空白背景按钮
 */
@property (nonatomic, strong) UIButton *blankBtn;
/**
 *  状态选择tableView
 */
@property (nonatomic, strong) UITableView *statuTableView;
/**
 *  状态，记录tableView选择
 */
@property (nonatomic, assign) int statu;
@end

@implementation WYLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [UIApplication sharedApplication].statusBarHidden = YES;
    //设置子控件
    [self setupSubViews];
    //布局
    [self setupAutolayout];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    /**
     *  初始化状态为-1
     */
    self.statu = -1;
}
/**
 *  设置子控件
 */
-(void)setupSubViews{
    
    self.view.backgroundColor = [UIColor colorWithRed:1.0 green:0.8902 blue:0.0 alpha:1.0];
    
    //添加子视图控制器
//    [self.view addSubview:self.backgroudImageView];
    [self.view addSubview:self.titleBtn];
    [self.view addSubview:self.userBackBtn];
    [self.userBackBtn addSubview:self.geniusLabel];
    [self.userBackBtn addSubview:self.usernameTextField];
    [self.userBackBtn addSubview:self.userBtnLastLabel];
    [self.view addSubview:self.userWarningLabel];
    [self.view addSubview:self.passBackBtn];
    [self.passBackBtn addSubview:self.passwordTextField];
    [self.view addSubview:self.passWarningLabel];
    [self.view addSubview:self.connectLabel];
    [self.view addSubview:self.statuBtn];
    [self.view addSubview:self.loginBtn];
    [self.view addSubview:self.connectWarningLabel];
    
    // 监听textField内容改变
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textChanged:) name:UITextFieldTextDidChangeNotification object:self.usernameTextField];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textChanged:) name:UITextFieldTextDidChangeNotification object:self.passwordTextField];
}

//自动布局
-(void)setupAutolayout{
    
    [self.titleBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).with.offset(40);
        make.left.equalTo(self.view).with.offset(30);
        make.right.equalTo(self.view).with.offset(-30);
        make.height.mas_equalTo(100);
    }];
    
    [self.userBackBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).with.offset(30);
        make.right.equalTo(self.view).with.offset(-30);
        make.top.equalTo(self.view).with.offset(180);
        make.height.mas_equalTo(44);
    }];
    
    [self.geniusLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.geniusLabel.superview);
        make.left.equalTo(self.geniusLabel.superview).with.offset(10);
        make.width.mas_equalTo(50);
        make.height.mas_equalTo(30);
    }];
    
    [self.userBtnLastLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.userBackBtn);
        make.right.equalTo(self.geniusLabel.superview).with.offset(-10);
        make.width.mas_equalTo(80);
        make.height.mas_equalTo(30);
    }];

    [self.usernameTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.userBackBtn);
        make.left.equalTo(self.geniusLabel.mas_right).with.offset(10);
        make.right.equalTo(self.userBtnLastLabel.mas_left).with.offset(-10);
        make.height.mas_equalTo(30);
    }];

    [self.userWarningLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.userBackBtn.mas_bottom).with.offset(10);
        make.left.equalTo(self.view).with.offset(30);
        make.right.equalTo(self.view).with.offset(-30);
        make.height.mas_equalTo(20);
    }];
    [self.passBackBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).with.offset(30);
        make.right.equalTo(self.view).with.offset(-30);
        make.top.equalTo(self.userWarningLabel.mas_bottom).with.offset(20);
        make.height.mas_equalTo(44);
    }];
    [self.passwordTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(self.passBackBtn);
        make.left.equalTo(self.passBackBtn).with.offset(10);
        make.right.equalTo(self.passBackBtn).with.offset(-10);
        make.height.mas_equalTo(30);
    }];
    [self.passWarningLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.passBackBtn.mas_bottom).with.offset(10);
        make.left.equalTo(self.view).with.offset(30);
        make.right.equalTo(self.view).with.offset(-30);
        make.height.mas_equalTo(20);
    }];
    [self.connectLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.passWarningLabel.mas_bottom).with.offset(10);
        make.left.equalTo(self.view).with.offset(30);
        make.width.mas_equalTo(80);
        make.height.mas_equalTo(20);
    }];
    
    [self.statuBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.connectLabel.mas_right).with.offset(20);
        make.centerY.equalTo(self.connectLabel.mas_centerY);
        make.width.mas_equalTo(80);
        make.height.mas_equalTo(30);
    }];
    
    [self.connectWarningLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.statuBtn.mas_bottom).with.offset(10);
        make.left.equalTo(self.view).with.offset(30);
        make.right.equalTo(self.view).with.offset(-30);
        make.height.mas_equalTo(20);
    }];
    
    [self.loginBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.connectWarningLabel.mas_bottom).with.offset(10);
        make.left.equalTo(self.view).with.offset(30);
        make.right.equalTo(self.view).with.offset(-30);
        make.height.mas_equalTo(44);
    }];
    
}
#pragma mark --- 懒加载
- (UIButton *)titleBtn {
    if(_titleBtn == nil) {
        _titleBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _titleBtn.userInteractionEnabled = NO;
        [_titleBtn setImage:[UIImage imageNamed:@"system_portrait"] forState:UIControlStateNormal];
        [_titleBtn setTitle:@"天才助理" forState:UIControlStateNormal];
        [_titleBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _titleBtn.titleLabel.font = [UIFont systemFontOfSize:40];
    }
    return _titleBtn;
}

- (UIButton *)userBackBtn {
    if(_userBackBtn == nil) {
        UIButton *userBackBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        userBackBtn.backgroundColor = [UIColor whiteColor];
        userBackBtn.layer.cornerRadius = 5.0;
        userBackBtn.layer.masksToBounds = YES;
        _userBackBtn = userBackBtn;
    }
    return _userBackBtn;
}

- (UILabel *)geniusLabel {
    if(_geniusLabel == nil) {
        UILabel *geniusLabel = [[UILabel alloc]init];
        geniusLabel.backgroundColor = [UIColor clearColor];
        geniusLabel.text = @"Genius";
        geniusLabel.textAlignment = NSTextAlignmentCenter;
        geniusLabel.textColor = [UIColor blackColor];
        geniusLabel.font = [UIFont systemFontOfSize:14];
        _geniusLabel = geniusLabel;
    }
    return _geniusLabel;
}

- (UITextField *)usernameTextField {
    if(_usernameTextField == nil) {
        UITextField *usernameTextField = [[UITextField alloc]init];
        usernameTextField.backgroundColor = [UIColor clearColor];
        usernameTextField.placeholder = @"请输入用户名";
        usernameTextField.font = [UIFont systemFontOfSize:14];
        usernameTextField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
        usernameTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
        usernameTextField.returnKeyType = UIReturnKeyNext;
        [usernameTextField addTarget:self action:@selector(returnByUsernameTextField) forControlEvents:UIControlEventEditingDidEndOnExit];
        _usernameTextField = usernameTextField;
    }
    return _usernameTextField;
}

- (UILabel *)userBtnLastLabel {
    if(_userBtnLastLabel == nil) {
        //用户名label
        UILabel *userBtnLastLabel = [[UILabel alloc]init];
        userBtnLastLabel.backgroundColor = [UIColor clearColor];
        userBtnLastLabel.text = @"@zhulilaiye";
        userBtnLastLabel.textAlignment = NSTextAlignmentCenter;
        userBtnLastLabel.textColor = [UIColor blackColor];
        userBtnLastLabel.font = [UIFont systemFontOfSize:14];
        _userBtnLastLabel = userBtnLastLabel;
    }
    return _userBtnLastLabel;
}

- (UILabel *)userWarningLabel {
    if(_userWarningLabel == nil) {
        UILabel *userWarningLabel = [[UILabel alloc]init];
        userWarningLabel.text = @"*用户名不能为空";
        userWarningLabel.textColor = [UIColor redColor];
        userWarningLabel.font = [UIFont systemFontOfSize:14];
        userWarningLabel.hidden = YES;
        
        _userWarningLabel = userWarningLabel;
    }
    return _userWarningLabel;
}

- (UIButton *)passBackBtn {
    if(_passBackBtn == nil) {
        //密码背景
        UIButton *passBackBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        passBackBtn.layer.cornerRadius = 5.0;
        passBackBtn.layer.masksToBounds = YES;
        passBackBtn.backgroundColor = [UIColor whiteColor];
        _passBackBtn = passBackBtn;
    }
    return _passBackBtn;
}

- (UITextField *)passwordTextField {
    if(_passwordTextField == nil) {
        UITextField *passwordTextField = [[UITextField alloc]init];
        passwordTextField.backgroundColor = [UIColor clearColor];
        passwordTextField.placeholder = @"请输入密码";
        passwordTextField.font = [UIFont systemFontOfSize:14];
        passwordTextField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
        passwordTextField.secureTextEntry = YES;
        passwordTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
        passwordTextField.returnKeyType = UIReturnKeyDone;
        [passwordTextField addTarget:self action:@selector(login) forControlEvents:UIControlEventEditingDidEndOnExit];
        
        _passwordTextField = passwordTextField;
    }
    return _passwordTextField;
}

- (UILabel *)passWarningLabel {
    if(_passWarningLabel == nil) {
        UILabel *passWarningLabel = [[UILabel alloc]init];
        passWarningLabel.text = @"*密码不能为空";
        passWarningLabel.textColor = [UIColor redColor];
        passWarningLabel.font = [UIFont systemFontOfSize:14];
        passWarningLabel.hidden = YES;
        
        _passWarningLabel = passWarningLabel;
    }
    return _passWarningLabel;
}

- (UILabel *)connectLabel {
    if(_connectLabel == nil) {
        //状态label
        UILabel *connectLabel = [[UILabel alloc]init];
        connectLabel.text = @"接入状态";
        connectLabel.font = [UIFont systemFontOfSize:14];
        _connectLabel = connectLabel;
    }
    return _connectLabel;
}

- (UIButton *)statuBtn {
    if(_statuBtn == nil) {
        //状态按钮
        UIButton *statuBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        statuBtn.backgroundColor = [UIColor whiteColor];
        [statuBtn setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        [statuBtn setTitle:@"关闭" forState:UIControlStateNormal];
        statuBtn.titleLabel.textAlignment = NSTextAlignmentLeft;
        statuBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0);
        statuBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        [statuBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        statuBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        statuBtn.layer.cornerRadius = 3.0;
        statuBtn.layer.masksToBounds = YES;
        [statuBtn addTarget:self action:@selector(addStatu) forControlEvents:UIControlEventTouchUpInside];
        _statuBtn = statuBtn;
    }
    return _statuBtn;
}

- (UIButton *)loginBtn {
    if(_loginBtn == nil) {
        UIButton *loginBtn = [[UIButton alloc]init];
        loginBtn.backgroundColor = [UIColor colorWithWhite:0.667 alpha:0.500];
        [loginBtn setTitle:@"登陆" forState:UIControlStateNormal];
        [loginBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [loginBtn setTitleColor:[UIColor colorWithWhite:0.000 alpha:0.500] forState:UIControlStateDisabled];
        [loginBtn addTarget:self action:@selector(login) forControlEvents:UIControlEventTouchUpInside];
        loginBtn.enabled = NO;
        _loginBtn = loginBtn;
    }
    return _loginBtn;
}

- (UILabel *)connectWarningLabel {
    if(_connectWarningLabel == nil) {
        UILabel *connectWarningLabel = [[UILabel alloc]init];
        connectWarningLabel.text = @"*用户名密码不正确";
        connectWarningLabel.textColor = [UIColor redColor];
        connectWarningLabel.font = [UIFont systemFontOfSize:14];
        connectWarningLabel.hidden = YES;
        
        _connectWarningLabel = connectWarningLabel;
    }
    return _connectWarningLabel;
}

- (UITableView *)statuTableView {
    if(_statuTableView == nil) {
        _statuTableView = [[UITableView alloc] init];
        _statuTableView.delegate = self;
        _statuTableView.dataSource = self;
        _statuTableView.bounces = NO;
        _statuTableView.layer.borderWidth = 1;
        _statuTableView.layer.cornerRadius = 5.0;
        _statuTableView.layer.borderColor = [[UIColor lightGrayColor] CGColor];
        [_statuTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"statuCell"];
    }
    return _statuTableView;
}

- (UIButton *)blankBtn {
    if(_blankBtn == nil) {
        _blankBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _blankBtn.backgroundColor = [UIColor blackColor];
        _blankBtn.alpha = 0.1;
        [_blankBtn addTarget:self action:@selector(hiddenTableView:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _blankBtn;
}

//点击按钮添加空白背景按钮和tableView
-(void)addStatu{
    [self.view addSubview:self.blankBtn];
    [self.view addSubview:self.statuTableView];
    [self.blankBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.bottom.equalTo(self.view);
    }];
    
    [self.statuTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.statuBtn.mas_top);
        make.left.equalTo(self.statuBtn.mas_left);
        make.right.equalTo(self.statuBtn.mas_right);
        make.height.mas_equalTo(60);
    }];
}

/**
 *  登陆
 */
-(void)login{
    if (![self.usernameTextField.text isEqualToString:@""] && ![self.passwordTextField.text isEqualToString:@""]) {
        [[SDJNetworkTools sharedTools]login:self.usernameTextField.text password:self.passwordTextField.text limit:@(self.statu).stringValue finished:^(NSDictionary<NSString *,id> * _Nullable result, NSError * _Nullable error) {
            if (!error) {
                if (![result[@"error"] isKindOfClass:[NSNull class]] && [[result[@"error"] allKeys] containsObject:@"error_code"]) {
                }
                if ([result[@"error"]isKindOfClass:[NSDictionary class]]) {
                    self.connectWarningLabel.hidden = NO;
                }
                else{
                    self.connectWarningLabel.hidden = YES;
                    NSLog(@"%@", NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).firstObject);
                    NSDictionary *dic = result[@"data"][@"genius"];
                    [[[SDJUserAccount alloc] initWithDic:dic geniustoken:result[@"data"][@"token"] enable:YES log:self.statu service:-1] savaAccount];
                    // 开始登录
                    [[RCIM sharedRCIM] connectWithToken:[SDJUserAccount loadAccount].rc_token success:^(NSString *userId) {
                        //                    [[RCIM sharedRCIM] setUserInfoDataSource:[SDJUserInfoDataSource sharedUserInfoDataSource]];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            NSDictionary *dic = @{@"statu":self.statuBtn.titleLabel.text};
                            [[NSNotificationCenter defaultCenter] postNotificationName:@"WYRootViewControllerSwitchNotification" object:@"1" userInfo:dic];
                        });
                        
                    } error:^(RCConnectErrorCode status) {
                        NSLog(@"login error status: %ld.", (long)status);
                    } tokenIncorrect:^{
                        NSLog(@"token 无效 ，请确保生成token 使用的appkey 和初始化时的appkey 一致");
                    }];
                }
            }
            else {
                
            }
        }];
    }
}

/**
 *  点空白处取消textField编辑状态
 */
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

/**
 *  切换到第二个textField输入
 */
- (void)returnByUsernameTextField {
    [self.passwordTextField becomeFirstResponder];
}

/**
 *  textField内容改变执行下列方法
 */
- (void)textChanged:(NSNotification *)noti {
    if (noti.object == self.usernameTextField) {
        self.userWarningLabel.hidden = (self.usernameTextField.text.length > 0);
        
    } else {
        self.passWarningLabel.hidden = (self.passwordTextField.text.length > 0);
    }
    
    if ((self.usernameTextField.text.length > 0 && self.passwordTextField.text.length > 0)) {
        self.loginBtn.backgroundColor = [UIColor blackColor];
    }
    else {
        self.loginBtn.backgroundColor = [UIColor colorWithWhite:0.667 alpha:0.500];
    }
    self.loginBtn.enabled = (self.usernameTextField.text.length > 0 && self.passwordTextField.text.length > 0);
}


-(BOOL)prefersStatusBarHidden{
    return YES;
}

#pragma mark --- UITableViewDelegate
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 3;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString * identifier = @"statuCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    cell.textLabel.font = [UIFont systemFontOfSize:12];
    switch (indexPath.row) {
        case 0:
            cell.textLabel.text = @"关闭";
            break;
        case 1:
            cell.textLabel.text = @"接单";
            break;
        case 2:
            cell.textLabel.text = @"派单";
            break;
        default:
            break;
    }
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 20;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.row) {
        case 0:
            [self.statuBtn setTitle:@"关闭" forState:UIControlStateNormal];
            self.statu = WYLoginStatuClose;
            break;
        case 1:
            [self.statuBtn setTitle:@"接单" forState:UIControlStateNormal];
            self.statu = WYLoginStatuCanConnect;
            break;
        case 2:
            [self.statuBtn setTitle:@"派单" forState:UIControlStateNormal];
            self.statu = WYLoginStatuCanUse;
            break;
        default:
            break;
    }
    [self.blankBtn removeFromSuperview];
    [self.statuTableView removeFromSuperview];
    self.blankBtn = nil;
    self.statuTableView = nil;
}

-(void)hiddenTableView:(UIButton *)sender{
    [self.statuTableView removeFromSuperview];
    [sender removeFromSuperview];
    self.statuTableView = nil;
    sender = nil;
}



@end
