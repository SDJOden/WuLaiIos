//
//  WYLoginViewController.m
//  Laiye
//
//  Created by 汪洋 on 15/12/9.
//  Copyright © 2015年 wangyang. All rights reserved.
//
#import "WYLoginViewController.h"
#import "WYMenuViewController.h"
#import "MBProgressHUD+MJ.h"
#import "MBProgressHUD.h"
#define BtnHeight 44
#define BtnWidth self.view.width * 0.8
#define SpaceWithSuperview self.view.width * 0.1
#define SpaceBetweenTextBtnAndWarningLabel 10
#define SpaceBetweenWarningAndLabelTextBtn 20
#define SpaceCommon 60
#define LabelHeight 20

#warning to do // tag = 0是禁用的.因为所有view默认tag都是0
typedef enum : NSInteger {
    WYLoginStatuClose = -1 + 500,
    WYLoginStatuCanConnect = 500,
    WYLoginStatuCanUse = 505/*接入人数上限为5人，线上版本修改成15，后期后端可能会做策略获取*/
} WYLoginStatu;

@interface WYLoginViewController () <UITextFieldDelegate>
/**
 *  背景图
 */
@property (nonatomic, strong) UILabel *titleLb;
/**
 *  邮箱第一个label
 */
@property (nonatomic, strong) UILabel *geniusLabel;
/**
 *  直线
 */
@property (nonatomic, strong) UIView *geniusLineView;
/**
 *  passwordLabel
 */
@property (nonatomic, strong) UILabel *passwordLabel;
/**
 *  用户名输入框
 */
@property (strong, nonatomic) UITextField *usernameTextField;
/**
 *  直线
 */
@property (nonatomic, strong) UIView *passwordLineView;
/**
 *  用户名是否为空的提示label
 */
@property (strong, nonatomic) UILabel *userWarningLabel;
/**
 *  密码输入框
 */
@property (strong, nonatomic) UITextField *passwordTextField;
/**
 *  密码是否为空的提示label
 */
@property (strong, nonatomic) UILabel *passWarningLabel;
/**
 *  连接状态label
 */
@property (nonatomic, strong) UILabel *connectLabel;
/**
 *  登陆按钮
 */
@property (nonatomic, strong) UIButton *loginBtn;
/**
 *  连接检测label
 */
@property (strong, nonatomic) UILabel *connectWarningLabel;
/**
 *  接单状态statuLabel
 */
@property (nonatomic, strong) UILabel *statuLabel;
/**
 *  关闭按钮
 */
@property (nonatomic, strong) UIButton *closeBtn;
/**
 *  接单按钮
 */
@property (nonatomic, strong) UIButton *connectBtn;
/**
 *  派单按钮
 */
@property (nonatomic, strong) UIButton *onlineBtn;
/**
 *  状态，记录tableView选择
 */
@property (nonatomic, assign) int statu;

@property (nonatomic, strong) UIButton *secureBtn;

@end

@implementation WYLoginViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    //设置子控件
    [self setupSubViews];
    //布局
    [self setupAutolayout];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    /**
     *  初始化状态为-1
     */
    self.statu = -1;
}
/**
 *  设置子控件
 */
-(void)setupSubViews{

    self.view.backgroundColor = [UIColor whiteColor];
    // 监听textField内容改变
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textChanged:) name:UITextFieldTextDidChangeNotification object:self.usernameTextField];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textChanged:) name:UITextFieldTextDidChangeNotification object:self.passwordTextField];
}

//自动布局
-(void)setupAutolayout{
    [self.titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(30);
        make.centerX.equalTo(self.view);
        make.width.equalTo(100);
        make.height.equalTo(50);
    }];
    
    [self.geniusLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(20);
        make.width.equalTo(80);
        make.height.equalTo(30);
        make.top.equalTo(self.titleLb.bottom).offset(40);
    }];

    [self.usernameTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.geniusLabel.right);
        make.height.equalTo(30);
        make.right.equalTo(self.view).offset(-20);
        make.top.equalTo(self.geniusLabel);
    }];
    
    [self.geniusLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).offset(20);
        make.right.equalTo(self.view).offset(-20);
        make.height.equalTo(1);
        make.top.equalTo(self.geniusLabel.bottom);
    }];
    
    [self.userWarningLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).offset(20);
        make.right.equalTo(self.view).offset(-20);
        make.height.equalTo(20);
        make.top.equalTo(self.geniusLabel.bottom).offset(10);
    }];
    
    [self.passwordLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.width.height.equalTo(self.geniusLabel);
        make.top.equalTo(self.userWarningLabel.bottom).offset(10);
    }];
    
    [self.passwordTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.secureBtn.left);
        make.left.height.equalTo(self.usernameTextField);
        make.top.equalTo(self.passwordLabel);
    }];
    
    [self.secureBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.passwordLabel);
        make.right.equalTo(self.usernameTextField);
        make.height.equalTo(25);
        make.width.equalTo(60);
    }];
    
    [self.passwordLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).offset(20);
        make.right.equalTo(self.view).offset(-20);
        make.height.equalTo(1);
        make.top.equalTo(self.passwordLabel.bottom);
    }];
    
    [self.passWarningLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).offset(20);
        make.right.equalTo(self.view).offset(-20);
        make.height.equalTo(20);
        make.top.equalTo(self.passwordLabel.bottom).offset(10);
    }];
    
    [self.loginBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.geniusLineView);
        make.top.equalTo(self.passWarningLabel.bottom).offset(10);
        make.height.equalTo(44);
    }];
    
    [self.connectWarningLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).offset(20);
        make.right.equalTo(self.view).offset(-20);
        make.height.equalTo(20);
        make.top.equalTo(self.loginBtn.bottom).offset(10);
    }];
    
    [self.statuLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).offset(20);
        make.top.equalTo(self.connectWarningLabel.bottom).offset(10);
        make.height.equalTo(30);
        make.width.equalTo(60);
    }];
    
    [self.closeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.statuLabel.right).offset(10);
        make.top.equalTo(self.statuLabel.top);
        make.height.equalTo(30);
        make.width.equalTo(self.connectBtn.width);
    }];
    
    [self.connectBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.closeBtn.right).offset(10);
        make.top.equalTo(self.statuLabel.top);
        make.height.equalTo(30);
        make.width.equalTo(self.onlineBtn.width);
    }];
    
    [self.onlineBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.connectBtn.right).offset(10);
        make.top.equalTo(self.statuLabel.top);
        make.height.equalTo(30);
        make.right.equalTo(self.view).offset(-20);
    }];
}

#pragma mark --- 懒加载
- (UILabel *)titleLb {
    
    if(_titleLb == nil) {
        _titleLb = [[UILabel alloc] init];
        _titleLb.text = @"天才助理";
        _titleLb.textAlignment = NSTextAlignmentCenter;
        _titleLb.font = [UIFont systemFontOfSize:20];
        [self.view addSubview:_titleLb];
    }
    return _titleLb;
}

- (UILabel *)geniusLabel {
    
    if(_geniusLabel == nil) {
        UILabel *geniusLabel = [[UILabel alloc]init];
        geniusLabel.backgroundColor = [UIColor clearColor];
        geniusLabel.text = @"Genius ID";
        geniusLabel.textAlignment = NSTextAlignmentCenter;
        geniusLabel.textColor = [UIColor blackColor];
        geniusLabel.font = [UIFont boldSystemFontOfSize:14];
        _geniusLabel = geniusLabel;
        [self.view addSubview:_geniusLabel];
    }
    return _geniusLabel;
}

- (UILabel *)passwordLabel {
    
    if(_passwordLabel == nil) {
        UILabel *passwordLabel = [[UILabel alloc]init];
        passwordLabel.backgroundColor = [UIColor clearColor];
        passwordLabel.text = @"Password";
        passwordLabel.textAlignment = NSTextAlignmentCenter;
        passwordLabel.textColor = [UIColor blackColor];
        passwordLabel.font = [UIFont boldSystemFontOfSize:14];
        _passwordLabel = passwordLabel;
        [self.view addSubview:_passwordLabel];
    }
    return _passwordLabel;
}

- (UITextField *)usernameTextField {
    
    if(_usernameTextField == nil) {
        UITextField *usernameTextField = [[UITextField alloc]init];
        usernameTextField.backgroundColor = [UIColor clearColor];
        usernameTextField.placeholder = @"请输入用户名";
        usernameTextField.font = [UIFont systemFontOfSize:14];
        usernameTextField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
        usernameTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
        usernameTextField.returnKeyType = UIReturnKeyNext;
        [usernameTextField addTarget:self action:@selector(returnByUsernameTextField) forControlEvents:UIControlEventEditingDidEndOnExit];
        usernameTextField.delegate = self;
        _usernameTextField = usernameTextField;
        [self.view addSubview:_usernameTextField];
    }
    return _usernameTextField;
}

- (UITextField *)passwordTextField {
    
    if(_passwordTextField == nil) {
        UITextField *passwordTextField = [[UITextField alloc]init];
        passwordTextField.backgroundColor = [UIColor clearColor];
        passwordTextField.placeholder = @"请输入密码";
        passwordTextField.font = [UIFont systemFontOfSize:14];
        passwordTextField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
        passwordTextField.secureTextEntry = YES;
        passwordTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
        passwordTextField.returnKeyType = UIReturnKeyDone;
        [passwordTextField addTarget:self action:@selector(login) forControlEvents:UIControlEventEditingDidEndOnExit];
        passwordTextField.delegate = self;
        _passwordTextField = passwordTextField;
        [self.view addSubview:_passwordTextField];
    }
    return _passwordTextField;
}

- (UILabel *)userWarningLabel {
    
    if(_userWarningLabel == nil) {
        UILabel *userWarningLabel = [[UILabel alloc]init];
        userWarningLabel.text = @"*用户名不能为空";
        userWarningLabel.textColor = [UIColor redColor];
        userWarningLabel.font = [UIFont systemFontOfSize:14];
        userWarningLabel.hidden = YES;
        _userWarningLabel = userWarningLabel;
        [self.view addSubview:_userWarningLabel];
    }
    return _userWarningLabel;
}

- (UILabel *)passWarningLabel {
    
    if(_passWarningLabel == nil) {
        UILabel *passWarningLabel = [[UILabel alloc]init];
        passWarningLabel.text = @"*密码不能为空";
        passWarningLabel.textColor = [UIColor redColor];
        passWarningLabel.font = [UIFont systemFontOfSize:14];
        passWarningLabel.hidden = YES;
        _passWarningLabel = passWarningLabel;
        [self.view addSubview:_passWarningLabel];
    }
    return _passWarningLabel;
}

- (UILabel *)connectLabel {
    
    if(_connectLabel == nil) {
        //状态label
        UILabel *connectLabel = [[UILabel alloc]init];
        connectLabel.text = @"接入状态";
        connectLabel.font = [UIFont systemFontOfSize:14];
        _connectLabel = connectLabel;
    }
    return _connectLabel;
}

- (UIButton *)loginBtn {
    
    if(_loginBtn == nil) {
        UIButton *loginBtn = [[UIButton alloc]init];
        loginBtn.backgroundColor = [UIColor colorWithWhite:0.925 alpha:1.0];
        [loginBtn setTitle:@"登录" forState:UIControlStateNormal];
        loginBtn.titleLabel.font = [UIFont systemFontOfSize:16];
        [loginBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [loginBtn setTitleColor:[UIColor colorWithWhite:0.000 alpha:0.500] forState:UIControlStateDisabled];
        loginBtn.layer.cornerRadius = 3.0;
        loginBtn.layer.masksToBounds = YES;
        [loginBtn addTarget:self action:@selector(login) forControlEvents:UIControlEventTouchUpInside];
        loginBtn.enabled = NO;
        _loginBtn = loginBtn;
        [self.view addSubview:_loginBtn];
    }
    return _loginBtn;
}

- (UILabel *)connectWarningLabel {
    
    if(_connectWarningLabel == nil) {
        UILabel *connectWarningLabel = [[UILabel alloc]init];
        connectWarningLabel.text = @"*用户名密码不正确";
        connectWarningLabel.textColor = [UIColor redColor];
        connectWarningLabel.font = [UIFont systemFontOfSize:14];
        connectWarningLabel.hidden = YES;
        _connectWarningLabel = connectWarningLabel;
        [self.view addSubview:_connectWarningLabel];
    }
    return _connectWarningLabel;
}

- (UILabel *)statuLabel {
    
    if(_statuLabel == nil) {
        _statuLabel = [[UILabel alloc] init];
        _statuLabel.textAlignment = NSTextAlignmentCenter;
        _statuLabel.text = @"接单状态";
        _statuLabel.font = [UIFont systemFontOfSize:14];
        [self.view addSubview:_statuLabel];
    }
    return _statuLabel;
}

- (UIButton *)closeBtn {
    
    if(_closeBtn == nil) {
        _closeBtn = [[UIButton alloc] init];
        [_closeBtn setTitle:@"关闭" forState:UIControlStateNormal];
        _closeBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        _closeBtn.selected = YES;
        [_closeBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        [_closeBtn setTitleColor:[UIColor blackColor] forState:UIControlStateSelected];
        _closeBtn.layer.cornerRadius = 2.0;
        _closeBtn.layer.masksToBounds = YES;
        _closeBtn.layer.borderColor = [[UIColor blackColor] CGColor];
        _closeBtn.layer.borderWidth = 1;
        [_closeBtn addTarget:self action:@selector(recordStatu:) forControlEvents:UIControlEventTouchUpInside];
        _closeBtn.tag = WYLoginStatuClose;
        [self.view addSubview:_closeBtn];
    }
    return _closeBtn;
}

- (UIButton *)connectBtn {
    
    if(_connectBtn == nil) {
        _connectBtn = [[UIButton alloc] init];
        [_connectBtn setTitle:@"接单" forState:UIControlStateNormal];
        _connectBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        _connectBtn.selected = NO;
        [_connectBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        [_connectBtn setTitleColor:[UIColor blackColor] forState:UIControlStateSelected];
        _connectBtn.layer.cornerRadius = 2.0;
        _connectBtn.layer.masksToBounds = YES;
        _connectBtn.layer.borderColor = [[UIColor lightGrayColor] CGColor];
        _connectBtn.layer.borderWidth = 1;
        [_connectBtn addTarget:self action:@selector(recordStatu:) forControlEvents:UIControlEventTouchUpInside];
        _connectBtn.tag = WYLoginStatuCanConnect;
        [self.view addSubview:_connectBtn];
    }
    return _connectBtn;
}

- (UIButton *)onlineBtn {
    
    if(_onlineBtn == nil) {
        _onlineBtn = [[UIButton alloc] init];
        [_onlineBtn setTitle:@"派单" forState:UIControlStateNormal];
        _onlineBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        _onlineBtn.selected = NO;
        [_onlineBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        [_onlineBtn setTitleColor:[UIColor blackColor] forState:UIControlStateSelected];
        _onlineBtn.layer.cornerRadius = 2.0;
        _onlineBtn.layer.masksToBounds = YES;
        _onlineBtn.layer.borderColor = [[UIColor lightGrayColor] CGColor];
        _onlineBtn.layer.borderWidth = 1;
        [_onlineBtn addTarget:self action:@selector(recordStatu:) forControlEvents:UIControlEventTouchUpInside];
        _onlineBtn.tag = WYLoginStatuCanUse;
        [self.view addSubview:_onlineBtn];
    }
    return _onlineBtn;
}

-(void)recordStatu:(UIButton *)sender{
    
    switch (sender.tag) {
        case -1 + 500:
            [MobClick event:@"login_closeButton_click"];
            break;
        case 0 + 500:
            [MobClick event:@"login_connectButton_click"];
            break;
        case 5 + 500:
            [MobClick event:@"login_distributeButton_click"];
            break;
        default:
            break;
    }
    self.closeBtn.selected = NO;
    self.connectBtn.selected = NO;
    self.onlineBtn.selected = NO;
    [self.closeBtn.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
    [self.connectBtn.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
    [self.onlineBtn.layer setBorderColor:[[UIColor lightGrayColor] CGColor]];
    sender.selected = YES;
    [sender.layer setBorderColor:[[UIColor blackColor] CGColor]];
    self.statu = (int)sender.tag - 500;
}

/**
 *  登陆
 */
-(void)login{
    
    [self.view endEditing:YES];
    
    [MBProgressHUD showMessage:@"正在登陆" toView:self.view];
    
    if (![self.usernameTextField.text isEqualToString:@""] && ![self.passwordTextField.text isEqualToString:@""]) {
        __weak typeof(self) __weakSelf = self;
        [[SDJNetworkTools sharedTools]login:self.usernameTextField.text password:self.passwordTextField.text limit:@(self.statu).stringValue finished:^(NSDictionary<NSString *,id> * _Nullable result, NSError * _Nullable error) {
            if (!error) {
                if (![result[@"error"] isKindOfClass:[NSNull class]] && [[result[@"error"] allKeys] containsObject:@"error_code"]) {
                    // --------------容错处理----------
                }
                if ([result[@"error"]isKindOfClass:[NSDictionary class]]) {
                    // --------------主线程刷新----------
                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                    __weakSelf.connectWarningLabel.hidden = NO;
                }
                else{
                    // --------------主线程刷新----------
                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                    __weakSelf.connectWarningLabel.hidden = YES;
//                    NSLog(@"%@", NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).firstObject);
                    NSDictionary *gesniusDic = result[@"data"][@"genius"];
                    [[[SDJUserAccount alloc] initWithDic:gesniusDic serviceIds:result[@"data"][@"service_ids"] geniustoken:result[@"data"][@"token"] enable:YES log:__weakSelf.statu service:-1] savaAccount];
                    // 开始登录
//                    NSLog(@"%@", [SDJUserAccount loadAccount].rc_token);
                    [[RCIM sharedRCIM] connectWithToken:[SDJUserAccount loadAccount].rc_token success:^(NSString *userId) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [[NSNotificationCenter defaultCenter] postNotificationName:@"WYRootViewControllerSwitchNotification" object:@"1" userInfo:nil];
                        });
                    } error:^(RCConnectErrorCode status) {
                        // --------------容错处理----------
                        NSLog(@"login error status: %ld.", (long)status);
                    } tokenIncorrect:^{
                        NSLog(@"token 无效 ，请确保生成token 使用的appkey 和初始化时的appkey 一致");
                    }];
                }
            }
            else {
                // --------------容错处理----------
            }
        }];
    }
}

/**
 *  点空白处取消textField编辑状态
 */
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    [self.view endEditing:YES];
}

/**
 *  切换到第二个textField输入
 */
- (void)returnByUsernameTextField {
    
    [self.passwordTextField becomeFirstResponder];
}

/**
 *  textField内容改变执行下列方法
 */
- (void)textChanged:(NSNotification *)noti {
    
    if (noti.object == self.usernameTextField) {
        
        self.userWarningLabel.hidden = (self.usernameTextField.text.length > 0);
        
    } else {
        
        self.passWarningLabel.hidden = (self.passwordTextField.text.length > 0);
        
    }
    
    if ((self.usernameTextField.text.length > 0 && self.passwordTextField.text.length > 0)) {
        
        self.loginBtn.backgroundColor = [UIColor blackColor];
        
    } else {
        
        self.loginBtn.backgroundColor = [UIColor colorWithWhite:0.667 alpha:0.500];
    }
    
    self.loginBtn.enabled = (self.usernameTextField.text.length > 0 && self.passwordTextField.text.length > 0);
}

-(BOOL)prefersStatusBarHidden{
    
    return YES;
}

- (UIView *)geniusLineView {
    
	if(_geniusLineView == nil) {
		_geniusLineView = [[UIView alloc] init];
        _geniusLineView.backgroundColor = [UIColor lightGrayColor];
        [self.view addSubview:_geniusLineView];
	}
	return _geniusLineView;
}

- (UIView *)passwordLineView {
    
	if(_passwordLineView == nil) {
		_passwordLineView = [[UIView alloc] init];
        _passwordLineView.backgroundColor = [UIColor lightGrayColor];
        [self.view addSubview:_passwordLineView];
	}
	return _passwordLineView;
}

- (void)dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    
    if (textField == self.usernameTextField) {
        
        [MobClick event:@"login_idTextField_click"];
    }else {
        [MobClick event:@"login_passwordTextField_click"];
    }
    
    self.connectWarningLabel.hidden = YES;
}

- (UIButton *)secureBtn {
	if(_secureBtn == nil) {
		_secureBtn = [UIButton buttonWithType:UIButtonTypeSystem];
        [_secureBtn setTitle:@"显示密码" forState:UIControlStateNormal];
        [_secureBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        _secureBtn.layer.masksToBounds = YES;
        _secureBtn.layer.cornerRadius = 4;
        _secureBtn.layer.borderWidth = 1;
        _secureBtn.layer.borderColor = [[UIColor blackColor]CGColor];
        _secureBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        [_secureBtn addTarget:self action:@selector(secureSwitchAction) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:_secureBtn];
	}
	return _secureBtn;
}

-(void)secureSwitchAction{
    [MobClick event:@"login_showPasswordButton_click"];
    
    if (self.passwordTextField.secureTextEntry) {
        self.secureBtn.backgroundColor = [UIColor yellowColor];
    }else {
        self.secureBtn.backgroundColor = [UIColor clearColor];
    }
    self.passwordTextField.secureTextEntry = !self.passwordTextField.secureTextEntry;
    
    NSString* text = self.passwordTextField.text;
    self.passwordTextField.text = @" ";
    self.passwordTextField.text = text;
}

@end
