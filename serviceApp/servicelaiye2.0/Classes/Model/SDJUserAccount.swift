//
//  SDJUserAccount.swift
//  geniuslaiyeOC1.0
//
//  Created by 盛东 on 15/10/9.
//  Copyright © 2015年 shengdong. All rights reserved.
//

// 这里放本地本人用户信息
// 对面的信息的话只需要用户名和头像, 因为是跟小来单聊, 所以只有一个对象,不需要单独建立一个类, 在networktools里面加一个getUserInfoWithUid就行了
// 至于发的信息模型, 看是放在这里还是放在SDK里面, 应该是放在SDK里面

import UIKit

class SDJUserAccount: NSObject {
    
    class var userLogin: Bool {
        return loadAccount() != nil;
    }
    
    // 过期时间字段
    var expire: NSTimeInterval = 0 {
        didSet {
            expiresDate = NSDate(timeIntervalSinceNow: expire)
        }
    }
    // 过期时间
    var expiresDate: NSDate?
    
    /// 融云登陆token
    var rong_token: String?
    // 当前用户的UID
    var user_id: NSInteger = 0
    // 来也订单token
    var utoken: String?
    //  这是啥?
    var virtual_genius_id: String?
    // 欢迎界面用户名
    var name: String?
    // 欢迎界面用户头像
    var avatar_large: String?
    
    var telNumber: String? // 手机号也存一下
    
    var available: Bool = true // 磁盘数据是否可以访问
    
    // 助理登录
    var rc_token: String?
    // 融云助理id
    var rcg_id: String?
    // 助理操作token
    var token: String?
    // 助理邮箱(登录名)
    var username: String?
    // 助理名称
    var real_name: String?
    // 助理手机号
    var phone : String?
    
    var genius_id: NSInteger = 0
    
    var logStatu : NSInteger = 0
    
    var serviceIdsDic : Dictionary <String, String>
    
    // 字典转模型
    init(dic: [String: AnyObject], serviceIds:[String:String], geniustoken: String, enable: Bool, log: Int, service:Int) {
        
        serviceIdsDic = serviceIds
        
        serviceIdsDic["-1"] = "不限"
        
        super.init()
        
        if !(dic["phone"]!.isKindOfClass(NSNull)) {
            genius_id = dic["genius_id"]!.integerValue
            username = (dic["username"]as! String)
            rcg_id = (dic["rcg_id"]as! String)
            rc_token = (dic["rc_token"]as! String)
            real_name = (dic["real_name"]as! String)
            phone = dic["phone"]!.stringValue
            logStatu = log;
        }
        else {
            genius_id = dic["genius_id"]!.integerValue
            username = (dic["username"]as! String)
            rcg_id = (dic["rcg_id"]as! String)
            rc_token = (dic["rc_token"]as! String)
            real_name = (dic["real_name"]as! String)
            logStatu = log
        }
        
        token = geniustoken
        
        available = enable
        
        // 设置全局的用户信息
        SDJUserAccount.userAccount = self
        
    }
    
    override func setValue(value: AnyObject?, forUndefinedKey key: String) {}
    
    /// desacription描述
    override var description: String {
        
        let properties = ["real_name", "rcg_id", "rc_token", "phone", "token"]
        
        return "\(dictionaryWithValuesForKeys(properties))"
    }
    
    // MARK: - 主动网络请求加载用户信息
    func loadUserInfo(finished: (error: NSError?) -> ()) {
    }
    
    // MARK: - 归档 & 解档 的方法
    // 保存归档文件的路径
    static private let accountPath = NSMutableString(string: NSURL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true).first!).URLByAppendingPathComponent("account.plist")!.absoluteString!)
    
    // 保存用户账号
    func savaAccount() {
        
        if SDJUserAccount.accountPath.hasPrefix("file") {
            SDJUserAccount.accountPath.deleteCharactersInRange(NSMakeRange(0, 7)) // 手动修改字符串, 这样不好, 晚点研究swift怎么一次写完
        }
        NSKeyedArchiver.archiveRootObject(self, toFile: SDJUserAccount.accountPath as String)
    }
    
    // MARK: - 加载token, 没过期的话直接用,就不用向服务器频繁请求了
    private static var userAccount: SDJUserAccount?
    class func loadAccount() -> SDJUserAccount? {
        // 判断本地账号是否存在,不存在,才需要解档
        if userAccount == nil {
            if SDJUserAccount.accountPath.hasPrefix("file") {
                SDJUserAccount.accountPath.deleteCharactersInRange(NSMakeRange(0, 7))
            }
            userAccount = NSKeyedUnarchiver.unarchiveObjectWithFile(SDJUserAccount.accountPath as String) as? SDJUserAccount
        }
        // 判断日期是否过期, 过期直接清空
        if let date = userAccount?.expiresDate where date.compare(NSDate()) == NSComparisonResult.OrderedAscending {
            userAccount = nil
        }
        return userAccount
    }
    
    // NSCoding
    // 归档 -> 保存,将自定义对象转换成二进制数据保存到磁盘
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(serviceIdsDic, forKey: "serviceIds")
        aCoder.encodeDouble(expire, forKey: "expire")
        aCoder.encodeObject(expiresDate, forKey: "expiresDate")
        aCoder.encodeObject(rong_token, forKey: "rong_token")
        aCoder.encodeInteger(user_id, forKey: "user_id")
        aCoder.encodeObject(utoken, forKey: "utoken")
        aCoder.encodeObject(virtual_genius_id, forKey: "virtual_genius_id")
        aCoder.encodeObject(name, forKey: "name")
        aCoder.encodeObject(avatar_large, forKey: "avatar_large")
        aCoder.encodeObject(telNumber, forKey: "telNumber")
        aCoder.encodeBool(available, forKey: "available")
        aCoder.encodeInteger(genius_id, forKey: "genius_id")
        aCoder.encodeObject(rc_token, forKey: "rc_token")
        aCoder.encodeObject(phone, forKey: "phone")
        aCoder.encodeObject(rcg_id, forKey: "rcg_id")
        aCoder.encodeObject(username, forKey: "username")
        aCoder.encodeObject(real_name, forKey: "real_name")
        aCoder.encodeObject(token, forKey: "token")
        aCoder.encodeInteger(logStatu, forKey: "logStatu")
    }
    
    // 解档 -> 恢复,将二进制数据从磁盘恢复成自定义对象
    required init?(coder aDecoder: NSCoder) {
        serviceIdsDic = (aDecoder.decodeObjectForKey("serviceIds")as?Dictionary)!
        expire = aDecoder.decodeDoubleForKey("expire")
        expiresDate = aDecoder.decodeObjectForKey("expiresDate") as? NSDate
        rong_token = aDecoder.decodeObjectForKey("rong_token") as? String
        user_id = aDecoder.decodeIntegerForKey("user_id")
        utoken = aDecoder.decodeObjectForKey("utoken") as? String
        virtual_genius_id = aDecoder.decodeObjectForKey("virtual_genius_id") as? String
        name = aDecoder.decodeObjectForKey("name") as? String
        avatar_large = aDecoder.decodeObjectForKey("avatar_large") as? String
        telNumber = aDecoder.decodeObjectForKey("telNumber") as? String
        available = aDecoder.decodeBoolForKey("available")
        genius_id = aDecoder.decodeIntegerForKey("genius_id")
        rc_token = aDecoder.decodeObjectForKey("rc_token") as? String
        phone = aDecoder.decodeObjectForKey("phone") as? String
        rcg_id = aDecoder.decodeObjectForKey("rcg_id") as? String
        username = aDecoder.decodeObjectForKey("username") as? String
        real_name = aDecoder.decodeObjectForKey("real_name") as? String
        token = aDecoder.decodeObjectForKey("token") as? String
        logStatu = aDecoder.decodeIntegerForKey("logStatu")
    }
}
