//
//  SDJUserInfoDataSource.h
//  geniuslaiyeOC1.0
//
//  Created by 盛东 on 15/10/13.
//  Copyright © 2015年 shengdong. All rights reserved.
//

#import <Foundation/Foundation.h>

// 这个类暂时仅用于远程推送名字显示. 用户名需要有查询接口, 或者暂时不做.

@interface SDJUserInfoDataSource : NSObject <RCIMUserInfoDataSource>

+ (instancetype)sharedUserInfoDataSource;

@end
