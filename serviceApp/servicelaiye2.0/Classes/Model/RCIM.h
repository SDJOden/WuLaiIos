//
//  RongUIKit.h
//  RongIMKit
//
//  Created by xugang on 15/1/13.
//  Copyright (c) 2015年 RongCloud. All rights reserved.
//

#ifndef __RongUIKit
#define __RongUIKit
#import <Foundation/Foundation.h>
#import <RongIMLib/RongIMLib.h>
#import "SDJCommonSetting.h"

FOUNDATION_EXPORT NSString *const RCKitDispatchMessageNotification;

FOUNDATION_EXPORT NSString *const RCKitDispatchConnectionStatusChangedNotification;

// 获取用户信息
@protocol RCIMUserInfoDataSource <NSObject>

- (void)getUserInfoWithUserId:(NSString *)userId completion:(void (^)(RCUserInfo *userInfo))completion;

@end

//  连接状态监听器，以获取连接相关状态。网络状态变化。
@protocol RCIMConnectionStatusDelegate <NSObject>

- (void)onRCIMConnectionStatusChanged:(RCConnectionStatus)status;

@end

// 融云UIKit核心单例
@interface RCIM : NSObject

@property(nonatomic) CGSize globalMessagePortraitSize;

@property(nonatomic) CGSize globalConversationPortraitSize;

@property(nonatomic) CGFloat portraitImageViewCornerRadius; // 头像圆角度数

@property(nonatomic) UIImage *geniusIcon;

@property(nonatomic) UIImage *userIcon;

@property(nonatomic, strong) NSMutableArray *inteligentMessages;

@property(nonatomic) RCUserAvatarStyle globalMessageAvatarStyle; // 默认RC_USER_AVATAR_RECTANGLE

@property(nonatomic, strong) RCUserInfo *currentUserInfo; // 自己的用户信息，开发者自己组装用户信息设置

@property(nonatomic, weak) id<RCIMUserInfoDataSource> userInfoDataSource; // 用户信息提供者

@property(nonatomic, weak) id<RCIMConnectionStatusDelegate> connectionStatusDelegate; // 连接状态监听器

@property(nonatomic, assign) BOOL disableMessageNotificaiton; // 消息免通知，默认是NO

@property(nonatomic, assign) BOOL disableMessageAlertSound; // 关闭新消息提示音，默认值是NO，新消息有提示音.

@property (nonatomic, strong) NSArray *geniusLabelArray;

+ (instancetype)sharedRCIM;

// 初始化SDK
- (void)initWithAppKey:(NSString *)appKey;

// 建立连接，注意该方法回调在非调用线程，如果需要UI操作，请切换主线程，如果使用IMKit，使用此方法，不再使用RongIMLib的同名方法。
- (void)connectWithToken:(NSString *)token
                 success:(void (^)(NSString *userId))successBlock
                   error:(void (^)(RCConnectErrorCode status))errorBlock
          tokenIncorrect:(void (^)())tokenIncorrectBlock;

// 断开连接, 不接收通知推送
- (void)logout;

- (void)setGeniusIcon:(UIImage *)geniusIcon;

- (void)setUserIcon:(UIImage *)userIcon;

@end

#endif