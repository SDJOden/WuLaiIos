//
//  RongUIKit.m
//  RongIMKit
//
//  Created by xugang on 15/1/13.
//  Copyright (c) 2015年 RongCloud. All rights reserved.
//

#import "RCIM.h"
#import "RCMessageModel.h"
#import "RCLocalNotification.h"
#import "RCSystemSoundPlayer.h"
#import "WYSeparateModel.h"

NSString *const RCKitDispatchMessageNotification = @"RCKitDispatchMessageNotification";
NSString *const RCKitDispatchConnectionStatusChangedNotification = @"RCKitDispatchConnectionStatusChangedNotification";

@interface RCIM () <RCIMClientReceiveMessageDelegate, RCConnectionStatusChangeDelegate>
@end

static RCIM *__rongUIKit = nil;

dispatch_queue_t __rc__conversationList_refresh_queue = NULL;

@implementation RCIM

+ (instancetype)sharedRCIM {
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if (__rongUIKit == nil) {
            __rongUIKit = [[RCIM alloc] init];
            __rongUIKit.userInfoDataSource = nil;
            __rongUIKit.disableMessageNotificaiton = NO;
            __rongUIKit.disableMessageAlertSound = [[NSUserDefaults standardUserDefaults] boolForKey:@"rcMessageBeep"]; ;
            __rongUIKit.globalMessagePortraitSize = CGSizeMake(46, 46);
            __rongUIKit.globalConversationPortraitSize = CGSizeMake(46, 46);
            __rongUIKit.globalMessageAvatarStyle = RC_USER_AVATAR_RECTANGLE;
                      __rc__conversationList_refresh_queue = dispatch_queue_create("com.rongcloud.refreshConversationList", NULL);
            __rongUIKit.portraitImageViewCornerRadius = 4;
            __rongUIKit.geniusLabelArray = [[NSArray alloc]init];
        }
    });
    return __rongUIKit;
}

- (void)setDisableMessageAlertSound:(BOOL)disableMessageAlertSound
{
    [[NSUserDefaults standardUserDefaults] setBool:disableMessageAlertSound forKey:@"rcMessageBeep"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    _disableMessageAlertSound = disableMessageAlertSound;
}

- (void)setGlobalMessagePortraitSize:(CGSize)globalMessagePortraitSize {

    _globalMessagePortraitSize.width = globalMessagePortraitSize.width;
    _globalMessagePortraitSize.height = globalMessagePortraitSize.height;
}

#pragma mark - userIcon
- (void)setGeniusIcon:(UIImage *)geniusIcon {
    
    _geniusIcon = geniusIcon;
}

- (void)setUserIcon:(UIImage *)userIcon {
    
    _userIcon = userIcon;
}

- (void)setCurrentUserInfo:(RCUserInfo *)currentUserInfo {
    
    [[RCIMClient sharedRCIMClient] setCurrentUserInfo:currentUserInfo];
}

- (RCUserInfo *)currentUserInfo {
    
    return [[RCIMClient sharedRCIMClient] currentUserInfo];
}

- (void)initWithAppKey:(NSString *)appKey {

    [[RCIMClient sharedRCIMClient] initWithAppKey:appKey];
    [[RCIMClient sharedRCIMClient] setReceiveMessageDelegate:self object:nil];
    [[RCIMClient sharedRCIMClient] setRCConnectionStatusChangeDelegate:self];
}



- (void)connectWithToken:(NSString *)token
                 success:(void (^)(NSString *userId))successBlock
                   error:(void (^)(RCConnectErrorCode status))errorBlock
          tokenIncorrect:(void (^)())tokenIncorrectBlock {
    
    [[RCIMClient sharedRCIMClient] connectWithToken:token success:^(NSString *userId) {
        if (successBlock!=nil) {
            successBlock(userId);
        }
    } error:^(RCConnectErrorCode status) {
        if(errorBlock!=nil)
            errorBlock(status);
    } tokenIncorrect:^() {
        tokenIncorrectBlock();
    }];
}

// Log out。不会接收到push消息。
- (void)logout {
    
    [[RCIMClient sharedRCIMClient] logout];
}

- (void)setUserInfoDataSource:(id<RCIMUserInfoDataSource>)userInfoDataSource {
    
    _userInfoDataSource = userInfoDataSource;
}

//  信息接口
- (void)onReceived:(RCMessage *)message left:(int)nLeft object:(id)object {
    /**
     收到的消息extra为NSString
     */
    RCMessageModel *msgModel = [[RCMessageModel alloc] initWithMessage:message];
    
#warning to do
    //-----------------------------以下, 耦合, 这里应该只负责消息的转发形式, 具体的解析判断不要放在这里.---------------------------------------------
    if ([message.content isKindOfClass:[RCTextMessage class]]) {
        RCTextMessage *textMsg = (RCTextMessage *)message.content;
        NSDictionary *msgJSONDic = [WYTool jsonStringToDictionary:textMsg.extra];
//        if (((NSNumber *)msgJSONDic[@"msg_detail"][@"action"]).integerValue != 2) {
//            NSLog(@"%@, %@", textMsg.content, msgJSONDic);
//        }
        
        //文本消息
        if (((NSNumber *)msgJSONDic[@"msg_type"]).intValue == 1) { // 加中间参数, ((NSNumber *)msgJSONDic[@"msg_type"])影响可读性
            if (((NSNumber *)msgJSONDic[@"direction"]).intValue == 1) {
                if (((NSNumber *)msgJSONDic[@"genius_info"][@"genius_id"]).integerValue == 1896209 || ((NSNumber *)msgJSONDic[@"genius_info"][@"genius_id"]).integerValue == 1896208 || ((NSNumber *)msgJSONDic[@"genius_info"][@"genius_id"]).integerValue == 1896207 ||((NSNumber *)msgJSONDic[@"genius_info"][@"genius_id"]).integerValue == 1896206) {
                    msgModel.messageDirection = 1;
                    message.messageDirection = 1;
                }
                //用户消息
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[NSNotificationCenter defaultCenter]postNotificationName:@"addCountNum" object:nil userInfo:@{@"inner_uid":[msgJSONDic[@"user_gid"]substringFromIndex:9], @"ts":msgJSONDic[@"msg_ts"],@"content":msgJSONDic[@"msg_detail"][@"content"], @"messageModel":msgModel}];
                });
            }
            else {
                int serviceId = 0;
                for (NSDictionary *sugDomainDict in msgJSONDic[@"msg_detail"][@"sug_domain"]) {
                    if (((NSNumber *)sugDomainDict[@"service_id"]).integerValue < 10005) {
                        serviceId = ((NSNumber *)sugDomainDict[@"service_id"]).intValue;
                        break;
                    }
                }
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[NSNotificationCenter defaultCenter]postNotificationName:@"addCountNum" object:nil userInfo:@{@"inner_uid":msgJSONDic[@"inner_uid"], @"ts":msgJSONDic[@"msg_ts"],@"content":msgJSONDic[@"msg_detail"][@"content"], @"messageModel":msgModel,@"serviceId":@(serviceId)}];
                });
            }
            //声音
            if (message.messageDirection == MessageDirection_RECEIVE) {
                [[RCSystemSoundPlayer defaultPlayer] playSoundByMessage:message];
            }
        }
        //通知消息，待接入变化
        else if (((NSNumber *)msgJSONDic[@"msg_type"]).intValue == 3) {
            if (((NSNumber *)msgJSONDic[@"msg_detail"][@"action"]).intValue == 2) {
                //通知待接入计数变化
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[NSNotificationCenter defaultCenter]postNotificationName:@"addWaitingUser" object:nil userInfo:@{@"content":msgJSONDic[@"msg_detail"][@"content"]}];
                NSLog(@"%@, %@", textMsg.content, msgJSONDic);
                });
                return;
            }
#warning 修改对话样式
            else if (((NSNumber *)msgJSONDic[@"msg_detail"][@"action"]).intValue == 3) {
                WYSeparateModel *sepa = [WYSeparateModel modelWithDict:@{@"textContent":msgJSONDic[@"msg_detail"][@"content"]}];
                RCMessageModel *msgModel = [[RCMessageModel alloc]init];
                msgModel.content = sepa;
                msgModel.messageDirection = 1;
                message.messageDirection = 1;
                //删除列表中用户
                if ([msgJSONDic[@"msg_detail"][@"effect"]isEqualToString:@"disable"]) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [[NSNotificationCenter defaultCenter]postNotificationName:@"deleteConnectUser" object:nil userInfo:@{@"inner_uid":msgJSONDic[@"inner_uid"],@"messageModel":msgModel}];
                    });
                }
                else {
                    int serviceId = 0;
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [[NSNotificationCenter defaultCenter]postNotificationName:@"addCountNum" object:nil userInfo:@{@"inner_uid":msgJSONDic[@"inner_uid"], @"ts":msgJSONDic[@"msg_ts"],@"content":msgJSONDic[@"msg_detail"][@"content"], @"messageModel":msgModel,@"serviceId":@(serviceId)}];
                    });
//                    [[NSNotificationCenter defaultCenter]postNotificationName:@"addConnectUser" object:nil userInfo:@{@"inner_uid":msgJSONDic[@"inner_uid"],@"messageModel":msgModel}];
                }
            }
#warning 修改对话样式
            else if (((NSNumber *)msgJSONDic[@"msg_detail"][@"action"]).intValue == 4) {
                WYSeparateModel *sepa = [WYSeparateModel modelWithDict:@{@"textContent":msgJSONDic[@"msg_detail"][@"content"]}];
                RCMessageModel *msgModel = [[RCMessageModel alloc]init];
                msgModel.content = sepa;
                msgModel.messageDirection = 1;
                message.messageDirection = 1;
                int serviceId = 0;
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[NSNotificationCenter defaultCenter]postNotificationName:@"addCountNum" object:nil userInfo:@{@"inner_uid":msgJSONDic[@"inner_uid"], @"ts":msgJSONDic[@"msg_ts"],@"content":msgJSONDic[@"msg_detail"][@"content"], @"messageModel":msgModel,@"serviceId":@(serviceId)}];
                });
                //声音
                if (message.messageDirection == MessageDirection_RECEIVE) {
                    [[RCSystemSoundPlayer defaultPlayer] playSoundByMessage:message];
                }
            }
            else if (((NSNumber *)msgJSONDic[@"msg_detail"][@"action"]).intValue == 6 || ((NSNumber *)msgJSONDic[@"msg_detail"][@"action"]).intValue == 7) {
                return;
            }
            else if (((NSNumber *)msgJSONDic[@"msg_detail"][@"action"]).intValue == 8) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[NSNotificationCenter defaultCenter]postNotificationName:@"addNoticeView" object:nil userInfo:@{@"content":msgJSONDic[@"msg_detail"][@"content"]}];
                });
                return;
            }
        }
    }
    
    if ([message.content isKindOfClass:[RCImageMessage class]]) {
        RCImageMessage *imageMsg = (RCImageMessage *)message.content;
        NSDictionary *msgJSONDic = [WYTool jsonStringToDictionary:imageMsg.extra];
            //主线程发通知
            dispatch_async(dispatch_get_main_queue(), ^{
                //通知计数加一
                [[NSNotificationCenter defaultCenter]postNotificationName:@"addCountNum" object:nil userInfo:@{@"inner_uid":msgJSONDic[@"inner_uid"],@"messageModel":msgModel}];
            });
    }
    
    if ([message.content isKindOfClass:[RCVoiceMessage class]]) {
        RCVoiceMessage *imageMsg = (RCVoiceMessage *)message.content;
        NSDictionary *msgJSONDic = [WYTool jsonStringToDictionary:imageMsg.extra];
        //主线程发通知
        dispatch_async(dispatch_get_main_queue(), ^{
            //通知计数加一
            [[NSNotificationCenter defaultCenter]postNotificationName:@"addCountNum" object:nil userInfo:@{@"inner_uid":msgJSONDic[@"inner_uid"],@"messageModel":msgModel}];
            
        });
    }
    //---------------------------------------以上-------------------------------------------------
    
    //改写, 为什么要改, 是在这里处理.
    if (message.conversationType == 1 || [message.targetId isEqualToString:@"rcg0"]) {
        message.conversationType = 3;
        RCTextMessage *textMsg = (RCTextMessage *)message.content;
        NSDictionary *msgJSONDic = [WYTool jsonStringToDictionary:textMsg.extra];
        message.targetId = [NSString stringWithFormat:@"rc_group_%@", msgJSONDic[@"inner_uid"]];
    }
    
    // 下面是收到消息的转发处理.
    NSDictionary *dic_left = @{@"left" : @(nLeft)};
    // 本地app收到消息的通知
    dispatch_async(dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] postNotificationName:RCKitDispatchMessageNotification object:message userInfo:dic_left];
    });
    
    //发出去的消息，不需要本地铃声和通知
    if (message.messageDirection == MessageDirection_SEND) {
        return;
    }
    BOOL isCustomMessageAlert = YES;

    if (!([[message.content class] persistentFlag] & MessagePersistent_ISPERSISTED)) {
        isCustomMessageAlert = NO;
    }
    if (0 == nLeft && [RCIMClient sharedRCIMClient].sdkRunningMode == RCSDKRunningMode_Foregroud && !self.disableMessageAlertSound && isCustomMessageAlert) {
             
        [[RCIMClient sharedRCIMClient] getConversationNotificationStatus:message.conversationType targetId:message.targetId success:^(RCConversationNotificationStatus nStatus) {
            if (NOTIFY == nStatus) {
                // 播放提示音
//                [[RCSystemSoundPlayer defaultPlayer] playSoundByMessage:message];
            }
        }error:^(RCErrorCode status){}];
    }
    
    if (0 == nLeft && NO == self.disableMessageNotificaiton &&[RCIMClient sharedRCIMClient].sdkRunningMode == RCSDKRunningMode_Backgroud && isCustomMessageAlert) {
        
        //聊天室消息不做本地通知
        if (ConversationType_CHATROOM == message.conversationType)
            return;
        NSDictionary *dictionary = [RCKitUtility getNotificationUserInfoDictionary:message];
        [[RCIMClient sharedRCIMClient] getConversationNotificationStatus:message.conversationType targetId:message.targetId success:^(RCConversationNotificationStatus nStatus) {
            if (NOTIFY == nStatus) {
                NSString *showMessage = [RCKitUtility formatMessage:message.content];
                // 本地收到消息后的通知
                if (self.userInfoDataSource && [self.userInfoDataSource respondsToSelector:@selector(getUserInfoWithUserId:completion:)]) {
                    [self.userInfoDataSource getUserInfoWithUserId:message.targetId completion:^(RCUserInfo *userInfo) {
                        if (nil == userInfo) {
                            return;
                        }
                        [[RCLocalNotification defaultCenter]postLocalNotification:[NSString stringWithFormat:@"%@:%@",userInfo.name,showMessage] userInfo:dictionary];
                    }];
                }
            }
        }error:^(RCErrorCode status){}];
    }
}

+ (NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString {
    if (jsonString == nil) {
        return nil;
    }
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSError *err;
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData
                                                        options:NSJSONReadingMutableContainers
                                                          error:&err];
    if(err) {
        NSLog(@"json解析失败：%@",err);
        return nil;
    }
    return dic;
}

// 网络状态变化
- (void)onConnectionStatusChanged:(RCConnectionStatus)status {
    if (ConnectionStatus_UNKNOWN != status && ConnectionStatus_Unconnected != status) {
        [[NSNotificationCenter defaultCenter] postNotificationName:RCKitDispatchConnectionStatusChangedNotification object:[NSNumber numberWithInt:status]];
    } else {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self performSelector:@selector(delayNotifyUnConnectedStatus) withObject:nil afterDelay:3];
        });
    }
    if (self.connectionStatusDelegate) {
        [self.connectionStatusDelegate onRCIMConnectionStatusChanged:status];
    }
}

- (void)delayNotifyUnConnectedStatus {
    RCConnectionStatus status = [[RCIMClient sharedRCIMClient] getConnectionStatus];
    if (ConnectionStatus_NETWORK_UNAVAILABLE == status || ConnectionStatus_UNKNOWN == status || ConnectionStatus_Unconnected == status) {
        [[NSNotificationCenter defaultCenter] postNotificationName:RCKitDispatchConnectionStatusChangedNotification object:[NSNumber numberWithInt:status]];
    }
}

- (void)dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationDidEnterBackgroundNotification object:nil];
}

@end
