//
//  SDJUserInfoDataSource.m
//  geniuslaiyeOC1.0
//
//  Created by 盛东 on 15/10/13.
//  Copyright © 2015年 shengdong. All rights reserved.
//

#import "SDJUserInfoDataSource.h"

@implementation SDJUserInfoDataSource

//单例
+ (instancetype)sharedUserInfoDataSource {
    static SDJUserInfoDataSource* instance = nil;
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        instance = [[[self class] alloc] init];
    });
    return instance;
}

/**
 *此方法中要提供给融云用户的信息，建议缓存到本地，然后改方法每次从缓存返回, 用FMDB存? 就只有一个人, 暂时没必要
 */
- (void)getUserInfoWithUserId:(NSString *)userId completion:(void(^)(RCUserInfo* userInfo))completion{
    
    //此处为了演示写了一个用户信息
    if ([[NSString stringWithFormat:@"rcu%ld",[SDJUserAccount loadAccount].user_id] isEqualToString:userId]) {
        // 用户信息从自己服务器拿, 不从这里拿
    } else if ([[SDJUserAccount loadAccount].virtual_genius_id isEqual:userId]) {
        RCUserInfo *user = [[RCUserInfo alloc] init];
        user.userId = userId;
        user.name = @"天才助理";
        user.portraitUri = @"http://s1.laiye.com/static/app/android/v1/laiye_genius_default_avatar.png";
        return completion(user);
    } else { // 为了防止客服小来号码变动
        RCUserInfo *user = [[RCUserInfo alloc] init];
        user.userId = userId;
        user.name = @"天才助理";
        user.portraitUri = @"http://s1.laiye.com/static/app/android/v1/laiye_genius_default_avatar.png";
        return completion(user);
    }
}

@end
