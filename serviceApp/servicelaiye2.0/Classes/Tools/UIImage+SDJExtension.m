//
//  UIImage+SDJExtension.m
//  geniuslaiyeOC1.0
//
//  Created by 盛东 on 15/10/13.
//  Copyright © 2015年 shengdong. All rights reserved.
//

#import "UIImage+SDJExtension.h"
#import <Accelerate/Accelerate.h>

@implementation UIImage (SDJExtension)

- (UIImage *)cutCircleImagewithWidth:(CGFloat)width withColor:(UIColor *)color
{
    
    //1.获取上下文
    UIImage *img = self;
    CGFloat margin = width;
    CGSize newSize = CGSizeMake(img.size.width + 2 * margin, img.size.height + 2 * margin);
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    //2.画外环
    CGPoint centerP = CGPointMake(newSize.width*0.5, newSize.height*0.5);
    CGFloat radius = MIN(img.size.width, img.size.height)*0.5;
    UIBezierPath *path = [UIBezierPath bezierPathWithArcCenter:centerP radius:radius startAngle:0 endAngle:M_PI*2 clockwise:YES];
    CGContextSetLineWidth(ctx, margin);
    [color set];
    CGContextAddPath(ctx, path.CGPath);
    CGContextDrawPath(ctx, kCGPathStroke);
    //3.画图片
    UIBezierPath *path2 = [UIBezierPath bezierPathWithArcCenter:centerP radius:radius startAngle:0 endAngle:M_PI*2 clockwise:YES];
    CGContextAddPath(ctx, path2.CGPath);
    [img drawAtPoint:CGPointMake(margin, margin)];
    CGContextClip(ctx);
    
    //4.保存
    UIImage *imgCliped = UIGraphicsGetImageFromCurrentImageContext();
    // 想要获得一个完整的圆,在这里得把图片切割一遍,切割矩形
    
    CGFloat scale = [UIScreen mainScreen].scale;
    CGFloat X = 0;
    CGFloat H = imgCliped.size.width*scale;
    CGFloat Y = (imgCliped.size.height*scale-H)*0.5;
    CGFloat W = H;
    CGRect ret = CGRectMake(X, Y, W, H);
    CGImageRef imgRf = CGImageCreateWithImageInRect(imgCliped.CGImage, ret);
    UIImage *img2 = [UIImage imageWithCGImage:imgRf];
    
    CGImageRelease(imgRf);
    UIGraphicsEndImageContext();
    
    return img2;
}

+ (UIImage *)getImageWithSize:(CGSize)size backgroundColor:(UIColor *)color {
    
    CGRect rect = (CGRect){0,0,size};
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context =UIGraphicsGetCurrentContext();
    CGContextClearRect(context, rect);
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *myImage =UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return myImage;
}

- (UIImage *)drawImagewithWidth:(CGFloat)width withColor:(UIColor *)color withDirection:(BOOL)isRight
{
    //1.获取上下文
    UIImage *img = self;
    CGFloat margin = width;
    CGSize newSize = CGSizeMake(img.size.width, img.size.height);
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    //2.画外环
    CGFloat imageWidth = img.size.width;
    CGFloat imageHeight = img.size.height;
    CGFloat radius = 5;
    CGFloat tailRaduis = 3; // 小尾巴的半边长
    CGFloat magicNumber = 23;
    UIBezierPath *path = [UIBezierPath bezierPath];
    if (isRight) {
        [path moveToPoint:CGPointMake(imageWidth - margin, magicNumber)];
        [path addLineToPoint:CGPointMake(imageWidth - tailRaduis * 1.7 - margin, magicNumber + tailRaduis)]; // 1.7代表根号三
        [path addLineToPoint:CGPointMake(imageWidth - tailRaduis * 1.7 - margin, imageHeight - margin - radius)];
        [path addArcWithCenter:CGPointMake(imageWidth - tailRaduis * 1.7 - radius - margin, imageHeight - margin - radius) radius:radius startAngle:0 endAngle:M_PI_2 clockwise:YES];
        [path addLineToPoint:CGPointMake(margin + radius, imageHeight - margin)];
        [path addArcWithCenter:CGPointMake(margin + radius, imageHeight - margin - radius) radius:radius startAngle:M_PI_2 endAngle:M_PI clockwise:YES];
        [path addLineToPoint:CGPointMake(margin, margin + radius)];
        [path addArcWithCenter:CGPointMake(margin + radius, margin + radius) radius:radius startAngle:M_PI endAngle:M_PI_2 * 3 clockwise:YES];
        [path addLineToPoint:CGPointMake(imageWidth - margin - radius - tailRaduis * 1.7, margin)];
        [path addArcWithCenter:CGPointMake(imageWidth - margin - radius - tailRaduis * 1.7, margin + radius) radius:radius startAngle:M_PI_2 * 3 endAngle:M_PI * 2 clockwise:YES];
        [path addLineToPoint:CGPointMake(imageWidth - margin - tailRaduis * 1.7, magicNumber - tailRaduis)];
        
    } else {
        [path moveToPoint:CGPointMake(margin, magicNumber)];
        [path addLineToPoint:CGPointMake(tailRaduis * 1.7 + margin, magicNumber - tailRaduis)]; // 1.7代表根号三
        [path addLineToPoint:CGPointMake(tailRaduis * 1.7 + margin, margin + radius)];
        [path addArcWithCenter:CGPointMake(tailRaduis * 1.7 + radius + margin, margin + radius) radius:radius startAngle:M_PI endAngle:M_PI_2 * 3 clockwise:YES];
        [path addLineToPoint:CGPointMake(imageWidth - margin - radius, margin)];
        [path addArcWithCenter:CGPointMake(imageWidth - margin - radius, margin + radius) radius:radius startAngle:M_PI_2 * 3 endAngle:0 clockwise:YES];
        [path addLineToPoint:CGPointMake(imageWidth - margin, imageHeight - margin - radius)];
        [path addArcWithCenter:CGPointMake(imageWidth - margin - radius, imageHeight - margin - radius) radius:radius startAngle:0 endAngle:M_PI_2 clockwise:YES];
        [path addLineToPoint:CGPointMake(margin + radius + tailRaduis * 1.7, imageHeight - margin)];
        [path addArcWithCenter:CGPointMake(margin + radius + tailRaduis * 1.7, imageHeight - margin - radius) radius:radius startAngle:M_PI_2 endAngle:M_PI clockwise:YES];
        [path addLineToPoint:CGPointMake(margin + tailRaduis * 1.7, magicNumber + tailRaduis)];
    }
    [path closePath];
    CGContextSetLineWidth(ctx, margin);
    [color set];
    CGContextAddPath(ctx, path.CGPath);
    CGContextDrawPath(ctx, kCGPathStroke);
    
    //3.画图片
    UIBezierPath *path2 = path;
    [path2 closePath];
    CGContextAddPath(ctx, path2.CGPath);
    CGContextClip(ctx);
    [img drawAtPoint:CGPointMake(margin, margin)];
    
    //4.保存
    UIImage *imgCliped = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return imgCliped;
}

// 画圆角矩形背景图
+ (UIImage *)getRoundImageWithSize:(CGSize)size backgroundColor:(UIColor *)color lineWidth:(CGFloat)lineWidth cornerRadius:(CGFloat)radius shouldFill:(BOOL)shouldFill{
    //1.获取上下文
    CGSize newSize = CGSizeMake(size.width + 2 * lineWidth, size.height + 2 * lineWidth);
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    //2.画外框
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(lineWidth, lineWidth, size.width, size.height) cornerRadius:radius];
    [color set];
    path.lineWidth = lineWidth;
    CGContextAddPath(ctx, path.CGPath);
    if (shouldFill == YES) {
        CGContextDrawPath(ctx, kCGPathFill);
    } else {
        CGContextDrawPath(ctx, kCGPathStroke);
    }
    //4.保存
    UIImage *imgCliped = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return imgCliped;
}

+ (UIImage *)drawBubbleBackgroundImagewithSize:(CGSize)size withWidth:(CGFloat)width withColor:(UIColor *)color withDirection:(BOOL)isRight {
    
    //1.获取上下文
    CGFloat margin = width;
    CGSize newSize = size;
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    //2.画外环
    CGFloat imageWidth = size.width;
    CGFloat imageHeight = size.height;
    CGFloat radius = 5;
    CGFloat tailRaduis = 3; // 小尾巴的半边长
    CGFloat magicNumber = 23;
    UIBezierPath *path = [UIBezierPath bezierPath];
    if (isRight) {
        [path moveToPoint:CGPointMake(imageWidth - margin, magicNumber)];
        [path addLineToPoint:CGPointMake(imageWidth - tailRaduis * 1.7 - margin, magicNumber + tailRaduis)]; // 1.7代表根号三
        [path addLineToPoint:CGPointMake(imageWidth - tailRaduis * 1.7 - margin, imageHeight - margin - radius)];
        [path addArcWithCenter:CGPointMake(imageWidth - tailRaduis * 1.7 - radius - margin, imageHeight - margin - radius) radius:radius startAngle:0 endAngle:M_PI_2 clockwise:YES];
        [path addLineToPoint:CGPointMake(margin + radius, imageHeight - margin)];
        [path addArcWithCenter:CGPointMake(margin + radius, imageHeight - margin - radius) radius:radius startAngle:M_PI_2 endAngle:M_PI clockwise:YES];
        [path addLineToPoint:CGPointMake(margin, margin + radius)];
        [path addArcWithCenter:CGPointMake(margin + radius, margin + radius) radius:radius startAngle:M_PI endAngle:M_PI_2 * 3 clockwise:YES];
        [path addLineToPoint:CGPointMake(imageWidth - margin - radius - tailRaduis * 1.7, margin)];
        [path addArcWithCenter:CGPointMake(imageWidth - margin - radius - tailRaduis * 1.7, margin + radius) radius:radius startAngle:M_PI_2 * 3 endAngle:M_PI * 2 clockwise:YES];
        [path addLineToPoint:CGPointMake(imageWidth - margin - tailRaduis * 1.7, magicNumber - tailRaduis)];
        
    } else {
        [path moveToPoint:CGPointMake(margin, magicNumber)];
        [path addLineToPoint:CGPointMake(tailRaduis * 1.7 + margin, magicNumber - tailRaduis)]; // 1.7代表根号三
        [path addLineToPoint:CGPointMake(tailRaduis * 1.7 + margin, margin + radius)];
        [path addArcWithCenter:CGPointMake(tailRaduis * 1.7 + radius + margin, margin + radius) radius:radius startAngle:M_PI endAngle:M_PI_2 * 3 clockwise:YES];
        [path addLineToPoint:CGPointMake(imageWidth - margin - radius, margin)];
        [path addArcWithCenter:CGPointMake(imageWidth - margin - radius, margin + radius) radius:radius startAngle:M_PI_2 * 3 endAngle:0 clockwise:YES];
        [path addLineToPoint:CGPointMake(imageWidth - margin, imageHeight - margin - radius)];
        [path addArcWithCenter:CGPointMake(imageWidth - margin - radius, imageHeight - margin - radius) radius:radius startAngle:0 endAngle:M_PI_2 clockwise:YES];
        [path addLineToPoint:CGPointMake(margin + radius + tailRaduis * 1.7, imageHeight - margin)];
        [path addArcWithCenter:CGPointMake(margin + radius + tailRaduis * 1.7, imageHeight - margin - radius) radius:radius startAngle:M_PI_2 endAngle:M_PI clockwise:YES];
        [path addLineToPoint:CGPointMake(margin + tailRaduis * 1.7, magicNumber + tailRaduis)];
    }
    [path closePath];
    CGContextSetLineWidth(ctx, margin);
    [color set];
    CGContextAddPath(ctx, path.CGPath);
    CGContextDrawPath(ctx, kCGPathFill);

    UIImage *imgCliped = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return imgCliped;
}

/*
 1.白色,参数:
 透明度 0~1,  0为白,   1为深灰色
 半径:默认30,推荐值 3   半径值越大越模糊 ,值越小越清楚
 色彩饱和度(浓度)因子:  0是黑白灰, 9是浓彩色, 1是原色  默认1.8
 “彩度”，英文是称Saturation，即饱和度。将无彩色的黑白灰定为0，最鲜艳定为9s，这样大致分成十阶段，让数值和人的感官直觉一致。
 */
- (UIImage *)imgWithLightAlpha:(CGFloat)alpha radius:(CGFloat)radius colorSaturationFactor:(CGFloat)colorSaturationFactor
{
    UIColor *tintColor = [UIColor colorWithWhite:1.0 alpha:alpha];
    return [self imgBluredWithRadius:radius tintColor:tintColor saturationDeltaFactor:colorSaturationFactor maskImage:nil];
}
// 2.封装好,供外界调用的
- (UIImage *)imgWithBlur
{
    // 调用方法1
    return [self imgWithLightAlpha:0.1 radius:2 colorSaturationFactor:1];
}

// 内部方法,核心代码,封装了毛玻璃效果 参数:半径,颜色,色彩饱和度
- (UIImage *)imgBluredWithRadius:(CGFloat)blurRadius tintColor:(UIColor *)tintColor saturationDeltaFactor:(CGFloat)saturationDeltaFactor maskImage:(UIImage *)maskImage
{
    CGRect imageRect = { CGPointZero, self.size };
    UIImage *effectImage = self;
    
    BOOL hasBlur = blurRadius > __FLT_EPSILON__;
    BOOL hasSaturationChange = fabs(saturationDeltaFactor - 1.) > __FLT_EPSILON__;
    if (hasBlur || hasSaturationChange) {
        UIGraphicsBeginImageContextWithOptions(self.size, NO, [[UIScreen mainScreen] scale]);
        CGContextRef effectInContext = UIGraphicsGetCurrentContext();
        CGContextScaleCTM(effectInContext, 1.0, -1.0);
        CGContextTranslateCTM(effectInContext, 0, -self.size.height);
        CGContextDrawImage(effectInContext, imageRect, self.CGImage);
        
        vImage_Buffer effectInBuffer;
        effectInBuffer.data     = CGBitmapContextGetData(effectInContext);
        effectInBuffer.width    = CGBitmapContextGetWidth(effectInContext);
        effectInBuffer.height   = CGBitmapContextGetHeight(effectInContext);
        effectInBuffer.rowBytes = CGBitmapContextGetBytesPerRow(effectInContext);
        
        UIGraphicsBeginImageContextWithOptions(self.size, NO, [[UIScreen mainScreen] scale]);
        CGContextRef effectOutContext = UIGraphicsGetCurrentContext();
        vImage_Buffer effectOutBuffer;
        effectOutBuffer.data     = CGBitmapContextGetData(effectOutContext);
        effectOutBuffer.width    = CGBitmapContextGetWidth(effectOutContext);
        effectOutBuffer.height   = CGBitmapContextGetHeight(effectOutContext);
        effectOutBuffer.rowBytes = CGBitmapContextGetBytesPerRow(effectOutContext);
        
        if (hasBlur) {
            CGFloat inputRadius = blurRadius * [[UIScreen mainScreen] scale];
            NSUInteger radius = floor(inputRadius * 3. * sqrt(2 * M_PI) / 4 + 0.5);
            if (radius % 2 != 1) {
                radius += 1; // force radius to be odd so that the three box-blur methodology works.
            }
            vImageBoxConvolve_ARGB8888(&effectInBuffer, &effectOutBuffer, NULL, 0, 0, radius, radius, 0, kvImageEdgeExtend);
            vImageBoxConvolve_ARGB8888(&effectOutBuffer, &effectInBuffer, NULL, 0, 0, radius, radius, 0, kvImageEdgeExtend);
            vImageBoxConvolve_ARGB8888(&effectInBuffer, &effectOutBuffer, NULL, 0, 0, radius, radius, 0, kvImageEdgeExtend);
        }
        BOOL effectImageBuffersAreSwapped = NO;
        if (hasSaturationChange) {
            CGFloat s = saturationDeltaFactor;
            CGFloat floatingPointSaturationMatrix[] = {
                0.0722 + 0.9278 * s,  0.0722 - 0.0722 * s,  0.0722 - 0.0722 * s,  0,
                0.7152 - 0.7152 * s,  0.7152 + 0.2848 * s,  0.7152 - 0.7152 * s,  0,
                0.2126 - 0.2126 * s,  0.2126 - 0.2126 * s,  0.2126 + 0.7873 * s,  0,
                0,                    0,                    0,  1,
            };
            const int32_t divisor = 256;
            NSUInteger matrixSize = sizeof(floatingPointSaturationMatrix)/sizeof(floatingPointSaturationMatrix[0]);
            int16_t saturationMatrix[matrixSize];
            for (NSUInteger i = 0; i < matrixSize; ++i) {
                saturationMatrix[i] = (int16_t)roundf(floatingPointSaturationMatrix[i] * divisor);
            }
            if (hasBlur) {
                vImageMatrixMultiply_ARGB8888(&effectOutBuffer, &effectInBuffer, saturationMatrix, divisor, NULL, NULL, kvImageNoFlags);
                effectImageBuffersAreSwapped = YES;
            }
            else {
                vImageMatrixMultiply_ARGB8888(&effectInBuffer, &effectOutBuffer, saturationMatrix, divisor, NULL, NULL, kvImageNoFlags);
            }
        }
        if (!effectImageBuffersAreSwapped)
            effectImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        if (effectImageBuffersAreSwapped)
            effectImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    
    // 开启上下文 用于输出图像
    UIGraphicsBeginImageContextWithOptions(self.size, NO, [[UIScreen mainScreen] scale]);
    CGContextRef outputContext = UIGraphicsGetCurrentContext();
    CGContextScaleCTM(outputContext, 1.0, -1.0);
    CGContextTranslateCTM(outputContext, 0, -self.size.height);
    
    // 开始画底图
    CGContextDrawImage(outputContext, imageRect, self.CGImage);
    
    // 开始画模糊效果
    if (hasBlur) {
        CGContextSaveGState(outputContext);
        if (maskImage) {
            CGContextClipToMask(outputContext, imageRect, maskImage.CGImage);
        }
        CGContextDrawImage(outputContext, imageRect, effectImage.CGImage);
        CGContextRestoreGState(outputContext);
    }
    
    // 添加颜色渲染
    if (tintColor) {
        CGContextSaveGState(outputContext);
        CGContextSetFillColorWithColor(outputContext, tintColor.CGColor);
        CGContextFillRect(outputContext, imageRect);
        CGContextRestoreGState(outputContext);
    }
    
    // 输出成品,并关闭上下文
    UIImage *outputImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return outputImage;
}

@end
