//
//  UIImage+SDJExtension.h
//  geniuslaiyeOC1.0
//
//  Created by 盛东 on 15/10/13.
//  Copyright © 2015年 shengdong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (SDJExtension)

// 画圆角矩形背景图, 这里, 在cell那里需要改. 
+ (UIImage *)getRoundImageWithSize:(CGSize)size backgroundColor:(UIColor *)color lineWidth:(CGFloat)lineWidth cornerRadius:(CGFloat)radius shouldFill:(BOOL)shouldFill;

// 图片裁剪成圆
- (UIImage *)cutCircleImagewithWidth:(CGFloat)width withColor:(UIColor *)color;

// 画填充背景图
+ (UIImage *)getImageWithSize:(CGSize)size backgroundColor:(UIColor *)color;

// 画聊天框
- (UIImage *)drawImagewithWidth:(CGFloat)width withColor:(UIColor *)color withDirection:(BOOL)isRight;

// 画气泡背景图
+ (UIImage *)drawBubbleBackgroundImagewithSize:(CGSize)size withWidth:(CGFloat)width withColor:(UIColor *)color withDirection:(BOOL)isRight;

// 加模糊效果,封装好,供外界调用的
- (UIImage *)imgWithBlur;

- (UIImage *)imgWithLightAlpha:(CGFloat)alpha radius:(CGFloat)radius colorSaturationFactor:(CGFloat)colorSaturationFactor;

@end
