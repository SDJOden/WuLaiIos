//
//  SDJCommonBtn.m
//  geniuslaiye2.0
//
//  Created by 盛东 on 16/3/8.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import "SDJCommonBtn.h"
#import "UIView+SDJExtension.h"

@implementation SDJCommonBtn

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGRect frame = self.imageView.frame;
    
    frame.size = self.ImageSize;
    
    self.imageView.frame = frame;
    
    if (self.labelSize.height != 0) {
        
        CGRect labelRect = self.titleLabel.frame;
        
        labelRect.size = self.labelSize;
        
        self.titleLabel.frame = labelRect;
    }
    
    self.imageView.y = (self.bounds.size.height - self.ImageSize.height) * 0.5;
    
    self.titleLabel.y = (self.bounds.size.height - self.titleLabel.h) * 0.5;
    
    if (self.btnType == ImageOnly) {
        
        self.imageView.x = (self.bounds.size.height - self.ImageSize.width) * 0.5;
        
    } else if (self.btnType == ImageAndLabel) {
        
        if (self.labelSize.height != 0) {
            
            self.imageView.x = (self.bounds.size.width - self.ImageSize.width - self.centerDistance - self.labelSize.width) * 0.5;
            
            self.titleLabel.x = self.imageView.x + self.ImageSize.width + self.centerDistance;
            
        } else {
            
            self.imageView.x = (self.bounds.size.width - self.ImageSize.width - self.centerDistance - self.titleLabel.w) * 0.5;
            
            self.titleLabel.x = self.imageView.x + self.ImageSize.width + self.centerDistance;
        }
        
    } else if (self.btnType == LabelAndImage) {
        
        self.titleLabel.x = (self.bounds.size.width - self.ImageSize.width - self.centerDistance - self.titleLabel.w) * 0.5;
        
        self.imageView.x = self.titleLabel.x + self.titleLabel.w + self.centerDistance;
        
    } else if (self.btnType == ImageUpLabel) {
        
        self.titleLabel.h = 20;
        
        self.titleLabel.w = self.bounds.size.width;
        
        self.imageView.x = (self.bounds.size.width - self.ImageSize.width) * 0.5;
        
        self.titleLabel.x = 0;
        
        self.imageView.y = 0;
        
        self.titleLabel.y = self.ImageSize.height + self.centerDistance;
        
    } else if (self.btnType == LabelOnly) {
        
        self.titleLabel.x = (self.bounds.size.width - self.titleLabel.w) * 0.5;
    }
}

- (void)setHighlighted:(BOOL)highlighted {
    
    if (self.HighlightNeeded == YES) {
        
        [super setHighlighted:highlighted];
    }
}

@end
