//
//  SDJCommonBtn.h
//  geniuslaiye2.0
//
//  Created by 盛东 on 16/3/8.
//  Copyright © 2016年 shengdong. All rights reserved.
//

#import <UIKit/UIKit.h>

// 通用按钮.

typedef enum : NSUInteger {
    ImageOnly,
    LabelOnly,
    LabelAndImage,
    ImageAndLabel,
    ImageUpLabel,
    LabelUpImage
} BtnSubViewType;

@interface SDJCommonBtn : UIButton

@property (nonatomic, assign)BtnSubViewType btnType;

@property (nonatomic, assign)CGSize ImageSize;

@property (nonatomic, assign)CGSize labelSize;

@property (nonatomic, assign)BOOL HighlightNeeded;

@property (nonatomic, assign)CGFloat centerDistance;

@end
