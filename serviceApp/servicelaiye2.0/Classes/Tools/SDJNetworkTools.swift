//
//  SDJNetworkTools.swift
//  geniuslaiyeOC1.0
//
//  Created by 盛东 on 15/10/9.
//  Copyright © 2015年 shengdong. All rights reserved.
//

//************************************************************************************************//
// 网络重构部分优化, 一是对AFN框架进行了解耦和代码抽取优化,二是对Error进行了枚举式集中处理
//************************************************************************************************//


import UIKit
import AFNetworking

private let LAIYE_BASEURL = (1 == 1 ? "http://genius.laiye.com" : "http://101.200.177.95:8229/"/*"http://m.laiye.com/"*/)

// 错误的类别标记
private let SDJErrorDomainName = "com.sd.error.network"

//***************************************错误方式集中处理枚举*********************************************//

private enum SDJNetworkError: Int { // 采用数值枚举
    
    case emptyDataError = -1
    
    case emptyTokenError = -2
    
    // 错误处理方案
    private var errorDescription: String {
        
        switch self {
        case .emptyDataError: return "数据 为空"
        case .emptyTokenError: return "Token 为空"
        }
    }
    
    // 返回枚举方式对应的处理方案
    private func error() -> NSError {
        // 跟之前的方案比较起来,调用的NSError方法仍然是一样的,不同的是error处理方式集中起来了,可读性更好,也更好维护
        return NSError(domain: SDJErrorDomainName, code: rawValue, userInfo: [SDJErrorDomainName: errorDescription])
    }
}

//*****************************************网络访问枚举*************************************************//
private enum SDJNetworkMethod: String {
    
    case GET = "GET"
    
    case POST = "POST"
}

class SDJNetworkTools: AFHTTPSessionManager {
    
    // 单例, swift 写法
    static let sharedTools: SDJNetworkTools = {
        
        let baseURL = NSURL(string:LAIYE_BASEURL)!
        
        let tools = SDJNetworkTools(baseURL: baseURL)
        
        tools.responseSerializer.acceptableContentTypes = NSSet(objects: "application/json", "text/json", "text/javascript", "text/plain", "text/html") as? Set<String>
        
        return tools
        }()
    
    //*****************************************网络数据请求*************************************************//
    //1.1 登入
    func login(username:String, password:String, limit:String, finished: SDJNetworkFinishedCallBack) {
        
        let urlString = "dkf/api/genius/login";
        
        let usernameAll = "Genius" + username + "@zhulilaiye";
        
        let params: [String:AnyObject] = ["username":usernameAll, "password":password, "limit":limit]
        
        request(SDJNetworkMethod.POST, urlString: urlString, params: params, finished: finished)
    }
    
    //1.2 登出
    func logout(token : String, finished : SDJNetworkFinishedCallBack) {
        
        let urlStirng = "/dkf/api/genius/logout?gt=" + token;
        //没有参数
        request(SDJNetworkMethod.POST, urlString: urlStirng, params: [String: AnyObject](), finished: finished)
    }
    
    //1.3 助理状态更改(接入数)
    func changeState(token : String, limit:Int, finished : SDJNetworkFinishedCallBack) {
        
        let urlStirng = "/dkf/api/genius/update-status?gt=" + token;
        
        let params: [String:AnyObject] = ["limit":limit]
        //没有参数
        request(SDJNetworkMethod.POST, urlString: urlStirng, params: params, finished: finished)
    }
    
    //1.4用户基础信息
    func userInformation(token : String, user_id : String, force : String,  finished : SDJNetworkFinishedCallBack) {
        
        let urlStirng = "/dkf/api/user/" + user_id + "?gt=" + token
        
        let params: [String:AnyObject] = ["user_id" : user_id, "force" : force]
        
        request(SDJNetworkMethod.GET, urlString: urlStirng, params: params, finished: finished)
    }
    
    //1.5 在线助理列表
    func onlineGeniusList(token : String, finished : SDJNetworkFinishedCallBack) {
        
        let urlStirng = "/dkf/api/genius/list?gt=" + token
        
        request(SDJNetworkMethod.GET, urlString: urlStirng, params:[String: AnyObject](), finished: finished)
    }
    //1.6 用户最近消息
    func userCurrentMessage(token : String, user_id : String, size : String, pn : String, finished : SDJNetworkFinishedCallBack) {
        
        let urlStirng = "/dkf/api/user/" + user_id + "/message/top?gt=" + token
        
        let params: [String:AnyObject] = ["user_id" : user_id, "size" : size, "pn" : pn]
        
        request(SDJNetworkMethod.GET, urlString: urlStirng, params: params, finished: finished)
    }
    
    //1.7 正在服务某用户的助理们
    func userByGeniusNow(token : String, user_id : String,  finished : SDJNetworkFinishedCallBack) {
        
        let urlStirng = "/dkf/api/user/" + user_id + "/genius?gt=" + token
        
        let params: [String:AnyObject] = ["user_id" : user_id]
        
        request(SDJNetworkMethod.GET, urlString: urlStirng, params: params, finished: finished)
    }
    
    //1.8 待接入
    func waitingUsers(token : String, finished : SDJNetworkFinishedCallBack) {
        
        let urlStirng = "/dkf/api/users/waiting?gt=" + token
        
        request(SDJNetworkMethod.GET, urlString: urlStirng, params: [String: AnyObject](), finished: finished)
    }
    
    //1.9 某助理正在接入的用户
    func didAccessUsers(token : String, finished : SDJNetworkFinishedCallBack) {
        
        let urlStirng = "/dkf/api/genius/users?gt=" + token
        
        request(SDJNetworkMethod.GET, urlString: urlStirng, params:[String: AnyObject](), finished: finished)
    }
    
    //1.10 [助理端]退出用户会话
    func leaveUserConversation(token : String, user_id:String, finished : SDJNetworkFinishedCallBack) {
        
        let urlStirng = "/dkf/api/user/" + user_id + "/conversation/leave?gt=" + token
        
        request(SDJNetworkMethod.POST, urlString: urlStirng, params:[String: AnyObject](), finished: finished)
    }
    
    //1.11 [助理端]加入用户会话，不踢出之前的助理
    func joinConversationWithOtherGenius(token : String, user_id : String, finished : SDJNetworkFinishedCallBack) {
        
        let urlStirng = "/dkf/api/user/"+user_id+"//conversation/join?gt="+token
        
        request(SDJNetworkMethod.POST, urlString: urlStirng, params:[String: AnyObject](), finished: finished)
    }
    
    //1.12 [助理端]加入用户会话，并踢出之前的助理
    func joinConversationWithoutOtherGenius(token : String, user_id : String, force : String, finished : SDJNetworkFinishedCallBack) {
        
        let urlStirng = "/dkf/api/user/" + user_id + "/conversation/start?gt=" + token
        
        let params : [String:AnyObject] = ["force" : force]
        
        request(SDJNetworkMethod.POST, urlString: urlStirng, params:params, finished: finished)
    }
    
    //1.13 [助理端]转接用户
    func changeUser(token : String, user_id : String, to : Int, extra_msg : String, finished : SDJNetworkFinishedCallBack) {
        
        let urlStirng = "/dkf/api/user/" + user_id + "/conversation/transfer?gt=" + token
        
        let params : [String:AnyObject] = ["to" : to, "extra_msg" : extra_msg]
        
        request(SDJNetworkMethod.POST, urlString: urlStirng, params:params, finished: finished)
    }
    
    //1.14 用户搜索
    func usersSearch(token : String, p : String, ps : String, user_id : String, phone : String, nickname : String, finished : SDJNetworkFinishedCallBack) {
        
        let urlStirng = "/dkf/api/users/search?gt=" + token + "&p=" + p + "&ps=" + ps
        
        let params : [String:AnyObject] = ["user_id" : user_id, "phone" : phone,"nickname" : nickname]
        
        request(SDJNetworkMethod.POST, urlString: urlStirng, params:params, finished: finished)
    }
    
    //1.15 批量用户信息接口
    func usersInfromationInterface(token : String, user_ids:String, finished : SDJNetworkFinishedCallBack) {
        
        let urlStirng = "/dkf/api/users?gt="+token
        
        let params : [String:AnyObject] = ["user_ids" : user_ids]
        
        request(SDJNetworkMethod.POST, urlString: urlStirng, params:params, finished: finished)
    }
    
    //1.16 用户地理位置信息
    func userLocationInformation(token : String, user_id:String, finished : SDJNetworkFinishedCallBack) {
        
        let urlStirng = "/dkf/api/user/" + user_id + "/location?gt=" + token
        
        let params : [String:AnyObject] = ["user_id" : user_id]
        
        request(SDJNetworkMethod.GET, urlString: urlStirng, params:params, finished: finished)
    }
    
    //1.17 最近发生会话的用户列表
    func recentListOfUsers(token : String, p:String, ps:String, finished : SDJNetworkFinishedCallBack) {
        
        let urlStirng = "/dkf/api/users/recent?gt=" + token
        
        let params : [String:AnyObject] = ["p" : p, "ps" : ps]
        
        request(SDJNetworkMethod.GET, urlString: urlStirng, params:params, finished: finished)
    }
    
    //1.18 助理基础信息
    func geniusBaseInformation(token : String, genius_id:String, finished : SDJNetworkFinishedCallBack) {
        
        let urlStirng = "/dkf/api/genius/" + genius_id + "?gt=" + token
        
        let params : [String:AnyObject] = ["genius_id" : genius_id]
        
        request(SDJNetworkMethod.GET, urlString: urlStirng, params:params, finished: finished)
    }
    
    //1.19 用户历史消息
    func userHistoryMessage(token : String, user_id:String, msg_date : String, pn : String, size : String, finished : SDJNetworkFinishedCallBack) {
        
        let urlStirng = "/dkf/api/user/" + user_id + "/message/history?gt=" + token
        
        let params : [String:AnyObject] = ["user_id" : user_id, "msg_date" : msg_date, "pn" : pn, "size" : size]
        
        request(SDJNetworkMethod.GET, urlString: urlStirng, params:params, finished: finished)
    }
    
    //1.20 用户组内成员
    func userGroupMembers(token : String, user_id:String, finished : SDJNetworkFinishedCallBack) {
        
        let urlStirng = "/dkf/api/user/" + user_id + "/group/members?gt=" + token
        
        let params : [String:AnyObject] = ["user_id" : user_id]
        
        request(SDJNetworkMethod.GET, urlString: urlStirng, params:params, finished: finished)
    }
    
    //1.21 图片上传,只支持.jpeg / .jpg / .png
    func imageUpload(token : String, image : UIImage, finished : SDJNetworkFinishedCallBack) {
        
        let urlStirng = "/dkf/api/genius/upload/image?gt=" + token
        
        let params : [String:AnyObject] = ["image" : image]

        request(SDJNetworkMethod.POST, image: image , urlString: urlStirng, params: params, finished: finished)
    }
    
    //1.22 消息搜索
    func messagesSearch(token : String, p : String, ps : String, msg : String, finished : SDJNetworkFinishedCallBack) {
        
        let urlStirng = "/dkf/api/messages/search?&p=" + p + "&ps=" + ps + "&gt=" + token
        
        let params : [String:AnyObject] = ["msg" : msg]
        
        request(SDJNetworkMethod.POST, urlString: urlStirng, params:params, finished: finished)
    }
    
    //1.23 快捷回复列表
    func quickReplies(token : String, finished : SDJNetworkFinishedCallBack) {
        
        let urlStirng = "/dkf/api/genius/quick-replies?gt=" + token
        
        request(SDJNetworkMethod.GET, urlString: urlStirng, params:[String: AnyObject](), finished: finished)
    }
    
    //1.24 [助理端]融云 token 刷新
    func rcTokenRefresh(token : String, finished : SDJNetworkFinishedCallBack) {
        
        let urlStirng = "/dkf/api/genius/rc-token/refresh?gt=" + token
        
        request(SDJNetworkMethod.POST, urlString: urlStirng, params:[String: AnyObject](), finished: finished)
    }
    
    //1.25 按服务类别更新助理接入状态
    func updateServiceType(token : String, service_id : String, finished : SDJNetworkFinishedCallBack) {
        
        let urlStirng = "/dkf/api/genius/update-service-type?gt=" + token
        
        let params : [String:AnyObject] = ["service_id" : service_id]
        
        request(SDJNetworkMethod.POST, urlString: urlStirng, params:params, finished: finished)
    }
    
    //1.26 某助理曾经接入的用户
    func recentUsers(token : String, p : String, ps : String, finished : SDJNetworkFinishedCallBack) {
        
        let urlStirng = "/dkf/api/genius/users/recent?gt=" + token
        
        let params : [String:AnyObject] = ["p" : p, "ps" : ps]
        
        request(SDJNetworkMethod.GET, urlString: urlStirng, params:params, finished: finished)
    }
    
    //1.27 助理当日接入人数排名
    func accessRank(token : String, finished : SDJNetworkFinishedCallBack) {
        
        let urlStirng = "/dkf/api/genius/ranks?gt=" + token
        
        request(SDJNetworkMethod.GET, urlString: urlStirng, params:[String: AnyObject](), finished: finished)
    }
    
    //1.28 助理 labels 列表
    func geniusLabel(token : String, force : Int, finished : SDJNetworkFinishedCallBack) {
        
        let urlStirng = "/dkf/api/genius/labels?gt=" + token
        
        let params : [String:AnyObject] = ["force" : force]
        
        request(SDJNetworkMethod.GET, urlString: urlStirng, params:params, finished: finished)
    }
    
    //1.29 按日期查看助理接入的用户
    func userOfGeniu(token : String, date : String, genius_id : String, p : String, ps : String, finished : SDJNetworkFinishedCallBack) {
        
        let urlStirng = "/dkf/api/genius/timeline/users?gt=" + token
        
        let params : [String:AnyObject] = ["date" : date, "genius_id" : genius_id, "p" : p, "ps" : ps]
        
        request(SDJNetworkMethod.GET, urlString: urlStirng, params:params, finished: finished)
    }
    
    //1.30 查小黑屋
    func blackRoom(token : String, finished : SDJNetworkFinishedCallBack) {
        
        let urlStirng = "/dkf/api/user/42699/black-room/status?gt=" + token
        
        request(SDJNetworkMethod.GET, urlString: urlStirng, params:[String: AnyObject](), finished: finished)
    }
    
    //1.31 关入小黑屋
    func putInBlackRoom(user_id : String, expire : String, finished : SDJNetworkFinishedCallBack) {
        
        let urlStirng = " /dkf/api/user/{" + user_id + "}/black-room/put-in"
        
        let params : [String:AnyObject] = ["user_id" : user_id, "expire" : expire]
        
        request(SDJNetworkMethod.GET, urlString: urlStirng, params:params, finished: finished)
    }
    
    //1.32 释放小黑屋
    func updateServiceTypes(user_id : String, finished : SDJNetworkFinishedCallBack) {
        
        let urlStirng = "/dkf/api/user/{" + user_id + "}/black-room/pull-out"
        
        let params : [String:AnyObject] = ["user_id" : user_id]
        
        request(SDJNetworkMethod.POST, urlString: urlStirng, params:params, finished: finished)
    }
    
    //1.33 获取助理的接入类型
    func serviceTypes(token : String, finished : SDJNetworkFinishedCallBack) {
        
        let urlStirng = "/dkf/api/genius/service-types?gt=" + token
        
        request(SDJNetworkMethod.GET, urlString: urlStirng, params:[String: AnyObject](), finished: finished)
    }
    
    //1.34 多选更改助理的接入类型
    func updateServiceTypes(token : String, service_ids : String, finished : SDJNetworkFinishedCallBack) {
        
        let urlStirng = "/dkf/api/genius/update-service-types?gt=" + token
        
        let params : [String:AnyObject] = ["service_ids" : service_ids]
        
        request(SDJNetworkMethod.POST, urlString: urlStirng, params:params, finished: finished)
    }
    
    //***************************************************************************************
    // MARK: - 封装AFN - 封装AFN 网络方法,便于替换网络访问方法,第三方框架的网络代码全部集中于此
    // 框架解耦
    
    // 网络回调类型别名
    typealias SDJNetworkFinishedCallBack = (result: [String: AnyObject]?, error: NSError?) -> ()
    
    // 新增image参数
    private func request(method: SDJNetworkMethod, image: UIImage? = nil, urlString: String, params: [String: AnyObject], finished: SDJNetworkFinishedCallBack) {
        
        // 1. 定义数据请求成功的闭包
        let successCallBack: (NSURLSessionDataTask?, AnyObject?) -> Void = { (_, JSON) -> Void in
            
            if let result = JSON as? [String: AnyObject] {
                
                finished(result: result, error: nil)
                
            } else {
                // 没有数据, 回调错误信息
                finished(result: nil, error: SDJNetworkError.emptyDataError.error())
            }
        }
        
        // 2. 定义数据请求出错的闭包
        let failedCallBack: (NSURLSessionDataTask?, NSError?) -> Void = { (_, error) -> Void in
            // 直接回调错误信息(网络错误信息,这是固定的网络通信错误代码库里的错误信息,而不是调用自己定义的错误代码源库)
            finished(result: nil, error: error)
        }
        
        // 3.调用第三方框架的网络数据请求方式
        if image == nil {
            // 无图模式的网络请求
            switch method {
                
            case .GET:
                
                GET(urlString, parameters: params, success: successCallBack, failure: failedCallBack)
                
            case .POST:
                
                POST(urlString, parameters: params, success: successCallBack, failure: failedCallBack)
                
            }
        } else {
            // 图文发送请求
            POST(urlString, parameters: params, constructingBodyWithBlock: { (fromData: AFMultipartFormData!) -> Void in
                // 1. 讲image转换成二进制数据
//                let data = UIImagePNGRepresentation(image!)
                let data = UIImageJPEGRepresentation(image!, 1.0)
                // 2. 构造结构体
                /*
                参数2: name, 服务器要求的字段名
                参数3: fileName, 服务器保存的文件名 提示: 看接口文档, 如果文档没有指定文件名参数, 可以随便写, 当然, 最好还是规范写
                参数4: mineType, 告诉服务器上传文件的类型, 如果不想详细描述, 可以直接写"application/octet-stream"
                */
//                fromData.appendPartWithFileData(data!, name: "pic", fileName: "xxx", mimeType: "application/octet-stream")
//                }, success: successCallBack, failure: failedCallBack)
                /**
                *  WY修改
                */
                fromData.appendPartWithFileData(data!, name: "image", fileName: "xxx", mimeType: "multipart/form-data")
                }, success: successCallBack, failure: failedCallBack)
        }
    }
}
