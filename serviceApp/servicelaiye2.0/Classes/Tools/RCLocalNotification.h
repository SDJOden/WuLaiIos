//
//  RCLocalNotification.h
//  RongIMKit
//
//  Created by xugang on 15/1/22.
//  Copyright (c) 2015年 RongCloud. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RCLocalNotification : NSObject

// 融云的推送, 使用的是自己的本地推送
+ (RCLocalNotification *)defaultCenter;

- (void)postLocalNotification:(NSString *)formatMessage userInfo:(NSDictionary *)userInfo;

@end
