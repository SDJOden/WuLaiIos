//
//  RCKitUtility.m
//  iOS-IMKit
//
//  Created by xugang on 7/7/14.
//  Copyright (c) 2014 Heq.Shinoda. All rights reserved.
//

#import "RCKitUtility.h"

@interface RCKitUtility ()

@end

@implementation RCKitUtility

+ (NSString *)ConvertChatMessageTime:(long long)secs {
    
    NSString *timeText = nil;

    NSDate *messageDate = [NSDate dateWithTimeIntervalSince1970:secs];

    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];

    NSString *strMsgDay = [formatter stringFromDate:messageDate];

    NSDate *now = [NSDate date];
    NSString *strToday = [formatter stringFromDate:now];
    NSDate *yesterday = [[NSDate alloc] initWithTimeIntervalSinceNow:-(24 * 60 * 60)];
    NSString *strYesterday = [formatter stringFromDate:yesterday];

    NSString *_yesterday = nil;
    if ([strMsgDay isEqualToString:strToday]) {
        [formatter setDateFormat:@"HH':'mm':'ss"];
    } else if ([strMsgDay isEqualToString:strYesterday]) {
        _yesterday = NSLocalizedString(@"Yesterday", @"");

        [formatter setDateFormat:@"HH:mm:ss"];
    } else {
        [formatter setDateFormat:@"yyyy-MM-dd' 'HH':'mm':'ss"];
    }
    
    timeText = [formatter stringFromDate:messageDate];
    
    if (nil != _yesterday) {
        timeText = [_yesterday stringByAppendingFormat:@" %@", timeText];
    }

    return timeText;
}

+ (UIImage *)imageNamed:(NSString *)name ofBundle:(NSString *)bundleName {
    
    UIImage *image = nil;
    NSString *image_name = [NSString stringWithFormat:@"%@.png", name];
    NSString *resourcePath = [[NSBundle mainBundle] resourcePath];
    NSString *bundlePath = [resourcePath stringByAppendingPathComponent:bundleName];
    NSString *image_path = [bundlePath stringByAppendingPathComponent:image_name];

    image = [[UIImage alloc] initWithContentsOfFile:image_path];

    return image;
}

+ (NSString *)formatMessage:(RCMessageContent *)messageContent {
    
    if ([messageContent isKindOfClass:[RCTextMessage class]]) {
        if ([((RCTextMessage *)messageContent).extra isKindOfClass:[NSDictionary class]]) {
            ((RCTextMessage *)messageContent).content = ((NSDictionary *)((RCTextMessage *)messageContent).extra)[@"msg_detail"][@"content"];
        }
        else {
            NSDictionary *msgJSONDic = [WYTool jsonStringToDictionary:((RCTextMessage *)messageContent).extra];
            ((RCTextMessage *)messageContent).content = msgJSONDic[@"msg_detail"][@"content"];
        }
    }
    
    if ([messageContent respondsToSelector:@selector(conversationDigest)]) {
        return [messageContent performSelector:@selector(conversationDigest)];
    }
    if ([messageContent isMemberOfClass:RCTextMessage.class]) {
        RCTextMessage *textMessage = (RCTextMessage *)messageContent;
        return textMessage.content;
    } else if ([messageContent isMemberOfClass:RCInformationNotificationMessage.class]) {
        RCInformationNotificationMessage *informationNotificationMessage =
            (RCInformationNotificationMessage *)messageContent;
        return informationNotificationMessage.message;
    }
    return [RCKitUtility localizedDescription:messageContent];
}

+ (NSString *)localizedDescription:(RCMessageContent *)messageContent {
    
    NSString *objectName = [[messageContent class] getObjectName];
    return NSLocalizedString(objectName, @"");
}

#pragma mark private method

+ (NSDictionary *)getNotificationUserInfoDictionary:(RCMessage *)message {
    
    return [RCKitUtility getNotificationUserInfoDictionary:message.conversationType fromUserId:message.senderUserId targetId:message.targetId messageContent:message.content];
}

+ (NSDictionary *)getNotificationUserInfoDictionary:(RCConversationType)conversationType fromUserId:(NSString *)fromUserId targetId:(NSString *)targetId messageContent:(RCMessageContent *)messageContent {
    
    NSString *type = @"PR";
    switch (conversationType) {
        case ConversationType_PRIVATE:
            type = @"PR";
            break;
        case ConversationType_GROUP:
            type = @"GRP";
            break;
        case ConversationType_DISCUSSION:
            type = @"DS";
            break;
        case ConversationType_CUSTOMERSERVICE:
            type = @"CS";
            break;
        case ConversationType_SYSTEM:
            type = @"SYS";
            break;
        case ConversationType_APPSERVICE:
            type = @"MC";
            break;
        case ConversationType_PUBLICSERVICE:
            type = @"MP";
            break;
        default:
            return nil;
    }
    return @{@"rc":@{@"cType":type, @"fId":fromUserId, @"oName":[[messageContent class] getObjectName], @"tId":targetId}};
}
@end
