//
//  UIView+SDJExtension.h
//  geniuslaiyeOC1.0
//
//  Created by 盛东 on 15/10/17.
//  Copyright © 2015年 shengdong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (SDJExtension)

@property (nonatomic, assign)CGFloat x;

@property (nonatomic, assign)CGFloat y;

@property (nonatomic, assign)CGFloat h;

@property (nonatomic, assign)CGFloat w;

@end
