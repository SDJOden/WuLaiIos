//
//  RCKitUtility.h
//  iOS-IMKit
//
//  Created by xugang on 7/7/14.
//  Copyright (c) 2014 Heq.Shinoda. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface RCKitUtility : NSObject

// 时间label转换
+ (NSString *)ConvertChatMessageTime:(long long)secs;

// 获取bundle里面图片
+ (UIImage *)imageNamed:(NSString *)name ofBundle:(NSString *)bundleName;

// 推送使用的消息转换
+ (NSString *)formatMessage:(RCMessageContent *)messageContent;

// 推送信息字典
+ (NSDictionary *)getNotificationUserInfoDictionary:(RCMessage *)message;

@end
