//
//  AppDelegate.m
//  servicelaiye2.0
//
//  Created by 盛东 on 15/12/9.
//  Copyright © 2015年 shengdong. All rights reserved.
//

#import "AppDelegate.h"
#import "WYLoginViewController.h"
#import <AudioToolbox/AudioToolbox.h>
#import "SDJUserInfoDataSource.h"
#import "Pingpp.h"
#import <CoreLocation/CoreLocation.h>
#import "WYMenuViewController.h"
#import <CoreLocation/CoreLocation.h>
#import "WYUnReadNumModel.h"
#define RONGCLOUD_IM_APPKEY (CODESTATU?@"ik1qhw091emcp":@"3argexb6r9cte")
#define kWXAPP_ID @"wx74034dbb800a0725"
#define kWXAPP_SECRET @"8bd2f25fe82ba38104ebb315686c94b4"
#define WYRootViewControllerSwitchNotification @"WYRootViewControllerSwitchNotification"

@interface AppDelegate ()

@property (nonatomic, strong)CLLocationManager *locationManager;

@property (nonatomic, assign)double lat;

@property (nonatomic, assign)double lng;

@property (nonatomic, assign)int flag;

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // 注册通知监听控制器跳转
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(switchRootViewController:) name:WYRootViewControllerSwitchNotification object:nil];
    
    // 初始化融云SDK
    [[RCIM sharedRCIM] initWithAppKey:RONGCLOUD_IM_APPKEY/*TEST_RONGCLOUD_IM_APPKEY*/];
    
    [[RCIM sharedRCIM] setUserInfoDataSource:[SDJUserInfoDataSource sharedUserInfoDataSource]];
    
    // 微信api
    [WXApi registerApp:kWXAPP_ID withDescription:@"LaiyeIsLove"];
    
    // 友盟统计
    [MobClick startWithAppkey:@"56d3c0c167e58e7c1d000862" reportPolicy:BATCH channelId:nil];
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    [MobClick setAppVersion:version];
    [MobClick setBackgroundTaskEnabled:YES];
    
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:NO];
    // 推送1
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveMessageNotification:)
     name:RCKitDispatchMessageNotification object:nil];
    
    [[RCIM sharedRCIM] setConnectionStatusDelegate:self];
    
    int i = 0;
    
    if (i == 0) {
        [NSThread sleepForTimeInterval:3.0];
        i = 6;
    } else {
        i = 5;
    }
    
    NSLog(@"%d", i);
    
#ifdef __IPHONE_8_0
    // 在 iOS 8 下注册苹果推送，申请推送权限。
    UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert categories:nil];
    [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
#else
    // 注册苹果推送，申请推送权限。
    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound];
#endif
    self.window.rootViewController = [self defaultViewController];
    
    [self.window makeKeyAndVisible];
    
    return YES;
}

/**
 *  设置根控制器
 */
-(UIViewController *)defaultViewController{
    if ([SDJUserAccount userLogin]) {
        [[SDJNetworkTools sharedTools] waitingUsers:[SDJUserAccount loadAccount].token finished:^(NSDictionary <NSString *,id> * _Nullable result, NSError * _Nullable error) {
            if (![result[@"error"] isEqual:[NSNull null]]) {//报错
                NSNumber *code = result[@"error"][@"error_code"];
                if (code.integerValue == 20000) {
                    NSString *UserPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES).firstObject stringByAppendingPathComponent:@"account.plist"];
                    [[NSFileManager defaultManager]removeItemAtPath:UserPath error:nil];
                    //其他平台登录，token改变需要先登出融云
                    [[RCIMClient sharedRCIMClient] logout];
                    // 发送通知让appdelegate去切换根视图为聊天列表界面
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [[NSNotificationCenter defaultCenter] postNotificationName:WYRootViewControllerSwitchNotification object:@"0"];
                    });
                }
            }
            else {
                [[RCIM sharedRCIM] connectWithToken:[SDJUserAccount loadAccount].rc_token success:^(NSString *userId) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [[NSNotificationCenter defaultCenter] postNotificationName:WYRootViewControllerSwitchNotification object:@"1"];
                    });
                    NSLog(@"login success with userid - %@",userId);
                } error:^(RCConnectErrorCode status) {
#warning to do
                    //------------------------需要加容错处理----------------------------------
                    NSLog(@"login error status: %ld.", (long)status);
                } tokenIncorrect:^{
#warning to do
                    //------------------------需要加容错处理----------------------------------
                    NSLog(@"token 无效 ，请确保生成token 使用的appkey 和初始化时的appkey 一致");
                }];
            }
        }];
#warning to do
        //------------------------这里应该展示启动页.----------------------------------
        UIViewController *VC = [[UIViewController alloc]init];
        
        VC.view.backgroundColor = [UIColor whiteColor];
        
        return VC;
    }
    else {

        return [[WYLoginViewController alloc]init];
    }
}

//  切换根控制器，通知
- (void)switchRootViewController: (NSNotification *)noti { // 需要切换到根控制器和登录控制器
    
    BOOL judge = [noti.object isEqualToString:@"1"] ? YES : NO;
    
    if (judge) {// 这里进入主视图
        
        WYMenuViewController *tabVC = [[WYMenuViewController alloc]init];
        
        tabVC.selectedIndex = 0;
        
        self.window.rootViewController = tabVC;
        
    } else {
        
        self.window.rootViewController = [[WYLoginViewController alloc] init];
        
        [WYUnReadNumModel shareUnReadNumModel].totalUnReadNum = 0;
    }
}

- (void)applicationWillResignActive:(UIApplication *)application {}

- (void)applicationWillEnterForeground:(UIApplication *)application {}

- (void)applicationDidBecomeActive:(UIApplication *)application {}

- (void)applicationWillTerminate:(UIApplication *)application {}

// 推送2: 注册用户通知设置
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:
(UIUserNotificationSettings *)notificationSettings {

    [application registerForRemoteNotifications];
}

// 推送3
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    
    NSString *token = [[[[deviceToken description] stringByReplacingOccurrencesOfString:@"<" withString:@""] stringByReplacingOccurrencesOfString:@">" withString:@""] stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    [[RCIMClient sharedRCIMClient] setDeviceToken:token];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo{ // 处理推送消息
}

// MARK: 代理
- (void)onRCIMConnectionStatusChanged:(RCConnectionStatus)status {
    
    if (status == ConnectionStatus_KICKED_OFFLINE_BY_OTHER_CLIENT) {
        
        // 退出
        SDJUserAccount *account = [SDJUserAccount loadAccount];
        
        account.available = NO;
        
        [account savaAccount];
        
        [[RCIM sharedRCIM] logout];
        
        self.window.rootViewController = [[WYLoginViewController alloc] init];
        
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:@"提示"
                              message:@"您"
                              @"的帐号在别的设备上登录，您被迫下线！"
                              delegate:nil
                              cancelButtonTitle:@"知道了"
                              otherButtonTitles:nil, nil];
        [alert show];
    }
}

- (void)didReceiveMessageNotification:(NSNotification *)noti { // 通知信息条数提示
    //发通知，刷新列表
    [UIApplication sharedApplication].applicationIconBadgeNumber = [WYUnReadNumModel shareUnReadNumModel].totalUnReadNum;
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
    
    [[RCIMClient sharedRCIMClient] recordLocalNotificationEvent:notification];
    //震动
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
    
    AudioServicesPlaySystemSound(1007);
}

// 推送4
- (void)application:(UIApplication *)application RemoteNotification:(nonnull NSDictionary *)userInfo {
    
    [[RCIMClient sharedRCIMClient] recordRemoteNotificationEvent:userInfo];
}

//退回桌面改变未读数量
-(void)applicationDidEnterBackground:(UIApplication *)application{
    
    application.applicationIconBadgeNumber = [WYUnReadNumModel shareUnReadNumModel].totalUnReadNum;
}

-(BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url{
    
    return [Pingpp handleOpenURL:url withCompletion:nil] || [WXApi handleOpenURL:url delegate:self];
}

-(BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation{
    
    return [Pingpp handleOpenURL:url withCompletion:nil] || [WXApi handleOpenURL:url delegate:self];
}

- (void)dealloc {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)applicationDidReceiveMemoryWarning:(UIApplication *)application{
    [[NSURLCache sharedURLCache]removeAllCachedResponses];
    //赶紧清除所有的内存缓存
    [[SDImageCache sharedImageCache] clearMemory];
    //赶紧停止正在进行的图片下载操作
    [[SDWebImageManager sharedManager] cancelAll];
}

@end
