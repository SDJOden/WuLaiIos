//
//  main.m
//  servicelaiye2.0
//
//  Created by 盛东 on 15/12/9.
//  Copyright © 2015年 shengdong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
