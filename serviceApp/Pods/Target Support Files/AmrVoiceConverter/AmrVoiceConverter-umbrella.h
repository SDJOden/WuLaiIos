#import <UIKit/UIKit.h>

#import "VoiceConverter.h"
#import "amrFileCodec.h"
#import "wav.h"
#import "interf_dec.h"
#import "interf_enc.h"
#import "dec_if.h"
#import "if_rom.h"

FOUNDATION_EXPORT double AmrVoiceConverterVersionNumber;
FOUNDATION_EXPORT const unsigned char AmrVoiceConverterVersionString[];

